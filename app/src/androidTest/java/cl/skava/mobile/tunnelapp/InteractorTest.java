package cl.skava.mobile.tunnelapp;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.executor.impl.ThreadExecutor;
import cl.skava.mobile.tunnelapp.domain.interactors.impl.home.WelcomingInteractorImpl;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.home.WelcomingInteractor;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.repository.ProjectRepository;
import cl.skava.mobile.tunnelapp.threading.MainThreadImpl;

import static org.mockito.Mockito.when;

/**
 * Created by Jose Ignacio Vera on 15-06-2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class InteractorTest extends TestCase {

    private WelcomingInteractor.Callback mMockedCallback;
    private ProjectRepository mRepository;
    private Executor threadExecutor;
    private MainThread mainThread;

    @Test
    public void testProjectListFound() throws Exception {

        Project project = new Project();
        project.setName("Project 1");
        project.setCode("DHSK121DSAD");
        project.setDescription("");
        project.setFinishDate(new Date());
        project.setStartDate(new Date());

        List<Project> projects = new ArrayList<>();
        projects.add(project);

        threadExecutor = ThreadExecutor.getInstance();
        mainThread = MainThreadImpl.getInstance();

        mMockedCallback = Mockito.mock(WelcomingInteractorImpl.Callback.class);
        mRepository = Mockito.mock(ProjectRepository.class);

        when(mRepository.getAllProjects())
                .thenReturn(projects);

//        WelcomingInteractorImpl interactor = new WelcomingInteractorImpl(
//                threadExecutor,
//                mainThread,
//                mMockedCallback,
//                mRepository
//        );
//        interactor.executeModel();

        Mockito.verify(mRepository).getAllProjects();
        Mockito.verifyNoMoreInteractions(mRepository);
        Mockito.verify(mMockedCallback).onProjectListRetrieved(projects);
    }
}
