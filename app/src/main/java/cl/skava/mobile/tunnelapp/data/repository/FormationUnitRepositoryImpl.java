package cl.skava.mobile.tunnelapp.data.repository;

import android.content.Context;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.repository.FormationUnitRepository;

/**
 * Created by Jose Ignacio Vera on 05-01-2017.
 */
public class FormationUnitRepositoryImpl implements FormationUnitRepository {

    private final HashMap<Project, List<FormationUnit>> formationUnitsByProject = new HashMap<>();
    private CloudStoreService cloudStoreService;

    public FormationUnitRepositoryImpl(Context activity) {
        cloudStoreService = new CloudStoreService(activity);
    }

    @Override
    public HashMap<Project, List<FormationUnit>> getFormationUnitsByProject() {
        List<Project> ps = cloudStoreService.getProjects();

        for(Project p : ps) {
            List<FormationUnit> formationUnits = cloudStoreService.getFormationUnitByProject(p.getId());
            formationUnitsByProject.put(p, formationUnits);
        }

        return formationUnitsByProject;
    }
}
