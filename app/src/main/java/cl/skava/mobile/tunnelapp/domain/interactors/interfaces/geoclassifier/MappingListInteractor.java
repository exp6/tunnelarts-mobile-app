package cl.skava.mobile.tunnelapp.domain.interactors.interfaces.geoclassifier;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.interactors.base.Interactor;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SelectionDetailLists;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public interface MappingListInteractor extends Interactor {

    interface Callback {
        //Interactor callback methods goes here
        void onMappingListRetrieved(List<Mapping> mappings);
        void onMappingInputListRetrieved(List<MappingInputModule> mappingInputModule);
        void onSyncStatus(boolean status, String msg);
        void onSyncProgressPercentaje(Integer percentaje);
        void notifySyncSegment(String msg);
        void onExportStatus(boolean status);
    }

    //Interactor methods goes here
    void create(Mapping mapping);
    void update(Mapping mapping);
    void delete(Mapping mapping);
    List<Mapping> getList(String faceId);
    void saveData();
    void getInputsByMapping(Mapping mapping);
    void onSyncError(String msg);
    void onSyncSuccess();
    void syncData(User user);
    void setSyncProgressPercentaje(Integer percentage);
    void notifySyncSegment(String msg);
    void exportData(Project p, Tunnel t, Face f, String directoryPath, SelectionDetailLists selectionDetailLists);
}
