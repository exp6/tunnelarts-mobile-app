package cl.skava.mobile.tunnelapp.domain.interactors.interfaces.home;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.interactors.base.Interactor;
import cl.skava.mobile.tunnelapp.domain.model.Face;

/**
 * Created by Jose Ignacio Vera on 21-06-2016.
 */
public interface TunnelSelectionInteractor extends Interactor {

    interface Callback {
        //Interactor callback methods goes here
        void onFaceListRetrieved(List<Face> faces);
    }

    //Interactor methods goes here
}
