package cl.skava.mobile.tunnelapp.domain.interactors.impl.home;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.base.AbstractInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.home.ProjectSelectionInteractor;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.repository.FaceRepository;
import cl.skava.mobile.tunnelapp.domain.repository.TunnelRepository;

/**
 * Created by Jose Ignacio Vera on 21-06-2016.
 */
public class ProjectSelectionInteractorImpl extends AbstractInteractor implements ProjectSelectionInteractor {

    private ProjectSelectionInteractor.Callback mCallback;
    private TunnelRepository mTunnelRepository;
    private FaceRepository mFaceRepository;
    private Project mProjectSelected;

    public ProjectSelectionInteractorImpl(Executor threadExecutor,
                                          MainThread mainThread,
                                          ProjectSelectionInteractor.Callback callback,
                                          TunnelRepository tunnelsRepository,
                                          FaceRepository facesRepository,
                                          Project selection) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mTunnelRepository = tunnelsRepository;
        mFaceRepository = facesRepository;
        mProjectSelected = selection;
    }

    private void postTunnels(final List<Tunnel> tunnels) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onTunnelListRetrieved(tunnels);
            }
        });
    }

    private void postFaces(final List<Face> faces) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFaceListRetrieved(faces);
            }
        });
    }

    @Override
    public void run() {
        final List<Tunnel> tunnels = mTunnelRepository.getTunnelsByProject(mProjectSelected);

        if(tunnels == null) {
            return;
        }

        final List<Face> faces;

        if(tunnels.isEmpty())
            faces = new ArrayList<>();
        else
            faces = mFaceRepository.getFacesByTunnel(tunnels.get(0));

        postFaces(faces);
        postTunnels(tunnels);

    }

}
