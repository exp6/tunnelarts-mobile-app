package cl.skava.mobile.tunnelapp.data.datasource.cloud.base;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.UserProject;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;

/**
 * Created by Jose Ignacio Vera on 23-09-2016.
 */
public interface DomainSyncService {

    void syncClient();
    void syncProjectsByUser();
    void syncProjects(List<UserProject> userProjects);
    void syncTunnels(List<Project> projects);
    void syncFaces(List<Tunnel> tunnels);
    void syncMappings(List<Face> faces);
    void syncMappingsInputs(List<Mapping> mappings);
    void syncMappingInput(Class c, String mappingId);
    void syncProspectionHoleGroups(List<Face> faces);
    void syncProspectionHoles(List<ProspectionHoleGroup> prospectionHoleGroups);
    void syncRods(List<ProspectionHole> prospections);
    void syncPictures(Class c);
    void syncExpanded(Class c);
    void syncGsi(Class c);
    void syncFormationUnit(List<Project> projects);
    void syncRockQuality(List<Project> projects);
    void syncProjectMappingInputs(List<Project> projects);
    void syncMappingInputs();
    void syncMappingsById(List<Mapping> mappings);
}
