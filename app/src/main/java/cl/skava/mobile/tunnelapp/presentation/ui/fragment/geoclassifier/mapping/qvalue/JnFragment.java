package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.qvalue;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;

/**
 * Created by Jose Ignacio Vera on 05-07-2016.
 */
public class JnFragment extends Fragment {

    private static final String TAG = "JnFragment";

    private List<Jn> jns = new ArrayList<>();
    private Boolean intersection = false;

    private OnJnValueListener mCallback;

    public interface OnJnValueListener {
        void OnJnSetValue(Double value, String id, Boolean intersection, int disc);
        void OnJnSetType(Integer type);
    }

    private boolean isValid;
    private EvaluationMethodSelectionType selectionMap;

    private Boolean mappingFinished;

    private Integer jnType;

    private final String lang = Locale.getDefault().getLanguage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initDataset();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_qvalue_jn, container, false);
        rootView.setTag(TAG);

        RadioGroup group = (RadioGroup) rootView.findViewById(R.id.radio_group1);
        RadioButton button;

        int i = 0;
        for(Jn jn : jns) {
            final Jn jn1 = jn;
            button = new RadioButton(getActivity());
            button.setId(i);
            button.setText(jn.getIndex().trim() + " (" + jn.getStart() + (jn.getEnd() != null ? " - " + jn.getEnd() : "") + ")     " + jn.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (jn1.getEnd() != null) {
                        execTextDialog(getResources().getString(R.string.characterization_mapping_input_qmet_label_range1)
                                + " " + jn1.getIndex(), getResources().getString(R.string.characterization_mapping_input_qmet_label_range2) +" "
                                + jn1.getStart() + " " + getResources().getString(R.string.characterization_mapping_input_qmet_label_range3) + " "
                                + jn1.getEnd(), jn1.getStart(), jn1.getEnd(), jn1.getId(), jn1
                                , (jn1.isChecked() ? enteredValue : (enteredValue >= jn1.getStart() && enteredValue <= jn1.getEnd() ? enteredValue : jn1.getStart())));
                    } else {
                        mCallback.OnJnSetValue(jn1.getStart(), jn1.getId(), intersection, getJointsAmount(jn1));
                        updateMap(jn1);
                    }
                }
            });
            button.setChecked(jn.isChecked());

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }

        RadioGroup groupType = (RadioGroup) rootView.findViewById(R.id.radio_group_type);

        switch (jnType) {
            case 1:
                groupType.check(R.id.radio_inter);
                break;
            case 2:
                groupType.check(R.id.radio_portal);
                break;
            default:
                groupType.check(R.id.radio_none);
                break;
        }

        groupType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                boolean isChecked = checkedRadioButton.isChecked();
                if (isChecked)
                {
                    switch (checkedRadioButton.getId()) {
                        case R.id.radio_inter:
                            mCallback.OnJnSetType(1);
                            break;
                        case R.id.radio_portal:
                            mCallback.OnJnSetType(2);
                            break;
                        default:
                            mCallback.OnJnSetType(0);
                            break;
                    }
                }
            }
        });

        return rootView;
    }

    private void updateMap(Jn jn) {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        EvaluationMethodSelection input;
        groups.get(0).setChecked(true);
        inputs = groups.get(0).getSubSelections();

        for(int j = 0; j < inputs.size(); j++) {
            input = inputs.get(j);
            input.setChecked(false);
            if(jn.getId().equalsIgnoreCase(input.getId())) {
                input.setChecked(true);
            }
        }

        groups.get(0).setSubSelections(inputs);
        selectionMap.setSelectionGroups(groups);
    }

    private Double enteredValue = null;

    public void execTextDialog(String title, String msg, Double start, Double end, String id, Jn jn, Double valueSelected) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this.getContext());
        alert.setTitle(title);
        alert.setMessage(msg);
        // Create TextView
        final EditText input = new EditText(this.getContext());
        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        final Double startValue = start;
        final Double endValue = end;
        final String id1 = id;

        input.addTextChangedListener(new TextValidator(input) {
            @Override
            public void validate(TextView textView, String text) {
                final String message = getResources().getString(R.string.characterization_mapping_input_qmet_label_range_validation1)
                        + " " + startValue
                        + " " + getResources().getString(R.string.characterization_mapping_input_qmet_label_range_validation2)
                        + " " + endValue;

                try {
                    Double inputValue = new Double(text);
                    if (inputValue.doubleValue() >= startValue.doubleValue() && inputValue.doubleValue() <= endValue.doubleValue()) {
                        isValid = true;
                        enteredValue = inputValue;
                    } else {
                        input.setError(message);
                        isValid = false;
                    }
                } catch (NumberFormatException e) {
                    input.setError(message);
                }
            }
        });

        final Jn jn1 = jn;

        Double valueFinal = round(valueSelected, 1);

        input.setText(valueFinal.toString());
        alert.setView(input);
        alert.setCancelable(false);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                final AlertDialog alertDialog = (AlertDialog)dialog;

                if(input.getText().toString().isEmpty()) {
                    enteredValue = startValue;
                }

                if(isValid) {
                    mCallback.OnJnSetValue(enteredValue, id1, intersection, getJointsAmount(jn1));
                    updateMap(jn1);
                    alertDialog.dismiss();
                }
                else {
                    input.setError(getResources().getString(R.string.characterization_mapping_input_qmet_label_range_validation3));
                }

            }
        });

        alert.show();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnJnValueListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnJnValueListener");
        }
    }

    public void setSelectionMap(EvaluationMethodSelectionType selectionMap) {
        this.selectionMap = selectionMap;
    }

    public EvaluationMethodSelectionType getSelectionMap() {
        return selectionMap;
    }

    public void setIntersection(Boolean intersection) {
        this.intersection = intersection;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public void setJnType(Integer jnType) {
        this.jnType = jnType;
    }

    public int getJointsAmount(Jn jn) {
        int amount = 1;

        if(jn.getIndex().trim().equalsIgnoreCase("A") || jn.getIndex().trim().equalsIgnoreCase("B")) amount = 1;
        if(jn.getIndex().trim().equalsIgnoreCase("C") || jn.getIndex().trim().equalsIgnoreCase("D")) amount = 2;
        if(jn.getIndex().trim().equalsIgnoreCase("E") || jn.getIndex().trim().equalsIgnoreCase("F")) amount = 3;
        if(jn.getIndex().trim().equalsIgnoreCase("G")) amount = 4;
        if(jn.getIndex().trim().equalsIgnoreCase("H") || jn.getIndex().trim().equalsIgnoreCase("J")) amount = 5;

        return amount;
    }

    public void setEnteredValue(Double enteredValue) {
        this.enteredValue = enteredValue;
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private void initDataset() {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        EvaluationMethodSelectionGroup e = groups.get(0);
        List<EvaluationMethodSelection> inputs = e.getSubSelections();

        Jn jn;

        for(EvaluationMethodSelection em : inputs) {
            jn = new Jn();
            jn.setId(em.getId());
            jn.setIndex(em.getIndex());
            jn.setStart(em.getStart());
            jn.setEnd(em.getEnd().doubleValue() == 0.0 ? null : em.getEnd());
            jn.setDescription((lang.equals("es") ? em.getDescriptionEs() : em.getDescription()));
            jn.setChecked(em.getChecked());
            jns.add(jn);
        }
    }

    class Jn {
        private String id;
        private String index;
        private Double value;
        private Double start;
        private Double end;
        private String description;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public Double getStart() {
            return start;
        }

        public void setStart(Double start) {
            this.start = start;
        }

        public Double getEnd() {
            return end;
        }

        public void setEnd(Double end) {
            this.end = end;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }

    public abstract class TextValidator implements TextWatcher {
        private final TextView textView;

        public TextValidator(TextView textView) {
            this.textView = textView;
        }

        public abstract void validate(TextView textView, String text);

        @Override
        final public void afterTextChanged(Editable s) {
            String text = textView.getText().toString();
            validate(textView, text);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { /* Don't care */ }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { /* Don't care */ }
    }
}
