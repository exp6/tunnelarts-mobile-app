package cl.skava.mobile.tunnelapp.domain.model.mapping;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;

/**
 * Created by Jose Ignacio Vera on 24-08-2016.
 */
public class EvaluationMethodSelectionType {

    private String id;
    private Integer code;
    private String description;
    private List<EvaluationMethodSelectionGroup> selectionGroups;
    private Integer methodTypeCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<EvaluationMethodSelectionGroup> getSelectionGroups() {
        return selectionGroups;
    }

    public void setSelectionGroups(List<EvaluationMethodSelectionGroup> selectionGroups) {
        this.selectionGroups = selectionGroups;
    }

    public Integer getMethodTypeCode() {
        return methodTypeCode;
    }

    public void setMethodTypeCode(Integer methodTypeCode) {
        this.methodTypeCode = methodTypeCode;
    }

    @Override
    public boolean equals(Object c) {
        if(!(c instanceof EvaluationMethodSelectionType)) {
            return false;
        }

        EvaluationMethodSelectionType that = (EvaluationMethodSelectionType)c;
        return this.id.equals(that.getId()) && this.id.equals(that.getId());
    }

    @Override
    public String toString() {
        return "EvaluationMethodSelectionType{" +
                "id='" + id + '\'' +
                ", code=" + code +
                ", description='" + description + '\'' +
                ", selectionGroups=" + selectionGroups +
                ", methodTypeCode=" + methodTypeCode +
                '}';
    }
}
