package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.MappingInputListAdapter;

/**
 * Created by Jose Ignacio Vera on 04-08-2016.
 */
public class MappingInputListItemsFragment extends Fragment {

    private static final String TAG = "MappingInputListItemsFragment";
    protected RecyclerView mRecyclerView;
    protected MappingInputListAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;

    protected List<MappingInputModule> mMappingInputModuleDataset;

    private OnModuleItemListener mCallback;

    public interface OnModuleItemListener {
        void onModuleItemSelected(int position, int listOrder);
        void userModulesLoaded();
    }

    private View rootView;
    private Mapping selectedMapping;

    private Toolbar toolbar;

    private int listOrder;
    private MappingInputModule selectedMappingInputModule;
    private int itemPosition;

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnModuleItemListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnModuleItemListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_inputs_list, container, false);
        rootView.setTag(TAG);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView1);
        setRecyclerViewLayoutManager();
        mAdapter = new MappingInputListAdapter(mMappingInputModuleDataset, mCallback, mRecyclerView, selectedMapping, itemPosition, listOrder);
        mRecyclerView.setAdapter(mAdapter);

        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        hideToolbar();

        if(listOrder == 1) {
            toolbar.setTitle(selectedMappingInputModule.getName());
            toolbar.setSubtitle(getResources().getString(R.string.characterization_label_swipe));
            showToolbar();
        }

        mCallback.userModulesLoaded();
        return rootView;
    }

    public void setRecyclerViewLayoutManager() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMMappingInputDataset(List<MappingInputModule> mMappingInputModuleDataset) {
        this.mMappingInputModuleDataset = mMappingInputModuleDataset;
    }

    public void updateItemList() {
        mAdapter = new MappingInputListAdapter(mMappingInputModuleDataset, mCallback, mRecyclerView, selectedMapping, itemPosition, listOrder);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void setSelectedMapping(Mapping mapping) {
        selectedMapping = mapping;
    }

    public void showToolbar() {
        toolbar.setVisibility(View.VISIBLE);
    }

    public void hideToolbar() {
        toolbar.setVisibility(View.GONE);
    }

    public void setListOrder(int listOrder) {
        this.listOrder = listOrder;
    }

    public void setSelectedMappingInputModule(MappingInputModule selectedMappingInputModule) {
        this.selectedMappingInputModule = selectedMappingInputModule;
    }

    public void setItemPosition(int itemPosition) {
        this.itemPosition = itemPosition;
    }
}
