package cl.skava.mobile.tunnelapp.presentation.presenters.impl.account;

import java.util.List;

import cl.skava.mobile.tunnelapp.data.cache.FileManager;
import cl.skava.mobile.tunnelapp.data.cache.JsonSerializer;
import cl.skava.mobile.tunnelapp.data.cache.user.UserCache;
import cl.skava.mobile.tunnelapp.data.cache.user.UserCacheImpl;
import cl.skava.mobile.tunnelapp.data.net.http.base.RestApi;
import cl.skava.mobile.tunnelapp.data.net.http.impl.RestApiImpl;
import cl.skava.mobile.tunnelapp.data.repository.UserRepositoryImpl;
import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.impl.account.LoginInteractorImpl;
import cl.skava.mobile.tunnelapp.domain.interactors.impl.account.UserCacheInteractorImpl;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.account.LoginInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.account.UserCacheInteractor;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.repository.UserRepository;
import cl.skava.mobile.tunnelapp.presentation.presenters.base.AbstractPresenter;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.account.LoginPresenter;

/**
 * Created by Jose Ignacio Vera on 28-07-2016.
 */
public class LoginPresenterImpl extends AbstractPresenter
        implements LoginPresenter,
        LoginInteractor.Callback,
        UserCacheInteractor.Callback {

    private RestApi mRestApi;
    private LoginPresenter.View mView;
    private String mDirectoryPath;

    public LoginPresenterImpl(Executor executor, MainThread mainThread, View view, String directoryPath) {
        super(executor, mainThread);
        mView = view;
        mDirectoryPath = directoryPath;
    }


    @Override
    public void validateUser(User user) {
        mView.hideProgress();

        UserCache userCache = new UserCacheImpl(new FileManager(), new JsonSerializer(), mDirectoryPath);
        UserRepository mUserRepository = new UserRepositoryImpl(userCache);
        LoginInteractor interactor = new LoginInteractorImpl(mExecutor,
                mMainThread,
                this,
                mUserRepository);

        interactor.validateUser(user);
    }

    @Override
    public void resume() {
        UserCache userCache = new UserCacheImpl(new FileManager(), new JsonSerializer(), mDirectoryPath);
        UserRepository mUserRepository = new UserRepositoryImpl(userCache);
        UserCacheInteractor interactor = new UserCacheInteractorImpl(mExecutor,
                mMainThread,
                this,
                mUserRepository);

        interactor.execute();
    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onUserValidationStatus(boolean status, User user) {
        mView.hideProgress();
        mView.onUserValidationStatus(status, user);
    }

    @Override
    public void onCachedUsers(List<User> users) {
        mView.onCachedUsers(users);
    }
}
