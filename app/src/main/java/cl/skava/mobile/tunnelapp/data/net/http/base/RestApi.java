package cl.skava.mobile.tunnelapp.data.net.http.base;

import java.net.MalformedURLException;

import cl.skava.mobile.tunnelapp.domain.model.account.User;

/**
 * Created by Jose Ignacio Vera on 27-07-2016.
 */
public interface RestApi {
    String API_BASE_URL = "http://tunnelarts-api.azurewebsites.net/";
    String API_URL_LOGIN_SERVICE = API_BASE_URL + "api/login/";

    void validateUserFromApi(User user) throws MalformedURLException;
    void onValidationResult(User user);
    User getmUser();
    void setmUser(User user);
}
