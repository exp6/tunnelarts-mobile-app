package cl.skava.mobile.tunnelapp.domain.model;

import java.io.Serializable;

/**
 * Created by JoseVera on 4/18/2017.
 */

public class ProjectMappingInput implements Serializable {

    private String id;
    private String idProject;
    private String idMappingInput;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public String getIdMappingInput() {
        return idMappingInput;
    }

    public void setIdMappingInput(String idMappingInput) {
        this.idMappingInput = idMappingInput;
    }

    @Override
    public String toString() {
        return "ProjectMappingInput{" +
                "id='" + id + '\'' +
                ", idProject='" + idProject + '\'' +
                ", idMappingInput='" + idMappingInput + '\'' +
                '}';
    }
}
