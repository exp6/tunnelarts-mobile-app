package cl.skava.mobile.tunnelapp.domain.interactors.interfaces.account;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.interactors.base.Interactor;
import cl.skava.mobile.tunnelapp.domain.model.account.User;

/**
 * Created by Jose Ignacio Vera on 25-08-2016.
 */
public interface UserCacheInteractor extends Interactor {

    interface Callback {
        void onCachedUsers(List<User> users);
    }
}
