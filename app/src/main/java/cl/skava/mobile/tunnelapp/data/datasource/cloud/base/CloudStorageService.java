package cl.skava.mobile.tunnelapp.data.datasource.cloud.base;

import java.io.File;
import java.util.List;

/**
 * Created by Jose Ignacio Vera on 01-09-2016.
 */
public interface CloudStorageService {

    void saveData(String referenceId, File sourceFile, String fileName);
    void setUploadDataStatus(Boolean status, String msg);
    void setDownloadDataStatus(Boolean status, String msg);
    void setDataExistsStatus(Boolean status, String msg);
}
