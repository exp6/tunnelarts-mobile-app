package cl.skava.mobile.tunnelapp.data.net.http.impl;

import android.os.AsyncTask;
import android.support.annotation.Nullable;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import cl.skava.mobile.tunnelapp.data.net.http.base.RestApi;
import cl.skava.mobile.tunnelapp.domain.model.account.User;

/**
 * Created by Jose Ignacio Vera on 27-07-2016.
 */
public class ApiConnection {

    private static final String CONTENT_TYPE_LABEL = "Content-Type";
    private static final String CONTENT_TYPE_VALUE_JSON = "application/json; charset=utf-8";

    private URL url;
    private String response;


    private ApiConnection(String url) throws MalformedURLException {
        this.url = new URL(url);
    }

    public static ApiConnection createGET(String url) throws MalformedURLException {
        return new ApiConnection(url);
    }

    @Nullable
    public String requestSyncCallGET() {
        connectToApi();
        return response;
    }

    @Nullable
    public String requestSyncCallPOST(String json, RestApi callback) {
        post(json, callback);
        return response;
    }

    private void connectToApi() {
        OkHttpClient okHttpClient = this.createClient();
        final Request request = new Request.Builder()
                .url(this.url)
                .addHeader(CONTENT_TYPE_LABEL, CONTENT_TYPE_VALUE_JSON)
                .get()
                .build();

        try {
            this.response = okHttpClient.newCall(request).execute().body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void post(String json, RestApi callback) {
        MediaType JSON = MediaType.parse(CONTENT_TYPE_VALUE_JSON);
        final OkHttpClient okHttpClient = this.createClient();
        RequestBody body = RequestBody.create(JSON, json);
        final Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        final RestApi mCallback = callback;

        new AsyncTask<Void, Void, Void>() {

            private Exception error = null;
            private String response;

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    response = okHttpClient.newCall(request).execute().body().string();
                } catch (IOException e) {
                    error = e;
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void result)
            {
                User user = mCallback.getmUser();
                user.setData(response);

                if(error != null) {
                    mCallback.onValidationResult(user);
                }
                else {
                    mCallback.onValidationResult(user);
                }
            }
        }.execute();
    }

    private OkHttpClient createClient() {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(10000, TimeUnit.MILLISECONDS);
        okHttpClient.setConnectTimeout(15000, TimeUnit.MILLISECONDS);

        return okHttpClient;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
