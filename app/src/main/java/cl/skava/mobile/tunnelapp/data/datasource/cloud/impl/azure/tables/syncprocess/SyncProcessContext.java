package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncprocess;

/**
 * Created by Jose Ignacio Vera on 23-09-2016.
 */
public class SyncProcessContext {
    private TableSyncProcess tableSyncProcess;

    public SyncProcessContext(TableSyncProcess tableSyncProcess){
        this.tableSyncProcess = tableSyncProcess;
    }

    public void executeProcess(){
        tableSyncProcess.doSync();
    }
}
