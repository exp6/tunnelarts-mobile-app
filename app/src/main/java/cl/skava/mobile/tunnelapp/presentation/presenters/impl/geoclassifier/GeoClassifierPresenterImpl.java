package cl.skava.mobile.tunnelapp.presentation.presenters.impl.geoclassifier;

import android.content.Context;

import java.util.List;

import cl.skava.mobile.tunnelapp.data.exports.base.ExportService;
import cl.skava.mobile.tunnelapp.data.exports.impl.xls.ExportServiceImpl;
import cl.skava.mobile.tunnelapp.data.repository.MappingRepositoryImpl;
import cl.skava.mobile.tunnelapp.data.repository.MappingInputRepositoryImpl;
import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.impl.geoclassifier.MappingListInteractorImpl;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.geoclassifier.MappingListInteractor;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SelectionDetailLists;
import cl.skava.mobile.tunnelapp.domain.repository.MappingInputRepository;
import cl.skava.mobile.tunnelapp.domain.repository.MappingRepository;
import cl.skava.mobile.tunnelapp.presentation.presenters.base.AbstractPresenter;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.geoclassifier.GeoClassifierPresenter;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public class GeoClassifierPresenterImpl extends AbstractPresenter
        implements GeoClassifierPresenter,
        MappingListInteractor.Callback
{

    private GeoClassifierPresenter.View mView;
    private MappingRepository mMappingRepository;
    private MappingInputRepository mMappingInputRepository;

    MappingListInteractor mappingListInteractor;

    private Context mActivity;
    private String mFaceId;

    private ExportService mExportService;

    public GeoClassifierPresenterImpl(Executor executor,
                                      MainThread mainThread,
                                      View view,
                                      Context activity,
                                      String faceId) {
        super(executor, mainThread);
        mView = view;
        mActivity = activity;
        mFaceId = faceId;
    }

    @Override
    public void getInputsByMapping(Mapping mapping) {
        mappingListInteractor.getInputsByMapping(mapping);
    }

    @Override
    public void createMapping(Mapping mapping) {
        mappingListInteractor.create(mapping);
    }

    @Override
    public void saveMappings() {
        mappingListInteractor.saveData();
    }

    @Override
    public void syncData(User user) {
        mappingListInteractor.syncData(user);
    }

    @Override
    public void deleteMapping(Mapping mapping) {
        mappingListInteractor.delete(mapping);
    }

    @Override
    public void updateMapping(Mapping mapping) {
        mappingListInteractor.update(mapping);
    }

    @Override
    public void exportData(Project p, Tunnel t, Face f, String directoryPath, SelectionDetailLists selectionDetailLists) {
        mappingListInteractor.exportData(p, t, f, directoryPath, selectionDetailLists);
    }

    @Override
    public void resume() {
        mView.showProgress();
        mMappingRepository = new MappingRepositoryImpl(mActivity, mFaceId);
        mMappingInputRepository = new MappingInputRepositoryImpl(mActivity, null);
        mExportService = new ExportServiceImpl(mMappingRepository.getCloudStoreService());

        mappingListInteractor = new MappingListInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mMappingRepository,
                mMappingInputRepository,
                mFaceId,
                mExportService
        );

        mappingListInteractor.execute();
    }

    @Override
    public void pause() {}

    @Override
    public void stop() {}

    @Override
    public void destroy() {}

    @Override
    public void onError(String message) {}

    @Override
    public void onMappingInputListRetrieved(List<MappingInputModule> mappingInputModule) {
        mView.hideProgress();
        mView.displayMappingInputList(mappingInputModule);
    }

    @Override
    public void onSyncStatus(boolean status, String msg) {
        mView.syncDataResult(status, msg);
    }

    @Override
    public void onSyncProgressPercentaje(Integer percentaje) {
        mView.onSyncProgressPercentaje(percentaje);
    }

    @Override
    public void notifySyncSegment(String msg) {
        mView.notifySyncSegment(msg);
    }

    @Override
    public void onExportStatus(boolean status) {
        mView.onExportFinish(status);
    }

    @Override
    public void onMappingListRetrieved(List<Mapping> mappings) {
        mView.hideProgress();
        mView.displayMappingList(mappings);
    }
}
