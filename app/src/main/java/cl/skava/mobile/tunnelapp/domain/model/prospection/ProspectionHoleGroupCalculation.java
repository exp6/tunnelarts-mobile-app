package cl.skava.mobile.tunnelapp.domain.model.prospection;

import java.util.Date;

/**
 * Created by Jose Ignacio Vera on 22-11-2016.
 */
public class ProspectionHoleGroupCalculation {

    private String id;
    private String idProspectionHoleGroup;
    private Integer rodPosition;
    private Float projectedDistance;
    private String estimatedRock;
    private Float qMax;
    private Float qMin;
    private Float qAvg;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdProspectionHoleGroup() {
        return idProspectionHoleGroup;
    }

    public void setIdProspectionHoleGroup(String idProspectionHoleGroup) {
        this.idProspectionHoleGroup = idProspectionHoleGroup;
    }

    public Integer getRodPosition() {
        return rodPosition;
    }

    public void setRodPosition(Integer rodPosition) {
        this.rodPosition = rodPosition;
    }

    public Float getProjectedDistance() {
        return projectedDistance;
    }

    public void setProjectedDistance(Float projectedDistance) {
        this.projectedDistance = projectedDistance;
    }

    public String getEstimatedRock() {
        return estimatedRock;
    }

    public void setEstimatedRock(String estimatedRock) {
        this.estimatedRock = estimatedRock;
    }

    public Float getqMax() {
        return qMax;
    }

    public void setqMax(Float qMax) {
        this.qMax = qMax;
    }

    public Float getqMin() {
        return qMin;
    }

    public void setqMin(Float qMin) {
        this.qMin = qMin;
    }

    public Float getqAvg() {
        return qAvg;
    }

    public void setqAvg(Float qAvg) {
        this.qAvg = qAvg;
    }

    @Override
    public String toString() {
        return "ProspectionHoleGroupCalculation{" +
                "id='" + id + '\'' +
                ", idProspectionHoleGroup='" + idProspectionHoleGroup + '\'' +
                ", rodPosition=" + rodPosition +
                ", projectedDistance=" + projectedDistance +
                ", estimatedRock='" + estimatedRock + '\'' +
                ", qMax=" + qMax +
                ", qMin=" + qMin +
                ", qAvg=" + qAvg +
                '}';
    }
}
