package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rockmasslith;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Lithology;

/**
 * Created by Jose Ignacio Vera on 04-07-2016.
 */
public class LithologyFragment extends Fragment {

    private static final String TAG = "LithologyFragment";

    private Lithology mappingInput;
    private Boolean mappingFinished;

    private List<String[]> rockTypes;
    private boolean instantiateFirst = false;
    private TextView briefDesc;
    private View rootView;

    private EditText inputPresence;
    private Spinner spinnerColourChroma;
    private Spinner spinnerGeneticGroup;
    private Spinner spinnerColourHue;
    private Spinner spinnerRockName;

    private Spinner spinnerStrength;
    private EditText inputFormationUnit;
    private Spinner spinnerTexture;
    private Spinner spinnerAlterationWeathering;
    private Spinner spinnerGrainSize;
    private Spinner spinnerColourValue;
    private EditText inputJvMin;
    private EditText inputJvMax;
    private Spinner spinnerBlockShape;
    private EditText inputBlockShapeL;
    private EditText inputBlockShapeW;
    private EditText inputBlockShapeH;

    private List<FormationUnit> formationUnits;
    private Spinner spinnerFormationUnit;

    private final String lang = Locale.getDefault().getLanguage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initRockTypeData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_rockmassl_1_lith, container, false);
        rootView.setTag(TAG);

        inputPresence = ((EditText) rootView.findViewById(R.id.input_presence));
        spinnerColourChroma = (Spinner) rootView.findViewById(R.id.spinner_colour_chroma);
        spinnerGeneticGroup = (Spinner) rootView.findViewById(R.id.spinner_genetic_group);
        spinnerColourHue = (Spinner) rootView.findViewById(R.id.spinner_colour_hue);
        spinnerRockName = (Spinner) rootView.findViewById(R.id.spinner_rock_name);
        spinnerStrength = (Spinner) rootView.findViewById(R.id.spinner_strength);
//        inputFormationUnit = ((EditText) rootView.findViewById(R.id.input_formation));
        spinnerTexture = (Spinner) rootView.findViewById(R.id.spinner_texture);
        spinnerAlterationWeathering = (Spinner) rootView.findViewById(R.id.spinner_alteration_w);
        spinnerGrainSize = (Spinner) rootView.findViewById(R.id.spinner_grain_size);
        spinnerColourValue = (Spinner) rootView.findViewById(R.id.spinner_colour_value);
        inputJvMin = ((EditText) rootView.findViewById(R.id.input_jv_min));
        inputJvMax = ((EditText) rootView.findViewById(R.id.input_jv_max));
        spinnerBlockShape = (Spinner) rootView.findViewById(R.id.spinner_block_shape);
        inputBlockShapeL = ((EditText) rootView.findViewById(R.id.input_block_shape_l));
        inputBlockShapeW = ((EditText) rootView.findViewById(R.id.input_block_shape_w));
        inputBlockShapeH = ((EditText) rootView.findViewById(R.id.input_block_shape_h));

        spinnerFormationUnit = (Spinner) rootView.findViewById(R.id.spinner_formation);
        setFormationUnits();

        inputPresence.setText(mappingInput.getPresence().toString());
        spinnerColourChroma.setSelection(mappingInput.getColourChroma());
        spinnerGeneticGroup.setSelection(mappingInput.getGeneticGroup() > 1 ? mappingInput.getGeneticGroup() - 1 : 0);
        spinnerColourHue.setSelection(mappingInput.getColorHue());
        setRockType(mappingInput.getGeneticGroup());
        spinnerRockName.setSelection(mappingInput.getSubGroup());
        instantiateFirst = true;
        spinnerStrength.setSelection(mappingInput.getStrength());
        spinnerTexture.setSelection(mappingInput.getTexture());
        spinnerAlterationWeathering.setSelection(mappingInput.getAlterationWeathering());
        spinnerGrainSize.setSelection(mappingInput.getGrainSize());
        spinnerColourValue.setSelection(mappingInput.getColourValue());
        inputJvMin.setText(mappingInput.getJvMin().toString());
        inputJvMax.setText(mappingInput.getJvMax().toString());
        spinnerBlockShape.setSelection(mappingInput.getBlockShape());
        inputBlockShapeL.setText(mappingInput.getBlockSizeOne().toString());
        inputBlockShapeW.setText(mappingInput.getBlockSizeTwo().toString());
        inputBlockShapeH.setText(mappingInput.getBlockSizeThree().toString());
        spinnerFormationUnit.setSelection(mappingInput.getFormationUnit());

        if(mappingFinished) {
            inputPresence.setEnabled(false);
            spinnerColourChroma.setEnabled(false);
            spinnerGeneticGroup.setEnabled(false);
            spinnerColourHue.setEnabled(false);
            spinnerRockName.setEnabled(false);
            spinnerStrength.setEnabled(false);
//            inputFormationUnit.setEnabled(false);
            spinnerTexture.setEnabled(false);
            spinnerAlterationWeathering.setEnabled(false);
            spinnerGrainSize.setEnabled(false);
            spinnerColourValue.setEnabled(false);
            inputJvMin.setEnabled(false);
            inputJvMax.setEnabled(false);
            spinnerBlockShape.setEnabled(false);
            inputBlockShapeL.setEnabled(false);
            inputBlockShapeW.setEnabled(false);
            inputBlockShapeH.setEnabled(false);
            spinnerFormationUnit.setEnabled(false);
        }

        inputPresence.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                setBriefDescText();

            }
        });

        spinnerColourChroma.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        spinnerGeneticGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (instantiateFirst) instantiateFirst = false;
                else setRockType(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        spinnerColourHue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        spinnerRockName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        spinnerStrength.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        spinnerTexture.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        spinnerAlterationWeathering.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        spinnerGrainSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        spinnerColourValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        return rootView;
    }

    private void setFormationUnits() {

        ArrayAdapter<String> adapter;
        List<String> list;

        list = new ArrayList<String>();

        list.add(lang.equals("es") ? "Seleccione Item..." : "Select Item...");
        for(FormationUnit f : formationUnits) {
            list.add(f.getLabel().trim());
        }

        adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFormationUnit.setAdapter(adapter);
    }

    private void setRockType(int position) {
        ArrayAdapter<String> a = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, rockTypes.get(position));
        spinnerRockName.setAdapter(a);
    }

    private void initRockTypeData() {
        rockTypes = new ArrayList<>(7);
        rockTypes.add(0, getResources().getStringArray(R.array.null_dropdown_arrays));
        rockTypes.add(1, getResources().getStringArray(R.array.igneous_plutonic_dropdown_arrays));
        rockTypes.add(2, getResources().getStringArray(R.array.igneous_volcanic_dropdown_arrays));
        rockTypes.add(3, getResources().getStringArray(R.array.pyroclastic_dropdown_arrays));
        rockTypes.add(4, getResources().getStringArray(R.array.sedimentary_clastic_dropdown_arrays));
        rockTypes.add(5, getResources().getStringArray(R.array.sedimentary_chemical_dropdown_arrays));
        rockTypes.add(6, getResources().getStringArray(R.array.metamorphic_dropdown_arrays));
    }

    private void setBriefDescText() {
        briefDesc = (TextView) rootView.findViewById(R.id.brief_desc);

        String briefDesc1 = inputPresence.getText().toString().trim();
        String briefDesc2 = spinnerAlterationWeathering.getSelectedItem().toString();
        String briefDesc3 = spinnerColourValue.getSelectedItem().toString();
        String briefDesc4 = spinnerColourChroma.getSelectedItem().toString();
        String briefDesc5 = spinnerColourHue.getSelectedItem().toString();
        String briefDesc7 = spinnerRockName.getSelectedItem().toString();
        String briefDesc8 = spinnerStrength.getSelectedItem().toString();
        String briefDesc9 = spinnerGrainSize.getSelectedItem().toString();
        String briefDesc10 = spinnerTexture.getSelectedItem().toString();

        StringBuilder sb = new StringBuilder();
        if(!briefDesc1.isEmpty()) sb.append(briefDesc1).append("% of ");
        if(spinnerAlterationWeathering.getSelectedItemPosition() != 0) sb.append(briefDesc2.toLowerCase());
        if(spinnerColourValue.getSelectedItemPosition() != 0) sb.append(", ").append(briefDesc3.toLowerCase());
        if(spinnerColourChroma.getSelectedItemPosition() != 0) sb.append(", ").append(briefDesc4.toLowerCase());
        if(spinnerColourHue.getSelectedItemPosition() != 0) sb.append("-").append(briefDesc5.toLowerCase());
        if(spinnerRockName.getSelectedItemPosition() != 0) sb.append(" ").append(briefDesc7.toLowerCase());
        if(spinnerStrength.getSelectedItemPosition() != 0) sb.append(" ").append(briefDesc8.toLowerCase());
        if(spinnerGrainSize.getSelectedItemPosition() != 0) sb.append(" ").append(briefDesc9.toLowerCase());
        if(spinnerTexture.getSelectedItemPosition() != 0) sb.append(" ").append(briefDesc10.toLowerCase());
        sb.append(".");

        briefDesc.setText(sb.toString());
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMappingInput(Lithology mappingInput) {
        this.mappingInput = mappingInput;
    }

    public Lithology getMappingInput() {
        return mappingInput;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public TextView getBriefDesc() {
        return briefDesc;
    }

    public View getRootView() {
        return rootView;
    }

    public void setFormationUnits(List<FormationUnit> formationUnits) {
        this.formationUnits = formationUnits;
    }
}
