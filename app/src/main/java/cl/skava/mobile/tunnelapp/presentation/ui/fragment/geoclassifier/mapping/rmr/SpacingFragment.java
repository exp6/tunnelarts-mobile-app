package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rmr;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;

/**
 * Created by Jose Ignacio Vera on 05-07-2016.
 */
public class SpacingFragment extends Fragment {

    private static final String TAG = "SpacingFragment";

    private List<Spacing> spacings = new ArrayList<>();

    private OnSpacingValueListener mCallback;

    public interface OnSpacingValueListener {
        void OnSpacingSetValue(Double value, String id);
    }

    private EvaluationMethodSelectionType selectionMap;

    private Boolean mappingFinished;

    private final String lang = Locale.getDefault().getLanguage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initDataset();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_rmr_spacing, container, false);
        rootView.setTag(TAG);

        RadioGroup group = (RadioGroup) rootView.findViewById(R.id.radio_group1);
        RadioButton button;

        int i = 0;
        for(Spacing spacing : spacings) {
            final Spacing spacing1 = spacing;

            button = new RadioButton(getActivity());
            button.setId(i);
//            button.setText(spacing.getIndex().trim() + " (" + spacing.getStart() + (spacing.getEnd() != null ? " - " + spacing.getEnd() : "") + ")     " + spacing.getDescription());
            button.setText("(" + spacing.getStart() + (spacing.getEnd() != null ? " - " + spacing.getEnd() : "") + ")     " + spacing.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mCallback.OnSpacingSetValue(spacing1.getStart(), spacing1.getId());
                    updateMap(spacing1);
                }
            });
            button.setChecked(spacing.isChecked());

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }

        return rootView;
    }

    private void updateMap(Spacing spacing) {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        EvaluationMethodSelection input;
        groups.get(0).setChecked(true);
        inputs = groups.get(0).getSubSelections();

        for(int j = 0; j < inputs.size(); j++) {
            input = inputs.get(j);
            input.setChecked(false);
            if(spacing.getId().equalsIgnoreCase(input.getId())) {
                input.setChecked(true);
            }
        }

        groups.get(0).setSubSelections(inputs);
        selectionMap.setSelectionGroups(groups);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnSpacingValueListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnSpacingValueListener");
        }
    }

    public void setSelectionMap(EvaluationMethodSelectionType selectionMap) {
        this.selectionMap = selectionMap;
    }

    public EvaluationMethodSelectionType getSelectionMap() {
        return selectionMap;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    private void initDataset() {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        EvaluationMethodSelectionGroup e = groups.get(0);
        List<EvaluationMethodSelection> inputs = e.getSubSelections();

        Spacing spacing;

        for(EvaluationMethodSelection em : inputs) {
            spacing = new Spacing();
            spacing.setId(em.getId());
            spacing.setIndex(em.getIndex());
            spacing.setStart(em.getStart());
            spacing.setEnd(em.getEnd().doubleValue() == 0.0 ? null : em.getEnd());
            spacing.setDescription((lang.equals("es") ? em.getDescriptionEs() : em.getDescription()));
            spacing.setChecked(em.getChecked());
            spacings.add(spacing);
        }
    }

    class Spacing {
        private String id;
        private String index;
        private Double value;
        private Double start;
        private Double end;
        private String description;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public Double getStart() {
            return start;
        }

        public void setStart(Double start) {
            this.start = start;
        }

        public Double getEnd() {
            return end;
        }

        public void setEnd(Double end) {
            this.end = end;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }
}
