package cl.skava.mobile.tunnelapp.domain.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Jose Ignacio Vera on 15-06-2016.
 */
public class Face implements Serializable {

    private String id;
    private String name;
    private String refChainage;
    private BigDecimal orientation;
    private BigDecimal inclination;
    private String idTunnel;
    private Integer direction;
    private Float progress;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRefChainage() {
        return refChainage;
    }

    public void setRefChainage(String refChainage) {
        this.refChainage = refChainage;
    }

    public BigDecimal getOrientation() {
        return orientation;
    }

    public void setOrientation(BigDecimal orientation) {
        this.orientation = orientation;
    }

    public BigDecimal getInclination() {
        return inclination;
    }

    public void setInclination(BigDecimal inclination) {
        this.inclination = inclination;
    }

    public String getIdTunnel() {
        return idTunnel;
    }

    public void setIdTunnel(String idTunnel) {
        this.idTunnel = idTunnel;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public Float getProgress() {
        return progress;
    }

    public void setProgress(Float progress) {
        this.progress = progress;
    }

    @Override
    public String toString() {
        return "Face{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", refChainage='" + refChainage + '\'' +
                ", orientation=" + orientation +
                ", inclination=" + inclination +
                ", idTunnel='" + idTunnel + '\'' +
                ", direction=" + direction +
                ", progress=" + progress +
                '}';
    }
}
