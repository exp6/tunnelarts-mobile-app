package cl.skava.mobile.tunnelapp.domain.model.mapping;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public class Mapping implements Serializable {

    private String id;
    private Double chainageStart;
    private Double chainageEnd;
    private Boolean completed;
    private Boolean finished;
    private String idFace;
    private String idUser;
    private Date createdAt;
    private Float direction;
    private Float slope;
    private Float roundLength;
    private Date mappingTime;

    public Double getChainageStart() {
        return chainageStart;
    }

    public void setChainageStart(Double chainageStart) {
        this.chainageStart = chainageStart;
    }

    public Double getChainageEnd() {
        return chainageEnd;
    }

    public void setChainageEnd(Double chainageEnd) {
        this.chainageEnd = chainageEnd;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Boolean getFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public String getIdFace() {
        return idFace;
    }

    public void setIdFace(String idFace) {
        this.idFace = idFace;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public boolean equals(Object o) {
        return o instanceof Mapping && ((Mapping) o).id.equalsIgnoreCase(id);
    }

    public Float getDirection() {
        return direction;
    }

    public void setDirection(Float direction) {
        this.direction = direction;
    }

    public Float getSlope() {
        return slope;
    }

    public void setSlope(Float slope) {
        this.slope = slope;
    }

    public Float getRoundLength() {
        return roundLength;
    }

    public void setRoundLength(Float roundLength) {
        this.roundLength = roundLength;
    }

    public Date getMappingTime() {
        return mappingTime;
    }

    public void setMappingTime(Date mappingTime) {
        this.mappingTime = mappingTime;
    }

    public String getFilesPath(String basePath) {
        if (id != null) {
            return basePath + File.separator + "media" + File.separator + id.toLowerCase();
        }
        return ""; //No hay id
    }

    @Override
    public String toString() {
        return "Mapping{" +
                "id='" + id + '\'' +
                ", chainageStart=" + chainageStart +
                ", chainageEnd=" + chainageEnd +
                ", completed=" + completed +
                ", finished=" + finished +
                ", idFace='" + idFace + '\'' +
                ", idUser='" + idUser + '\'' +
                ", createdAt=" + createdAt +
                ", direction=" + direction +
                ", slope=" + slope +
                ", roundLength=" + roundLength +
                ", mappingTime=" + mappingTime +
                '}';
    }
}
