package cl.skava.mobile.tunnelapp.domain.repository;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;

/**
 * Created by Jose Ignacio Vera on 05-01-2017.
 */
public interface RockQualityRepository {

    HashMap<Project, List<RockQuality>> getRockQualityByProject();
}
