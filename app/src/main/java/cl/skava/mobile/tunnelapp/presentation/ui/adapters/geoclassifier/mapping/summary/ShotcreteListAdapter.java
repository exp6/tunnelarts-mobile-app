package cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.mapping.summary;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RecommendationShotcrete;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.recommendations.ShotcreteFragment;

/**
 * Created by Jose Ignacio Vera on 12-10-2016.
 */
public class ShotcreteListAdapter extends RecyclerView.Adapter<ShotcreteListAdapter.ViewHolder>  {

    private static final String TAG = "ShotcreteListAdapter";
    protected RecyclerView mRecyclerView;
    private List<RecommendationShotcrete> mShotcreteList;
    private ShotcreteFragment mCallback;
    static ViewHolder viewHolderPool[];
    ViewHolder viewHolderPool1[];
    private final String lang = Locale.getDefault().getLanguage();

    public ShotcreteListAdapter(List<RecommendationShotcrete> shotcreteList, RecyclerView recyclerView, ShotcreteFragment callback) {
        mRecyclerView = recyclerView;
        mShotcreteList = shotcreteList;
        mCallback = callback;
        viewHolderPool = new ViewHolder[mShotcreteList.size()];
        viewHolderPool1 = viewHolderPool;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView shotcreteLabel;
        private final EditText thickness;
        private final Spinner fiberReinforced;
        private final EditText pk;
        private final Spinner location;
        private final EditText timeFrom;
        private final EditText timeTo;

        public ViewHolder(View v) {
            super(v);
            shotcreteLabel = (TextView) v.findViewById(R.id.shotcrete_label);
            thickness = (EditText) v.findViewById(R.id.input_thickness);
            fiberReinforced = (Spinner) v.findViewById(R.id.spinner_shotcrete);
            pk = (EditText) v.findViewById(R.id.input_pk);
            location = (Spinner) v.findViewById(R.id.spinner_location);
            timeFrom = (EditText) v.findViewById(R.id.input_rockbolt_time_from);
            timeTo = (EditText) v.findViewById(R.id.input_rockbolt_time_to);
        }

        public TextView getShotcreteLabel() {
            return shotcreteLabel;
        }

        public EditText getThickness() {
            return thickness;
        }

        public Spinner getFiberReinforced() {
            return fiberReinforced;
        }

        public EditText getPk() {
            return pk;
        }

        public Spinner getLocation() {
            return location;
        }

        public EditText getTimeFrom() {
            return timeFrom;
        }

        public EditText getTimeTo() {
            return timeTo;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_geoclassifier_mapping_summary_recommendations_shotcrete_item, parent, false);
        holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RecommendationShotcrete shotcrete = mShotcreteList.get(position);
        if (viewHolderPool[position] == null) viewHolderPool[position] = holder;
        final int position1 = position;
        viewHolderPool[position].getShotcreteLabel().setText((lang.equals("es") ? "Hormigón " : "Shotcrete ") + (shotcrete.getPosition() + 1));
        viewHolderPool[position].getThickness().setText(shotcrete.getThickness().toString());
        viewHolderPool[position].getFiberReinforced().setSelection(shotcrete.getFiberReinforced());
        viewHolderPool[position].getPk().setText(shotcrete.getPk().toString());
        viewHolderPool[position].getLocation().setSelection(shotcrete.getLocation());
        viewHolderPool[position].getTimeFrom().setText(shotcrete.getTimeFrom());
        viewHolderPool[position].getTimeTo().setText(shotcrete.getTimeTo());

        ImageView deleteItem = (ImageView) viewHolderPool[position].itemView.findViewById(R.id.delete_item);
        final ImageButton b1 = (ImageButton) viewHolderPool[position].itemView.findViewById(R.id.rockbolt_time_from_btn);
        b1.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTimePickerDialog(position1, 0);
                    }
                });
        final ImageButton b2 = (ImageButton) viewHolderPool[position].itemView.findViewById(R.id.rockbolt_time_to_btn);
        b2.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTimePickerDialog(position1, 1);
                    }
                });

        deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.deleteItem(position1);
            }
        });
        deleteItem.setVisibility(View.GONE);

        viewHolderPool[position].getLocation().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (position > 1) {
                    viewHolderPool[position1].getTimeFrom().setEnabled(true);
                    viewHolderPool[position1].getTimeTo().setEnabled(true);

                    b1.setEnabled(true);
                    b2.setEnabled(true);

                } else {
                    if (position == 1) {
                        viewHolderPool[position1].getTimeFrom().setText("");
                        viewHolderPool[position1].getTimeTo().setText("");
                        viewHolderPool[position1].getTimeFrom().setEnabled(false);
                        viewHolderPool[position1].getTimeTo().setEnabled(false);

                        b1.setEnabled(false);
                        b2.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        if(position == mShotcreteList.size()-1 && mShotcreteList.size() > 1) deleteItem.setVisibility(View.VISIBLE);
        viewHolderPool1 = viewHolderPool;
    }

    @Override
    public int getItemCount() {
        return mShotcreteList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public List<RecommendationShotcrete> getmShotcreteList() {
        return mShotcreteList;
    }

    public void showTimePickerDialog(int position, int inputTimeField) {
        DialogFragment newFragment = new TimePickerFragment();
        Bundle args = new Bundle();
        args.putInt("positionTime", position);
        args.putInt("inputTimeField", inputTimeField);
        newFragment.setArguments(args);
        newFragment.show(mCallback.getChildFragmentManager(), "timePicker");
    }

    public ViewHolder[] getViewHolderPool() {
        return viewHolderPool1;
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        private int positionTime;
        private int inputTimeField;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            Bundle args = getArguments();
            positionTime = args.getInt("positionTime");
            inputTimeField = args.getInt("inputTimeField");
            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            EditText mTime;

            switch(inputTimeField) {
                case 1:
                    mTime = (EditText) viewHolderPool[positionTime].itemView.findViewById(R.id.input_rockbolt_time_to);
                    break;
                default:
                    mTime = (EditText) viewHolderPool[positionTime].itemView.findViewById(R.id.input_rockbolt_time_from);
                    break;
            }

            mTime.setText(hourOfDay + ":" + (minute < 10 ? "0" + minute : minute));
        }
    }
}
