package cl.skava.mobile.tunnelapp.presentation.ui.fragment.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.home.TunnelListAdapter;

/**
 * Created by Jose Ignacio Vera on 07-06-2016.
 */
public class TunnelListFragment extends Fragment {

    private static final String TAG = "TunnelListFragment";

    protected RecyclerView mRecyclerView;
    protected TunnelListAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;

    protected List<Tunnel> mTunnelDataset;

    private OnTunnelItemSelectedListener mCallback;

    public interface OnTunnelItemSelectedListener {
        void onTunnelSelected(int position);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_tunnels, container, false);
        rootView.setTag(TAG);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        setRecyclerViewLayoutManager();
        mAdapter = new TunnelListAdapter(mTunnelDataset, mCallback, mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);
        return rootView;
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnTunnelItemSelectedListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnTunnelItemSelectedListener");
        }
    }

    public void setRecyclerViewLayoutManager() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMTunnelDataset(List<Tunnel> mTunnelDataset) {
        this.mTunnelDataset = mTunnelDataset;
    }

    public void updateItemList() {
        mAdapter = new TunnelListAdapter(mTunnelDataset, mCallback, mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);
    }
}
