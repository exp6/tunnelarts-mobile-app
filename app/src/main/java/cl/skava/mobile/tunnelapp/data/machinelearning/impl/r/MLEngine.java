package cl.skava.mobile.tunnelapp.data.machinelearning.impl.r;

/*import org.dmg.pmml.FieldName;
import org.jpmml.evaluator.DoubleVector;
import org.jpmml.evaluator.Evaluator;
import org.jpmml.evaluator.EvaluatorUtil;
import org.jpmml.evaluator.FieldValue;
import org.jpmml.evaluator.FieldValueUtil;
import org.jpmml.model.SerializationUtil;*/
//import org.rosuda.REngine.REXPMismatchException;
//import org.rosuda.REngine.REngineException;
//import org.rosuda.REngine.Rserve.RConnection;
//import org.rosuda.REngine.Rserve.RserveException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cl.skava.mobile.tunnelapp.data.cache.FileManager;
import cl.skava.mobile.tunnelapp.data.machinelearning.base.MachineLearningService;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;

/**
 * Created by Jose Ignacio Vera on 02-08-2016.
 */
public class MLEngine implements MachineLearningService {

    private FileManager mFileManager;
    private File mDirectory;
    private File mModelFile;

    private List<InputStream> mModelsIn;
    private List<OutputStream> mModelsOut;
    private List<File> mOutputFiles;

    private List<Rod> mRods;
    private Float mInitialQ;

    private List<Rod> mRodsAux;

    public MLEngine(File modelFile, InputStream modelFileIn, OutputStream modelFileOut, List<InputStream> modelsIn, List<OutputStream> modelsOut, List<File> outputFiles, List<Rod> rods, Float initialQ) {
        mModelFile = modelFile;
        mDirectory = mModelFile.getParentFile();
        mFileManager = new FileManager();

        mModelsIn = modelsIn;
        mModelsOut = modelsOut;
        mOutputFiles = outputFiles;

        mRods = rods;
        mRodsAux = new ArrayList<>();
        mInitialQ = initialQ;

        initInputData(modelFileIn, modelFileOut);
        initDataset();
    }

    private void initInputData(InputStream modelFileIn, OutputStream modelFileOut) {
        if (!mDirectory.exists()) {
            try{
                mDirectory.mkdir();
            }
            catch(SecurityException se){
                //handle it
            }
        }

        try {
            mFileManager.copyFile(modelFileIn, modelFileOut);

            for(int i = 0; i < mOutputFiles.size(); i++ ) {
                mFileManager.copyFile(mModelsIn.get(i), mModelsOut.get(i));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                modelFileIn.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                modelFileOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
//    private void sortDataset() {
//        List<Rod> newSortList = new ArrayList<>();
//
//        for(int i = mRods.size()-1; i >= 0; --i) {
//            newSortList.add(mRods.get(i));
//        }
//        mRods = newSortList;
//    }

    private void initDataset() {
        Float cumulativeDepth = new Float(0.0);
        Rod newRod;
        for(Rod r : mRods) {
            newRod = new Rod();
            cumulativeDepth += r.getDrilledLength();
            newRod.setDrilledLength(cumulativeDepth);
            mRodsAux.add(newRod);
        }
    }

    @Override
    public void executeModel() {

//        RConnection connection = null;
//
//        String linuxPath = "/sdcard" + mModelFile.getPath().substring(19);
//        String linuxPathParent = "/sdcard" + mDirectory.getPath().substring(19);
//
//        try {
//             /* Create a connection to Rserve instance running
//              * on default port 6311
//              */
//            connection = new RConnection();
////            connection.eval("source('"+ mModelFile.getPath() +"')");
//            connection.eval("source('"+ linuxPath +"')");
//
//            String vectorStr = "c(";
//            String speedVector = vectorStr;
//            String pressureVector = vectorStr;
//            String depthVector = vectorStr;
//
//            Rod r;
//
////            sortDataset();
//
//            for(int i=0;i<mRods.size();i++) {
//                r = mRods.get(i);
//
//                if((i+1) == mRods.size()) {
//                    speedVector += (round(new Double(r.getSpeed()), 4));
//                    pressureVector += (round(new Double(r.getAveragePressure()), 4));
//                    depthVector += (round(new Double(mRodsAux.get(i).getDrilledLength()), 4));
//                }
//                else {
//                    speedVector += (round(new Double(r.getSpeed()), 4)) + ",";
//                    pressureVector += (round(new Double(r.getAveragePressure()), 4)) + ",";
//                    depthVector += (round(new Double(mRodsAux.get(i).getDrilledLength()), 4)) + ",";
//                }
//            }
//
//            vectorStr = ")";
//            speedVector += vectorStr;
//            pressureVector += vectorStr;
//            depthVector += vectorStr;
//
//            Double previousQ = round(new Double(mInitialQ), 4);
//
//            //prediccion_Q(c(2,2),c(3,2),c(2,4),0.5)
//
////            String statement = "prediccion_Q(" + speedVector + "," + pressureVector + "," + depthVector + "," + previousQ + ",\"" + mDirectory.getPath() + "\")";
//            String statement = "prediccion_Q(" + speedVector + "," + pressureVector + "," + depthVector + "," + previousQ + ",\"" + linuxPathParent + "\")";
//            System.out.println(statement);
//            int result = connection.eval(statement).asInteger();
//            mRods = processOutputData(result);
//
//        } catch (RserveException e) {
//            e.printStackTrace();
//        } catch (REXPMismatchException e) {
//            e.printStackTrace();
//        } catch (REngineException e) {
//            e.printStackTrace();
//        } finally{
//            connection.close();
//        }
    }

    @Override
    public List<Rod> getFinalRods() {
        return this.mRods;
    }

    private List<Rod> processOutputData(int result) {
        if(result == 1) {
            File outputFile = new File(mDirectory.getPath(), "Predicted_Q.txt");
            String fileContent = mFileManager.readFileContent(outputFile);

            String[] parts = fileContent.split("\n");
            String resultLine;

            for(int i=1; i < parts.length; i++) {
                resultLine = parts[i];
                String[] subParts = resultLine.split(";");

                Pattern p = Pattern.compile("\"([^\"]*)\"");
                Matcher m = p.matcher(subParts[1]);

                Float newQValue = new Float(0);

                while (m.find()) {
                    newQValue = new Float(m.group(1));
                }

                mRods.get(i-1).setqValue(newQValue);
            }
        }

        System.out.println("MODEL PREDICTION RESULT: " + (result == 1 ? "SUCCESS" : "FAILED"));
        mFileManager.clearDirectory(mDirectory);

//        sortDataset();
        return mRods;
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_DOWN);
        return bd.doubleValue();
    }
}
