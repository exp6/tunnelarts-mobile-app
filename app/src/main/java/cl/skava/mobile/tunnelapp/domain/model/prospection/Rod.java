package cl.skava.mobile.tunnelapp.domain.model.prospection;

import java.util.Date;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class Rod {
    private String id;
    private String name;
    private Float speed;
    private Float averagePressure;
    private Float drilledLength;
    private Float qPrevious;
    private String waterColor;
    private Float qValue;
    private String idProspectionHole;
    private Date createdAt;
    private Float time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getSpeed() {
        return speed;
    }

    public void setSpeed(Float speed) {
        this.speed = speed;
    }

    public Float getAveragePressure() {
        return averagePressure;
    }

    public void setAveragePressure(Float averagePressure) {
        this.averagePressure = averagePressure;
    }

    public Float getDrilledLength() {
        return drilledLength;
    }

    public void setDrilledLength(Float drilledLength) {
        this.drilledLength = drilledLength;
    }

    public Float getqPrevious() {
        return qPrevious;
    }

    public void setqPrevious(Float qPrevious) {
        this.qPrevious = qPrevious;
    }

    public String getWaterColor() {
        return waterColor;
    }

    public void setWaterColor(String waterColor) {
        this.waterColor = waterColor;
    }

    public Float getqValue() {
        return qValue;
    }

    public void setqValue(Float qValue) {
        this.qValue = qValue;
    }

    public String getIdProspectionHole() {
        return idProspectionHole;
    }

    public void setIdProspectionHole(String idProspectionHole) {
        this.idProspectionHole = idProspectionHole;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Float getTime() {
        return time;
    }

    public void setTime(Float time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Rod{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", speed=" + speed +
                ", averagePressure=" + averagePressure +
                ", drilledLength=" + drilledLength +
                ", qPrevious=" + qPrevious +
                ", waterColor='" + waterColor + '\'' +
                ", qValue=" + qValue +
                ", idProspectionHole='" + idProspectionHole + '\'' +
                ", createdAt=" + createdAt +
                ", time=" + time +
                '}';
    }
}
