package cl.skava.mobile.tunnelapp.presentation.ui.fragment.prospection;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.prospection.ProspectionAvgListAdapter;

/**
 * Created by Jose Ignacio Vera on 24-08-2016.
 */
public class ProspectionModulesFragment extends Fragment
        implements ProspectionImagesFragment.OnProspectionPicturelListener, ProspectionAvgListFragment.OnProspectionAvgListener {

    private static final String TAG = "ProspectionModulesFragment";

    protected RecyclerView mRecyclerView;
    protected ProspectionAvgListAdapter mAdapter;

    protected RecyclerView.LayoutManager mLayoutManager;

    //User Selection Context
    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Module selectedModule;
    private User mUser;
    private ProspectionHoleGroup selectedProspectionHoleGroup;
    private HashMap<ProspectionHole, List<Rod>> rodsByProspection;

    private Button goToForecasting;

    private OnProspectionModulesListener mCallback;

    private List<ProspectionRefPicture> mProspectionRefPictures;

    @Override
    public void onProspectionPicturelListener(int prospectionHoleGroupPosition) {
        mCallback.onProspectionPicturelListener(prospectionHoleGroupPosition);
    }

    @Override
    public void onTakeRefPicture() {
        mCallback.onTakeRefPicture();
    }

    @Override
    public void onEditRefPicture() {
        mCallback.onEditRefPicture();
    }

    @Override
    public void onUndoRefPicture() {
        mCallback.onUndoRefPicture();
    }

    @Override
    public void onCalculationsReady(List<Double> avgDistancesPerPosition, List<Double> avgQValuesPerPosition, List<Double> minQValuesPerPosition, List<Double> maxQValuesPerPosition, List<String> estimatedRockPerPosition) {
        mCallback.onCalculationsReady(avgDistancesPerPosition, avgQValuesPerPosition, minQValuesPerPosition, maxQValuesPerPosition, estimatedRockPerPosition);
    }

    public interface OnProspectionModulesListener {
        void goToForecasting();
        void onProspectionPicturelListener(int prospectionHoleGroupPosition);
        void onTakeRefPicture();
        void setRefPictures();
        void onEditRefPicture();
        void onUndoRefPicture();
        void onCalculationsReady(List<Double> avgDistancesPerPosition,
                                 List<Double> avgQValuesPerPosition,
                                 List<Double> minQValuesPerPosition,
                                 List<Double> maxQValuesPerPosition,
                                 List<String> estimatedRockPerPosition);
    }

    private TabLayout tabLayout;
    private int NUM_INPUTS;

    private List<Fragment> modulesFragments;

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;

    private Integer viewpagerPosition;
    private Integer prospectionHoleGroupPosition;

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnProspectionModulesListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnProspectionModulesListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_prospection_modules, container, false);
        rootView.setTag(TAG);
//        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
//        setRecyclerViewLayoutManager();
//
//        if(rodsByProspection != null) {
//            mAdapter = new ProspectionAvgListAdapter(rodsByProspection, mRecyclerView);
//            mRecyclerView.setAdapter(mAdapter);
//        }

        if(mProspectionRefPictures == null) mCallback.setRefPictures();

        TextView title = (TextView) rootView.findViewById(R.id.prospection_avg_title);

        goToForecasting = (Button) rootView.findViewById(R.id.go_to_forecasting);
        goToForecasting.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCallback.goToForecasting();
            }
        });

        if(selectedProspectionHoleGroup == null) {
            title.setText(getResources().getString(R.string.forecasting_label_select_module_add_new));
            goToForecasting.setVisibility(View.GONE);
        }
        else {
            title.setText(selectedProspectionHoleGroup.getName().trim());
            goToForecasting.setVisibility(View.VISIBLE);
        }

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);

        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.forecasting_label_select_module_1)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.forecasting_label_select_module_2)));

        modulesFragments = new ArrayList<>();
        ProspectionAvgListFragment prospectionAvgListFragment = new ProspectionAvgListFragment();
        prospectionAvgListFragment.setRodsByProspection(rodsByProspection);
        prospectionAvgListFragment.onAttachFragment(this);
        modulesFragments.add(prospectionAvgListFragment);

        ProspectionImagesFragment prospectionImagesFragment = new ProspectionImagesFragment();
        prospectionImagesFragment.setProspectionHoleGroupPosition(prospectionHoleGroupPosition);
        prospectionImagesFragment.onAttachFragment(this);

        if(mProspectionRefPictures != null) prospectionImagesFragment.setmProspectionRefPictures(mProspectionRefPictures);

        modulesFragments.add(prospectionImagesFragment);

        NUM_INPUTS = modulesFragments.size();

        mPager = (ViewPager) rootView.findViewById(R.id.modules_content);
        mPagerAdapter = new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager(), modulesFragments);
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        TabLayout.Tab tab = tabLayout.getTabAt(position);
                        tab.select();
                    }
                });

        mPager.setCurrentItem(viewpagerPosition);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setRecyclerViewLayoutManager() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

//    public void updateItemList() {
//        mAdapter = new ProspectionAvgListAdapter(rodsByProspection, mRecyclerView);
//        mRecyclerView.setAdapter(mAdapter);
//    }

    public void setSelectionContext(Project project,
                                    Tunnel tunnel,
                                    Face face,
                                    Module module,
                                    User user,
                                    ProspectionHoleGroup prospectionHoleGroup) {
        selectedProject = project;
        selectedTunnel = tunnel;
        selectedFace = face;
        selectedModule = module;
        mUser = user;
        selectedProspectionHoleGroup = prospectionHoleGroup;
    }

    public void setRodsByProspection(HashMap<ProspectionHole, List<Rod>> rodsByProspection) {
        this.rodsByProspection = rodsByProspection;
    }

    public void setViewpagerPosition(Integer viewpagerPosition) {
        this.viewpagerPosition = viewpagerPosition;
    }

    public void setProspectionHoleGroupPosition(Integer prospectionHoleGroupPosition) {
        this.prospectionHoleGroupPosition = prospectionHoleGroupPosition;
    }

    public void setmProspectionRefPictures(List<ProspectionRefPicture> mProspectionRefPictures) {
        this.mProspectionRefPictures = mProspectionRefPictures;
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        private List<Fragment> mInputFragments;

        public ScreenSlidePagerAdapter(FragmentManager fm, List<Fragment> inputFragments) {
            super(fm);
            mInputFragments = inputFragments;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = mInputFragments.get(position);
            return fragment;
        }

        @Override
        public int getCount() {
            return NUM_INPUTS;
        }

        public Fragment getInputFragments(int position) {
            return mInputFragments.get(position);
        }
    }
}
