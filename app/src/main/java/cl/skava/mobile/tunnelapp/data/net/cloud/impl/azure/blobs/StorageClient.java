package cl.skava.mobile.tunnelapp.data.net.cloud.impl.azure.blobs;

import android.os.AsyncTask;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.BlobContainerPermissions;
import com.microsoft.azure.storage.blob.BlobContainerPublicAccessType;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.blob.ListBlobItem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudStorageService;

/**
 * Created by Jose Ignacio Vera on 01-09-2016.
 */
public class StorageClient {

    private String storageConnectionString;
    private CloudBlobClient serviceClient;
    private CloudStorageService mCallbackService;

    public StorageClient(String connectionStr, CloudStorageService callbackService) {
        storageConnectionString = connectionStr;
        mCallbackService = callbackService;
        createCloudBlobClient();
    }

    private void createCloudBlobClient() {
        try {
            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.parse(storageConnectionString);
            serviceClient = cloudStorageAccount.createCloudBlobClient();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    public void uploadData(String containerId, File sourceFile, String fileName) {
        final String mContainerId = containerId;
        final File mSourceFile = sourceFile;
        final String mFileName = fileName;

        new AsyncTask<Void, Void, Void>() {

            private Exception error = null;
            private String finalUri = "";

            @Override
            protected Void doInBackground(Void... params) {

                CloudBlobContainer mContainer = null;

                try {
                    CloudBlobContainer container = serviceClient.getContainerReference(mContainerId);
                    mContainer = container;
                    BlobContainerPermissions blobContainerPermissions = new BlobContainerPermissions();
                    blobContainerPermissions.setPublicAccess(BlobContainerPublicAccessType.CONTAINER);
                    container.uploadPermissions(blobContainerPermissions);

                    CloudBlockBlob blob = container.getBlockBlobReference(mFileName);
                    blob.upload(new FileInputStream(mSourceFile), mSourceFile.length());
                    finalUri = blob.getStorageUri().getPrimaryUri().toString();
                }
                catch (FileNotFoundException fileNotFoundException) {
                    System.out.print("FileNotFoundException encountered: ");
                    System.out.println(fileNotFoundException.getMessage());
                    error = fileNotFoundException;
                }
                catch (StorageException storageException) {
                    System.out.print("StorageException encountered: ");
                    System.out.println(storageException.getMessage());
                    error = storageException;

                    if(storageException.getErrorCode().trim().equalsIgnoreCase("ContainerNotFound")) {
                        try {
                            mContainer.createIfNotExists();
                            BlobContainerPermissions blobContainerPermissions = new BlobContainerPermissions();
                            blobContainerPermissions.setPublicAccess(BlobContainerPublicAccessType.CONTAINER);
                            mContainer.uploadPermissions(blobContainerPermissions);

                            CloudBlockBlob blob = mContainer.getBlockBlobReference(mFileName);
                            blob.upload(new FileInputStream(mSourceFile), mSourceFile.length());
                            finalUri = blob.getStorageUri().getPrimaryUri().toString();
                        } catch (StorageException e) {
                            e.printStackTrace();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }
                    }
                }
                catch (Exception e) {
                    System.out.print("Exception encountered: ");
                    System.out.println(e.getMessage());
                    error = e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void result)
            {
                mCallbackService.setUploadDataStatus(true, finalUri);
            }
        }.execute();
    }

    public void downloadData(String containerId, File destinationDirectory, String destinationFileName) {
        final String mContainerId = containerId;
        final File mDestinationDirectory = destinationDirectory;
        final String mDestinationFileName = destinationFileName;

        new AsyncTask<Void, Void, Void>() {

            private Exception error = null;

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    CloudBlobContainer container = serviceClient.getContainerReference(mContainerId);
                    CloudBlockBlob blob = container.getBlockBlobReference(mDestinationFileName);
//                    if(blob.exists()) {
                        File destinationFile = new File(mDestinationDirectory, mDestinationFileName);
                        blob.downloadToFile(destinationFile.getAbsolutePath());
//                    }
//                    return destinationFile;
                }
                catch (FileNotFoundException fileNotFoundException) {
                    System.out.print("FileNotFoundException encountered: ");
                    System.out.println(fileNotFoundException.getMessage());
                    error = fileNotFoundException;
                }
                catch (StorageException storageException) {
                    System.out.print("StorageException encountered: ");
                    System.out.println(storageException.getMessage());
                    error = storageException;

//                    if(storageException.getErrorCode().trim().equalsIgnoreCase("ContainerNotFound")) mCallbackService.setDownloadDataStatus(true, "");
                }
                catch (Exception e) {
                    System.out.print("Exception encountered: ");
                    System.out.println(e.getMessage());
                    error = e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void result)
            {
                mCallbackService.setDownloadDataStatus(true, "");
            }
        }.execute();
    }

    public void blobExists(String containerId, String name) {
        final String mContainerId = containerId;
        final String mName = name;

        new AsyncTask<Void, Void, Void>() {

            private Exception error = null;
            private Boolean exists = false;

            @Override
            protected Void doInBackground(Void... params) {

                CloudBlobContainer mContainer = null;

                try {
                    CloudBlobContainer container = serviceClient.getContainerReference(mContainerId);
                    mContainer = container;
                    BlobContainerPermissions blobContainerPermissions = new BlobContainerPermissions();
                    blobContainerPermissions.setPublicAccess(BlobContainerPublicAccessType.CONTAINER);
                    container.uploadPermissions(blobContainerPermissions);
                    exists = false;
//                    boolean exists = container.exists();
//                    if(exists) {
                        for(ListBlobItem listBlobItem : container.listBlobs()) {
                            String itemName = listBlobItem.getUri().toString();
                            itemName = itemName.substring(itemName.lastIndexOf("/") + 1, itemName.length());
                            if(mName.equalsIgnoreCase(itemName)) {
                                exists = true;
                                break;
                            }
                        }
//                    }
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    error = e;
                } catch (StorageException se) {
                    se.printStackTrace();
                    error = se;

                    if(se.getErrorCode().trim().equalsIgnoreCase("ContainerNotFound")) {
                        try {
                            mContainer.createIfNotExists();
                            BlobContainerPermissions blobContainerPermissions = new BlobContainerPermissions();
                            blobContainerPermissions.setPublicAccess(BlobContainerPublicAccessType.CONTAINER);
                            mContainer.uploadPermissions(blobContainerPermissions);
                            exists = false;
                        } catch (StorageException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result)
            {
                if(error != null) {
                    mCallbackService.setDataExistsStatus(false, mName);
                }
                else {
                    mCallbackService.setDataExistsStatus(exists, mName);
                }
            }
        }.execute();
    }

    public void containerExists(String containerId) {
        final String mContainerId = containerId;

        new AsyncTask<Void, Void, Void>() {

            private Exception error = null;
            private Boolean exists = false;

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    CloudBlobContainer container = serviceClient.getContainerReference(mContainerId);
                    exists = container.exists();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    error = e;
                } catch (StorageException se) {
                    se.printStackTrace();
                    error = se;

//                    if(se.getErrorCode().trim().equalsIgnoreCase("ContainerNotFound")) {
//                        exists = false;
//                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result)
            {
                if(error != null) {
                    mCallbackService.setDataExistsStatus(exists, "");
                }
                else {
                    mCallbackService.setDataExistsStatus(exists, "");
                }
            }
        }.execute();
    }
}
