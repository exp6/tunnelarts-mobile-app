package cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.prospection;

//import org.jpmml.evaluator.Evaluator;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.presentation.presenters.base.BasePresenter;
import cl.skava.mobile.tunnelapp.presentation.ui.base.BaseView;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public interface ProspectionCalculationsPresenter extends BasePresenter {

    interface View extends BaseView {
        void showExecutionProgress();
        void hideExecutionProgress();
        void displayPositionList(List<ProspectionHole> prospections);
        void displayStickList(List<Rod> rods);
    }

    void executeModel(File file, InputStream modelFileIn, OutputStream modelFileOut, List<InputStream> modelsIn, List<OutputStream> modelsOut, List<File> outputFiles, List<Rod> rods, Float initialQ);
    void executeModel(File[] evaluators, List<Rod> rods, Float initialQ);
    void getRodsByProspection(ProspectionHole prospection);
    void createNewProspection(ProspectionHole prospection);
    void createNewRod(Rod rod);
    void deleteProspection(ProspectionHole prospection);
    void updateProspection(ProspectionHole prospection);
    void deleteRod(Rod rod);
    void updateRod(Rod rod);
}
