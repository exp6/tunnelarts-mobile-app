package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by Jose Ignacio Vera on 08-08-2016.
 */
public class SpecialFeatures extends DataInput {
    private String id;
    private Boolean zeolites;
    private Boolean clay;
    private Boolean chlorite;
    private Boolean redTuff;
    private Boolean sulfides;
    private Boolean sulfates;
    private String idAdditionalInformation;
    private Boolean completed;
    private String idMapping;
    private Boolean none;

    public SpecialFeatures(String id, Boolean zeolites, Boolean clay, Boolean chlorite, Boolean redTuff, Boolean sulfides, Boolean sulfates, String idAdditionalInformation, Boolean completed, String idMapping, Boolean none) {
        this.id = id;
        this.zeolites = zeolites;
        this.clay = clay;
        this.chlorite = chlorite;
        this.redTuff = redTuff;
        this.sulfides = sulfides;
        this.sulfates = sulfates;
        this.idAdditionalInformation = idAdditionalInformation;
        this.completed = completed;
        this.idMapping = idMapping;
        this.none = none;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getZeolites() {
        return zeolites;
    }

    public void setZeolites(Boolean zeolites) {
        this.zeolites = zeolites;
    }

    public Boolean getClay() {
        return clay;
    }

    public void setClay(Boolean clay) {
        this.clay = clay;
    }

    public Boolean getChlorite() {
        return chlorite;
    }

    public void setChlorite(Boolean chlorite) {
        this.chlorite = chlorite;
    }

    public Boolean getRedTuff() {
        return redTuff;
    }

    public void setRedTuff(Boolean redTuff) {
        this.redTuff = redTuff;
    }

    public Boolean getSulfides() {
        return sulfides;
    }

    public void setSulfides(Boolean sulfides) {
        this.sulfides = sulfides;
    }

    public Boolean getSulfates() {
        return sulfates;
    }

    public void setSulfates(Boolean sulfates) {
        this.sulfates = sulfates;
    }

    public String getIdAdditionalInformation() {
        return idAdditionalInformation;
    }

    public void setIdAdditionalInformation(String idAdditionalInformation) {
        this.idAdditionalInformation = idAdditionalInformation;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Boolean getNone() {
        return none;
    }

    public void setNone(Boolean none) {
        this.none = none;
    }

    @Override
    public String toString() {
        return "SpecialFeatures{" +
                "id='" + id + '\'' +
                ", zeolites=" + zeolites +
                ", clay=" + clay +
                ", chlorite=" + chlorite +
                ", redTuff=" + redTuff +
                ", sulfides=" + sulfides +
                ", sulfates=" + sulfates +
                ", idAdditionalInformation='" + idAdditionalInformation + '\'' +
                ", completed=" + completed +
                ", idMapping='" + idMapping + '\'' +
                ", none=" + none +
                '}';
    }
}
