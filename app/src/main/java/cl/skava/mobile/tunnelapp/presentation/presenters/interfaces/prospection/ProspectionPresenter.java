package cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.prospection;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SelectionDetailLists;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroupCalculation;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.presentation.presenters.base.BasePresenter;
import cl.skava.mobile.tunnelapp.presentation.ui.base.BaseView;

/**
 * Created by Jose Ignacio Vera on 02-08-2016.
 */
public interface ProspectionPresenter extends BasePresenter {

    interface View extends BaseView {
        void displayProspectionHoleGroupList(List<ProspectionHoleGroup> prospectionHoleGroups);
        void setQValuesMappings(HashMap<Mapping, QValue> qValuesByMappings);
        void syncDataResult(boolean success, String msg);
        void onSyncProgressPercentaje(Integer percentaje);
        void notifySyncSegment(String msg);
        void onRodsByProspectionHoleRetrieved(HashMap<ProspectionHole, List<Rod>> rodsByProspectionHole);
        void onExportFinish(boolean success);
        void displayProspectionRefPictureList(List<ProspectionRefPicture> prospectionRefPictures);
        void onCalculationsByProspectionHoleGroup(HashMap<ProspectionHoleGroup, List<ProspectionHoleGroupCalculation>> calculationsByGroup);
    }

    void createNewProspectionHoleGroup(ProspectionHoleGroup prospectionHoleGroup);
    void syncData(User user);
    void deleteProspectionHoleGroup(ProspectionHoleGroup prospectionHoleGroup);
    void updateProspectionHoleGroup(ProspectionHoleGroup prospectionHoleGroup);
    void getRodsByProspectionHoleMap(String prospectionHoleGroupId);
    void exportData(Project p, Tunnel t, Face f, String directoryPath);
    void savePicture(ProspectionRefPicture prospectionRefPicture);
    void updateProspectionHoleGroupCalculations(List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations, String prospectionHoleGroupId);
}
