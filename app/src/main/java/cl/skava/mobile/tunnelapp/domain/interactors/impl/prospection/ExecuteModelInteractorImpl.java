package cl.skava.mobile.tunnelapp.domain.interactors.impl.prospection;

import java.util.List;

import cl.skava.mobile.tunnelapp.data.machinelearning.base.MachineLearningService;
import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.base.AbstractInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.prospection.ExecuteModelInteractor;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;

/**
 * Created by Jose Ignacio Vera on 02-08-2016.
 */
public class ExecuteModelInteractorImpl extends AbstractInteractor implements ExecuteModelInteractor {

    private MachineLearningService mMLService;
    private ExecuteModelInteractor.Callback mCallback;

    public ExecuteModelInteractorImpl(Executor threadExecutor,
                                      MainThread mainThread,
                                      ExecuteModelInteractor.Callback callback,
                                      MachineLearningService mLService) {
        super(threadExecutor, mainThread);
        mMLService = mLService;
        mCallback = callback;
    }

    private void notifyExecResult(final List<Rod> finalRods) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onExecResult(finalRods);
            }
        });
    }

    @Override
    public void run() {
        mMLService.executeModel();
        final List<Rod> finalRods = mMLService.getFinalRods();
        notifyExecResult(finalRods);
    }
}
