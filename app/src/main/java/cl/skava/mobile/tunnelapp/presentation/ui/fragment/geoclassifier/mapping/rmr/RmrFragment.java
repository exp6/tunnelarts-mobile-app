package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rmr;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.Rmr;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputSelection;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class RmrFragment extends Fragment
        implements StrengthFragment.OnStrengthValueListener,
        RqdFragment.OnRqdValueListener,
        SpacingFragment.OnSpacingValueListener,
        GroundwaterFragment.OnGroundwaterValueListener,
        OrientationDiscontinuitiesFragment.OnOrientationDValueListener,
        GuidelinesConditionsFragment.OnConditionDiscontinuitiesValueListener,
        ConditionDiscontinuitiesFragment.OnConditionDiscontinuitiesValueListener {

    private static final String TAG = "RmrFragment";

    private TabLayout tabLayout;
    private Toolbar toolbar;
    private Toolbar toolbarParent;

    private Double strengthValue;
    private Double rqdValue;
    private Double spacingValue;
    private Double persistencValue;
    private Double openingValue;
    private Double roughnessValue;
    private Double infillingValue;
    private Double weatheringValue;

    private Double groundwaterValue;
    private Double orientationDValue;
    private Double conditionValue;

    private Double rmrValue;

    private List<RockQuality> qualities = new ArrayList<>();

    private String rockQuality = "";
    private String rockCategory = "";

    private Rmr mappingInput;

    private Boolean mappingFinished;

    private List<EvaluationMethodSelectionType> rmrSelectionMap;
    private RmrInputSelection rmrInputSelection;

    private Fragment currentFragment;

    private TabLayout tabLayoutConditions;

    private boolean conditionEnabled = false;

    private final String lang = Locale.getDefault().getLanguage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_rmr, container, false);
        rootView.setTag(TAG);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.characterization_mapping_input_rmr_label_strength)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.characterization_mapping_input_rmr_label_rqd)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.characterization_mapping_input_rmr_label_disc)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.characterization_mapping_input_rmr_label_condition_disc)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.characterization_mapping_input_rmr_label_groundwater)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.characterization_mapping_input_rmr_label_orientation_dd)));

        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        updateRmrValue();

        tabLayoutConditions = (TabLayout) rootView.findViewById(R.id.tabs1);
        tabLayoutConditions.addTab(tabLayoutConditions.newTab().setText(getResources().getString(R.string.characterization_mapping_input_rmr_label_condition_disc)));
        tabLayoutConditions.addTab(tabLayoutConditions.newTab().setText(getResources().getString(R.string.characterization_mapping_input_rmr_label_guidelines)));
        tabLayoutConditions.setVisibility(View.GONE);

        StrengthFragment f1 = new StrengthFragment();
        f1.onAttachFragment(this);
        EvaluationMethodSelectionType emst = getSelectionMap("Strength");
        f1.setSelectionMap(emst);
        f1.setMappingFinished(mappingFinished);
        currentFragment = f1;
        updateFragment(f1, R.id.rmr_input_content);

        final Fragment parentFragment = this;

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                saveCurrentSelection(currentFragment);
                EvaluationMethodSelectionType emst;

                switch (tab.getPosition()) {
                    case 1:
                        tabLayoutConditions.setVisibility(View.GONE);
                        emst = getSelectionMap("Rqd");
                        RqdFragment f1 = new RqdFragment();
                        f1.onAttachFragment(parentFragment);
                        f1.setSelectionMap(emst);
                        f1.setMappingFinished(mappingFinished);
                        currentFragment = f1;
                        break;
                    case 2:
                        tabLayoutConditions.setVisibility(View.GONE);
                        emst = getSelectionMap("Spacing");
                        SpacingFragment f2 = new SpacingFragment();
                        f2.onAttachFragment(parentFragment);
                        f2.setSelectionMap(emst);
                        f2.setMappingFinished(mappingFinished);
                        currentFragment = f2;
                        break;
                    case 3:
                        tabLayoutConditions.setVisibility(View.VISIBLE);

                        TabLayout.Tab tab1;

                        if(!conditionEnabled) {
                            emst = getSelectionMap("ConditionDiscontinuities");
                            ConditionDiscontinuitiesFragment f9 = new ConditionDiscontinuitiesFragment();
                            f9.onAttachFragment(parentFragment);
                            f9.setSelectionMap(emst);
                            f9.setMappingFinished(mappingFinished);
                            currentFragment = f9;
                            tab1 = tabLayoutConditions.getTabAt(0);
                        }
                        else {
                            List<EvaluationMethodSelectionType> emsts = new ArrayList<>();
                            emsts.add(getSelectionMap("Persistence"));
                            emsts.add(getSelectionMap("Opening"));
                            emsts.add(getSelectionMap("Roughness"));
                            emsts.add(getSelectionMap("Infilling"));
                            emsts.add(getSelectionMap("Weathering"));

                            GuidelinesConditionsFragment f9 = new GuidelinesConditionsFragment();
                            f9.onAttachFragment(parentFragment);
                            f9.setRmrSelectionMaps(emsts);
                            f9.setMappingFinished(mappingFinished);
                            currentFragment = f9;
                            tab1 = tabLayoutConditions.getTabAt(1);
                        }

                        tab1.select();

                        tabLayoutConditions.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {
                                saveCurrentSelection(currentFragment);

                                EvaluationMethodSelectionType emst;

                                switch(tab.getPosition()) {
                                    case 1:
                                        List<EvaluationMethodSelectionType> emsts = new ArrayList<>();
                                        emsts.add(getSelectionMap("Persistence"));
                                        emsts.add(getSelectionMap("Opening"));
                                        emsts.add(getSelectionMap("Roughness"));
                                        emsts.add(getSelectionMap("Infilling"));
                                        emsts.add(getSelectionMap("Weathering"));

                                        GuidelinesConditionsFragment f3 = new GuidelinesConditionsFragment();
                                        f3.onAttachFragment(parentFragment);
                                        f3.setRmrSelectionMaps(emsts);
                                        f3.setMappingFinished(mappingFinished);
                                        currentFragment = f3;
                                        conditionEnabled = true;
                                        break;
                                    default:
                                        emst = getSelectionMap("ConditionDiscontinuities");
                                        ConditionDiscontinuitiesFragment f1 = new ConditionDiscontinuitiesFragment();
                                        f1.onAttachFragment(parentFragment);
                                        f1.setSelectionMap(emst);
                                        f1.setMappingFinished(mappingFinished);
                                        currentFragment = f1;
                                        conditionEnabled = false;
                                        break;
                                }

                                updateRmrValue();
                                updateFragment(currentFragment, R.id.rmr_input_content);
                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {
                            }

                            @Override
                            public void onTabReselected(TabLayout.Tab tab) {
                            }
                        });
//                        currentFragment = f9;
                        break;
                    case 4:
                        tabLayoutConditions.setVisibility(View.GONE);
                        emst = getSelectionMap("Groundwater");
                        GroundwaterFragment f4 = new GroundwaterFragment();
                        f4.onAttachFragment(parentFragment);
                        f4.setSelectionMap(emst);
                        f4.setMappingFinished(mappingFinished);
                        currentFragment = f4;
                        break;
                    case 5:
                        tabLayoutConditions.setVisibility(View.GONE);
                        emst = getSelectionMap("OrientationDiscontinuities");
                        OrientationDiscontinuitiesFragment f5 = new OrientationDiscontinuitiesFragment();
                        f5.onAttachFragment(parentFragment);
                        f5.setSelectionMap(emst);
                        f5.setMappingFinished(mappingFinished);
                        currentFragment = f5;
                        break;
                    default:
                        tabLayoutConditions.setVisibility(View.GONE);
                        emst = getSelectionMap("Strength");
                        StrengthFragment f8 = new StrengthFragment();
                        f8.onAttachFragment(parentFragment);
                        f8.setSelectionMap(emst);
                        f8.setMappingFinished(mappingFinished);
                        currentFragment = f8;
                        break;
                }

                updateFragment(currentFragment, R.id.rmr_input_content);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        toolbarParent.inflateMenu(R.menu.menu_geoclassifier_mapping_input_nav);
        toolbarParent.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                TabLayout.Tab tab = tabLayout.getTabAt(0);
                switch (item.getItemId()) {
                    case R.id.action_next_mapping_input:
                        int prevPosition = tabLayout.getSelectedTabPosition();
                        tab = (prevPosition == tabLayout.getTabCount() - 1) ? tabLayout.getTabAt(0) : tabLayout.getTabAt(++prevPosition);
                        break;

                    case R.id.action_previous_mapping_input:
                        int nextPosition = tabLayout.getSelectedTabPosition();
                        tab = (nextPosition > 0) ? tabLayout.getTabAt(--nextPosition) : tabLayout.getTabAt(tabLayout.getTabCount() - 1);
                        break;
                }
                tab.select();
                return false;
            }
        });

        return rootView;
    }

    private void saveCurrentSelection(Fragment f) {
        EvaluationMethodSelectionType map;
        int position = 0;

        if(f instanceof RqdFragment) {
            Log.i(TAG, "RqdFragment");
            RqdFragment fx = (RqdFragment) f;
            map = fx.getSelectionMap();
            position = getSelectionMapPosition(map);
            rmrSelectionMap.set(position, map);
        }

        if(f instanceof SpacingFragment) {
            Log.i(TAG, "SpacingFragment");
            SpacingFragment fx = (SpacingFragment) f;
            map = fx.getSelectionMap();
            position = getSelectionMapPosition(map);
            rmrSelectionMap.set(position, map);
        }

        if(f instanceof GuidelinesConditionsFragment) {
            Log.i(TAG, "GuidelinesConditionsFragment");
            GuidelinesConditionsFragment fx = (GuidelinesConditionsFragment) f;
            List<EvaluationMethodSelectionType> maps = fx.getRmrSelectionMaps();
            for(EvaluationMethodSelectionType map1 : maps) {
                position = getSelectionMapPosition(map1);
                rmrSelectionMap.set(position, map1);
            }
        }

        if(f instanceof ConditionDiscontinuitiesFragment) {
            Log.i(TAG, "ConditionDiscontinuitiesFragment");
            ConditionDiscontinuitiesFragment fx = (ConditionDiscontinuitiesFragment) f;
            map = fx.getSelectionMap();
            position = getSelectionMapPosition(map);
            rmrSelectionMap.set(position, map);
        }

        if(f instanceof GroundwaterFragment) {
            Log.i(TAG, "GroundwaterFragment");
            GroundwaterFragment fx = (GroundwaterFragment) f;
            map = fx.getSelectionMap();
            position = getSelectionMapPosition(map);
            rmrSelectionMap.set(position, map);
        }

        if(f instanceof OrientationDiscontinuitiesFragment) {
            Log.i(TAG, "OrientationDiscontinuitiesFragment");
            OrientationDiscontinuitiesFragment fx = (OrientationDiscontinuitiesFragment) f;
            map = fx.getSelectionMap();
            position = getSelectionMapPosition(map);
            rmrSelectionMap.set(position, map);
        }

        if(f instanceof StrengthFragment) {
            Log.i(TAG, "StrengthFragment");
            StrengthFragment fx = (StrengthFragment) f;
            map = fx.getSelectionMap();
            position = getSelectionMapPosition(map);
            rmrSelectionMap.set(position, map);
        }
    }

    private EvaluationMethodSelectionType getSelectionMap(Integer code) {
        for(EvaluationMethodSelectionType e : rmrSelectionMap) {
            if(e.getCode().intValue() == code.intValue()) {
                return e;
            }
        }
        return null;
    }

    private EvaluationMethodSelectionType getSelectionMap(String name) {
        for(EvaluationMethodSelectionType e : rmrSelectionMap) {
            if(e.getDescription().trim().equalsIgnoreCase(name.trim())) {
                return e;
            }
        }
        return null;
    }

    private int getSelectionMapPosition(EvaluationMethodSelectionType map) {
        for(int i = 0; i < rmrSelectionMap.size(); i++) {
            EvaluationMethodSelectionType e = rmrSelectionMap.get(i);
            if(e.getCode().intValue() == map.getCode().intValue()) {
                return i;
            }
        }
        return 0;
    }

    public void setToolbarParent(Toolbar toolbarParent) {
        this.toolbarParent = toolbarParent;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void updateFragment(Fragment fragment, int container) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(container, fragment);
        transaction.commitAllowingStateLoss();
    }

    private void updateRmrValue() {
        mappingInput.setStrength(new Float(strengthValue));
        mappingInput.setRqd(new Float(rqdValue));
        mappingInput.setSpacing(new Float(spacingValue));
        mappingInput.setPersistence(new Float(persistencValue));
        mappingInput.setOpening(new Float(openingValue));
        mappingInput.setRoughness(new Float(roughnessValue));
        mappingInput.setInfilling(new Float(infillingValue));
        mappingInput.setWeathering(new Float(weatheringValue));
        mappingInput.setGroundwater(new Float(groundwaterValue));
        mappingInput.setOrientation(new Float(orientationDValue));
        mappingInput.setCondition(new Float(conditionValue));
        mappingInput.setConditionEnabled(conditionEnabled);

        double conditionDiscontinuityValue = !conditionEnabled ? conditionValue : (persistencValue + openingValue + roughnessValue + infillingValue + weatheringValue);

        rmrValue = (strengthValue + rqdValue + spacingValue + conditionDiscontinuityValue + groundwaterValue + orientationDValue);

        RockQuality rockQualityObj = qualities.get(0);

        for(RockQuality rq : qualities) {
            if(qualityMatched(rq, rmrValue)) {
                rockQualityObj = rq;
                break;
            }
        }

        mappingInput.setRockQualityCode(rockQualityObj.getCode());

        toolbar.setTitle((lang.equals("es") ? "Valor RMR" : "RMR Value")+" = " + rmrValue);
        toolbar.setSubtitle(rockQualityObj == null ? "" : (lang.equals("es") ? "Calidad Roca" : "Rock Quality")+": " + rockQualityObj.getName() + " - "+(lang.equals("es") ? "Categoría" : "Category")+": " + rockQualityObj.getClassification());
    }

    @Override
    public void OnGroundwaterSetValue(Double value, String id) {
        groundwaterValue = value;
        rmrInputSelection.setGroundwater(id);
        updateRmrValue();
    }

    @Override
    public void OnOrientationDSetValue(Double value, String id) {
        orientationDValue = value;
        rmrInputSelection.setOrientationDD(id);
        updateRmrValue();
    }

    @Override
    public void OnRqdSetValue(Double value, String id) {
        rqdValue = value;
        rmrInputSelection.setRqd(id);
        updateRmrValue();
    }

    @Override
    public void OnSpacingSetValue(Double value, String id) {
        spacingValue = value;
        rmrInputSelection.setSpacing(id);
        updateRmrValue();
    }

    @Override
    public void OnStrengthSetValue(Double value, String id) {
        strengthValue = value;
        rmrInputSelection.setStrength(id);
        updateRmrValue();
    }

    private boolean qualityMatched(RockQuality rq, Double qvalue) {
        return (rq.getLowerBound() <= qvalue && qvalue < rq.getUpperBound());
    }

    public void setMappingInput(Rmr mappingInput) {
        this.mappingInput = mappingInput;
    }

    public Rmr getMappingInput() {
        return mappingInput;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public RmrInputSelection getRmrInputSelection() {
        return rmrInputSelection;
    }

    public void setRmrInputSelection(RmrInputSelection rmrInputSelection) {
        this.rmrInputSelection = rmrInputSelection;
    }

    public void setRmrSelectionMap(List<EvaluationMethodSelectionType> rmrSelectionMap) {
        this.rmrSelectionMap = rmrSelectionMap;
    }

    public void initData() {

        strengthValue = (double) mappingInput.getStrength();
        rqdValue = (double) mappingInput.getRqd();
        spacingValue = (double) mappingInput.getSpacing();
        persistencValue = (double) mappingInput.getPersistence();
        openingValue = (double) mappingInput.getOpening();
        roughnessValue = (double) mappingInput.getRoughness();
        infillingValue = (double) mappingInput.getInfilling();
        weatheringValue = (double) mappingInput.getWeathering();
        groundwaterValue = (double) mappingInput.getGroundwater();
        orientationDValue = (double) mappingInput.getOrientation();
        conditionValue = (double) mappingInput.getCondition();

        conditionEnabled = mappingInput.getConditionEnabled();

        RockQuality rq = new RockQuality();
        rq.setName("Poor rock");
        rq.setClassification("Class IV");
        rq.setUpperBound(new Double(40));
        rq.setLowerBound(new Double(21));
        rq.setCode(1);
        qualities.add(rq);

        rq = new RockQuality();
        rq.setName("Fair rock");
        rq.setClassification("Class III");
        rq.setUpperBound(new Double(60));
        rq.setLowerBound(new Double(41));
        rq.setCode(2);
        qualities.add(rq);

        rq = new RockQuality();
        rq.setName("Very poor rock");
        rq.setClassification("Class V");
        rq.setUpperBound(new Double(20));
        rq.setLowerBound(new Double(0));
        rq.setCode(0);
        qualities.add(rq);

        rq = new RockQuality();
        rq.setName("Very good rock");
        rq.setClassification("Class I");
        rq.setUpperBound(new Double(100));
        rq.setLowerBound(new Double(81));
        rq.setCode(4);
        qualities.add(rq);

        rq = new RockQuality();
        rq.setName("Good rock");
        rq.setClassification("Class II");
        rq.setUpperBound(new Double(80));
        rq.setLowerBound(new Double(61));
        rq.setCode(3);
        qualities.add(rq);
    }

    @Override
    public void OnConditionDiscontinuitiesSetValue(int position, Double value, String id) {
        switch (position) {
            case 1:
                Log.i(TAG, "Aperture");
                openingValue = value;
                rmrInputSelection.setOpening(id);
                break;
            case 2:
                Log.i(TAG, "Roughness");
                roughnessValue = value;
                rmrInputSelection.setRoughness(id);
                break;
            case 3:
                Log.i(TAG, "Infilling");
                infillingValue = value;
                rmrInputSelection.setInfilling(id);
                break;
            case 4:
                Log.i(TAG, "Weathering");
                weatheringValue = value;
                rmrInputSelection.setWeathering(id);
                break;
            default:
                Log.i(TAG, "Persistence");
                persistencValue = value;
                rmrInputSelection.setPersistence(id);
                break;
        }

        updateRmrValue();
    }

    @Override
    public void OnConditionDiscontinuitiesSetValue(Double value, String id) {
        conditionValue = value;
        rmrInputSelection.setConditionDD(id);
        updateRmrValue();
    }

    class RockQuality {
        String name;
        Double lowerBound;
        Double upperBound;
        String classification;
        Integer code;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Double getLowerBound() {
            return lowerBound;
        }

        public void setLowerBound(Double lowerBound) {
            this.lowerBound = lowerBound;
        }

        public Double getUpperBound() {
            return upperBound;
        }

        public void setUpperBound(Double upperBound) {
            this.upperBound = upperBound;
        }

        public String getClassification() {
            return classification;
        }

        public void setClassification(String classification) {
            this.classification = classification;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }
    }
}
