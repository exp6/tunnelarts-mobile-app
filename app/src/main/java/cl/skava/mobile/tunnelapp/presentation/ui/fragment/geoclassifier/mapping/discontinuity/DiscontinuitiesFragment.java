package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.discontinuity;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Discontinuity;
import cl.skava.mobile.tunnelapp.presentation.views.geoclassifier.mapping.summary.StereonetView;

/**
 * Created by Jose Ignacio Vera on 10-11-2016.
 */
public class DiscontinuitiesFragment extends Fragment {

    private static final String TAG = "DiscontinuitiesFragment";
    private List<Discontinuity> discontinuities;
    private List<Discontinuity> deletedDiscontinuities;
    private DiscontinuityFragment currentFragment;
    private boolean mappingFinished;
    private String mappingId;
    private View rootView;

    public DiscontinuitiesFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setArguments(Bundle args) {
        Discontinuity[] discontinuitiesParcel = (Discontinuity[])args.getParcelableArray("discontinuities");
        discontinuities = new ArrayList<>(Arrays.asList(discontinuitiesParcel));

        Discontinuity[] deletedDiscontinuitiesParcel = (Discontinuity[])args.getParcelableArray("deletedDiscontinuities");
        deletedDiscontinuities = new ArrayList<>(Arrays.asList(deletedDiscontinuitiesParcel));

        mappingFinished = args.getInt("mappingFinished") != 0;
        mappingId = args.getString("mappingId");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setArguments(getArguments());
        rootView = createRootView(inflater, container);
        TabLayout tabLayout = getTabLayout(rootView);
        fillTabs(tabLayout);
        tabLayout.setOnTabSelectedListener(createSelectListener());
        selectFirstTab(tabLayout);
        return rootView;
    }

    private View createRootView(LayoutInflater inflater, ViewGroup container) {
        View rootView = inflateView(inflater, container);
        rootView.setTag(TAG);
        return rootView;
    }

    private View inflateView(LayoutInflater inflater, ViewGroup container) {
        int mappingDiscontinuitiesId = R.layout.fragment_geoclassifier_mapping_discontinuities;
        return inflater.inflate(mappingDiscontinuitiesId, container, false);
    }

    private TabLayout getTabLayout(View container) {
        int tabsId = R.id.tabs;
        return (TabLayout)container.findViewById(tabsId);
    }

    private void fillTabs(TabLayout tabLayout) {
        for (Discontinuity discontinuity : discontinuities) {
            createNewTab(tabLayout, discontinuity);
        }
        addExtraTab(tabLayout);
    }

    private void createNewTab(TabLayout tabLayout, Discontinuity discontinuity) {
        String tabName = getDiscontinuityTabName(discontinuity.getPosition() + 1);
        createNamedTab(tabLayout, tabName);
    }

    private String getDiscontinuityTabName(int position) {
        int stringId = R.string.characterization_mapping_input_disc_label_discontinuity;
        String discontinuity = getResources().getString(stringId);
        return discontinuity + " " + position;
    }

    private void createNamedTab(TabLayout tabLayout, String name) {
        TabLayout.Tab newTab = createUnattachedNamedTab(tabLayout, name);
        tabLayout.addTab(newTab);
    }

    private TabLayout.Tab createUnattachedNamedTab(TabLayout tabLayout, String name) {
        TabLayout.Tab newTab = tabLayout.newTab();
        newTab.setText(name);
        return newTab;
    }

    private void addExtraTab(TabLayout tabLayout) {
        TabLayout.Tab newTab = tabLayout.newTab();
        newTab.setCustomView(createExtraButton(tabLayout));
        tabLayout.addTab(newTab);
    }

    private Button createExtraButton(TabLayout tabLayout) {
        Button button = new Button(getContext());
        button.setText("+");
        button.setOnClickListener(clickNewDiscontinuityButton(tabLayout));
        return button;
    }

    private Button.OnClickListener clickNewDiscontinuityButton(final TabLayout tabLayout) {
        return new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                addNewDiscontinuityTab(tabLayout);
            }
        };
    }

    private void addNewDiscontinuityTab(TabLayout tabLayout) {
        int nextTabPosition = tabLayout.getTabCount();
        String tabName = getDiscontinuityTabName(nextTabPosition);
        createNamedTabAt(tabLayout, tabName, nextTabPosition - 1);
        Discontinuity discontinuity = createNewDiscontinuity(nextTabPosition - 1);
        discontinuities.add(discontinuity);
        setFirstTabIfThereIsOnlyOne(tabLayout);
    }

    private void createNamedTabAt(TabLayout tabLayout, String name, int position) {
        TabLayout.Tab newTab = createUnattachedNamedTab(tabLayout, name);
        tabLayout.addTab(newTab, position);
    }

    private Discontinuity createNewDiscontinuity(int position) {
        return new Discontinuity("", 0, 0, new Float(0), new Float(0), 0, 0, 0, 0, 0, 0, 0, mappingId, false, position, 0);
    }

    private void setFirstTabIfThereIsOnlyOne(TabLayout tabLayout) {
        if (tabLayout.getTabCount() == 2) {
            selectFirstTab(tabLayout);
        }
    }

    private TabLayout.OnTabSelectedListener createSelectListener() {
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectTab(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                saveDiscontinuity(tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        };
    }

    private void selectTab(TabLayout.Tab tab) {
        int position = tab.getPosition();
        selectOrClearFragment(position);
        tab.select();
    }

    private void selectOrClearFragment(int position) {
        if (discontinuities.size() > 0) {
            Discontinuity discontinuity = discontinuities.get(position);
            updateDiscontinuityFragment(discontinuity);
        } else {
            clearDiscontinuityFragment();
        }
    }

    private void updateDiscontinuityFragment(Discontinuity discontinuity) {
        FragmentTransaction fragmentTransaction = beginTransaction();
        DiscontinuityFragment discontinuityFragment = createDiscontinuityFragment(discontinuity);
        int containerId = R.id.disc_input_content;
        replaceAndCommit(fragmentTransaction, containerId, discontinuityFragment);
        currentFragment = discontinuityFragment;
    }

    private FragmentTransaction beginTransaction() {
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        return fragmentTransaction;
    }

    private void replaceAndCommit(FragmentTransaction fragmentTransaction, int containerId, DiscontinuityFragment fragment) {
        fragmentTransaction.replace(containerId, fragment);
        fragmentTransaction.commit();
    }

    private DiscontinuityFragment createDiscontinuityFragment(Discontinuity discontinuity) {
        DiscontinuityFragment discontinuityFragment = new DiscontinuityFragment();
        discontinuityFragment.setMappingInput(discontinuity);
        discontinuityFragment.setMappingFinished(mappingFinished);
        return discontinuityFragment;
    }

    private void clearDiscontinuityFragment() {
        if (currentFragment == null) {
            return;
        }
        FragmentTransaction fragmentTransaction = beginTransaction();
        removeAndCommit(fragmentTransaction, currentFragment);
        currentFragment = null;
    }

    private void removeAndCommit(FragmentTransaction fragmentTransaction, DiscontinuityFragment fragment) {
        fragmentTransaction.remove(fragment);
        fragmentTransaction.commit();
    }

    private void selectFirstTab(TabLayout tabLayout) {
        try {
            selectTabAt(tabLayout, 0);
        } catch (IndexOutOfBoundsException e) {

        }
    }

    private void selectTabAt(TabLayout tabLayout, int position) {
        TabLayout.Tab firstTab = tabLayout.getTabAt(position);
        selectTab(firstTab);
    }

    private void saveDiscontinuity(TabLayout.Tab tab) {
        try {
            doSaveDiscontinuity(tab);
        }
        catch (IndexOutOfBoundsException e) {
            return;
        }

    }

    private void doSaveDiscontinuity(TabLayout.Tab tab) {
        int position = tab.getPosition();
        Discontinuity discontinuity = discontinuities.get(position);
        saveFragmentIntoDiscontinuity(currentFragment, discontinuity);
    }

    private void saveFragmentIntoDiscontinuity(DiscontinuityFragment fragment, Discontinuity discontinuity) {
        Spinner spinnerType = (Spinner) fragment.getView().findViewById(R.id.spinner_type);
        Spinner spinnerRelevance = (Spinner) fragment.getView().findViewById(R.id.spinner_relevance);
        EditText inputOrientationDD = ((EditText) fragment.getView().findViewById(R.id.input_orientation_dd));
        EditText inputOrientationD = ((EditText) fragment.getView().findViewById(R.id.input_orientation_d));
        Spinner spinnerSpacing = (Spinner) fragment.getView().findViewById(R.id.spinner_spacing);
        Spinner spinnerPersistence = (Spinner) fragment.getView().findViewById(R.id.spinner_persistence);
        Spinner spinnerOpening = (Spinner) fragment.getView().findViewById(R.id.spinner_opening);
        Spinner spinnerRoughness = (Spinner) fragment.getView().findViewById(R.id.spinner_roughness);
        Spinner spinnerInfilling = (Spinner) fragment.getView().findViewById(R.id.spinner_infilling);
        Spinner spinnerInfillingType = (Spinner) fragment.getView().findViewById(R.id.spinner_infilling_type);
        Spinner spinnerWeathering = (Spinner) fragment.getView().findViewById(R.id.spinner_weathering);
        Spinner spinnerSlickensided = (Spinner) fragment.getView().findViewById(R.id.spinner_slickensided);

        discontinuity.setType(spinnerType.getSelectedItemPosition());
        discontinuity.setRelevance(spinnerRelevance.getSelectedItemPosition());
        discontinuity.setOrientationDd(Float.parseFloat(inputOrientationDD.getText().toString()));
        discontinuity.setOrientationD(Float.parseFloat(inputOrientationD.getText().toString()));
        discontinuity.setSpacing(spinnerSpacing.getSelectedItemPosition());
        discontinuity.setPersistence(spinnerPersistence.getSelectedItemPosition());
        discontinuity.setOpening(spinnerOpening.getSelectedItemPosition());
        discontinuity.setRoughness(spinnerRoughness.getSelectedItemPosition());
        discontinuity.setInfilling(spinnerInfilling.getSelectedItemPosition());
        discontinuity.setInfillingType(spinnerInfillingType.getSelectedItemPosition());
        discontinuity.setWeathering(spinnerWeathering.getSelectedItemPosition());
        discontinuity.setSlickensided(spinnerSlickensided.getSelectedItemPosition());
        discontinuity.setCompleted(!inputOrientationDD.getText().toString().trim().equals("")
                && !inputOrientationD.getText().toString().trim().equals("")
                && spinnerType.getSelectedItemPosition() != 0
                && spinnerRelevance.getSelectedItemPosition() != 0
                && spinnerSpacing.getSelectedItemPosition() != 0
                && spinnerPersistence.getSelectedItemPosition() != 0
                && spinnerOpening.getSelectedItemPosition() != 0
                && spinnerRoughness.getSelectedItemPosition() != 0
                && spinnerInfilling.getSelectedItemPosition() != 0
                && spinnerInfillingType.getSelectedItemPosition() != 0
                && spinnerWeathering.getSelectedItemPosition() != 0
                && spinnerSlickensided.getSelectedItemPosition() != 0);
    }

    public List<Discontinuity> getDiscontinuities() {
        return discontinuities;
    }

    public void deleteDiscontinuity(int position) {
        deleteRelevantObjectsAt(position);
    }

    private void deleteRelevantObjectsAt(int position) {
        deleteDiscontinuityDataAt(position);
        deleteTab(position);
        if (discontinuities.size() <= 0) {
            clearDiscontinuityFragment();
        }
    }

    private void deleteDiscontinuityDataAt(int position) {
        Discontinuity removedDiscontinuity = discontinuities.remove(position);
        deletedDiscontinuities.add(removedDiscontinuity);
        renumberDiscontinuities();
    }

    private void renumberDiscontinuities() {
        for (int i = 0; i < discontinuities.size(); i++) {
            Discontinuity discontinuity = discontinuities.get(i);
            discontinuity.setPosition(i);
        }
    }

    private void deleteTab(int position) {
        TabLayout tabLayout = getTabLayout(rootView);
        tabLayout.removeTabAt(position);
        renumberAllTabs(tabLayout);
    }

    private void renumberAllTabs(TabLayout tabLayout) {
        for (int i = 0; i < discontinuities.size(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setText(getDiscontinuityTabName(i + 1));
        }
    }

    public void saveCurrentTab() {
        TabLayout tabLayout = getTabLayout(rootView);
        int currentPosition = tabLayout.getSelectedTabPosition();
        TabLayout.Tab currentTab = tabLayout.getTabAt(currentPosition);
        saveDiscontinuity(currentTab);
    }

    public List<Discontinuity> getDeletedDiscontinuities() {
        return deletedDiscontinuities;
    }

    public void saveStereonet() {
        StereonetView stereonetView = new StereonetView(getContext(), discontinuities);
        stereonetView.setDrawingCacheEnabled(true);

        stereonetView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        stereonetView.layout(0, 0, stereonetView.getMeasuredWidth(), stereonetView.getMeasuredHeight());
        stereonetView.setDrawingCacheBackgroundColor(Color.WHITE);

        stereonetView.buildDrawingCache(true);

        Bitmap b = stereonetView.getDrawingCache();

        File mediaDirectory = new File(getActivity().getExternalFilesDir(null).getPath() + File.separator + "media");

        if (!mediaDirectory.exists()) {
            try{
                mediaDirectory.mkdir();
            }
            catch(SecurityException se){
                se.printStackTrace();
            }
        }

        File newFile = new File(mediaDirectory.getPath() + File.separator + mappingId.toLowerCase() + File.separator + "stereonet.jpg");

        if (!newFile.getParentFile().exists()) {
            try{
                newFile.getParentFile().mkdir();
            }
            catch(SecurityException se){
                se.printStackTrace();
            }
        }

        try {
            FileOutputStream ostream = new FileOutputStream(newFile);
            b.compress(Bitmap.CompressFormat.JPEG, 95, ostream);
            ostream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
