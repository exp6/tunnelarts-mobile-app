package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.additionalinfo;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.FailureZone;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Particularities;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMassHazard;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SpecialFeatures;

/**
 * Created by Jose Ignacio Vera on 10-11-2016.
 */
public class AdditionalInfoMainFragment extends Fragment {

    private static final String TAG = "DiscontinuitiesFragment";
    private TabLayout tabLayout;
    private Boolean mappingFinished;
    private int NUM_INPUTS;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private int listPosition;
    private Boolean firstTimeLoadSlide = true;
    private String overbreakPicturePath;

    private List<Fragment> inputFragments;

    private SpecialFeatures specialFeatures;
    private RockMassHazard rockMassHazard;

    private FailureZone failureZone;

    private Particularities particularities;

    private Fragment currentTabFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_additionalinfo_main, container, false);
        rootView.setTag(TAG);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        NUM_INPUTS = 3;

        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.characterization_mapping_input_ainfo_label_additional_info)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.characterization_mapping_input_ainfo_label_fault_zone)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.characterization_mapping_input_ainfo_label_particularities)));

        inputFragments = new ArrayList<>();
        AdditionalInfoFragment additionalInfoFragment = new AdditionalInfoFragment();
        additionalInfoFragment.setMappingInput1(specialFeatures);
        additionalInfoFragment.setMappingInput2(rockMassHazard);
        additionalInfoFragment.setMappingFinished(mappingFinished);
        inputFragments.add(additionalInfoFragment);

        FailureZoneFragment failureZoneFragment = new FailureZoneFragment();
        failureZoneFragment.setMappingInput(failureZone);
        failureZoneFragment.setMappingFinished(mappingFinished);
        inputFragments.add(failureZoneFragment);

        ParticularitiesFragment particularitiesFragment = new ParticularitiesFragment();
        particularitiesFragment.setMappingInput(particularities);
        particularitiesFragment.setMappingFinished(mappingFinished);
        particularitiesFragment.setOverbreakPicturePath(overbreakPicturePath);
        inputFragments.add(particularitiesFragment);

        mPager = (ViewPager) rootView.findViewById(R.id.additional_input_content);
        mPagerAdapter = new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager(), inputFragments);
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        TabLayout.Tab tab = tabLayout.getTabAt(position);
                        tab.select();

                        if (!firstTimeLoadSlide) {
                            if (currentTabFragment.getView() != null)
                                saveData();
                        } else {
                            firstTimeLoadSlide = false;
                        }
                        listPosition = position;
                        currentTabFragment = ((ScreenSlidePagerAdapter) mPager.getAdapter()).getInputFragments(position);
                    }
                });

        mPager.setCurrentItem(0);
        currentTabFragment = ((ScreenSlidePagerAdapter) mPager.getAdapter()).getInputFragments(mPager.getCurrentItem());

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        return rootView;
    }

    private void saveData() {
        Fragment f = currentTabFragment;

        if(f instanceof AdditionalInfoFragment) {
            AdditionalInfoFragment aif = (AdditionalInfoFragment) f;

            CheckBox chkZeolites = (CheckBox) aif.getView().findViewById(R.id.chk_zeolites);
            CheckBox chkClay = (CheckBox) aif.getView().findViewById(R.id.chk_clay);
            CheckBox chkChlorite = (CheckBox) aif.getView().findViewById(R.id.chk_chlorite);
            CheckBox chkSulfides = (CheckBox) aif.getView().findViewById(R.id.chk_sulfides);
            CheckBox chkSulfates = ((CheckBox) aif.getView().findViewById(R.id.chk_sulfates));

            CheckBox chkRockBurst = (CheckBox) aif.getView().findViewById(R.id.chk_rock_burst);
            CheckBox chkSwelling = ((CheckBox) aif.getView().findViewById(R.id.chk_swelling));
            CheckBox chkSqueezing = (CheckBox) aif.getView().findViewById(R.id.chk_squeezing);
            CheckBox chkSlaking = ((CheckBox) aif.getView().findViewById(R.id.chk_slaking));

            specialFeatures.setZeolites(chkZeolites.isChecked());
            specialFeatures.setClay(chkClay.isChecked());
            specialFeatures.setChlorite(chkChlorite.isChecked());
            specialFeatures.setSulfides(chkSulfides.isChecked());
            specialFeatures.setSulfates(chkSulfates.isChecked());
            specialFeatures.setCompleted(
                    specialFeatures.getZeolites()
                            || specialFeatures.getClay()
                            || specialFeatures.getChlorite()
                            || specialFeatures.getRedTuff()
                            || specialFeatures.getSulfides()
                            || specialFeatures.getSulfates());

            rockMassHazard.setRockBurst(chkRockBurst.isChecked());
            rockMassHazard.setSwelling(chkSwelling.isChecked());
            rockMassHazard.setSqueezing(chkSqueezing.isChecked());
            rockMassHazard.setSlaking(chkSlaking.isChecked());
            rockMassHazard.setCompleted(rockMassHazard.getRockBurst()
                    || rockMassHazard.getSwelling()
                    || rockMassHazard.getSqueezing()
                    || rockMassHazard.getSlaking());
        }

        if(f instanceof FailureZoneFragment) {
            FailureZoneFragment fzf = (FailureZoneFragment) f;
            Spinner spinnerTypeFault = (Spinner) fzf.getView().findViewById(R.id.spinner_sense_movement);
            EditText inputOrientationDD = ((EditText) fzf.getView().findViewById(R.id.input_orientation_dd));
            EditText inputOrientationD = ((EditText) fzf.getView().findViewById(R.id.input_orientation_d));
            EditText inputThickness = ((EditText) fzf.getView().findViewById(R.id.input_thickness));
            EditText inputMatrixBlock = ((EditText) fzf.getView().findViewById(R.id.input_matrix_Block));
            Spinner spinnerMatrixColourValue = (Spinner) fzf.getView().findViewById(R.id.spinner_matrix_colour_value);
            Spinner spinnerMatrixColourChroma = (Spinner) fzf.getView().findViewById(R.id.spinner_matrix_colour_chroma);
            Spinner spinnerMatrixColorHue = (Spinner) fzf.getView().findViewById(R.id.spinner_matrix_colour_hue);
            Spinner spinnerMatrixGrainSize = (Spinner) fzf.getView().findViewById(R.id.spinner_matrix_grain_size);
            Spinner spinnerBlockSize = (Spinner) fzf.getView().findViewById(R.id.spinner_block_size);
            Spinner spinnerBlockShape = (Spinner) fzf.getView().findViewById(R.id.spinner_block_shape);
            Spinner spinnerBlockGeneticGroup = (Spinner) fzf.getView().findViewById(R.id.spinner_block_genetic_group);
            Spinner spinnerBlockSubGroup = (Spinner) fzf.getView().findViewById(R.id.spinner_block_sub_group);
            EditText inputRakeOfStriae = ((EditText) fzf.getView().findViewById(R.id.input_rake_striae));
            CheckBox none = (CheckBox) fzf.getView().findViewById(R.id.chk_none);

            failureZone.setSenseOfMovement(spinnerTypeFault.getSelectedItemPosition());
            failureZone.setOrientationDd(Float.parseFloat(inputOrientationDD.getText().toString()));
            failureZone.setOrientationD(Float.parseFloat(inputOrientationD.getText().toString()));
            failureZone.setThickness(Float.parseFloat(inputThickness.getText().toString()));
            failureZone.setMatrixBlock(Float.parseFloat(inputMatrixBlock.getText().toString()));
            failureZone.setMatrixColourValue(spinnerMatrixColourValue.getSelectedItemPosition());
            failureZone.setMatrixColourChroma(spinnerMatrixColourChroma.getSelectedItemPosition());
            failureZone.setMatrixColourHue(spinnerMatrixColorHue.getSelectedItemPosition());
            failureZone.setMatrixGainSize(spinnerMatrixGrainSize.getSelectedItemPosition());
            failureZone.setBlockSize(spinnerBlockSize.getSelectedItemPosition());
            failureZone.setBlockShape(spinnerBlockShape.getSelectedItemPosition());
            failureZone.setBlockGeneticGroup(spinnerBlockGeneticGroup.getSelectedItemPosition());
            failureZone.setBlockSubGroup(spinnerBlockSubGroup.getSelectedItemPosition());
            failureZone.setRakeOfStriae(inputRakeOfStriae.getText().toString().isEmpty() ? 0 : Float.parseFloat(inputRakeOfStriae.getText().toString()));
            failureZone.setNoneRake(none.isChecked());
            failureZone.setCompleted(!inputOrientationDD.getText().toString().trim().equals("")
                    && !inputOrientationD.getText().toString().trim().equals("")
                    && !inputThickness.getText().toString().trim().equals("")
                    && !inputMatrixBlock.getText().toString().trim().equals("")
                    && spinnerTypeFault.getSelectedItemPosition() != 0
                    && spinnerMatrixColourValue.getSelectedItemPosition() != 0
                    && spinnerMatrixColourChroma.getSelectedItemPosition() != 0
                    && spinnerMatrixColorHue.getSelectedItemPosition() != 0
                    && spinnerMatrixGrainSize.getSelectedItemPosition() != 0
                    && spinnerBlockSize.getSelectedItemPosition() != 0
                    && spinnerBlockShape.getSelectedItemPosition() != 0
                    && spinnerBlockGeneticGroup.getSelectedItemPosition() != 0
                    && spinnerBlockSubGroup.getSelectedItemPosition() != 0
                    && !inputRakeOfStriae.getText().toString().trim().equals(""));
        }

        if(f instanceof ParticularitiesFragment) {
            ParticularitiesFragment pf = (ParticularitiesFragment) f;

            EditText inputInstabilityCausedBy = ((EditText) pf.getView().findViewById(R.id.input_instability_caused_by));
            EditText inputInstabilityLocation = ((EditText) pf.getView().findViewById(R.id.input_instability_location));
//            EditText inputOverbreakCausedBy = ((EditText) pf.getView().findViewById(R.id.input_overbreak_caused_by));
//            EditText inputOverbreakLocation = ((EditText) pf.getView().findViewById(R.id.input_overbreak_location));
            EditText inputOverbreakVolume = ((EditText) pf.getView().findViewById(R.id.input_volume_location));
            Spinner spinnerOverbreakCausedByValue = (Spinner) pf.getView().findViewById(R.id.spinner_overbreak_caused_by);

            CheckBox none1 = (CheckBox) pf.getView().findViewById(R.id.chk_none1);
            CheckBox none2 = (CheckBox) pf.getView().findViewById(R.id.chk_none2);

            particularities.setInstabilityCausedBy(inputInstabilityCausedBy.getText().toString());
            particularities.setInstabilityLocation(inputInstabilityLocation.getText().toString());
//            particularities.setOverbreakCausedBy(inputOverbreakCausedBy.getText().toString());
//            particularities.setOverbreakLocation(inputOverbreakLocation.getText().toString());
            particularities.setOverbreakCausedByValue(spinnerOverbreakCausedByValue.getSelectedItemPosition());
            particularities.setOverbreakVolume(inputOverbreakVolume.getText().toString().isEmpty() ? 0 : Integer.parseInt(inputOverbreakVolume.getText().toString()));
            particularities.setInstabilityNa(none1.isChecked());
            particularities.setOverbreakNa(none2.isChecked());
            particularities.setCompleted((!inputInstabilityCausedBy.getText().toString().equals("")
                    && !inputInstabilityLocation.getText().toString().equals("")
//                    && !inputOverbreakCausedBy.getText().toString().equals("")
//                    && !inputOverbreakLocation.getText().toString().equals("")
                    && !inputOverbreakVolume.getText().toString().equals("")
                    && spinnerOverbreakCausedByValue.getSelectedItemPosition() != 0) || (none1.isChecked() && none2.isChecked()));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public Particularities getParticularities() {
        return particularities;
    }

    public void setParticularities(Particularities particularities) {
        this.particularities = particularities;
    }

    public FailureZone getFailureZone() {
        return failureZone;
    }

    public void setFailureZone(FailureZone failureZone) {
        this.failureZone = failureZone;
    }

    public RockMassHazard getRockMassHazard() {
        return rockMassHazard;
    }

    public void setRockMassHazard(RockMassHazard rockMassHazard) {
        this.rockMassHazard = rockMassHazard;
    }

    public SpecialFeatures getSpecialFeatures() {
        return specialFeatures;
    }

    public void setSpecialFeatures(SpecialFeatures specialFeatures) {
        this.specialFeatures = specialFeatures;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public Fragment getCurrentTabFragment() {
        return currentTabFragment;
    }

    public void setOverbreakPicturePath(String overbreakPicturePath) {
        this.overbreakPicturePath = overbreakPicturePath;
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        private List<Fragment> mInputFragments;

        public ScreenSlidePagerAdapter(FragmentManager fm, List<Fragment> inputFragments) {
            super(fm);
            mInputFragments = inputFragments;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = mInputFragments.get(position);
            return fragment;
        }

        @Override
        public int getCount() {
            return NUM_INPUTS;
        }

        public Fragment getInputFragments(int position) {
            return mInputFragments.get(position);
        }
    }
}
