package cl.skava.mobile.tunnelapp.presentation.presenters.impl.prospection;

import android.content.Context;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.exports.base.ExportService;
import cl.skava.mobile.tunnelapp.data.exports.impl.xls.ExportServiceImpl;
import cl.skava.mobile.tunnelapp.data.repository.ProspectionHoleGroupRepositoryImpl;
import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.impl.prospection.ProspectionHoleGroupListInteractorImpl;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.prospection.ProspectionHoleGroupListInteractor;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroupCalculation;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.domain.repository.ProspectionHoleGroupRepository;
import cl.skava.mobile.tunnelapp.presentation.presenters.base.AbstractPresenter;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.prospection.ProspectionPresenter;

/**
 * Created by Jose Ignacio Vera on 02-08-2016.
 */
public class ProspectionPresenterImpl extends AbstractPresenter
        implements ProspectionPresenter,
        ProspectionHoleGroupListInteractor.Callback
 {

    private ProspectionPresenter.View mView;
    private ProspectionHoleGroupRepository mProspectionHoleGroupRepository;
    private ProspectionHoleGroupListInteractor mProspectionHoleGroupListInteractor;

    private Context mActivity;
    private String mFaceId;

     private ExportService mExportService;

    public ProspectionPresenterImpl(Executor executor,
                                    MainThread mainThread,
                                    View view,
                                    Context activity,
                                    String faceId) {
        super(executor, mainThread);
        mView = view;
        mActivity = activity;
        mFaceId = faceId;

        mProspectionHoleGroupRepository = new ProspectionHoleGroupRepositoryImpl(mActivity, mFaceId);

        mExportService = new ExportServiceImpl(mProspectionHoleGroupRepository.getCloudStoreService());

        mProspectionHoleGroupListInteractor = new ProspectionHoleGroupListInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mProspectionHoleGroupRepository,
                mExportService
        );
    }

    @Override
    public void resume() {
        mView.showProgress();
        mProspectionHoleGroupListInteractor.execute();
    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void createNewProspectionHoleGroup(ProspectionHoleGroup prospectionHoleGroup) {
        mProspectionHoleGroupListInteractor.create(prospectionHoleGroup);
    }

    @Override
    public void syncData(User user) {
        mProspectionHoleGroupListInteractor.syncData(user);
    }

    @Override
    public void deleteProspectionHoleGroup(ProspectionHoleGroup prospectionHoleGroup) {
        mProspectionHoleGroupListInteractor.delete(prospectionHoleGroup);
    }

    @Override
    public void updateProspectionHoleGroup(ProspectionHoleGroup prospectionHoleGroup) {
        mProspectionHoleGroupListInteractor.update(prospectionHoleGroup);
    }

     @Override
     public void getRodsByProspectionHoleMap(String prospectionHoleGroupId) {
         mProspectionHoleGroupListInteractor.getRodsByProspectionHoleMap(prospectionHoleGroupId);
     }

     @Override
     public void exportData(Project p, Tunnel t, Face f, String directoryPath) {
         mProspectionHoleGroupListInteractor.exportData(p, t, f, directoryPath);
     }

     @Override
     public void savePicture(ProspectionRefPicture prospectionRefPicture) {
         mProspectionHoleGroupListInteractor.savePicture(prospectionRefPicture);
     }

     @Override
     public void updateProspectionHoleGroupCalculations(List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations, String prospectionHoleGroupId) {
         mProspectionHoleGroupListInteractor.updateProspectionHoleGroupCalculations(prospectionHoleGroupCalculations, prospectionHoleGroupId);
     }

     @Override
    public void onProspectionHoleGroupListRetrieved(List<ProspectionHoleGroup> prospectionHoleGroups) {
        mView.displayProspectionHoleGroupList(prospectionHoleGroups);

        if(prospectionHoleGroups.isEmpty()) {
            mView.hideProgress();
        }
    }

    @Override
    public void onQValuesByMappingsRetrieved(HashMap<Mapping, QValue> qValuesByMappings) {
        mView.setQValuesMappings(qValuesByMappings);
    }

    @Override
    public void onSyncStatus(boolean status, String msg) {
        mView.hideProgress();
        mView.syncDataResult(status, msg);
    }

    @Override
    public void onSyncProgressPercentaje(Integer percentaje) {
        mView.onSyncProgressPercentaje(percentaje);
    }

    @Override
    public void notifySyncSegment(String msg) {
        mView.notifySyncSegment(msg);
    }

     @Override
     public void onRodsByProspectionHoleRetrieved(HashMap<ProspectionHole, List<Rod>> rodsByProspectionHole) {
//         mView.hideProgress();
         mView.onRodsByProspectionHoleRetrieved(rodsByProspectionHole);
     }

     @Override
     public void onExportStatus(boolean status) {
         mView.onExportFinish(status);
     }

     @Override
     public void onProspectionRefPictureListRetrieved(List<ProspectionRefPicture> prospectionRefPicture) {
         mView.hideProgress();
         mView.displayProspectionRefPictureList(prospectionRefPicture);
     }

     @Override
     public void onProspectionHoleGroupCalculationListRetrieved(HashMap<ProspectionHoleGroup, List<ProspectionHoleGroupCalculation>> calculationsByGroup) {
         mView.onCalculationsByProspectionHoleGroup(calculationsByGroup);
     }
 }
