package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.blobs.syncprocess;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.blobs.BlobStorageService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.ExpandedTunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Particularities;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Picture;
import cl.skava.mobile.tunnelapp.domain.model.mapping.StereonetPicture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.repository.BaseRepository;

/**
 * Created by JoseVera on 5/3/2017.
 */

public class ModuleStorageService {

    private BlobStorageService blobStorageService;
    private List<Mapping> allMapping;
    private CloudStoreService cloudStoreService;
    private List<Face> allFaces;
    private List<ProspectionHoleGroup> allProspectionHoleGroup;

    public ModuleStorageService(BlobStorageService mBlobStorageService, CloudStoreService mCloudStoreService, BaseRepository mCallback) {
        blobStorageService = mBlobStorageService;
        cloudStoreService = mCloudStoreService;
        cloudStoreService.setmCallback(mCallback);
    }

    public void syncStereonetFiles(List<Mapping> mappings) {
        HashMap<Mapping, File[]> stereonetFilesByMapping = new HashMap<>();
        HashMap<Mapping, StereonetPicture> stereonetsHashMap = new HashMap<>();
        for(Mapping m : mappings) {
            StereonetPicture stereonetPicture = (StereonetPicture) cloudStoreService.getMappingInput(m.getId(), StereonetPicture.class);
            if (stereonetPicture == null) {
                continue;
            }
            File[] stereonetFiles = new File[] {
                    blobStorageService.getLocalStereonetFile(m.getId())
            };
            stereonetFilesByMapping.put(m, stereonetFiles);
            stereonetsHashMap.put(m, stereonetPicture);
        }
        blobStorageService.setmFileListPerMapping(stereonetFilesByMapping);
        blobStorageService.setmStereonetsByMapping(stereonetsHashMap);
        blobStorageService.setModuleCode(0);
        blobStorageService.setMediaItemCode(2);
        blobStorageService.executeSyncProcess();
    }

    public void syncAdditionalInfoFiles(List<Mapping> mappings) {
        HashMap<Mapping, File[]> overbreakFilesByMapping = new HashMap<>();
        HashMap<Mapping, Particularities> particularitiesHashMap = new HashMap<>();
        for(Mapping m : mappings) {
            Particularities particularities = (Particularities) cloudStoreService.getMappingInput(m.getId(), Particularities.class);
            if (particularities == null) {
                continue;
            }
            File[] overbreakFiles = new File[] {
                    blobStorageService.getLocalOverbreakFile(m.getId())
            };
            overbreakFilesByMapping.put(m, overbreakFiles);
            particularitiesHashMap.put(m, particularities);
        }
        blobStorageService.setmFileListPerMapping(overbreakFilesByMapping);
        blobStorageService.setmParticularitiesByMapping(particularitiesHashMap);
        blobStorageService.setModuleCode(0);
        blobStorageService.setMediaItemCode(3);
        blobStorageService.executeSyncProcess();
    }

    public void syncForecastingModels() {
        //Sync forecast models
        List<String> ids = new ArrayList<>();
        List<Project> projects = cloudStoreService.getProjects();
        List<Tunnel> tunnels;
        for(Project p : projects) {
            tunnels = cloudStoreService.getTunnelsByProject(p.getId());
            for(Tunnel t : tunnels) {
                ids.add(t.getId());
            }
        }

        blobStorageService.setTunnelIds(ids);
        blobStorageService.setModuleCode(2);
        blobStorageService.executeSyncProcess();
    }

    public void syncPictureFiles(boolean syncAll, List<Mapping> mappings) {
        HashMap<Mapping, File[]> filesByMapping = new HashMap<>();
        HashMap<Mapping, List<Picture>> picturesByMapping = new HashMap<>();
        HashMap<Mapping, List<Picture>> picturesOriginalByMapping = new HashMap<>();
        List<Picture> mappingPicturesData;

        if(mappings != null) {
            for (Mapping m : mappings) {
                mappingPicturesData = (List<Picture>) cloudStoreService.getMappingInputPictures(m.getId(), Picture.class);

                if (mappingPicturesData.isEmpty()) {
                    continue;
                } else {
                    File[] localFiles = blobStorageService.getLocalPictureFiles(m.getId(), mappingPicturesData);
                    filesByMapping.put(m, localFiles);
                    picturesByMapping.put(m, mappingPicturesData);
                    picturesOriginalByMapping.put(m, mappingPicturesData);
                }
            }
        }
        blobStorageService.setmFileListPerMapping(filesByMapping);
        blobStorageService.setmPicturesPerMapping(picturesByMapping);
        blobStorageService.setPicturesOriginalByMapping(picturesOriginalByMapping);
        blobStorageService.setMediaItemCode(0);
        blobStorageService.setmSyncAll(syncAll);
        blobStorageService.executeSyncProcess();
    }

    public void syncProspectionHoleGroupFiles(boolean syncAll, List<ProspectionHoleGroup> phgList) {
        HashMap<ProspectionHoleGroup, File[]> filesPerProspectionHoleGroup = new HashMap<>();
        HashMap<ProspectionHoleGroup, List<ProspectionRefPicture>> picturesByProspectionHoleGroup = new HashMap<>();
        HashMap<ProspectionHoleGroup, List<ProspectionRefPicture>> picturesOriginalByProspectionHoleGroup = new HashMap<>();
        List<ProspectionRefPicture> prospectionRefPicturesData;

        for(ProspectionHoleGroup prospectionHoleGroup : phgList) {
            prospectionRefPicturesData = cloudStoreService.getProspectionRefPictureByPHG(prospectionHoleGroup.getId());

            if(prospectionRefPicturesData.isEmpty()) continue;
            else {
                File[] localFiles = blobStorageService.getLocalProspectionRefPictureFiles(prospectionHoleGroup.getId(), prospectionRefPicturesData);
                filesPerProspectionHoleGroup.put(prospectionHoleGroup, localFiles);
                picturesByProspectionHoleGroup.put(prospectionHoleGroup, prospectionRefPicturesData);
                picturesOriginalByProspectionHoleGroup.put(prospectionHoleGroup, prospectionRefPicturesData);
            }
        }

        blobStorageService.setmFilesPerProspectionHoleGroup(filesPerProspectionHoleGroup);
        blobStorageService.setPicturesByProspectionHoleGroup(picturesByProspectionHoleGroup);
        blobStorageService.setPicturesOriginalByProspectionHoleGroup(picturesOriginalByProspectionHoleGroup);
        blobStorageService.setModuleCode(1);
        blobStorageService.setMediaItemCode(0);
        blobStorageService.setmSyncAll(syncAll);
        blobStorageService.executeSyncProcess();
    }

    public void syncPicturesTableData() {
        HashMap<Mapping, List<Picture>> picturesPerMapping = blobStorageService.getmPicturesPerMapping();
        Iterator<Mapping> iter = picturesPerMapping.keySet().iterator();
        while(iter.hasNext()) {
            Mapping m = iter.next();
            List<Picture> pictures = picturesPerMapping.get(m);

            for(Picture picture : pictures) {
                cloudStoreService.updateEntity(Picture.class, picture);
            }
        }
        cloudStoreService.syncPictures(Picture.class);
    }

    public void syncExpandedTunnelFiles(boolean syncAll, List<Mapping> mappings) {
        HashMap<Mapping, File[]> filesByMapping = new HashMap<>();
        HashMap<Mapping, ExpandedTunnel> expandedByMapping = new HashMap<>();
        ExpandedTunnel mappingExpandedData;

        for(Mapping m : mappings) {
            mappingExpandedData = (ExpandedTunnel) cloudStoreService.getMappingInput(m.getId(), ExpandedTunnel.class);

            if(mappingExpandedData == null) {
                continue;
            }
            else {
                File[] localFile = new File[] {
                        blobStorageService.getLocalExpandedFile(m.getId(), mappingExpandedData),
                        blobStorageService.getLocalExpandedFile(m.getId(), mappingExpandedData, true),
                };
                filesByMapping.put(m, localFile);
                expandedByMapping.put(m, mappingExpandedData);
            }
        }
        blobStorageService.setmFileListPerMapping(filesByMapping);
        blobStorageService.setmExpandedByMapping(expandedByMapping);
        blobStorageService.setmSyncAll(syncAll);
        blobStorageService.setMediaItemCode(1);
        blobStorageService.executeSyncProcess();
    }

    public void syncExpandedTunnelTableData() {
        HashMap<Mapping, ExpandedTunnel> expandedByMappingRes = blobStorageService.getmExpandedByMapping();
        Iterator<Mapping> iter1 = expandedByMappingRes.keySet().iterator();
        while(iter1.hasNext()) {
            Mapping m = iter1.next();
            ExpandedTunnel expanded = expandedByMappingRes.get(m);
            cloudStoreService.updateEntity(ExpandedTunnel.class, expanded);
        }
        cloudStoreService.syncExpanded(ExpandedTunnel.class);
    }

    public void syncProspectionHoleGroupTableData() {
        HashMap<ProspectionHoleGroup, List<ProspectionRefPicture>> picturesByProspectionHoleGroup = blobStorageService.getPicturesByProspectionHoleGroup();
        Iterator<ProspectionHoleGroup> iter11 = picturesByProspectionHoleGroup.keySet().iterator();
        while (iter11.hasNext()) {
            ProspectionHoleGroup m = iter11.next();
            List<ProspectionRefPicture> pictures = picturesByProspectionHoleGroup.get(m);
            for (ProspectionRefPicture picture : pictures) cloudStoreService.updateEntity(ProspectionRefPicture.class, picture);
        }
        cloudStoreService.syncProspectionPictures(ProspectionRefPicture.class);
    }

    public void syncStereonetTableData() {
        HashMap<Mapping, StereonetPicture> stereonetsByMappingRes = blobStorageService.getmStereonetsByMapping();
        Iterator<Mapping> iter1 = stereonetsByMappingRes.keySet().iterator();
        while(iter1.hasNext()) {
            Mapping m = iter1.next();
            StereonetPicture stereonetPicture = stereonetsByMappingRes.get(m);
            cloudStoreService.updateEntity(StereonetPicture.class, stereonetPicture);
        }
        cloudStoreService.syncStereonet(StereonetPicture.class);
    }
}
