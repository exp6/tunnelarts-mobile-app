package cl.skava.mobile.tunnelapp.domain.interactors.interfaces.geoclassifier;

import cl.skava.mobile.tunnelapp.domain.interactors.base.Interactor;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInstance;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public interface MappingInputsInteractor extends Interactor {

    interface Callback {
        //Interactor callback methods goes here
//        void onMappingInputListRetrieved(List<MappingInputModule> mappingInput);
        void onMappingInstanceRetrieved(MappingInstance mappingInstance);
        void onSaveStatus(boolean success);
    }

    //Interactor methods goes here
    void saveMappingInstance(MappingInstance mappingInstance);
}
