package cl.skava.mobile.tunnelapp.presentation.ui.adapters.home;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.home.ModuleListFragment;

/**
 * Created by Jose Ignacio Vera on 13-06-2016.
 */
public class ModuleListAdapter extends RecyclerView.Adapter<ModuleListAdapter.ViewHolder> {

    private static final String TAG = "ModuleListAdapter";
    private List<Module> mModuleDataSet;
    private ModuleListFragment.OnModuleItemListener mCallback;

    private int selectedItem = 0;
    ViewHolder viewHolderPool[];
    protected RecyclerView mRecyclerView;

    public ModuleListAdapter(List<Module> dataSet,
                             ModuleListFragment.OnModuleItemListener callback,
                             RecyclerView recyclerView) {
        mModuleDataSet = dataSet;
        mCallback = callback;
        viewHolderPool = new ViewHolder[mModuleDataSet.size()];
        mRecyclerView = recyclerView;
        setHasStableIds(true);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedItem = getLayoutPosition();
                    mCallback.onModuleSelected(selectedItem);

                    for(ViewHolder vh : viewHolderPool) {
                        vh.itemView.setSelected(false);
                        vh.itemView.setBackgroundColor(Color.TRANSPARENT);
                    }

                    notifyDataSetChanged();
                    mRecyclerView.smoothScrollToPosition(selectedItem);
                }
            });
            textView = (TextView) v.findViewById(R.id.project_name);
        }

        public TextView getTextView() {
            return textView;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_home_faces_modules_row_item, viewGroup, false);
        holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Module module = mModuleDataSet.get(position);
        viewHolder.getTextView().setText(module.getName().toUpperCase());
        if(module.getEnabled()) viewHolder.itemView.findViewById(R.id.locked_icon).setVisibility(View.GONE);
        else viewHolder.itemView.findViewById(R.id.forward_icon).setVisibility(View.GONE);

        if (viewHolderPool[position] == null)
            viewHolderPool[position] = viewHolder;

        viewHolder.itemView.setSelected(selectedItem == position);
        viewHolder.itemView.setBackgroundColor(viewHolder.itemView.isSelected() ? Color.LTGRAY : Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return mModuleDataSet.size();
    }
}
