package cl.skava.mobile.tunnelapp.domain.repository;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;

/**
 * Created by Jose Ignacio Vera on 16-06-2016.
 */
public interface TunnelRepository {

    List<Tunnel> getTunnelsByProject(Project project);
}
