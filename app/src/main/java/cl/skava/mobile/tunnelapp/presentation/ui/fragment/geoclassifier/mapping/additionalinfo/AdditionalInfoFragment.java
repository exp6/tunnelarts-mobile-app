package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.additionalinfo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.AdditionalInformation;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMassHazard;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SpecialFeatures;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class AdditionalInfoFragment extends Fragment {

    private static final String TAG = "AdditionalInfoFragment";

    private SpecialFeatures mappingInput1;
    private RockMassHazard mappingInput2;

    private Boolean mappingFinished;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_additionalinfo_0, container, false);
        rootView.setTag(TAG);

        CheckBox none1 = (CheckBox) rootView.findViewById(R.id.chk_none1);
        final CheckBox chkZeolites = (CheckBox) rootView.findViewById(R.id.chk_zeolites);
        final CheckBox chkClay = (CheckBox) rootView.findViewById(R.id.chk_clay);
        final CheckBox chkChlorite = (CheckBox) rootView.findViewById(R.id.chk_chlorite);
        final CheckBox chkSulfides = (CheckBox) rootView.findViewById(R.id.chk_sulfides);
        final CheckBox chkSulfates = ((CheckBox) rootView.findViewById(R.id.chk_sulfates));

        CheckBox none2 = (CheckBox) rootView.findViewById(R.id.chk_none2);
        final CheckBox chkRockBurst = (CheckBox) rootView.findViewById(R.id.chk_rock_burst);
        final CheckBox chkSwelling = ((CheckBox) rootView.findViewById(R.id.chk_swelling));
        final CheckBox chkSqueezing = (CheckBox) rootView.findViewById(R.id.chk_squeezing);
        final CheckBox chkSlaking = ((CheckBox) rootView.findViewById(R.id.chk_slaking));

        none1.setChecked(mappingInput1.getNone());
        chkZeolites.setChecked(mappingInput1.getZeolites());
        chkClay.setChecked(mappingInput1.getClay());
        chkChlorite.setChecked(mappingInput1.getChlorite());
        chkSulfides.setChecked(mappingInput1.getSulfides());
        chkSulfates.setChecked(mappingInput1.getSulfates());

        none2.setChecked(mappingInput2.getNone());
        chkRockBurst.setChecked(mappingInput2.getRockBurst());
        chkSwelling.setChecked(mappingInput2.getSwelling());
        chkSqueezing.setChecked(mappingInput2.getSqueezing());
        chkSlaking.setChecked(mappingInput2.getSlaking());

        chkZeolites.setEnabled(!none1.isChecked());
        chkClay.setEnabled(!none1.isChecked());
        chkChlorite.setEnabled(!none1.isChecked());
        chkSulfides.setEnabled(!none1.isChecked());
        chkSulfates.setEnabled(!none1.isChecked());

        chkRockBurst.setEnabled(!none2.isChecked());
        chkSwelling.setEnabled(!none2.isChecked());
        chkSqueezing.setEnabled(!none2.isChecked());
        chkSlaking.setEnabled(!none2.isChecked());

        none1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                chkZeolites.setChecked(false);
                chkClay.setChecked(false);
                chkChlorite.setChecked(false);
                chkSulfides.setChecked(false);
                chkSulfates.setChecked(false);

                chkZeolites.setEnabled(!isChecked);
                chkClay.setEnabled(!isChecked);
                chkChlorite.setEnabled(!isChecked);
                chkSulfides.setEnabled(!isChecked);
                chkSulfates.setEnabled(!isChecked);
            }
        });

        none2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                chkRockBurst.setChecked(false);
                chkSwelling.setChecked(false);
                chkSqueezing.setChecked(false);
                chkSlaking.setChecked(false);

                chkRockBurst.setEnabled(!isChecked);
                chkSwelling.setEnabled(!isChecked);
                chkSqueezing.setEnabled(!isChecked);
                chkSlaking.setEnabled(!isChecked);
            }
        });

        if(mappingFinished) {
            none1.setEnabled(false);
            chkZeolites.setEnabled(false);
            chkClay.setEnabled(false);
            chkChlorite.setEnabled(false);
            chkSulfides.setEnabled(false);
            chkSulfates.setEnabled(false);

            none2.setEnabled(false);
            chkRockBurst.setEnabled(false);
            chkSwelling.setEnabled(false);
            chkSqueezing.setEnabled(false);
            chkSlaking.setEnabled(false);
        }

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMappingInput1(SpecialFeatures mappingInput1) {
        this.mappingInput1 = mappingInput1;
    }

    public void setMappingInput2(RockMassHazard mappingInput2) {
        this.mappingInput2 = mappingInput2;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }
}
