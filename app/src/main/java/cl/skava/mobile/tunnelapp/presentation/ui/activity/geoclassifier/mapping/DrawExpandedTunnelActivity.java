package cl.skava.mobile.tunnelapp.presentation.ui.activity.geoclassifier.mapping;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.IOException;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.presentation.views.geoclassifier.mapping.expandedtunnel.FlexibleGrid;
import cl.skava.mobile.tunnelapp.presentation.views.geoclassifier.mapping.expandedtunnel.LockableDraw;
import cl.skava.mobile.tunnelapp.presentation.views.geoclassifier.mapping.expandedtunnel.LockableScrollView;

public class DrawExpandedTunnelActivity extends AppCompatActivity {

    //private FreeDrawView mSignatureView;
    private LockableDraw mSignatureView;
    private Double diffCh;
    private String path;
    private String exportPath;
    private float perimeter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        diffCh = Math.abs(intent.getDoubleExtra("initial-ch", 0) - intent.getDoubleExtra("final-ch", 0));
        path = intent.getStringExtra("filename");
        int extensionIndex = path.lastIndexOf('.');
        exportPath = path.substring(0, extensionIndex) + "-export" + path.substring(extensionIndex, path.length());
        perimeter = intent.getFloatExtra("perimeter", 0.0f);

        setContentView(R.layout.activity_draw_expanded_tunnel);

        //mSignatureView = (FreeDrawView) findViewById(R.id.drawing_canvas);
        mSignatureView = (LockableDraw) findViewById(R.id.drawing_canvas);
        mSignatureView.setPaintAlpha(255);
        mSignatureView.setPaintWidthDp(4);
        mSignatureView.setPaintColor(Color.BLACK);
        mSignatureView.getBackground().setAlpha(0);

        final LockableScrollView scrollView = (LockableScrollView) findViewById(R.id.expanded_tunnel_scroll);
        final CheckBox checkBox = (CheckBox) findViewById(R.id.is_editing_picture);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                scrollView.setLocked(isChecked);
                mSignatureView.setLocked(!isChecked);
            }
        });

        TextView perimeterScale = (TextView) findViewById(R.id.perimeter_scale);
        perimeterScale.setText(getResources().getText(R.string.characterization_mappings_label_horizontal_segment) + " " + perimeter/12 + "mts");

        final Bitmap draw = BitmapFactory.decodeFile(path);
        if (draw != null) {
            mSignatureView.setBackground(new Drawable() {
                @Override
                public void draw(Canvas canvas) {
                    canvas.drawBitmap(draw, 0, 0, null);
                }

                @Override
                public void setAlpha(int alpha) {

                }

                @Override
                public void setColorFilter(ColorFilter colorFilter) {

                }

                @Override
                public int getOpacity() {
                    return PixelFormat.OPAQUE;
                }
            });
        }

        Button currentColor = (Button) findViewById(R.id.current_color);
        currentColor.setBackgroundColor(Color.BLACK);

        SeekBar seekBar1 = (SeekBar) findViewById(R.id.stroke_width);
        seekBar1.setMax(50);
        seekBar1.setProgress((int) mSignatureView.getPaintWidth());
        seekBar1.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        mSignatureView.setPaintWidthDp(progress);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );

        SeekBar seekBar2 = (SeekBar) findViewById(R.id.alpha_value);
        seekBar2.setMax(255);
        seekBar2.setProgress(mSignatureView.getPaintAlpha());
        seekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mSignatureView.setPaintAlpha(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Button undo = (Button) findViewById(R.id.drawer_undo);
        undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSignatureView.undoLast();
            }
        });

        Button redo = (Button) findViewById(R.id.drawer_redo);
        redo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSignatureView.redoLast();
            }
        });

        Button undoAll = (Button) findViewById(R.id.drawer_undo_all);
        undoAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSignatureView.undoAll();
            }
        });

        Button screenshot = (Button) findViewById(R.id.button_screenshot);
        screenshot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                View vWorkArea = v.getRootView().findViewById(R.id.expanded_workarea);
                vWorkArea.setDrawingCacheEnabled(true);
                vWorkArea.buildDrawingCache();

                View vCanvas = v.getRootView().findViewById(R.id.drawing_canvas);
                vCanvas.setDrawingCacheEnabled(true);
                vCanvas.buildDrawingCache();

                int height = mSignatureView.getMinimumHeight();
                int width = mSignatureView.getWidth();

                Bitmap bExport = DrawExpandedTunnelActivity.cargarBitmapDesdeVista(vWorkArea, width, height);
                Bitmap bOut = DrawExpandedTunnelActivity.cargarBitmapDesdeVista(vCanvas, width, height);
                bOut.setHasAlpha(true);

                java.io.FileOutputStream out = null;
                java.io.FileOutputStream export = null;
                BufferedOutputStream outputStream = null;
                BufferedOutputStream exportStream = null;

                try {
                    out = new java.io.FileOutputStream(path);
                    outputStream = new BufferedOutputStream(out);
                    export = new java.io.FileOutputStream(exportPath);
                    exportStream = new BufferedOutputStream(export);
                    bOut.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                    bExport.compress(Bitmap.CompressFormat.PNG, 100, exportStream);
                    outputStream.flush();
                    exportStream.flush();
                    Toast.makeText(v.getContext(), "Imagen guardada.", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    Toast.makeText(v.getContext(), "No se pudo guardar la imagen.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) out.close();
                        if (export != null) export.close();
                        if (outputStream != null) outputStream.close();
                        if (exportStream != null) exportStream.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    vWorkArea.setDrawingCacheEnabled(false);
                    vCanvas.setDrawingCacheEnabled(false);
                    Intent rIntent = new Intent();
                    rIntent.setData(new Uri.Builder().path(path).build());
                    setResult(RESULT_OK, rIntent);
                    finish();
                }
            }
        });

        FlexibleGrid flexibleGrid = (FlexibleGrid) findViewById(R.id.background_grid);
        flexibleGrid.setSizeOfExcavation(diffCh);

        mSignatureView.setMinimumHeight(flexibleGrid.getMinimumHeight());
        mSignatureView.setLayoutParams(new RelativeLayout.LayoutParams(getResources().getDisplayMetrics().widthPixels, flexibleGrid.getMinimumHeight()));
        mSignatureView.invalidate();
    }

    public void changeColor(View v) {
        int color;
        switch (v.getId()) {
            case (R.id.button_red):
                color = 0xff0000;
                break;
            case (R.id.button_orange):
                color = 0xffa500;
                break;
            case (R.id.button_yellow):
                color = 0xffd700;
                break;
            case (R.id.button_green):
                color = 0x228b22;
                break;
            case (R.id.button_cyan):
                color = 0x48d1cc;
                break;
            case (R.id.button_blue):
                color = 0x4682b4;
                break;
            case (R.id.button_purple):
                color = 0x9499d3;
                break;
            default:
                color = 0;
                break;
        }
        mSignatureView.setPaintColor(color);
        Button currentColor = (Button) findViewById(R.id.current_color);
        currentColor.setBackgroundColor(0xFF000000+color);
    }

    /**
     * Carga un nuevo Bitmap a partir de una View, que tiene el contenido a dibujar en su totalidad
     * en vez de únicamente lo que sale en pantalla.
     *
     * @param v la vista cuyo contenido completo quiere dibujarse
     * @param width el ancho que quiere dibujarse
     * @param height el alto que quiere dibujarse
     * @return un <code>Bitmap</code> con la imagen que desea guardarse
     */
    public static Bitmap cargarBitmapDesdeVista(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(0, 0, v.getLayoutParams().width, v.getLayoutParams().height);
        v.draw(c);
        return b;
    }
}
