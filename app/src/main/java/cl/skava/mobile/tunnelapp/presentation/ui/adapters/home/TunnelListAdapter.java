package cl.skava.mobile.tunnelapp.presentation.ui.adapters.home;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.home.TunnelListFragment;

/**
 * Created by Jose Ignacio Vera on 08-06-2016.
 */
public class TunnelListAdapter extends RecyclerView.Adapter<TunnelListAdapter.ViewHolder> {

    private static final String TAG = "TunnelListAdapter";
    protected List<Tunnel> mTunnelDataset;
    private TunnelListFragment.OnTunnelItemSelectedListener mCallback;

    private int selectedItem = 0;
    ViewHolder viewHolderPool[];
    protected RecyclerView mRecyclerView;

    public TunnelListAdapter(List<Tunnel> dataSet,
                             TunnelListFragment.OnTunnelItemSelectedListener callback,
                             RecyclerView recyclerView) {
        mTunnelDataset = dataSet;
        mCallback = callback;
        viewHolderPool = new ViewHolder[mTunnelDataset.size()];
        mRecyclerView = recyclerView;
        setHasStableIds(true);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedItem = getLayoutPosition();
                    mCallback.onTunnelSelected(selectedItem);

                    for(ViewHolder vh : viewHolderPool) {
                        vh.itemView.setSelected(false);
                        vh.itemView.setBackgroundColor(Color.TRANSPARENT);
                    }
                    notifyDataSetChanged();
                    mRecyclerView.smoothScrollToPosition(selectedItem);
                }
            });
            textView = (TextView) v.findViewById(R.id.project_name);
        }

        public TextView getTextView() {
            return textView;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_home_tunnels_row_item, viewGroup, false);
        holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");
        viewHolder.getTextView().setText(mTunnelDataset.get(position).getName());

        if (viewHolderPool[position] == null)
            viewHolderPool[position] = viewHolder;

        viewHolder.itemView.setSelected(selectedItem == position);
        viewHolder.itemView.setBackgroundColor(viewHolder.itemView.isSelected() ? Color.LTGRAY : Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return mTunnelDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
