package cl.skava.mobile.tunnelapp.data.net.cloud.impl.azure.tables;

import android.support.annotation.NonNull;

import com.microsoft.windowsazure.mobileservices.table.sync.localstore.ColumnDataType;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jose Ignacio Vera on 15-07-2016.
 */
public class SyncClientHelper {

    public void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    protected static ColumnDataType translateType(String type) {

        switch (type) {
            case "String":
                return ColumnDataType.String;
            case "Integer":
                return ColumnDataType.Integer;
            case "Boolean":
                return ColumnDataType.Boolean;
            case "Date":
                return ColumnDataType.Date;
            case "DateTimeOffset":
                return ColumnDataType.DateTimeOffset;
            case "Float":
                return ColumnDataType.Real;
            case "Custom":
                return ColumnDataType.String;
            case "Version":
                return ColumnDataType.String;
        }

        return ColumnDataType.String;
    }
}
