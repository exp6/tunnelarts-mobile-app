package cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue;

import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInput;
import cl.skava.mobile.tunnelapp.domain.model.modelentity.ModelEntity;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class QValue extends DataInput implements ModelEntity {

    private String id;
    private Float rqd;
    private Float jn;
    private Float jr;
    private Float ja;
    private Float jw;
    private Float srf;
    private Integer rockQualityCode;
    private String idMapping;
    private Boolean completed;
    private Float qMax;
    private Float qMin;
    private Float qAvg;
    private Integer discontinuities;
    private Integer jnType;

    public QValue(String id, Float rqd, Float jn, Float jr, Float ja, Float jw, Float srf, Integer rockQualityCode, String idMapping, Boolean completed, Float qMax, Float qMin, Float qAvg, Integer discontinuities, Integer jnType) {
        this.id = id;
        this.rqd = rqd;
        this.jn = jn;
        this.jr = jr;
        this.ja = ja;
        this.jw = jw;
        this.srf = srf;
        this.rockQualityCode = rockQualityCode;
        this.idMapping = idMapping;
        this.completed = completed;
        this.qMax = qMax;
        this.qMin = qMin;
        this.qAvg = qAvg;
        this.discontinuities = discontinuities;
        this.jnType = jnType;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Float getRqd() {
        return rqd;
    }

    public void setRqd(Float rqd) {
        this.rqd = rqd;
    }

    public Float getJn() {
        return jn;
    }

    public void setJn(Float jn) {
        this.jn = jn;
    }

    public Float getJr() {
        return jr;
    }

    public void setJr(Float jr) {
        this.jr = jr;
    }

    public Float getJa() {
        return ja;
    }

    public void setJa(Float ja) {
        this.ja = ja;
    }

    public Float getJw() {
        return jw;
    }

    public void setJw(Float jw) {
        this.jw = jw;
    }

    public Float getSrf() {
        return srf;
    }

    public void setSrf(Float srf) {
        this.srf = srf;
    }

    public Integer getRockQualityCode() {
        return rockQualityCode;
    }

    public void setRockQualityCode(Integer rockQualityCode) {
        this.rockQualityCode = rockQualityCode;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Float getqMax() {
        return qMax;
    }

    public void setqMax(Float qMax) {
        this.qMax = qMax;
    }

    public Float getqMin() {
        return qMin;
    }

    public void setqMin(Float qMin) {
        this.qMin = qMin;
    }

    public Float getqAvg() {
        return qAvg;
    }

    public void setqAvg(Float qAvg) {
        this.qAvg = qAvg;
    }

    public Integer getDiscontinuities() {
        return discontinuities;
    }

    public void setDiscontinuities(Integer discontinuities) {
        this.discontinuities = discontinuities;
    }

    public Integer getJnType() {
        return jnType;
    }

    public void setJnType(Integer jnType) {
        this.jnType = jnType;
    }

    @Override
    public String toString() {
        return "QValue{" +
                "id='" + id + '\'' +
                ", rqd=" + rqd +
                ", jn=" + jn +
                ", jr=" + jr +
                ", ja=" + ja +
                ", jw=" + jw +
                ", srf=" + srf +
                ", rockQualityCode=" + rockQualityCode +
                ", idMapping='" + idMapping + '\'' +
                ", completed=" + completed +
                ", qMax=" + qMax +
                ", qMin=" + qMin +
                ", qAvg=" + qAvg +
                ", discontinuities=" + discontinuities +
                ", jnType=" + jnType +
                '}';
    }
}
