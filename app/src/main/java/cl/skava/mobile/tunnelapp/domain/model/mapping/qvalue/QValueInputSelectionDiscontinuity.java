package cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue;

/**
 * Created by Jose Ignacio Vera on 14-10-2016.
 */
public class QValueInputSelectionDiscontinuity {

    private String id;
    private String idQValueInputSelection;
    private Integer discontinuity;
    private String jr;
    private String ja;

    public QValueInputSelectionDiscontinuity(String id, String idQValueInputSelection, Integer discontinuity, String jr, String ja) {
        this.id = id;
        this.idQValueInputSelection = idQValueInputSelection;
        this.discontinuity = discontinuity;
        this.jr = jr;
        this.ja = ja;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdQValueInputSelection() {
        return idQValueInputSelection;
    }

    public void setIdQValueInputSelection(String idQValueInputSelection) {
        this.idQValueInputSelection = idQValueInputSelection;
    }

    public Integer getDiscontinuity() {
        return discontinuity;
    }

    public void setDiscontinuity(Integer discontinuity) {
        this.discontinuity = discontinuity;
    }

    public String getJr() {
        return jr;
    }

    public void setJr(String jr) {
        this.jr = jr;
    }

    public String getJa() {
        return ja;
    }

    public void setJa(String ja) {
        this.ja = ja;
    }

    @Override
    public String toString() {
        return "QValueInputSelectionDiscontinuity{" +
                "id='" + id + '\'' +
                ", idQValueInputSelection='" + idQValueInputSelection + '\'' +
                ", discontinuity=" + discontinuity +
                ", jr='" + jr + '\'' +
                ", ja='" + ja + '\'' +
                '}';
    }
}
