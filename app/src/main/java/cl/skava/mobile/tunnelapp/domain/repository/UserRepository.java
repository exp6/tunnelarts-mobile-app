package cl.skava.mobile.tunnelapp.domain.repository;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.account.User;

/**
 * Created by Jose Ignacio Vera on 25-08-2016.
 */
public interface UserRepository {
    List<User> getList();
    void writeCache(User user);
    boolean isCached(User user);
    User get(String userId);
}
