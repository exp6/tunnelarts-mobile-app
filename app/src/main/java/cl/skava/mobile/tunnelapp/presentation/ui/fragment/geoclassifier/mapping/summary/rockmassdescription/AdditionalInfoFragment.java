package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.rockmassdescription;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.FailureZone;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Particularities;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMassHazard;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SpecialFeatures;

/**
 * Created by Jose Ignacio Vera on 20-10-2016.
 */
public class AdditionalInfoFragment extends Fragment {

    private SpecialFeatures specialFeatures;
    private RockMassHazard rockMassHazard;
    private FailureZone failureZone;
    private Particularities particularities;

    private List<String[]> rockTypes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_summary_additionalinfo, container, false);

        ImageView img1 = ((ImageView) rootView.findViewById(R.id.img11));
        ImageView img2 = ((ImageView) rootView.findViewById(R.id.img12));
        ImageView img3 = ((ImageView) rootView.findViewById(R.id.img13));
//        ImageView img4 = ((ImageView) rootView.findViewById(R.id.img14));
        ImageView img5 = ((ImageView) rootView.findViewById(R.id.img15));
        ImageView img6 = ((ImageView) rootView.findViewById(R.id.img16));

        if(specialFeatures.getZeolites()) img1.setImageResource(R.drawable.ic_check_box);
        if(specialFeatures.getClay()) img2.setImageResource(R.drawable.ic_check_box);
        if(specialFeatures.getChlorite()) img3.setImageResource(R.drawable.ic_check_box);
//        if(specialFeatures.getRedTuff()) img4.setImageResource(R.drawable.ic_check_box);
        if(specialFeatures.getSulfides()) img5.setImageResource(R.drawable.ic_check_box);
        if(specialFeatures.getSulfates()) img6.setImageResource(R.drawable.ic_check_box);

        ImageView img7 = ((ImageView) rootView.findViewById(R.id.img21));
        ImageView img8 = ((ImageView) rootView.findViewById(R.id.img22));
        ImageView img9 = ((ImageView) rootView.findViewById(R.id.img23));
        ImageView img10 = ((ImageView) rootView.findViewById(R.id.img24));

        if(rockMassHazard.getRockBurst()) img7.setImageResource(R.drawable.ic_check_box);
        if(rockMassHazard.getSwelling()) img8.setImageResource(R.drawable.ic_check_box);
        if(rockMassHazard.getSqueezing()) img9.setImageResource(R.drawable.ic_check_box);
        if(rockMassHazard.getSlaking()) img10.setImageResource(R.drawable.ic_check_box);

        String[] array1 = getResources().getStringArray(R.array.sense_of_movement_dropdown_arrays);
        String[] array2 = getResources().getStringArray(R.array.colour_chroma_dropdown_arrays);
        String[] array3 = getResources().getStringArray(R.array.colour_hue_dropdown_arrays);
        String[] array4 = getResources().getStringArray(R.array.grain_size_dropdown_arrays);
        String[] array5 = getResources().getStringArray(R.array.block_size_dropdown_arrays);
        String[] array6 = getResources().getStringArray(R.array.block_shape_dropdown_arrays);
        String[] array7 = getResources().getStringArray(R.array.genetic_group_dropdown_arrays);
        String[] array8 = getResources().getStringArray(R.array.colour_value_dropdown_arrays);
        String[] array9 = getResources().getStringArray(R.array.sub_group_dropdown_arrays);

        rockTypes = new ArrayList<>(6);
        rockTypes.add(0, getResources().getStringArray(R.array.null_dropdown_arrays));
        rockTypes.add(1, getResources().getStringArray(R.array.igneous_plutonic_dropdown_arrays));
        rockTypes.add(2, getResources().getStringArray(R.array.igneous_volcanic_dropdown_arrays));
        rockTypes.add(3, getResources().getStringArray(R.array.pyroclastic_dropdown_arrays));
        rockTypes.add(4, getResources().getStringArray(R.array.sedimentary_clastic_dropdown_arrays));
        rockTypes.add(5, getResources().getStringArray(R.array.sedimentary_chemical_dropdown_arrays));
        rockTypes.add(6, getResources().getStringArray(R.array.metamorphic_dropdown_arrays));

        TextView array111 = (TextView) rootView.findViewById(R.id.array1);
        array111.setText(failureZone.getSenseOfMovement() == 0 ? "N/A" : array1[failureZone.getSenseOfMovement()]);

        TextView array12 = (TextView) rootView.findViewById(R.id.array2);
        array12.setText(failureZone.getRakeOfStriae() + " [°]");

        TextView array13 = (TextView) rootView.findViewById(R.id.array3);
        array13.setText(failureZone.getOrientationDd() + " [°]");

        TextView array14 = (TextView) rootView.findViewById(R.id.array4);
        array14.setText(failureZone.getOrientationD() + " [°]");

        TextView array15 = (TextView) rootView.findViewById(R.id.array5);
        array15.setText(failureZone.getThickness() + " [m]");

        TextView array16 = (TextView) rootView.findViewById(R.id.array6);
        array16.setText(failureZone.getMatrixBlock() + " [%]");

        TextView array17 = (TextView) rootView.findViewById(R.id.array7);
        array17.setText(failureZone.getMatrixColourValue() == 0 ? "N/A" : array8[failureZone.getMatrixColourValue()]);

        TextView array18 = (TextView) rootView.findViewById(R.id.array8);
        array18.setText(failureZone.getMatrixColourChroma() == 0 ? "N/A" : array2[failureZone.getMatrixColourChroma()]);

        TextView array19 = (TextView) rootView.findViewById(R.id.array9);
        array19.setText(failureZone.getMatrixColourHue() == 0 ? "N/A" : array3[failureZone.getMatrixColourHue()]);

        TextView array20 = (TextView) rootView.findViewById(R.id.array10);
        array20.setText(failureZone.getMatrixGainSize() == 0 ? "N/A" : array4[failureZone.getMatrixGainSize()]);

        TextView array21 = (TextView) rootView.findViewById(R.id.array11);
        array21.setText(failureZone.getBlockSize() == 0 ? "N/A" : array5[failureZone.getBlockSize()]);

        TextView array22 = (TextView) rootView.findViewById(R.id.array12);
        array22.setText(failureZone.getBlockShape() == 0 ? "N/A" : array6[failureZone.getBlockShape()]);

        TextView array23 = (TextView) rootView.findViewById(R.id.array13);
        array23.setText(failureZone.getBlockGeneticGroup() == 0 ? "N/A" : array7[failureZone.getBlockGeneticGroup()]);

        TextView array24 = (TextView) rootView.findViewById(R.id.array14);
        array24.setText(failureZone.getBlockSubGroup() == 0 ? "N/A" : rockTypes.get(failureZone.getBlockGeneticGroup()-1)[failureZone.getBlockSubGroup()]);


        TextView text1 = (TextView) rootView.findViewById(R.id.text1);
        text1.setText(particularities.getInstabilityCausedBy().trim().isEmpty() ? "N/A" : particularities.getInstabilityCausedBy().trim());

        TextView text2 = (TextView) rootView.findViewById(R.id.text2);
        text2.setText(particularities.getInstabilityLocation().trim().isEmpty() ? "N/A" : particularities.getInstabilityLocation().trim());

        TextView text3 = (TextView) rootView.findViewById(R.id.text3);
        text3.setText(particularities.getOverbreakCausedBy().trim().isEmpty() ? "N/A" : particularities.getOverbreakCausedBy().trim());

        TextView text4 = (TextView) rootView.findViewById(R.id.text4);
        text4.setText(particularities.getOverbreakLocation().trim().isEmpty() ? "N/A" : particularities.getOverbreakLocation().trim());

        TextView text5 = (TextView) rootView.findViewById(R.id.text5);
        text5.setText(particularities.getOverbreakVolume().toString());

        return rootView;
    }

    public void setSpecialFeatures(SpecialFeatures specialFeatures) {
        this.specialFeatures = specialFeatures;
    }

    public void setRockMassHazard(RockMassHazard rockMassHazard) {
        this.rockMassHazard = rockMassHazard;
    }

    public void setFailureZone(FailureZone failureZone) {
        this.failureZone = failureZone;
    }

    public void setParticularities(Particularities particularities) {
        this.particularities = particularities;
    }
}
