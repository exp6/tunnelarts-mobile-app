package cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue;

/**
 * Created by Jose Ignacio Vera on 14-10-2016.
 */
public class QValueDiscontinuity {

    private String id;
    private String idQValue;
    private Integer discontinuity;
    private Float jr;
    private Float ja;
    private Boolean completed;
    private String jrSelection;
    private String jaSelection;

    public QValueDiscontinuity(String id, String idQValue, Integer discontinuity, Float jr, Float ja, Boolean completed, String jrSelection, String jaSelection) {
        this.id = id;
        this.idQValue = idQValue;
        this.discontinuity = discontinuity;
        this.jr = jr;
        this.ja = ja;
        this.completed = completed;
        this.jrSelection = jrSelection;
        this.jaSelection = jaSelection;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdQValue() {
        return idQValue;
    }

    public void setIdQValue(String idQValue) {
        this.idQValue = idQValue;
    }

    public Integer getDiscontinuity() {
        return discontinuity;
    }

    public void setDiscontinuity(Integer discontinuity) {
        this.discontinuity = discontinuity;
    }

    public Float getJr() {
        return jr;
    }

    public void setJr(Float jr) {
        this.jr = jr;
    }

    public Float getJa() {
        return ja;
    }

    public void setJa(Float ja) {
        this.ja = ja;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public String getJrSelection() {
        return jrSelection;
    }

    public void setJrSelection(String jrSelection) {
        this.jrSelection = jrSelection;
    }

    public String getJaSelection() {
        return jaSelection;
    }

    public void setJaSelection(String jaSelection) {
        this.jaSelection = jaSelection;
    }

    @Override
    public String toString() {
        return "QValueDiscontinuity{" +
                "id='" + id + '\'' +
                ", idQValue='" + idQValue + '\'' +
                ", discontinuity=" + discontinuity +
                ", jr=" + jr +
                ", ja=" + ja +
                ", completed=" + completed +
                ", jrSelection='" + jrSelection + '\'' +
                ", jaSelection='" + jaSelection + '\'' +
                '}';
    }
}
