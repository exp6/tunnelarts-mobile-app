package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOperations;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.LocalStoreService;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.MappingInput;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.ProjectMappingInput;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.UserProject;
import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInput;
import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInputType;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueDiscontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputSelectionDiscontinuity;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroupCalculation;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;

/**
 * Created by Jose Ignacio Vera on 23-09-2016.
 */
public class LocalStoreServiceImpl implements LocalStoreService {

    private MobileServiceClient mobileServiceClient;

    public void setMobileServiceClient(MobileServiceClient mobileServiceClient) {
        this.mobileServiceClient = mobileServiceClient;
    }

    public List<Project> getProjects() {
        List<Project> mProjects = new ArrayList<>();

        try {
            mProjects = mobileServiceClient.getSyncTable(Project.class).read(QueryOperations.tableName(Project.class.getSimpleName())).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mProjects;
    }

    public List<UserProject> getProjectsByUser(String userId) {
        List<UserProject> mUserProjects = new ArrayList<>();

        try {
            mUserProjects = mobileServiceClient.getSyncTable(UserProject.class)
                    .read(QueryOperations.tableName(UserProject.class.getSimpleName()).field("IdUser").eq(userId)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mUserProjects;
    }

    public Project getProjectsById(String projectId) {
        Project mProject = null;

        try {
            mProject = mobileServiceClient.getSyncTable(Project.class)
                    .lookUp(projectId)
                    .get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mProject;
    }

    public Client getClientById(String clientId) {
        Client mClient = null;

        try {
            mClient = this.mobileServiceClient.getSyncTable(Client.class)
                    .lookUp(clientId)
                    .get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mClient;
    }

    public List<Tunnel> getTunnelsByProject(String projectId) {
        List<Tunnel> mTunnels = new ArrayList<>();

        try {
            mTunnels = mobileServiceClient.getSyncTable(Tunnel.class)
                    .read(QueryOperations.tableName(Tunnel.class.getSimpleName()).field("IdProject").eq(projectId)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mTunnels;
    }

    public List<Face> getFaces() {
        List<Face> mFaces = new ArrayList<>();

        try {
            mFaces = mobileServiceClient.getSyncTable(Face.class).read(QueryOperations.tableName(Face.class.getSimpleName())).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mFaces;
    }

    public List<Face> getFacesByTunnel(String tunnelId) {
        List<Face> mFaces = new ArrayList<>();

        try {
            mFaces = mobileServiceClient.getSyncTable(Face.class)
                    .read(QueryOperations.tableName(Face.class.getSimpleName()).field("IdTunnel").eq(tunnelId)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mFaces;
    }

    public List<Mapping> getMappingsByFace(String faceId) {
        List<Mapping> mMappings = new ArrayList<>();

        try {
            mMappings = mobileServiceClient.getSyncTable(Mapping.class)
                    .read(QueryOperations.tableName(Mapping.class.getSimpleName()).field("IdFace").eq(faceId).orderBy("createdAt", QueryOrder.Ascending)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mMappings;
    }

    public Mapping getMappingById(String mappingId) {
        Mapping mMapping = null;

        try {
            mMapping = mobileServiceClient.getSyncTable(Mapping.class)
                    .lookUp(mappingId)
                    .get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mMapping;
    }

    public ProspectionHoleGroup getProspectionHoleGroupById(String prospectionHoleGroupId) {
        ProspectionHoleGroup prospectionHoleGroup = null;

        try {
            prospectionHoleGroup = mobileServiceClient.getSyncTable(ProspectionHoleGroup.class)
                    .lookUp(prospectionHoleGroupId)
                    .get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return prospectionHoleGroup;
    }

    public boolean persistEntity(Class c, Object o) {
        try {
            mobileServiceClient.getSyncTable(c.getSimpleName(), c).insert(o).get();
            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        } catch (ExecutionException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean deleteEntity(Class c, Object o) {
        try {
            mobileServiceClient.getSyncTable(c.getSimpleName(), c).delete(o).get();
            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        } catch (ExecutionException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateEntity(Class c, Object o) {
        try {
            mobileServiceClient.getSyncTable(c.getSimpleName(), c).update(o).get();
            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        } catch (ExecutionException e) {
            e.printStackTrace();
            return false;
        }
    }

    public DataInput getMappingInput(String mappingId, Class c) {
        List<DataInput> mMappingInputList = new ArrayList<>();
        DataInput mMappingInput = null;

        try {
            mMappingInputList = (List<DataInput>) mobileServiceClient.getSyncTable(c)
                    .read(QueryOperations.tableName(c.getSimpleName()).field("IdMapping").eq(mappingId))
                    .get();
            if(!mMappingInputList.isEmpty())
                mMappingInput = mMappingInputList.get(0);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mMappingInput;
    }

    public DataInput getSubMappingInput(String subInputId, Class c) {
        List<DataInput> mMappingInputList = new ArrayList<>();
        DataInput mMappingInput = null;

        try {
            mMappingInputList = (List<DataInput>) mobileServiceClient.getSyncTable(c)
                    .read(QueryOperations.tableName(c.getSimpleName()).field("IdAdditionalInformation").eq(subInputId))
                    .get();
            if(!mMappingInputList.isEmpty())
                mMappingInput = mMappingInputList.get(0);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mMappingInput;
    }

    public List<? extends DataInput> getMappingInputs(String mappingId, Class c) {
        List<? extends DataInput> mMappingInputList = new ArrayList<>();

        try {
            mMappingInputList = (List<? extends DataInput>) mobileServiceClient.getSyncTable(c)
                    .read(QueryOperations.tableName(c.getSimpleName()).field("IdMapping").eq(mappingId).orderBy("position", QueryOrder.Ascending))
                    .get();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mMappingInputList;
    }

    public List<? extends DataInput> getMappingInputPictures(String mappingId, Class c) {
        List<? extends DataInput> mMappingInputList = new ArrayList<>();

        try {
            mMappingInputList = (List<? extends DataInput>) mobileServiceClient.getSyncTable(c)
                    .read(QueryOperations.tableName(c.getSimpleName()).field("IdMapping").eq(mappingId).orderBy("code", QueryOrder.Ascending))
                    .get();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mMappingInputList;
    }

    public List<? extends DataInputType> getMappingInputTypes(Class c) {
        List<? extends DataInputType> mMappingInputList = new ArrayList<>();

        try {
            mMappingInputList = (List<? extends DataInputType>) mobileServiceClient.getSyncTable(c)
                    .read(QueryOperations.tableName(c.getSimpleName()))
                    .get();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mMappingInputList;
    }

    public List<? extends DataInputType> getMappingInputTypes(Class c, String idType, String column) {
        List<? extends DataInputType> mMappingInputList = new ArrayList<>();

        try {
            mMappingInputList = (List<? extends DataInputType>) mobileServiceClient.getSyncTable(c)
                    .read(QueryOperations.tableName(c.getSimpleName()).field(column).eq(idType).orderBy("codeindex", QueryOrder.Ascending))
                    .get();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mMappingInputList;
    }

    public List<ProspectionHoleGroup> getProspectionHoleGroupsByFace(String faceId) {
        List<ProspectionHoleGroup> mProspectionHoleGroups = new ArrayList<>();

        try {
            mProspectionHoleGroups = mobileServiceClient.getSyncTable(ProspectionHoleGroup.class)
                    .read(QueryOperations.tableName(ProspectionHoleGroup.class.getSimpleName()).field("IdFace").eq(faceId).orderBy("createdAt", QueryOrder.Ascending)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mProspectionHoleGroups;
    }


    public List<ProspectionHoleGroupCalculation> getProspectionCalculationByGroups(String prospectionHoleGroupId) {
        List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations = new ArrayList<>();

        try {
            prospectionHoleGroupCalculations = mobileServiceClient.getSyncTable(ProspectionHoleGroupCalculation.class)
                    .read(QueryOperations.tableName(ProspectionHoleGroupCalculation.class.getSimpleName()).field("IdProspectionHoleGroup").eq(prospectionHoleGroupId).orderBy("rodPosition", QueryOrder.Ascending)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return prospectionHoleGroupCalculations;
    }
    public List<ProspectionHole> getProspectionHolesByGroup(String prospectionHoleGroupId) {
        List<ProspectionHole> mProspectionHoles = new ArrayList<>();

        try {
            mProspectionHoles = mobileServiceClient.getSyncTable(ProspectionHole.class)
                    .read(QueryOperations.tableName(ProspectionHole.class.getSimpleName()).field("IdProspectionHoleGroup").eq(prospectionHoleGroupId).orderBy("createdAt", QueryOrder.Ascending)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mProspectionHoles;
    }

    public List<ProspectionHole> getProspectionHolesByPHG(String prospectionHoleGroupId) {
        List<ProspectionHole> mProspections = new ArrayList<>();

        try {
            mProspections = mobileServiceClient.getSyncTable(ProspectionHole.class)
                    .read(QueryOperations.tableName(ProspectionHole.class.getSimpleName()).field("IdProspectionHoleGroup").eq(prospectionHoleGroupId).orderBy("createdAt", QueryOrder.Ascending)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mProspections;
    }

    public List<ProspectionRefPicture> getProspectionRefPictureByPHG(String prospectionHoleGroupId) {
        List<ProspectionRefPicture> mProspectionRefPictures = new ArrayList<>();

        try {
            mProspectionRefPictures = mobileServiceClient.getSyncTable(ProspectionRefPicture.class)
                    .read(QueryOperations.tableName(ProspectionRefPicture.class.getSimpleName()).field("IdProspectionHoleGroup").eq(prospectionHoleGroupId)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mProspectionRefPictures;
    }

    public List<Rod> getRodsByProspectionHole(String prospectionHoleId) {
        List<Rod> mRods = new ArrayList<>();

        try {
            mRods = mobileServiceClient.getSyncTable(Rod.class)
                    .read(QueryOperations.tableName(Rod.class.getSimpleName()).field("IdProspectionHole").eq(prospectionHoleId).orderBy("createdAt", QueryOrder.Ascending)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mRods;
    }

    @Override
    public List<QValueDiscontinuity> getQValueDiscontinuities(String qvalueId) {
        List<QValueDiscontinuity> qValueDiscontinuities = new ArrayList<>();

        try {
            qValueDiscontinuities = mobileServiceClient.getSyncTable(QValueDiscontinuity.class)
                    .read(QueryOperations.tableName(QValueDiscontinuity.class.getSimpleName()).field("IdQValue").eq(qvalueId).orderBy("discontinuity", QueryOrder.Ascending)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return qValueDiscontinuities;
    }

    @Override
    public List<QValueInputSelectionDiscontinuity> getQValueInputSelectionDiscontinuities(String qvalueselectionId) {
        List<QValueInputSelectionDiscontinuity> qValueInputSelectionDiscontinuities = new ArrayList<>();

        try {
            qValueInputSelectionDiscontinuities = mobileServiceClient.getSyncTable(QValueInputSelectionDiscontinuity.class)
                    .read(QueryOperations.tableName(QValueInputSelectionDiscontinuity.class.getSimpleName()).field("IdQValueInputSelection").eq(qvalueselectionId).orderBy("discontinuity", QueryOrder.Ascending)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return qValueInputSelectionDiscontinuities;
    }

    @Override
    public List<FormationUnit> getFormationUnitByProject(String projectId) {
        List<FormationUnit> formationUnits = new ArrayList<>();

        try {
            formationUnits = mobileServiceClient.getSyncTable(FormationUnit.class)
                    .read(QueryOperations.tableName(FormationUnit.class.getSimpleName()).field("IdProject").eq(projectId).orderBy("Code", QueryOrder.Ascending)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return formationUnits;
    }

    @Override
    public List<RockQuality> getRockQualityByProject(String projectId) {
        List<RockQuality> rockQualities = new ArrayList<>();

        try {
            rockQualities = mobileServiceClient.getSyncTable(RockQuality.class)
                    .read(QueryOperations.tableName(RockQuality.class.getSimpleName()).field("IdProject").eq(projectId).orderBy("Code", QueryOrder.Ascending)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return rockQualities;
    }

    @Override
    public Tunnel getTunnelByFace(String faceId) {
        Tunnel mTunnel = null;
        Face mFace = null;

        try {
            mFace = mobileServiceClient.getSyncTable(Face.class)
                    .lookUp(faceId)
                    .get();

            mTunnel = mobileServiceClient.getSyncTable(Tunnel.class)
                    .lookUp(mFace.getIdTunnel())
                    .get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mTunnel;
    }

    @Override
    public Face getFaceById(String faceId) {
        Face mFace = null;

        try {
            mFace = mobileServiceClient.getSyncTable(Face.class)
                    .lookUp(faceId)
                    .get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mFace;
    }

    @Override
    public List<ProjectMappingInput> getMappingInputsByProject(String projectId) {
        List<ProjectMappingInput> projectMappingInputs = new ArrayList<>();

        try {
            projectMappingInputs = mobileServiceClient.getSyncTable(ProjectMappingInput.class)
                    .read(QueryOperations.tableName(ProjectMappingInput.class.getSimpleName()).field("IdProject").eq(projectId)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return projectMappingInputs;
    }

    @Override
    public List<MappingInput> getMappingInputs() {
        List<MappingInput> mappingInputs = new ArrayList<>();

        try {
            mappingInputs = mobileServiceClient.getSyncTable(MappingInput.class).read(QueryOperations.tableName(MappingInput.class.getSimpleName()).orderBy("Code", QueryOrder.Ascending)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return mappingInputs;
    }

    @Override
    public Project getProjectsByMapping(String mappingId) {
        Project project = null;

        try {
            Mapping m = mobileServiceClient.getSyncTable(Mapping.class)
                    .lookUp(mappingId)
                    .get();

            Tunnel t = getTunnelByFace(m.getIdFace());
            project = getProjectsById(t.getIdProject());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return project;
    }
}
