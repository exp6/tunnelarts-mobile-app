package cl.skava.mobile.tunnelapp.data.repository;

import android.content.Context;
import android.provider.Telephony;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.blobs.BlobStorageService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.blobs.syncprocess.ModuleStorageService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreServiceHelper;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.geoclassifier.MappingListInteractor;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.MappingInput;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.ProjectMappingInput;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.ExpandedTunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Gsi;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Particularities;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Picture;
import cl.skava.mobile.tunnelapp.domain.model.mapping.StereonetPicture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.repository.BaseRepository;
import cl.skava.mobile.tunnelapp.domain.repository.MappingRepository;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.MainPresenter;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public class MappingRepositoryImpl implements MappingRepository, BaseRepository {

    private List<Mapping> mappings = new ArrayList<>();
    private CloudStoreService cloudStoreService;
    private BlobStorageService blobStorageService;
    private MappingListInteractor mInteractor;
    private Context mActivity;
    private String mFaceId;
    private final int STORAGE_SERVICE = 3;
    private final String lang = Locale.getDefault().getLanguage();
    private List<MappingInputModule> mappingInputModules;
    private ModuleStorageService moduleStorageService;
    private List<MappingInput> mappingInputsModulesEnabled;
    private List<String> storageOrder;

    public MappingRepositoryImpl(Context activity, String faceId) {
        cloudStoreService = new CloudStoreService(activity);
        mActivity = activity;
        blobStorageService = new BlobStorageService(activity.getExternalFilesDir(null), this);
        mFaceId = faceId;
        moduleStorageService = new ModuleStorageService(blobStorageService, cloudStoreService, this);
    }

//    public MappingRepositoryImpl(Context activity, String faceId, User user) {
//        cloudStoreService = new CloudStoreService(activity, this, user);
//    }

    @Override
    public CloudStoreService getCloudStoreService() {
        return cloudStoreService;
    }

    public void setCloudStoreService(CloudStoreService cloudStoreService) {
        this.cloudStoreService = cloudStoreService;
    }

    @Override
    public boolean insert(Mapping mapping) {
        mapping.setId(UUID.randomUUID().toString().toUpperCase());
        mapping.setCompleted(false);
        mapping.setFinished(false);
        mappings.add(mapping);
        boolean status = cloudStoreService.persistEntity(Mapping.class, mapping);
        updateFaceProgress(mapping);

        return status;
    }

    private void updateFaceProgress(Mapping mapping) {
        Face face = cloudStoreService.getFaceById(mapping.getIdFace());
        List<Mapping> mappings = cloudStoreService.getMappingsByFace(face.getId());
        Double progress = 0.0;

        for(Mapping m : mappings) {
            Double mappingProgress = m.getChainageEnd() - m.getChainageStart();
            if(mappingProgress < 0) mappingProgress = (-1)*mappingProgress;

            progress += mappingProgress;
        }

        face.setProgress(progress.floatValue());
        cloudStoreService.updateEntity(Face.class, face);
    }

    @Override
    public boolean update(Mapping mapping) {
        boolean status = cloudStoreService.updateEntity(Mapping.class, mapping);
        updateFaceProgress(mapping);

        return status;
    }

    @Override
    public Mapping get(Object id) {
        return null;
    }

    @Override
    public boolean delete(Mapping mapping) {
        mappings.remove(mapping);
        boolean status = cloudStoreService.deleteEntity(Mapping.class, mapping);
        updateFaceProgress(mapping);

        return status;
    }

    @Override
    public List<Mapping> getList(String faceId) {
        mappings = cloudStoreService.getMappingsByFace(faceId);
        return mappings;
    }

    @Override
    public List<Mapping> getAll() {
        return mappings;
    }

    @Override
    public void saveData() {
        cloudStoreService.setmCallback(this);
        cloudStoreService.pushAllData();
    }

    @Override
    public void setInteractor(MappingListInteractor interactor) {
        mInteractor = interactor;
    }

    @Override
    public void syncDataModule(User user) {
        //Sync Table Data related
//        mappingInputModules = inputs;
        cloudStoreService = new CloudStoreService(mActivity, this, user);
        cloudStoreService.setSyncFaceProgress(true);

        List<Mapping> mappings = cloudStoreService.getMappingsByFace(mFaceId);
        List<Mapping> finalMappings = new ArrayList<>();

        for(Mapping m : mappings) {
//            if(m.getFinished()) continue;

            finalMappings.add(m);
        }

        if(finalMappings.isEmpty())
            onSyncResult(1);
        else {
            List<Class> entityTables = CloudStoreServiceHelper.getMappingInputEntities();
            String progressLabel = lang.equals("es") ? "Sincronizando Datos Caracterización..." : "Synchronizing Characterization Data...";
            cloudStoreService.initSyncProgress(progressLabel, entityTables.size());
            cloudStoreService.setSelectedMappings(finalMappings);
            cloudStoreService.syncMappingsById(finalMappings);
        }
    }

    @Override
    public void syncData(User user) {
        //Sync Table Data related
        cloudStoreService = new CloudStoreService(mActivity, this, user);
        Tunnel t = cloudStoreService.getTunnelByFace(mFaceId);
        List<Tunnel> tunnels = new ArrayList<>();
        tunnels.add(t);
        cloudStoreService.setSyncFaceProgress(true);
        cloudStoreService.syncFaces(tunnels);
//        cloudStoreService.syncGeoCharacterizerData();
    }

    @Override
    public void syncStorageData(Boolean syncAll, int moduleCode) {
        //Sync Storage Files related
        blobStorageService = new BlobStorageService(mActivity.getExternalFilesDir(null), this);
        mappings = cloudStoreService.getMappingsByFace(mFaceId);

        List<Mapping> mappingsAux = new ArrayList<>();
        for(Mapping m : mappings) {
            if(!m.getFinished()) mappingsAux.add(m);
        }

        mappings = mappingsAux;

        Tunnel tunnel =  cloudStoreService.getTunnelByFace(mFaceId);
        Project project = cloudStoreService.getProjectsById(tunnel.getIdProject());
        List<ProjectMappingInput> projectMappingInputs = cloudStoreService.getMappingInputsByProject(project.getId());
        List<MappingInput> mappingInputsModules = cloudStoreService.getMappingInputs();
        mappingInputsModulesEnabled = cloudStoreService.findMappingInputsEnabled(projectMappingInputs, mappingInputsModules);

        String[] storageModulesEnabled = new String[4];
        for(MappingInput mi : mappingInputsModulesEnabled) {
            switch (mi.getLabel().trim()) {
                case "PIC":
                    storageModulesEnabled[0] = mi.getLabel().trim();
                    break;
                case "EXPT":
                    storageModulesEnabled[1] = mi.getLabel().trim();
                    break;
                case "AINFO":
                    storageModulesEnabled[2] = mi.getLabel().trim();
                    break;
                case "DISC":
                    storageModulesEnabled[3] = mi.getLabel().trim();
                    break;
            }
        }

        storageOrder = new ArrayList<>();
        for(int i = 0; i < storageModulesEnabled.length; i++)
            if(storageModulesEnabled[i] != null) storageOrder.add(storageModulesEnabled[i]);

        if(storageOrder.isEmpty()) {
            if(syncAll) cloudStoreService.syncForecastingData();
            else mInteractor.onSyncSuccess();
        }
        else {
            switch(storageOrder.get(0)) {
                case "PIC":
                    moduleStorageService.syncPictureFiles(syncAll, mappings);
                    break;
                case "EXPT":
                    moduleStorageService.syncExpandedTunnelFiles(syncAll, mappings);
                    break;
                case "AINFO":
                    moduleStorageService.syncAdditionalInfoFiles(mappings);
                    break;
                case "DISC":
                    moduleStorageService.syncStereonetFiles(mappings);
                    break;
            }
        }
    }

    @Override
    public void onSyncStorageResult(int code, Boolean syncAll) {
        switch(code) {
            case 1:
                moduleStorageService.syncPicturesTableData();
                break;
            case 2:
                if(storageOrder.size() <= 1) {
                    if(syncAll) cloudStoreService.syncForecastingData();
                    else mInteractor.onSyncSuccess();
                }
                else {
                    switch(storageOrder.get(1)) {
                        case "EXPT":
                            moduleStorageService.syncExpandedTunnelFiles(syncAll, mappings);
                            break;
                        case "AINFO":
                            moduleStorageService.syncAdditionalInfoFiles(mappings);
                            break;
                        case "DISC":
                            moduleStorageService.syncStereonetFiles(mappings);
                            break;
                    }
                }
                break;
            case 3:
                moduleStorageService.syncExpandedTunnelTableData();
                break;
            case 10:
                if(storageOrder.size() <= 2) {
                    if(syncAll) cloudStoreService.syncForecastingData();
                    else mInteractor.onSyncSuccess();
                }
                else {
                    moduleStorageService.syncAdditionalInfoFiles(mappings);
                }
                break;
            case 11:
                cloudStoreService.syncParticularities(Particularities.class);
                break;
            case 5:
                if(storageOrder.size() <= 3) {
                    if(syncAll) cloudStoreService.syncForecastingData();
                    else mInteractor.onSyncSuccess();
                }
                else {
                    moduleStorageService.syncStereonetFiles(mappings);
                }
                break;
            case 6:
                moduleStorageService.syncStereonetTableData();
                break;
            default:
                if(syncAll) cloudStoreService.syncForecastingData();
                else mInteractor.onSyncSuccess();
                break;
        }
    }

    @Override
    public void syncBasicData(Context activity, MainPresenter presenter, User user) {

    }

    @Override
    public void initRepository(Context activity, MainPresenter callback, User user) {

    }

    @Override
    public void setSyncProgress(Integer percentage) {
        mInteractor.setSyncProgressPercentaje(percentage);
    }

    @Override
    public void notifySyncSegment(String msg) {
        mInteractor.notifySyncSegment(msg);
    }

    @Override
    public void onSyncResult(int code) {
        mInteractor.onSyncSuccess();
    }

    @Override
    public void onNetworkError(String msg) {
        mInteractor.onSyncError(msg);
    }
}
