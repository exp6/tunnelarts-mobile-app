package cl.skava.mobile.tunnelapp.domain.model.prospection;

import java.util.Date;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class ProspectionHole {

    private String id;
    private String name;
    private Float angle;
    private String idProspectionHoleGroup;
    private Date createdAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getAngle() {
        return angle;
    }

    public void setAngle(Float angle) {
        this.angle = angle;
    }

    public String getIdProspectionHoleGroup() {
        return idProspectionHoleGroup;
    }

    public void setIdProspectionHoleGroup(String idProspectionHoleGroup) {
        this.idProspectionHoleGroup = idProspectionHoleGroup;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "ProspectionHole{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", angle=" + angle +
                ", idProspectionHoleGroup='" + idProspectionHoleGroup + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}
