package cl.skava.mobile.tunnelapp.domain.model.prospection;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Jose Ignacio Vera on 12-08-2016.
 */
public class ProspectionHoleGroup implements Serializable {

    private String id;
    private String name;
    private String idFace;
    private String idUser;
    private Float qValueSelected;
    private Date createdAt;
    private String idMapping;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdFace() {
        return idFace;
    }

    public void setIdFace(String idFace) {
        this.idFace = idFace;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public Float getqValueSelected() {
        return qValueSelected;
    }

    public void setqValueSelected(Float qValueSelected) {
        this.qValueSelected = qValueSelected;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    @Override
    public String toString() {
        return "ProspectionHoleGroup{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", idFace='" + idFace + '\'' +
                ", idUser='" + idUser + '\'' +
                ", qValueSelected=" + qValueSelected +
                ", createdAt=" + createdAt +
                ", idMapping='" + idMapping + '\'' +
                '}';
    }
}
