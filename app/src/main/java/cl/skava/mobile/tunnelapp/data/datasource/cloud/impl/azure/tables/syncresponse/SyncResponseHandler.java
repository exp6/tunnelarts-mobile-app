package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;

/**
 * Created by Carlos Vergara on 20-03-2017.
 */

public interface SyncResponseHandler {
    public void handleResponseFromService(CloudMobileService cloudMobileService);
}
