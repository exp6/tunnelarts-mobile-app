package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.executor.impl.ThreadExecutor;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SelectionDetailLists;
import cl.skava.mobile.tunnelapp.presentation.presenters.impl.geoclassifier.GeoClassifierPresenterImpl;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.geoclassifier.GeoClassifierPresenter;
import cl.skava.mobile.tunnelapp.threading.MainThreadImpl;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public class GeoClassifierFragment extends Fragment
        implements GeoClassifierPresenter.View,
        MappingInputListFragment.OnModuleItemListener,
        MappingListFragment.OnMappingItemListener {

    private static final String TAG = "GeoClassifierFragment";
    protected List<Mapping> mMappingDataset;
    protected List<MappingInputModule> mMappingInputModuleDataset;
    private GeoClassifierPresenter mPresenter;

    private Double maxMappingKmValue = 0.0;
    private Integer selectedValue = 0;

    //User Selection Context
    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Module selectedModule;

    private Mapping selectedMapping;

    private OnMappingItemListListener mCallback;

    public interface OnMappingItemListListener {
        void userMappingInputsLoaded();
        void initMappingInputActivity(Project project,
                                      Tunnel tunnel,
                                      Face face,
                                      Module module,
                                      Mapping mapping,
                                      MappingInputModule mappingInputModule,
                                      List<MappingInputModule> mappingInputModuleDataset,
                                      List<Mapping> mappingDataset,
                                      List<Integer> metaPositions);
        void onSyncFinish();
        void hideDelete();
        void hideUpload();
        void hideEdit();
        void showDelete();
        void showUpload();
        void showEdit();
    }

    private View dialogContent;
    private AlertDialog alertDialog;

    private ProgressDialog uiProgressDialog;

    private User mUser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallback = (OnMappingItemListListener) getActivity();

        if (savedInstanceState == null) {
            initDatasources();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier, container, false);
        rootView.setTag(TAG);

        dialogContent = inflater.inflate(R.layout.fragment_geoclassifier_mapping_new, null);

        FloatingActionButton myFab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                newMapping();
            }
        });

        return rootView;
    }

    private void newMapping() {

        if(alertDialog == null)
            alertDialog = new AlertDialog.Builder(getActivity()).create();

        alertDialog.setTitle(getResources().getString(R.string.characterization_label_new_mapping));
        alertDialog.setMessage(getResources().getString(R.string.characterization_label_value_final_chainage));
        // Alert dialog button
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.system_label_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        EditText input = (EditText) dialogContent.findViewById(R.id.input_chainage);
                        EditText inputDirection = (EditText) dialogContent.findViewById(R.id.input_direction);
//                        EditText inputSlope = (EditText) dialogContent.findViewById(R.id.input_slope);
//                        EditText inputRoundLength = (EditText) dialogContent.findViewById(R.id.input_round_length);

                        Mapping mapping = new Mapping();
                        mapping.setChainageStart(maxMappingKmValue);

                        double finalChainage = new Double(input.getText().toString().isEmpty() ? "0" : input.getText().toString());

                        mapping.setChainageEnd(finalChainage);
                        mapping.setIdFace(selectedFace.getId());
                        mapping.setIdUser(mUser.getId());
                        mapping.setCreatedAt(new Date());
                        mapping.setMappingTime(new Date());
//                        mapping.setDirection(Float.parseFloat(inputDirection.getText().toString().isEmpty() ? "0" : inputDirection.getText().toString()));
//                        mapping.setSlope(Float.parseFloat(inputSlope.getText().toString().isEmpty() ? "0" : inputSlope.getText().toString()));

                        double roundLength =  finalChainage - maxMappingKmValue;

                        mapping.setRoundLength(new Float(roundLength));
                        mPresenter.createMapping(mapping);
                        dialog.dismiss();
                    }
                });

        TextView tv1 = (TextView) dialogContent.findViewById(R.id.textView2);
        tv1.setText(getResources().getString(R.string.characterization_label_last_chainage_value) + maxMappingKmValue);

        alertDialog.setView(dialogContent);
        alertDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.resume();
    }

    private void initDatasources() {
        mPresenter = new GeoClassifierPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                getActivity(),
                selectedFace.getId()
        );
        mMappingDataset = new ArrayList<>();
        mMappingInputModuleDataset = new ArrayList<>();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void updateFragment(Fragment fragment, int container) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(container, fragment);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void displayMappingList(List<Mapping> mappings) {
        mMappingDataset = mappings;

        if(mMappingDataset.isEmpty()) {
            mCallback.hideDelete();
            mCallback.hideUpload();
            mCallback.hideEdit();
        }
        else {
            if(mMappingDataset.get(0).getFinished()) {
                mCallback.hideDelete();
                mCallback.hideUpload();
                mCallback.hideEdit();
            }
        }

        sortDataset();
        setMaxMappingKmValue(mMappingDataset);

        MappingListFragment fragment = (MappingListFragment) getChildFragmentManager().findFragmentById(R.id.mapping_list);

        if(!mappings.isEmpty())
            selectedMapping = mMappingDataset.get(0);

        MappingInputListFragment f1 = (MappingInputListFragment) getChildFragmentManager().findFragmentById(R.id.mapping_input_list);
//        if(f1 != null && selectedMapping != null) {
        if(f1 != null) {
            f1.setSelectedMapping(selectedMapping);
        }

        if (fragment == null) {
            fragment = new MappingListFragment();
            fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule);
            fragment.setMMappingDataset(mMappingDataset);
            fragment.onAttachFragment(this);
            updateFragment(fragment, R.id.mapping_list);
        }
        else {
            fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule);
            fragment.setMMappingDataset(mMappingDataset);
            fragment.updateItemList();
        }
    }

    private void setMaxMappingKmValue(List<Mapping> mappings) {
        if(!mappings.isEmpty()) {
            maxMappingKmValue = mappings.get(0).getChainageEnd();
        }
        else {
            maxMappingKmValue = (selectedFace.getRefChainage() != null ? (!selectedFace.getRefChainage().isEmpty() ? new Double(selectedFace.getRefChainage()) : 0.0) : 0.0);
        }
    }

    @Override
    public void displayMappingInputList(List<MappingInputModule> mappingInputModules) {
        mMappingInputModuleDataset = mappingInputModules;
        MappingInputListFragment fragment = (MappingInputListFragment) getChildFragmentManager().findFragmentById(R.id.mapping_input_list);

        if(mMappingDataset.isEmpty()) {
            selectedMapping = null;
        }

        if (fragment == null) {
            fragment = new MappingInputListFragment();
            fragment.onAttachFragment(this);
            fragment.setSelectedMapping(selectedMapping);
            fragment.setMMappingInputDataset(mMappingInputModuleDataset);
            updateFragment(fragment, R.id.mapping_input_list);
        }
        else {
            fragment.setSelectedMapping(selectedMapping);
            fragment.setMMappingInputDataset(mMappingInputModuleDataset);
            fragment.updateItemList();
        }

        if(selectedMapping != null) {
            if(selectedMapping.getFinished()) {
                mCallback.hideUpload();
                mCallback.hideDelete();
                mCallback.hideEdit();
            }
            else {
                mCallback.showUpload();
                mCallback.showDelete();
                mCallback.showEdit();
            }
        }
    }

    @Override
    public void syncDataResult(boolean success, String msg) {
        uiProgressDialog.dismiss();

        if(success) {
            mCallback.onSyncFinish();
        }
        else {
            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                public void run() {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getResources().getString(R.string.system_label_failure_data_sync))
                            .setPositiveButton(getResources().getString(R.string.system_label_button_retry), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    syncData();
                                }
                            })
                            .setNegativeButton(getResources().getString(R.string.system_label_button_skip), new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            });
        }
    }

    @Override
    public void onSyncProgressPercentaje(Integer percentaje) {
        uiProgressDialog.setProgress(percentaje);
    }

    @Override
    public void notifySyncSegment(String msg) {
        uiProgressDialog.setMessage(msg);
    }

    @Override
    public void onExportFinish(boolean success) {
        uiProgressDialog.dismiss();

        Handler h = new Handler(Looper.getMainLooper());
        h.post(new Runnable() {
            public void run() {
                new AlertDialog.Builder(getActivity())
                        .setTitle(getResources().getString(R.string.system_label_data_export_finished))
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .show();
            }
        });
    }

    public void setSelectionContext(Project project,
                                    Tunnel tunnel,
                                    Face face,
                                    Module module,
                                    User user) {
        selectedProject = project;
        selectedTunnel = tunnel;
        selectedFace = face;
        selectedModule = module;
        mUser = user;
    }

    @Override
    public void showProgress() {}

    @Override
    public void hideProgress() {}

    @Override
    public void showError(String message) {}

    @Override
    public void onModuleSelected(List<Integer> metaPositions) {
        int position = metaPositions.get(0).intValue();
        MappingInputModule mappingInputModule = mMappingInputModuleDataset.get(position);
        mCallback.initMappingInputActivity(selectedProject, selectedTunnel, selectedFace, selectedModule, selectedMapping, mappingInputModule, mMappingInputModuleDataset, mMappingDataset, metaPositions);
    }

    @Override
    public void userModulesLoaded() {
        mCallback.userMappingInputsLoaded();
    }

//    @Override
//    public void getSelectedMapping() {
//        MappingInputListFragment fragment = (MappingInputListFragment) getChildFragmentManager().findFragmentById(R.id.mapping_input_list);
//        fragment.setSelectedMapping(selectedMapping);
//    }

    @Override
    public void onMappingSelected(int position) {
        selectedMapping = mMappingDataset.get(position);
//        MappingInputListFragment fragment = (MappingInputListFragment) getChildFragmentManager().findFragmentById(R.id.mapping_input_list);
//        fragment.setSelectedMapping(selectedMapping);

//        if(selectedMapping.getFinished()) {
//            mCallback.hideUpload();
//            mCallback.hideDelete();
//            mCallback.hideEdit();
//        }
//        else {
//            mCallback.showUpload();
//            mCallback.showDelete();
//            mCallback.showEdit();
//        }

        mPresenter.getInputsByMapping(selectedMapping);
    }

    private void sortDataset() {
        List<Mapping> newSortList = new ArrayList<>();

        for(int i = mMappingDataset.size()-1; i >= 0; --i) {
            newSortList.add(mMappingDataset.get(i));
        }
        mMappingDataset = newSortList;
    }

    public void saveData() {
        if(isNetworkAvailable()) {
            uiProgressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_Dark_Dialog);
            uiProgressDialog.setMessage(getResources().getString(R.string.system_label_uploading_data));
            uiProgressDialog.setCancelable(false);
            uiProgressDialog.show();

            mPresenter.saveMappings();
        }
        else {
            final String alertTitle = getResources().getString(R.string.system_label_no_internet);
            final Context c = getActivity();
            final String msg1 = getResources().getString(R.string.system_label_no_internet_msg);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                public void run() {
                    new android.support.v7.app.AlertDialog.Builder(c)
                            .setTitle(alertTitle)
                            .setMessage(msg1)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            })
                            .show();
                }
            });
        }
    }

    public void syncData() {
        if(isNetworkAvailable()) {
            uiProgressDialog=new ProgressDialog(getActivity());
            uiProgressDialog.setMessage(getResources().getString(R.string.system_label_sync_user_data));
            uiProgressDialog.setCancelable(false);
            uiProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            uiProgressDialog.setIndeterminate(false);
            uiProgressDialog.setProgress(0);
            uiProgressDialog.show();

            mPresenter.syncData(mUser);
        }
        else {
            final String alertTitle = getResources().getString(R.string.system_label_no_internet);
            final Context c = getActivity();
            final String msg1 = getResources().getString(R.string.system_label_no_internet_msg);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                public void run() {
                    new android.support.v7.app.AlertDialog.Builder(c)
                            .setTitle(alertTitle)
                            .setMessage(msg1)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            })
                            .show();
                }
            });
        }
    }

    public void exportData() {
        uiProgressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_Dark_Dialog);
        uiProgressDialog.setCancelable(false);
        uiProgressDialog.setMessage(getResources().getString(R.string.system_label_exporting_data1) + getResources().getString(R.string.characterization_label_module_name) + getResources().getString(R.string.system_label_exporting_data2));
        uiProgressDialog.show();

        SelectionDetailLists s = getListsData();

        mPresenter.exportData(selectedProject, selectedTunnel, selectedFace, Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS).toString(), s);
    }

    private SelectionDetailLists getListsData() {
        SelectionDetailLists s = new SelectionDetailLists();
        s.setStructure(getResources().getStringArray(R.array.structure_dropdown_arrays));
        s.setJointing(getResources().getStringArray(R.array.jointing_dropdown_arrays));
        s.setWaterDropdown(getResources().getStringArray(R.array.water_dropdown_arrays));
        s.setWaterQuality(getResources().getStringArray(R.array.water_quality_dropdown_arrays));
        s.setGeneticGroup(getResources().getStringArray(R.array.genetic_group_dropdown_arrays));
        s.setStrength(getResources().getStringArray(R.array.strength_dropdown_arrays));
        s.setColourValue(getResources().getStringArray(R.array.colour_value_dropdown_arrays));
        s.setColourChroma(getResources().getStringArray(R.array.colour_chroma_dropdown_arrays));
        s.setColourHue(getResources().getStringArray(R.array.colour_hue_dropdown_arrays));
        s.setTexture(getResources().getStringArray(R.array.texture_dropdown_arrays));
        s.setGrainSize(getResources().getStringArray(R.array.grain_size_dropdown_arrays));
        s.setType(getResources().getStringArray(R.array.type_dropdown_arrays));
        s.setRelevance(getResources().getStringArray(R.array.relevance_dropdown_arrays));
        s.setSpacing(getResources().getStringArray(R.array.spacing_dropdown_arrays));
        s.setPersistence(getResources().getStringArray(R.array.persistence_dropdown_arrays));
        s.setOpening(getResources().getStringArray(R.array.opening_dropdown_arrays));
        s.setRoughness(getResources().getStringArray(R.array.roughness_dropdown_arrays));
        s.setInfilling(getResources().getStringArray(R.array.infilling_dropdown_arrays));
        s.setInfillingType(getResources().getStringArray(R.array.infilling_type_dropdown_arrays));
        s.setWeathering(getResources().getStringArray(R.array.weathering_dropdown_arrays));
        s.setBlockSize(getResources().getStringArray(R.array.block_size_dropdown_arrays));
        s.setBlockShape(getResources().getStringArray(R.array.block_shape_dropdown_arrays));
        s.setBlockShape2(getResources().getStringArray(R.array.block_shape_2_dropdown_arrays));
        s.setFaceDistance(getResources().getStringArray(R.array.face_distance_dropdown_arrays));
        s.setResponsible(getResources().getStringArray(R.array.responsible_dropdown_arrays));
        s.setReason(getResources().getStringArray(R.array.reason_dropdown_arrays));
        s.setLight(getResources().getStringArray(R.array.light_dropdown_arrays));
        s.setAir(getResources().getStringArray(R.array.air_dropdown_arrays));
        s.setYesNo(getResources().getStringArray(R.array.yes_no_dropdown_arrays));
        s.setIgneousPlutonic(getResources().getStringArray(R.array.igneous_plutonic_dropdown_arrays));
        s.setIgneousVolcanic(getResources().getStringArray(R.array.igneous_volcanic_dropdown_arrays));
        s.setPyroclastic(getResources().getStringArray(R.array.pyroclastic_dropdown_arrays));
        s.setSedimentaryClastic(getResources().getStringArray(R.array.sedimentary_clastic_dropdown_arrays));
        s.setSedimentaryChemical(getResources().getStringArray(R.array.sedimentary_chemical_dropdown_arrays));
        s.setSenseOfMovement(getResources().getStringArray(R.array.sense_of_movement_dropdown_arrays));
        s.setTypeOfBolt(getResources().getStringArray(R.array.type_of_bolt_dropdown_arrays));
        s.setBoltsDiameter(getResources().getStringArray(R.array.bolts_diameter_dropdown_arrays));
        s.setShotcrete(getResources().getStringArray(R.array.shotcrete_dropdown_arrays));
        s.setLocation(getResources().getStringArray(R.array.location_dropdown_arrays));
        s.setMetamorphic(getResources().getStringArray(R.array.metamorphic_dropdown_arrays));

        return s;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void deleteSelectedItem() {
        mPresenter.deleteMapping(selectedMapping);
    }

    public void editSelectedItem() {
        editMapping();
    }

    private void editMapping() {
        EditText input = (EditText) dialogContent.findViewById(R.id.input_chainage);
        input.setText(selectedMapping.getChainageEnd().toString());

        setMaxMappingKmValue(mMappingDataset);

        if(alertDialog == null)
            alertDialog = new AlertDialog.Builder(getActivity()).create();

        alertDialog.setTitle(getResources().getString(R.string.characterization_label_edit_mapping));
        alertDialog.setMessage(getResources().getString(R.string.characterization_label_edit_mapping_msg));
        // Alert dialog button
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.system_label_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Alert dialog action goes here
                        // onClick button code here
                        dialog.dismiss();// use dismiss to cancel alert dialog
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.system_label_save),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        EditText input = (EditText) dialogContent.findViewById(R.id.input_chainage);
                        selectedMapping.setChainageEnd(new Double(input.getText().toString()));
                        mPresenter.updateMapping(selectedMapping);
                        dialog.dismiss();
                    }
                });

        TextView tv1 = (TextView) dialogContent.findViewById(R.id.textView2);
        tv1.setText(getResources().getString(R.string.characterization_label_last_chainage_value) + selectedMapping.getChainageStart());

        alertDialog.setView(dialogContent);
        alertDialog.show();
    }
}
