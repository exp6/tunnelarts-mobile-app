package cl.skava.mobile.tunnelapp.data.datasource.cloud.base;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.MappingInput;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.ProjectMappingInput;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.UserProject;
import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInput;
import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInputType;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueDiscontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputSelectionDiscontinuity;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;

/**
 * Created by Jose Ignacio Vera on 23-09-2016.
 */
public interface LocalStoreService {

    List<Project> getProjects();
    List<UserProject> getProjectsByUser(String userId);
    Project getProjectsById(String projectId);
    Client getClientById(String clientId);
    List<Tunnel> getTunnelsByProject(String projectId);
    List<Face> getFaces();
    List<Face> getFacesByTunnel(String tunnelId);
    List<Mapping> getMappingsByFace(String faceId);
    Mapping getMappingById(String mappingId);
    DataInput getMappingInput(String mappingId, Class c);
    DataInput getSubMappingInput(String subInputId, Class c);
    List<? extends DataInput> getMappingInputs(String mappingId, Class c);
    List<? extends DataInput> getMappingInputPictures(String mappingId, Class c);
    List<? extends DataInputType> getMappingInputTypes(Class c);
    List<? extends DataInputType> getMappingInputTypes(Class c, String idType, String column);
    List<ProspectionHoleGroup> getProspectionHoleGroupsByFace(String faceId);
    List<ProspectionHole> getProspectionHolesByGroup(String prospectionHoleGroupId);
    List<ProspectionHole> getProspectionHolesByPHG(String prospectionHoleGroupId);
    List<Rod> getRodsByProspectionHole(String prospectionHoleId);
    List<QValueDiscontinuity> getQValueDiscontinuities(String qvalueId);
    List<QValueInputSelectionDiscontinuity> getQValueInputSelectionDiscontinuities(String qvalueselectionId);
    List<FormationUnit> getFormationUnitByProject(String projectId);
    List<RockQuality> getRockQualityByProject(String projectId);
    Tunnel getTunnelByFace(String faceId);
    Face getFaceById(String faceId);
    List<ProjectMappingInput> getMappingInputsByProject(String projectId);
    List<MappingInput> getMappingInputs();
    Project getProjectsByMapping(String mappingId);
}
