package cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.MappingListFragment;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public class MappingListAdapter extends RecyclerView.Adapter<MappingListAdapter.ViewHolder> {

    private static final String TAG = "MappingListAdapter";
    protected List<Mapping> mMappingDataset;
    private MappingListFragment.OnMappingItemListener mCallback;

    private int selectedItem = 0;
    ViewHolder viewHolderPool[];
    protected RecyclerView mRecyclerView;

    private final ChainageNumberFormat chainageNumberFormat = new ChainageNumberFormat();

    public MappingListAdapter(List<Mapping> dataSet,
                              MappingListFragment.OnMappingItemListener callback,
                              RecyclerView recyclerView) {
        mMappingDataset = dataSet;
        mCallback = callback;
        viewHolderPool = new ViewHolder[mMappingDataset.size()];
        mRecyclerView = recyclerView;
        setHasStableIds(true);
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedItem = getLayoutPosition();
                    mCallback.onMappingSelected(selectedItem);

                    for (ViewHolder vh : viewHolderPool) {
                        if (vh == null) {
                            continue;
                        }
                        vh.itemView.setSelected(false);
                        vh.itemView.setBackgroundColor(Color.TRANSPARENT);
                    }
                    notifyDataSetChanged();
                    mRecyclerView.smoothScrollToPosition(selectedItem);
                }
            });

            textView = (TextView) v.findViewById(R.id.pk_name);
        }

        public TextView getTextView() {
            return textView;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_geoclassifier_mappings_row_item, parent, false);
        holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Mapping mapping = mMappingDataset.get(position);

        holder.getTextView().setText(chainageNumberFormat.format(mapping.getChainageStart()) + " - " + chainageNumberFormat.format(mapping.getChainageEnd()));
        ImageView lock = ((ImageView) holder.itemView.findViewById(R.id.locked_icon));

        if(mapping.getFinished()) lock.setImageResource(R.drawable.ic_lock);

        if (viewHolderPool[position] == null)
            viewHolderPool[position] = holder;

        holder.itemView.setSelected(selectedItem == position);
        holder.itemView.setBackgroundColor(holder.itemView.isSelected() ? Color.LTGRAY : Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return mMappingDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class ChainageNumberFormat extends DecimalFormat {

        public ChainageNumberFormat() {
            DecimalFormatSymbols custom = new DecimalFormatSymbols();
            custom.setGroupingSeparator('+');
            custom.setDecimalSeparator(',');
            setMinimumFractionDigits(2);
            setMaximumFractionDigits(2);

            setDecimalFormatSymbols(custom);
        }
    }
}
