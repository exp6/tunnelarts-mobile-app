package cl.skava.mobile.tunnelapp.domain.model.mapping.rmr;

import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInput;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class Rmr extends DataInput {
    private String id;
    private Float strength;
    private Float rqd;
    private Float spacing;
    private Float persistence;
    private Float opening;
    private Float roughness;
    private Float infilling;
    private Float weathering;
    private Float groundwater;
    private Float orientation;
    private Integer rockQualityCode;
    private String idMapping;
    private Boolean completed;
    private Float condition;
    private Boolean conditionEnabled;

    public Rmr(String id, Float strength, Float rqd, Float spacing, Float persistence, Float opening, Float roughness, Float infilling, Float weathering, Float groundwater, Float orientation, Integer rockQualityCode, String idMapping, Boolean completed, Float condition, Boolean conditionEnabled) {
        this.id = id;
        this.strength = strength;
        this.rqd = rqd;
        this.spacing = spacing;
        this.persistence = persistence;
        this.opening = opening;
        this.roughness = roughness;
        this.infilling = infilling;
        this.weathering = weathering;
        this.groundwater = groundwater;
        this.orientation = orientation;
        this.rockQualityCode = rockQualityCode;
        this.idMapping = idMapping;
        this.completed = completed;
        this.condition = condition;
        this.conditionEnabled = conditionEnabled;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Float getStrength() {
        return strength;
    }

    public void setStrength(Float strength) {
        this.strength = strength;
    }

    public Float getRqd() {
        return rqd;
    }

    public void setRqd(Float rqd) {
        this.rqd = rqd;
    }

    public Float getSpacing() {
        return spacing;
    }

    public void setSpacing(Float spacing) {
        this.spacing = spacing;
    }

    public Float getPersistence() {
        return persistence;
    }

    public void setPersistence(Float persistence) {
        this.persistence = persistence;
    }

    public Float getOpening() {
        return opening;
    }

    public void setOpening(Float opening) {
        this.opening = opening;
    }

    public Float getRoughness() {
        return roughness;
    }

    public void setRoughness(Float roughness) {
        this.roughness = roughness;
    }

    public Float getInfilling() {
        return infilling;
    }

    public void setInfilling(Float infilling) {
        this.infilling = infilling;
    }

    public Float getWeathering() {
        return weathering;
    }

    public void setWeathering(Float weathering) {
        this.weathering = weathering;
    }

    public Float getGroundwater() {
        return groundwater;
    }

    public void setGroundwater(Float groundwater) {
        this.groundwater = groundwater;
    }

    public Float getOrientation() {
        return orientation;
    }

    public void setOrientation(Float orientation) {
        this.orientation = orientation;
    }

    public Integer getRockQualityCode() {
        return rockQualityCode;
    }

    public void setRockQualityCode(Integer rockQualityCode) {
        this.rockQualityCode = rockQualityCode;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Float getCondition() {
        return condition;
    }

    public void setCondition(Float condition) {
        this.condition = condition;
    }

    public Boolean getConditionEnabled() {
        return conditionEnabled;
    }

    public void setConditionEnabled(Boolean conditionEnabled) {
        this.conditionEnabled = conditionEnabled;
    }

    @Override
    public String toString() {
        return "Rmr{" +
                "id='" + id + '\'' +
                ", strength=" + strength +
                ", rqd=" + rqd +
                ", spacing=" + spacing +
                ", persistence=" + persistence +
                ", opening=" + opening +
                ", roughness=" + roughness +
                ", infilling=" + infilling +
                ", weathering=" + weathering +
                ", groundwater=" + groundwater +
                ", orientation=" + orientation +
                ", rockQualityCode=" + rockQualityCode +
                ", idMapping='" + idMapping + '\'' +
                ", completed=" + completed +
                ", condition=" + condition +
                ", conditionEnabled=" + conditionEnabled +
                '}';
    }
}
