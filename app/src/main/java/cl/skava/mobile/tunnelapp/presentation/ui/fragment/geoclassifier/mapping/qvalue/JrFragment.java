package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.qvalue;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;

/**
 * Created by Jose Ignacio Vera on 05-07-2016.
 */
public class JrFragment extends Fragment {

    private static final String TAG = "JrFragment";

    private List<JrGroup> jrGroups = new ArrayList<>();

    private OnJrValueListener mCallback;

    public interface OnJrValueListener {
        void OnJrSetValue(Double value, String id, int disc);
    }

    private EvaluationMethodSelectionType selectionMap;

    private Boolean mappingFinished;

//    private TabLayout tabLayout;
//
    private int discontinuity;

    private final String lang = Locale.getDefault().getLanguage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initDataset();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_qvalue_jr, container, false);
        rootView.setTag(TAG);

//        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
//        if(discontinuities > 1) initTabs();
//        else tabLayout.setVisibility(View.GONE);

        RadioGroup group = (RadioGroup) rootView.findViewById(R.id.radio_group1);
        RadioButton button;

        final RadioGroup group2 = (RadioGroup) rootView.findViewById(R.id.radio_group2);

        final TableLayout tbl1 = (TableLayout) rootView.findViewById(R.id.table1);
        final TableLayout tbl2 = (TableLayout) rootView.findViewById(R.id.table2);

        tbl1.setVisibility(View.GONE);
        tbl2.setVisibility(View.GONE);

        int i = 0;
        for(JrGroup jrGroup :  jrGroups) {
            final JrGroup jrGroup1 = jrGroup;
            button = new RadioButton(getActivity());
            button.setId(i);
            button.setText(jrGroup.getIndex().trim() + ". " + jrGroup.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    displayJrs(group2, jrGroup1);

                    if(jrGroup1.getIndex().trim().equalsIgnoreCase("a") || jrGroup1.getIndex().trim().equalsIgnoreCase("b")) {
                        tbl1.setVisibility(View.VISIBLE);
                        tbl2.setVisibility(View.GONE);
                    }

                    if(jrGroup1.getIndex().trim().equalsIgnoreCase("c")) {
                        tbl2.setVisibility(View.VISIBLE);
                        tbl1.setVisibility(View.GONE);
                    }
                }
            });
            button.setChecked(jrGroup.isChecked());
            if(jrGroup.isChecked()) {
                displayJrs(group2, jrGroup1);

                if(jrGroup1.getIndex().trim().equalsIgnoreCase("a") || jrGroup1.getIndex().trim().equalsIgnoreCase("b")) {
                    tbl1.setVisibility(View.VISIBLE);
                    tbl2.setVisibility(View.GONE);
                }

                if(jrGroup1.getIndex().trim().equalsIgnoreCase("c")) {
                    tbl2.setVisibility(View.VISIBLE);
                    tbl1.setVisibility(View.GONE);
                }
            }

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }

        return rootView;
    }

//    private void initTabs() {
//        tabLayout.removeAllTabs();
//        for(int i = 0; i < discontinuities; i++) {
//            tabLayout.addTab(tabLayout.newTab().setText("Discontinuity " + (i+1)));
//        }
//    }

    private void displayJrs(RadioGroup group, JrGroup jrGroup) {
        List<Jr> jrs = jrGroup.getJrs();
        final JrGroup jrGroup1 = jrGroup;
        group.removeAllViews();
        RadioButton button;

        int i = 0;
        for(Jr jr :  jrs) {
            final Jr jr1 = jr;

            button = new RadioButton(getActivity());
            button.setId(i);
            button.setText(jr.getIndex().trim() + " (" + jr.getStart() + (jr.getEnd() != null ? " - " + jr.getEnd() : "") + ")     " + jr.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mCallback.OnJrSetValue(jr1.getStart(), jr1.getId(), discontinuity);
                    updateMap(jrGroup1, jr1);
                }
            });
            button.setChecked(jr1.isChecked());

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }
    }

    private void updateMap(JrGroup jrGroup, Jr jr) {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        EvaluationMethodSelection input;
        for(int i = 0; i < groups.size(); i++) {
            groups.get(i).setChecked(false);
            if(jrGroup.getId().equalsIgnoreCase(groups.get(i).getId())) {
                groups.get(i).setChecked(true);
            }

            inputs = groups.get(i).getSubSelections();
            for(int j = 0; j < inputs.size(); j++) {
                input = inputs.get(j);
                input.setChecked(false);
                if(jr.getId().equalsIgnoreCase(input.getId())) {
                    input.setChecked(true);
                }
            }

            groups.get(i).setSubSelections(inputs);
        }
        selectionMap.setSelectionGroups(groups);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnJrValueListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnJrValueListener");
        }
    }

    public void setSelectionMap(EvaluationMethodSelectionType selectionMap) {
        this.selectionMap = selectionMap;
    }

    public EvaluationMethodSelectionType getSelectionMap() {
        return selectionMap;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public void setDiscontinuity(int discontinuity) {
        this.discontinuity = discontinuity;
    }

    public int getDiscontinuity() {
        return discontinuity;
    }

    private void initDataset() {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        JrGroup jrGroup;
        List<Jr> jrs;
        Jr jr;

        for(EvaluationMethodSelectionGroup e : groups) {
            jrGroup = new JrGroup();
            jrGroup.setId(e.getId());
            jrGroup.setIndex(e.getIndex());
            jrGroup.setDescription((lang.equals("es") ? e.getDescriptionEs() : e.getDescription()));
            jrGroup.setChecked(e.getChecked());
            inputs = e.getSubSelections();

            jrs = new ArrayList<>();

            for(EvaluationMethodSelection em : inputs) {
                jr = new Jr();
                jr.setId(em.getId());
                jr.setIndex(em.getIndex());
                jr.setStart(em.getStart());
                jr.setEnd(em.getEnd().doubleValue() == 0.0 ? null : em.getEnd());
                jr.setDescription((lang.equals("es") ? em.getDescriptionEs() : em.getDescription()));
                jr.setChecked(em.getChecked());
                jrs.add(jr);
            }

            jrGroup.setJrs(jrs);
            jrGroups.add(jrGroup);
        }
    }

    class Jr {
        private String id;
        private String index;
        private Double value;
        private Double start;
        private Double end;
        private String description;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public Double getStart() {
            return start;
        }

        public void setStart(Double start) {
            this.start = start;
        }

        public Double getEnd() {
            return end;
        }

        public void setEnd(Double end) {
            this.end = end;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }

    class JrGroup {
        private String id;
        private String index;
        private String description;
        private List<Jr> jrs;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<Jr> getJrs() {
            return jrs;
        }

        public void setJrs(List<Jr> jrs) {
            this.jrs = jrs;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }
}
