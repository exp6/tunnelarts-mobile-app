package cl.skava.mobile.tunnelapp.data.repository;

import android.content.Context;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.blobs.BlobStorageService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.blobs.syncprocess.ModuleStorageService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.MappingInput;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.ProjectMappingInput;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.ExpandedTunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Gsi;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Particularities;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Picture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.repository.BaseRepository;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.MainPresenter;

/**
 * Created by Jose Ignacio Vera on 25-07-2016.
 */
public class BaseRepositoryImpl implements BaseRepository {

    private MainPresenter mPresenter;
    private BlobStorageService blobStorageService;
    private CloudStoreService cloudStoreService;
    private Context mActivity;
    private List<Mapping> allMapping;
    private List<ProspectionHoleGroup> allProspectionHoleGroup;
    private ModuleStorageService moduleStorageService;
    private HashMap<String, List<String>> mappingInputsPerProject;
    private HashMap<String, List<Mapping>> mappingsPerProject;
    private Iterator<String> projectIterator;
    private HashMap<String, List<String>> storageOrderMap;
    private HashMap<String, String[]> storageModulesEnabledMap;
    private String projectIdFromIterator;
    private List<Mapping> iteratorMappings;

    @Override
    public void initRepository(Context activity, MainPresenter presenter, User user) {
        mPresenter = presenter;

        //Repository strategy
        cloudStoreService = new CloudStoreService(activity, this, user);
//        cloudStoreService.initDataRepository();
        blobStorageService = new BlobStorageService(activity.getExternalFilesDir(null), this);
        moduleStorageService = new ModuleStorageService(blobStorageService, cloudStoreService, this);

        mActivity = activity;
        cloudStoreService.syncAllData();
    }

    @Override
    public void setSyncProgress(Integer percentage) {
        mPresenter.setSyncProgressPercentaje(percentage);
    }

    @Override
    public void notifySyncSegment(String msg) {
        mPresenter.notifySyncSegment(msg);
    }

    @Override
    public void onSyncResult(int code) {
        switch(code) {
            case 1:
                mPresenter.onSyncResult(code);
                break;
            case 2:
                moduleStorageService.syncForecastingModels();
                break;
        }
    }

    @Override
    public void onNetworkError(String msg) {
        mPresenter.onNetworkError(msg);
    }

    @Override
    public void saveData() {

    }

    @Override
    public void syncData(User user) {

    }

    @Override
    public void syncStorageData(Boolean syncAll, int moduleCode) {
        //Sync Storage Files related
        List<Face> allFaces = new ArrayList<>();

        mappingInputsPerProject = new HashMap<>();
        mappingsPerProject = new HashMap<>();
        List<ProjectMappingInput> projectMappingInputs;
        List<MappingInput> mappingInputsModules = cloudStoreService.getMappingInputs();
        List<MappingInput> mappingInputsModulesEnabled;
        List<String> entityLabels;

        List<Project> allProjects = cloudStoreService.getProjects();
        List<Tunnel> allTunnels;
        List<Mapping> mappingList;
        List<Face> faceList;
        for(Project p : allProjects) {

            projectMappingInputs = cloudStoreService.getMappingInputsByProject(p.getId());
            mappingInputsModulesEnabled = cloudStoreService.findMappingInputsEnabled(projectMappingInputs, mappingInputsModules);
            entityLabels = new ArrayList<>();
            for(MappingInput mi : mappingInputsModulesEnabled) {
                entityLabels.add(mi.getLabel().trim());
            }
            mappingInputsPerProject.put(p.getId(), entityLabels);

            allTunnels = cloudStoreService.getTunnelsByProject(p.getId());
            faceList = new ArrayList<>();
            for(Tunnel t : allTunnels) {
                allFaces.addAll(cloudStoreService.getFacesByTunnel(t.getId()));
                faceList.addAll(cloudStoreService.getFacesByTunnel(t.getId()));
            }

            mappingList = new ArrayList<>();
            for(Face f : faceList) {
                mappingList.addAll(cloudStoreService.getMappingsByFace(f.getId()));
            }

            mappingsPerProject.put(p.getId(), mappingList);
        }

        allMapping = new ArrayList<>();

        for(Face f : allFaces) {
            allMapping.addAll(cloudStoreService.getMappingsByFace(f.getId()));
        }

        switch (moduleCode) {
            case 1:
                allProspectionHoleGroup = new ArrayList<>();

                for(Face f : allFaces) {
                    allProspectionHoleGroup.addAll(cloudStoreService.getProspectionHoleGroupsByFace(f.getId()));
                }
                moduleStorageService.syncProspectionHoleGroupFiles(syncAll, allProspectionHoleGroup);
                break;
            default:

                Iterator<String> iter = mappingsPerProject.keySet().iterator();
                List<Mapping> mappings;
                List<String> inputLabels;
                String projectId;

                storageOrderMap = new HashMap<>();
                storageModulesEnabledMap = new HashMap<>();

                while(iter.hasNext()) {
                    projectId = iter.next();
//                    mappings = mappingsPerProject.get(id);
                    inputLabels = mappingInputsPerProject.get(projectId);

                    String[] storageModulesEnabled = new String[4];
                    for(String label : inputLabels) {
                        switch (label) {
                            case "PIC":
                                storageModulesEnabled[0] = label;
                                break;
                            case "EXPT":
                                storageModulesEnabled[1] = label;
                                break;
                            case "AINFO":
                                storageModulesEnabled[2] = label;
                                break;
                            case "DISC":
                                storageModulesEnabled[3] = label;
                                break;
                        }
                    }

                    List<String> storageOrder = new ArrayList<>();
                    for(int i = 0; i < storageModulesEnabled.length; i++)
                        if(storageModulesEnabled[i] != null) storageOrder.add(storageModulesEnabled[i]);

                    if(storageOrder.isEmpty()) {
                        continue;
                    }
                    else {
                        storageOrderMap.put(projectId, storageOrder);
                        storageModulesEnabledMap.put(projectId, storageModulesEnabled);
                    }
                }

                if(storageOrderMap.isEmpty()) {
                    if(syncAll) cloudStoreService.syncForecastingData();
                    else mPresenter.onSyncResult(1);
                }
                else {
                    projectIterator = storageOrderMap.keySet().iterator();
                    execProjectCaracterizationStorageSync(syncAll);
                }
                break;
        }
    }

    private void execProjectCaracterizationStorageSync(Boolean syncAll) {
        projectIdFromIterator = projectIterator.next();
//        String[] storageModulesEnabled = storageModulesEnabledMap.get(projectIdFromIterator);
        List<String> storageOrder = storageOrderMap.get(projectIdFromIterator);

        iteratorMappings = mappingsPerProject.get(projectIdFromIterator);

        switch(storageOrder.get(0)) {
            case "PIC":
                moduleStorageService.syncPictureFiles(syncAll, iteratorMappings);
                break;
            case "EXPT":
                moduleStorageService.syncExpandedTunnelFiles(syncAll, iteratorMappings);
                break;
            case "AINFO":
                moduleStorageService.syncAdditionalInfoFiles(iteratorMappings);
                break;
            case "DISC":
                moduleStorageService.syncStereonetFiles(iteratorMappings);
                break;
        }
    }

    @Override
    public void onSyncStorageResult(int code, Boolean syncAll) {
        List<String> storageOrderCallback;

        switch(code) {
            case 1:
                moduleStorageService.syncPicturesTableData();
                break;
            case 2:
                storageOrderCallback = storageOrderMap.get(projectIdFromIterator);

                if(storageOrderCallback.size() <= 1) {
                    if(syncAll) cloudStoreService.syncForecastingData();
                    else mPresenter.onSyncResult(1);
                }
                else {
                    switch(storageOrderCallback.get(1)) {
                        case "EXPT":
                            moduleStorageService.syncExpandedTunnelFiles(syncAll, iteratorMappings);
                            break;
                        case "AINFO":
                            moduleStorageService.syncAdditionalInfoFiles(iteratorMappings);
                            break;
                    }
                }
                break;
            case 3:
                moduleStorageService.syncExpandedTunnelTableData();
                break;
            case 8:
                moduleStorageService.syncProspectionHoleGroupTableData();
                break;
            case 7:
                moduleStorageService.syncForecastingModels();
                break;
            case 9:
                mPresenter.onSyncResult(1);
                break;
            case 10:
                storageOrderCallback = storageOrderMap.get(projectIdFromIterator);

                if(storageOrderCallback.size() <= 2) {
                    if(syncAll) cloudStoreService.syncForecastingData();
                    else mPresenter.onSyncResult(1);
                }
                else {
                    switch(storageOrderCallback.get(2)) {
                        case "AINFO":
                            moduleStorageService.syncAdditionalInfoFiles(iteratorMappings);
                            break;
                        case "DISC":
                            moduleStorageService.syncStereonetFiles(iteratorMappings);
                            break;
                    }
                }
                break;
            case 11:
                cloudStoreService.syncParticularities(Particularities.class);
                break;
            case 5:
                storageOrderCallback = storageOrderMap.get(projectIdFromIterator);

                if(storageOrderCallback.size() <= 3) {
                    if(projectIterator.hasNext()) {
                        execProjectCaracterizationStorageSync(syncAll);
                    }
                    else {
                        if(syncAll) cloudStoreService.syncForecastingData();
                        else mPresenter.onSyncResult(1);
                    }
                }
                else {
                    moduleStorageService.syncStereonetFiles(iteratorMappings);
                }
                break;
            case 6:
                moduleStorageService.syncStereonetTableData();
                break;
            default:
                if(projectIterator.hasNext()) {
                    execProjectCaracterizationStorageSync(syncAll);
                }
                else {
                    if(syncAll) cloudStoreService.syncForecastingData();
                    else mPresenter.onSyncResult(1);
                }
                break;
        }
    }

    @Override
    public void syncBasicData(Context activity, MainPresenter presenter, User user) {
        mPresenter = presenter;
        cloudStoreService = new CloudStoreService(activity, this, user);
        blobStorageService = new BlobStorageService(activity.getExternalFilesDir(null), this);
        mActivity = activity;
        cloudStoreService.syncBaseData();
    }
}
