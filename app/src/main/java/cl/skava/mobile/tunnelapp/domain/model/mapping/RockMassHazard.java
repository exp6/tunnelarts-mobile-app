package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by Jose Ignacio Vera on 08-08-2016.
 */
public class RockMassHazard extends DataInput {
    private String id;
    private Boolean rockBurst;
    private Boolean swelling;
    private Boolean squeezing;
    private Boolean slaking;
    private String idAdditionalInformation;
    private Boolean completed;
    private String idMapping;
    private Boolean none;

    public RockMassHazard(String id, Boolean rockBurst, Boolean swelling, Boolean squeezing, Boolean slaking, String idAdditionalInformation, Boolean completed, String idMapping, Boolean none) {
        this.id = id;
        this.rockBurst = rockBurst;
        this.swelling = swelling;
        this.squeezing = squeezing;
        this.slaking = slaking;
        this.idAdditionalInformation = idAdditionalInformation;
        this.completed = completed;
        this.idMapping = idMapping;
        this.none = none;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getRockBurst() {
        return rockBurst;
    }

    public void setRockBurst(Boolean rockBurst) {
        this.rockBurst = rockBurst;
    }

    public Boolean getSwelling() {
        return swelling;
    }

    public void setSwelling(Boolean swelling) {
        this.swelling = swelling;
    }

    public Boolean getSqueezing() {
        return squeezing;
    }

    public void setSqueezing(Boolean squeezing) {
        this.squeezing = squeezing;
    }

    public Boolean getSlaking() {
        return slaking;
    }

    public void setSlaking(Boolean slaking) {
        this.slaking = slaking;
    }

    public String getIdAdditionalInformation() {
        return idAdditionalInformation;
    }

    public void setIdAdditionalInformation(String idAdditionalInformation) {
        this.idAdditionalInformation = idAdditionalInformation;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Boolean getNone() {
        return none;
    }

    public void setNone(Boolean none) {
        this.none = none;
    }

    @Override
    public String toString() {
        return "RockMassHazard{" +
                "id='" + id + '\'' +
                ", rockBurst=" + rockBurst +
                ", swelling=" + swelling +
                ", squeezing=" + squeezing +
                ", slaking=" + slaking +
                ", idAdditionalInformation='" + idAdditionalInformation + '\'' +
                ", completed=" + completed +
                ", idMapping='" + idMapping + '\'' +
                ", none=" + none +
                '}';
    }
}
