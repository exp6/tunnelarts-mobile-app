package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse;

import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputGroupType;

/**
 * Created by Carlos Vergara on 20-03-2017.
 */

public class QValueInputSyncResponseHandler extends FetchFromTableSyncResponseHandler<QValue> {
    QValueInputSyncResponseHandler() {

    }

    @Override
    protected void onDataFetchedSuccessfully(CloudMobileService cloudMobileService, List<QValue> fetchedData) {
        CloudStoreService cloudStoreService = (CloudStoreService) cloudMobileService;
        cloudStoreService.syncQValuesDiscontinuities(fetchedData);
    }

    @Override
    protected void onDataFetchedFailed(CloudMobileService cloudMobileService) {
        CloudStoreService cloudStoreService = (CloudStoreService) cloudMobileService;
        cloudStoreService.incrementProgress();
        cloudStoreService.syncSingleTable(RmrInputGroupType.class);
    }

    @Override
    protected void initializeClassToFetchFrom() {
        setClassToFetchFrom(QValue.class);
    }
}
