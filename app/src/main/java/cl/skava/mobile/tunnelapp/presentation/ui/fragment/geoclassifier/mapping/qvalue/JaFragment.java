package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.qvalue;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;

/**
 * Created by Jose Ignacio Vera on 05-07-2016.
 */
public class JaFragment extends Fragment {

    private static final String TAG = "JaFragment";

    private List<JaGroup> jaGroups = new ArrayList<>();

    private OnJaValueListener mCallback;

    public interface OnJaValueListener {
        void OnJaSetValue(Double value, String id, int disc);
    }

    private boolean isValid;
    private EvaluationMethodSelectionType selectionMap;

    private RadioGroup group;
    private RadioGroup group2;

    private Boolean mappingFinished;

    private int discontinuity;

    private final String lang = Locale.getDefault().getLanguage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initDataset();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_qvalue_ja, container, false);
        rootView.setTag(TAG);

        group = (RadioGroup) rootView.findViewById(R.id.radio_group1);
        RadioButton button;

        group2 = (RadioGroup) rootView.findViewById(R.id.radio_group2);

        final TableLayout tbl1 = (TableLayout) rootView.findViewById(R.id.table1);
        final TableLayout tbl2 = (TableLayout) rootView.findViewById(R.id.table2);
        final TableLayout tbl3 = (TableLayout) rootView.findViewById(R.id.table3);

        tbl1.setVisibility(View.GONE);
        tbl2.setVisibility(View.GONE);
        tbl3.setVisibility(View.GONE);

        int i = 0;
        for(JaGroup jaGroup :  jaGroups) {
            final JaGroup jaGroup1 = jaGroup;
            button = new RadioButton(getActivity());
            button.setId(i);
            button.setText(jaGroup.getIndex().trim() + ". " + jaGroup.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    displayJas(jaGroup1);

                    if(jaGroup1.getIndex().trim().equalsIgnoreCase("a")) {
                        tbl1.setVisibility(View.VISIBLE);
                        tbl2.setVisibility(View.GONE);
                        tbl3.setVisibility(View.GONE);
                    }

                    if(jaGroup1.getIndex().trim().equalsIgnoreCase("b")) {
                        tbl1.setVisibility(View.GONE);
                        tbl2.setVisibility(View.VISIBLE);
                        tbl3.setVisibility(View.GONE);
                    }

                    if(jaGroup1.getIndex().trim().equalsIgnoreCase("c")) {
                        tbl1.setVisibility(View.GONE);
                        tbl2.setVisibility(View.GONE);
                        tbl3.setVisibility(View.VISIBLE);
                    }
                }
            });
            button.setChecked(jaGroup.isChecked());
            if(jaGroup.isChecked()) {
                displayJas(jaGroup);

                if(jaGroup.getIndex().trim().equalsIgnoreCase("a")) {
                    tbl1.setVisibility(View.VISIBLE);
                    tbl2.setVisibility(View.GONE);
                    tbl3.setVisibility(View.GONE);
                }

                if(jaGroup.getIndex().trim().equalsIgnoreCase("b")) {
                    tbl1.setVisibility(View.GONE);
                    tbl2.setVisibility(View.VISIBLE);
                    tbl3.setVisibility(View.GONE);
                }

                if(jaGroup.getIndex().trim().equalsIgnoreCase("c")) {
                    tbl1.setVisibility(View.GONE);
                    tbl2.setVisibility(View.GONE);
                    tbl3.setVisibility(View.VISIBLE);
                }
            }

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }
        return rootView;
    }

    private void displayJas(JaGroup jaGroup) {
        final List<Ja> jas = jaGroup.getJas();
        final JaGroup jaGroup1 = jaGroup;
        group2.removeAllViews();
        RadioButton button;

        for(int i = 0; i < jas.size(); i++) {
            final Ja ja1 = jas.get(i);

            button = new RadioButton(getActivity());
            button.setId(i);
            button.setText(ja1.getIndex().trim() + " (" + ja1.getStart() + (ja1.getEnd() != null ? " - " + ja1.getEnd() : "") + ")     " + ja1.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (ja1.getEnd() != null) {
                        execTextDialog(getResources().getString(R.string.characterization_mapping_input_qmet_label_range1)
                                + " " + ja1.getIndex(), getResources().getString(R.string.characterization_mapping_input_qmet_label_range2) +" "
                                + ja1.getStart() + " " + getResources().getString(R.string.characterization_mapping_input_qmet_label_range3) + " "
                                + ja1.getEnd(), ja1.getStart(), ja1.getEnd(), ja1.getId(), ja1
                        , (enteredValue == null ? ja1.getStart() : (ja1.isChecked() ? enteredValue : (enteredValue >= ja1.getStart() && enteredValue <= ja1.getEnd() ? enteredValue : ja1.getStart()))),jaGroup1 );
                    } else {
                        mCallback.OnJaSetValue(ja1.getStart(), ja1.getId(), discontinuity);
                        updateMap(jaGroup1, ja1);
                    }
                }
            });
            button.setChecked(ja1.isChecked());

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group2.addView(button);
        }
    }

    private void updateMap(JaGroup jaGroup, Ja ja) {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        EvaluationMethodSelection input;
        for(int i = 0; i < groups.size(); i++) {
            groups.get(i).setChecked(false);
            if(jaGroup.getId().equalsIgnoreCase(groups.get(i).getId())) {
                groups.get(i).setChecked(true);
            }

            inputs = groups.get(i).getSubSelections();
            for(int j = 0; j < inputs.size(); j++) {
                input = inputs.get(j);
                input.setChecked(false);
                if(ja.getId().equalsIgnoreCase(input.getId())) {
                    input.setChecked(true);
                }
            }

            groups.get(i).setSubSelections(inputs);
        }
        selectionMap.setSelectionGroups(groups);
    }

    private Double enteredValue = null;

    public void setEnteredValue(Double enteredValue) {
        this.enteredValue = enteredValue;
    }

    public void execTextDialog(String title, String msg, Double start, Double end, String id, Ja ja, Double valueSelected, JaGroup jaGroup) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this.getContext());
        alert.setTitle(title);
        alert.setMessage(msg);
        // Create TextView
        final EditText input = new EditText(this.getContext());
        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        final Double startValue = start;
        final Double endValue = end;
        final String id1 = id;
        input.addTextChangedListener(new TextValidator(input) {
            @Override
            public void validate(TextView textView, String text) {
                final String message = getResources().getString(R.string.characterization_mapping_input_qmet_label_range_validation1)
                        + " " + startValue
                        + " " + getResources().getString(R.string.characterization_mapping_input_qmet_label_range_validation2)
                        + " " + endValue;

                try {
                    Double inputValue = new Double(text);
                    if (inputValue.doubleValue() >= startValue.doubleValue() && inputValue.doubleValue() <= endValue.doubleValue()) {
                        isValid = true;
                        enteredValue = inputValue;
                    } else {
                        input.setError(message);
                        isValid = false;
                    }
                } catch (NumberFormatException e) {
                    input.setError(message);
                }
            }
        });

        final Ja ja1 = ja;
        Double valueFinal = round(valueSelected, 1);
        final JaGroup jaGroup1 = jaGroup;

        input.setText(valueFinal.toString());
        alert.setView(input);
        alert.setCancelable(false);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                final AlertDialog alertDialog = (AlertDialog)dialog;

                if(input.getText().toString().isEmpty()) {
                    enteredValue = startValue;
                }

                if(isValid) {
                    mCallback.OnJaSetValue(enteredValue, id1, discontinuity);
                    updateMap(jaGroup1, ja1);
                    alertDialog.dismiss();
                }
                else {
                    input.setError(getResources().getString(R.string.characterization_mapping_input_qmet_label_range_validation3));
                }

            }
        });

        alert.show();
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnJaValueListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnJaValueListener");
        }
    }

    public void setSelectionMap(EvaluationMethodSelectionType selectionMap) {
        this.selectionMap = selectionMap;
    }

    public EvaluationMethodSelectionType getSelectionMap() {
        return selectionMap;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public int getDiscontinuity() {
        return discontinuity;
    }

    public void setDiscontinuity(int discontinuity) {
        this.discontinuity = discontinuity;
    }

    private void initDataset() {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        JaGroup jaGroup;
        List<Ja> jas;
        Ja ja;

        for(EvaluationMethodSelectionGroup e : groups) {
            jaGroup = new JaGroup();
            jaGroup.setId(e.getId());
            jaGroup.setIndex(e.getIndex());
            jaGroup.setDescription((lang.equals("es") ? e.getDescriptionEs() : e.getDescription()));
            jaGroup.setChecked(e.getChecked());
            inputs = e.getSubSelections();

            jas = new ArrayList<>();

            for(EvaluationMethodSelection em : inputs) {
                ja = new Ja();
                ja.setId(em.getId());
                ja.setIndex(em.getIndex());
                ja.setStart(em.getStart());
                ja.setEnd(em.getEnd().doubleValue() == 0.0 ? null : em.getEnd());
                ja.setDescription((lang.equals("es") ? em.getDescriptionEs() : em.getDescription()));
                ja.setChecked(em.getChecked());
                jas.add(ja);
            }

            jaGroup.setJas(jas);
            jaGroups.add(jaGroup);
        }
    }

    class Ja {
        private String id;
        private String index;
        private Double value;
        private Double start;
        private Double end;
        private String description;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public Double getStart() {
            return start;
        }

        public void setStart(Double start) {
            this.start = start;
        }

        public Double getEnd() {
            return end;
        }

        public void setEnd(Double end) {
            this.end = end;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }

    class JaGroup {
        private String id;
        private String index;
        private String description;
        private List<Ja> jas;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<Ja> getJas() {
            return jas;
        }

        public void setJas(List<Ja> jas) {
            this.jas = jas;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }

    public abstract class TextValidator implements TextWatcher {
        private final TextView textView;

        public TextValidator(TextView textView) {
            this.textView = textView;
        }

        public abstract void validate(TextView textView, String text);

        @Override
        final public void afterTextChanged(Editable s) {
            String text = textView.getText().toString();
            validate(textView, text);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { /* Don't care */ }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { /* Don't care */ }
    }
}
