package cl.skava.mobile.tunnelapp.domain.interactors.impl.home;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.base.AbstractInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.home.TunnelSelectionInteractor;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.repository.FaceRepository;

/**
 * Created by Jose Ignacio Vera on 21-06-2016.
 */
public class TunnelSelectionInteractorImpl extends AbstractInteractor implements TunnelSelectionInteractor {

    private TunnelSelectionInteractor.Callback mCallback;
    private FaceRepository mFaceRepository;
    private Tunnel mTunnelSelected;

    public TunnelSelectionInteractorImpl(Executor threadExecutor,
                                         MainThread mainThread,
                                         TunnelSelectionInteractor.Callback callback,
                                         FaceRepository facesRepository,
                                         Tunnel selection) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mFaceRepository = facesRepository;
        mTunnelSelected = selection;
    }

    private void postFaces(final List<Face> faces) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFaceListRetrieved(faces);
            }
        });
    }

    @Override
    public void run() {
        final List<Face> faces = mFaceRepository.getFacesByTunnel(mTunnelSelected);

        if(faces == null) {
            //TODO: notify error
            return;
        }
        postFaces(faces);
    }
}
