package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse;

import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.UserProject;
import cl.skava.mobile.tunnelapp.domain.model.account.User;

/**
 * Created by Carlos Vergara on 20-03-2017.
 */

public class UserProjectSyncResponseHandler extends FetchFromTableSyncResponseHandler<UserProject> {
    UserProjectSyncResponseHandler() {

    }

    @Override
    protected void onDataFetchedSuccessfully(CloudMobileService cloudMobileService, List<UserProject> userProjects) {
        CloudStoreService cloudStoreService = (CloudStoreService) cloudMobileService;
        cloudStoreService.syncProjects(userProjects);
    }

    @Override
    protected void initializeClassToFetchFrom() {
        setClassToFetchFrom(UserProject.class);
    }
}
