package cl.skava.mobile.tunnelapp.domain.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Jose Ignacio Vera on 15-06-2016.
 */
public class Module implements Serializable {

    private String name;
    private Date createdAt;
    private Boolean enabled;

    private Integer code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Module{" +
                "name='" + name + '\'' +
                ", createdAt=" + createdAt +
                ", enabled=" + enabled +
                ", code=" + code +
                '}';
    }
}
