package cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.mapping.rmr;

import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rmr.ConditionDiscontinuitiesFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rmr.GuidelinesConditionsFragment;

/**
 * Created by Jose Ignacio Vera on 11-07-2016.
 */
public class ConditionDiscontinuitiesAdapter extends RecyclerView.Adapter<ConditionDiscontinuitiesAdapter.ViewHolder> {

    public List<ArrayList> mInputItems;
    public GuidelinesConditionsFragment mCallback;
    private List<EvaluationMethodSelectionType> mSelectionMaps;

    private Boolean mMappingFinished;

    private final String lang = Locale.getDefault().getLanguage();

    public ConditionDiscontinuitiesAdapter(List<ArrayList> inputItems,
                                           GuidelinesConditionsFragment callback,
                                           List<EvaluationMethodSelectionType> selectionMaps,
                                           Boolean mappingFinished) {
        mInputItems = inputItems;
        mCallback = callback;
        mSelectionMaps = selectionMaps;
        mMappingFinished = mappingFinished;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            textView = (TextView) v.findViewById(R.id.mapping_rmr_input_item_text);
        }

        public TextView getTextView() {
            return textView;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_geoclassifier_mapping_rmr_guidelinesconditions_item, parent, false);
        holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        RadioGroup group = (RadioGroup) holder.itemView.findViewById(R.id.radio_group1);
        List<GuidelinesConditionsFragment.SelectionItem> items = mInputItems.get(position);
        RadioButton button;

        switch (position) {
            case 1:
                holder.getTextView().setText(lang.equals("es") ? "Separación (apertura)" : "Separation (aperture)");
                break;
            case 2:
                holder.getTextView().setText(lang.equals("es") ? "Rugosidad" : "Roughness");
                break;
            case 3:
                holder.getTextView().setText(lang.equals("es") ? "Relleno" : "Infilling (gouge)");
                break;
            case 4:
                holder.getTextView().setText(lang.equals("es") ? "Alteración" : "Weathering");
                break;
            default:
                holder.getTextView().setText(lang.equals("es") ? "Largo de la discontinuidad (persistencia)" : "Discontinuity length (persistence)");
                break;
        }

        int i = 0;
        for(GuidelinesConditionsFragment.SelectionItem jw :  items) {
            final GuidelinesConditionsFragment.SelectionItem jw1 = jw;

            button = new RadioButton(mCallback.getActivity());
            button.setId(i);
//            button.setText(jw.getIndex().trim() + " (" + jw.getStart() + (jw.getEnd() != null ? " - " + jw.getEnd() : "") + ")     " + jw.getDescription());
            button.setText("(" + jw.getStart() + (jw.getEnd() != null ? " - " + jw.getEnd() : "") + ")     " + jw.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    updateMap(jw1, position);
                    mCallback.onItemClick(position, jw1.getStart(), jw1.getId(), mSelectionMaps);
                }
            });
            button.setChecked(jw.isChecked());

            if(mMappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }
    }

    @Override
    public int getItemCount() {
        return mInputItems.size();
    }

    private void updateMap(GuidelinesConditionsFragment.SelectionItem selectionItem, int position) {
        List<EvaluationMethodSelectionGroup> groups = mSelectionMaps.get(position).getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        EvaluationMethodSelection input;
        groups.get(0).setChecked(true);
        inputs = groups.get(0).getSubSelections();

        for(int j = 0; j < inputs.size(); j++) {
            input = inputs.get(j);
            input.setChecked(false);
            if(selectionItem.getId().equalsIgnoreCase(input.getId())) {
                input.setChecked(true);
            }
        }

        groups.get(0).setSubSelections(inputs);
        mSelectionMaps.get(position).setSelectionGroups(groups);
    }
}
