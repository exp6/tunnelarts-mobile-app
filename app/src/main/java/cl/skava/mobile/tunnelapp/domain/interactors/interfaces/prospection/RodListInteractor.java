package cl.skava.mobile.tunnelapp.domain.interactors.interfaces.prospection;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.interactors.base.Interactor;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public interface RodListInteractor extends Interactor {

    interface Callback {
        void onRodListRetrieved(List<Rod> rods);
    }

    void create(Rod rod);
    void updateItems(List<Rod> rods);
    void update(Rod rod);
    void delete(Rod rod);
}
