package cl.skava.mobile.tunnelapp.presentation.presenters.impl.prospection;

import android.content.Context;

//import org.jpmml.evaluator.Evaluator;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.machinelearning.base.MachineLearningService;
import cl.skava.mobile.tunnelapp.data.machinelearning.impl.r.MLEngine;
import cl.skava.mobile.tunnelapp.data.repository.ProspectionRepositoryImpl;
import cl.skava.mobile.tunnelapp.data.repository.RodRepositoryImpl;
import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.impl.prospection.ExecuteModelInteractorImpl;
import cl.skava.mobile.tunnelapp.domain.interactors.impl.prospection.ProspectionListInteractorImpl;
import cl.skava.mobile.tunnelapp.domain.interactors.impl.prospection.RodListInteractorImpl;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.prospection.ExecuteModelInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.prospection.ProspectionListInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.prospection.RodListInteractor;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.domain.repository.ProspectionRepository;
import cl.skava.mobile.tunnelapp.domain.repository.RodRepository;
import cl.skava.mobile.tunnelapp.presentation.presenters.base.AbstractPresenter;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.prospection.ProspectionCalculationsPresenter;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class ProspectionCalculationsPresenterImpl extends AbstractPresenter
        implements ProspectionCalculationsPresenter,
        ExecuteModelInteractor.Callback,
        ProspectionListInteractor.Callback,
        RodListInteractor.Callback {

    private ProspectionCalculationsPresenter.View mView;
    private MachineLearningService mMLService;

    private Context mActivity;
    private String mProspectionId;

    private ProspectionListInteractor mProspectionListInteractor;
    private ProspectionRepository mProspectionRepository;
    private RodListInteractor mRodListInteractor;
    private RodRepository mRodRepository;

    public ProspectionCalculationsPresenterImpl(Executor executor,
                                                MainThread mainThread,
                                                View view,
                                                Context activity,
                                                String prospectionId) {
        super(executor, mainThread);
        mView = view;
        mActivity = activity;
        mProspectionId = prospectionId;

        mProspectionRepository = new ProspectionRepositoryImpl(mActivity, mProspectionId);
        mProspectionListInteractor = new ProspectionListInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mProspectionRepository
        );
    }

    @Override
    public void executeModel(File file, InputStream modelFileIn, OutputStream modelFileOut, List<InputStream> modelsIn, List<OutputStream> modelsOut, List<File> outputFiles, List<Rod> rods, Float initialQ) {
        mView.showExecutionProgress();
        mMLService = new MLEngine(file, modelFileIn, modelFileOut, modelsIn, modelsOut, outputFiles, rods, initialQ);
        ExecuteModelInteractor interactor = new ExecuteModelInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mMLService
        );
        interactor.execute();
    }

    @Override
    public void executeModel(File[] evaluators, List<Rod> rods, Float initialQ) {
        mView.showExecutionProgress();
        mMLService = new cl.skava.mobile.tunnelapp.data.machinelearning.impl.java.MLEngine(evaluators, rods, initialQ);

        ExecuteModelInteractor interactor = new ExecuteModelInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mMLService
        );
        interactor.execute();
    }

    @Override
    public void getRodsByProspection(ProspectionHole prospection) {
        mRodRepository = new RodRepositoryImpl(mActivity, prospection.getId());
        mRodListInteractor = new RodListInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mRodRepository);

        mRodListInteractor.execute();
    }

    @Override
    public void createNewProspection(ProspectionHole prospection) {
        mProspectionListInteractor.create(prospection);
    }

    @Override
    public void createNewRod(Rod rod) {
        mRodRepository = new RodRepositoryImpl(mActivity, rod.getIdProspectionHole());
        mRodListInteractor = new RodListInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mRodRepository);

        mRodListInteractor.create(rod);
    }

    @Override
    public void deleteProspection(ProspectionHole prospection) {
        mProspectionListInteractor.delete(prospection);
    }

    @Override
    public void updateProspection(ProspectionHole prospection) {
        mProspectionListInteractor.update(prospection);
    }

    @Override
    public void deleteRod(Rod rod) {
        mRodRepository = new RodRepositoryImpl(mActivity, rod.getIdProspectionHole());
        mRodListInteractor = new RodListInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mRodRepository);
        mRodListInteractor.delete(rod);
    }

    @Override
    public void updateRod(Rod rod) {
        mRodRepository = new RodRepositoryImpl(mActivity, rod.getIdProspectionHole());
        mRodListInteractor = new RodListInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mRodRepository);
        mRodListInteractor.update(rod);
    }

    @Override
    public void resume() {
        mView.showProgress();
        mProspectionListInteractor.execute();
    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onExecResult(List<Rod> finalRods) {
        mRodRepository = new RodRepositoryImpl(mActivity, finalRods.get(0).getIdProspectionHole());

        RodListInteractor interactor = new RodListInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mRodRepository
        );
        interactor.updateItems(finalRods);
        mView.hideExecutionProgress();
        mView.showProgress();
    }

    @Override
    public void onProspectionListRetrieved(List<ProspectionHole> prospections) {
        mView.displayPositionList(prospections);

        if(prospections.isEmpty()) {
            mView.hideProgress();
        }
        else {
            ProspectionHole p = prospections.get(0);
            RodRepository mStickRepository = new RodRepositoryImpl(mActivity, p.getId());

            RodListInteractor interactor = new RodListInteractorImpl(
                    mExecutor,
                    mMainThread,
                    this,
                    mStickRepository
            );

            interactor.execute();
        }
    }

    @Override
    public void onRodListRetrieved(List<Rod> rods) {
        mView.hideProgress();
        mView.displayStickList(rods);
    }
}
