package cl.skava.mobile.tunnelapp.domain.interactors.impl.prospection;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.exports.base.ExportService;
import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.base.AbstractInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.prospection.ProspectionHoleGroupListInteractor;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroupCalculation;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.domain.repository.ProspectionHoleGroupRepository;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class ProspectionHoleGroupListInteractorImpl extends AbstractInteractor
        implements ProspectionHoleGroupListInteractor {

    private ProspectionHoleGroupRepository mProspectionHoleGroupRepository;
    private ProspectionHoleGroupListInteractor.Callback mCallback;

    private ExportService mExportService;

    public ProspectionHoleGroupListInteractorImpl(Executor threadExecutor,
                                                  MainThread mainThread,
                                                  ProspectionHoleGroupListInteractor.Callback callback,
                                                  ProspectionHoleGroupRepository prospectionHoleGroupRepository,
                                                  ExportService exportService) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mProspectionHoleGroupRepository = prospectionHoleGroupRepository;
        mExportService = exportService;
    }

    private void notifyError() {
//        mMainThread.post(new Runnable() {
//            @Override
//            public void run() {
//                mCallback.onRetrievalFailed("No Modules on the system.");
//            }
//        });
    }

    private void postProspectionHoleGroups(final List<ProspectionHoleGroup> prospectionHoleGroups) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProspectionHoleGroupListRetrieved(prospectionHoleGroups);
            }
        });
    }

    private void postQValuesByMappings(final HashMap<Mapping, QValue> qValuesByMappings) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onQValuesByMappingsRetrieved(qValuesByMappings);
            }
        });
    }

    private void postRodsByProspectionHole(final HashMap<ProspectionHole, List<Rod>> rodsByProspectionHole) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRodsByProspectionHoleRetrieved(rodsByProspectionHole);
            }
        });
    }

    private void postProspectionRefPictures(final List<ProspectionRefPicture> prospectionRefPictures) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProspectionRefPictureListRetrieved(prospectionRefPictures);
            }
        });
    }

    private void postCalculationsByGroup(final HashMap<ProspectionHoleGroup, List<ProspectionHoleGroupCalculation>> calculationsByGroup) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProspectionHoleGroupCalculationListRetrieved(calculationsByGroup);
            }
        });
    }

    @Override
    public void run() {
        final List<ProspectionHoleGroup> prospectionHoleGroups = getList();
        final HashMap<Mapping, QValue> qValuesByMappings = mProspectionHoleGroupRepository.getQValuesByMappings();
        final List<ProspectionRefPicture> prospectionRefPictures = mProspectionHoleGroupRepository.getRefPictures();
        final HashMap<ProspectionHoleGroup, List<ProspectionHoleGroupCalculation>> calculationsByGroup = mProspectionHoleGroupRepository.getCalculationsByGroup();

        postProspectionHoleGroups(prospectionHoleGroups);
        postQValuesByMappings(qValuesByMappings);
        postProspectionRefPictures(prospectionRefPictures);
        postCalculationsByGroup(calculationsByGroup);
    }

    @Override
    public void create(ProspectionHoleGroup prospectionHoleGroup) {
        if(mProspectionHoleGroupRepository.insert(prospectionHoleGroup)) {
            final List<ProspectionHoleGroup> prospectionHoleGroups = getList();

            if(prospectionHoleGroups == null) {
                return;
            }

            postProspectionHoleGroups(prospectionHoleGroups);
        }
    }

    @Override
    public void update(ProspectionHoleGroup prospectionHoleGroup) {
        if(mProspectionHoleGroupRepository.update(prospectionHoleGroup)) {
            final List<ProspectionHoleGroup> prospectionHoleGroups = getList();

            if(prospectionHoleGroups == null) {
                return;
            }

            postProspectionHoleGroups(prospectionHoleGroups);
        }
    }

    @Override
    public void delete(ProspectionHoleGroup prospectionHoleGroup) {
        if(mProspectionHoleGroupRepository.delete(prospectionHoleGroup)) {
            final List<ProspectionHoleGroup> prospectionHoleGroups = getList();

            if(prospectionHoleGroups == null) {
                return;
            }

            postProspectionHoleGroups(prospectionHoleGroups);
        }
    }

    @Override
    public void syncData(User user) {
        mProspectionHoleGroupRepository.setInteractor(this);
        mProspectionHoleGroupRepository.syncData(user);
    }

    @Override
    public void onSyncError(String msg) {
        final String msg1 = msg;
        final boolean status = false;
        postSyncStatus(status, msg1);
    }

    @Override
    public void onSyncSuccess() {
        final String msg = "";
        final boolean status = true;
        postSyncStatus(status, msg);
    }

    @Override
    public void setSyncProgressPercentaje(Integer percentage) {
        mCallback.onSyncProgressPercentaje(percentage);
    }

    @Override
    public void notifySyncSegment(String msg) {
        mCallback.notifySyncSegment(msg);
    }

    @Override
    public void getRodsByProspectionHoleMap(String prospectionHoleGroupId) {
        final HashMap<ProspectionHole, List<Rod>> rodsByProspectionHole = mProspectionHoleGroupRepository.getRodsByProspectionHole(prospectionHoleGroupId);
        postRodsByProspectionHole(rodsByProspectionHole);
    }

    @Override
    public void exportData(Project p, Tunnel t, Face f, String directoryPath) {
        final boolean status = mExportService.exportForecastingData(p, t, f, directoryPath);
        postExportStatus(status);
    }

    @Override
    public void savePicture(ProspectionRefPicture prospectionRefPicture) {
        mProspectionHoleGroupRepository.insertRefPicture(prospectionRefPicture);
    }

    @Override
    public void updateProspectionHoleGroupCalculations(List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations, String prospectionHoleGroupId) {
        mProspectionHoleGroupRepository.insertProspectionHoleGroupCalculations(prospectionHoleGroupCalculations, prospectionHoleGroupId);
        final HashMap<ProspectionHoleGroup, List<ProspectionHoleGroupCalculation>> calculationsByGroup = mProspectionHoleGroupRepository.getCalculationsByGroup();
        postCalculationsByGroup(calculationsByGroup);
    }

    private List<ProspectionHoleGroup> getList() {
        final List<ProspectionHoleGroup> prospectionHoleGroups = mProspectionHoleGroupRepository.getList();

        if (prospectionHoleGroups == null) {
            notifyError();
        }

        return prospectionHoleGroups;
    }

    private void postExportStatus(final boolean status) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onExportStatus(status);
            }
        });
    }

    private void postSyncStatus(final boolean status, final String msg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSyncStatus(status, msg);
            }
        });
    }
}
