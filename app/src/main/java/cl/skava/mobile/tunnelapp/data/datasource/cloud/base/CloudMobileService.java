package cl.skava.mobile.tunnelapp.data.datasource.cloud.base;

import com.microsoft.windowsazure.mobileservices.table.query.Query;

import java.util.List;

/**
 * Created by Jose Ignacio Vera on 15-07-2016.
 */
public interface CloudMobileService {

    //Define system functions for cloud service
    void setSyncProgressPercentage(Integer percentage);
    void onSyncResult(int code);
    void handleSyncResponse(boolean success, String entity);
    void initDataRepository();
    void networkError(String msg);
    void syncEntity(Class c, Query mSyncQuery);
    void pushAllData();
    void setPushSyncStatus(boolean success, String msg);
    void syncAllData();
}
