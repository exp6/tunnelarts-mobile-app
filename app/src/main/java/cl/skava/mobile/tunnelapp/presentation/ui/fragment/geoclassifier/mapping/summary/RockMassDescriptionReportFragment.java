package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInstance;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.rockmassdescription.AdditionalInfoFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.rockmassdescription.DiscontinuitiesFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.rockmassdescription.LithologiesFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.rockmassdescription.RockMassOverviewFragment;

public class RockMassDescriptionReportFragment extends Fragment {

    private MappingInstance finalInput;
    private List<MappingInputModule> mMappingInputModuleDataset;

    public static RockMassDescriptionReportFragment newInstance() {
        RockMassDescriptionReportFragment fragment = new RockMassDescriptionReportFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_summary_rock_mass_description_report, container, false);

        if(isModuleEnabled("RMO")) {
            RockMassOverviewFragment fragment1 = new RockMassOverviewFragment();
            fragment1.setMappingInput(finalInput.getMappingRockMass());
            updateFragment(fragment1, R.id.rockmass_overview);
        }

        if(isModuleEnabled("LTH")) {
            LithologiesFragment fragment2 = new LithologiesFragment();
            fragment2.setLithologyList(finalInput.getMappingLithologies());
            updateFragment(fragment2, R.id.lithologies);
        }

        if(isModuleEnabled("DISC")) {
            DiscontinuitiesFragment fragment3 = new DiscontinuitiesFragment();
            fragment3.setDiscontinuities(finalInput.getMappingDiscontinuities());
            updateFragment(fragment3, R.id.discontinuities);
        }

        if(isModuleEnabled("AINFO")) {
            AdditionalInfoFragment fragment4 = new AdditionalInfoFragment();
            fragment4.setSpecialFeatures(finalInput.getMappingSpecialFeatures());
            fragment4.setRockMassHazard(finalInput.getMappingRockMassHazard());
            fragment4.setFailureZone(finalInput.getMappingFailureZone());
            fragment4.setParticularities(finalInput.getMappingParticularities());
            updateFragment(fragment4, R.id.additionalinfo);
        }

        return rootView;
    }

    private boolean isModuleEnabled(String label) {
        for(MappingInputModule mim : mMappingInputModuleDataset)
            if(mim.getLabel().trim().equalsIgnoreCase(label)) return true;

        return false;

    }

    private void updateFragment(Fragment fragment, int container) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(container, fragment);
        transaction.commitAllowingStateLoss();
    }

    public void setFinalInput(MappingInstance finalInput) {
        this.finalInput = finalInput;
    }

    public void setmMappingInputModuleDataset(List<MappingInputModule> mMappingInputModuleDataset) {
        this.mMappingInputModuleDataset = mMappingInputModuleDataset;
    }
}
