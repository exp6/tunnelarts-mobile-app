package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by Jose Ignacio Vera on 01-08-2016.
 */
public class RockMass extends DataInput {

    private String id;
    private Integer geneticGroup;//
    private Integer rockName;
    private Integer structure;//
    private Integer weathering;
    private Integer colorValue;
    private Integer colorChroma;
    private Integer colorHue;
    private Integer jointing;//
    private Integer strength;
    private Integer water;//
    private Float waterT;//
    private Integer waterQuality;//
    private Float inflow;//
    private String idMapping;
    private Boolean completed;
    private String briefDesc;

    public RockMass(String id, Integer geneticGroup, Integer rockName, Integer structure, Integer weathering, Integer colorValue, Integer colorChroma, Integer colorHue, Integer jointing, Integer strength, Integer water, Float waterT, Integer waterQuality, Float inflow, String idMapping, Boolean completed, String briefDesc) {
        this.id = id;
        this.geneticGroup = geneticGroup;
        this.rockName = rockName;
        this.structure = structure;
        this.weathering = weathering;
        this.colorValue = colorValue;
        this.colorChroma = colorChroma;
        this.colorHue = colorHue;
        this.jointing = jointing;
        this.strength = strength;
        this.water = water;
        this.waterT = waterT;
        this.waterQuality = waterQuality;
        this.inflow = inflow;
        this.idMapping = idMapping;
        this.completed = completed;
        this.briefDesc = briefDesc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getGeneticGroup() {
        return geneticGroup;
    }

    public void setGeneticGroup(Integer geneticGroup) {
        this.geneticGroup = geneticGroup;
    }

    public Integer getRockName() {
        return rockName;
    }

    public void setRockName(Integer rockName) {
        this.rockName = rockName;
    }

    public Integer getStructure() {
        return structure;
    }

    public void setStructure(Integer structure) {
        this.structure = structure;
    }

    public Integer getWeathering() {
        return weathering;
    }

    public void setWeathering(Integer weathering) {
        this.weathering = weathering;
    }

    public Integer getColorValue() {
        return colorValue;
    }

    public void setColorValue(Integer colorValue) {
        this.colorValue = colorValue;
    }

    public Integer getColorChroma() {
        return colorChroma;
    }

    public void setColorChroma(Integer colorChroma) {
        this.colorChroma = colorChroma;
    }

    public Integer getColorHue() {
        return colorHue;
    }

    public void setColorHue(Integer colorHue) {
        this.colorHue = colorHue;
    }

    public Integer getJointing() {
        return jointing;
    }

    public void setJointing(Integer jointing) {
        this.jointing = jointing;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getWater() {
        return water;
    }

    public void setWater(Integer water) {
        this.water = water;
    }

    public Float getWaterT() {
        return waterT;
    }

    public void setWaterT(Float waterT) {
        this.waterT = waterT;
    }

    public Integer getWaterQuality() {
        return waterQuality;
    }

    public void setWaterQuality(Integer waterQuality) {
        this.waterQuality = waterQuality;
    }

    public Float getInflow() {
        return inflow;
    }

    public void setInflow(Float inflow) {
        this.inflow = inflow;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public String getBriefDesc() {
        return briefDesc;
    }

    public void setBriefDesc(String briefDesc) {
        this.briefDesc = briefDesc;
    }

    @Override
    public String toString() {
        return "RockMass{" +
                "id='" + id + '\'' +
                ", geneticGroup=" + geneticGroup +
                ", rockName=" + rockName +
                ", structure=" + structure +
                ", weathering=" + weathering +
                ", colorValue=" + colorValue +
                ", colorChroma=" + colorChroma +
                ", colorHue=" + colorHue +
                ", jointing=" + jointing +
                ", strength=" + strength +
                ", water=" + water +
                ", waterT=" + waterT +
                ", waterQuality=" + waterQuality +
                ", inflow=" + inflow +
                ", idMapping='" + idMapping + '\'' +
                ", completed=" + completed +
                ", briefDesc='" + briefDesc + '\'' +
                '}';
    }
}
