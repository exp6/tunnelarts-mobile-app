package cl.skava.mobile.tunnelapp.domain.interactors.interfaces.prospection;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.interactors.base.Interactor;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public interface ProspectionListInteractor extends Interactor {

    interface Callback {
        void onProspectionListRetrieved(List<ProspectionHole> prospections);
    }

    void create(ProspectionHole prospection);
    void update(ProspectionHole prospection);
    void delete(ProspectionHole prospection);
}
