package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;

public class GeneralReportFragment extends Fragment {
    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Mapping selectedMapping;
    private User mUser;
    private Client mClient;

    public GeneralReportFragment() {}

    public static GeneralReportFragment newInstance() {
        GeneralReportFragment fragment = new GeneralReportFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_summary_info_report, container, false);

        TextView text1 = (TextView) rootView.findViewById(R.id.client_name);
        text1.setText(mClient.getName());
        TextView text2 = (TextView) rootView.findViewById(R.id.project_name);
        text2.setText(selectedProject.getName());
        TextView text3 = (TextView) rootView.findViewById(R.id.tunnel_name);
        text3.setText(selectedTunnel.getName());
        TextView text4 = (TextView) rootView.findViewById(R.id.face_name);
        text4.setText(selectedFace.getName());
        TextView text5 = (TextView) rootView.findViewById(R.id.mapping_time);

        Date currentDate = new Date();
        text5.setText(selectedMapping.getMappingTime() == null ? (new SimpleDateFormat("dd-MM-yyyy")).format(currentDate) + " / " + (new SimpleDateFormat("hh:mm:ss")).format(currentDate) : (new SimpleDateFormat("dd/MM/yyyy")).format(selectedMapping.getMappingTime()) + " / " + (new SimpleDateFormat("hh:mm:ss")).format(selectedMapping.getMappingTime()));
        TextView text6 = (TextView) rootView.findViewById(R.id.mapping_geologist);
        text6.setText(mUser.getFirstName() + " " + mUser.getLastName());
        TextView text7 = (TextView) rootView.findViewById(R.id.mapping_direction);
        text7.setText(selectedFace.getOrientation() == null ? "0" : selectedFace.getOrientation().toString());
        TextView text8 = (TextView) rootView.findViewById(R.id.mapping_slope);
        text8.setText(selectedFace.getInclination() == null ? "0" : selectedFace.getInclination().toString());
        TextView text9 = (TextView) rootView.findViewById(R.id.mapping_round_length);
        text9.setText(selectedMapping.getRoundLength() == null ? "0" : selectedMapping.getRoundLength().toString());

        ChainageNumberFormat chainageNumberFormat = new ChainageNumberFormat();
        TextView text10 = (TextView) rootView.findViewById(R.id.mapping_chainage_start);
        text10.setText(chainageNumberFormat.format(selectedMapping.getChainageStart()));
        TextView text11 = (TextView) rootView.findViewById(R.id.mapping_chainage_end);
        text11.setText(chainageNumberFormat.format(selectedMapping.getChainageEnd()));

        return rootView;
    }

    public void setSelectionContext(Project project,
                                    Tunnel tunnel,
                                    Face face,
                                    Mapping mapping,
                                    User user,
                                    Client client) {
        selectedProject = project;
        selectedTunnel = tunnel;
        selectedFace = face;
        selectedMapping = mapping;
        mUser = user;
        mClient = client;
    }

    class ChainageNumberFormat extends DecimalFormat {

        public ChainageNumberFormat() {
            DecimalFormatSymbols custom = new DecimalFormatSymbols();
            custom.setGroupingSeparator('+');
            custom.setDecimalSeparator(',');
            setMinimumFractionDigits(2);
            setMaximumFractionDigits(2);

            setDecimalFormatSymbols(custom);
        }
    }
}
