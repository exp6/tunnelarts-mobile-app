package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.supportrec;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import cl.skava.mobile.tunnelapp.R;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class SupportRecFragment extends Fragment {

    private static final String TAG = "SupportRecFragment";

    private Boolean mappingFinished;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_supportrec, container, false);
        rootView.setTag(TAG);

        Button b = (Button) rootView.findViewById(R.id.reference_pic);

        final ImageView img = (ImageView) rootView.findViewById(R.id.bolt_type_img);
        Spinner spinner = (Spinner) rootView.findViewById(R.id.bolt_type_spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                switch (position) {
                    case 1:
                        img.setImageResource(R.drawable.perno_expansiva);
                        break;
                    case 2:
                        img.setImageResource(R.drawable.perno_autoperforante);
                        break;
                    case 3:
                        img.setImageResource(R.drawable.perno_lechado);
                        break;
                    default:
                        img.setImageResource(R.drawable.perno_resina);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });


        final ImageView img1 = (ImageView) rootView.findViewById(R.id.wall_mesh_type_img);
        Spinner spinner1 = (Spinner) rootView.findViewById(R.id.wall_mesh_type_spinner);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                switch (position) {
                    case 1:
                        img1.setImageResource(R.drawable.malla_tejido);
                        break;
                    default:
                        img1.setImageResource(R.drawable.malla_soldado);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        final ImageView img2 = (ImageView) rootView.findViewById(R.id.roof_mesh_type_img);
        Spinner spinner2 = (Spinner) rootView.findViewById(R.id.roof_mesh_type_spinner);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                switch (position) {
                    case 1:
                        img2.setImageResource(R.drawable.malla_tejido);
                        break;
                    default:
                        img2.setImageResource(R.drawable.malla_soldado);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        final ImageView img3 = (ImageView) rootView.findViewById(R.id.arch_type_img);
        Spinner spinner3 = (Spinner) rootView.findViewById(R.id.arch_type_spinner);
        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                switch (position) {
                    case 1:
                        img3.setImageResource(R.drawable.arco_srr);
                        break;
                    case 2:
                        img3.setImageResource(R.drawable.arco_vigas);
                        break;
                    case 3:
                        img3.setImageResource(R.drawable.arco_acero);
                        break;
                    default:
                        img3.setImageResource(R.drawable.arco_marco_reticulado);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }
}
