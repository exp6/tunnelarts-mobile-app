package cl.skava.mobile.tunnelapp.presentation.views.geoclassifier.mapping.summary;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.w3c.dom.Attr;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Discontinuity;

/**
 * Created by JoseVera on 5/11/2017.
 */

public class StereonetView extends View {

    private ShapeDrawable mDrawable;
    private static int centreX, centreY, radius;
    private RectF mGreatCircleBounds;
    private Paint paint;

    private List<Double> mDipList;
    private List<Double> mDipDirList;

    public StereonetView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StereonetView(Context context, List<Discontinuity> discontinuities) {
        super(context);

        mDipList = new ArrayList<>();
        mDipDirList = new ArrayList<>();

        for(int i = 0; i < discontinuities.size(); i++) {
            mDipList.add(new Double(discontinuities.get(i).getOrientationD()));
            mDipDirList.add(new Double(discontinuities.get(i).getOrientationDd()));
        }

//        mDipList = dipList;
//        mDipDirList = dipDirList;
        init();
    }

    public StereonetView(Context context) {
        super(context);
        init();
    }

    public StereonetView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(21)
    public StereonetView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int desiredWidth = 700;
        int desiredHeight = 700;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize);
        } else {
            //Be whatever you want
            width = desiredWidth;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize);
        } else {
            //Be whatever you want
            height = desiredHeight;
        }

        //MUST CALL THIS
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2F);
        paint.setColor(Color.GRAY);
        canvas.drawOval(mGreatCircleBounds, paint);
        paint.setStrokeWidth(1F);
        canvas.drawLine(centreX, centreY - radius, centreX, centreX + radius, paint);
        canvas.drawLine(centreX - radius, centreY, centreX + radius, centreY, paint);

        paint.setStrokeWidth(1F);

        for(int degrees = 10; degrees <= 80; degrees += 10){
            double greatRadius = radius / (Math.cos(Math.toRadians(degrees))); // radius of great circle
            int greatX1 = (int)Math.round(centreX + radius * (Math.tan(Math.toRadians(degrees)))
                    - greatRadius); // x coord of great circle left hemisphere
            int greatX2 = (int)Math.round(centreX - (radius * (Math.tan(Math.toRadians(degrees))))
                    - greatRadius); // x coord of great circle right hemisphere
            int greatY = (int)Math.round(centreY - greatRadius); // y coord of great circle

            double smallRadius = (radius / (Math.tan(Math.toRadians(degrees))));
            int smallY1 = (int)Math.round((centreY  - (radius / (Math.sin(Math.toRadians(degrees)))) - smallRadius));
            int smallY2 = (int)Math.round((centreY  + (radius / (Math.sin(Math.toRadians(degrees)))) - smallRadius));
            int smallX = (int)Math.round(centreX - smallRadius);

            RectF mShapeAux = new RectF(greatX1, greatY, greatX1 + (2 * (int)Math.round(greatRadius)), greatY + (2 * (int)Math.round(greatRadius)));
            canvas.drawArc(mShapeAux, 90 + degrees, 180 - ( 2 * degrees), false, paint);

            mShapeAux = new RectF(greatX2, greatY, greatX2 + (2 * (int)Math.round(greatRadius)), greatY + (2 * (int)Math.round(greatRadius)));
            canvas.drawArc(mShapeAux, 270 + degrees, 180 - ( 2 * degrees), false, paint);

            mShapeAux = new RectF(smallX, smallY2, smallX + (2 * (int)Math.round(smallRadius)), smallY2 + (2 * (int)Math.round(smallRadius)));
            canvas.drawArc(mShapeAux, 270 - degrees, 180 - ( 2 * (90 - degrees)), false, paint);

            mShapeAux = new RectF(smallX, smallY1, smallX + (2 * (int)Math.round(smallRadius)), smallY1 + (2 * (int)Math.round(smallRadius)));
            canvas.drawArc(mShapeAux, 90 - degrees, 180 - ( 2 * (90 - degrees)), false, paint);
        }

        //Draw great circles
        int azimuth;
        int dip;

        int[] colors = {Color.BLACK, Color.RED};

        for(int i = 0; i < mDipList.size(); i++) {
            dip = mDipList.get(i).intValue();
            azimuth = mDipDirList.get(i).intValue() + 90;
            paint.setStyle(Paint.Style.STROKE);
            paint.setColor(i > 0 ? colors[1] : colors[0]);

            double greatRadius = radius / (Math.cos(Math.toRadians(dip))); // radius of great circle
            double correction = (radius / Math.cos(Math.toRadians(dip)))
                    - (radius * Math.tan(Math.toRadians(dip))); // correction for great circle inflexion from center

            int x = (int)Math.round(greatRadius *  Math.cos(Math.toRadians(azimuth)) + centreX - greatRadius
                    - (correction * Math.cos(Math.toRadians(azimuth)))); //convert from polar to cartesian
            int y = (int)Math.round(greatRadius *  Math.sin(Math.toRadians(azimuth)) + centreY - greatRadius
                    - (correction * Math.sin(Math.toRadians(azimuth)))); //convert from polar to cartesian

            RectF mShapeAux = new RectF(x, y, x + (2 * (int)Math.round(greatRadius)), y + (2 * (int)Math.round(greatRadius)));
            canvas.drawArc(mShapeAux, (360 - (90 + dip - azimuth)) - (180 - (2 * dip)), 180 - (2 * dip), false, paint);

            int poleRadius = 3;

            double poleX = radius * Math.sin(Math.toRadians(90 - azimuth)) *
                    Math.tan(Math.toRadians(45 - ((90 - dip) / 2)))+ centreX - poleRadius;
            double poleY = radius * Math.cos(Math.toRadians(90 - azimuth)) *
                    Math.tan(Math.toRadians(45 - ((90 - dip) / 2)))+ centreY - poleRadius;

            int poleXR = (int)Math.round(poleX);
            int poleYR = (int)Math.round(poleY);

            paint.setStyle(Paint.Style.FILL);
            canvas.drawRect(new RectF(poleXR, poleYR, poleXR + 2 * poleRadius, poleYR + 2 * poleRadius), paint);
            canvas.drawText((i+1)+":s"+(i+1), poleXR, poleYR, paint);
        }

        paint.setColor(Color.BLACK);
        canvas.drawText("W", ((centreX - radius) - 12), centreY, paint);
        canvas.drawText("E", ((centreX + radius) + 10), centreY, paint);
        canvas.drawText("N", centreX, (centreY - radius) - 10, paint);
        canvas.drawText("S", centreX, (centreY + radius) + 12, paint);
    }

    public void init() {
        centreX = 350;
        centreY = 350;
        radius = 300;

        int x = centreX - radius;
        int y = centreY - radius;
        int width = radius * 2;
        int height = radius * 2;

        mGreatCircleBounds = new RectF(x, y, x + width, y + height);

        this.setBackgroundColor(Color.TRANSPARENT);
    }
}
