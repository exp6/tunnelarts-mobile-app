package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.MappingListAdapter;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public class MappingListFragment extends Fragment {

    private static final String TAG = "MappingListFragment";
    protected RecyclerView mRecyclerView;
    protected MappingListAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;

    protected List<Mapping> mMappingDataset;

    //User Selection Context
    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Module selectedModule;

    private OnMappingItemListener mCallback;

    public interface OnMappingItemListener {
        void onMappingSelected(int position);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnMappingItemListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnMappingItemListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mappings, container, false);
        rootView.setTag(TAG);

        String projectName = (selectedProject == null)? "" : selectedProject.getName();
        ((TextView) rootView.findViewById(R.id.project_label)).setText(projectName);

        String tunnelName = (selectedTunnel == null)? "" : selectedTunnel.getName();
        ((TextView) rootView.findViewById(R.id.tunnel_label)).setText(tunnelName);

        String faceName = (selectedFace == null)? "" : selectedFace.getName();
        ((TextView) rootView.findViewById(R.id.face_label)).setText(faceName);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        setRecyclerViewLayoutManager();
        mAdapter = new MappingListAdapter(mMappingDataset, mCallback, mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);
        return rootView;
    }

    public void setRecyclerViewLayoutManager() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMMappingDataset(List<Mapping> mMappingDataset) {
        this.mMappingDataset = mMappingDataset;
    }

    public void setSelectionContext(Project project,
                                    Tunnel tunnel,
                                    Face face,
                                    Module module) {
        selectedProject = project;
        selectedTunnel = tunnel;
        selectedFace = face;
        selectedModule = module;
    }

    public void updateItemList() {
        mAdapter = new MappingListAdapter(mMappingDataset, mCallback, mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);
    }


}
