package cl.skava.mobile.tunnelapp.domain.repository;

import android.content.Context;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Picture;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.MainPresenter;

/**
 * Created by Jose Ignacio Vera on 25-07-2016.
 */
public interface BaseRepository {

    void initRepository(Context activity, MainPresenter callback, User user);
    void setSyncProgress(Integer percentage);
    void notifySyncSegment(String msg);
    void onSyncResult(int code);
    void onNetworkError(String msg);
    void saveData();
    void syncData(User user);
    void syncStorageData(Boolean syncAll, int moduleCode);
    void onSyncStorageResult(int code, Boolean syncAll);
    void syncBasicData(Context activity, MainPresenter presenter, User user);
}
