package cl.skava.mobile.tunnelapp.domain.repository;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public interface ProspectionRepository {

    boolean insert(ProspectionHole prospection);

    boolean update(ProspectionHole prospection);

    boolean delete(ProspectionHole prospection);

    List<ProspectionHole> getList();
}
