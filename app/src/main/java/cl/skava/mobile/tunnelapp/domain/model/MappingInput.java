package cl.skava.mobile.tunnelapp.domain.model;

/**
 * Created by JoseVera on 4/18/2017.
 */

public class MappingInput {

    private String id;
    private String parent;
    private String name;
    private String nameEs;
    private Integer code;
    private String label;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameEs() {
        return nameEs;
    }

    public void setNameEs(String nameEs) {
        this.nameEs = nameEs;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "MappingInput{" +
                "id='" + id + '\'' +
                ", parent='" + parent + '\'' +
                ", name='" + name + '\'' +
                ", nameEs='" + nameEs + '\'' +
                ", code=" + code +
                ", label='" + label + '\'' +
                '}';
    }
}
