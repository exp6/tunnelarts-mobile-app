package cl.skava.mobile.tunnelapp.data.net.cloud.impl.azure.tables;

import android.os.AsyncTask;

import com.google.gson.JsonObject;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.table.MobileServicePreconditionFailedExceptionJson;
import com.microsoft.windowsazure.mobileservices.table.query.Query;
import com.microsoft.windowsazure.mobileservices.table.sync.MobileServiceSyncContext;
import com.microsoft.windowsazure.mobileservices.table.sync.MobileServiceSyncTable;
import com.microsoft.windowsazure.mobileservices.table.sync.localstore.ColumnDataType;
import com.microsoft.windowsazure.mobileservices.table.sync.localstore.MobileServiceLocalStoreException;
import com.microsoft.windowsazure.mobileservices.table.sync.operations.RemoteTableOperationProcessor;
import com.microsoft.windowsazure.mobileservices.table.sync.operations.TableOperation;
import com.microsoft.windowsazure.mobileservices.table.sync.push.MobileServicePushCompletionResult;
import com.microsoft.windowsazure.mobileservices.table.sync.synchandler.MobileServiceSyncHandler;
import com.microsoft.windowsazure.mobileservices.table.sync.synchandler.MobileServiceSyncHandlerException;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudLocalStore;
import cl.skava.mobile.tunnelapp.data.datasource.DbConstants;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;

/**
 * Created by Jose Ignacio Vera on 15-07-2016.
 */
public class SyncClient {

    private MobileServiceClient mClient;
    private CloudMobileService mCallbackService;
    private MobileServiceSyncContext syncContext;

    public SyncClient(CloudMobileService service, MobileServiceClient serviceClient) {
        mClient = serviceClient;
        mCallbackService = service;
    }

    public void initLocalStore(List<String> list) {
        CloudLocalStore localStore = new CloudLocalStore(mClient.getContext(), DbConstants.DATABASE_NAME, null, DbConstants.DATABASE_VERSION);
        MobileServiceSyncHandler handler = new ConflictResolvingSyncHandler();
        syncContext = mClient.getSyncContext();

        for (String item : list) {
            Class c = null;
            try {
                c = Class.forName(item);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            String className = c.getSimpleName();

            Field allFields[] = c.getDeclaredFields();
            Field[] f = filterOutStaticFields(allFields);

            Map<String, ColumnDataType> tableDefinition = new HashMap<String, ColumnDataType>();

            for (Field field : f) {
                tableDefinition.put(field.getName(), SyncClientHelper.translateType(field.getType().getSimpleName()));
            }

            try {
                localStore.defineTable(className, tableDefinition);
            } catch (MobileServiceLocalStoreException e) {
                e.printStackTrace();
            }
        }

        try {
            syncContext.initialize(localStore, handler).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private Field[] filterOutStaticFields(Field[] fields) {
        List<Field> nonStaticFields = new ArrayList<>();
        for (Field field : fields) {
            if(fieldIsNotStatic(field)) {
                nonStaticFields.add(field);
            }
        }
        Field[] fieldStore = new Field[nonStaticFields.size()];
        return nonStaticFields.toArray(fieldStore);
    }

    private boolean fieldIsNotStatic(Field field) {
        int modifiers = field.getModifiers();
        return !Modifier.isStatic(modifiers);
    }

    public void pushAllEntities() {
        new AsyncTask<Void, Void, Void>() {

            private Exception error = null;

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    mClient.getSyncContext().push().get();

                } catch (InterruptedException e) {
                    error = e;
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    error = e;
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void result)
            {
                if(error != null) {
                    mCallbackService.setPushSyncStatus(false, error.getCause().getMessage());
                }
                else {
                    mCallbackService.setPushSyncStatus(true, "");
                }
            }
        }.execute();
    }

    public void pullData(Class c, Query mSyncQuery, final boolean doHandleResponse) {
        final Class mC = c;
        final Query mQuery = mSyncQuery;

        new AsyncTask<Void, Void, Void>() {

            private Exception error = null;

            @Override
            protected Void doInBackground(Void... params) {

                MobileServiceSyncTable mSyncTable = mClient.getSyncTable(mC.getSimpleName(), mC);

                try {
                    mSyncTable.pull(mQuery).get();

                } catch (InterruptedException e) {
                    error = e;
//                    mCallbackService.networkError(e.getCause().getMessage());
                    e.printStackTrace();
                } catch (ExecutionException e) {
                        error = e;
//                    mCallbackService.networkError(e.getCause().getMessage());
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void result)
            {
                if(error != null) {
                    mCallbackService.networkError(error.getCause().getMessage());
                }
                else {
                    if (doHandleResponse) {
                        mCallbackService.handleSyncResponse(true, mC.getSimpleName());
                    }
                }
            }

        }.execute();
    }

    private class ConflictResolvingSyncHandler implements MobileServiceSyncHandler {

        @Override
        public JsonObject executeTableOperation(
                RemoteTableOperationProcessor processor, TableOperation operation)
                throws MobileServiceSyncHandlerException {

            MobileServicePreconditionFailedExceptionJson ex = null;
            JsonObject result = null;
            try {
                result = operation.accept(processor);
            } catch (MobileServicePreconditionFailedExceptionJson e) {
                ex = e;
            } catch (Throwable e) {
                ex = (MobileServicePreconditionFailedExceptionJson) e.getCause();
            }

            if (ex != null) {
                JsonObject serverItem = ex.getValue();

                if (serverItem == null) {
                    try {
                        serverItem = mClient.getTable(operation.getTableName()).lookUp(operation.getItemId()).get();
                    } catch (Exception e) {
                        throw new MobileServiceSyncHandlerException(e);
                    }
                }

                result = serverItem;
            }

            return result;
        }

        @Override
        public void onPushComplete(MobileServicePushCompletionResult result)
                throws MobileServiceSyncHandlerException {
        }
    }
}
