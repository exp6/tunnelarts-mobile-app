package cl.skava.mobile.tunnelapp.data.repository;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Jose Ignacio Vera on 25-07-2016.
 */
public class ChangeLog implements Serializable {

    private String id;
    private Date createdAt;
    private String idClient;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    @Override
    public String toString() {
        return "ChangeLog{" +
                "id='" + id + '\'' +
                ", createdAt=" + createdAt +
                ", idClient='" + idClient + '\'' +
                '}';
    }
}
