package cl.skava.mobile.tunnelapp.domain.repository;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInstance;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public interface MappingInputRepository {

    List<MappingInputModule> getAll();
    List<MappingInputModule> getList(String mappingId);
    MappingInstance getMappingInstance();
    boolean saveMappingInstance(MappingInstance mappingInstance);
    boolean deleteInputs(Mapping mapping);
}
