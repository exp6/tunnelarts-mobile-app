package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInstance;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.recommendations.RockboltsFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.recommendations.ShotcreteFragment;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class SummaryFragment extends Fragment {

    private static final String TAG = "SummaryFragment";

    private MappingInstance finalInput;

    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Mapping selectedMapping;
    private User mUser;
    private Client mClient;

    private TabLayout tabLayout;
    private FloatingActionButton myFab;

    private RockboltsFragment rockboltsFragment;
    private ShotcreteFragment shotcreteFragment;

    private List<MappingInputModule> mMappingInputModuleDataset;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_summary, container, false);
        rootView.setTag(TAG);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.characterization_mapping_input_summ_label_rockbolt)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.characterization_mapping_input_summ_label_shotcrete)));
        tabLayout.setVisibility(View.GONE);

        myFab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        myFab.setVisibility(View.GONE);

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        GeneralReportFragment f = GeneralReportFragment.newInstance();
        f.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedMapping, mUser, mClient);
        ft.replace(R.id.fragReport, f);
        ft.commit();

        ImageView ivRecommendations = (ImageView) rootView.findViewById(R.id.ivQ);
        ivRecommendations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tabLayout.setVisibility(View.VISIBLE);
                myFab.setVisibility(View.VISIBLE);

                TabLayout.Tab tab = tabLayout.getTabAt(0);
                tab.select();

                FragmentTransaction ft = getChildFragmentManager().beginTransaction();
                rockboltsFragment = new RockboltsFragment();
                if(finalInput.getRockbolts().size() < 6) myFab.setVisibility(View.VISIBLE);
                else myFab.setVisibility(View.GONE);
                rockboltsFragment.setMyFab(myFab);
                rockboltsFragment.setRockbolts(finalInput.getRockbolts());
                rockboltsFragment.setRockboltsDeleted(finalInput.getDeletedRockbolts());
                rockboltsFragment.setSelectedMapping(selectedMapping);
                ft.replace(R.id.fragReport, rockboltsFragment);
                ft.commit();

                tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                        Fragment f = getChildFragmentManager().findFragmentById(R.id.fragReport);

                        if(f instanceof RockboltsFragment) {
                            rockboltsFragment = (RockboltsFragment) f;
                            rockboltsFragment.saveData();
                            finalInput.setRockbolts(rockboltsFragment.getRockbolts());
                            finalInput.setDeletedRockbolts(rockboltsFragment.getRockboltsDeleted());
                        }

                        if(f instanceof ShotcreteFragment) {
                            shotcreteFragment = (ShotcreteFragment) f;
                            shotcreteFragment.saveData();
                            finalInput.setShotcretes(shotcreteFragment.getShotcreteList());
                            finalInput.setDeletedShotcretes(shotcreteFragment.getShotcreteListDeleted());
                        }

                        switch (tab.getPosition()) {
                            case 1:
                                shotcreteFragment = new ShotcreteFragment();
                                if(finalInput.getShotcretes().size() < 3) myFab.setVisibility(View.VISIBLE);
                                else myFab.setVisibility(View.GONE);
                                shotcreteFragment.setMyFab(myFab);
                                shotcreteFragment.setShotcreteList(finalInput.getShotcretes());
                                shotcreteFragment.setShotcreteListDeleted(finalInput.getDeletedShotcretes());
                                shotcreteFragment.setSelectedMapping(selectedMapping);
                                transaction.replace(R.id.fragReport, shotcreteFragment);
                                break;
                            default:
                                rockboltsFragment = new RockboltsFragment();
                                if(finalInput.getRockbolts().size() < 6) myFab.setVisibility(View.VISIBLE);
                                else myFab.setVisibility(View.GONE);
                                rockboltsFragment.setMyFab(myFab);
                                rockboltsFragment.setRockbolts(finalInput.getRockbolts());
                                rockboltsFragment.setRockboltsDeleted(finalInput.getDeletedRockbolts());
                                rockboltsFragment.setSelectedMapping(selectedMapping);
                                transaction.replace(R.id.fragReport, rockboltsFragment);
                                break;
                        }
                        transaction.commitAllowingStateLoss();
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                    }
                });

            }
        });

        ImageView ivGeneral = (ImageView) rootView.findViewById(R.id.ivInfo);
        ivGeneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment f1 = getChildFragmentManager().findFragmentById(R.id.fragReport);

                if(f1 instanceof RockboltsFragment) {
                    rockboltsFragment = (RockboltsFragment) f1;
                    rockboltsFragment.saveData();
                    finalInput.setRockbolts(rockboltsFragment.getRockbolts());
                    finalInput.setDeletedRockbolts(rockboltsFragment.getRockboltsDeleted());
                }

                if(f1 instanceof ShotcreteFragment) {
                    shotcreteFragment = (ShotcreteFragment) f1;
                    shotcreteFragment.saveData();
                    finalInput.setShotcretes(shotcreteFragment.getShotcreteList());
                    finalInput.setDeletedShotcretes(shotcreteFragment.getShotcreteListDeleted());
                }

                tabLayout.setVisibility(View.GONE);
                myFab.setVisibility(View.GONE);
                FragmentTransaction ft = getChildFragmentManager().beginTransaction();
                GeneralReportFragment f = GeneralReportFragment.newInstance();
                f.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedMapping, mUser, mClient);
                ft.replace(R.id.fragReport, f);
                ft.commit();
            }
        });

        ImageView ivRMDescription = (ImageView) rootView.findViewById(R.id.ivDiscontinuity);
        ivRMDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment f = getChildFragmentManager().findFragmentById(R.id.fragReport);

                if(f instanceof RockboltsFragment) {
                    rockboltsFragment = (RockboltsFragment) f;
                    rockboltsFragment.saveData();
                    finalInput.setRockbolts(rockboltsFragment.getRockbolts());
                    finalInput.setDeletedRockbolts(rockboltsFragment.getRockboltsDeleted());
                }

                if(f instanceof ShotcreteFragment) {
                    shotcreteFragment = (ShotcreteFragment) f;
                    shotcreteFragment.saveData();
                    finalInput.setShotcretes(shotcreteFragment.getShotcreteList());
                    finalInput.setDeletedShotcretes(shotcreteFragment.getShotcreteListDeleted());
                }

                tabLayout.setVisibility(View.GONE);
                myFab.setVisibility(View.GONE);
                FragmentTransaction ft = getChildFragmentManager().beginTransaction();
                RockMassDescriptionReportFragment frmd = RockMassDescriptionReportFragment.newInstance();
                frmd.setFinalInput(finalInput);
                frmd.setmMappingInputModuleDataset(mMappingInputModuleDataset);
                ft.replace(R.id.fragReport, frmd);
                ft.commit();
            }
        });

        ImageView ivRMClasification = (ImageView) rootView.findViewById(R.id.ivLithology);
        ivRMClasification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment f1 = getChildFragmentManager().findFragmentById(R.id.fragReport);

                if(f1 instanceof RockboltsFragment) {
                    rockboltsFragment = (RockboltsFragment) f1;
                    rockboltsFragment.saveData();
                    finalInput.setRockbolts(rockboltsFragment.getRockbolts());
                    finalInput.setDeletedRockbolts(rockboltsFragment.getRockboltsDeleted());
                }

                if(f1 instanceof ShotcreteFragment) {
                    shotcreteFragment = (ShotcreteFragment) f1;
                    shotcreteFragment.saveData();
                    finalInput.setShotcretes(shotcreteFragment.getShotcreteList());
                    finalInput.setDeletedShotcretes(shotcreteFragment.getShotcreteListDeleted());
                }

                tabLayout.setVisibility(View.GONE);
                myFab.setVisibility(View.GONE);
                FragmentTransaction ft = getChildFragmentManager().beginTransaction();
                RockMassClassificationReportFragment f = RockMassClassificationReportFragment.newInstance();
                f.setQvalue(finalInput.getMappingQValue());
                f.setRmr(finalInput.getMappingRmr());
                f.setqValueDiscontinuities(finalInput.getqValueDiscontinuities());
                f.setqValueInputSelection(finalInput.getqValueInputSelection());
                f.setmMappingInputModuleDataset(mMappingInputModuleDataset);
                ft.replace(R.id.fragReport, f);
                ft.commit();
            }
        });

        ImageView ivStereonet = (ImageView) rootView.findViewById(R.id.ivStereonet);
        ivStereonet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment f1 = getChildFragmentManager().findFragmentById(R.id.fragReport);

                if(f1 instanceof RockboltsFragment) {
                    rockboltsFragment = (RockboltsFragment) f1;
                    rockboltsFragment.saveData();
                    finalInput.setRockbolts(rockboltsFragment.getRockbolts());
                    finalInput.setDeletedRockbolts(rockboltsFragment.getRockboltsDeleted());
                }

                if(f1 instanceof ShotcreteFragment) {
                    shotcreteFragment = (ShotcreteFragment) f1;
                    shotcreteFragment.saveData();
                    finalInput.setShotcretes(shotcreteFragment.getShotcreteList());
                    finalInput.setDeletedShotcretes(shotcreteFragment.getShotcreteListDeleted());
                }

                tabLayout.setVisibility(View.GONE);
                myFab.setVisibility(View.GONE);
                FragmentTransaction ft = getChildFragmentManager().beginTransaction();

                StereonetFragment f = StereonetFragment.newInstance();
                f.setDiscontinuities(finalInput.getMappingDiscontinuities());

                ft.replace(R.id.fragReport, f);
                ft.commit();
            }
        });

        boolean rmDesDisabled = true;
        boolean rmClasDisabled = true;
        boolean stereonetDisabled = true;

        List<MappingInputModule> parents = new ArrayList<>();

        for(MappingInputModule mim : mMappingInputModuleDataset) {
            if(mim.getParent() != null) parents.add(mim.getParent());

            String label = mim.getLabel().trim();
            if(label.equalsIgnoreCase("DISC")) stereonetDisabled = false;
        }

        for(MappingInputModule mim : parents) {
            String label = mim.getLabel().trim();
            if(label.equalsIgnoreCase("RMD")) rmDesDisabled = false;
            if(label.equalsIgnoreCase("RMC")) rmClasDisabled = false;
        }

        if(rmDesDisabled) {
            LinearLayout rmDescriptionL = (LinearLayout) rootView.findViewById(R.id.ivDiscontinuityL);
            rmDescriptionL.setVisibility(View.GONE);
        }

        if(rmClasDisabled) {
            LinearLayout rmClassificationL = (LinearLayout) rootView.findViewById(R.id.ivLithologyL);
            rmClassificationL.setVisibility(View.GONE);
        }

        if(stereonetDisabled) {
            LinearLayout stereonetL = (LinearLayout) rootView.findViewById(R.id.ivStereonetLayout);
            stereonetL.setVisibility(View.GONE);
        }

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setFinalInput(MappingInstance finalInput) {
        this.finalInput = finalInput;
    }

    public void setSelectionContext(Project project,
                                    Tunnel tunnel,
                                    Face face,
                                    Mapping mapping,
                                    User user,
                                    Client client) {
        selectedProject = project;
        selectedTunnel = tunnel;
        selectedFace = face;
        selectedMapping = mapping;
        mUser = user;
        mClient = client;
    }

    public RockboltsFragment getRockboltsFragment() {
        return rockboltsFragment;
    }

    public ShotcreteFragment getShotcreteFragment() {
        return shotcreteFragment;
    }

    public void setmMappingInputModuleDataset(List<MappingInputModule> mMappingInputModuleDataset) {
        this.mMappingInputModuleDataset = mMappingInputModuleDataset;
    }
}
