package cl.skava.mobile.tunnelapp.presentation.ui.adapters.account;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.account.LoginUsersCachedFragment;

/**
 * Created by Jose Ignacio Vera on 25-08-2016.
 */
public class UserCacheListAdapter extends RecyclerView.Adapter<UserCacheListAdapter.ViewHolder> {

    private static final String TAG = "UserCacheListAdapter";
    private List<User> mUserDataSet;
    private LoginUsersCachedFragment.OnUserDataListener mCallback;

    private int selectedItem = 0;
    ViewHolder viewHolderPool[];
    protected RecyclerView mRecyclerView;

    public UserCacheListAdapter(List<User> dataSet,
                             LoginUsersCachedFragment.OnUserDataListener callback,
                             RecyclerView recyclerView) {
        mUserDataSet = dataSet;
        mCallback = callback;
        viewHolderPool = new ViewHolder[mUserDataSet.size()];
        mRecyclerView = recyclerView;
        setHasStableIds(true);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedItem = getLayoutPosition();
                    mCallback.onLoginAccessCache(mUserDataSet.get(selectedItem));

                    for(ViewHolder vh : viewHolderPool) {
                        vh.itemView.setSelected(false);
                        vh.itemView.setBackgroundColor(Color.TRANSPARENT);
                    }

                    notifyDataSetChanged();
                    mRecyclerView.smoothScrollToPosition(selectedItem);
                }
            });
            textView = (TextView) v.findViewById(R.id.user_name);
        }

        public TextView getTextView() {
            return textView;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_login_cached_user_item, viewGroup, false);
        holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        User module = mUserDataSet.get(position);
        viewHolder.getTextView().setText(module.getFirstName() + " " + module.getLastName());
//        if(module.getEnabled()) ((ImageView)viewHolder.itemView.findViewById(R.id.locked_icon)).setVisibility(View.GONE);

        if (viewHolderPool[position] == null)
            viewHolderPool[position] = viewHolder;

        viewHolder.itemView.setSelected(selectedItem == position);
        viewHolder.itemView.setBackgroundColor(viewHolder.itemView.isSelected() ? Color.LTGRAY : Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return mUserDataSet.size();
    }
}
