package cl.skava.mobile.tunnelapp.domain.model.mapping.rmr;

import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInput;
import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInputType;

/**
 * Created by Jose Ignacio Vera on 24-08-2016.
 */
public class RmrInputGroupType extends DataInputType {

    private String id;
    private Integer code;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "RmrInputGroupType{" +
                "id='" + id + '\'' +
                ", code=" + code +
                ", description='" + description + '\'' +
                '}';
    }
}
