package cl.skava.mobile.tunnelapp.presentation.ui.fragment.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.home.ModuleListAdapter;

/**
 * Created by Jose Ignacio Vera on 08-06-2016.
 */
public class ModuleListFragment extends Fragment {

    private static final String TAG = "ModuleListFragment";
    private static final int DATASET_COUNT = 4;

    protected RecyclerView mRecyclerView;
    protected ModuleListAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected List<Module> mModulesDataset;

    public ModuleListFragment() {}

    private OnModuleItemListener mCallback;

    public interface OnModuleItemListener {
        void onModuleSelected(int position);
        void userModulesLoaded();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_faces_modules, container, false);
        rootView.setTag(TAG);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        setRecyclerViewLayoutManager();

        mAdapter = new ModuleListAdapter(mModulesDataset, mCallback, mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);

        mCallback.userModulesLoaded();

        return rootView;
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnModuleItemListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnModuleItemListener");
        }
    }

    public void setRecyclerViewLayoutManager() {
//        int scrollPosition = 0;
//
//        if (mRecyclerView.getLayoutManager() != null) {
//            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
//                    .findFirstCompletelyVisibleItemPosition();
//        }

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.scrollToPosition(scrollPosition);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMModulesDataset(List<Module> mModulesDataset) {
        this.mModulesDataset = mModulesDataset;
    }
}
