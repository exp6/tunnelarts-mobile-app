package cl.skava.mobile.tunnelapp.domain.executor.base;

import cl.skava.mobile.tunnelapp.domain.interactors.base.AbstractInteractor;

/**
 * Created by Jose Ignacio Vera on 15-06-2016.
 *
 * This executor is responsible for running interactors on background threads.
 * <p/>
 */
public interface Executor {

    /**
     * This method should call the interactor's executeModel method and thus start the interactor. This should be called
     * on a background thread as interactors might do lengthy operations.
     *
     * @param interactor The interactor to executeModel.
     */
    void execute(final AbstractInteractor interactor);
}
