package cl.skava.mobile.tunnelapp.data.repository;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.domain.repository.RodRepository;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class RodRepositoryImpl implements RodRepository {

    private List<Rod> rods;
    private CloudStoreService cloudStoreService;
    private Context mActivity;
    private String mPositionId;

    public RodRepositoryImpl(Context activity, String positionId) {
        cloudStoreService = new CloudStoreService(activity);
        mActivity = activity;
        rods = new ArrayList<>();
        mPositionId = positionId;
    }

    @Override
    public boolean insert(Rod rod) {
        rod.setId(UUID.randomUUID().toString().toUpperCase());
        return cloudStoreService.persistEntity(Rod.class, rod);
    }

    @Override
    public boolean update(Rod rod) {
        return cloudStoreService.updateEntity(Rod.class, rod);
    }

    @Override
    public boolean delete(Rod rod) {
        return cloudStoreService.deleteEntity(Rod.class, rod);
    }

    @Override
    public List<Rod> getList() {
        rods = cloudStoreService.getRodsByProspectionHole(mPositionId);
        return rods;
    }
}
