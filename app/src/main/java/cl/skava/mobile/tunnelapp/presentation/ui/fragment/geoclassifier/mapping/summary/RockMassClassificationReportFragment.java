package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueDiscontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.Rmr;

public class RockMassClassificationReportFragment extends Fragment {

    private QValue qvalue;
    private Rmr rmr;

    private List<QValueDiscontinuity> qValueDiscontinuities;
    private QValueInputSelection qValueInputSelection;

    private List<MappingInputModule> mMappingInputModuleDataset;

    public RockMassClassificationReportFragment() {}

    public static RockMassClassificationReportFragment newInstance() {
        RockMassClassificationReportFragment fragment = new RockMassClassificationReportFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_summary_rock_mass_classification_report, container, false);

        if(isModuleEnabled("QMET")) {
            double rqdValue = (double) qvalue.getRqd();
            double jnValue = (double) qvalue.getJn();
            double jwValue = (double) qvalue.getJw();
            double srfValue = (double) qvalue.getSrf();

            double localjn = (qValueInputSelection.getJnIntersection() != null ? (qValueInputSelection.getJnIntersection() ? jnValue*3 : jnValue) : 1);

            double degreeOfJointing = rqdValue/localjn;
            double activeStress = jwValue/srfValue;

            List<Double> jointFrictionsList = new ArrayList<>();
            for(QValueDiscontinuity q : qValueDiscontinuities) {
                jointFrictionsList.add(q.getJr().doubleValue()/q.getJa().doubleValue());
            }

            Double jointFrictionsMax = calculateQMax(jointFrictionsList);
            Double jointFrictionsMin = calculateQMin(jointFrictionsList);
            Double jointFrictionsAvg = calculateQAvg(jointFrictionsList);

            TextView text1 = (TextView) rootView.findViewById(R.id.q_value_min);
            text1.setText(round(qvalue.getqMin(), 4)+"");
            TextView text111 = (TextView) rootView.findViewById(R.id.q_value_avg);
            text111.setText(round(qvalue.getqAvg(), 4)+"");
            TextView text121 = (TextView) rootView.findViewById(R.id.q_value_max);
            text121.setText(round(qvalue.getqMax(), 4)+"");

            TextView text2 = (TextView) rootView.findViewById(R.id.rqd);
            text2.setText(round(rqdValue, 4)+"");
            TextView text3 = (TextView) rootView.findViewById(R.id.jn);
            text3.setText(round(jnValue, 4)+"");
            TextView text6 = (TextView) rootView.findViewById(R.id.jw);
            text6.setText(round(jwValue, 4)+"");
            TextView text7 = (TextView) rootView.findViewById(R.id.srf);
            text7.setText(round(srfValue, 4)+"");
            TextView text8 = (TextView) rootView.findViewById(R.id.rqd_jn);
            text8.setText(round(degreeOfJointing, 4)+"");
            TextView text9 = (TextView) rootView.findViewById(R.id.jr_ja_min);
            text9.setText(round(jointFrictionsMin, 4)+"");
            TextView text99 = (TextView) rootView.findViewById(R.id.jr_ja_avg);
            text99.setText(round(jointFrictionsAvg, 4)+"");
            TextView text999 = (TextView) rootView.findViewById(R.id.jr_ja_max);
            text999.setText(round(jointFrictionsMax, 4)+"");
            TextView text10 = (TextView) rootView.findViewById(R.id.jw_srf);
            text10.setText(round(activeStress, 4)+"");
        }
        else {
            LinearLayout qL = (LinearLayout) rootView.findViewById(R.id.q_layout_title);
            qL.setVisibility(View.GONE);

            LinearLayout qLContent = (LinearLayout) rootView.findViewById(R.id.q_layout_content);
            qLContent.setVisibility(View.GONE);
        }

        if(isModuleEnabled("RMR")) {
            double strengthValue = (double) rmr.getStrength();
            double rqdValue = (double) rmr.getRqd();
            double spacingValue = (double) rmr.getSpacing();
            double persistencValue = (double) rmr.getPersistence();
            double openingValue = (double) rmr.getOpening();
            double roughnessValue = (double) rmr.getRoughness();
            double infillingValue = (double) rmr.getInfilling();
            double weatheringValue = (double) rmr.getWeathering();
            double groundwaterValue = (double) rmr.getGroundwater();
            double orientationDValue = (double) rmr.getOrientation();

            double conditionDiscontinuityValue = !rmr.getConditionEnabled() ? rmr.getCondition() : (persistencValue + openingValue + roughnessValue + infillingValue + weatheringValue);

            double rmrValue = (strengthValue + rqdValue + spacingValue + conditionDiscontinuityValue + groundwaterValue + orientationDValue);

            TextView text11 = (TextView) rootView.findViewById(R.id.rmr_value);
            text11.setText(round(rmrValue, 4)+"");
            TextView text12 = (TextView) rootView.findViewById(R.id.strength_value);
            text12.setText(round(strengthValue, 4)+"");
            TextView text13 = (TextView) rootView.findViewById(R.id.rmr_rqd);
            text13.setText(round(rqdValue, 4)+"");
            TextView text14 = (TextView) rootView.findViewById(R.id.spacing_value);
            text14.setText(round(spacingValue, 4)+"");
            TextView text15 = (TextView) rootView.findViewById(R.id.groundwater);
            text15.setText(round(groundwaterValue, 4)+"");
            TextView text16 = (TextView) rootView.findViewById(R.id.orientationd_value);
            text16.setText(round(orientationDValue, 4)+"");

            LinearLayout l1 = (LinearLayout) rootView.findViewById(R.id.conditions_disc);
            TextView text17 = (TextView) rootView.findViewById(R.id.orientationd_value);
            text17.setText(round(rmr.getCondition(), 4)+"");

            LinearLayout l2 = (LinearLayout) rootView.findViewById(R.id.conditions_disc_guidelines);
            TextView text18 = (TextView) rootView.findViewById(R.id.conditions_disc_guidelines1);
            text18.setText(round(persistencValue, 4)+"");
            TextView text19 = (TextView) rootView.findViewById(R.id.conditions_disc_guidelines2);
            text19.setText(round(openingValue, 4)+"");
            TextView text20 = (TextView) rootView.findViewById(R.id.conditions_disc_guidelines3);
            text20.setText(round(roughnessValue, 4)+"");
            TextView text21 = (TextView) rootView.findViewById(R.id.conditions_disc_guidelines4);
            text21.setText(round(infillingValue, 4)+"");
            TextView text22 = (TextView) rootView.findViewById(R.id.conditions_disc_guidelines5);
            text22.setText(round(weatheringValue, 4)+"");

            if(!rmr.getConditionEnabled()) {
                l1.setVisibility(View.VISIBLE);
                l2.setVisibility(View.GONE);
            }
            else {
                l1.setVisibility(View.GONE);
                l2.setVisibility(View.VISIBLE);
            }
        }
        else {
            LinearLayout rmrL = (LinearLayout) rootView.findViewById(R.id.rmr_layout_title);
            rmrL.setVisibility(View.GONE);

            LinearLayout rmrLContent = (LinearLayout) rootView.findViewById(R.id.rmr_layout_content);
            rmrLContent.setVisibility(View.GONE);
        }

        return rootView;
    }

    private Double calculateQMax(List<Double> items) {
        Double result = 0.0;
        for(Double item : items) {
            if(item > result) result = item;
            else continue;
        }
        return result;
    }

    private Double calculateQMin(List<Double> items) {
        Double result = calculateQMax(items);
        for(Double item : items) {
            if(item < result) result = item;
            else continue;
        }
        return result;
    }

    private Double calculateQAvg(List<Double> items) {
        Double result = 0.0;
        Double total = new Double(items.size());

        for(Double item  : items) {
            result += item;
        }
        return result/total;
    }

    public QValue getQvalue() {
        return qvalue;
    }

    public void setQvalue(QValue qvalue) {
        this.qvalue = qvalue;
    }

    public Rmr getRmr() {
        return rmr;
    }

    public void setRmr(Rmr rmr) {
        this.rmr = rmr;
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private boolean isModuleEnabled(String label) {
        for(MappingInputModule mim : mMappingInputModuleDataset)
            if(mim.getLabel().trim().equalsIgnoreCase(label)) return true;

        return false;
    }

    public void setqValueDiscontinuities(List<QValueDiscontinuity> qValueDiscontinuities) {
        this.qValueDiscontinuities = qValueDiscontinuities;
    }

    public void setqValueInputSelection(QValueInputSelection qValueInputSelection) {
        this.qValueInputSelection = qValueInputSelection;
    }

    public void setmMappingInputModuleDataset(List<MappingInputModule> mMappingInputModuleDataset) {
        this.mMappingInputModuleDataset = mMappingInputModuleDataset;
    }
}
