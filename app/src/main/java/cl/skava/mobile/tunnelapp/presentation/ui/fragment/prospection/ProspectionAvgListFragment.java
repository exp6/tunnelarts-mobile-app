package cl.skava.mobile.tunnelapp.presentation.ui.fragment.prospection;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.prospection.ProspectionAvgListAdapter;

/**
 * Created by Jose Ignacio Vera on 10-11-2016.
 */
public class ProspectionAvgListFragment extends Fragment {

    private static final String TAG = "ProspectionAvgListFragment";

    protected RecyclerView mRecyclerView;
    protected ProspectionAvgListAdapter mAdapter;

    protected RecyclerView.LayoutManager mLayoutManager;

    private ProspectionHoleGroup selectedProspectionHoleGroup;
    private HashMap<ProspectionHole, List<Rod>> rodsByProspection;

    private OnProspectionAvgListener mCallback;

    public interface OnProspectionAvgListener {
        void onCalculationsReady(List<Double> avgDistancesPerPosition,
                                 List<Double> avgQValuesPerPosition,
                                 List<Double> minQValuesPerPosition,
                                 List<Double> maxQValuesPerPosition,
                                 List<String> estimatedRockPerPosition);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnProspectionAvgListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnProspectionAvgListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_prospection_calculations_avg_list, container, false);
        rootView.setTag(TAG);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        setRecyclerViewLayoutManager();

        if(rodsByProspection != null) {
            mAdapter = new ProspectionAvgListAdapter(rodsByProspection, mRecyclerView, this);
            mRecyclerView.setAdapter(mAdapter);
        }

        return rootView;
    }

    public void setRecyclerViewLayoutManager() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void updateItemList() {
        mAdapter = new ProspectionAvgListAdapter(rodsByProspection, mRecyclerView, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void setRodsByProspection(HashMap<ProspectionHole, List<Rod>> rodsByProspection) {
        this.rodsByProspection = rodsByProspection;
    }

    public void onCalculationsReady(List<Double> avgDistancesPerPosition,
                                    List<Double> avgQValuesPerPosition,
                                    List<Double> minQValuesPerPosition,
                                    List<Double> maxQValuesPerPosition,
                                    List<String> estimatedRockPerPosition) {

        mCallback.onCalculationsReady(avgDistancesPerPosition, avgQValuesPerPosition, minQValuesPerPosition, maxQValuesPerPosition, estimatedRockPerPosition);
    }
}
