package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by JoseVera on 5/23/2017.
 */

public class StereonetPicture extends DataInput {

    private String id;
    private String idMapping;
    private String sideName;
    private String remoteUri;
    private String localUri;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public String getSideName() {
        return sideName;
    }

    public void setSideName(String sideName) {
        this.sideName = sideName;
    }

    public String getRemoteUri() {
        return remoteUri;
    }

    public void setRemoteUri(String remoteUri) {
        this.remoteUri = remoteUri;
    }

    public String getLocalUri() {
        return localUri;
    }

    public void setLocalUri(String localUri) {
        this.localUri = localUri;
    }

    @Override
    public String toString() {
        return "StereonetPicture{" +
                "id='" + id + '\'' +
                ", idMapping='" + idMapping + '\'' +
                ", sideName='" + sideName + '\'' +
                ", remoteUri='" + remoteUri + '\'' +
                ", localUri='" + localUri + '\'' +
                '}';
    }
}
