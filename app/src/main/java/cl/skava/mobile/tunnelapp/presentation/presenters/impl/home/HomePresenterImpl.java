package cl.skava.mobile.tunnelapp.presentation.presenters.impl.home;

import android.content.Context;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.repository.FaceRepositoryImpl;
import cl.skava.mobile.tunnelapp.data.repository.FormationUnitRepositoryImpl;
import cl.skava.mobile.tunnelapp.data.repository.ProjectRepositoryImpl;
import cl.skava.mobile.tunnelapp.data.repository.RockQualityRepositoryImpl;
import cl.skava.mobile.tunnelapp.data.repository.TunnelRepositoryImpl;
import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.impl.home.ProjectSelectionInteractorImpl;
import cl.skava.mobile.tunnelapp.domain.interactors.impl.home.TunnelSelectionInteractorImpl;
import cl.skava.mobile.tunnelapp.domain.interactors.impl.home.WelcomingInteractorImpl;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.home.ProjectSelectionInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.home.TunnelSelectionInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.home.WelcomingInteractor;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.repository.FaceRepository;
import cl.skava.mobile.tunnelapp.domain.repository.FormationUnitRepository;
import cl.skava.mobile.tunnelapp.domain.repository.ProjectRepository;
import cl.skava.mobile.tunnelapp.domain.repository.RockQualityRepository;
import cl.skava.mobile.tunnelapp.domain.repository.TunnelRepository;
import cl.skava.mobile.tunnelapp.presentation.presenters.base.AbstractPresenter;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.home.HomePresenter;

/**
 * Created by Jose Ignacio Vera on 16-06-2016.
 */
public class HomePresenterImpl extends AbstractPresenter
        implements HomePresenter,
        WelcomingInteractor.Callback,
        ProjectSelectionInteractor.Callback,
        TunnelSelectionInteractor.Callback {

    private HomePresenter.View mView;
    private ProjectRepository mProjectRepository;
    private TunnelRepository mTunnelRepository;
    private FaceRepository mFaceRepository;
    private FormationUnitRepository mFormationUnitRepository;
    private RockQualityRepository mRockQualityRepository;

    private Context mActivity;

    private User mUser;

    public HomePresenterImpl(Executor executor,
                             MainThread mainThread,
                             View view,
                             Context activity,
                             User user) {
        super(executor, mainThread);
        mView = view;
        mActivity = activity;
        mUser = user;
    }

    @Override
    public void resume() {
        mView.showProgress();

        mProjectRepository = new ProjectRepositoryImpl(mActivity, mUser);
        mTunnelRepository = new TunnelRepositoryImpl(mActivity);
        mFaceRepository = new FaceRepositoryImpl(mActivity);
        mFormationUnitRepository = new FormationUnitRepositoryImpl(mActivity);
        mRockQualityRepository = new RockQualityRepositoryImpl(mActivity);

        WelcomingInteractor interactor = new WelcomingInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mProjectRepository,
                mTunnelRepository,
                mFaceRepository,
                mFormationUnitRepository,
                mRockQualityRepository
        );
        interactor.execute();
    }

    @Override
    public void pause() {}

    @Override
    public void stop() {}

    @Override
    public void destroy() {}

    @Override
    public void onError(String message) {}

    @Override
    public void onProjectListRetrieved(List<Project> projects) {
        mView.hideProgress();
        mView.displayProjectList(projects);
    }

    @Override
    public void onRetrievalFailed(String error) {
        mView.hideProgress();
        onError(error);
    }

    @Override
    public void onClientRetrieved(Client client) {
        mView.setClientData(client);
    }

    @Override
    public void onFormationUnitRetrieved(HashMap<Project, List<FormationUnit>> formationUnits) {
        mView.setFormationUnits(formationUnits);
    }

    @Override
    public void onRockQualityRetrieved(HashMap<Project, List<RockQuality>> rockQualities) {
        mView.setRockQualities(rockQualities);
    }

    @Override
    public void getTunnelsByProject(Project project) {
        ProjectSelectionInteractor interactor = new ProjectSelectionInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mTunnelRepository,
                mFaceRepository,
                project
        );
        interactor.execute();
    }

    @Override
    public void onTunnelListRetrieved(List<Tunnel> tunnels) {
        mView.hideProgress();
        mView.displayTunnelList(tunnels);
    }

    @Override
    public void getFacesByTunnel(Tunnel tunnel) {
        TunnelSelectionInteractor interactor = new TunnelSelectionInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mFaceRepository,
                tunnel
        );
        interactor.execute();
    }

    @Override
    public void onFaceListRetrieved(List<Face> faces) {
        mView.hideProgress();
        mView.displayFaceList(faces);
    }
}
