package cl.skava.mobile.tunnelapp.data.repository;

import android.content.Context;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.repository.RockQualityRepository;

/**
 * Created by Jose Ignacio Vera on 05-01-2017.
 */
public class RockQualityRepositoryImpl implements RockQualityRepository {

    private final HashMap<Project, List<RockQuality>> rockQualityByProject = new HashMap<>();
    private CloudStoreService cloudStoreService;

    public RockQualityRepositoryImpl(Context activity) {
        cloudStoreService = new CloudStoreService(activity);
    }

    @Override
    public HashMap<Project, List<RockQuality>> getRockQualityByProject() {
        List<Project> ps = cloudStoreService.getProjects();

        for(Project p : ps) {
            List<RockQuality> formationUnits = cloudStoreService.getRockQualityByProject(p.getId());
            rockQualityByProject.put(p, formationUnits);
        }

        return rockQualityByProject;
    }
}
