package cl.skava.mobile.tunnelapp.data.exports.impl.xls;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.cache.FileManager;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.data.exports.base.ExportService;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.AdditionalDescription;
import cl.skava.mobile.tunnelapp.domain.model.mapping.AdditionalInformation;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Discontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.ExpandedTunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.FailureZone;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Gsi;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Lithology;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Particularities;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Picture;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RecommendationRockbolt;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RecommendationShotcrete;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMass;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMassHazard;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SelectionDetailLists;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SpecialFeatures;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueDiscontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.Rmr;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;

/**
 * Created by Jose Ignacio Vera on 24-10-2016.
 */
public class ExportServiceImpl implements ExportService {

    private CloudStoreService mCloudStoreService;

    public ExportServiceImpl(CloudStoreService cloudStoreService) {
        mCloudStoreService = cloudStoreService;
    }

    @Override
    public boolean exportCharacterizerData(Project p, Tunnel t, Face f, String directoryPath, SelectionDetailLists selectionDetailLists) {
        List<String> directories = new ArrayList<>();
        directories.add(p.getName().trim());
        directories.add(t.getName().trim());
        directories.add(f.getName().trim());
        directories.add("Characterizer_data");
        File newDir = generateDirectoryTree(directories, directoryPath, "Tunnelarts");
        directories.add("Images");
        File newDirPictures = generateDirectoryTree(directories, directoryPath, "Tunnelarts");

        List<Mapping> mappings = mCloudStoreService.getMappingsByFace(f.getId());
        ChainageNumberFormat chainageNumberFormat = new ChainageNumberFormat();

        Workbook wb = new HSSFWorkbook();

        CellStyle cs = wb.createCellStyle();
        cs.setFillForegroundColor(HSSFColor.SKY_BLUE.index);
        cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        Font font = wb.createFont();
        font.setColor(HSSFColor.WHITE.index);
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        cs.setFont(font);

        CellStyle csTitle = wb.createCellStyle();
        Font fontTitle = wb.createFont();
        fontTitle.setBoldweight(Font.BOLDWEIGHT_BOLD);
        csTitle.setFont(fontTitle);

        CellStyle csSelection = wb.createCellStyle();
        csSelection.setFillForegroundColor(HSSFColor.SEA_GREEN.index);
        csSelection.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        Cell c = null;

        RockMass rockMass;

        List<String[]> rockTypes = new ArrayList<>(7);
        String[] na = {"N/A",""};
        rockTypes.add(0, na);
        rockTypes.add(1, selectionDetailLists.getIgneousPlutonic());
        rockTypes.add(2, selectionDetailLists.getIgneousVolcanic());
        rockTypes.add(3, selectionDetailLists.getPyroclastic());
        rockTypes.add(4, selectionDetailLists.getSedimentaryClastic());
        rockTypes.add(5, selectionDetailLists.getSedimentaryChemical());
        rockTypes.add(6, selectionDetailLists.getMetamorphic());

        for(Mapping mapping : mappings) {
            Sheet sheet1 = wb.createSheet(chainageNumberFormat.format(mapping.getChainageStart()) + " - " + chainageNumberFormat.format(mapping.getChainageEnd()));
            Row row = sheet1.createRow(0);

            c = row.createCell(0);
            c.setCellValue("Rock Mass Description");
            c.setCellStyle(cs);

            for(int i = 1; i <= 20; i++) {
                c = row.createCell(i);
                c.setCellStyle(cs);
            }

            sheet1.setColumnWidth(0, (15 * 500));
            sheet1.setColumnWidth(1, (15 * 500));
            sheet1.setColumnWidth(2, (15 * 500));
            sheet1.setColumnWidth(3, (15 * 500));
            sheet1.setColumnWidth(4, (15 * 500));
            sheet1.setColumnWidth(5, (15 * 500));
            sheet1.setColumnWidth(6, (15 * 500));
            sheet1.setColumnWidth(7, (15 * 500));
            sheet1.setColumnWidth(8, (15 * 500));
            sheet1.setColumnWidth(9, (15 * 500));
            sheet1.setColumnWidth(10, (15 * 500));
            sheet1.setColumnWidth(11, (15 * 500));

            row = sheet1.createRow(2);
            c = row.createCell(0);
            c.setCellValue("Rock Mass Overview");
            c.setCellStyle(csTitle);

            rockMass = (RockMass) mCloudStoreService.getMappingInput(mapping.getId(), RockMass.class);

            c = row.createCell(2);
            c.setCellValue("Brief description");
            c = row.createCell(3);
            c.setCellValue(rockMass.getBriefDesc() == null ? "N/A" : rockMass.getBriefDesc().trim());

            row = sheet1.createRow(3);
            c = row.createCell(0);
            c.setCellValue("Genetic group:");
            c = row.createCell(1);
            c.setCellValue(rockMass.getGeneticGroup() == 0 ? "N/A" : selectionDetailLists.getGeneticGroup()[rockMass.getGeneticGroup()]);

            row = sheet1.createRow(4);
            c = row.createCell(0);
            c.setCellValue("Rock Name:");
            c = row.createCell(1);
            c.setCellValue(rockMass.getRockName() == 0 ? "N/A" : rockTypes.get(rockMass.getGeneticGroup())[rockMass.getRockName()]);

            row = sheet1.createRow(5);
            c = row.createCell(0);
            c.setCellValue("Structure:");
            c = row.createCell(1);
            c.setCellValue(rockMass.getStructure() == 0 ? "N/A" : selectionDetailLists.getStructure()[rockMass.getStructure()]);

            row = sheet1.createRow(6);
            c = row.createCell(0);
            c.setCellValue("Weathering:");
            c = row.createCell(1);
            c.setCellValue(rockMass.getWeathering() == 0 ? "N/A" : selectionDetailLists.getWeathering()[rockMass.getWeathering()]);

            row = sheet1.createRow(7);
            c = row.createCell(0);
            c.setCellValue("Colour value:");
            c = row.createCell(1);
            c.setCellValue(rockMass.getColorValue() == 0 ? "N/A" : selectionDetailLists.getColourValue()[rockMass.getColorValue()]);

            row = sheet1.createRow(8);
            c = row.createCell(0);
            c.setCellValue("Colour chroma:");
            c = row.createCell(1);
            c.setCellValue(rockMass.getColorChroma() == 0 ? "N/A" : selectionDetailLists.getColourChroma()[rockMass.getColorChroma()]);

            row = sheet1.createRow(9);
            c = row.createCell(0);
            c.setCellValue("Colour hue:");
            c = row.createCell(1);
            c.setCellValue(rockMass.getColorHue() == 0 ? "N/A" : selectionDetailLists.getColourHue()[rockMass.getColorHue()]);

            row = sheet1.createRow(10);
            c = row.createCell(0);
            c.setCellValue("Jointing:");
            c = row.createCell(1);
            c.setCellValue(rockMass.getJointing() == 0 ? "N/A" : selectionDetailLists.getJointing()[rockMass.getJointing()]);

            row = sheet1.createRow(11);
            c = row.createCell(0);
            c.setCellValue("Strength:");
            c = row.createCell(1);
            c.setCellValue(rockMass.getStrength() == 0 ? "N/A" : selectionDetailLists.getStrength()[rockMass.getStrength()]);

            row = sheet1.createRow(12);
            c = row.createCell(0);
            c.setCellValue("Water:");
            c = row.createCell(1);
            c.setCellValue(rockMass.getWater() == 0 ? "N/A" : selectionDetailLists.getWaterDropdown()[rockMass.getWater()]);

            row = sheet1.createRow(13);
            c = row.createCell(0);
            c.setCellValue("Water T°:");
            c = row.createCell(1);
            c.setCellValue(rockMass.getWaterT());

            row = sheet1.createRow(14);
            c = row.createCell(0);
            c.setCellValue("Water quality:");
            c = row.createCell(1);
            c.setCellValue(rockMass.getWaterQuality() == 0 ? "N/A" : selectionDetailLists.getWaterQuality()[rockMass.getWaterQuality()]);

            row = sheet1.createRow(15);
            c = row.createCell(0);
            c.setCellValue("Inflow [l/min]");
            c = row.createCell(1);
            c.setCellValue(rockMass.getInflow());

            row = sheet1.createRow(17);
            c = row.createCell(0);
            c.setCellValue("Lithologies");
            c.setCellStyle(csTitle);

            List<Lithology> lithologies = (List<Lithology>) mCloudStoreService.getMappingInputs(mapping.getId(), Lithology.class);

            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue("Lithology " + (i+1));
                c.setCellStyle(csTitle);
            }

            row = sheet1.createRow(18);
            c = row.createCell(0);
            c.setCellValue("Presence:");
            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(lithologies.get(i).getPresence() + "%");
            }

            row = sheet1.createRow(19);
            c = row.createCell(0);
            c.setCellValue("Genetic group:");
            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(lithologies.get(i).getGeneticGroup() == 0 ? "N/A" : selectionDetailLists.getGeneticGroup()[lithologies.get(i).getGeneticGroup()]);
            }

            row = sheet1.createRow(20);
            c = row.createCell(0);
            c.setCellValue("Rock Name:");
            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(lithologies.get(i).getSubGroup() == 0 ? "N/A" : rockTypes.get(lithologies.get(i).getGeneticGroup())[lithologies.get(i).getSubGroup()]);
            }

            row = sheet1.createRow(21);
            c = row.createCell(0);
            c.setCellValue("Formation/Unit: ");
            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue("N/A");
            }

            row = sheet1.createRow(22);
            c = row.createCell(0);
            c.setCellValue("Weathering: ");
            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(lithologies.get(i).getAlterationWeathering() == 0 ? "N/A" : selectionDetailLists.getWeathering()[lithologies.get(i).getAlterationWeathering()]);
            }

            row = sheet1.createRow(23);
            c = row.createCell(0);
            c.setCellValue("Colour value:");
            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(lithologies.get(i).getColourValue() == 0 ? "N/A" : selectionDetailLists.getColourValue()[lithologies.get(i).getColourValue()]);
            }

            row = sheet1.createRow(24);
            c = row.createCell(0);
            c.setCellValue("Colour chroma:");
            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(lithologies.get(i).getColourChroma() == 0 ? "N/A" : selectionDetailLists.getColourChroma()[lithologies.get(i).getColourChroma()]);
            }

            row = sheet1.createRow(25);
            c = row.createCell(0);
            c.setCellValue("Colour hue:");
            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(lithologies.get(i).getColorHue() == 0 ? "N/A" : selectionDetailLists.getColourHue()[lithologies.get(i).getColorHue()]);
            }

            row = sheet1.createRow(26);
            c = row.createCell(0);
            c.setCellValue("Strength [Mpa]:");
            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(lithologies.get(i).getStrength() == 0 ? "N/A" : selectionDetailLists.getStrength()[lithologies.get(i).getStrength()]);
            }

            row = sheet1.createRow(27);
            c = row.createCell(0);
            c.setCellValue("Texture:");
            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(lithologies.get(i).getTexture() == 0 ? "N/A" : selectionDetailLists.getTexture()[lithologies.get(i).getTexture()]);
            }

            row = sheet1.createRow(28);
            c = row.createCell(0);
            c.setCellValue("Grain size [mm]:");
            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(lithologies.get(i).getGrainSize() == 0 ? "N/A" : selectionDetailLists.getGrainSize()[lithologies.get(i).getGrainSize()]);
            }

            row = sheet1.createRow(29);
            c = row.createCell(0);
            c.setCellValue("J(v) (joints/m3) min-max:");
            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(lithologies.get(i).getJvMin() + " - " + lithologies.get(i).getJvMax());
            }

            row = sheet1.createRow(30);
            c = row.createCell(0);
            c.setCellValue("Block shape:");
            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(lithologies.get(i).getBlockShape() == 0 ? "N/A" : selectionDetailLists.getBlockShape2()[lithologies.get(i).getBlockShape()]);
            }

            row = sheet1.createRow(31);
            c = row.createCell(0);
            c.setCellValue("Size (LxWxH) [m]:");
            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(lithologies.get(i).getBlockSizeOne() + " x " + lithologies.get(i).getBlockSizeTwo() + " x " + lithologies.get(i).getBlockSizeThree());
            }

            row = sheet1.createRow(33);
            c = row.createCell(0);
            c.setCellValue("Brief description");
            c.setCellStyle(csTitle);

            for(int i = 0; i < lithologies.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(lithologies.get(i).getBriefDesc() == null ? "N/A" : lithologies.get(i).getBriefDesc().trim());
            }

            row = sheet1.createRow(35);
            c = row.createCell(0);
            c.setCellValue("Discontinuities");
            c.setCellStyle(csTitle);

            List<Discontinuity> discontinuities = (List<Discontinuity>) mCloudStoreService.getMappingInputs(mapping.getId(), Discontinuity.class);

            for(int i = 0; i < discontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue("Discontinuity " + (i+1));
                c.setCellStyle(csTitle);
            }

            row = sheet1.createRow(36);
            c = row.createCell(0);
            c.setCellValue("Type:");

            for(int i = 0; i < discontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(discontinuities.get(i).getType() == 0 ? "N/A" : selectionDetailLists.getType()[discontinuities.get(i).getType()]);
            }

            row = sheet1.createRow(37);
            c = row.createCell(0);
            c.setCellValue("Relevance:");

            for(int i = 0; i < discontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(discontinuities.get(i).getRelevance() == 0 ? "N/A" : selectionDetailLists.getRelevance()[discontinuities.get(i).getRelevance()]);
            }

            row = sheet1.createRow(38);
            c = row.createCell(0);
            c.setCellValue("Orientation DD:");

            for(int i = 0; i < discontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(discontinuities.get(i).getOrientationDd());
            }

            row = sheet1.createRow(39);
            c = row.createCell(0);
            c.setCellValue("Orientation D:");

            for(int i = 0; i < discontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(discontinuities.get(i).getOrientationD());
            }

            row = sheet1.createRow(40);
            c = row.createCell(0);
            c.setCellValue("Spacing:");

            for(int i = 0; i < discontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(discontinuities.get(i).getSpacing() == 0 ? "N/A" : selectionDetailLists.getSpacing()[discontinuities.get(i).getSpacing()]);
            }

            row = sheet1.createRow(41);
            c = row.createCell(0);
            c.setCellValue("Persistence:");

            for(int i = 0; i < discontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(discontinuities.get(i).getPersistence() == 0 ? "N/A" : selectionDetailLists.getPersistence()[discontinuities.get(i).getPersistence()]);
            }

            row = sheet1.createRow(42);
            c = row.createCell(0);
            c.setCellValue("Opening:");

            for(int i = 0; i < discontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(discontinuities.get(i).getOpening() == 0 ? "N/A" : selectionDetailLists.getOpening()[discontinuities.get(i).getOpening()]);
            }

            row = sheet1.createRow(43);
            c = row.createCell(0);
            c.setCellValue("Roughness:");

            for(int i = 0; i < discontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(discontinuities.get(i).getRoughness() == 0 ? "N/A" : selectionDetailLists.getRoughness()[discontinuities.get(i).getRoughness()]);
            }

            row = sheet1.createRow(44);
            c = row.createCell(0);
            c.setCellValue("Slickensided:");

            for(int i = 0; i < discontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(discontinuities.get(i).getSlickensided() == 0 ? "N/A" : selectionDetailLists.getYesNo()[discontinuities.get(i).getSlickensided()]);
            }

            row = sheet1.createRow(45);
            c = row.createCell(0);
            c.setCellValue("Infilling:");

            for(int i = 0; i < discontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(discontinuities.get(i).getInfilling() == 0 ? "N/A" : selectionDetailLists.getInfilling()[discontinuities.get(i).getInfilling()]);
            }

            row = sheet1.createRow(46);
            c = row.createCell(0);
            c.setCellValue("Infilling Type:");

            for(int i = 0; i < discontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(discontinuities.get(i).getInfillingType() == 0 ? "N/A" : selectionDetailLists.getInfillingType()[discontinuities.get(i).getInfillingType()]);
            }

            row = sheet1.createRow(47);
            c = row.createCell(0);
            c.setCellValue("Weathering:");

            for(int i = 0; i < discontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(discontinuities.get(i).getWeathering() == 0 ? "N/A" : selectionDetailLists.getWeathering()[discontinuities.get(i).getWeathering()]);
            }

            AdditionalInformation ai = (AdditionalInformation) mCloudStoreService.getMappingInput(mapping.getId(), AdditionalInformation.class);
            SpecialFeatures sf = (SpecialFeatures) mCloudStoreService.getSubMappingInput(ai.getId(), SpecialFeatures.class);
            RockMassHazard rmh = (RockMassHazard) mCloudStoreService.getSubMappingInput(ai.getId(), RockMassHazard.class);

            row = sheet1.createRow(49);
            c = row.createCell(0);
            c.setCellValue("Additional Information");
            c.setCellStyle(csTitle);

            row = sheet1.createRow(50);
            c = row.createCell(0);
            c.setCellValue("Special Features");
            c.setCellStyle(csTitle);

            c = row.createCell(1);
            c.setCellValue("Rock Mass Hazard");
            c.setCellStyle(csTitle);

            row = sheet1.createRow(51);
            c = row.createCell(0);
            c.setCellValue("Zeolites");
            if(sf.getZeolites()) c.setCellStyle(csSelection);

            c = row.createCell(1);
            c.setCellValue("Rock Burst");
            if(rmh.getRockBurst()) c.setCellStyle(csSelection);

            row = sheet1.createRow(52);
            c = row.createCell(0);
            c.setCellValue("Clay");
            if(sf.getClay()) c.setCellStyle(csSelection);

            c = row.createCell(1);
            c.setCellValue("Swelling");
            if(rmh.getSwelling()) c.setCellStyle(csSelection);

            row = sheet1.createRow(53);
            c = row.createCell(0);
            c.setCellValue("Chlorite");
            if(sf.getChlorite()) c.setCellStyle(csSelection);

            c = row.createCell(1);
            c.setCellValue("Squeezing");
            if(rmh.getSqueezing()) c.setCellStyle(csSelection);

            row = sheet1.createRow(54);
            c = row.createCell(0);
            c.setCellValue("Red Tuff");
            if(sf.getRedTuff()) c.setCellStyle(csSelection);

            c = row.createCell(1);
            c.setCellValue("Slaking");
            if(rmh.getSlaking()) c.setCellStyle(csSelection);

            row = sheet1.createRow(55);
            c = row.createCell(0);
            c.setCellValue("Sulfides");
            if(sf.getSulfides()) c.setCellStyle(csSelection);

            row = sheet1.createRow(56);
            c = row.createCell(0);
            c.setCellValue("Sulfates");
            if(sf.getSulfates()) c.setCellStyle(csSelection);

            FailureZone failureZone = (FailureZone) mCloudStoreService.getMappingInput(mapping.getId(), FailureZone.class);

            row = sheet1.createRow(58);
            c = row.createCell(0);
            c.setCellValue("Fault Zone Description");
            c.setCellStyle(csTitle);

            row = sheet1.createRow(59);
            c = row.createCell(0);
            c.setCellValue("Sense of movement:");
            c = row.createCell(1);
            c.setCellValue(failureZone.getSenseOfMovement() == 0 ? "N/A" : selectionDetailLists.getSenseOfMovement()[failureZone.getSenseOfMovement()]);

            row = sheet1.createRow(60);
            c = row.createCell(0);
            c.setCellValue("Rake of striae:");
            c = row.createCell(1);
            c.setCellValue(failureZone.getRakeOfStriae());

            row = sheet1.createRow(61);
            c = row.createCell(0);
            c.setCellValue("Orientation DD:");
            c = row.createCell(1);
            c.setCellValue(failureZone.getOrientationDd());

            row = sheet1.createRow(62);
            c = row.createCell(0);
            c.setCellValue("Orientation D:");
            c = row.createCell(1);
            c.setCellValue(failureZone.getOrientationD());

            row = sheet1.createRow(63);
            c = row.createCell(0);
            c.setCellValue("Thickness:");
            c = row.createCell(1);
            c.setCellValue(failureZone.getThickness());

            row = sheet1.createRow(64);
            c = row.createCell(0);
            c.setCellValue("Matrix/Block:");
            c = row.createCell(1);
            c.setCellValue(failureZone.getMatrixBlock());

            row = sheet1.createRow(65);
            c = row.createCell(0);
            c.setCellValue("Matrix colour value:");
            c = row.createCell(1);
            c.setCellValue(failureZone.getMatrixColourValue() == 0 ? "N/A" : selectionDetailLists.getColourValue()[failureZone.getMatrixColourValue()]);

            row = sheet1.createRow(66);
            c = row.createCell(0);
            c.setCellValue("Matrix colour chroma:");
            c = row.createCell(1);
            c.setCellValue(failureZone.getMatrixColourChroma() == 0 ? "N/A" : selectionDetailLists.getColourChroma()[failureZone.getMatrixColourChroma()]);

            row = sheet1.createRow(67);
            c = row.createCell(0);
            c.setCellValue("Matrix colour hue:");
            c = row.createCell(1);
            c.setCellValue(failureZone.getMatrixColourHue() == 0 ? "N/A" : selectionDetailLists.getColourHue()[failureZone.getMatrixColourHue()]);

            row = sheet1.createRow(68);
            c = row.createCell(0);
            c.setCellValue("Matrix grain size [mm]:");
            c = row.createCell(1);
            c.setCellValue(failureZone.getMatrixGainSize() == 0 ? "N/A" : selectionDetailLists.getGrainSize()[failureZone.getMatrixGainSize()]);

            row = sheet1.createRow(69);
            c = row.createCell(0);
            c.setCellValue("Block size:");
            c = row.createCell(1);
            c.setCellValue(failureZone.getBlockSize() == 0 ? "N/A" : selectionDetailLists.getBlockSize()[failureZone.getBlockSize()]);

            row = sheet1.createRow(70);
            c = row.createCell(0);
            c.setCellValue("Block shape:");
            c = row.createCell(1);
            c.setCellValue(failureZone.getBlockShape() == 0 ? "N/A" : selectionDetailLists.getBlockShape()[failureZone.getBlockShape()]);

            row = sheet1.createRow(71);
            c = row.createCell(0);
            c.setCellValue("Block genetic group:");
            c = row.createCell(1);
            c.setCellValue(failureZone.getBlockGeneticGroup() == 0 ? "N/A" : selectionDetailLists.getGeneticGroup()[failureZone.getBlockGeneticGroup()]);

            row = sheet1.createRow(72);
            c = row.createCell(0);
            c.setCellValue("Block sub-group:");
            c = row.createCell(1);
            c.setCellValue(failureZone.getBlockSubGroup() == 0 ? "N/A" : rockTypes.get(failureZone.getBlockGeneticGroup())[failureZone.getBlockSubGroup()]);

            Particularities particularities = (Particularities) mCloudStoreService.getMappingInput(mapping.getId(), Particularities.class);

            row = sheet1.createRow(74);
            c = row.createCell(0);
            c.setCellValue("Particularities");
            c.setCellStyle(csTitle);

            row = sheet1.createRow(75);
            c = row.createCell(0);
            c.setCellValue("Instability");
            c.setCellStyle(csTitle);

            row = sheet1.createRow(76);
            c = row.createCell(0);
            c.setCellValue("Caused by:");
            c = row.createCell(1);
            c.setCellValue(particularities.getInstabilityCausedBy() == null ? "N/A" : particularities.getInstabilityCausedBy().trim());

            row = sheet1.createRow(77);
            c = row.createCell(0);
            c.setCellValue("Location:");
            c = row.createCell(1);
            c.setCellValue(particularities.getInstabilityLocation() == null ? "N/A" : particularities.getInstabilityLocation().trim());

            row = sheet1.createRow(79);
            c = row.createCell(0);
            c.setCellValue("Overbreak");
            c.setCellStyle(csTitle);

            row = sheet1.createRow(80);
            c = row.createCell(0);
            c.setCellValue("Caused by:");
            c = row.createCell(1);
            c.setCellValue(particularities.getOverbreakCausedBy() == null ? "N/A" : particularities.getOverbreakCausedBy().trim());

            row = sheet1.createRow(81);
            c = row.createCell(0);
            c.setCellValue("Location:");
            c = row.createCell(1);
            c.setCellValue("Start:");
            c = row.createCell(2);
            c.setCellValue(particularities.getOverbreakLocationStart() == null ? "N/A" : particularities.getOverbreakLocationStart().trim());
            c = row.createCell(3);
            c.setCellValue("End:");
            c = row.createCell(4);
            c.setCellValue(particularities.getOverbreakLocationEnd() == null ? "N/A" : particularities.getOverbreakLocationEnd().trim());

            row = sheet1.createRow(82);
            c = row.createCell(0);
            c.setCellValue("Volume:");
            c = row.createCell(1);
            c.setCellValue(particularities.getOverbreakVolume());

            row = sheet1.createRow(85);
            c = row.createCell(0);
            c.setCellValue("Rock Mass Classification");
            c.setCellStyle(cs);

            for(int i = 1; i <= 20; i++) {
                c = row.createCell(i);
                c.setCellStyle(cs);
            }

            QValue qv = (QValue) mCloudStoreService.getMappingInput(mapping.getId(), QValue.class);

            row = sheet1.createRow(87);
            c = row.createCell(0);
            c.setCellValue("Q-Method (Barton, 2013)");
            c.setCellStyle(csTitle);

            row = sheet1.createRow(88);
            c = row.createCell(0);
            c.setCellValue("Q Min");

            c = row.createCell(1);
            c.setCellValue("Q Avg");

            c = row.createCell(2);
            c.setCellValue("Q Max");

            row = sheet1.createRow(89);
            c = row.createCell(0);
            c.setCellValue(qv.getqMin());

            c = row.createCell(1);
            c.setCellValue(qv.getqAvg());

            c = row.createCell(2);
            c.setCellValue(qv.getqMax());

            row = sheet1.createRow(91);
            c = row.createCell(0);
            c.setCellValue("RQD");
            c = row.createCell(1);
            c.setCellValue(qv.getRqd());

            row = sheet1.createRow(92);
            c = row.createCell(0);
            c.setCellValue("Jn");
            c = row.createCell(1);
            c.setCellValue(qv.getJn());
            c = row.createCell(3);
            c.setCellValue("Type:");
            c = row.createCell(4);
            c.setCellValue(qv.getJnType() == 2 ? "Portal" : (qv.getJnType() == 1 ? "Intersection" : "None"));

            List<QValueDiscontinuity> qValueDiscontinuities = new ArrayList<>();
            if(!qv.getId().trim().isEmpty()) qValueDiscontinuities = mCloudStoreService.getQValueDiscontinuities(qv.getId());

            row = sheet1.createRow(94);
            for(int i = 0; i < qValueDiscontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue("Discontinuity " + (i+1));
            }

            row = sheet1.createRow(95);
            c = row.createCell(0);
            c.setCellValue("Jr");
            for(int i = 0; i < qValueDiscontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(qValueDiscontinuities.get(i).getJr());
            }

            row = sheet1.createRow(96);
            c = row.createCell(0);
            c.setCellValue("Ja");
            for(int i = 0; i < qValueDiscontinuities.size(); i++) {
                c = row.createCell(i+1);
                c.setCellValue(qValueDiscontinuities.get(i).getJa());
            }

            row = sheet1.createRow(98);
            c = row.createCell(0);
            c.setCellValue("Jw");
            c = row.createCell(1);
            c.setCellValue(qv.getJw());

            row = sheet1.createRow(99);
            c = row.createCell(0);
            c.setCellValue("SRF");
            c = row.createCell(1);
            c.setCellValue(qv.getSrf());

            Rmr rmr = (Rmr) mCloudStoreService.getMappingInput(mapping.getId(), Rmr.class);

            row = sheet1.createRow(101);
            c = row.createCell(0);
            c.setCellValue("RMR (Bieniawski, 1989)");
            c.setCellStyle(csTitle);

            row = sheet1.createRow(103);
            c = row.createCell(0);
            c.setCellValue("Strength of intact rock material");
            c = row.createCell(1);
            c.setCellValue(rmr.getStrength());

            row = sheet1.createRow(104);
            c = row.createCell(0);
            c.setCellValue("Drill core quality RQD");
            c = row.createCell(1);
            c.setCellValue(rmr.getRqd());

            row = sheet1.createRow(105);
            c = row.createCell(0);
            c.setCellValue("Spacing of discontinuities");
            c = row.createCell(1);
            c.setCellValue(rmr.getSpacing());

            row = sheet1.createRow(106);
            c = row.createCell(0);
            c.setCellValue("Groundwater");
            c = row.createCell(1);
            c.setCellValue(rmr.getGroundwater());

            row = sheet1.createRow(107);
            c = row.createCell(0);
            c.setCellValue("Orientation of discontinuities");
            c = row.createCell(1);
            c.setCellValue(rmr.getOrientation());

            row = sheet1.createRow(109);
            c = row.createCell(0);
            c.setCellValue("Condition of discontinuities");

            if(!rmr.getConditionEnabled()) {
                c = row.createCell(1);
                c.setCellValue(rmr.getCondition());
            }
            else {
                row = sheet1.createRow(110);
                c = row.createCell(0);
                c.setCellValue("Discontinuity length (persistence)");
                c = row.createCell(1);
                c.setCellValue("Separation (aperture)");
                c = row.createCell(2);
                c.setCellValue("Roughness");
                c = row.createCell(3);
                c.setCellValue("Infilling (gouge)");
                c = row.createCell(4);
                c.setCellValue("Weathering");

                row = sheet1.createRow(111);
                c = row.createCell(0);
                c.setCellValue(rmr.getPersistence());
                c = row.createCell(1);
                c.setCellValue(rmr.getOpening());
                c = row.createCell(2);
                c.setCellValue(rmr.getRoughness());
                c = row.createCell(3);
                c.setCellValue(rmr.getInfilling());
                c = row.createCell(4);
                c.setCellValue(rmr.getWeathering());
            }

            AdditionalDescription ad = (AdditionalDescription) mCloudStoreService.getMappingInput(mapping.getId(), AdditionalDescription.class);

            row = sheet1.createRow(113);
            c = row.createCell(0);
            c.setCellValue("General Comments");
            c.setCellStyle(cs);

            for(int i = 1; i <= 20; i++) {
                c = row.createCell(i);
                c.setCellStyle(cs);
            }

            row = sheet1.createRow(115);
            c = row.createCell(0);
            c.setCellValue("Mapping Conditions");
            c.setCellStyle(csTitle);

            row = sheet1.createRow(116);
            c = row.createCell(0);
            c.setCellValue("Distance to face:");
            c = row.createCell(1);
            c.setCellValue(ad.getDistanceToFace() == 0 ? "N/A" : selectionDetailLists.getFaceDistance()[ad.getDistanceToFace()]);

            row = sheet1.createRow(117);
            c = row.createCell(0);
            c.setCellValue("Responsible for restricted area:");
            c = row.createCell(1);
            c.setCellValue(ad.getResponsibleForRestrictedArea() == 0 ? "N/A" : selectionDetailLists.getResponsible()[ad.getResponsibleForRestrictedArea()]);

            row = sheet1.createRow(118);
            c = row.createCell(0);
            c.setCellValue("Reason for restricted area:");
            c = row.createCell(1);
            c.setCellValue(ad.getReasonForRestrictedArea() == 0 ? "N/A" : selectionDetailLists.getReason()[ad.getReasonForRestrictedArea()]);

            row = sheet1.createRow(119);
            c = row.createCell(0);
            c.setCellValue("Light quality:");
            c = row.createCell(1);
            c.setCellValue(ad.getLightQuality() == 0 ? "N/A" : selectionDetailLists.getLight()[ad.getLightQuality()]);

            row = sheet1.createRow(120);
            c = row.createCell(0);
            c.setCellValue("Air quality:");
            c = row.createCell(1);
            c.setCellValue(ad.getAirQuality() == 0 ? "N/A" : selectionDetailLists.getAir()[ad.getAirQuality()]);

            row = sheet1.createRow(121);
            c = row.createCell(0);
            c.setCellValue("Available time (min):");
            c = row.createCell(1);
            c.setCellValue(ad.getAvailableTime());

            row = sheet1.createRow(123);
            c = row.createCell(0);
            c.setCellValue("Other information");
            c.setCellStyle(csTitle);

            row = sheet1.createRow(124);
            c = row.createCell(0);
            c.setCellValue("New damage 40[m] behind:");
            c = row.createCell(1);
            c.setCellValue(ad.getNewDamageBehind() == 0 ? "N/A" : selectionDetailLists.getYesNo()[ad.getNewDamageBehind()]);

            row = sheet1.createRow(125);
            c = row.createCell(0);
            c.setCellValue("Description:");
            c = row.createCell(1);
            c.setCellValue(ad.getDescription());

            row = sheet1.createRow(127);
            c = row.createCell(0);
            c.setCellValue("Recommendations");
            c.setCellStyle(cs);

            for(int i = 1; i <= 20; i++) {
                c = row.createCell(i);
                c.setCellStyle(cs);
            }

            row = sheet1.createRow(128);
            c = row.createCell(0);
            c.setCellValue("Rockbolts");
            c.setCellStyle(csTitle);
            c = row.createCell(8);
            c.setCellValue("Time Zone (hr)");
            c.setCellStyle(csTitle);
            c = row.createCell(10);
            c.setCellValue("Position");
            c.setCellStyle(csTitle);

            row = sheet1.createRow(129);
            c = row.createCell(0);
            c.setCellValue("Bolts");
            c = row.createCell(1);
            c.setCellValue("Type");
            c = row.createCell(2);
            c.setCellValue("Quantity");
            c = row.createCell(3);
            c.setCellValue("PK");
            c = row.createCell(4);
            c.setCellValue("Length (m)");
            c = row.createCell(5);
            c.setCellValue("Diameter (inches)");
            c = row.createCell(6);
            c.setCellValue("Spacing (cm)");
            c = row.createCell(7);
            c.setCellValue("Angle (°)");
            c = row.createCell(8);
            c.setCellValue("From");
            c = row.createCell(9);
            c.setCellValue("To");
            c = row.createCell(10);
            c.setCellValue("Horizontal (m)");
            c = row.createCell(11);
            c.setCellValue("Vertical (m)");

            int index = 129;

            List<RecommendationRockbolt> rockbolts = (List<RecommendationRockbolt>) mCloudStoreService.getMappingInputs(mapping.getId(), RecommendationRockbolt.class);

            for(int i = 0; i < rockbolts.size(); i++) {
                row = sheet1.createRow(index + (i + 1));
                c = row.createCell(0);
                c.setCellValue(rockbolts.get(i).getPosition()+1);
                c = row.createCell(1);
                c.setCellValue(rockbolts.get(i).getType() == 0 ? "N/A" : selectionDetailLists.getTypeOfBolt()[rockbolts.get(i).getType()]);
                c = row.createCell(2);
                c.setCellValue(rockbolts.get(i).getQuantity());
                c = row.createCell(3);
                c.setCellValue(rockbolts.get(i).getPk());
                c = row.createCell(4);
                c.setCellValue(rockbolts.get(i).getLength());
                c = row.createCell(5);
                c.setCellValue(rockbolts.get(i).getDiameter() == 0 ? "N/A" : selectionDetailLists.getBoltsDiameter()[rockbolts.get(i).getDiameter()]);
                c = row.createCell(6);
                c.setCellValue(rockbolts.get(i).getSpacing());
                c = row.createCell(7);
                c.setCellValue(rockbolts.get(i).getAngle());
                c = row.createCell(8);
                c.setCellValue(rockbolts.get(i).getTimeFrom());
                c = row.createCell(9);
                c.setCellValue(rockbolts.get(i).getTimeTo());
                c = row.createCell(10);
                c.setCellValue(rockbolts.get(i).getHorizontal());
                c = row.createCell(11);
                c.setCellValue(rockbolts.get(i).getVertical());
            }

            index += rockbolts.size()+2;

            row = sheet1.createRow(++index);
            c = row.createCell(0);
            c.setCellValue("Shotcrete");
            c.setCellStyle(csTitle);
            c = row.createCell(5);
            c.setCellValue("Time Zone (hr)");
            c.setCellStyle(csTitle);

            row = sheet1.createRow(++index);
            c = row.createCell(0);
            c.setCellValue("Shotcrete");
            c = row.createCell(1);
            c.setCellValue("Thickness (cm)");
            c = row.createCell(2);
            c.setCellValue("Fiber reinforced");
            c = row.createCell(3);
            c.setCellValue("PK");
            c = row.createCell(4);
            c.setCellValue("Location");
            c = row.createCell(5);
            c.setCellValue("From");
            c = row.createCell(6);
            c.setCellValue("To");

            List<RecommendationShotcrete> shotcretes = (List<RecommendationShotcrete>) mCloudStoreService.getMappingInputs(mapping.getId(), RecommendationShotcrete.class);

            for(int i = 0; i < shotcretes.size(); i++) {
                row = sheet1.createRow(index + (i + 1));
                c = row.createCell(0);
                c.setCellValue(shotcretes.get(i).getPosition()+1);
                c = row.createCell(1);
                c.setCellValue(shotcretes.get(i).getThickness());
                c = row.createCell(2);
                c.setCellValue(shotcretes.get(i).getFiberReinforced() == 0 ? "N/A" : selectionDetailLists.getYesNo()[shotcretes.get(i).getFiberReinforced()]);
                c = row.createCell(3);
                c.setCellValue(shotcretes.get(i).getPk());
                c = row.createCell(4);
                c.setCellValue(shotcretes.get(i).getLocation() == 0 ? "N/A" : selectionDetailLists.getLocation()[shotcretes.get(i).getLocation()]);
                c = row.createCell(5);
                c.setCellValue(shotcretes.get(i).getTimeFrom());
                c = row.createCell(6);
                c.setCellValue(shotcretes.get(i).getTimeTo());
            }

            //Images
            List<Picture> mappingPicturesData = (List<Picture>) mCloudStoreService.getMappingInputPictures(mapping.getId(), Picture.class);
            List<File> pictureFiles = new ArrayList<>();
            List<File> pictureFilesDestination = new ArrayList<>();

            File mappingDir = generateDirectory(newDirPictures.getPath(), chainageNumberFormat.format(mapping.getChainageStart()) + " - " + chainageNumberFormat.format(mapping.getChainageEnd()));

            for(int i = 0; i < mappingPicturesData.size(); i++) {
                Picture picture = mappingPicturesData.get(i);
                File newFile = new File(picture.getLocalUri() + File.separator + picture.getSideName().trim().toLowerCase() + ".jpg");
                File newFileOriginal = new File(picture.getLocalUri() + File.separator + picture.getSideName().trim().toLowerCase() + "-original.jpg");

                if(picture.getLocalUri() != null) {
                    pictureFiles.add(newFile);
                    pictureFiles.add(newFileOriginal);
                    pictureFilesDestination.add(new File(mappingDir, picture.getSideName().trim().toLowerCase() + ".jpg"));
                    pictureFilesDestination.add(new File(mappingDir, picture.getSideName().trim().toLowerCase() + "-original.jpg"));
                }
            }

            writeFiles(pictureFiles, pictureFilesDestination);

            ExpandedTunnel et = (ExpandedTunnel) mCloudStoreService.getMappingInput(mapping.getId(), ExpandedTunnel.class);
            List<File> etFiles = new ArrayList<>();
            List<File> etFilesDest = new ArrayList<>();
            if(et.getLocalUri() != null) {
                File newFile = new File(et.getLocalUri() + File.separator + et.getSideName().trim().toLowerCase() + ".jpg");
                File newFileDest = new File(mappingDir, et.getSideName().trim().toLowerCase() + ".jpg");

                etFiles.add(newFile);
                etFilesDest.add(newFileDest);
            }

            writeFiles(etFiles, etFilesDest);

            Gsi gsi = (Gsi) mCloudStoreService.getMappingInput(mapping.getId(), Gsi.class);
            List<File> gsiFiles = new ArrayList<>();
            List<File> gsiFilesDest = new ArrayList<>();
            if(gsi.getLocalUri() != null) {
                File newFile = new File(gsi.getLocalUri() + File.separator + gsi.getSideName().trim().toLowerCase() + ".jpg");
                File newFileDest = new File(mappingDir, gsi.getSideName().trim().toLowerCase() + ".jpg");

                gsiFiles.add(newFile);
                gsiFilesDest.add(newFileDest);
            }
            writeFiles(gsiFiles, gsiFilesDest);
        }

        File file = getDestinationExcelFile(newDir);
        FileOutputStream os = null;
        writeFile(os, file, wb);

        return false;
    }

    private void writeFiles(List<File> srcfFiles, List<File> destfFiles) {
        for(int i = 0; i < destfFiles.size(); i++) {
            File srcFile = srcfFiles.get(i);
            File destFile = destfFiles.get(i);

            InputStream is;
            FileManager fm = new FileManager();
            try {
                is = new FileInputStream(srcFile);
                OutputStream out = new FileOutputStream(destFile);
                fm.copyFile(is, out);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean exportForecastingData(Project p, Tunnel t, Face f, String directoryPath) {
        List<String> directories = new ArrayList<>();
        directories.add(p.getName().trim());
        directories.add(t.getName().trim());
        directories.add(f.getName().trim());
        directories.add("Forecast_data");
        File newDir = generateDirectoryTree(directories, directoryPath, "Tunnelarts");

        Workbook wb = new HSSFWorkbook();

        CellStyle cs = wb.createCellStyle();
        cs.setFillForegroundColor(HSSFColor.SKY_BLUE.index);
        cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        Font font = wb.createFont();
        font.setColor(HSSFColor.WHITE.index);
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        cs.setFont(font);

        CellStyle csTitle = wb.createCellStyle();
        Font fontTitle = wb.createFont();
        fontTitle.setBoldweight(Font.BOLDWEIGHT_BOLD);
        csTitle.setFont(fontTitle);

        CellStyle csSelection = wb.createCellStyle();
        csSelection.setFillForegroundColor(HSSFColor.SEA_GREEN.index);
        csSelection.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        Cell c = null;

        List<ProspectionHoleGroup> prospectionHoleGroups =  mCloudStoreService.getProspectionHoleGroupsByFace(f.getId());
        ChainageNumberFormat chainageNumberFormat = new ChainageNumberFormat();

        for(ProspectionHoleGroup prospectionHoleGroup : prospectionHoleGroups) {
            Sheet sheet1 = wb.createSheet(prospectionHoleGroup.getName());
            Row row = sheet1.createRow(0);

            c = row.createCell(0);
            c.setCellValue("Prospection Hole Group");
            c.setCellStyle(cs);

            for(int i = 1; i <= 20; i++) {
                c = row.createCell(i);
                c.setCellStyle(cs);
            }

            sheet1.setColumnWidth(0, (15 * 500));
            sheet1.setColumnWidth(1, (15 * 500));
            sheet1.setColumnWidth(2, (15 * 500));
            sheet1.setColumnWidth(3, (15 * 500));
            sheet1.setColumnWidth(4, (15 * 500));
            sheet1.setColumnWidth(5, (15 * 500));
            sheet1.setColumnWidth(6, (15 * 500));
            sheet1.setColumnWidth(7, (15 * 500));
            sheet1.setColumnWidth(8, (15 * 500));
            sheet1.setColumnWidth(9, (15 * 500));
            sheet1.setColumnWidth(10, (15 * 500));
            sheet1.setColumnWidth(11, (15 * 500));

            Mapping mapping = mCloudStoreService.getMappingById(prospectionHoleGroup.getIdMapping());

            row = sheet1.createRow(2);
            c = row.createCell(0);
            c.setCellValue("Chainages");
            c.setCellStyle(csTitle);
            c = row.createCell(1);
            c.setCellValue(chainageNumberFormat.format(mapping.getChainageStart()) + " - " + chainageNumberFormat.format(mapping.getChainageEnd()));

            row = sheet1.createRow(3);
            c = row.createCell(0);
            c.setCellValue("Q Value (observed)");
            c.setCellStyle(csTitle);
            c = row.createCell(1);
            c.setCellValue(prospectionHoleGroup.getqValueSelected());

            row = sheet1.createRow(5);
            c = row.createCell(0);
            c.setCellValue("Prospection Holes");
            c.setCellStyle(cs);

            for(int i = 1; i <= 20; i++) {
                c = row.createCell(i);
                c.setCellStyle(cs);
            }

            List<ProspectionHole> prospectionHoles = mCloudStoreService.getProspectionHolesByGroup(prospectionHoleGroup.getId());

            int index = 7;

            for(int i = 0; i < prospectionHoles.size(); i++) {
                row = sheet1.createRow(index);
                c = row.createCell(0);
                c.setCellValue(prospectionHoles.get(i).getName().trim());
                c.setCellStyle(csTitle);

                row = sheet1.createRow(++index);
                c = row.createCell(0);
                c.setCellValue("Angle (°):");
                c = row.createCell(1);
                c.setCellValue(prospectionHoles.get(i).getAngle());

                ++index;

                row = sheet1.createRow(++index);
                c = row.createCell(0);
                c.setCellValue("Rods");
                c = row.createCell(1);
                c.setCellValue("Time [min]");
                c = row.createCell(2);
                c.setCellValue("Average Pressure [Bar]");
                c = row.createCell(3);
                c.setCellValue("Drilled Length [m]");
                c = row.createCell(4);
                c.setCellValue("Speed [m/min]");
                c = row.createCell(5);
                c.setCellValue("Water color");
                c = row.createCell(6);
                c.setCellValue("Q Predicted");

                List<Rod> rods = mCloudStoreService.getRodsByProspectionHole(prospectionHoles.get(i).getId());

                for(int j = 0; j < rods.size(); j++) {
                    row = sheet1.createRow(++index);
                    c = row.createCell(0);
                    c.setCellValue(j+1);
                    c = row.createCell(1);
                    c.setCellValue(rods.get(j).getTime());
                    c = row.createCell(2);
                    c.setCellValue(rods.get(j).getAveragePressure());
                    c = row.createCell(3);
                    c.setCellValue(rods.get(j).getDrilledLength());
                    c = row.createCell(4);
                    c.setCellValue(rods.get(j).getSpeed());
                    c = row.createCell(5);
                    c.setCellValue(rods.get(j).getWaterColor());
                    c = row.createCell(6);
                    c.setCellValue(rods.get(j).getqValue());
                }

                index += 2;
            }
        }

        File file = getDestinationExcelFile(newDir);
        FileOutputStream os = null;
        writeFile(os, file, wb);

        return false;
    }

    @Override
    public boolean exportAll() {
        return false;
    }

    private void writeFile(FileOutputStream os, File file, Workbook wb) {
        try {
            os = new FileOutputStream(file);
            wb.write(os);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != os)
                    os.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private File generateDirectory(String directoryPath, String newDirectory) {
        File m_file = new File (directoryPath + File.separator + newDirectory);
        if (!m_file.exists()) {
            try{
                m_file.mkdir();
            }
            catch(SecurityException se){
                se.printStackTrace();
            }
        }
        return m_file;
    }

    private File generateDirectoryTree(List<String> directories, String initialPath, String initialDirectory) {
        File newDir = generateDirectory(initialPath, initialDirectory);
        for(int i = 0; i < directories.size(); i++) {
            newDir = generateDirectory(newDir.getPath(), directories.get(i));
        }
        return newDir;
    }

    private File getDestinationExcelFile(File destinationDir) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String dateFormatted = dateFormat.format(cal.getTime());
        String fileName = dateFormatted + ".xls";

        return new File(destinationDir, fileName);
    }

    class ChainageNumberFormat extends DecimalFormat {

        public ChainageNumberFormat() {
            DecimalFormatSymbols custom = new DecimalFormatSymbols();
            custom.setGroupingSeparator('+');
            custom.setDecimalSeparator(',');
            setMinimumFractionDigits(2);
            setMaximumFractionDigits(2);

            setDecimalFormatSymbols(custom);
        }
    }
}
