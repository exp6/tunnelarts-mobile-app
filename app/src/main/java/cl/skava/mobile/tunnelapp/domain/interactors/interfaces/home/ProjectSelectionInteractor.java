package cl.skava.mobile.tunnelapp.domain.interactors.interfaces.home;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.interactors.base.Interactor;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;

/**
 * Created by Jose Ignacio Vera on 20-06-2016.
 */
public interface ProjectSelectionInteractor extends Interactor {

    interface Callback {
        //Interactor callback methods goes here
        void onTunnelListRetrieved(List<Tunnel> tunnels);
        void onFaceListRetrieved(List<Face> faces);
    }

    //Interactor methods goes here
}
