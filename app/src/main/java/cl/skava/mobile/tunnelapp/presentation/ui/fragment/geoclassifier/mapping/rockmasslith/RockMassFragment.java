package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rockmasslith;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMass;

/**
 * Created by Jose Ignacio Vera on 30-06-2016.
 */
public class RockMassFragment extends Fragment {

    private static final String TAG = "RockMassFragment";

    private RockMass mappingInput;
    private Boolean mappingFinished;
    private TextView briefDesc;
    private View rootView;

    private Spinner spinnerStructure;
    private Spinner spinnerJointing;
    private Spinner spinnerWater;
    private EditText inputWaterT;
    private Spinner spinnerWaterQuality;
    private EditText inputInflow;
    private Spinner spinnerGeneticGroup;
    private Spinner rockName;
    private Spinner weathering;
    private Spinner colorValue;
    private Spinner colorChroma;
    private Spinner colorHue;
    private Spinner strength;

    private List<String[]> rockTypes;
    private boolean instantiateFirst = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initRockTypeData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_rockmassl_0, container, false);
        rootView.setTag(TAG);

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.characterization_mapping_input_rmo_label_title));

        spinnerStructure = (Spinner) rootView.findViewById(R.id.spinner_structure);
        spinnerJointing = (Spinner) rootView.findViewById(R.id.spinner_jointing);
        spinnerWater = (Spinner) rootView.findViewById(R.id.spinner_water);
        inputWaterT = ((EditText) rootView.findViewById(R.id.input_water_t));
        spinnerWaterQuality = (Spinner) rootView.findViewById(R.id.spinner_water_quality);
        inputInflow = ((EditText) rootView.findViewById(R.id.input_inflow));
        spinnerGeneticGroup = (Spinner) rootView.findViewById(R.id.spinner_genetic_group);
        rockName = (Spinner) rootView.findViewById(R.id.spinner_rock_name);
        weathering = (Spinner) rootView.findViewById(R.id.spinner_weathering);
        colorValue = (Spinner) rootView.findViewById(R.id.spinner_colour_value);
        colorChroma = (Spinner) rootView.findViewById(R.id.spinner_colour_chroma);
        colorHue = (Spinner) rootView.findViewById(R.id.spinner_colour_hue);
        strength = (Spinner) rootView.findViewById(R.id.spinner_strength);

        spinnerStructure.setSelection(mappingInput.getStructure());
        spinnerJointing.setSelection(mappingInput.getJointing());
        spinnerWater.setSelection(mappingInput.getWater());
        inputWaterT.setText(mappingInput.getWaterT().toString());
        spinnerWaterQuality.setSelection(mappingInput.getWaterQuality());
        inputInflow.setText(mappingInput.getInflow().toString());
        spinnerGeneticGroup.setSelection(mappingInput.getGeneticGroup() > 1 ? mappingInput.getGeneticGroup()-1 : 0);
        setRockType(mappingInput.getGeneticGroup());
        instantiateFirst = true;
        rockName.setSelection(mappingInput.getRockName());
        weathering.setSelection(mappingInput.getWeathering());
        colorValue.setSelection(mappingInput.getColorValue());
        colorChroma.setSelection(mappingInput.getColorChroma());
        colorHue.setSelection(mappingInput.getColorHue());
        strength.setSelection(mappingInput.getStrength());

        spinnerStructure.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });

        spinnerJointing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });

        spinnerWater.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });

        spinnerWaterQuality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });

        spinnerGeneticGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if(instantiateFirst) instantiateFirst = false;
                else setRockType(position);

                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });

        rockName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });

        weathering.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });

        colorValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });

        colorChroma.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });

        colorHue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });

        strength.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setBriefDescText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });

        if(mappingFinished) {
            spinnerStructure.setEnabled(false);
            spinnerJointing.setEnabled(false);
            spinnerWater.setEnabled(false);
            inputWaterT.setEnabled(false);
            spinnerWaterQuality.setEnabled(false);
            inputInflow.setEnabled(false);
            spinnerGeneticGroup.setEnabled(false);
            rockName.setEnabled(false);
            weathering.setEnabled(false);
            colorValue.setEnabled(false);
            colorChroma.setEnabled(false);
            colorHue.setEnabled(false);
            strength.setEnabled(false);
        }

        return rootView;
    }

    private void setBriefDescText() {
            briefDesc = (TextView) rootView.findViewById(R.id.brief_desc);

            String briefDesc1 = weathering.getSelectedItem().toString();
            String briefDesc2 = spinnerStructure.getSelectedItem().toString();
            String briefDesc3 = colorValue.getSelectedItem().toString();
            String briefDesc4 = colorChroma.getSelectedItem().toString();
            String briefDesc5 = colorHue.getSelectedItem().toString();
            String briefDesc6 = rockName.getSelectedItem().toString();
            String briefDesc7 = strength.getSelectedItem().toString();
            String briefDesc8 = spinnerJointing.getSelectedItem().toString();
            String briefDesc9 = spinnerWater.getSelectedItem().toString();

            StringBuilder sb = new StringBuilder();
            if(weathering.getSelectedItemPosition() != 0) sb.append(briefDesc1);
            if(spinnerStructure.getSelectedItemPosition() != 0) sb.append(", ").append(briefDesc2.toLowerCase());
            if(colorValue.getSelectedItemPosition() != 0) sb.append(", ").append(briefDesc3.toLowerCase());
            if(colorChroma.getSelectedItemPosition() != 0) sb.append(" ").append(briefDesc4.toLowerCase());
            if(colorHue.getSelectedItemPosition() != 0) sb.append("-").append(briefDesc5.toLowerCase());
            if(rockName.getSelectedItemPosition() != 0) sb.append(" ").append(briefDesc6.toLowerCase());
            if(strength.getSelectedItemPosition() != 0) sb.append(", ").append(briefDesc7.toLowerCase());
            if(spinnerJointing.getSelectedItemPosition() != 0) sb.append(", with ").append(briefDesc8.toLowerCase()).append(" jointing");
            if(spinnerWater.getSelectedItemPosition() != 0) sb.append(", ").append(briefDesc9.toLowerCase());
            if (sb.length() > 0) sb.append(".");

            briefDesc.setText(sb.toString());
    }

    private void setRockType(int position) {
        ArrayAdapter<String> a = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, rockTypes.get(position));
        rockName.setAdapter(a);
    }

    private void initRockTypeData() {
        rockTypes = new ArrayList<>(7);
        rockTypes.add(0, getResources().getStringArray(R.array.null_dropdown_arrays));
        rockTypes.add(1, getResources().getStringArray(R.array.igneous_plutonic_dropdown_arrays));
        rockTypes.add(2, getResources().getStringArray(R.array.igneous_volcanic_dropdown_arrays));
        rockTypes.add(3, getResources().getStringArray(R.array.pyroclastic_dropdown_arrays));
        rockTypes.add(4, getResources().getStringArray(R.array.sedimentary_clastic_dropdown_arrays));
        rockTypes.add(5, getResources().getStringArray(R.array.sedimentary_chemical_dropdown_arrays));
        rockTypes.add(6, getResources().getStringArray(R.array.metamorphic_dropdown_arrays));
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMappingInput(RockMass mappingInput) {
        this.mappingInput = mappingInput;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public TextView getBriefDesc() {
        return briefDesc;
    }
}
