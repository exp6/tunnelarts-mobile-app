package cl.skava.mobile.tunnelapp.domain.model.mapping;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class Discontinuity extends DataInput implements Parcelable {

    private String id;
    private Integer type;
    private Integer relevance;
    private Float orientationDd;
    private Float orientationD;
    private Integer spacing;
    private Integer persistence;
    private Integer opening;
    private Integer roughness;
    private Integer infilling;
    private Integer infillingType;
    private Integer weathering;
    private String idMapping;
    private Boolean completed;
    private Integer position;
    private Integer slickensided;

    public Discontinuity(String id, Integer type, Integer relevance, Float orientationDd, Float orientationD, Integer spacing, Integer persistence, Integer opening, Integer roughness, Integer infilling, Integer infillingType, Integer weathering, String idMapping, Boolean completed, Integer position, Integer slickensided) {
        this.id = id;
        this.type = type;
        this.relevance = relevance;
        this.orientationDd = orientationDd;
        this.orientationD = orientationD;
        this.spacing = spacing;
        this.persistence = persistence;
        this.opening = opening;
        this.roughness = roughness;
        this.infilling = infilling;
        this.infillingType = infillingType;
        this.weathering = weathering;
        this.idMapping = idMapping;
        this.completed = completed;
        this.position = position;
        this.slickensided = slickensided;
    }

    public Discontinuity(Parcel in) {
        this.id = in.readString();
        this.type = in.readInt();
        this.relevance = in.readInt();
        this.orientationDd = in.readFloat();
        this.orientationD = in.readFloat();
        this.spacing = in.readInt();
        this.persistence = in.readInt();
        this.opening = in.readInt();
        this.roughness = in.readInt();
        this.infilling = in.readInt();
        this.infillingType = in.readInt();
        this.weathering = in.readInt();
        this.idMapping = in.readString();
        this.completed = in.readInt() != 0;
        this.position = in.readInt();
        this.slickensided = in.readInt();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRelevance() {
        return relevance;
    }

    public void setRelevance(Integer relevance) {
        this.relevance = relevance;
    }

    public Float getOrientationDd() {
        return orientationDd;
    }

    public void setOrientationDd(Float orientationDd) {
        this.orientationDd = orientationDd;
    }

    public Float getOrientationD() {
        return orientationD;
    }

    public void setOrientationD(Float orientationD) {
        this.orientationD = orientationD;
    }

    public Integer getSpacing() {
        return spacing;
    }

    public void setSpacing(Integer spacing) {
        this.spacing = spacing;
    }

    public Integer getPersistence() {
        return persistence;
    }

    public void setPersistence(Integer persistence) {
        this.persistence = persistence;
    }

    public Integer getOpening() {
        return opening;
    }

    public void setOpening(Integer opening) {
        this.opening = opening;
    }

    public Integer getRoughness() {
        return roughness;
    }

    public void setRoughness(Integer roughness) {
        this.roughness = roughness;
    }

    public Integer getInfilling() {
        return infilling;
    }

    public void setInfilling(Integer infilling) {
        this.infilling = infilling;
    }

    public Integer getInfillingType() {
        return infillingType;
    }

    public void setInfillingType(Integer infillingType) {
        this.infillingType = infillingType;
    }

    public Integer getWeathering() {
        return weathering;
    }

    public void setWeathering(Integer weathering) {
        this.weathering = weathering;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getSlickensided() {
        return slickensided;
    }

    public void setSlickensided(Integer slickensided) {
        this.slickensided = slickensided;
    }

    @Override
    public String toString() {
        return "Discontinuity{" +
                "id='" + id + '\'' +
                ", type=" + type +
                ", relevance=" + relevance +
                ", orientationDd=" + orientationDd +
                ", orientationD=" + orientationD +
                ", spacing=" + spacing +
                ", persistence=" + persistence +
                ", opening=" + opening +
                ", roughness=" + roughness +
                ", infilling=" + infilling +
                ", infillingType=" + infillingType +
                ", weathering=" + weathering +
                ", idMapping='" + idMapping + '\'' +
                ", completed=" + completed +
                ", position=" + position +
                ", slickensided=" + slickensided +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(type);
        dest.writeInt(relevance);
        dest.writeFloat(orientationDd);
        dest.writeFloat(orientationD);
        dest.writeInt(spacing);
        dest.writeInt(persistence);
        dest.writeInt(opening);
        dest.writeInt(roughness);
        dest.writeInt(infilling);
        dest.writeInt(infillingType);
        dest.writeInt(weathering);
        dest.writeString(idMapping);
        dest.writeInt(completed? 1 : 0);
        dest.writeInt(position);
        dest.writeInt(slickensided);
    }

    public static final Parcelable.Creator<Discontinuity> CREATOR
            = new Parcelable.Creator<Discontinuity>() {
        @Override
        public Discontinuity createFromParcel(Parcel source) {
            return new Discontinuity(source);
        }

        @Override
        public Discontinuity[] newArray(int size) {
            return new Discontinuity[size];
        }
    };
}
