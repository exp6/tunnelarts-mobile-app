package cl.skava.mobile.tunnelapp.data.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.repository.ModuleRepository;

/**
 * Created by Jose Ignacio Vera on 24-06-2016.
 */
public class ModuleRepositoryImpl implements ModuleRepository {

    private final List<Module> modules = new ArrayList<>();

    public ModuleRepositoryImpl() {
        initDataset();
    }

    @Override
    public List<Module> getUserModules() {
        return modules;
    }

    private void initDataset() {
        String lang = Locale.getDefault().getLanguage();

        Module module = new Module();
        module.setName(lang.equals("es") ? "Caracterización" : "Characterization");
        module.setCode(1);
        module.setEnabled(true);
        module.setCreatedAt(new Date());
        modules.add(module);

        module = new Module();
        module.setName(lang.equals("es") ? "Pronóstico" : "Forecast");
        module.setCode(2);
        module.setEnabled(true);
        module.setCreatedAt(new Date());
        modules.add(module);

//        module = new Module();
//        module.setName(lang.equals("es") ? "Visualización" : "Visualization");
//        module.setCode(3);
//        module.setEnabled(false);
//        module.setCreatedAt(new Date());
//        modules.add(module);
    }
}
