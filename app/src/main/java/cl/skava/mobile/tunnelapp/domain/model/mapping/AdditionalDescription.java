package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class AdditionalDescription extends DataInput {

    private String id;
    private Integer distanceToFace;
    private Integer responsibleForRestrictedArea;
    private Integer reasonForRestrictedArea;
    private Integer lightQuality;
    private Integer airQuality;
    private String availableTime;
    private Integer newDamageBehind;
    private String description;
    private String idMapping;
    private Boolean completed;

    public AdditionalDescription(String id, Integer distanceToFace, Integer responsibleForRestrictedArea, Integer reasonForRestrictedArea, Integer lightQuality, Integer airQuality, String availableTime, Integer newDamageBehind, String description, String idMapping, Boolean completed) {
        this.id = id;
        this.distanceToFace = distanceToFace;
        this.responsibleForRestrictedArea = responsibleForRestrictedArea;
        this.reasonForRestrictedArea = reasonForRestrictedArea;
        this.lightQuality = lightQuality;
        this.airQuality = airQuality;
        this.availableTime = availableTime;
        this.newDamageBehind = newDamageBehind;
        this.description = description;
        this.idMapping = idMapping;
        this.completed = completed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getDistanceToFace() {
        return distanceToFace;
    }

    public void setDistanceToFace(Integer distanceToFace) {
        this.distanceToFace = distanceToFace;
    }

    public Integer getResponsibleForRestrictedArea() {
        return responsibleForRestrictedArea;
    }

    public void setResponsibleForRestrictedArea(Integer responsibleForRestrictedArea) {
        this.responsibleForRestrictedArea = responsibleForRestrictedArea;
    }

    public Integer getReasonForRestrictedArea() {
        return reasonForRestrictedArea;
    }

    public void setReasonForRestrictedArea(Integer reasonForRestrictedArea) {
        this.reasonForRestrictedArea = reasonForRestrictedArea;
    }

    public Integer getLightQuality() {
        return lightQuality;
    }

    public void setLightQuality(Integer lightQuality) {
        this.lightQuality = lightQuality;
    }

    public Integer getAirQuality() {
        return airQuality;
    }

    public void setAirQuality(Integer airQuality) {
        this.airQuality = airQuality;
    }

    public String getAvailableTime() {
        return availableTime;
    }

    public void setAvailableTime(String availableTime) {
        this.availableTime = availableTime;
    }

    public Integer getNewDamageBehind() {
        return newDamageBehind;
    }

    public void setNewDamageBehind(Integer newDamageBehind) {
        this.newDamageBehind = newDamageBehind;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "AdditionalDescription{" +
                "id='" + id + '\'' +
                ", distanceToFace=" + distanceToFace +
                ", responsibleForRestrictedArea=" + responsibleForRestrictedArea +
                ", reasonForRestrictedArea=" + reasonForRestrictedArea +
                ", lightQuality=" + lightQuality +
                ", airQuality=" + airQuality +
                ", availableTime='" + availableTime + '\'' +
                ", newDamageBehind=" + newDamageBehind +
                ", description='" + description + '\'' +
                ", idMapping='" + idMapping + '\'' +
                ", completed=" + completed +
                '}';
    }
}
