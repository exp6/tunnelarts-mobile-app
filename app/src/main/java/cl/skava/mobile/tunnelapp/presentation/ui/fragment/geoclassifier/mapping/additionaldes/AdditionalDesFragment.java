package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.additionaldes;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TimePicker;

import java.util.Calendar;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.AdditionalDescription;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class AdditionalDesFragment extends Fragment {

    private static final String TAG = "AdditionalDesFragment";

    private static EditText time;
    private AdditionalDescription mappingInput;

    private OnAdditionalDescriptionValueListener mCallback;

    public interface OnAdditionalDescriptionValueListener {
        void OnAdditionalDescriptionSetValue(AdditionalDescription input);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnAdditionalDescriptionValueListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnAdditionalDescriptionValueListener");
        }
    }

    private Boolean mappingFinished;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_additionaldes, container, false);
        rootView.setTag(TAG);

        time = (EditText) rootView.findViewById(R.id.input_time);

//        ImageButton b1 = (ImageButton) rootView.findViewById(R.id.mapping_additionaldes_time_btn);
//        b1.setOnClickListener(
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        showTimePickerDialog(v);
//                    }
//                });


        Spinner spinnerDistance = (Spinner) rootView.findViewById(R.id.spinner_distance_to_face);
        Spinner spinnerResponsible = (Spinner) rootView.findViewById(R.id.spinner_responsible);
        Spinner spinnerReason = (Spinner) rootView.findViewById(R.id.spinner_reason);
        Spinner spinnerLightQuality = ((Spinner) rootView.findViewById(R.id.spinner_light_quality));
        Spinner spinnerAirQuality = (Spinner) rootView.findViewById(R.id.spinner_air_quality);
        EditText inputTime = ((EditText) rootView.findViewById(R.id.input_time));
        Spinner spinnerDamage = (Spinner) rootView.findViewById(R.id.spinner_damage);
        EditText inputDescription = ((EditText) rootView.findViewById(R.id.input_description));

        spinnerDistance.setSelection(mappingInput.getDistanceToFace());
        spinnerResponsible.setSelection(mappingInput.getResponsibleForRestrictedArea());
        spinnerReason.setSelection(mappingInput.getReasonForRestrictedArea());
        spinnerLightQuality.setSelection(mappingInput.getLightQuality());
        spinnerAirQuality.setSelection(mappingInput.getAirQuality());
        inputTime.setText(mappingInput.getAvailableTime().toString());
        spinnerDamage.setSelection(mappingInput.getNewDamageBehind());
        inputDescription.setText(mappingInput.getDescription().toString());

        if(mappingFinished) {
            spinnerDistance.setEnabled(false);
            spinnerResponsible.setEnabled(false);
            spinnerReason.setEnabled(false);
            spinnerLightQuality.setEnabled(false);
            spinnerAirQuality.setEnabled(false);
            inputTime.setEnabled(false);
            spinnerDamage.setEnabled(false);
            inputDescription.setEnabled(false);
        }

        return rootView;
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getChildFragmentManager(), "timePicker");
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMappingInput(AdditionalDescription mappingInput) {
        this.mappingInput = mappingInput;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            time.setText(hourOfDay + ":" + (minute < 10 ? "0" + minute : minute));
        }
    }
}
