package cl.skava.mobile.tunnelapp.data.machinelearning.impl.java;

import org.dmg.pmml.FieldName;
import org.jpmml.evaluator.Evaluator;
import org.jpmml.evaluator.FieldValue;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cl.skava.mobile.tunnelapp.data.cache.FileManager;
import cl.skava.mobile.tunnelapp.data.machinelearning.base.MachineLearningService;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;

/**
 * Created by Jose Ignacio Vera on 04-11-2016.
 */
public class MLEngine implements MachineLearningService {

    private File mModelFile;
    private File[] mEvaluators;
    private FileManager mFileManager;

    private List<Rod> mRods;
    private Float mInitialQ;
    private List<Rod> mRodsAux;

    public MLEngine(File[] evaluators, List<Rod> rods, Float initialQ) {
        mEvaluators = evaluators;
        mFileManager = new FileManager();

        mRods = rods;
        mRodsAux = new ArrayList<>();
        mInitialQ = initialQ;

//        initInputData();
        initDataset();
    }

//    private void initInputData() {
//        if (!mDirectory.exists()) {
//            try{
//                mDirectory.mkdir();
//            }
//            catch(SecurityException se){
//                //handle it
//            }
//        }
//    }

    private void initDataset() {
        Float cumulativeDepth = new Float(0.0);
        Rod newRod;
        for(Rod r : mRods) {
            newRod = new Rod();
            cumulativeDepth += r.getDrilledLength();
            newRod.setDrilledLength(cumulativeDepth);
            mRodsAux.add(newRod);
        }
    }

    @Override
    public void executeModel() {
        try {
//            File[] models = mDirectory.listFiles();

            Double previousQ = round(new Double(mInitialQ), 4);

            File pmmlFile;
            InputStream is;
            Evaluator evaluator;
            List<FieldName> activeFields;
            List<FieldName> outputFields;
            Map<FieldName, FieldValue> arguments;
            Map<FieldName, ?> results;
            FieldName outputFieldName;
            Object outputFieldValue;
            List<Double> predictedValues = new ArrayList<>();

            double speed;
            double pressure;
            double depth;

            double sum = 0.0;
            double mean;
            double qinitialValue;

            List<Evaluator> evaluators = new ArrayList<>();

            for(int j = 0; j < mEvaluators.length; j++) {
                pmmlFile = mEvaluators[j];
//                pmmlFile = new File(mDirectory, "PMML_Modelo_Skava_" + j + ".pmml.ser");
                is = new FileInputStream(pmmlFile);
                evaluator = org.jpmml.android.EvaluatorUtil.createEvaluator(is);
                evaluators.add(evaluator);
            }

            Rod r;
            for(int i=0;i<mRods.size();i++) {
                r = mRods.get(i);
                speed = round(new Double(r.getSpeed()), 4);
                pressure = round(new Double(r.getAveragePressure()), 4);
                depth = round(new Double(mRodsAux.get(i).getDrilledLength()), 4);

                qinitialValue = (i == 0 ? previousQ : mRods.get(i-1).getqValue());

                for(Evaluator eval : evaluators) {
                    arguments = new LinkedHashMap<>();

                    activeFields = eval.getActiveFields();
                    outputFields = org.jpmml.evaluator.EvaluatorUtil.getOutputFields(eval);

                    FieldValue value1 = eval.prepare(activeFields.get(0), speed);
                    arguments.put(activeFields.get(0), value1);

                    FieldValue value2 = eval.prepare(activeFields.get(1), pressure);
                    arguments.put(activeFields.get(1), value2);

                    FieldValue value3 = eval.prepare(activeFields.get(2), qinitialValue);
                    arguments.put(activeFields.get(2), value3);

                    FieldValue value4 = eval.prepare(activeFields.get(3), depth);
                    arguments.put(activeFields.get(3), value4);

                    results = eval.evaluate(arguments);
                    outputFieldName = outputFields.get(1);
                    outputFieldValue = results.get(outputFieldName);
                    predictedValues.add((Double) outputFieldValue);
                }

                for(Double value : predictedValues) {
                    sum += value.doubleValue();
                }

                mean = sum/predictedValues.size();
                mRods.get(i).setqValue((float) Math.exp(mean));
                predictedValues = new ArrayList<>();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Rod> getFinalRods() {
        return this.mRods;
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_DOWN);
        return bd.doubleValue();
    }
}
