package cl.skava.mobile.tunnelapp.presentation.ui.fragment.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.executor.impl.ThreadExecutor;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.presentation.presenters.impl.home.HomePresenterImpl;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.home.HomePresenter;
import cl.skava.mobile.tunnelapp.threading.MainThreadImpl;

/**
 * Created by Jose Ignacio Vera on 02-06-2016.
 */
public class HomeFragment extends Fragment implements
        HomePresenter.View,
        ProjectListFragment.OnProjectItemSelectedListener,
        TunnelListFragment.OnTunnelItemSelectedListener,
        FaceListFragment.OnFaceModuleItemListener {

    private static final String TAG = "HomeFragment";
    protected List<Project> mProjectDataset;
    protected List<Tunnel> mTunnelDataset;
    protected List<Face> mFacesDataset;
    protected List<Module> mModulesDataset;
    private HomePresenter mPresenter;

    //User Selection Context
    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Module selectedModule;

    private OnHomeItemListener mCallback;

    private HashMap<Project, List<FormationUnit>> mFormationUnitsByProject;
    HashMap<Project, List<RockQuality>> mRockQualities;

    public interface OnHomeItemListener {
        void userModulesLoaded();
        void initModuleActivity(Project project, Tunnel tunnel, Face face, Module module, Client client, List<FormationUnit> formationUnits, List<RockQuality> rockQualities);
        void changeTitle(Client client);
    }

    private User user;
    private Client mClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallback = (OnHomeItemListener) getActivity();

        if (savedInstanceState == null) {
            initDatasources();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        rootView.setTag(TAG);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.resume();
    }

    private void initDatasources() {
        mPresenter = new HomePresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                getActivity(),
                user
        );
        mProjectDataset = new ArrayList<>();
        mTunnelDataset = new ArrayList<>();
        mFacesDataset = new ArrayList<>();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onProjectSelected(int position) {
        selectedProject = mProjectDataset.get(position);
        mPresenter.getTunnelsByProject(selectedProject);
    }

    @Override
    public void onTunnelSelected(int position) {
        selectedTunnel = mTunnelDataset.get(position);
        mPresenter.getFacesByTunnel(selectedTunnel);
    }

    @Override
    public void displayProjectList(List<Project> projects) {
        mProjectDataset = projects;

        if(mProjectDataset != null){
            if(!mProjectDataset.isEmpty()){
                selectedProject = mProjectDataset.get(0);
            }
        }

        ProjectListFragment fragment = (ProjectListFragment) getChildFragmentManager().findFragmentById(R.id.project_list);

        if (fragment == null) {
            fragment = new ProjectListFragment();
            fragment.setMProjectDataset(mProjectDataset);
            fragment.onAttachFragment(this);
            updateFragment(fragment, R.id.project_list);
        }
        else {
            fragment.setMProjectDataset(mProjectDataset);
            fragment.updateItemList();
        }
    }

    @Override
    public void displayTunnelList(List<Tunnel> tunnels) {
        mTunnelDataset = tunnels;

        if(mTunnelDataset != null){
            if(!mTunnelDataset.isEmpty()){
                selectedTunnel = mTunnelDataset.get(0);
            }
        }

        TunnelListFragment fragment = (TunnelListFragment) getChildFragmentManager().findFragmentById(R.id.tunnel_list);

        if (fragment == null) {
            fragment = new TunnelListFragment();
            fragment.setMTunnelDataset(mTunnelDataset);
            fragment.onAttachFragment(this);
            updateFragment(fragment, R.id.tunnel_list);
        }
        else {
            fragment.setMTunnelDataset(mTunnelDataset);
            fragment.updateItemList();
        }
    }

    @Override
    public void displayFaceList(List<Face> faces) {
        mFacesDataset = faces;

        if(mFacesDataset != null){
            if(!mFacesDataset.isEmpty()){
                selectedFace = mFacesDataset.get(0);
            }
        }

        FaceListFragment fragment = (FaceListFragment) getChildFragmentManager().findFragmentById(R.id.face_list);

        if (fragment == null) {
            fragment = new FaceListFragment();
            fragment.setMFacesDataset(mFacesDataset);
            fragment.setMModulesDataset(mModulesDataset);
            fragment.onAttachFragment(this);
            updateFragment(fragment, R.id.face_list);
        }
        else {
            fragment.setMFacesDataset(mFacesDataset);
            fragment.updateItemList();
        }
    }

    @Override
    public void setClientData(Client client) {
        mClient = client;
        mCallback.changeTitle(client);
    }

    @Override
    public void setFormationUnits(HashMap<Project, List<FormationUnit>> formationUnitsByProject) {
        mFormationUnitsByProject = formationUnitsByProject;
    }

    @Override
    public void setRockQualities(HashMap<Project, List<RockQuality>> rockQualities) {
        mRockQualities = rockQualities;
    }

    private void updateFragment(Fragment fragment, int container) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(container, fragment);
        transaction.commitAllowingStateLoss();
    }

    public void setMModulesDataset(List<Module> mModulesDataset) {
        this.mModulesDataset = mModulesDataset;
    }

    @Override
    public void showProgress() {}

    @Override
    public void hideProgress() {}

    @Override
    public void showError(String message) {}


    @Override
    public void onFaceSelected(Face face) {
        selectedFace = face;
    }

    @Override
    public void onModuleSelected(Module module) {
        selectedModule = module;
        List<FormationUnit> formationUnits = getFormationUnits(selectedProject);
        List<RockQuality> rockQualities = getRockQualities(selectedProject);
        mCallback.initModuleActivity(selectedProject, selectedTunnel, selectedFace, selectedModule, mClient, formationUnits, rockQualities);
    }

    private List<FormationUnit> getFormationUnits(Project p) {
        List<FormationUnit> result = new ArrayList<>();
        Iterator<Project> iter = mFormationUnitsByProject.keySet().iterator();

        while(iter.hasNext()) {
            Project p1 = iter.next();
            if(p1.getId().trim().equalsIgnoreCase(p.getId().trim())) {
                result = mFormationUnitsByProject.get(p1);
                break;
            }
        }

        return result;
    }

    private List<RockQuality> getRockQualities(Project p) {
        List<RockQuality> result = new ArrayList<>();
        Iterator<Project> iter = mRockQualities.keySet().iterator();

        while(iter.hasNext()) {
            Project p1 = iter.next();
            if(p1.getId().trim().equalsIgnoreCase(p.getId().trim())) {
                result = mRockQualities.get(p1);
                break;
            }
        }

        return result;
    }

    @Override
    public void userModulesLoaded() {
        mCallback.userModulesLoaded();
    }

    public void setUser(User user) {
        this.user = user;
    }
}
