package cl.skava.mobile.tunnelapp.domain.repository;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.Project;

/**
 * Created by Jose Ignacio Vera on 05-01-2017.
 */
public interface FormationUnitRepository {

    HashMap<Project, List<FormationUnit>> getFormationUnitsByProject();
}
