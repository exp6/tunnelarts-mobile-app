package cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.geoclassifier;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInstance;
import cl.skava.mobile.tunnelapp.presentation.presenters.base.BasePresenter;
import cl.skava.mobile.tunnelapp.presentation.ui.base.BaseView;

/**
 * Created by Jose Ignacio Vera on 26-07-2016.
 */
public interface MappingInputPresenter extends BasePresenter {

    interface View extends BaseView {
        //View methods goes here
        void displayMappingInstance(MappingInstance mappingInstance);
        void onSaveStatus(boolean success);
    }

    //Presenter methods goes here
    void getMappingInputs(Mapping mapping, List<MappingInputModule> inputs);
    void saveMappingInputs(MappingInstance mappingInstance, List<MappingInputModule> inputs);
}
