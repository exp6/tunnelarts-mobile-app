package cl.skava.mobile.tunnelapp.data.repository;

import android.content.Context;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import cl.skava.mobile.tunnelapp.data.cache.FileManager;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.blobs.BlobStorageService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.blobs.syncprocess.ModuleStorageService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.prospection.ProspectionHoleGroupListInteractor;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroupCalculation;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.domain.repository.BaseRepository;
import cl.skava.mobile.tunnelapp.domain.repository.ProspectionHoleGroupRepository;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.MainPresenter;

/**
 * Created by Jose Ignacio Vera on 16-08-2016.
 */
public class ProspectionHoleGroupRepositoryImpl implements ProspectionHoleGroupRepository, BaseRepository {

    private List<ProspectionHoleGroup> prospectionHoleGroups;
    private CloudStoreService cloudStoreService;
    private Context mActivity;
    private String mFaceId;
    private ProspectionHoleGroupListInteractor mInteractor;
    private BlobStorageService blobStorageService;
    private ModuleStorageService moduleStorageService;

    public ProspectionHoleGroupRepositoryImpl(Context activity, String faceId) {
        cloudStoreService = new CloudStoreService(activity);
        mActivity = activity;
        prospectionHoleGroups = new ArrayList<>();
        mFaceId = faceId;
        blobStorageService = new BlobStorageService(activity.getExternalFilesDir(null), this);
        moduleStorageService = new ModuleStorageService(blobStorageService, cloudStoreService, this);
    }

    @Override
    public boolean insert(ProspectionHoleGroup prospectionHoleGroup) {
        prospectionHoleGroup.setId(UUID.randomUUID().toString().toUpperCase());
        return cloudStoreService.persistEntity(ProspectionHoleGroup.class, prospectionHoleGroup);
    }

    @Override
    public boolean update(ProspectionHoleGroup prospectionHoleGroup) {
        return cloudStoreService.updateEntity(ProspectionHoleGroup.class, prospectionHoleGroup);
    }

    @Override
    public boolean delete(ProspectionHoleGroup prospectionHoleGroup) {
        List<Rod> rods;
        List<ProspectionHole> prospectionHoles = cloudStoreService.getProspectionHolesByPHG(prospectionHoleGroup.getId());

        for(ProspectionHole p : prospectionHoles) {
            rods = cloudStoreService.getRodsByProspectionHole(p.getId());

            for(Rod r : rods) {
                cloudStoreService.deleteEntity(Rod.class, r);
            }
            cloudStoreService.deleteEntity(ProspectionHole.class, p);
        }

        List<ProspectionRefPicture> prfList = cloudStoreService.getProspectionRefPictureByPHG(prospectionHoleGroup.getId());
        FileManager fm = new FileManager();
        for(ProspectionRefPicture prp : prfList) {

            File dir = new File(prp.getLocalUri());
            fm.clearDirectory(dir);
            dir.delete();

            cloudStoreService.deleteEntity(ProspectionRefPicture.class, prp);
        }

        List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations = cloudStoreService.getProspectionCalculationByGroups(prospectionHoleGroup.getId());
        for(ProspectionHoleGroupCalculation prp : prospectionHoleGroupCalculations) {
            cloudStoreService.deleteEntity(ProspectionHoleGroupCalculation.class, prp);
        }

        return cloudStoreService.deleteEntity(ProspectionHoleGroup.class, prospectionHoleGroup);
    }

    @Override
    public List<ProspectionHoleGroup> getList() {
        prospectionHoleGroups = cloudStoreService.getProspectionHoleGroupsByFace(mFaceId);
        return prospectionHoleGroups;
    }

    @Override
    public HashMap<Mapping, QValue> getQValuesByMappings() {
        HashMap<Mapping, QValue> qValuesMap = new HashMap<>();
        List<Mapping> mappings = cloudStoreService.getMappingsByFace(mFaceId);

        QValue qValue;
        for(Mapping m : mappings) {
            qValue = (QValue) cloudStoreService.getMappingInput(m.getId(), QValue.class);
            qValuesMap.put(m, qValue);
        }

        return qValuesMap;
    }

    @Override
    public HashMap<ProspectionHole, List<Rod>> getRodsByProspectionHole(String prospectionHoleGroupId) {
        HashMap<ProspectionHole, List<Rod>> rodsByProspectionHole = new HashMap<>();
        List<ProspectionHole> prospectionHoles = cloudStoreService.getProspectionHolesByGroup(prospectionHoleGroupId);
        List<Rod> rods;
        for(ProspectionHole ph : prospectionHoles) {
            rods = cloudStoreService.getRodsByProspectionHole(ph.getId());
            rodsByProspectionHole.put(ph, rods);
        }
        return rodsByProspectionHole;
    }

    @Override
    public void setInteractor(ProspectionHoleGroupListInteractor interactor) {
        mInteractor = interactor;
    }

    @Override
    public void initRepository(Context activity, MainPresenter callback, User user) {

    }

    @Override
    public void setSyncProgress(Integer percentage) {
        mInteractor.setSyncProgressPercentaje(percentage);
    }

    @Override
    public void notifySyncSegment(String msg) {
        mInteractor.notifySyncSegment(msg);
    }

    @Override
    public void onSyncResult(int code) {

        switch(code) {
            case 1:
                mInteractor.onSyncSuccess();
                break;
            case 2:
                moduleStorageService.syncForecastingModels();
                break;
        }
    }

    @Override
    public void onNetworkError(String msg) {
        mInteractor.onSyncError(msg);
    }

    @Override
    public void saveData() {
        cloudStoreService.setmCallback(this);
        cloudStoreService.pushAllData();
    }

    @Override
    public void syncData(User user) {
        cloudStoreService = new CloudStoreService(mActivity, this, user);
        cloudStoreService.syncForecastingData();
    }

    @Override
    public CloudStoreService getCloudStoreService() {
        return cloudStoreService;
    }

    @Override
    public List<ProspectionRefPicture> getRefPictures() {
        List<ProspectionHoleGroup> phgList = getList();
        List<ProspectionRefPicture> prfList = new ArrayList<>();

        List<ProspectionRefPicture> prfListAux;
        ProspectionRefPicture prp;
        for(ProspectionHoleGroup phg : phgList) {
            prfListAux = cloudStoreService.getProspectionRefPictureByPHG(phg.getId());

            if(prfListAux.isEmpty()) {
                prp = new ProspectionRefPicture();
                prp.setId("");
                prp.setIdProspectionHoleGroup(phg.getId());
                prp.setCode(0);
                prp.setSideName("prospection_ref_image");
                prfListAux.add(prp);
            }

            prfList.addAll(prfListAux);
        }

        return prfList;
    }

    @Override
    public boolean insertRefPicture(ProspectionRefPicture prospectionRefPicture) {
        prospectionRefPicture.setId(UUID.randomUUID().toString().toUpperCase());
        return cloudStoreService.persistEntity(ProspectionRefPicture.class, prospectionRefPicture);
    }

    @Override
    public HashMap<ProspectionHoleGroup, List<ProspectionHoleGroupCalculation>> getCalculationsByGroup() {
        List<ProspectionHoleGroup> phgList = getList();
        HashMap<ProspectionHoleGroup, List<ProspectionHoleGroupCalculation>> calculationsByPhg = new HashMap<>();

        List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations;
        for(ProspectionHoleGroup phg : phgList) {
            prospectionHoleGroupCalculations = cloudStoreService.getProspectionCalculationByGroups(phg.getId());
            calculationsByPhg.put(phg, prospectionHoleGroupCalculations);
        }

        return calculationsByPhg;
    }

    @Override
    public boolean insertProspectionHoleGroupCalculations(List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations, String prospectionHoleGroupId) {
        boolean dataListSaved = false;

        List<ProspectionHoleGroupCalculation> existingItems = cloudStoreService.getProspectionCalculationByGroups(prospectionHoleGroupId);

        for(ProspectionHoleGroupCalculation prospectionHoleGroupCalculation : prospectionHoleGroupCalculations) {
            if(existingItems.size() == 0) {
                prospectionHoleGroupCalculation.setId(UUID.randomUUID().toString().toUpperCase());
                dataListSaved = cloudStoreService.persistEntity(ProspectionHoleGroupCalculation.class, prospectionHoleGroupCalculation);
            }
            else {
                if(prospectionHoleGroupCalculation.getId() != null) {
                    if(!prospectionHoleGroupCalculation.getId().isEmpty()) {
                        dataListSaved = cloudStoreService.updateEntity(ProspectionHoleGroupCalculation.class, prospectionHoleGroupCalculation);
                    }
                    else {
                        prospectionHoleGroupCalculation.setId(UUID.randomUUID().toString().toUpperCase());
                        dataListSaved = cloudStoreService.persistEntity(ProspectionHoleGroupCalculation.class, prospectionHoleGroupCalculation);
                    }
                }
            }

            if(prospectionHoleGroupCalculations.size() < existingItems.size()) {
                for(int i = prospectionHoleGroupCalculations.size(); i < existingItems.size(); i++) {
                    cloudStoreService.deleteEntity(ProspectionHoleGroupCalculation.class, existingItems.get(i));
                }
            }
        }
        return dataListSaved;
    }

    @Override
    public void syncStorageData(Boolean syncAll, int moduleCode) {
        blobStorageService = new BlobStorageService(mActivity.getExternalFilesDir(null), this);
        List<ProspectionHoleGroup> phgList = getList();

        moduleStorageService.syncProspectionHoleGroupFiles(syncAll, phgList);

//        HashMap<ProspectionHoleGroup, File[]> filesPerProspectionHoleGroup = new HashMap<>();
//        HashMap<ProspectionHoleGroup, List<ProspectionRefPicture>> picturesByProspectionHoleGroup = new HashMap<>();
//        HashMap<ProspectionHoleGroup, List<ProspectionRefPicture>> picturesOriginalByProspectionHoleGroup = new HashMap<>();
//        List<ProspectionRefPicture> prospectionRefPicturesData;
//
//        for(ProspectionHoleGroup prospectionHoleGroup : phgList) {
//            prospectionRefPicturesData = cloudStoreService.getProspectionRefPictureByPHG(prospectionHoleGroup.getId());
//
//            if(prospectionRefPicturesData.isEmpty()) continue;
//            else {
//                File[] localFiles = blobStorageService.getLocalProspectionRefPictureFiles(prospectionHoleGroup.getId(), prospectionRefPicturesData);
//                filesPerProspectionHoleGroup.put(prospectionHoleGroup, localFiles);
//                picturesByProspectionHoleGroup.put(prospectionHoleGroup, prospectionRefPicturesData);
//                picturesOriginalByProspectionHoleGroup.put(prospectionHoleGroup, prospectionRefPicturesData);
//            }
//        }
//
//        blobStorageService.setmFilesPerProspectionHoleGroup(filesPerProspectionHoleGroup);
//        blobStorageService.setPicturesByProspectionHoleGroup(picturesByProspectionHoleGroup);
//        blobStorageService.setPicturesOriginalByProspectionHoleGroup(picturesOriginalByProspectionHoleGroup);
//        blobStorageService.setmSyncAll(syncAll);
//        blobStorageService.setMediaItemCode(0);
//        blobStorageService.setModuleCode(1);
//        blobStorageService.executeSyncProcess();
    }

//    private void syncForecastingModels() {
//        //Sync forecast models
//        List<String> ids = new ArrayList<>();
//        List<Project> projects = cloudStoreService.getProjects();
//        List<Tunnel> tunnels;
//        for(Project p : projects) {
//            tunnels = cloudStoreService.getTunnelsByProject(p.getId());
//            for(Tunnel t : tunnels) {
//                ids.add(t.getId());
//            }
//        }
//
//        blobStorageService.setTunnelIds(ids);
//        blobStorageService.setModuleCode(2);
//        blobStorageService.executeSyncProcess();
//    }

    @Override
    public void onSyncStorageResult(int code, Boolean syncAll) {
        switch(code) {
            case 8:
//                HashMap<ProspectionHoleGroup, List<ProspectionRefPicture>> picturesByProspectionHoleGroup = blobStorageService.getPicturesByProspectionHoleGroup();
//                Iterator<ProspectionHoleGroup> iter = picturesByProspectionHoleGroup.keySet().iterator();
//                while (iter.hasNext()) {
//                    ProspectionHoleGroup m = iter.next();
//                    List<ProspectionRefPicture> pictures = picturesByProspectionHoleGroup.get(m);
//                    for (ProspectionRefPicture picture : pictures) cloudStoreService.updateEntity(ProspectionRefPicture.class, picture);
//                }
//                cloudStoreService.syncProspectionPictures(ProspectionRefPicture.class);

                moduleStorageService.syncProspectionHoleGroupTableData();
                break;
            case 7:
                moduleStorageService.syncForecastingModels();
                break;
            default:
                mInteractor.onSyncSuccess();
                break;
        }
    }

    @Override
    public void syncBasicData(Context activity, MainPresenter presenter, User user) {

    }
}
