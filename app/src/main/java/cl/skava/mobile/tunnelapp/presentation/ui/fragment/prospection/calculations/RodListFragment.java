package cl.skava.mobile.tunnelapp.presentation.ui.fragment.prospection.calculations;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.prospection.RodListAdapter;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class RodListFragment extends Fragment {

    private static final String TAG = "RodListFragment";

    protected RecyclerView mRecyclerView;
    protected RodListAdapter mAdapter;

    protected RecyclerView.LayoutManager mLayoutManager;

    protected List<Rod> mRodDataset;

    private OnRodItemSelectedListener mCallback;

    public interface OnRodItemSelectedListener {
        void onRodSelected(int position);
        void createRod(Rod rod);
        void execModel();
        void deleteSelectedRod(Rod rod);
        void updateSelectedRod(Rod rod);
        void editRod(Rod rod);
    }

    private View dialogContent;
    private AlertDialog alertDialog;

    //User Selection Context
    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Module selectedModule;

    private User mUser;
    private ProspectionHoleGroup selectedProspectionHoleGroup;
    private ProspectionHole selectedProspectionHole;
    private ProspectionHole selectedProspection;

    private TextView mTitle;

    private Button newRod;
    private Button execModel;

    private static EditText inputSpeed;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_prospection_calculations_rod_list, container, false);
        rootView.setTag(TAG);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        setRecyclerViewLayoutManager();
        mAdapter = new RodListAdapter(mRodDataset, mCallback, mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);

        dialogContent = inflater.inflate(R.layout.fragment_prospection_calculations_rod_new, null);
        inputSpeed = (EditText) dialogContent.findViewById(R.id.input_speed);
        ImageButton b1 = (ImageButton) dialogContent.findViewById(R.id.rod_time_btn);
        b1.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTimePickerDialog();
                    }
                });

        newRod = (Button) rootView.findViewById(R.id.new_rod);
        execModel = (Button) rootView.findViewById(R.id.execute_model);

        if(mRodDataset == null) {
            newRod.setVisibility(View.GONE);
            execModel.setVisibility(View.GONE);
        }
        else {
            if(mRodDataset.isEmpty()) {
                execModel.setVisibility(View.GONE);
            }
        }

        newRod.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                newRod();
            }
        });

        execModel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCallback.execModel();
            }
        });

        mTitle = (TextView) rootView.findViewById(R.id.prospection_title);

        if(selectedProspection == null) {
            mTitle.setText(getResources().getString(R.string.forecasting_label_select_ph_add_new));
            newRod.setVisibility(View.GONE);
        }
        else {
            mTitle.setText(selectedProspection.getName().trim());
            newRod.setVisibility(View.VISIBLE);
        }

        return rootView;
    }

    public void showTimePickerDialog() {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getChildFragmentManager(), "timePicker");
    }

    private double getSpeed(double time, double distance) {
        return distance/time;
    }

    private double toMins(String s) {
        String[] minSec = s.split(":");
        int mins = Integer.parseInt(minSec[0]);
        int secs = Integer.parseInt(minSec[1]);
        double secsD = secs;
        double secsInMins = secsD / 60.0;
        return round(secsInMins, 4) + mins;
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private void newRod() {

        if(alertDialog == null)
            alertDialog = new AlertDialog.Builder(getActivity())
                    .create();

        final int index = mRodDataset.size() + 1;

        alertDialog.setTitle(getResources().getString(R.string.forecasting_label_rod_new));
        alertDialog.setMessage(getResources().getString(R.string.forecasting_label_rod_new_msg) + " " + index);
        // Alert dialog button
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.system_label_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        inputSpeed = (EditText) dialogContent.findViewById(R.id.input_speed);
                        EditText inputAvgPressure = (EditText) dialogContent.findViewById(R.id.input_avg_pressure);
                        EditText inputCumDepth = (EditText) dialogContent.findViewById(R.id.input_cum_depth);
                        EditText inputWaterColour = (EditText) dialogContent.findViewById(R.id.input_water_colour);

                        boolean emptyTime = inputSpeed.getText().toString().trim().isEmpty();
                        boolean emptyAvgPressure = inputAvgPressure.getText().toString().trim().isEmpty();
                        boolean emptyDrilledLength = inputCumDepth.getText().toString().trim().isEmpty();

                        if (emptyTime) inputSpeed.setError(getResources().getString(R.string.forecasting_label_rod_enter_value));
                        if (emptyAvgPressure) inputAvgPressure.setError(getResources().getString(R.string.forecasting_label_rod_enter_value));
                        if (emptyDrilledLength) inputCumDepth.setError(getResources().getString(R.string.forecasting_label_rod_enter_value));

                        if (!emptyTime && !emptyAvgPressure && !emptyDrilledLength) {

                            Rod rod = new Rod();
                            rod.setName("Rod " + index);
                            rod.setTime(new Float(toMins(inputSpeed.getText().toString())));
                            rod.setAveragePressure(Float.parseFloat(inputAvgPressure.getText().toString()));
                            rod.setDrilledLength(Float.parseFloat(inputCumDepth.getText().toString()));
                            rod.setWaterColor(inputWaterColour.getText().toString());
                            rod.setCreatedAt(new Date());
                            rod.setIdProspectionHole(selectedProspection.getId().toUpperCase());
                            rod.setqPrevious(selectedProspectionHoleGroup.getqValueSelected());

                            double distance = rod.getDrilledLength().doubleValue();
                            double time = rod.getTime().doubleValue();
                            rod.setSpeed(new Float(getSpeed(time, distance)));

                            dialog.dismiss();
                            inputSpeed.setText("");
                            inputAvgPressure.setText("");
                            inputCumDepth.setText("");
                            inputWaterColour.setText("");
                            mCallback.createRod(rod);
                        }
                    }
                });

        alertDialog.setView(dialogContent);
        alertDialog.show();
    }

    private String minToFormat(Float minutes) {
        Double res = round(minutes.doubleValue(), 4) * 60000.0;
        long millis = res.longValue();

        String hms = String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        return hms;
    }

    public void editRod(Rod rod) {
        inputSpeed = (EditText) dialogContent.findViewById(R.id.input_speed);
        EditText inputAvgPressure = (EditText) dialogContent.findViewById(R.id.input_avg_pressure);
        EditText inputCumDepth = (EditText) dialogContent.findViewById(R.id.input_cum_depth);
        EditText inputWaterColour = (EditText) dialogContent.findViewById(R.id.input_water_colour);

        String timeFormat = minToFormat(rod.getTime());

        inputSpeed.setText(timeFormat);
        inputAvgPressure.setText(rod.getAveragePressure().toString());
        inputCumDepth.setText(rod.getDrilledLength().toString());
        inputWaterColour.setText(rod.getWaterColor());

        final Rod editRod = rod;

        if(alertDialog == null)
            alertDialog = new AlertDialog.Builder(getActivity())
                    .create();

        alertDialog.setTitle(getResources().getString(R.string.forecasting_label_rod_edit));
        alertDialog.setMessage(getResources().getString(R.string.forecasting_label_rod_edit_msg) + " " + editRod.getName().trim());
        // Alert dialog button
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.system_label_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        EditText inputSpeed = (EditText) dialogContent.findViewById(R.id.input_speed);
                        EditText inputAvgPressure = (EditText) dialogContent.findViewById(R.id.input_avg_pressure);
                        EditText inputCumDepth = (EditText) dialogContent.findViewById(R.id.input_cum_depth);
                        EditText inputWaterColour = (EditText) dialogContent.findViewById(R.id.input_water_colour);

                        boolean emptyTime = inputSpeed.getText().toString().trim().isEmpty();
                        boolean emptyAvgPressure = inputAvgPressure.getText().toString().trim().isEmpty();
                        boolean emptyDrilledLength = inputCumDepth.getText().toString().trim().isEmpty();

                        if (emptyTime) inputSpeed.setError(getResources().getString(R.string.forecasting_label_rod_enter_value));
                        if (emptyAvgPressure) inputAvgPressure.setError(getResources().getString(R.string.forecasting_label_rod_enter_value));
                        if (emptyDrilledLength) inputCumDepth.setError(getResources().getString(R.string.forecasting_label_rod_enter_value));

                        if (!emptyTime && !emptyAvgPressure && !emptyDrilledLength) {
                            editRod.setTime(new Float(toMins(inputSpeed.getText().toString())));
                            editRod.setAveragePressure(Float.parseFloat(inputAvgPressure.getText().toString()));
                            editRod.setDrilledLength(Float.parseFloat(inputCumDepth.getText().toString()));
                            editRod.setWaterColor(inputWaterColour.getText().toString());

                            double distance = editRod.getDrilledLength().doubleValue();
                            double time = editRod.getTime().doubleValue();
                            editRod.setSpeed(new Float(getSpeed(time, distance)));

                            dialog.dismiss();
                            inputSpeed.setText("");
                            inputAvgPressure.setText("");
                            inputCumDepth.setText("");
                            inputWaterColour.setText("");
                            mCallback.updateSelectedRod(editRod);
                        }
                    }
                });

        alertDialog.setView(dialogContent);
        alertDialog.show();
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnRodItemSelectedListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnRodItemSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setRecyclerViewLayoutManager() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMRodDataset(List<Rod> mRodDataset) {
        this.mRodDataset = mRodDataset;
    }

    public RecyclerView.LayoutManager getMLayoutManager() {
        return mLayoutManager;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void updateItemList() {
        if(mRodDataset == null) {
            newRod.setVisibility(View.GONE);
            execModel.setVisibility(View.GONE);
        }
        else {
            if(mRodDataset.isEmpty()) {
                execModel.setVisibility(View.GONE);
            }
            else {
                execModel.setVisibility(View.VISIBLE);
            }

            newRod.setVisibility(View.VISIBLE);
        }
        mTitle.setText(selectedProspection.getName().trim());
        mAdapter = new RodListAdapter(mRodDataset, mCallback, mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void setSelectionContext(Project project,
                                    Tunnel tunnel,
                                    Face face,
                                    Module module,
                                    User user,
                                    ProspectionHole prospectionHole,
                                    ProspectionHole prospection,
                                    ProspectionHoleGroup prospectionHoleGroup) {
        selectedProject = project;
        selectedTunnel = tunnel;
        selectedFace = face;
        selectedModule = module;
        mUser = user;
        selectedProspectionHole = prospectionHole;
        selectedProspection = prospection;
        selectedProspectionHoleGroup = prospectionHoleGroup;
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            inputSpeed.setText(hourOfDay + ":" + (minute < 10 ? "0" + minute : minute));
        }
    }
}
