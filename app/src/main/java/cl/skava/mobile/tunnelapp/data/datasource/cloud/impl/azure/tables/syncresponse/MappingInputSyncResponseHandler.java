package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse;

import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.Project;

/**
 * Created by JoseVera on 4/18/2017.
 */

public class MappingInputSyncResponseHandler extends FetchFromTableSyncResponseHandler<Project> {

    MappingInputSyncResponseHandler(){}

    @Override
    protected void onDataFetchedSuccessfully(CloudMobileService cloudMobileService, List<Project> projects) {
        CloudStoreService cloudStoreService = (CloudStoreService) cloudMobileService;
        cloudStoreService.syncTunnels(projects);
    }

    @Override
    protected void initializeClassToFetchFrom() {
        setClassToFetchFrom(Project.class);
    }
}
