package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class ExpandedTunnel extends DataInput {

    private String id;
    private String idMapping;
    private Integer code;
    private String sideName;
    private String remoteUri;
    private String localUri;
    private Boolean completed;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getSideName() {
        return sideName;
    }

    public void setSideName(String sideName) {
        this.sideName = sideName;
    }

    public String getRemoteUri() {
        return remoteUri;
    }

    public void setRemoteUri(String remoteUri) {
        this.remoteUri = remoteUri;
    }

    public String getLocalUri() {
        return localUri;
    }

    public void setLocalUri(String localUri) {
        this.localUri = localUri;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "ExpandedTunnel{" +
                "id='" + id + '\'' +
                ", idMapping='" + idMapping + '\'' +
                ", code=" + code +
                ", sideName='" + sideName + '\'' +
                ", remoteUri='" + remoteUri + '\'' +
                ", localUri='" + localUri + '\'' +
                ", completed=" + completed +
                '}';
    }
}
