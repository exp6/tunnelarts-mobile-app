package cl.skava.mobile.tunnelapp.presentation.views.geoclassifier.mapping.expandedtunnel;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.rm.freedrawview.FreeDrawView;

/**
 * Created by Carlos Vergara on 23-02-2017.
 */

public class LockableDraw extends FreeDrawView {
    private boolean isLocked = true;

    public LockableDraw(Context context) {
        super(context);
    }

    public LockableDraw(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LockableDraw(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    /*@Override
    protected void onDraw(Canvas canvas) {
        getParent().requestDisallowInterceptTouchEvent(true);
        super.onDraw(canvas);
    }*/

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return !isLocked;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        //getParent().requestDisallowInterceptTouchEvent(true);
        return (isLocked)? false : super.onTouch(view, motionEvent);
    }
}
