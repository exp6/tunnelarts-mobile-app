package cl.skava.mobile.tunnelapp.presentation.ui.fragment.home;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Module;

/**
 * Created by Jose Ignacio Vera on 07-06-2016.
 */
public class FaceListFragment extends Fragment
        implements ModuleListFragment.OnModuleItemListener {
    private TabLayout tabLayout;
    protected List<Face> mFacesDataset;
    protected List<Module> mModulesDataset;

    private OnFaceModuleItemListener mCallback;

    public interface OnFaceModuleItemListener {
        void onFaceSelected(Face face);
        void onModuleSelected(Module module);
        void userModulesLoaded();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_faces, container, false);
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.home_label_faces_title));

        for(Face f : mFacesDataset) {
            tabLayout.addTab(tabLayout.newTab().setText(f.getName()));
        }

        ModuleListFragment fragment = new ModuleListFragment();
        fragment.setMModulesDataset(mModulesDataset);
        fragment.onAttachFragment(this);
        updateFragment(fragment, R.id.module_list);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mCallback.onFaceSelected(getFaceByName(tab.getText().toString().trim()));
//                ModuleListFragment fragment = new ModuleListFragment();
//                fragment.setMModulesDataset(mModulesDataset);
//                updateFragment(fragment, R.id.pk_input_content);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        return rootView;
    }

    private Face getFaceByName(String name) {
        Face result = null;

        for (Face f : mFacesDataset) {
            if(f.getName().equalsIgnoreCase(name)) {
                result = f;
                break;
            }
        }
        return result;
    }

    private void updateFragment(Fragment fragment, int container) {
//        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(container, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commitAllowingStateLoss();
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnFaceModuleItemListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnFaceModuleItemListener");
        }
    }

    public void setMFacesDataset(List<Face> mFacesDataset) {
        this.mFacesDataset = mFacesDataset;
    }

    public void setMModulesDataset(List<Module> mModulesDataset) {
        this.mModulesDataset = mModulesDataset;
    }

    public void updateItemList() {
        tabLayout.removeAllTabs();

        for(Face f : mFacesDataset) {
            tabLayout.addTab(tabLayout.newTab().setText(f.getName()));
        }
    }

    @Override
    public void onModuleSelected(int position) {
        mCallback.onModuleSelected(mModulesDataset.get(position));
    }

    @Override
    public void userModulesLoaded() {
        mCallback.userModulesLoaded();
    }
}
