package cl.skava.mobile.tunnelapp.data.cache;

import com.google.gson.Gson;

import cl.skava.mobile.tunnelapp.domain.model.account.User;

/**
 * Created by Jose Ignacio Vera on 27-07-2016.
 */
public class JsonSerializer {
    private final Gson gson = new Gson();

    public String serialize(User user) {
        String jsonString = gson.toJson(user, User.class);
        return jsonString;
    }

    public User deserialize(String jsonString) {
        User userEntity = gson.fromJson(jsonString, User.class);
        return userEntity;
    }
}
