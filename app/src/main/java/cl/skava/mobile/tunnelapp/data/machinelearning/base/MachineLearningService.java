package cl.skava.mobile.tunnelapp.data.machinelearning.base;

import org.xml.sax.SAXException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;

/**
 * Created by Jose Ignacio Vera on 02-08-2016.
 */
public interface MachineLearningService {
    void executeModel();
    List<Rod> getFinalRods();
}
