package cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.mapping.summary;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RecommendationRockbolt;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.recommendations.RockboltsFragment;

/**
 * Created by Jose Ignacio Vera on 12-10-2016.
 */
public class RockboltListAdapter extends RecyclerView.Adapter<RockboltListAdapter.ViewHolder>  {

    private static final String TAG = "RockboltListAdapter";
    protected RecyclerView mRecyclerView;
    private List<RecommendationRockbolt> mRockbolts;
    private RockboltsFragment mCallback;
    static ViewHolder viewHolderPool[];
    ViewHolder viewHolderPool1[];
    private final String lang = Locale.getDefault().getLanguage();

    public RockboltListAdapter(List<RecommendationRockbolt> rockbolts, RecyclerView recyclerView, RockboltsFragment callback) {
        mRecyclerView = recyclerView;
        mRockbolts = rockbolts;
        mCallback = callback;
        viewHolderPool = new ViewHolder[mRockbolts.size()];
        viewHolderPool1 = viewHolderPool;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView boltLabel;
        private final Spinner type;
        private final EditText quantity;
        private final EditText pk;
        private final EditText length;
        private final Spinner diameter;
        private final EditText spacing;
        private final EditText angle;
        private final EditText timeFrom;
        private final EditText timeTo;
        private final EditText positionHorizontal;
        private final EditText positionVertical;

        public ViewHolder(View v) {
            super(v);
            boltLabel = (TextView) v.findViewById(R.id.bolt_label);
            type = (Spinner) v.findViewById(R.id.spinner_type_of_bolt);
            quantity = (EditText) v.findViewById(R.id.input_quantity);
            pk = (EditText) v.findViewById(R.id.input_pk);
            length = (EditText) v.findViewById(R.id.input_length);
            diameter = (Spinner) v.findViewById(R.id.spinner_diameter);
            spacing = (EditText) v.findViewById(R.id.input_spacing);
            angle = (EditText) v.findViewById(R.id.input_angle);
            timeFrom = (EditText) v.findViewById(R.id.input_rockbolt_time_from);
            timeTo = (EditText) v.findViewById(R.id.input_rockbolt_time_to);
            positionHorizontal = (EditText) v.findViewById(R.id.input_position_horizontal);
            positionVertical = (EditText) v.findViewById(R.id.input_position_vertical);
        }

        public TextView getBoltLabel() {
            return boltLabel;
        }

        public Spinner getType() {
            return type;
        }

        public EditText getQuantity() {
            return quantity;
        }

        public EditText getPk() {
            return pk;
        }

        public EditText getLength() {
            return length;
        }

        public Spinner getDiameter() {
            return diameter;
        }

        public EditText getSpacing() {
            return spacing;
        }

        public EditText getAngle() {
            return angle;
        }

        public EditText getTimeFrom() {
            return timeFrom;
        }

        public EditText getTimeTo() {
            return timeTo;
        }

        public EditText getPositionHorizontal() {
            return positionHorizontal;
        }

        public EditText getPositionVertical() {
            return positionVertical;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_geoclassifier_mapping_summary_recommendations_rockbolts_item, parent, false);
        holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RecommendationRockbolt rockbolt = mRockbolts.get(position);
        if (viewHolderPool[position] == null) viewHolderPool[position] = holder;
        final int position1 = position;
        viewHolderPool[position].getBoltLabel().setText((lang.equals("es") ? "Perno " : "Bolt ") + (rockbolt.getPosition() + 1));
        viewHolderPool[position].getType().setSelection(rockbolt.getType());
        viewHolderPool[position].getQuantity().setText(rockbolt.getQuantity().toString());
        viewHolderPool[position].getPk().setText(rockbolt.getPk().toString());
        viewHolderPool[position].getLength().setText(rockbolt.getLength().toString());
        viewHolderPool[position].getDiameter().setSelection(rockbolt.getDiameter());
        viewHolderPool[position].getSpacing().setText(rockbolt.getSpacing().toString());
        viewHolderPool[position].getAngle().setText(rockbolt.getAngle().toString());
        viewHolderPool[position].getTimeFrom().setText(rockbolt.getTimeFrom());
        viewHolderPool[position].getTimeTo().setText(rockbolt.getTimeTo());
        viewHolderPool[position].getPositionHorizontal().setText(rockbolt.getHorizontal().toString());
        viewHolderPool[position].getPositionVertical().setText(rockbolt.getVertical().toString());

        ImageView deleteItem = (ImageView) viewHolderPool[position].itemView.findViewById(R.id.delete_item);
        final ImageButton b1 = (ImageButton) viewHolderPool[position].itemView.findViewById(R.id.rockbolt_time_from_btn);
        b1.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTimePickerDialog(position1, 0);
                    }
                });
        final ImageButton b2 = (ImageButton) viewHolderPool[position].itemView.findViewById(R.id.rockbolt_time_to_btn);
        b2.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTimePickerDialog(position1, 1);
                    }
                });

        deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.deleteItem(position1);
            }
        });
        deleteItem.setVisibility(View.GONE);

        viewHolderPool[position].getType().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (position > 1) {
                    viewHolderPool[position1].getSpacing().setEnabled(true);

                    viewHolderPool[position1].getTimeFrom().setEnabled(true);
                    viewHolderPool[position1].getTimeTo().setEnabled(true);

                    b1.setEnabled(true);
                    b2.setEnabled(true);

                    viewHolderPool[position1].getPositionHorizontal().setText("");
                    viewHolderPool[position1].getPositionVertical().setText("");

                    viewHolderPool[position1].getPositionHorizontal().setEnabled(false);
                    viewHolderPool[position1].getPositionVertical().setEnabled(false);
                } else {
                    if (position == 1) {
                        viewHolderPool[position1].getSpacing().setText("0.0");
                        viewHolderPool[position1].getSpacing().setEnabled(false);

                        viewHolderPool[position1].getTimeFrom().setText("");
                        viewHolderPool[position1].getTimeTo().setText("");
                        viewHolderPool[position1].getTimeFrom().setEnabled(false);
                        viewHolderPool[position1].getTimeTo().setEnabled(false);

                        b1.setEnabled(false);
                        b2.setEnabled(false);

                        viewHolderPool[position1].getPositionHorizontal().setEnabled(true);
                        viewHolderPool[position1].getPositionVertical().setEnabled(true);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        if(position == mRockbolts.size()-1 && mRockbolts.size() > 1) deleteItem.setVisibility(View.VISIBLE);
        viewHolderPool1 = viewHolderPool;
    }

    @Override
    public int getItemCount() {
        return mRockbolts.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public List<RecommendationRockbolt> getmRockbolts() {
        return mRockbolts;
    }

    public void showTimePickerDialog(int position, int inputTimeField) {
        DialogFragment newFragment = new TimePickerFragment();
        Bundle args = new Bundle();
        args.putInt("positionTime", position);
        args.putInt("inputTimeField", inputTimeField);
        newFragment.setArguments(args);
        newFragment.show(mCallback.getChildFragmentManager(), "timePicker");
    }

    public ViewHolder[] getViewHolderPool() {
        return viewHolderPool1;
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        private int positionTime;
        private int inputTimeField;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            Bundle args = getArguments();
            positionTime = args.getInt("positionTime");
            inputTimeField = args.getInt("inputTimeField");
            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            EditText mTime;

            switch(inputTimeField) {
                case 1:
                    mTime = (EditText) viewHolderPool[positionTime].itemView.findViewById(R.id.input_rockbolt_time_to);
                    break;
                default:
                    mTime = (EditText) viewHolderPool[positionTime].itemView.findViewById(R.id.input_rockbolt_time_from);
                    break;
            }

            mTime.setText(hourOfDay + ":" + (minute < 10 ? "0" + minute : minute));
        }
    }
}
