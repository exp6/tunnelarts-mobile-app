package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by Jose Ignacio Vera on 12-10-2016.
 */
public class RecommendationRockbolt extends DataInput {

    private String id;
    private Integer position;
    private Integer type;
    private Integer quantity;
    private Float pk;
    private Float length;
    private Integer diameter;
    private Float spacing;
    private Float angle;
    private String timeFrom;
    private String timeTo;
    private Float horizontal;
    private Float vertical;
    private String idMapping;
    private Boolean completed;

    public RecommendationRockbolt(String id, Integer position, Integer type, Integer quantity, Float pk, Float length, Integer diameter, Float spacing, Float angle, String timeFrom, String timeTo, Float horizontal, Float vertical, String idMapping, Boolean completed) {
        this.id = id;
        this.position = position;
        this.type = type;
        this.quantity = quantity;
        this.pk = pk;
        this.length = length;
        this.diameter = diameter;
        this.spacing = spacing;
        this.angle = angle;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.horizontal = horizontal;
        this.vertical = vertical;
        this.idMapping = idMapping;
        this.completed = completed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Float getPk() {
        return pk;
    }

    public void setPk(Float pk) {
        this.pk = pk;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Integer getDiameter() {
        return diameter;
    }

    public void setDiameter(Integer diameter) {
        this.diameter = diameter;
    }

    public Float getSpacing() {
        return spacing;
    }

    public void setSpacing(Float spacing) {
        this.spacing = spacing;
    }

    public Float getAngle() {
        return angle;
    }

    public void setAngle(Float angle) {
        this.angle = angle;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public Float getHorizontal() {
        return horizontal;
    }

    public void setHorizontal(Float horizontal) {
        this.horizontal = horizontal;
    }

    public Float getVertical() {
        return vertical;
    }

    public void setVertical(Float vertical) {
        this.vertical = vertical;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "RecommendationRockbolt{" +
                "id='" + id + '\'' +
                ", position=" + position +
                ", type=" + type +
                ", quantity=" + quantity +
                ", pk=" + pk +
                ", length=" + length +
                ", diameter=" + diameter +
                ", spacing=" + spacing +
                ", angle=" + angle +
                ", timeFrom='" + timeFrom + '\'' +
                ", timeTo='" + timeTo + '\'' +
                ", horizontal=" + horizontal +
                ", vertical=" + vertical +
                ", idMapping='" + idMapping + '\'' +
                ", completed=" + completed +
                '}';
    }
}
