package cl.skava.mobile.tunnelapp.domain.model.mapping;

import java.io.File;

/**
 * Created by Jose Ignacio Vera on 08-08-2016.
 */
public class Particularities extends DataInput {

    private String id;
    private String instabilityCausedBy;
    private String instabilityLocation;
    private String overbreakCausedBy;
    private String overbreakLocation;
    private Integer overbreakVolume;
    private String idMapping;
    private Boolean completed;
    private String overbreakLocationStart;
    private String overbreakLocationEnd;
    private Boolean overbreakNa;
    private Boolean instabilityNa;
    private Integer overbreakCausedByValue;
    private String overbreakPictureRemoteUri;

    public Particularities(String id, String instabilityCausedBy, String instabilityLocation, String overbreakCausedBy, String overbreakLocation, Integer overbreakVolume, String idMapping, Boolean completed, String overbreakLocationStart, String overbreakLocationEnd, Boolean overbreakNa, Boolean instabilityNa, Integer overbreakCausedByValue) {
        this.id = id;
        this.instabilityCausedBy = instabilityCausedBy;
        this.instabilityLocation = instabilityLocation;
        this.overbreakCausedBy = overbreakCausedBy;
        this.overbreakLocation = overbreakLocation;
        this.overbreakVolume = overbreakVolume;
        this.idMapping = idMapping;
        this.completed = completed;
        this.overbreakLocationStart = overbreakLocationStart;
        this.overbreakLocationEnd = overbreakLocationEnd;
        this.overbreakNa = overbreakNa;
        this.instabilityNa = instabilityNa;
        this.overbreakCausedByValue = overbreakCausedByValue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstabilityCausedBy() {
        return instabilityCausedBy;
    }

    public void setInstabilityCausedBy(String instabilityCausedBy) {
        this.instabilityCausedBy = instabilityCausedBy;
    }

    public String getInstabilityLocation() {
        return instabilityLocation;
    }

    public void setInstabilityLocation(String instabilityLocation) {
        this.instabilityLocation = instabilityLocation;
    }

    public String getOverbreakCausedBy() {
        return overbreakCausedBy;
    }

    public void setOverbreakCausedBy(String overbreakCausedBy) {
        this.overbreakCausedBy = overbreakCausedBy;
    }

    public String getOverbreakLocation() {
        return overbreakLocation;
    }

    public void setOverbreakLocation(String overbreakLocation) {
        this.overbreakLocation = overbreakLocation;
    }

    public Integer getOverbreakVolume() {
        return overbreakVolume;
    }

    public void setOverbreakVolume(Integer overbreakVolume) {
        this.overbreakVolume = overbreakVolume;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public String getOverbreakLocationStart() {
        return overbreakLocationStart;
    }

    public void setOverbreakLocationStart(String overbreakLocationStart) {
        this.overbreakLocationStart = overbreakLocationStart;
    }

    public String getOverbreakLocationEnd() {
        return overbreakLocationEnd;
    }

    public void setOverbreakLocationEnd(String overbreakLocationEnd) {
        this.overbreakLocationEnd = overbreakLocationEnd;
    }

    public Boolean getOverbreakNa() {
        return overbreakNa;
    }

    public void setOverbreakNa(Boolean overbreakNa) {
        this.overbreakNa = overbreakNa;
    }

    public Boolean getInstabilityNa() {
        return instabilityNa;
    }

    public void setInstabilityNa(Boolean instabilityNa) {
        this.instabilityNa = instabilityNa;
    }

    public Integer getOverbreakCausedByValue() {
        return overbreakCausedByValue;
    }

    public void setOverbreakCausedByValue(Integer overbreakCausedByValue) {
        this.overbreakCausedByValue = overbreakCausedByValue;
    }

    public String getOverbreakPictureRemoteUri() {
        return overbreakPictureRemoteUri;
    }

    public void setOverbreakPictureRemoteUri(String overbreakPictureRemoteUri) {
        this.overbreakPictureRemoteUri = overbreakPictureRemoteUri;
    }

    @Override
    public String toString() {
        return "Particularities{" +
                "id='" + id + '\'' +
                ", instabilityCausedBy='" + instabilityCausedBy + '\'' +
                ", instabilityLocation='" + instabilityLocation + '\'' +
                ", overbreakCausedBy='" + overbreakCausedBy + '\'' +
                ", overbreakLocation='" + overbreakLocation + '\'' +
                ", overbreakVolume=" + overbreakVolume +
                ", idMapping='" + idMapping + '\'' +
                ", completed=" + completed +
                ", overbreakLocationStart='" + overbreakLocationStart + '\'' +
                ", overbreakLocationEnd='" + overbreakLocationEnd + '\'' +
                ", overbreakNa=" + overbreakNa +
                ", instabilityNa=" + instabilityNa +
                ", overbreakCausedByValue=" + overbreakCausedByValue +
                ", overbreakPictureRemoteUri=" + overbreakPictureRemoteUri +
                '}';
    }
}
