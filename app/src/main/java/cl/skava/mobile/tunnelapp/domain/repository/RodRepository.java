package cl.skava.mobile.tunnelapp.domain.repository;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public interface RodRepository {

    boolean insert(Rod rod);

    boolean update(Rod rod);

    boolean delete(Rod rod);

    List<Rod> getList();
}
