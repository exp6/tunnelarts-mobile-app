package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse.SyncResponseHandler;
import cl.skava.mobile.tunnelapp.domain.repository.BaseRepository;

/**
 * Created by Carlos Vergara on 20-03-2017.
 */

public class DefaultSyncResponseHandler implements SyncResponseHandler {

    DefaultSyncResponseHandler() {

    }

    @Override
    public void handleResponseFromService(CloudMobileService cloudMobileService) {
        CloudStoreService cloudStoreService = (CloudStoreService) cloudMobileService;
        if (cloudStoreService.isPicturesOnly()) {
            cloudStoreService.sendStorageResultCodeToCallback(2);
        } else if (cloudStoreService.isExpandedOnly()) {
            cloudStoreService.setExpandedOnly(false);
            cloudStoreService.sendStorageResultCodeToCallback(10);
        } else if (cloudStoreService.isOverbreakOnly()) {
            cloudStoreService.setOverbreakOnly(false);
            cloudStoreService.sendStorageResultCodeToCallback(5);
        } else if(cloudStoreService.isStereonetOnly()) {
            cloudStoreService.setStereonetOnly(false);
            cloudStoreService.sendStorageResultCodeToCallback(0);
        } else {
            cloudStoreService.increaseMappingProgressIndex();
            cloudStoreService.finishMappingOrProceed();
        }
    }
}
