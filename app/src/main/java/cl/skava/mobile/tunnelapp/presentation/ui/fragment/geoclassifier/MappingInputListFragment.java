package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public class MappingInputListFragment extends Fragment
        implements MappingInputListItemsFragment.OnModuleItemListener {

    private static final String TAG = "MappingInputListFragment";
    protected List<MappingInputModule> mMappingInputModuleDataset;
    private OnModuleItemListener mCallback;
    private MappingInputModule selectedMappingInputModule = null;
    private int itemPos = 0;

    @Override
    public void onModuleItemSelected(int position, int listOrder) {

        List<Integer> positions = new ArrayList<>();

        switch (listOrder) {
            case 1:
                positions.add(selectedMappingInputModule.getCode());
                positions.add(position);
                mCallback.onModuleSelected(positions);
                break;
            default:
                selectedMappingInputModule = mMappingInputModuleDataset.get(position);
                itemPos = position;

                if(selectedMappingInputModule.getSubInputs() == null) {
                    positions.add(selectedMappingInputModule.getCode());
                    mCallback.onModuleSelected(positions);
                }
                else {
                    mPager.getAdapter().notifyDataSetChanged();
                    mPager.setCurrentItem(1, true);
                }
                break;
        }
    }

    @Override
    public void userModulesLoaded() {
        mCallback.userModulesLoaded();
    }

    public interface OnModuleItemListener {
        void onModuleSelected(List<Integer> metaPositions);
        void userModulesLoaded();
    }

    private Mapping selectedMapping;

    private TextView title;
    private ChainageNumberFormat chainageNumberFormat;

    private View rootView;

    private static final int NUM_LISTS = 2;

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnModuleItemListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnModuleItemListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_inputs, container, false);
        rootView.setTag(TAG);

        mPager = (ViewPager) rootView.findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager(), this);
        mPager.setAdapter(mPagerAdapter);

        title = (TextView) rootView.findViewById(R.id.mapping_input_title);
        chainageNumberFormat = new ChainageNumberFormat();

        if(selectedMapping != null)
            title.setText(chainageNumberFormat.format(selectedMapping.getChainageStart()) + " - " + chainageNumberFormat.format(selectedMapping.getChainageEnd()));

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMMappingInputDataset(List<MappingInputModule> mMappingInputModuleDataset) {
        this.mMappingInputModuleDataset = mMappingInputModuleDataset;
//        mCallback.getSelectedMapping();
    }

    public void updateItemList() {
        mPagerAdapter = new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager(), this);
        mPager.setAdapter(mPagerAdapter);
    }

    public void setSelectedMapping(Mapping mapping) {
        selectedMapping = mapping;

        if(title != null) {
            if(selectedMapping != null) {
                title.setText(chainageNumberFormat.format(selectedMapping.getChainageStart()) + " - " + chainageNumberFormat.format(selectedMapping.getChainageEnd()));
            }
            else {
                title.setText(getResources().getString(R.string.characterization_label_select_mapping_add_new));
            }
        }
    }

    class ChainageNumberFormat extends DecimalFormat {

        public ChainageNumberFormat() {
            DecimalFormatSymbols custom = new DecimalFormatSymbols();
            custom.setGroupingSeparator('+');
            custom.setDecimalSeparator(',');
            setMinimumFractionDigits(2);
            setMaximumFractionDigits(2);

            setDecimalFormatSymbols(custom);
        }
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        private Fragment mCallback;

        public ScreenSlidePagerAdapter(FragmentManager fm, Fragment callback) {
            super(fm);
            mCallback = callback;
        }

        @Override
        public Fragment getItem(int position) {
            MappingInputListItemsFragment fr = new MappingInputListItemsFragment();
            fr.onAttachFragment(mCallback);
            fr.setSelectedMapping(selectedMapping);
            fr.setMMappingInputDataset(mMappingInputModuleDataset);
            fr.setListOrder(position);
            fr.setItemPosition(itemPos);

            if(position == 1) {
                fr.setItemPosition(0);

                if(selectedMappingInputModule == null) {
                    selectedMappingInputModule = mMappingInputModuleDataset.isEmpty() ? new MappingInputModule() : mMappingInputModuleDataset.get(0);
                }

                fr.setSelectedMappingInputModule(selectedMappingInputModule);

                if(selectedMappingInputModule.getSubInputs() == null) {
                    fr.setMMappingInputDataset(new ArrayList<MappingInputModule>());
                } else {
                    fr.setMMappingInputDataset(selectedMappingInputModule.getSubInputs());
                }
            }
            return fr;
        }

        @Override
        public int getCount() {
            return NUM_LISTS;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }
}
