package cl.skava.mobile.tunnelapp.presentation.ui.adapters.prospection;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.prospection.ProspectionHoleGroupListFragment;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class ProspectionHoleGroupListAdapter extends RecyclerView.Adapter<ProspectionHoleGroupListAdapter.ViewHolder> {

    private static final String TAG = "ProspectionHoleGroupListAdapter";
    protected List<ProspectionHoleGroup> mProspectionHoleGroupDataset;

    private int selectedItem = 0;
    ViewHolder viewHolderPool[];
    protected RecyclerView mRecyclerView;
    private ProspectionHoleGroupListFragment.OnProspectionHoleGroupListlListener mCallback;

    public ProspectionHoleGroupListAdapter(List<ProspectionHoleGroup> dataSet,
                                           RecyclerView recyclerView,
                                           ProspectionHoleGroupListFragment.OnProspectionHoleGroupListlListener callback,
                                           Integer itemPosition
    ) {
        mProspectionHoleGroupDataset = dataSet;
        mCallback = callback;
        viewHolderPool = new ViewHolder[mProspectionHoleGroupDataset.size()];
        mRecyclerView = recyclerView;
        setHasStableIds(true);
        selectedItem = itemPosition;

//        if(selectedItem > 0) {
//            mCallback.onProspectionHoleGroupSelected(selectedItem, true);
//            notifyDataSetChanged();
//            mRecyclerView.smoothScrollToPosition(selectedItem);
//        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameTextView;
        private final TextView qValueTextView;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedItem = getLayoutPosition();
                    mCallback.onProspectionHoleGroupSelected(selectedItem, false);

                    for (ViewHolder vh : viewHolderPool) {
                        vh.itemView.setSelected(false);
                        vh.itemView.setBackgroundColor(Color.TRANSPARENT);
                    }
                    notifyDataSetChanged();
                    mRecyclerView.smoothScrollToPosition(selectedItem);
                }
            });

            nameTextView = (TextView) v.findViewById(R.id.prospection_hole_group_name);
            qValueTextView = (TextView) v.findViewById(R.id.phg_qvalue);
        }

        public TextView getNameTextView() {
            return nameTextView;
        }

        public TextView getqValueTextView() {
            return qValueTextView;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_prospection_hole_group_row_item, parent, false);
        holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProspectionHoleGroup prospectionHoleGroup = mProspectionHoleGroupDataset.get(position);

        holder.getNameTextView().setText(prospectionHoleGroup.getName().trim());
        holder.getqValueTextView().setText(round(prospectionHoleGroup.getqValueSelected(), 4)+"");
//        ImageView lock = ((ImageView) holder.itemView.findViewById(R.id.locked_icon));

//        if(mapping.getFinished()) lock.setImageResource(R.drawable.ic_lock);

        if (viewHolderPool[position] == null)
            viewHolderPool[position] = holder;

        holder.itemView.setSelected(selectedItem == position);
        holder.itemView.setBackgroundColor(holder.itemView.isSelected() ? Color.LTGRAY : Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return mProspectionHoleGroupDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
