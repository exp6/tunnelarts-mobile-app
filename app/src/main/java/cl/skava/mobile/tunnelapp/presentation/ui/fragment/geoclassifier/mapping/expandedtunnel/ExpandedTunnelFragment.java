package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.expandedtunnel;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.ExpandedTunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.presentation.ui.activity.geoclassifier.mapping.DrawExpandedTunnelActivity;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.mapping.expandedtunnel.ExpandedViewAdapter;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class ExpandedTunnelFragment extends Fragment {

    public static final String TAG = "ExpandedTunnelFragment";

    private RecyclerView recyclerView;

    private int selectedPicPos;
    public int EDIT_PICTURE_REQUEST_CODE = 1;

    private Uri[] pictureURIs;
    private ImageView[] mImgs;
    private Double[] initialCh;
    private Double[] finalCh;

    private Boolean mappingFinished;

    private File[] mFiles;

    private ExpandedTunnel mappingPicture;

    protected List<Mapping> mMappingDataset;

    private Mapping selectedMapping;

    private Tunnel tunnel;

    private File[] mFilesAux;

    private String pathAviaryDir = "/sdcard/DCIM/100AVIARY";

    private boolean writePermissionGranted;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        writePermissionGranted = ActivityCompat.checkSelfPermission(this.getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_expandedtunnel, container, false);
        rootView.setTag(TAG);

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.characterization_mappings_label_horizontal_segment)+" "+(tunnel.getPerimeter()/12+" mts"));

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        initDataset();
        pictureURIs = new Uri[mImgs.length];

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new ExpandedViewAdapter(this, mImgs, initialCh, finalCh, mFilesAux, mappingFinished));

        return rootView;
    }

    private void initDataset() {
        List<Mapping> mappingDatasubset = new ArrayList<>();
        mappingDatasubset.add(selectedMapping);

        int selectedMappingIndex = 0;

        for(int i = 0; i < mMappingDataset.size(); i++) {
            Mapping m = mMappingDataset.get(i);
            if(!m.getId().equalsIgnoreCase(selectedMapping.getId())) continue;
            else {
                selectedMappingIndex = i;
                break;
            }
        }

        int datasetSize = mMappingDataset.size() - selectedMappingIndex;
        mImgs = new ImageView[datasetSize];
        initialCh = new Double[datasetSize];
        finalCh = new Double[datasetSize];

        for(int i = 0; i < datasetSize; i++) {
            Mapping m = mMappingDataset.get(selectedMappingIndex + i);
            initialCh[i] = m.getChainageStart();
            finalCh[i] = m.getChainageEnd();
        }

        mFilesAux = new File[datasetSize];
        mFilesAux[0] = mFiles[0];

        File mediaDirectory = new File(getActivity().getExternalFilesDir(null).getPath() + File.separator + "media");
        File newFile;

        for(int i = 1; i < datasetSize; i++) {
            Mapping m = mMappingDataset.get(selectedMappingIndex + i);
            newFile = new File(mediaDirectory.getPath() + File.separator + m.getId().toLowerCase() + File.separator + "expanded.png");
            mFilesAux[i] = newFile.exists() ? newFile : null;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void editPicture(int location) throws Exception {
        selectedPicPos = location;
        if (mappingPicture == null || mappingPicture.getId().isEmpty()) {
            doDeletePicture(location);
        }
        dispatchEditPictureIntent();
    }

    private void doDeletePicture(int location) {
        mImgs[location] = null;
        mFiles[0] = null;

        File extendedViewFile = getOutputFile(mappingPicture);
        File extendedViewFileExport = getExportFile(mappingPicture);

        if (extendedViewFile.exists()) {
            try {
                extendedViewFile.getCanonicalFile().delete();
            } catch (IOException e) {
                // Nada, solo debe hacer lo del finally
            }
            finally {
                if (extendedViewFile.exists()) {
                    getActivity().getApplicationContext().deleteFile(extendedViewFile.getName());
                }
            }
        }

        if (extendedViewFileExport.exists()) {
            try {
                extendedViewFileExport.getCanonicalFile().delete();
            } catch (IOException e) {
                // Nada, solo debe hacer lo del finally
            }
            finally {
                if (extendedViewFileExport.exists()) {
                    getActivity().getApplicationContext().deleteFile(extendedViewFileExport.getName());
                }
            }
        }

        /*Bitmap baseAsBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.expanded_tunnel_light);
        File extendedViewFile = getOutputFile(mappingPicture);
        FileOutputStream fos = null;
        try {
            //create a file from the bitmap
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            baseAsBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bos);
            byte[] bitmapdata = bos.toByteArray();
            //write the bytes in file
            fos = new FileOutputStream(extendedViewFile);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();

        }
        pictureURIs[selectedPicPos] = Uri.fromFile(extendedViewFile);*/

        updateDataset();
    }

    public void deletePicture(int location) {

        final int pos = location;

        new AlertDialog.Builder(getActivity())
                .setTitle("Delete edition")
                .setMessage("Are you sure to delete this edition?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        doDeletePicture(pos);
                    }})
                .setNegativeButton(android.R.string.no, null).show();


    }

    private void dispatchEditPictureIntent() throws Exception {

        /*if (mImgs[selectedPicPos] == null){
            Bitmap baseAsBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.expanded_tunnel_light);
            File extendedViewFile = getOutputFile(mappingPicture);
            FileOutputStream fos = null;
            try {
                //create a file from the bitmap
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                baseAsBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bos);
                byte[] bitmapdata = bos.toByteArray();
                //write the bytes in file
                fos = new FileOutputStream(extendedViewFile);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();

            }
            pictureURIs[selectedPicPos] = Uri.fromFile(extendedViewFile);
        }
        else {
            pictureURIs[selectedPicPos] = Uri.fromFile(mFilesAux[0]);
        }

        Intent newIntent = new Intent("aviary.intent.action.EDIT");
        newIntent.setDataAndType(pictureURIs[selectedPicPos], "image/*"); // required
        newIntent.putExtra("app-id", getActivity().getPackageName());
        String[] tools = new String[]{"BRIGHTNESS", "CROP", "DRAW", "TEXT"};
        newIntent.putExtra("tools-list", tools);
        newIntent.putExtra("extra-api-key-secret", "cd9f4966112e789c");
        newIntent.putExtra("white-label", "");

        File mediaDirectory1 = new File(pathAviaryDir);
        FileManager fm = new FileManager();
        fm.clearDirectory(mediaDirectory1);

        startActivityForResult(newIntent, EDIT_PICTURE_REQUEST_CODE);*/
        Intent newIntent = new Intent(this.getContext(), DrawExpandedTunnelActivity.class);
        newIntent.putExtra("initial-ch", this.initialCh[0]);
        newIntent.putExtra("final-ch", this.finalCh[0]);
        newIntent.putExtra("perimeter", tunnel.getPerimeter());
        newIntent.putExtra("filename", this.getOutputFile(mappingPicture).getAbsolutePath());
        startActivityForResult(newIntent, EDIT_PICTURE_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageView mImageView = new ImageView(getActivity());

        if (resultCode == getActivity().RESULT_OK && requestCode == EDIT_PICTURE_REQUEST_CODE) {
            Uri editedImageUri = data.getData();
            mImageView.setImageURI(editedImageUri);
            mImgs[selectedPicPos] = mImageView;
            pictureURIs[selectedPicPos] = editedImageUri;

            try {
                File mFile = getOutputFile(mappingPicture);

                FileOutputStream fos = new FileOutputStream(mFile);
                Bitmap imageBitmap = ((BitmapDrawable)mImageView.getDrawable()).getBitmap();
                //imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);

                /**
                 * PATCH TEMPORAL
                 */

                /*File mediaDirectory = new File(pathAviaryDir);
                File[] image = mediaDirectory.listFiles();
                InputStream is = new FileInputStream(image[0]);
                OutputStream out = new FileOutputStream(mFile);

                FileManager fm = new FileManager();
                fm.copyFile(is, out);

                fm.clearDirectory(mediaDirectory);*/
                /**
                 * FIN PATCH TEMPORAL
                 */

                mFiles[selectedPicPos] = mFile;
                mappingPicture.setLocalUri(mFile.getParent());

            } catch (Exception e) {
                e.printStackTrace();
            }

            updateDataset();
        }
    }

    private void updateDataset() {
        mFilesAux[0] = mFiles[0];
        recyclerView.setAdapter(new ExpandedViewAdapter(this, mImgs, initialCh, finalCh, mFilesAux, mappingFinished));
    }

    public File getOutputFile(ExpandedTunnel et) {
        File mediaDirectory = getMediaDirectory();
        File mFile = new File(mediaDirectory.getPath() + File.separator + et.getIdMapping().toLowerCase() + File.separator + et.getSideName().trim().toLowerCase() + ".png");

        if (!mFile.getParentFile().exists()) {
            try{
                mFile.getParentFile().mkdir();
            }
            catch(SecurityException se){
                se.printStackTrace();
            }
        }
        return mFile;
    }

    public File getExportFile(ExpandedTunnel et) {
        File mediaDirectory = getMediaDirectory();
        File mFile = new File(mediaDirectory.getPath() + File.separator + et.getIdMapping().toLowerCase() + File.separator + et.getSideName().trim().toLowerCase() + "-export.png");

        if (!mFile.getParentFile().exists()) {
            try{
                mFile.getParentFile().mkdir();
            }
            catch(SecurityException se){
                se.printStackTrace();
            }
        }
        return mFile;
    }

    public File getMediaDirectory() {
        File mediaDirectory = new File(getActivity().getExternalFilesDir(null).getPath() + File.separator + "media");
        if (!mediaDirectory.exists()) {
            try{
                mediaDirectory.mkdir();
            }
            catch(SecurityException se){
                se.printStackTrace();
            }
        }

        return mediaDirectory;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public File[] getmFiles() {
        return mFiles;
    }

    public void setmFiles(File[] mFiles) {
        this.mFiles = mFiles;
    }

    public ExpandedTunnel getMappingPicture() {
        return mappingPicture;
    }

    public void setMappingPicture(ExpandedTunnel mappingPicture) {
        this.mappingPicture = mappingPicture;
    }

    public void setTunnel(Tunnel tunnel) {
        this.tunnel = tunnel;
    }

    public void setmMappingDataset(List<Mapping> mMappingDataset) {
        this.mMappingDataset = mMappingDataset;
    }

    public void setSelectedMapping(Mapping selectedMapping) {
        this.selectedMapping = selectedMapping;
    }

    /**
     * Maneja el resultado de una petición de permisos. Específicamente maneja cuando se piden
     * los permisos para WRITE_EXTERNAL_STORAGE en {@link ExpandedViewAdapter#onBindViewHolder(ExpandedViewAdapter.ViewHolder, int)}, código 1.
     *
     * @param requestCode el código de la petición de permisos
     * @param permissions los permisos
     * @param grantResults los resultados de cada permiso, si fue concedido o no
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0) {
                    this.writePermissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                }
                break;
        }
    }

    /**
     * Permite saber si los permisos de escritura han sido concedidos a la app.
     *
     * @return <code>true</code> si el permiso fue concedido, o <code>false</code> en caso contrario.
     */
    public boolean isWritePermissionGranted() {
        return writePermissionGranted;
    }

    static class BitmapFileUtils {

        public static Bitmap combineImages(Bitmap c, Bitmap s, boolean vertically ) {
            Bitmap cs = null;
            int width, height = 0;

            if(c.getWidth() > s.getWidth()) {
                width = c.getWidth();
                height = c.getHeight() + s.getHeight();
            } else {
                width = s.getWidth();
                height = c.getHeight() + s.getHeight();
            }

            cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas comboImage = new Canvas(cs);

            if (vertically){
                comboImage.drawBitmap(c, 0f, 0f, null);
                comboImage.drawBitmap(s, 0f, c.getHeight(), null);
            } else {
                comboImage.drawBitmap(c, 0f, 0f, null);
                comboImage.drawBitmap(s, c.getWidth(), 0f, null);
            }
            return cs;
        }


        public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
            Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
            Canvas canvas = new Canvas(bmOverlay);
            canvas.drawBitmap(bmp1, new Matrix(), null);
            canvas.drawBitmap(bmp2, 0, 0, null);
            return bmOverlay;
        }

        public static int calculateInSampleSize(
                BitmapFactory.Options options, int reqWidth, int reqHeight) {
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {
                final int halfHeight = height / 2;
                final int halfWidth = width / 2;

                while ((halfHeight / inSampleSize) > reqHeight
                        && (halfWidth / inSampleSize) > reqWidth) {
                    inSampleSize *= 2;
                }
            }
            return inSampleSize;
        }

        public static Bitmap decodeSampledBitmapFromFile(File file, int reqWidth, int reqHeight) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file.getPath(), options);
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(file.getPath(), options);
        }

        public static Bitmap decodeSampledBitmapFromFilePath(String filePath, int reqWidth, int reqHeight) {
            File file = new File(filePath);
            if (file.exists()) {
                return decodeSampledBitmapFromFile(file, reqWidth, reqHeight);
            }
            return null;
        }
    }
}
