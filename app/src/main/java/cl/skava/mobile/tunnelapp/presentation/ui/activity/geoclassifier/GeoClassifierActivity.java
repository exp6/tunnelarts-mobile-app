package cl.skava.mobile.tunnelapp.presentation.ui.activity.geoclassifier;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.presentation.ui.activity.geoclassifier.mapping.MappingInputActivity;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.GeoClassifierFragment;

/**
 * Created by Jose Ignacio Vera on 28-06-2016.
 */
public class GeoClassifierActivity extends AppCompatActivity
        implements GeoClassifierFragment.OnMappingItemListListener {

    private static final String TAG = "GeoClassifierActivity";

    private CharSequence mTitle;
    private Toolbar toolbar;

    private DrawerLayout mDrawer;

    //User Selection Context
    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Module selectedModule;
    private List<FormationUnit> formationUnits;
    private List<RockQuality> rockQualities;

    private ProgressDialog uiProgressDialog;

    private User user;
    private Client mClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            user = (User) getIntent().getSerializableExtra("user");
            mClient = (Client) getIntent().getSerializableExtra("client");
            initComponents();
            initMainFragment();
        }
    }

    private void initComponents() {
        setContentView(R.layout.activity_geoclassifier);

        NavigationView navigationView = (NavigationView)findViewById(R.id.navigation);
        View header = navigationView.getHeaderView(0);
        TextView user_email = (TextView) header.findViewById(R.id.user_email);
        user_email.setText(user.getEmail());
        TextView user_complete_name = (TextView) header.findViewById(R.id.user_complete_name);
        user_complete_name.setText(user.getFirstName() + " " + user.getLastName());
        TextView user_role = (TextView) header.findViewById(R.id.user_role);
        user_role.setText(getResources().getString(R.string.home_label_geologist));

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        setTitle(getResources().getString(R.string.app_name) + " - " + mClient.getName().trim() + " - " + getResources().getString(R.string.characterization_label_module_name));

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                mDrawer.closeDrawers();

                switch (menuItem.getItemId()) {
                    case R.id.home:
                        finish();
                        return true;
                    case R.id.logout:
                        Intent returnIntent1 = new Intent();
                        returnIntent1.putExtra("result","login");
                        setResult(Activity.RESULT_OK,returnIntent1);
                        finish();
                        return true;
                    default:
                        return true;

                }
            }
        });
    }

    private void initMainFragment() {
        uiProgressDialog = new ProgressDialog(GeoClassifierActivity.this, R.style.AppTheme_Dark_Dialog);
        uiProgressDialog.setCancelable(false);
        uiProgressDialog.setMessage(getResources().getString(R.string.system_label_preparing_user_interface));
        uiProgressDialog.show();

        selectedProject = (Project) getIntent().getSerializableExtra("project");
        selectedTunnel = (Tunnel) getIntent().getSerializableExtra("tunnel");
        selectedFace = (Face) getIntent().getSerializableExtra("face");
        selectedModule = (Module) getIntent().getSerializableExtra("module");
        formationUnits = (List<FormationUnit>) getIntent().getSerializableExtra("formationUnits");
        rockQualities = (List<RockQuality>) getIntent().getSerializableExtra("rockQualities");

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        GeoClassifierFragment fragment = new GeoClassifierFragment();
        fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, user);
        transaction.replace(R.id.content_frame_gc, fragment);
        transaction.commit();
    }

    private void reloadMainFragment() {
        uiProgressDialog = new ProgressDialog(GeoClassifierActivity.this, R.style.AppTheme_Dark_Dialog);
        uiProgressDialog.setCancelable(false);
        uiProgressDialog.setMessage(getResources().getString(R.string.system_label_preparing_user_interface));
        uiProgressDialog.show();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        GeoClassifierFragment fragment = new GeoClassifierFragment();
        fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, user);
        transaction.replace(R.id.content_frame_gc, fragment);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        final GeoClassifierFragment fragment = (GeoClassifierFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame_gc);
        final Context c = this;
        Handler h;

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_user:
                if (mDrawer.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawer.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawer.openDrawer(Gravity.RIGHT);
                }
                return true;
//            case R.id.save_data:
//                fragment.saveData();
//                return true;
            case R.id.export_data:
                h = new Handler(Looper.getMainLooper());
                h.post(new Runnable() {
                    public void run() {
                        new android.support.v7.app.AlertDialog.Builder(c)
                                .setTitle(getResources().getString(R.string.system_label_archive_data1) + " " + getResources().getString(R.string.characterization_label_module_name) + " " + getResources().getString(R.string.system_label_archive_data2))
                                .setMessage(getResources().getString(R.string.system_label_archive_data_msg1) + " " + getResources().getString(R.string.characterization_label_module_name) + getResources().getString(R.string.system_label_proceed))
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setPositiveButton(getResources().getString(R.string.system_label_yes), new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        fragment.exportData();
                                    }
                                })
                                .setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                })
                                .show();
                    }
                });
                return true;
            case R.id.sync_data:
                h = new Handler(Looper.getMainLooper());
                h.post(new Runnable() {
                    public void run() {
                        new android.support.v7.app.AlertDialog.Builder(c)
                                .setTitle(getResources().getString(R.string.system_label_sync_data1) + " " + getResources().getString(R.string.characterization_label_module_name) + " " + getResources().getString(R.string.system_label_sync_data2))
                                .setMessage(getResources().getString(R.string.system_label_sync_data_msg1) + " " + getResources().getString(R.string.characterization_label_module_name) + getResources().getString(R.string.system_label_proceed))
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setPositiveButton(getResources().getString(R.string.system_label_yes), new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        fragment.syncData();
                                    }
                                })
                                .setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                })
                                .show();
                    }
                });
                return true;
            case R.id.delete_item:
                h = new Handler(Looper.getMainLooper());
                h.post(new Runnable() {
                    public void run() {
                        new android.support.v7.app.AlertDialog.Builder(c)
                                .setTitle(getResources().getString(R.string.system_label_delete_item) + " " + getResources().getString(R.string.characterization_label_mapping))
                                .setMessage(getResources().getString(R.string.system_label_delete_item_msg) + getResources().getString(R.string.system_label_proceed))
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(getResources().getString(R.string.system_label_yes), new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        fragment.deleteSelectedItem();
                                    }
                                })
                                .setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                })
                                .show();
                    }
                });
                return true;
            case R.id.edit_item:
                fragment.editSelectedItem();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_geoclassifier_right, menu);
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void userMappingInputsLoaded() {
        uiProgressDialog.dismiss();
    }

    @Override
    public void initMappingInputActivity(Project project, Tunnel tunnel, Face face, Module module, Mapping mapping, MappingInputModule mappingInputModule, List<MappingInputModule> mappingInputModuleDataset, List<Mapping> mappingDataset, List<Integer> metaPositions) {
        Intent intent = new Intent(this, MappingInputActivity.class);
        intent.putExtra("project", project);
        intent.putExtra("tunnel", tunnel);
        intent.putExtra("face", face);
        intent.putExtra("module", module);
        intent.putExtra("mapping", mapping);
        intent.putExtra("mapping_input_position", mappingInputModule.getCode());
        intent.putExtra("mapping_input_list", (Serializable) mappingInputModuleDataset);
        intent.putExtra("mapping_list", (Serializable) mappingDataset);
        intent.putExtra("user", user);
        intent.putExtra("meta_positions", (Serializable) metaPositions);
        intent.putExtra("client", mClient);
        intent.putExtra("formationUnits", (Serializable) formationUnits);
        intent.putExtra("rockQualities", (Serializable) rockQualities);

        startActivityForResult(intent, 1);
//        startActivity(intent);
    }

    @Override
    public void onSyncFinish() {
        reloadMainFragment();
    }

    @Override
    public void hideDelete() {
        MenuItem item = toolbar.getMenu().findItem(R.id.delete_item);
        if(item != null) item.setVisible(false);

        MenuItem item2 = toolbar.getMenu().findItem(R.id.export_data);
        if(item2 != null) item2.setVisible(false);

    }

    @Override
    public void hideUpload() {
//        MenuItem item = toolbar.getMenu().findItem(R.id.save_data);
//        if(item != null)
//            item.setVisible(false);
    }

    @Override
    public void hideEdit() {
        MenuItem item = toolbar.getMenu().findItem(R.id.edit_item);
        if(item != null)
            item.setVisible(false);
    }

    @Override
    public void showDelete() {
        MenuItem item = toolbar.getMenu().findItem(R.id.delete_item);
        item.setVisible(true);

        MenuItem item2 = toolbar.getMenu().findItem(R.id.export_data);
        item2.setVisible(true);
    }

    @Override
    public void showUpload() {
//        MenuItem item = toolbar.getMenu().findItem(R.id.save_data);
//        item.setVisible(true);
    }

    @Override
    public void showEdit() {
        MenuItem item = toolbar.getMenu().findItem(R.id.edit_item);
        item.setVisible(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            String result = data.getStringExtra("result");

            if(result.equals("login")) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result","login");
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }

            if(result.equals("home")) {
                finish();
            }

            if(result.equals("mappingInputs")) {
                reloadMainFragment();
            }
        }
    }

//    @Override
//    public void onBackPressed() {
//        if (mPager.getCurrentItem() == 0) {
//            // If the user is currently looking at the first step, allow the system to handle the
//            // Back button. This calls finish() on this activity and pops the back stack.
//            super.getActivity().onBackPressed();
//        } else {
//            // Otherwise, select the previous step.
//            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
//        }
//    }
}
