package cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.home;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.presentation.presenters.base.BasePresenter;
import cl.skava.mobile.tunnelapp.presentation.ui.base.BaseView;

/**
 * Created by Jose Ignacio Vera on 16-06-2016.
 */
public interface HomePresenter extends BasePresenter {

    interface View extends BaseView {
        //View methods goes here
        void displayProjectList(List<Project> projects);
        void displayTunnelList(List<Tunnel> tunnels);
        void displayFaceList(List<Face> faces);
        void setClientData(Client client);
        void setFormationUnits(HashMap<Project, List<FormationUnit>> formationUnitsByProject);
        void setRockQualities(HashMap<Project, List<RockQuality>> rockQualities);
    }

    //Presenter methods goes here
    void getTunnelsByProject(Project project);
    void getFacesByTunnel(Tunnel tunnel);
}
