package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rmr;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;

/**
 * Created by Jose Ignacio Vera on 08-07-2016.
 */
public class GroundwaterFragment extends Fragment {

    private static final String TAG = "GroundwaterFragment";

    private List<GroundwaterGroup> groundwaterGroups = new ArrayList<>();

    private OnGroundwaterValueListener mCallback;

    public interface OnGroundwaterValueListener {
        void OnGroundwaterSetValue(Double value, String id);
    }

    private EvaluationMethodSelectionType selectionMap;

    private Boolean mappingFinished;

    private final String lang = Locale.getDefault().getLanguage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initDataset();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_rmr_groundwater, container, false);
        rootView.setTag(TAG);

        RadioGroup group = (RadioGroup) rootView.findViewById(R.id.radio_group1);
        RadioButton button;

        final RadioGroup group2 = (RadioGroup) rootView.findViewById(R.id.radio_group2);

        int i = 0;
        for(GroundwaterGroup groundwaterGroup :  groundwaterGroups) {
            final GroundwaterGroup groundwaterGroup1 = groundwaterGroup;
            button = new RadioButton(getActivity());
            button.setId(i);
//            button.setText(groundwaterGroup.getIndex().trim() + ". " + groundwaterGroup.getDescription());
            button.setText(groundwaterGroup.getDescription() + (groundwaterGroup.getIndex().trim().equalsIgnoreCase("b") ? "\u03C3" : ""));
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    displayGws(group2, groundwaterGroup1);
                }
            });
            button.setChecked(groundwaterGroup.isChecked());
            if(groundwaterGroup.isChecked()) {
                displayGws(group2, groundwaterGroup1);
            }

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }

        return rootView;
    }

    private void displayGws(RadioGroup group, GroundwaterGroup groundwaterGroup) {
        List<Groundwater> groundwaters = groundwaterGroup.getGws();
        final GroundwaterGroup groundwaterGroup1 = groundwaterGroup;
        group.removeAllViews();
        RadioButton button;

        int i = 0;
        for(Groundwater groundwater :  groundwaters) {
            final Groundwater groundwater1 = groundwater;

            button = new RadioButton(getActivity());
            button.setId(i);
//            button.setText(groundwater.getIndex().trim() + " (" + groundwater.getStart() + (groundwater.getEnd() != null ? " - " + groundwater.getEnd() : "") + ")     " + groundwater.getDescription());
            button.setText("(" + groundwater.getStart() + (groundwater.getEnd() != null ? " - " + groundwater.getEnd() : "") + ")     " + groundwater.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mCallback.OnGroundwaterSetValue(groundwater1.getStart(), groundwater1.getId());
                    updateMap(groundwaterGroup1, groundwater1);
                }
            });
            button.setChecked(groundwater1.isChecked());

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }
    }

    private void updateMap(GroundwaterGroup groundwaterGroup, Groundwater groundwater) {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        EvaluationMethodSelection input;
        for(int i = 0; i < groups.size(); i++) {
            groups.get(i).setChecked(false);
            if(groundwaterGroup.getId().equalsIgnoreCase(groups.get(i).getId())) {
                groups.get(i).setChecked(true);
            }

            inputs = groups.get(i).getSubSelections();
            for(int j = 0; j < inputs.size(); j++) {
                input = inputs.get(j);
                input.setChecked(false);
                if(groundwater.getId().equalsIgnoreCase(input.getId())) {
                    input.setChecked(true);
                }
            }

            groups.get(i).setSubSelections(inputs);
        }
        selectionMap.setSelectionGroups(groups);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnGroundwaterValueListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnGroundwaterValueListener");
        }
    }

    public void setSelectionMap(EvaluationMethodSelectionType selectionMap) {
        this.selectionMap = selectionMap;
    }

    public EvaluationMethodSelectionType getSelectionMap() {
        return selectionMap;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    private void initDataset() {

        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        GroundwaterGroup groundwaterGroup;
        List<Groundwater> groundwaters;
        Groundwater groundwater;

        for(EvaluationMethodSelectionGroup e : groups) {
            groundwaterGroup = new GroundwaterGroup();
            groundwaterGroup.setId(e.getId());
            groundwaterGroup.setIndex(e.getIndex());
            groundwaterGroup.setDescription((lang.equals("es") ? e.getDescriptionEs() : e.getDescription()));
            groundwaterGroup.setChecked(e.getChecked());
            inputs = e.getSubSelections();

            groundwaters = new ArrayList<>();

            for (EvaluationMethodSelection em : inputs) {
                groundwater = new Groundwater();
                groundwater.setId(em.getId());
                groundwater.setIndex(em.getIndex());
                groundwater.setStart(em.getStart());
                groundwater.setEnd(em.getEnd().doubleValue() == 0.0 ? null : em.getEnd());
                groundwater.setDescription((lang.equals("es") ? em.getDescriptionEs() : em.getDescription()));
                groundwater.setChecked(em.getChecked());
                groundwaters.add(groundwater);
            }

            groundwaterGroup.setGws(groundwaters);
            groundwaterGroups.add(groundwaterGroup);
        }
    }

    class Groundwater {
        private String id;
        private String index;
        private Double value;
        private Double start;
        private Double end;
        private String description;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public Double getStart() {
            return start;
        }

        public void setStart(Double start) {
            this.start = start;
        }

        public Double getEnd() {
            return end;
        }

        public void setEnd(Double end) {
            this.end = end;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }

    class GroundwaterGroup {
        private String id;
        private String index;
        private String description;
        private List<Groundwater> gws;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<Groundwater> getGws() {
            return gws;
        }

        public void setGws(List<Groundwater> gws) {
            this.gws = gws;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }
}
