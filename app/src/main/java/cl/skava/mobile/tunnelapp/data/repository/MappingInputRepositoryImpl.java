package cl.skava.mobile.tunnelapp.data.repository;

        import android.content.Context;
        import android.content.ContextWrapper;

        import java.io.File;
        import java.math.BigDecimal;
        import java.math.RoundingMode;
        import java.util.ArrayList;
        import java.util.List;
        import java.util.Locale;
        import java.util.UUID;

        import cl.skava.mobile.tunnelapp.R;
        import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.blobs.BlobStorageService;
        import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
        import cl.skava.mobile.tunnelapp.domain.model.MappingInput;
        import cl.skava.mobile.tunnelapp.domain.model.ProjectMappingInput;
        import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.AdditionalDescription;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.AdditionalInformation;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.BaseData;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.Discontinuity;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelection;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.ExpandedTunnel;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.FailureZone;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.Gsi;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.GsiValue;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.Lithology;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInstance;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.Particularities;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.Picture;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.RecommendationRockbolt;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.RecommendationShotcrete;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.StereonetPicture;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueDiscontinuity;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInput;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputGroup;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputGroupType;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputSelection;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputSelectionDiscontinuity;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.Rmr;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMass;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMassHazard;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.SpecialFeatures;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInput;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputGroup;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputGroupType;
        import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputSelection;
        import cl.skava.mobile.tunnelapp.domain.repository.MappingInputRepository;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public class MappingInputRepositoryImpl implements MappingInputRepository {

    private List<MappingInputModule> inputs = new ArrayList<>();
    private CloudStoreService cloudStoreService;
    private BlobStorageService blobStorageService;
    private Mapping mMapping;
    private Context context;
    private List<ProjectMappingInput> projectMappingInputs;
    private List<MappingInput> mappingInputs;

    public MappingInputRepositoryImpl(Context activity, Mapping mapping) {
        cloudStoreService = new CloudStoreService(activity);
        blobStorageService = new BlobStorageService(activity.getExternalFilesDir(null));
        mMapping = mapping;
        this.context = activity;
    }

    public MappingInputRepositoryImpl(Context activity, Mapping mapping, List<MappingInputModule> inputs) {
        cloudStoreService = new CloudStoreService(activity);
        blobStorageService = new BlobStorageService(activity.getExternalFilesDir(null));
        mMapping = mapping;
        this.context = activity;
        this.inputs = inputs;
    }

    @Override
    public List<MappingInputModule> getAll() {
        return inputs;
    }

    @Override
    public List<MappingInputModule> getList(String mappingId) {
        Mapping mapping = cloudStoreService.getMappingById(mappingId);

        if(mapping != null) {
            initDatasetPerItem(mappingId);
            return inputs;
        }
        else {
            return new ArrayList<>();
        }
    }

    private MappingInstance initMappingInput(String label, MappingInstance mappingInstance) {
        List<EvaluationMethodSelectionType> evaluationMethodSelectionTypes;

        switch(label) {
            case "RMO":
                BaseData bd = (BaseData) cloudStoreService.getMappingInput(mMapping.getId(), BaseData.class);
                if(bd == null)
                    bd = new BaseData("", "", "", "", "", "", "", mMapping.getId(), false);
                mappingInstance.setMappingBaseData(bd);

                RockMass rm = (RockMass) cloudStoreService.getMappingInput(mMapping.getId(), RockMass.class);
                if(rm == null)
                    rm = new RockMass("", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, new Float(0), 0, new Float(0), mMapping.getId(), false, "");
                mappingInstance.setMappingRockMass(rm);
                break;
            case "LTH":
                List<Lithology> lithologies = (List<Lithology>) cloudStoreService.getMappingInputs(mMapping.getId(), Lithology.class);
                if(lithologies.isEmpty()) {
                    Lithology l;
                    for(int i = 0; i < 3 ; i++) {
                        l = new Lithology("", i, new Float(0), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, new Float(0), new Float(0), new Float(0), mMapping.getId(), false, "", 0);
                        lithologies.add(l);
                    }
                }
                mappingInstance.setMappingLithologies(lithologies);
                break;
            case "DISC":
                List<Discontinuity> discontinuities = (List<Discontinuity>) cloudStoreService.getMappingInputs(mMapping.getId(), Discontinuity.class);
                mappingInstance.setMappingDiscontinuities(discontinuities);

                StereonetPicture stereonetPicture = (StereonetPicture) cloudStoreService.getMappingInput(mMapping.getId(), StereonetPicture.class);
//                File[] mappingStereonetFiles = new File[1];

                String externalFilePath1 = context.getExternalFilesDir(null).getPath() + File.separator + "media";
                File mediaDir1 = new File(externalFilePath1);
                if (!mediaDir1.exists()) {
                    try{
                        mediaDir1.mkdir();
                    }
                    catch(SecurityException se){
                        se.printStackTrace();
                    }
                }

                if(stereonetPicture == null) {
                    stereonetPicture = new StereonetPicture();
                    stereonetPicture.setId("");
                    stereonetPicture.setIdMapping(mMapping.getId());
                    stereonetPicture.setSideName("stereonet");
                }
                stereonetPicture.setLocalUri(externalFilePath1);
//                File stereonetPictureFile = new File(stereonetPicture.getLocalUri() + File.separator + stereonetPicture.getIdMapping().toLowerCase() + File.separator +  stereonetPicture.getSideName().trim().toLowerCase() + ".jpg");
//                mappingStereonetFiles[0] = stereonetPicture.getLocalUri() == null ? null : stereonetPictureFile;
                mappingInstance.setMappingStereonet(stereonetPicture);
//                mappingInstance.setMappingExpandedTunnelPictures(mappingExpandedTunnel);
                break;
            case "AINFO":
                AdditionalInformation ai = (AdditionalInformation) cloudStoreService.getMappingInput(mMapping.getId(), AdditionalInformation.class);
                SpecialFeatures sf;
                RockMassHazard rmh;
                if(ai == null) {
                    ai = new AdditionalInformation("", mMapping.getId(), false);
                    sf = new SpecialFeatures("", false, false, false, false, false, false, "", false, mMapping.getId(), false);
                    rmh = new RockMassHazard("", false, false, false, false, "", false, mMapping.getId(), false);
                }
                else {
                    sf = (SpecialFeatures) cloudStoreService.getSubMappingInput(ai.getId(), SpecialFeatures.class);
                    rmh = (RockMassHazard) cloudStoreService.getSubMappingInput(ai.getId(), RockMassHazard.class);
                }
                mappingInstance.setMappingAdditionalInformation(ai);
                mappingInstance.setMappingSpecialFeatures(sf);
                mappingInstance.setMappingRockMassHazard(rmh);

                FailureZone f = (FailureZone) cloudStoreService.getMappingInput(mMapping.getId(), FailureZone.class);
                if(f == null)
                    f = new FailureZone("", 0, new Float(0), new Float(0), new Float(0), new Float(0), 0, 0, 0, 0, 0, 0, 0, 0, mMapping.getId(), false, new Float(0), false);
                mappingInstance.setMappingFailureZone(f);

                Particularities p = (Particularities) cloudStoreService.getMappingInput(mMapping.getId(), Particularities.class);
                if(p == null)
                    p = new Particularities("", "", "", "", "", 0, mMapping.getId(), false, "", "", false, false, 0);
                mappingInstance.setMappingParticularities(p);
                break;
            case "QMET":
                QValue qv = (QValue) cloudStoreService.getMappingInput(mMapping.getId(), QValue.class);
                if(qv == null)
                    qv = new QValue("", new Float(0.0), new Float(1.0), new Float(0.0), new Float(1.0), new Float(0.0), new Float(1.0), 0, mMapping.getId(), false, new Float(0.0), new Float(0.0), new Float(0.0), 1, 0);
                mappingInstance.setMappingQValue(qv);

                QValueInputSelection qvis = (QValueInputSelection) cloudStoreService.getMappingInput(mMapping.getId(), QValueInputSelection.class);
                if(qvis == null) {
                    qvis = new QValueInputSelection();
                    qvis.setId("");
                    qvis.setIdMapping(mMapping.getId());
                }
                mappingInstance.setqValueInputSelection(qvis);

                evaluationMethodSelectionTypes = getQValueSelectionMap(qvis);
                mappingInstance.setqValueEvaluationMethodSelectionTypes(evaluationMethodSelectionTypes);

                List<QValueDiscontinuity> qValueDiscontinuities = new ArrayList<>();
                if(!qv.getId().trim().isEmpty()) qValueDiscontinuities = cloudStoreService.getQValueDiscontinuities(qv.getId());
                if(qValueDiscontinuities.isEmpty()) {
                    QValueDiscontinuity qValueDiscontinuity = new QValueDiscontinuity("", "", 0, new Float(0), new Float(1), false, UUID.fromString("00000000-0000-0000-0000-000000000000").toString(), UUID.fromString("00000000-0000-0000-0000-000000000000").toString());
                    qValueDiscontinuities.add(qValueDiscontinuity);
                }

                mappingInstance.setqValueDiscontinuities(qValueDiscontinuities);

                List<EvaluationMethodSelectionType> jrSelectionMap = getQJrJaSelectionMap(qValueDiscontinuities, 1);
                List<EvaluationMethodSelectionType> jaSelectionMap = getQJrJaSelectionMap(qValueDiscontinuities, 2);

                mappingInstance.setJrSelectionMap(jrSelectionMap);
                mappingInstance.setJaSelectionMap(jaSelectionMap);

                QValueDiscontinuity qValueInputSelectionDisc = new QValueDiscontinuity("", "", 0, new Float(0), new Float(1), false, UUID.fromString("00000000-0000-0000-0000-000000000000").toString(), UUID.fromString("00000000-0000-0000-0000-000000000000").toString());
                List<QValueDiscontinuity> qValueInputSelectionDiscontinuityAux = new ArrayList<>();
                qValueInputSelectionDiscontinuityAux.add(qValueInputSelectionDisc);
                List<EvaluationMethodSelectionType> emptyJrSelectionMap = getQJrJaSelectionMap(qValueInputSelectionDiscontinuityAux, 1);
                List<EvaluationMethodSelectionType> emptyJaSelectionMap = getQJrJaSelectionMap(qValueInputSelectionDiscontinuityAux, 2);

                mappingInstance.setEmptyJrMap(emptyJrSelectionMap.get(0));
                mappingInstance.setEmptyJaMap(emptyJaSelectionMap.get(0));
                break;
            case "RMR":
                Rmr rmr = (Rmr) cloudStoreService.getMappingInput(mMapping.getId(), Rmr.class);
                if(rmr == null)
                    rmr = new Rmr("", new Float(0.0), new Float(0.0), new Float(0.0), new Float(0.0), new Float(0.0), new Float(0.0), new Float(0.0), new Float(0.0), new Float(0.0), new Float(0.0), 0, mMapping.getId(), false, new Float(0.0), false);
                mappingInstance.setMappingRmr(rmr);

                RmrInputSelection rmris = (RmrInputSelection) cloudStoreService.getMappingInput(mMapping.getId(), RmrInputSelection.class);
                if(rmris == null) {
                    rmris = new RmrInputSelection();
                    rmris.setId("");
                    rmris.setIdMapping(mMapping.getId());
                }
                mappingInstance.setRmrInputSelection(rmris);

                evaluationMethodSelectionTypes = getRmrSelectionMap(rmris);
                mappingInstance.setRmrEvaluationMethodSelectionTypes(evaluationMethodSelectionTypes);
                break;
            case "GSI":
                GsiValue gsiValue = (GsiValue) cloudStoreService.getMappingInput(mMapping.getId(), GsiValue.class);
                if(gsiValue == null)
                    gsiValue = new GsiValue("", mMapping.getId(), 0, 0, new Float(0.0), new Float(0.0), new Float(0.0), false);
                mappingInstance.setMappingGsiValue(gsiValue);
                break;
            case "PIC":
                List<Picture> mappingPicturesData = (List<Picture>) cloudStoreService.getMappingInputPictures(mMapping.getId(), Picture.class);
                File[] mappingPictures = new File[4];
                File[] mappingOriginalPictures = new File[4];

                if(mappingPicturesData.isEmpty()) {
                    mappingPicturesData = new ArrayList<>();
                    for(int i = 0; i < 4; i++) {
                        Picture picture = new Picture();
                        picture.setId("");
                        picture.setIdMapping(mMapping.getId());
                        picture.setCode(i);

                        switch(i) {
                            case 1:
                                picture.setSideName("left-wall");
                                break;
                            case 2:
                                picture.setSideName("right-wall");
                                break;
                            case 3:
                                picture.setSideName("roof");
                                break;
                            default:
                                picture.setSideName("face");
                                break;
                        }
                        mappingPicturesData.add(picture);
                    }
                }
                else {
                    for(int i = 0; i < mappingPicturesData.size(); i++) {
                        Picture picture = mappingPicturesData.get(i);
                        File newFile = new File(picture.getLocalUri() + File.separator + picture.getSideName().trim().toLowerCase() + ".jpg");
                        File newFileOriginal = new File(picture.getLocalUri() + File.separator + picture.getSideName().trim().toLowerCase() + "-original.jpg");
                        mappingPictures[i] = picture.getLocalUri() == null ? null : newFile;
                        mappingOriginalPictures[i] = picture.getLocalUri() == null ? null : newFileOriginal;
                    }
                }

                mappingInstance.setMappingPicturesData(mappingPicturesData);
                mappingInstance.setMappingPictures(mappingPictures);
                mappingInstance.setMappingOriginalPictures(mappingOriginalPictures);
                break;
            case "EXPT":
                ExpandedTunnel et = (ExpandedTunnel) cloudStoreService.getMappingInput(mMapping.getId(), ExpandedTunnel.class);
                File[] mappingExpandedTunnel = new File[1];

                String externalFilePath = context.getExternalFilesDir(null).getPath() + File.separator + "media";
                File mediaDir = new File(externalFilePath);
                if (!mediaDir.exists()) {
                    try{
                        mediaDir.mkdir();
                    }
                    catch(SecurityException se){
                        se.printStackTrace();
                    }
                }


                if(et == null) {
                    et = new ExpandedTunnel();
                    et.setId("");
                    et.setIdMapping(mMapping.getId());
                    et.setCode(0);
                    et.setSideName("expanded");
                }
                et.setLocalUri(externalFilePath);
                File expandedPicture = new File(et.getLocalUri() + File.separator + et.getIdMapping().toLowerCase() + File.separator +  et.getSideName().trim().toLowerCase() + ".png");
                mappingExpandedTunnel[0] = et.getLocalUri() == null ? null : expandedPicture;
                mappingInstance.setMappingExpandedTunnel(et);
                mappingInstance.setMappingExpandedTunnelPictures(mappingExpandedTunnel);
                break;
            case "GCOMM":
                AdditionalDescription ad = (AdditionalDescription) cloudStoreService.getMappingInput(mMapping.getId(), AdditionalDescription.class);
                if(ad == null)
                    ad = new AdditionalDescription("", 0, 0, 0, 0, 0, "", 0, "", mMapping.getId(), false);
                mappingInstance.setMappingAdditionalDescription(ad);
                break;
            case "SUMM":
                List<RecommendationRockbolt> rockbolts = (List<RecommendationRockbolt>) cloudStoreService.getMappingInputs(mMapping.getId(), RecommendationRockbolt.class);
                List<RecommendationRockbolt> deletedRockbolts = new ArrayList<>();
                if(rockbolts.isEmpty()) {
                    RecommendationRockbolt rockbolt = new RecommendationRockbolt("", 0, 0, 0, new Float(0), new Float(0), 0, new Float(0), new Float(0), "", "", new Float(0), new Float(0), mMapping.getId(), false);
                    rockbolts.add(rockbolt);
                }

                mappingInstance.setRockbolts(rockbolts);
                mappingInstance.setDeletedRockbolts(deletedRockbolts);

                List<RecommendationShotcrete> shotcretes = (List<RecommendationShotcrete>) cloudStoreService.getMappingInputs(mMapping.getId(), RecommendationShotcrete.class);
                List<RecommendationShotcrete> deletedShotcretes = new ArrayList<>();
                if(shotcretes.isEmpty()) {
                    RecommendationShotcrete shotcrete = new RecommendationShotcrete("", 0, new Float(0), 0, new Float(0), 0, "", "", mMapping.getId(), false);
                    shotcretes.add(shotcrete);
                }

                mappingInstance.setShotcretes(shotcretes);
                mappingInstance.setDeletedShotcretes(deletedShotcretes);
                break;
            default:
                break;
        }

        return mappingInstance;
    }

    @Override
    public MappingInstance getMappingInstance() {
        MappingInstance mappingInstance = new MappingInstance();
        mappingInstance.setMapping(mMapping);

        for(MappingInputModule mim : inputs) {
            if(mim.getEnabled()) mappingInstance = initMappingInput(mim.getLabel(), mappingInstance);

            if(mim.getSubInputs() != null) {
                for(MappingInputModule mim2 : mim.getSubInputs()) {
                    if(mim2.getEnabled()) mappingInstance = initMappingInput(mim2.getLabel(), mappingInstance);
                }
            }
        }

        return mappingInstance;
    }

    private List<EvaluationMethodSelectionType> getQJrJaSelectionMap(List<QValueDiscontinuity> qValueInputSelectionDiscontinuities, int code) {
        List<QValueInputGroupType> qValueInputGroupTypes  = (List<QValueInputGroupType>) cloudStoreService.getMappingInputTypes(QValueInputGroupType.class);
        QValueInputGroupType qValueInputGroupType = null;
        for(QValueInputGroupType qValueInputGroupType1 : qValueInputGroupTypes) {
            if(qValueInputGroupType1.getCode().intValue() == code) {
                qValueInputGroupType = qValueInputGroupType1;
                break;
            }
        }

        List<QValueInputGroup> qValueInputGroups;
        List<QValueInput> qValueInputs;

        List<EvaluationMethodSelectionType> evaluationMethodSelectionTypes = new ArrayList<>();
        List<EvaluationMethodSelectionGroup> emsgs;
        List<EvaluationMethodSelection> emss;

        EvaluationMethodSelectionType qemst;
        EvaluationMethodSelectionGroup emsg;
        EvaluationMethodSelection ems;

        for(QValueDiscontinuity qValueInputSelectionDisc : qValueInputSelectionDiscontinuities) {
            qemst = new EvaluationMethodSelectionType();
            qemst.setId(qValueInputGroupType.getId());
            qemst.setCode(qValueInputGroupType.getCode());
            qemst.setDescription(qValueInputGroupType.getDescription());
            qemst.setMethodTypeCode(0);

            qValueInputGroups  = (List<QValueInputGroup>) cloudStoreService.getMappingInputTypes(QValueInputGroup.class, qemst.getId(), "IdQValueInputGroupType");
            emsgs = new ArrayList<>();

            for(QValueInputGroup qvig : qValueInputGroups) {
                emsg = new EvaluationMethodSelectionGroup();
                emsg.setId(qvig.getId());
                emsg.setIndex(qvig.getCodeIndex());
                emsg.setDescription(qvig.getDescription());
                emsg.setDescriptionEs(qvig.getDescriptionEs());
                emsg.setChecked(false);

                qValueInputs  = (List<QValueInput>) cloudStoreService.getMappingInputTypes(QValueInput.class, qvig.getId(), "IdQValueInputGroup");
                emss = new ArrayList<>();

                for(QValueInput qvi : qValueInputs) {
                    ems = new EvaluationMethodSelection();
                    ems.setId(qvi.getId());
                    ems.setIndex(qvi.getCodeIndex());
                    ems.setStart(round(qvi.getStartValue().doubleValue(), 2));
                    ems.setEnd(round(qvi.getEndValue().doubleValue(), 2));
                    ems.setDescription(qvi.getDescription());
                    ems.setDescriptionEs(qvi.getDescriptionEs());

                    String selection = "";

                    if(code == 1) selection = qValueInputSelectionDisc.getJrSelection();
                    if(code == 2) selection = qValueInputSelectionDisc.getJaSelection();

                    ems.setChecked(selectionMatch(ems.getId(), selection));

                    if(ems.getChecked()) {
                        emsg.setChecked(true);
                    }

                    emss.add(ems);
                }

                emsg.setSubSelections(emss);
                emsgs.add(emsg);
            }

            qemst.setSelectionGroups(emsgs);
            evaluationMethodSelectionTypes.add(qemst);
        }

        return evaluationMethodSelectionTypes;
    }

    private List<EvaluationMethodSelectionType> getRmrSelectionMap(RmrInputSelection rmris) {
        List<RmrInputGroupType> rmrInputGroupTypes  = (List<RmrInputGroupType>) cloudStoreService.getMappingInputTypes(RmrInputGroupType.class);
        List<RmrInputGroup> rmrInputGroups;
        List<RmrInput> rmrInputs;

        List<EvaluationMethodSelectionType> evaluationMethodSelectionTypes = new ArrayList<>();
        List<EvaluationMethodSelectionGroup> emsgs;
        List<EvaluationMethodSelection> emss;

        EvaluationMethodSelectionType rmremst;
        EvaluationMethodSelectionGroup emsg;
        EvaluationMethodSelection ems;

        for(RmrInputGroupType rmrigt : rmrInputGroupTypes) {
            rmremst = new EvaluationMethodSelectionType();
            rmremst.setId(rmrigt.getId());
            rmremst.setCode(rmrigt.getCode());
            rmremst.setDescription(rmrigt.getDescription());
            rmremst.setMethodTypeCode(0);

            rmrInputGroups  = (List<RmrInputGroup>) cloudStoreService.getMappingInputTypes(RmrInputGroup.class, rmremst.getId(), "IdRmrInputGroupType");
            emsgs = new ArrayList<>();

            for(RmrInputGroup rmrig : rmrInputGroups) {
                emsg = new EvaluationMethodSelectionGroup();
                emsg.setId(rmrig.getId());
                emsg.setIndex(rmrig.getCodeIndex());
                emsg.setDescription(rmrig.getDescription());
                emsg.setDescriptionEs(rmrig.getDescriptionEs());
                emsg.setChecked(false);

                rmrInputs  = (List<RmrInput>) cloudStoreService.getMappingInputTypes(RmrInput.class, rmrig.getId(), "IdRmrInputGroup");
                emss = new ArrayList<>();

                for(RmrInput rmri : rmrInputs) {
                    ems = new EvaluationMethodSelection();
                    ems.setId(rmri.getId());
                    ems.setIndex(rmri.getCodeIndex());
                    ems.setStart(round(rmri.getStartValue().doubleValue(), 2));
                    ems.setEnd(round(rmri.getEndValue().doubleValue(), 2));
                    ems.setDescription(rmri.getDescription());
                    ems.setDescriptionEs(rmri.getDescriptionEs());

                    String selection = getRmrEvalMethodSelection(rmremst.getCode(), rmris);
                    ems.setChecked(selectionMatch(ems.getId(), selection));

                    if(ems.getChecked()) {
                        emsg.setChecked(true);
                    }

                    emss.add(ems);
                }

                emsg.setSubSelections(emss);
                emsgs.add(emsg);
            }

            rmremst.setSelectionGroups(emsgs);
            evaluationMethodSelectionTypes.add(rmremst);
        }

        return evaluationMethodSelectionTypes;
    }

    private List<EvaluationMethodSelectionType> getQValueSelectionMap(QValueInputSelection qvis) {
        List<QValueInputGroupType> qValueInputGroupTypes  = (List<QValueInputGroupType>) cloudStoreService.getMappingInputTypes(QValueInputGroupType.class);
        List<QValueInputGroup> qValueInputGroups;
        List<QValueInput> qValueInputs;

        List<EvaluationMethodSelectionType> evaluationMethodSelectionTypes = new ArrayList<>();
        List<EvaluationMethodSelectionGroup> emsgs;
        List<EvaluationMethodSelection> emss;

        EvaluationMethodSelectionType qemst;
        EvaluationMethodSelectionGroup emsg;
        EvaluationMethodSelection ems;

        for(QValueInputGroupType qvigt : qValueInputGroupTypes) {
            qemst = new EvaluationMethodSelectionType();
            qemst.setId(qvigt.getId());
            qemst.setCode(qvigt.getCode());
            qemst.setDescription(qvigt.getDescription());
            qemst.setMethodTypeCode(0);

            qValueInputGroups  = (List<QValueInputGroup>) cloudStoreService.getMappingInputTypes(QValueInputGroup.class, qemst.getId(), "IdQValueInputGroupType");
            emsgs = new ArrayList<>();

            for(QValueInputGroup qvig : qValueInputGroups) {
                emsg = new EvaluationMethodSelectionGroup();
                emsg.setId(qvig.getId());
                emsg.setIndex(qvig.getCodeIndex());
                emsg.setDescription(qvig.getDescription());
                emsg.setDescriptionEs(qvig.getDescriptionEs());
                emsg.setChecked(false);

                qValueInputs  = (List<QValueInput>) cloudStoreService.getMappingInputTypes(QValueInput.class, qvig.getId(), "IdQValueInputGroup");
                emss = new ArrayList<>();

                for(QValueInput qvi : qValueInputs) {
                    ems = new EvaluationMethodSelection();
                    ems.setId(qvi.getId());
                    ems.setIndex(qvi.getCodeIndex());
                    ems.setStart(round(qvi.getStartValue().doubleValue(), 2));
                    ems.setEnd(round(qvi.getEndValue().doubleValue(), 2));
                    ems.setDescription(qvi.getDescription());
                    ems.setDescriptionEs(qvi.getDescriptionEs());

                    String selection = getQValueEvalMethodSelection(qemst.getCode(), qvis);
                    ems.setChecked(selectionMatch(ems.getId(), selection));

                    if(ems.getChecked()) {
                        emsg.setChecked(true);
                    }

                    emss.add(ems);
                }

                emsg.setSubSelections(emss);
                emsgs.add(emsg);
            }

            qemst.setSelectionGroups(emsgs);
            evaluationMethodSelectionTypes.add(qemst);
        }

        return evaluationMethodSelectionTypes;
    }

    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private String getQValueEvalMethodSelection(Integer code, QValueInputSelection qvis) {
        switch(code) {
            case 1:
                return qvis.getJr();
            case 2:
                return qvis.getJa();
            case 3:
                return qvis.getJw();
            case 4:
                return qvis.getSrf();
            default:
                return qvis.getJn();
        }
    }

    private String getRmrEvalMethodSelection(Integer code, RmrInputSelection rmris) {
        switch(code) {
            case 1:
                return rmris.getRqd();
            case 2:
                return rmris.getSpacing();
            case 3:
                return rmris.getPersistence();
            case 4:
                return rmris.getOpening();
            case 5:
                return rmris.getRoughness();
            case 6:
                return rmris.getInfilling();
            case 7:
                return rmris.getWeathering();
            case 8:
                return rmris.getGroundwater();
            case 9:
                return rmris.getOrientationDD();
            case 10:
                return rmris.getConditionDD();
            default:
                return rmris.getStrength();
        }
    }

    private boolean selectionMatch(String id, String selection) {
        return id.equalsIgnoreCase(selection);
    }

    private boolean saveMappingInput(String label, MappingInstance mappingInstance) {
        boolean dataSaved = true;
        Boolean dataListSaved;

        switch(label) {
            case "RMO":
                BaseData bd = mappingInstance.getMappingBaseData();
                if(cloudStoreService.getMappingInput(mMapping.getId(), BaseData.class) == null) {
                    bd.setId(UUID.randomUUID().toString().toUpperCase());
                    dataSaved = cloudStoreService.persistEntity(BaseData.class, bd);
                }
                else
                    dataSaved = cloudStoreService.updateEntity(BaseData.class, bd);

                RockMass rm = mappingInstance.getMappingRockMass();
                if(cloudStoreService.getMappingInput(mMapping.getId(), RockMass.class) == null) {
                    rm.setId(UUID.randomUUID().toString().toUpperCase());
                    dataSaved = cloudStoreService.persistEntity(RockMass.class, rm);
                }
                else
                    dataSaved = cloudStoreService.updateEntity(RockMass.class, rm);
                break;
            case "LTH":
                List<Lithology> lithologies = mappingInstance.getMappingLithologies();
                dataListSaved = false;
                for(Lithology l : lithologies) {
                    if(cloudStoreService.getMappingInput(mMapping.getId(), Lithology.class) == null) {
                        l.setId(UUID.randomUUID().toString().toUpperCase());
                        dataListSaved = cloudStoreService.persistEntity(Lithology.class, l);
                    }
                    else {
                        if(!l.getId().isEmpty()) {
                            dataListSaved = cloudStoreService.updateEntity(Lithology.class, l);
                        }
                        else {
                            l.setId(UUID.randomUUID().toString().toUpperCase());
                            dataListSaved = cloudStoreService.persistEntity(Lithology.class, l);
                        }

                    }
                }
                dataSaved = dataListSaved;
                break;
            case "DISC":
                List<Discontinuity> discontinuities = mappingInstance.getMappingDiscontinuities();
                dataListSaved = false;
                for(Discontinuity d : discontinuities) {
                    if(cloudStoreService.getMappingInput(mMapping.getId(), Discontinuity.class) == null) {
                        d.setId(UUID.randomUUID().toString().toUpperCase());
                        dataListSaved = cloudStoreService.persistEntity(Discontinuity.class, d);
                    }
                    else {
                        if(!d.getId().isEmpty()) {
                            dataListSaved = cloudStoreService.updateEntity(Discontinuity.class, d);
                        }
                        else {
                            d.setId(UUID.randomUUID().toString().toUpperCase());
                            dataListSaved = cloudStoreService.persistEntity(Discontinuity.class, d);
                        }
                    }
                }

                List<Discontinuity> deletedDiscontinuities = mappingInstance.getDeletedDiscontinuities();
                for(Discontinuity discontinuity : deletedDiscontinuities) {
                    cloudStoreService.deleteEntity(Discontinuity.class, discontinuity);
                }

                if(!discontinuities.isEmpty()) {
                    StereonetPicture stereonetPicture = mappingInstance.getMappingStereonet();
                    if(cloudStoreService.getMappingInput(mMapping.getId(), StereonetPicture.class) == null) {
                        stereonetPicture.setId(UUID.randomUUID().toString().toUpperCase());
                        cloudStoreService.persistEntity(StereonetPicture.class, stereonetPicture);
                    }
                    else {
                        cloudStoreService.updateEntity(StereonetPicture.class, stereonetPicture);
                    }
                }

                dataSaved = dataListSaved;
                break;
            case "AINFO":
                FailureZone f = mappingInstance.getMappingFailureZone();
                if(cloudStoreService.getMappingInput(mMapping.getId(), FailureZone.class) == null) {
                    f.setId(UUID.randomUUID().toString().toUpperCase());
                    cloudStoreService.persistEntity(FailureZone.class, f);
                }
                else
                    cloudStoreService.updateEntity(FailureZone.class, f);

                Particularities p = mappingInstance.getMappingParticularities();
                if(cloudStoreService.getMappingInput(mMapping.getId(), Particularities.class) == null) {
                    p.setId(UUID.randomUUID().toString().toUpperCase());
                    cloudStoreService.persistEntity(Particularities.class, p);
                }
                else
                    cloudStoreService.updateEntity(Particularities.class, p);

                AdditionalInformation ai = mappingInstance.getMappingAdditionalInformation();
                SpecialFeatures sf = mappingInstance.getMappingSpecialFeatures();
                RockMassHazard rmh = mappingInstance.getMappingRockMassHazard();
                boolean aInfoCompleted = sf.getCompleted() && rmh.getCompleted() && f.getCompleted() && p.getCompleted();
                ai.setCompleted(aInfoCompleted);
                if(cloudStoreService.getMappingInput(mMapping.getId(), AdditionalInformation.class) == null) {
                    ai.setId(UUID.randomUUID().toString().toUpperCase());
                    dataSaved = cloudStoreService.persistEntity(AdditionalInformation.class, ai);

                    sf.setId(UUID.randomUUID().toString().toUpperCase());
                    sf.setIdAdditionalInformation(ai.getId());
                    rmh.setId(UUID.randomUUID().toString().toUpperCase());
                    rmh.setIdAdditionalInformation(ai.getId());

                    cloudStoreService.persistEntity(SpecialFeatures.class, sf);
                    cloudStoreService.persistEntity(RockMassHazard.class, rmh);
                }
                else {
                    dataSaved = cloudStoreService.updateEntity(AdditionalInformation.class, ai);
                    cloudStoreService.updateEntity(SpecialFeatures.class, sf);
                    cloudStoreService.updateEntity(RockMassHazard.class, rmh);
                }
                break;
            case "QMET":
                QValue qv = mappingInstance.getMappingQValue();
                if(cloudStoreService.getMappingInput(mMapping.getId(), QValue.class) == null) {
                    qv.setId(UUID.randomUUID().toString().toUpperCase());
                    cloudStoreService.persistEntity(QValue.class, qv);
                }
                else
                    cloudStoreService.updateEntity(QValue.class, qv);

                QValueInputSelection qvis = mappingInstance.getqValueInputSelection();
                if(cloudStoreService.getMappingInput(mMapping.getId(), QValueInputSelection.class) == null) {
                    qvis.setId(UUID.randomUUID().toString().toUpperCase());
                    dataSaved = cloudStoreService.persistEntity(QValueInputSelection.class, qvis);
                }
                else
                    dataSaved = cloudStoreService.updateEntity(QValueInputSelection.class, qvis);

                List<QValueDiscontinuity> qValueDiscontinuities = new ArrayList<>();
                List<QValueDiscontinuity> qValueDiscontinuitiesNew = mappingInstance.getqValueDiscontinuities();
                if(!qv.getId().trim().isEmpty()) qValueDiscontinuities = cloudStoreService.getQValueDiscontinuities(qv.getId());

                for(QValueDiscontinuity q : qValueDiscontinuitiesNew) {
                    if(!q.getId().isEmpty()) {
                        dataListSaved = cloudStoreService.updateEntity(QValueDiscontinuity.class, q);
                    }
                    else {
                        q.setId(UUID.randomUUID().toString().toUpperCase());
                        q.setIdQValue(qv.getId().trim());
                        dataListSaved = cloudStoreService.persistEntity(QValueDiscontinuity.class, q);
                    }
                }

                List<QValueDiscontinuity> qValueDiscontinuitiesDelete = new ArrayList<>();
                for(QValueDiscontinuity q : qValueDiscontinuities) {
                    boolean exists = false;

                    for (QValueDiscontinuity qNew : qValueDiscontinuitiesNew)
                        if (q.getId().trim().equalsIgnoreCase(qNew.getId().trim())) exists = true;

                    if(!exists) qValueDiscontinuitiesDelete.add(q);
                }

                for (QValueDiscontinuity qDelete : qValueDiscontinuitiesDelete) cloudStoreService.deleteEntity(QValueDiscontinuity.class, qDelete);
                break;
            case "RMR":
                Rmr rmr = mappingInstance.getMappingRmr();
                if(cloudStoreService.getMappingInput(mMapping.getId(), Rmr.class) == null) {
                    rmr.setId(UUID.randomUUID().toString().toUpperCase());
                    cloudStoreService.persistEntity(Rmr.class, rmr);
                }
                else
                    cloudStoreService.updateEntity(Rmr.class, rmr);

                RmrInputSelection rmris = mappingInstance.getRmrInputSelection();
                if(cloudStoreService.getMappingInput(mMapping.getId(), RmrInputSelection.class) == null) {
                    rmris.setId(UUID.randomUUID().toString().toUpperCase());
                    dataSaved = cloudStoreService.persistEntity(RmrInputSelection.class, rmris);
                }
                else
                    dataSaved = cloudStoreService.updateEntity(RmrInputSelection.class, rmris);
                break;
            case "GSI":
                GsiValue gsiValue = mappingInstance.getMappingGsiValue();
                if(cloudStoreService.getMappingInput(mMapping.getId(), GsiValue.class) == null) {
                    gsiValue.setId(UUID.randomUUID().toString().toUpperCase());
                    dataSaved = cloudStoreService.persistEntity(GsiValue.class, gsiValue);
                }
                else
                    dataSaved = cloudStoreService.updateEntity(GsiValue.class, gsiValue);
                break;
            case "PIC":
                List<Picture> mappingPicturesData = mappingInstance.getMappingPicturesData();
                File[] mappingPictures = mappingInstance.getMappingPictures();

                dataListSaved = false;

                for(Picture picture : mappingPicturesData) {
                    if(cloudStoreService.getMappingInput(mMapping.getId(), Picture.class) == null) {
                        picture.setId(UUID.randomUUID().toString().toUpperCase());
                        picture.setCompleted(mappingPictures[picture.getCode().intValue()] != null);
                        dataListSaved = cloudStoreService.persistEntity(Picture.class, picture);
                    }
                    else {
                        if(!picture.getId().isEmpty()) {
                            picture.setCompleted(mappingPictures[picture.getCode().intValue()] != null);
                            dataListSaved = cloudStoreService.updateEntity(Picture.class, picture);
                        }
                        else {
                            picture.setId(UUID.randomUUID().toString().toUpperCase());
                            picture.setCompleted(mappingPictures[picture.getCode().intValue()] != null);
                            dataListSaved = cloudStoreService.persistEntity(Picture.class, picture);
                        }
                    }
                }
                dataSaved = dataListSaved;
                break;
            case "EXPT":
                ExpandedTunnel expandedTunnel = mappingInstance.getMappingExpandedTunnel();
                File[] mappingExpandedPictures = mappingInstance.getMappingExpandedTunnelPictures();
                if(cloudStoreService.getMappingInput(mMapping.getId(), ExpandedTunnel.class) == null) {
                    expandedTunnel.setId(UUID.randomUUID().toString().toUpperCase());
                    expandedTunnel.setCompleted(mappingExpandedPictures[expandedTunnel.getCode().intValue()] != null);
                    dataSaved = cloudStoreService.persistEntity(ExpandedTunnel.class, expandedTunnel);
                }
                else {
                    expandedTunnel.setCompleted(mappingExpandedPictures[expandedTunnel.getCode().intValue()] != null);
                    dataSaved = cloudStoreService.updateEntity(ExpandedTunnel.class, expandedTunnel);
                }
                break;
            case "GCOMM":
                AdditionalDescription ad = mappingInstance.getMappingAdditionalDescription();
                if(cloudStoreService.getMappingInput(mMapping.getId(), AdditionalDescription.class) == null) {
                    ad.setId(UUID.randomUUID().toString().toUpperCase());
                    dataSaved = cloudStoreService.persistEntity(AdditionalDescription.class, ad);
                }
                else
                    dataSaved = cloudStoreService.updateEntity(AdditionalDescription.class, ad);
                break;
            case "SUMM":
                List<RecommendationRockbolt> rockbolts = mappingInstance.getRockbolts();
                List<RecommendationRockbolt> deletedRockbolts = mappingInstance.getDeletedRockbolts();
                dataListSaved = false;
                for(RecommendationRockbolt rockbolt : rockbolts) {
                    if(cloudStoreService.getMappingInput(mMapping.getId(), RecommendationRockbolt.class) == null) {
                        rockbolt.setId(UUID.randomUUID().toString().toUpperCase());
                        dataListSaved = cloudStoreService.persistEntity(RecommendationRockbolt.class, rockbolt);
                    }
                    else {
                        if(!rockbolt.getId().isEmpty()) {
                            dataListSaved = cloudStoreService.updateEntity(RecommendationRockbolt.class, rockbolt);
                        }
                        else {
                            rockbolt.setId(UUID.randomUUID().toString().toUpperCase());
                            dataListSaved = cloudStoreService.persistEntity(RecommendationRockbolt.class, rockbolt);
                        }
                    }
                }

                for(RecommendationRockbolt rockbolt : deletedRockbolts) {
                    cloudStoreService.deleteEntity(RecommendationRockbolt.class, rockbolt);
                }

                List<RecommendationShotcrete> shotcretes = mappingInstance.getShotcretes();
                List<RecommendationShotcrete> deletedShotcretes = mappingInstance.getDeletedShotcretes();
                dataListSaved = false;
                for(RecommendationShotcrete shotcrete : shotcretes) {
                    if(cloudStoreService.getMappingInput(mMapping.getId(), RecommendationShotcrete.class) == null) {
                        shotcrete.setId(UUID.randomUUID().toString().toUpperCase());
                        dataListSaved = cloudStoreService.persistEntity(RecommendationShotcrete.class, shotcrete);
                    }
                    else {
                        if(!shotcrete.getId().isEmpty()) {
                            dataListSaved = cloudStoreService.updateEntity(RecommendationShotcrete.class, shotcrete);
                        }
                        else {
                            shotcrete.setId(UUID.randomUUID().toString().toUpperCase());
                            dataListSaved = cloudStoreService.persistEntity(RecommendationShotcrete.class, shotcrete);
                        }
                    }
                }

                for(RecommendationShotcrete shotcrete : deletedShotcretes) {
                    cloudStoreService.deleteEntity(RecommendationShotcrete.class, shotcrete);
                }
                dataSaved = dataListSaved;
                break;
            default:
                break;
        }

        return dataSaved;
    }

    @Override
    public boolean saveMappingInstance(MappingInstance mappingInstance) {
        boolean dataSaved = false;
        List<Boolean> savingStatus = new ArrayList<>();

        for(MappingInputModule mim : inputs) {
            if(mim.getEnabled()) {
                dataSaved = saveMappingInput(mim.getLabel(), mappingInstance);
                savingStatus.add(dataSaved);
            }

            if(mim.getSubInputs() != null) {
                for(MappingInputModule mim2 : mim.getSubInputs()) {
                    if(mim2.getEnabled()) dataSaved = saveMappingInput(mim2.getLabel(), mappingInstance);
                    savingStatus.add(dataSaved);
                }
            }
        }

        //END OF STORAGE
        for(Boolean b : savingStatus) {
            if(!b) {
                dataSaved = !b;
                break;
            }
        }
        return dataSaved;
    }

    private boolean deleteMappingInput(String label, MappingInstance mi) {
        switch(label) {
            case "RMO":
                BaseData bd = mi.getMappingBaseData();
                cloudStoreService.deleteEntity(BaseData.class, bd);

                RockMass rm = mi.getMappingRockMass();
                cloudStoreService.deleteEntity(RockMass.class, rm);
                break;
            case "LTH":
                List<Lithology> ls = mi.getMappingLithologies();
                for(Lithology l : ls) {
                    cloudStoreService.deleteEntity(Lithology.class, l);
                }
                break;
            case "DISC":
                List<Discontinuity> ds = mi.getMappingDiscontinuities();
                for(Discontinuity d : ds) {
                    cloudStoreService.deleteEntity(Discontinuity.class, d);
                }
                break;
            case "AINFO":
                AdditionalInformation ai = mi.getMappingAdditionalInformation();
                RockMassHazard rmh = mi.getMappingRockMassHazard();
                SpecialFeatures sf = mi.getMappingSpecialFeatures();
                cloudStoreService.deleteEntity(SpecialFeatures.class, sf);
                cloudStoreService.deleteEntity(RockMassHazard.class, rmh);
                cloudStoreService.deleteEntity(AdditionalInformation.class, ai);

                FailureZone fz = mi.getMappingFailureZone();
                cloudStoreService.deleteEntity(FailureZone.class, fz);

                Particularities p = mi.getMappingParticularities();
                cloudStoreService.deleteEntity(Particularities.class, p);
                break;
            case "QMET":
                List<QValueDiscontinuity> qValueDiscontinuities = mi.getqValueDiscontinuities();
                for(int i = 0; i < qValueDiscontinuities.size(); i++) cloudStoreService.deleteEntity(QValueDiscontinuity.class, qValueDiscontinuities.get(i));

                QValue qv = mi.getMappingQValue();
                cloudStoreService.deleteEntity(QValue.class, qv);

                QValueInputSelection qvis = mi.getqValueInputSelection();
                cloudStoreService.deleteEntity(QValueInputSelection.class, qvis);
                break;
            case "RMR":
                Rmr rmr = mi.getMappingRmr();
                cloudStoreService.deleteEntity(Rmr.class, rmr);

                RmrInputSelection rmris = mi.getRmrInputSelection();
                cloudStoreService.deleteEntity(RmrInputSelection.class, rmris);
                break;
            case "GSI":
                GsiValue gsiValue = mi.getMappingGsiValue();
                cloudStoreService.deleteEntity(GsiValue.class, gsiValue);
                break;
            case "PIC":
                List<Picture> pictures = mi.getMappingPicturesData();
                File[] pictureFiles = blobStorageService.getLocalPictureFiles(mi.getMapping().getId().toLowerCase(), pictures);

                for(int i = 0; i < pictures.size(); i++) {
                    Picture picture = pictures.get(i);

                    if (pictureFiles[i].exists()) {
                        pictureFiles[i].delete();
                    }
                    cloudStoreService.deleteEntity(Picture.class, picture);
                }
                break;
            case "EXPT":

                break;
            case "GCOMM":
                AdditionalDescription ad = mi.getMappingAdditionalDescription();
                cloudStoreService.deleteEntity(AdditionalDescription.class, ad);
                break;
            case "SUMM":
                List<RecommendationRockbolt> rockbolts = mi.getRockbolts();
                for(RecommendationRockbolt r : rockbolts) {
                    cloudStoreService.deleteEntity(RecommendationRockbolt.class, r);
                }
                List<RecommendationRockbolt> deletedRockbolts = mi.getDeletedRockbolts();
                for(RecommendationRockbolt r : deletedRockbolts) {
                    cloudStoreService.deleteEntity(RecommendationRockbolt.class, r);
                }

                List<RecommendationShotcrete> shotcretes = mi.getShotcretes();
                for(RecommendationShotcrete r : shotcretes) {
                    cloudStoreService.deleteEntity(RecommendationShotcrete.class, r);
                }
                List<RecommendationShotcrete> deletedShotcretes = mi.getDeletedShotcretes();
                for(RecommendationShotcrete r : deletedShotcretes) {
                    cloudStoreService.deleteEntity(RecommendationShotcrete.class, r);
                }
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public boolean deleteInputs(Mapping mapping) {
        mMapping = mapping;
        MappingInstance mi = getMappingInstance();

        for(MappingInputModule mim : inputs) {
            if(mim.getEnabled()) {
                deleteMappingInput(mim.getLabel(), mi);
            }

            if(mim.getSubInputs() != null) {
                for(MappingInputModule mim2 : mim.getSubInputs()) {
                    if(mim2.getEnabled()) deleteMappingInput(mim2.getLabel(), mi);
                }
            }
        }

        return true;
    }

    private boolean isMappingInputEnabled(MappingInputModule input) {
        boolean res = false;
        MappingInput mi = getMappingInput(input);

        for(ProjectMappingInput pmi : projectMappingInputs) {
            if(pmi.getIdMappingInput().trim().equalsIgnoreCase(mi.getId().trim())) {
                res = true;
                break;
            }
        }
        return res;
    }

    private MappingInput getMappingInput(MappingInputModule input) {
        MappingInput res = null;
        for(MappingInput mi : mappingInputs) {
            if (mi.getLabel().trim().equalsIgnoreCase(input.getLabel().trim())) {
                res = mi;
                break;
            }
        }
        return res;
    }

    private boolean getCompletedStatus(String label, String mappingId) {
        switch(label) {
            case "RMO":
                RockMass rm = (RockMass) cloudStoreService.getMappingInput(mappingId, RockMass.class);
                return rm != null ? (rm.getCompleted()) : false;
            case "LTH":
                List<Lithology> ls = (List<Lithology>) cloudStoreService.getMappingInputs(mappingId, Lithology.class);
                boolean lithologiesCompleted = !ls.isEmpty();
                for(Lithology l : ls) {
                    lithologiesCompleted = lithologiesCompleted && l.getCompleted();
                }
                return lithologiesCompleted;
            case "DISC":
                List<Discontinuity> ds = (List<Discontinuity>) cloudStoreService.getMappingInputs(mappingId, Discontinuity.class);
                boolean discontinuitiesCompleted = !ds.isEmpty();
                for(Discontinuity d : ds) {
                    discontinuitiesCompleted = discontinuitiesCompleted && d.getCompleted();
                }
                return discontinuitiesCompleted;
            case "AINFO":
                AdditionalInformation ai = (AdditionalInformation) cloudStoreService.getMappingInput(mappingId, AdditionalInformation.class);
                return ai != null ? (ai.getCompleted()) : false;
            case "QMET":
                QValue qv = (QValue) cloudStoreService.getMappingInput(mappingId, QValue.class);
                return qv != null ? (qv.getCompleted()) : false;
            case "RMR":
                Rmr rmr = (Rmr) cloudStoreService.getMappingInput(mappingId, Rmr.class);
                return rmr != null ? (rmr.getCompleted()) : false;
            case "GSI":
//                Gsi gsi = (Gsi) cloudStoreService.getMappingInput(mappingId, Gsi.class);
//                return gsi != null ? (gsi.getCompleted()) : false;
                return false;
            case "PIC":
                List<Picture> pictures = (List<Picture>) cloudStoreService.getMappingInputPictures(mappingId, Picture.class);
                boolean picturesCompleted = !pictures.isEmpty();
                for(Picture p : pictures) {
                    picturesCompleted = picturesCompleted && p.getCompleted();
                }
                return picturesCompleted;
            case "EXPT":
                ExpandedTunnel expandedTunnel = (ExpandedTunnel) cloudStoreService.getMappingInput(mappingId, ExpandedTunnel.class);
                return expandedTunnel != null ? (expandedTunnel.getCompleted()) : false;
            case "GCOMM":
                AdditionalDescription ad = (AdditionalDescription) cloudStoreService.getMappingInput(mappingId, AdditionalDescription.class);
                return ad != null ? (ad.getCompleted()) : false;
            case "SUMM":
                return getCompleteFinalStatus();
            default:
                return false;
        }
    }

    private void initDatasetPerItem(String mappingId) {
        inputs = new ArrayList<>();
        List<MappingInputModule> subInputs = new ArrayList<>();

        String lang = Locale.getDefault().getLanguage();

        Mapping mapping = cloudStoreService.getMappingById(mappingId);
        Tunnel tunnel = cloudStoreService.getTunnelByFace(mapping.getIdFace());
        projectMappingInputs = cloudStoreService.getMappingInputsByProject(tunnel.getIdProject());
        mappingInputs = cloudStoreService.getMappingInputs();

        List<MappingInput> parents = new ArrayList<>();
        List<MappingInput> childs = new ArrayList<>();
        for(MappingInput mi : mappingInputs) {
            if(mi.getParent() == null) parents.add(mi);
            else childs.add(mi);
        }

        MappingInputModule input;
        Boolean inputCompleted;
        MappingInputModule subInput;
        int newCode;
        int newCodeParent = 0;
        boolean isSummary;

        for(MappingInput mi : parents) {
            input = new MappingInputModule();
            input.setName(lang.equals("es") ? mi.getNameEs().trim() : mi.getName().trim());
            input.setCode(newCodeParent);
            input.setLabel(mi.getLabel().trim());
            isSummary = input.getLabel().trim().equalsIgnoreCase("SUMM");
            if(!isMappingInputEnabled(input) && !isSummary) continue;

            input.setEnabled(true);

            inputCompleted = true;
            newCode = 0;

            for(MappingInput mi1 : childs) {

                if(mi1.getParent().trim().equalsIgnoreCase(mi.getId().trim())) {
                    subInput = new MappingInputModule();
                    subInput.setName(lang.equals("es") ? mi1.getNameEs().trim() : mi1.getName().trim());
                    subInput.setCode(newCode);
                    subInput.setLabel(mi1.getLabel().trim());
                    subInput.setEnabled(isMappingInputEnabled(subInput));
                    subInput.setCompleted(getCompletedStatus(mi1.getLabel().trim(), mappingId));
                    subInput.setParent(input);
                    if(subInput.getEnabled()) { subInputs.add(subInput); ++newCode; }

                    inputCompleted = inputCompleted && subInput.getCompleted();
                }
            }

//            if(isSummary) input.setCompleted(getCompleteFinalStatus());
//            else input.setCompleted(inputCompleted);

            if(subInputs.size() > 0) {
                input.setCompleted(inputCompleted);
                input.setSubInputs(subInputs);
            }
            else input.setCompleted(getCompletedStatus(input.getLabel().trim(), mappingId));

            inputs.add(input);
            ++newCodeParent;

            subInputs = new ArrayList<>();
        }
    }

    private boolean getCompleteFinalStatus() {
        boolean status = true;
        for(MappingInputModule mappingInputModule : inputs) {
            if(!mappingInputModule.getCompleted()) {
                status = false;
                break;
            }
        }
        return status;
    }
}
