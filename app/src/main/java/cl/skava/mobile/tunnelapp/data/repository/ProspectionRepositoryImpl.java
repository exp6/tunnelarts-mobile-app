package cl.skava.mobile.tunnelapp.data.repository;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.domain.repository.ProspectionRepository;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class ProspectionRepositoryImpl implements ProspectionRepository {

    private List<ProspectionHole> prospections;
    private CloudStoreService cloudStoreService;
    private Context mActivity;
    private String mProspectionHoleGroupId;

    public ProspectionRepositoryImpl(Context activity, String prospectionHoleGroupId) {
        cloudStoreService = new CloudStoreService(activity);
        mActivity = activity;
        prospections = new ArrayList<>();
        mProspectionHoleGroupId = prospectionHoleGroupId;
    }

    @Override
    public boolean insert(ProspectionHole prospection) {
        prospection.setId(UUID.randomUUID().toString().toUpperCase());
        return cloudStoreService.persistEntity(ProspectionHole.class, prospection);
    }

    @Override
    public boolean update(ProspectionHole prospection) {
        return cloudStoreService.updateEntity(ProspectionHole.class, prospection);
    }

    @Override
    public boolean delete(ProspectionHole prospection) {
        List<Rod> rods = cloudStoreService.getRodsByProspectionHole(prospection.getId());

        for(Rod r : rods) {
            cloudStoreService.deleteEntity(Rod.class, r);
        }

        return cloudStoreService.deleteEntity(ProspectionHole.class, prospection);
    }

    @Override
    public List<ProspectionHole> getList() {
        prospections = cloudStoreService.getProspectionHolesByPHG(mProspectionHoleGroupId);
        return prospections;
    }
}
