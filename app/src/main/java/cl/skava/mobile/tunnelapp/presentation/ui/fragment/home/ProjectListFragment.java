package cl.skava.mobile.tunnelapp.presentation.ui.fragment.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.home.ProjectListAdapter;

/**
 * Created by Jose Ignacio Vera on 07-06-2016.
 */
public class ProjectListFragment extends Fragment {

    private static final String TAG = "ProjectListFragment";

    protected RecyclerView mRecyclerView;
    protected ProjectListAdapter mAdapter;

    protected RecyclerView.LayoutManager mLayoutManager;

    protected List<Project> mProjectDataset;

    private OnProjectItemSelectedListener mCallback;

    public interface OnProjectItemSelectedListener {
        void onProjectSelected(int position);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_projects, container, false);
        rootView.setTag(TAG);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        setRecyclerViewLayoutManager();
        mAdapter = new ProjectListAdapter(mProjectDataset, mCallback, mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);
        return rootView;
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnProjectItemSelectedListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnUserDataListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setRecyclerViewLayoutManager() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMProjectDataset(List<Project> mProjectDataset) {
        this.mProjectDataset = mProjectDataset;
    }

    public RecyclerView.LayoutManager getMLayoutManager() {
        return mLayoutManager;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void updateItemList() {
        mAdapter = new ProjectListAdapter(mProjectDataset, mCallback, mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);
    }
}
