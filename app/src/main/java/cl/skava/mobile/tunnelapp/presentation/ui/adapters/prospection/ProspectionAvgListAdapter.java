package cl.skava.mobile.tunnelapp.presentation.ui.adapters.prospection;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.prospection.ProspectionAvgListFragment;

/**
 * Created by Jose Ignacio Vera on 24-08-2016.
 */
public class ProspectionAvgListAdapter extends RecyclerView.Adapter<ProspectionAvgListAdapter.ViewHolder> {

    private static final String TAG = "ProspectionAvgListAdapter";
    private HashMap<ProspectionHole, List<Rod>> rodsByProspection;
//    ViewHolder viewHolderPool[];
    protected RecyclerView mRecyclerView;
    private List<RockQuality> rockQualities;
    private List<ArrayList> rodsByPosition;
    private List<Double> avgDistancesPerPosition;
    private List<Double> avgQValuesPerPosition;
    private List<Double> minQValuesPerPosition;
    private List<Double> maxQValuesPerPosition;
    private List<String> estimatedRockPerPosition;
    private ProspectionAvgListFragment mCallback;

    private final String lang = Locale.getDefault().getLanguage();

    public ProspectionAvgListAdapter(HashMap<ProspectionHole, List<Rod>> dataSet,
                                     RecyclerView recyclerView,
                                     ProspectionAvgListFragment callback) {
        rodsByProspection = dataSet;
//        viewHolderPool = new ViewHolder[rodsByProspection.size()];
        mRecyclerView = recyclerView;
        setHasStableIds(true);
        rockQualities = new ArrayList<>();
        initData();
        rodsByPosition = new ArrayList<>();
        estimatedRockPerPosition = new ArrayList<>();
        mCallback = callback;
        calculateAverages();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView avgTitle;
        private final TextView estimatedRock;
        private final TextView projectedDistance;
        private final TextView qValue;
        private final TextView qValueMax;
        private final TextView qValueMin;

        public ViewHolder(View v) {
            super(v);

            avgTitle = (TextView) v.findViewById(R.id.avg_title);
            estimatedRock = (TextView) v.findViewById(R.id.estimated_rock_quality);
            projectedDistance = (TextView) v.findViewById(R.id.projected_distance_value);
            qValue = (TextView) v.findViewById(R.id.avg_q_value);
            qValueMax = (TextView) v.findViewById(R.id.q_value_max);
            qValueMin = (TextView) v.findViewById(R.id.q_value_min);
        }

        public TextView getAvgTitle() {
            return avgTitle;
        }

        public TextView getEstimatedRock() {
            return estimatedRock;
        }

        public TextView getProjectedDistance() {
            return projectedDistance;
        }

        public TextView getqValue() {
            return qValue;
        }

        public TextView getqValueMax() {
            return qValueMax;
        }

        public TextView getqValueMin() {
            return qValueMin;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_prospection_calculations_avg_list_item, parent, false);
        holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.getAvgTitle().setText((lang.equals("es") ? "Promedio Pruebas de Agujero - Barra" : "Probe Holes Average - Rod ") + (position+1));
        holder.getEstimatedRock().setText(calculateEstimatedRock(avgQValuesPerPosition.get(position)));
        holder.getProjectedDistance().setText(round(avgDistancesPerPosition.get(position), 4) + " [m]");
        holder.getqValue().setText(round(avgQValuesPerPosition.get(position), 4) + "");
        holder.getqValueMax().setText(round(maxQValuesPerPosition.get(position), 4) + "");
        holder.getqValueMin().setText(round(minQValuesPerPosition.get(position), 4) + "");
    }

    @Override
    public int getItemCount() {
        return rodsByPosition.size();
    }

    private void sortDataset() {
        List<ArrayList> newSortList = new ArrayList<>();

        for(int i = rodsByPosition.size()-1; i >= 0; --i) {
            newSortList.add(rodsByPosition.get(i));
        }
        rodsByPosition = newSortList;
    }

    private void calculateAverages() {
        rodsByPosition = getRodsByPosition();

//        sortDataset();

        avgDistancesPerPosition = new ArrayList<>();
        avgQValuesPerPosition = new ArrayList<>();

        minQValuesPerPosition = new ArrayList<>();
        maxQValuesPerPosition = new ArrayList<>();

        double avgProjectedDistance;
        double distanceSum;

        for(List<Rod> rods : rodsByPosition) {
            Double totalRods = new Double(rods.size());
            distanceSum = 0.0;

            for(Rod rod : rods) {
                ProspectionHole ph = getProspectionHole(rod.getIdProspectionHole().trim());
                Double calculatedDistance = calculateProjectedDistance(ph,rod);
                distanceSum += calculatedDistance;
            }

            avgProjectedDistance = distanceSum/totalRods.doubleValue();
            avgDistancesPerPosition.add(avgProjectedDistance);
            avgQValuesPerPosition.add(calculateQAvg(rods));
            minQValuesPerPosition.add(calculateQMin(rods));
            maxQValuesPerPosition.add(calculateQMax(rods));
            estimatedRockPerPosition.add(calculateEstimatedRock(calculateQAvg(rods)));
        }

        mCallback.onCalculationsReady(avgDistancesPerPosition, avgQValuesPerPosition, minQValuesPerPosition, maxQValuesPerPosition, estimatedRockPerPosition);
    }

    private Double calculateQMax(List<Rod> rods) {
        Double qvalue = 0.0;
        for(Rod r : rods) {
            if(r.getqValue() > qvalue) qvalue = r.getqValue().doubleValue();
            else continue;
        }
        return qvalue;
    }

    private Double calculateQMin(List<Rod> rods) {
        Double qvalue = calculateQMax(rods);
        for(Rod r : rods) {
            if(r.getqValue() < qvalue) qvalue = r.getqValue().doubleValue();
            else continue;
        }
        return qvalue;
    }

    private ProspectionHole getProspectionHole(String phId) {
        List<ProspectionHole> prospectionHoles = new ArrayList<ProspectionHole>(rodsByProspection.keySet());

        for(ProspectionHole ph : prospectionHoles)
            if(ph.getId().trim().equalsIgnoreCase(phId)) return ph;

        return null;
    }

    private List<ArrayList> getRodsByPosition() {
        Rod[][] rodMap = getRodMatrix();
        List<ArrayList> positions = new ArrayList<>();

        if(rodMap.length > 0) {
            int rows = rodMap.length;
            int cols = rodMap[0].length;

            for(int j = 0 ; j < cols; j++) {
                List<Rod> rods = new ArrayList<>();

                for(int i = 0; i < rows; i++) {
                    Rod r = rodMap[i][j];
                    if(r != null) rods.add(r);
                    else continue;
                }
                positions.add((ArrayList) rods);
            }
        }
        return positions;
    }

    private Rod[][] getRodMatrix() {
        List<ProspectionHole> prospectionHoles = new ArrayList<ProspectionHole>(rodsByProspection.keySet());
        int rows = prospectionHoles.size();
        int columns = 0;

        List<Rod> rods;
        for(ProspectionHole ph : prospectionHoles) {
            rods = rodsByProspection.get(ph);
            if(columns < rods.size()) columns = rods.size();
        }

        Rod[][] rodMap = new Rod[rows][columns];

        for(int i = 0; i < rows; i++ ) {
            ProspectionHole ph = prospectionHoles.get(i);
            rods = rodsByProspection.get(ph);

            for(int j = 0; j < columns; j++) {
                if(rods.size() <= j) {
                    rodMap[i][j] = null;
                }
                else {
                    Rod r = rods.get(j);
                    rodMap[i][j] = r;
                }
            }
        }
        return rodMap;
    }

    private Double calculateProjectedDistance(ProspectionHole prospection, Rod rod) {
        Double angle = prospection.getAngle().doubleValue();
        Double distance = rod.getDrilledLength().doubleValue();
        return (Math.cos(Math.toRadians(angle)) * distance);
    }

    private Double calculateQAvg(List<Rod> rods) {
        Double qvalue = 0.0;
        Double total = new Double(rods.size());

        for(Rod r : rods) {
            qvalue += r.getqValue();
        }

        return qvalue/total;
    }

    private String calculateEstimatedRock(Double avgQ) {
        String quality = "N/A";
        for(RockQuality rq : rockQualities) {
            if(rq.qualityMatched(avgQ)) {
                quality = rq.getName();
                break;
            }
        }

        return quality;
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private void initData() {
        RockQuality rq = new RockQuality();
        rq.setName("Poor");
        rq.setUpperBound(new Double(4));
        rq.setLowerBound(new Double(1));
        rq.setCode(3);
        rockQualities.add(rq);

        rq = new RockQuality();
        rq.setName("Good");
        rq.setUpperBound(new Double(40));
        rq.setLowerBound(new Double(10));
        rq.setCode(5);
        rockQualities.add(rq);

        rq = new RockQuality();
        rq.setName("Extremmely poor");
        rq.setUpperBound(new Double(0.1));
        rq.setLowerBound(new Double(0.01));
        rq.setCode(1);
        rockQualities.add(rq);

        rq = new RockQuality();
        rq.setName("Exceptionally good");
        rq.setUpperBound(new Double(401));
        rq.setLowerBound(new Double(400));
        rq.setCode(8);
        rockQualities.add(rq);

        rq = new RockQuality();
        rq.setName("Exceptionally poor");
        rq.setUpperBound(new Double(0.001));
        rq.setLowerBound(new Double(0.01));
        rq.setCode(0);
        rockQualities.add(rq);

        rq = new RockQuality();
        rq.setName("Fair");
        rq.setUpperBound(new Double(10));
        rq.setLowerBound(new Double(4));
        rq.setCode(4);
        rockQualities.add(rq);

        rq = new RockQuality();
        rq.setName("Extremmely good");
        rq.setUpperBound(new Double(400));
        rq.setLowerBound(new Double(100));
        rq.setCode(7);
        rockQualities.add(rq);

        rq = new RockQuality();
        rq.setName("Very poor");
        rq.setUpperBound(new Double(1));
        rq.setLowerBound(new Double(0.1));
        rq.setCode(2);
        rockQualities.add(rq);

        rq = new RockQuality();
        rq.setName("Very good");
        rq.setUpperBound(new Double(100));
        rq.setLowerBound(new Double(40));
        rq.setCode(6);
        rockQualities.add(rq);
    }

    class RockQuality {
        String name;
        Double lowerBound;
        Double upperBound;
        Integer code;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Double getLowerBound() {
            return lowerBound;
        }

        public void setLowerBound(Double lowerBound) {
            this.lowerBound = lowerBound;
        }

        public Double getUpperBound() {
            return upperBound;
        }

        public void setUpperBound(Double upperBound) {
            this.upperBound = upperBound;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public boolean qualityMatched(Double qvalue) {
            return (lowerBound <= qvalue && qvalue < upperBound);
        }
    }
}
