package cl.skava.mobile.tunnelapp.data.repository;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.UserProject;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.repository.ProjectRepository;

/**
 * Created by Jose Ignacio Vera on 16-06-2016.
 */
public class ProjectRepositoryImpl implements ProjectRepository {

    private CloudStoreService cloudStoreService;
    private User mUser;

    public ProjectRepositoryImpl(Context activity, User user) {
        cloudStoreService = new CloudStoreService(activity);
        mUser = user;
    }

    @Override
    public Client getClient() {
        return cloudStoreService.getClientById(mUser.getIdClient().toUpperCase());
    }

    @Override
    public List<Project> getAllProjects() {
        List<UserProject> userProjects = cloudStoreService.getProjectsByUser(mUser.getId().toUpperCase());

//        List<Project> projects = cloudStoreService.getProjects();
        List<Project> projects = new ArrayList<>();
        Project project;

        for(UserProject up : userProjects) {
            project = cloudStoreService.getProjectsById(up.getIdProject());
            projects.add(project);
        }

        return projects;
    }
}
