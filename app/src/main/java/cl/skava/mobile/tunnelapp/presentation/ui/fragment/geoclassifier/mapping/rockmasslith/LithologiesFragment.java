package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rockmasslith;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Lithology;

/**
 * Created by Jose Ignacio Vera on 30-06-2016.
 */
public class LithologiesFragment extends Fragment {

    private static final String TAG = "LithologiesFragment";

    private TabLayout tabLayout;

    private List<Lithology> mappingInputs;

    private LithologyFragment currentTabFragment;

    private Boolean mappingFinished;

    private int NUM_INPUTS;

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;

    private int listPosition;
    private int lastPosition;

    private List<LithologyFragment> lithInputFragments;

    private Boolean firstTimeLoadSlide = true;

    private List<FormationUnit> formationUnits;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_rockmassl_1, container, false);
        rootView.setTag(TAG);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        NUM_INPUTS = mappingInputs.size();

        lithInputFragments = new ArrayList<>();
        for(int i = 0; i < mappingInputs.size(); i++) {
            LithologyFragment lfg = new LithologyFragment();
            lfg.setMappingInput(findLithologyByPosition(i));
            lfg.setMappingFinished(mappingFinished);
            lfg.setFormationUnits(formationUnits);
            lithInputFragments.add(lfg);
            tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.characterization_mapping_input_lith_label_lithology) + " " + (i + 1)));
        }

        mPager = (ViewPager) rootView.findViewById(R.id.lith_input_content);
        mPagerAdapter = new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager(), lithInputFragments);
        mPager.setAdapter(mPagerAdapter);

        mPager.setCurrentItem(0);
        currentTabFragment = ((ScreenSlidePagerAdapter) mPager.getAdapter()).getInputFragments(mPager.getCurrentItem());


        mPager.addOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        Log.i(TAG, "onPageSelected con position: " + position);
                        TabLayout.Tab tab = tabLayout.getTabAt(position);
                        tab.select();
                        Log.i(TAG, "Selecciono el tab " + position);
                        if (!firstTimeLoadSlide) {
                            if (currentTabFragment.getView() != null)
                                Log.i(TAG, "llamo a saveLithology");
                                saveLithologyData(listPosition);
                        } else {
                            firstTimeLoadSlide = false;
                        }
                        lastPosition = listPosition;
                        listPosition = position;
                        currentTabFragment = ((ScreenSlidePagerAdapter) mPager.getAdapter()).getInputFragments(listPosition);
                        Log.i(TAG, "Llamo a getPresenceValues");
                        getPresenceValues();
                    }
                });

        mPager.setOffscreenPageLimit(3);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        return rootView;
    }

    private void getPresenceValues()
    {
        //Log.i(TAG,"lastPosition fue: " + lastPosition);
        Lithology lith = currentTabFragment.getMappingInput();
        Float sumVar = 0f;
        Float sumDenominador = 0f;
        Float newN = 0f;
        //lith.setPresence(percentage);
        Log.i(TAG, "-------------------------------------------------------------");
        for(Lithology l : mappingInputs) {
            //Log.i(TAG, "Evalúo posición del TAB: " + lith.getPosition().intValue() + " contra la del for: " + l.getPosition().intValue());
            //Log.i(TAG, "Valor de SumVar: " + sumVar);
            LithologyFragment tabFragAux = ((ScreenSlidePagerAdapter) mPager.getAdapter()).getInputFragments(l.getPosition().intValue());
            EditText inputPresence = ((EditText) tabFragAux.getView().findViewById(R.id.input_presence));
            Float percentage = inputPresence.getText().toString().isEmpty() ? 0 : Float.parseFloat(inputPresence.getText().toString());
            Log.i(TAG, "Valor de presencia: " + percentage);

            //Log.i(TAG, "Nuevo Valor de SumVar: " + sumVar);
            if (l.getPosition().intValue() != lastPosition) {
                //Log.i(TAG, "Sumo: " + (l.getPresence().isNaN() ? "0" : l.getPresence().toString()) + " a sumDenominador");
                sumDenominador += percentage;
                //Log.i(TAG, "Ahora sumDenominador vale: " + sumDenominador);
            }
            else if(percentage > 0)
            {
                Log.i(TAG, "Estoy en la Lithology anterior");
                newN = 100 - percentage;
                Log.i(TAG, "El valor de newN es " + newN);
                if(newN < 0)
                {
                    newN = 0f;
                    percentage = 100f;
                    lith.setPresence(percentage);
                    inputPresence.setText("100");
                }
            }
            sumVar += percentage;
        }
        Log.i(TAG, "El valor de newN es " + newN);
        if(sumVar > 100) {
            for (Lithology l : mappingInputs) {
                if (l.getPosition().intValue() != lastPosition) {
                    Log.i(TAG, "El valor presence es: " + l.getPresence().toString());
                    Float nuevo_valor = (newN * l.getPresence()) / sumDenominador;
                    if (nuevo_valor.isNaN()) {
                        nuevo_valor = 0f;
                    }
                    Log.i(TAG, "El valor proporcional es : " + nuevo_valor.toString() + " y lo asigno");
                    l.setPresence(nuevo_valor);
                    EditText inputPresence = ((EditText) ((ScreenSlidePagerAdapter) mPager.getAdapter()).getInputFragments(l.getPosition().intValue()).getView().findViewById(R.id.input_presence));
                    inputPresence.setText(nuevo_valor.toString());
                }
            }
        }
        // Log.i(TAG, "SETEO LA PRESENCE EN " + percentage);
    }

    private Lithology saveLithologyData(int tabPosition) {
        EditText inputPresence = ((EditText) currentTabFragment.getView().findViewById(R.id.input_presence));
        Spinner spinnerStrength = (Spinner) currentTabFragment.getView().findViewById(R.id.spinner_strength);
        Spinner spinnerColourValue = (Spinner) currentTabFragment.getView().findViewById(R.id.spinner_colour_value);
        Spinner spinnerColourChroma = (Spinner) currentTabFragment.getView().findViewById(R.id.spinner_colour_chroma);
        Spinner spinnerColourHue = (Spinner) currentTabFragment.getView().findViewById(R.id.spinner_colour_hue);
        Spinner spinnerTexture = (Spinner) currentTabFragment.getView().findViewById(R.id.spinner_texture);
        Spinner spinnerAlterationWeathering = (Spinner) currentTabFragment.getView().findViewById(R.id.spinner_alteration_w);
        Spinner spinnerGrainSize = (Spinner) currentTabFragment.getView().findViewById(R.id.spinner_grain_size);
        Spinner spinnerGeneticGroup = (Spinner) currentTabFragment.getView().findViewById(R.id.spinner_genetic_group);
        Spinner spinnerSubGroup = (Spinner) currentTabFragment.getView().findViewById(R.id.spinner_rock_name);
        EditText inputJvMin = ((EditText) currentTabFragment.getView().findViewById(R.id.input_jv_min));
        EditText inputJvMax = ((EditText) currentTabFragment.getView().findViewById(R.id.input_jv_max));
        Spinner spinnerBlockShape = (Spinner) currentTabFragment.getView().findViewById(R.id.spinner_block_shape);
        EditText inputBlockShapeL = ((EditText) currentTabFragment.getView().findViewById(R.id.input_block_shape_l));
        EditText inputBlockShapeW = ((EditText) currentTabFragment.getView().findViewById(R.id.input_block_shape_w));
        EditText inputBlockShapeH = ((EditText) currentTabFragment.getView().findViewById(R.id.input_block_shape_h));
        Spinner spinnerFormationUnit = (Spinner) currentTabFragment.getView().findViewById(R.id.spinner_formation);

        Lithology lith = currentTabFragment.getMappingInput();
        for(Lithology l : mappingInputs)
        {
            if(l.getPosition().intValue() == lith.getPosition().intValue()) {
                lith = l;
                break;
            }
        }
        Float percentage = inputPresence.getText().toString().isEmpty() ? 0 : Float.parseFloat(inputPresence.getText().toString());
        lith.setPresence(percentage);
        lith.setStrength(spinnerStrength.getSelectedItemPosition());
        lith.setColourValue(spinnerColourValue.getSelectedItemPosition());
        lith.setColourChroma(spinnerColourChroma.getSelectedItemPosition());
        lith.setColorHue(spinnerColourHue.getSelectedItemPosition());
        lith.setTexture(spinnerTexture.getSelectedItemPosition());
        lith.setAlterationWeathering(spinnerAlterationWeathering.getSelectedItemPosition());
        lith.setGrainSize(spinnerGrainSize.getSelectedItemPosition());
        lith.setGeneticGroup(spinnerGeneticGroup.getSelectedItemPosition());
        lith.setSubGroup(spinnerSubGroup.getSelectedItemPosition());
        lith.setJvMin(Integer.parseInt(inputJvMin.getText().toString()));
        lith.setJvMax(Integer.parseInt(inputJvMax.getText().toString()));
        lith.setBlockShape(spinnerBlockShape.getSelectedItemPosition());
        lith.setBlockSizeOne(Float.parseFloat(inputBlockShapeL.getText().toString()));
        lith.setBlockSizeTwo(Float.parseFloat(inputBlockShapeW.getText().toString()));
        lith.setBlockSizeThree(Float.parseFloat(inputBlockShapeH.getText().toString()));
        lith.setFormationUnit(spinnerFormationUnit.getSelectedItemPosition());
        lith.setCompleted(!inputPresence.getText().toString().trim().equals("")
                && spinnerStrength.getSelectedItemPosition() != 0
                && spinnerColourValue.getSelectedItemPosition() != 0
                && spinnerColourChroma.getSelectedItemPosition() != 0
                && spinnerColourHue.getSelectedItemPosition() != 0
                && spinnerTexture.getSelectedItemPosition() != 0
                && spinnerAlterationWeathering.getSelectedItemPosition() != 0
                && spinnerGrainSize.getSelectedItemPosition() != 0
                && spinnerGeneticGroup.getSelectedItemPosition() != 0
                && spinnerSubGroup.getSelectedItemPosition() != 0
                && spinnerBlockShape.getSelectedItemPosition() != 0
                && !inputJvMin.getText().toString().trim().equals("")
                && !inputJvMax.getText().toString().trim().equals("")
                && !inputBlockShapeL.getText().toString().trim().equals("")
                && !inputBlockShapeW.getText().toString().trim().equals("")
                && !inputBlockShapeH.getText().toString().trim().equals("")
                && spinnerFormationUnit.getSelectedItemPosition() != 0);
        lith.setBriefDesc(currentTabFragment.getBriefDesc().getText().toString());

        mappingInputs.set(lith.getPosition(), lith);

//        for(Lithology l : mappingInputs) {
//            if(l.getPosition().intValue() == tabPosition) {
//                lith = l;
//                break;
//            }
//        }

        return findLithologyByPosition(tabPosition);
    }

    private Lithology findLithologyByPosition(int position) {
        Lithology result = null;

        for(Lithology l : mappingInputs) {
            if(l.getPosition().intValue() == position) {
                result = l;
                break;
            }
        }

        return result;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMappingInputs(List<Lithology> mappingInputs) {
        this.mappingInputs = mappingInputs;
    }

    public List<Lithology> getMappingInputs() {
        return mappingInputs;
    }

    public LithologyFragment getCurrentTabFragment() {
        return currentTabFragment;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public void setFormationUnits(List<FormationUnit> formationUnits) {
        this.formationUnits = formationUnits;
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        private List<LithologyFragment> mInputFragments;

        public ScreenSlidePagerAdapter(FragmentManager fm, List<LithologyFragment> inputFragments) {
            super(fm);
            mInputFragments = inputFragments;
        }

        @Override
        public Fragment getItem(int position) {
            LithologyFragment fragment = mInputFragments.get(position);
            return fragment;
        }

        @Override
        public int getCount() {
            return NUM_INPUTS;
        }

        public LithologyFragment getInputFragments(int position) {
            return mInputFragments.get(position);
        }
    }
}
