package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.rockmassdescription;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Lithology;

/**
 * Created by Jose Ignacio Vera on 20-10-2016.
 */
public class LithologiesFragment extends Fragment {

    private List<Lithology> lithologyList;
    private List<String[]> rockTypes;

    private String[] array1;
    private String[] array2;
    private String[] array3;
    private String[] array4;
    private String[] array5;
    private String[] array6;
    private String[] array7;
    private String[] array8;
    private String[] array9;
    private String[] array10;

    private List<TextView[]> fields = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_summary_lithologies, container, false);

        array1 = getResources().getStringArray(R.array.colour_chroma_dropdown_arrays);
        array2 = getResources().getStringArray(R.array.genetic_group_dropdown_arrays);
        array3 = getResources().getStringArray(R.array.colour_hue_dropdown_arrays);
        array4 = getResources().getStringArray(R.array.null_dropdown_arrays);
        array5 = getResources().getStringArray(R.array.strength_dropdown_arrays);
        array6 = getResources().getStringArray(R.array.texture_dropdown_arrays);
        array7 = getResources().getStringArray(R.array.weathering_dropdown_arrays);
        array8 = getResources().getStringArray(R.array.grain_size_dropdown_arrays);
        array9 = getResources().getStringArray(R.array.colour_value_dropdown_arrays);
        array10 = getResources().getStringArray(R.array.block_shape_2_dropdown_arrays);

        rockTypes = new ArrayList<>(6);
        rockTypes.add(0, getResources().getStringArray(R.array.null_dropdown_arrays));
        rockTypes.add(1, getResources().getStringArray(R.array.igneous_plutonic_dropdown_arrays));
        rockTypes.add(2, getResources().getStringArray(R.array.igneous_volcanic_dropdown_arrays));
        rockTypes.add(3, getResources().getStringArray(R.array.pyroclastic_dropdown_arrays));
        rockTypes.add(4, getResources().getStringArray(R.array.sedimentary_clastic_dropdown_arrays));
        rockTypes.add(5, getResources().getStringArray(R.array.sedimentary_chemical_dropdown_arrays));
        rockTypes.add(6, getResources().getStringArray(R.array.metamorphic_dropdown_arrays));

        TextView[] fields1 = new TextView[15];
        fields1[14] = (TextView) rootView.findViewById(R.id.text01);
        fields1[0] = (TextView) rootView.findViewById(R.id.array001);
        fields1[1] = (TextView) rootView.findViewById(R.id.array002);
        fields1[2] = (TextView) rootView.findViewById(R.id.array003);
        fields1[3] = (TextView) rootView.findViewById(R.id.array004);
        fields1[4] = (TextView) rootView.findViewById(R.id.array005);
        fields1[5] = (TextView) rootView.findViewById(R.id.array006);
        fields1[6] = (TextView) rootView.findViewById(R.id.array007);
        fields1[7] = (TextView) rootView.findViewById(R.id.array008);
        fields1[8] = (TextView) rootView.findViewById(R.id.array009);
        fields1[9] = (TextView) rootView.findViewById(R.id.array0010);
        fields1[10] = (TextView) rootView.findViewById(R.id.array0011);
        fields1[11] = (TextView) rootView.findViewById(R.id.array0012);
        fields1[12] = (TextView) rootView.findViewById(R.id.block001);
        fields1[13] = (TextView) rootView.findViewById(R.id.block002);
        fields.add(fields1);

        fields1 = new TextView[15];
        fields1[14] = (TextView) rootView.findViewById(R.id.text11);
        fields1[0] = (TextView) rootView.findViewById(R.id.array101);
        fields1[1] = (TextView) rootView.findViewById(R.id.array102);
        fields1[2] = (TextView) rootView.findViewById(R.id.array103);
        fields1[3] = (TextView) rootView.findViewById(R.id.array104);
        fields1[4] = (TextView) rootView.findViewById(R.id.array105);
        fields1[5] = (TextView) rootView.findViewById(R.id.array106);
        fields1[6] = (TextView) rootView.findViewById(R.id.array107);
        fields1[7] = (TextView) rootView.findViewById(R.id.array108);
        fields1[8] = (TextView) rootView.findViewById(R.id.array109);
        fields1[9] = (TextView) rootView.findViewById(R.id.array1010);
        fields1[10] = (TextView) rootView.findViewById(R.id.array1011);
        fields1[11] = (TextView) rootView.findViewById(R.id.array1012);
        fields1[12] = (TextView) rootView.findViewById(R.id.block101);
        fields1[13] = (TextView) rootView.findViewById(R.id.block102);
        fields.add(fields1);

        fields1 = new TextView[15];
        fields1[14] = (TextView) rootView.findViewById(R.id.text21);
        fields1[0] = (TextView) rootView.findViewById(R.id.array201);
        fields1[1] = (TextView) rootView.findViewById(R.id.array202);
        fields1[2] = (TextView) rootView.findViewById(R.id.array203);
        fields1[3] = (TextView) rootView.findViewById(R.id.array204);
        fields1[4] = (TextView) rootView.findViewById(R.id.array205);
        fields1[5] = (TextView) rootView.findViewById(R.id.array206);
        fields1[6] = (TextView) rootView.findViewById(R.id.array207);
        fields1[7] = (TextView) rootView.findViewById(R.id.array208);
        fields1[8] = (TextView) rootView.findViewById(R.id.array209);
        fields1[9] = (TextView) rootView.findViewById(R.id.array2010);
        fields1[10] = (TextView) rootView.findViewById(R.id.array2011);
        fields1[11] = (TextView) rootView.findViewById(R.id.array2012);
        fields1[12] = (TextView) rootView.findViewById(R.id.block201);
        fields1[13] = (TextView) rootView.findViewById(R.id.block202);
        fields.add(fields1);

        ImageView[] imgs = new ImageView[3];
        imgs[0] = ((ImageView) rootView.findViewById(R.id.block003));
        imgs[1] = ((ImageView) rootView.findViewById(R.id.block103));
        imgs[2] = ((ImageView) rootView.findViewById(R.id.block203));

        for(Lithology lith : lithologyList) {
            setFields(fields.get(lith.getPosition()), lith);
            enablePicture(imgs[lith.getPosition()], lith);
        }

        return rootView;
    }

    private void setFields(TextView[] fields1, Lithology lith) {
        fields1[14].setText(lith.getBriefDesc());
        fields1[0].setText(lith.getPresence().toString() + "%");
        fields1[1].setText(lith.getGeneticGroup() == 0 ? "N/A" : array2[lith.getGeneticGroup()]);
        fields1[2].setText(lith.getSubGroup() == 0 ? "N/A" : rockTypes.get(lith.getGeneticGroup()-1)[lith.getSubGroup()]);
        fields1[3].setText("N/A");
        fields1[4].setText(lith.getAlterationWeathering() == 0 ? "N/A" : array7[lith.getAlterationWeathering()]);
        fields1[5].setText(lith.getColourValue() == 0 ? "N/A" : array9[lith.getColourValue()]);
        fields1[6].setText(lith.getColourChroma() == 0 ? "N/A" : array1[lith.getColourChroma()]);
        fields1[7].setText(lith.getColorHue() == 0 ? "N/A" : array3[lith.getColorHue()]);
        fields1[8].setText(lith.getStrength() == 0 ? "N/A" : array5[lith.getStrength()]);
        fields1[9].setText(lith.getTexture() == 0 ? "N/A" : array6[lith.getTexture()]);
        fields1[10].setText(lith.getGrainSize() == 0 ? "N/A" : array8[lith.getGrainSize()]);
        fields1[11].setText(lith.getJvMin() + " - " + lith.getJvMax());
        fields1[12].setText(lith.getBlockSizeOne() + " [m] x " + lith.getBlockSizeTwo() + " [m] x " + lith.getBlockSizeThree() + " [m]");
        fields1[13].setText(lith.getBlockShape() == 0 ? "N/A" : array10[lith.getBlockShape()]);
    }

    private void enablePicture(ImageView img, Lithology lith) {
        if(lith.getBlockShape() == 0) {
            img.setVisibility(View.GONE);
        }
        else {
            switch(lith.getBlockShape()) {
                case 1:
                    img.setImageResource(R.drawable.lith_polyhedral);
                    break;
                case 2:
                    img.setImageResource(R.drawable.lith_tabular);
                    break;
                case 3:
                    img.setImageResource(R.drawable.lith_prismatic);
                    break;
                case 4:
                    img.setImageResource(R.drawable.lith_equidimensional);
                    break;
                case 5:
                    img.setImageResource(R.drawable.lith_rhomboidal);
                    break;
                case 6:
                    img.setImageResource(R.drawable.lith_columnar);
                    break;
            }
        }
    }

    public void setLithologyList(List<Lithology> lithologyList) {
        this.lithologyList = lithologyList;
    }
}
