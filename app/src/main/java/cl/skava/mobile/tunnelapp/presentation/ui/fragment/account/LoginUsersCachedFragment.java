package cl.skava.mobile.tunnelapp.presentation.ui.fragment.account;

        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.support.v7.widget.LinearLayoutManager;
        import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.view.WindowManager;
        import android.widget.Button;
        import android.widget.EditText;

        import java.util.List;

        import cl.skava.mobile.tunnelapp.R;
        import cl.skava.mobile.tunnelapp.domain.model.account.User;
        import cl.skava.mobile.tunnelapp.presentation.ui.adapters.account.UserCacheListAdapter;

/**
 * Created by Jose Ignacio Vera on 25-08-2016.
 */
public class LoginUsersCachedFragment extends Fragment {

    private static final String TAG = "LoginUsersCachedFragment";

    protected RecyclerView mRecyclerView;
    protected UserCacheListAdapter mAdapter;

    protected RecyclerView.LayoutManager mLayoutManager;

    private Button _loginButton;

    private OnUserDataListener mCallback;

    public interface OnUserDataListener {
        void onLoginAccessCache(User user);
        void goToNewLogin();
    }

    private List<User> mUserDataSet;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCallback = (OnUserDataListener) getActivity();

        if (savedInstanceState == null) {
            initDatasources();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login_cached_user, container, false);
        rootView.setTag(TAG);

        _loginButton = (Button) rootView.findViewById(R.id.btn_login_new_user);

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mCallback.goToNewLogin();
            }
        });

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        setRecyclerViewLayoutManager();
        mAdapter = new UserCacheListAdapter(mUserDataSet,mCallback, mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setRecyclerViewLayoutManager() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void initDatasources() {
    }

    public void login() {
//        mCallback.onLoginAccess(user);
    }

    public void setmUserDataSet(List<User> mUserDataSet) {
        this.mUserDataSet = mUserDataSet;
    }
}
