package cl.skava.mobile.tunnelapp.presentation.presenters.interfaces;

import android.content.Context;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.presentation.presenters.base.BasePresenter;
import cl.skava.mobile.tunnelapp.presentation.ui.base.BaseView;

/**
 * Created by Jose Ignacio Vera on 15-06-2016.
 */
public interface MainPresenter extends BasePresenter {

    interface View extends BaseView {
        void setupModuleDataset(List<Module> modules);
        void syncUserDataResult(int code);
        void onSyncProgressPercentaje(Integer percentaje);
        void onNetworkErrorUI(String msg);
        void notifySyncSegment(String msg);
    }

    // Add your presenter methods
    void syncUserData(Context activity, boolean isNetworkAvailable, User user);
    void onSyncResult(int code);
    void setSyncProgressPercentaje(Integer percentaje);
    void onNetworkError(String msg);
    void notifySyncSegment(String msg);
    void syncBasicData(Context activity, boolean isNetworkAvailable, User user);
}
