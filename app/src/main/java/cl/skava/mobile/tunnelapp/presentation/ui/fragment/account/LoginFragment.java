package cl.skava.mobile.tunnelapp.presentation.ui.fragment.account;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.presentation.ui.activity.account.LoginActivity;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.mapping.expandedtunnel.ExpandedViewAdapter;

/**
 * Created by Jose Ignacio Vera on 30-05-2016.
 */
public class LoginFragment extends Fragment {

    private static final String TAG = "LoginFragment";

    private Button _loginButton;
    private Button _usersLoggedButton;
    EditText _emailText;
    EditText _passwordText;

    private ProgressDialog progressDialog;

    private OnUserDataListener mCallback;
    private boolean writePermissionGranted;

    public interface OnUserDataListener {
        void onLoginAccess(User user);
        void onLoginFailed();
        void goToCacheLogin();
    }

    private boolean existCache;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCallback = (OnUserDataListener) getActivity();

        if (savedInstanceState == null) {
            initDatasources();
        }
        updateWritePermissionGranted();
        //this.writePermissionGranted = ActivityCompat.checkSelfPermission(this.getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void updateWritePermissionGranted() {
        LoginActivity parentActivity = (LoginActivity) getActivity();
        writePermissionGranted = parentActivity.isWritePermissionGranted();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login_new_user, container, false);
        rootView.setTag(TAG);

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        _emailText = (EditText) rootView.findViewById(R.id.input_email);
        _passwordText = (EditText) rootView.findViewById(R.id.input_password);

        _loginButton = (Button) rootView.findViewById(R.id.btn_login);

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkPermissionsGranted();
                if (writePermissionGranted) {
                    login();
                }
            }
        });

        _usersLoggedButton = (Button) rootView.findViewById(R.id.btn_users_logged);

        _usersLoggedButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkPermissionsGranted();
                if (writePermissionGranted) {
                    mCallback.goToCacheLogin();
                }
            }
        });

        if(!existCache) {
            _usersLoggedButton.setVisibility(View.GONE);
        }
        else {
            _usersLoggedButton.setVisibility(View.VISIBLE);
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void initDatasources() {
//        mPresenter = new HomePresenterImpl(
//                ThreadExecutor.getInstance(),
//                MainThreadImpl.getInstance(),
//                this,
//                getActivity()
//        );
//        mProjectDataset = new ArrayList<>();
//        mTunnelDataset = new ArrayList<>();
//        mFacesDataset = new ArrayList<>();
    }

    public void checkPermissionsGranted() {
        updateWritePermissionGranted();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !writePermissionGranted) {
            getActivity().requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            mCallback.onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        User user = new User();
        user.setUserName(email);
        user.setPassword(password);

        mCallback.onLoginAccess(user);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

//        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
        if (email.isEmpty()) {
//            _emailText.setError("enter a valid email address");
            _emailText.setError(getResources().getString(R.string.account_label_username_valid));
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 20) {
            _passwordText.setError(getResources().getString(R.string.account_label_between_characters));
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    public void enableLoginButton() {
        _loginButton.setEnabled(true);
    }

    public void setExistCache(boolean existCache) {
        this.existCache = existCache;
    }
}
