package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.rockmassdescription;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Discontinuity;

/**
 * Created by Jose Ignacio Vera on 20-10-2016.
 */
public class DiscontinuitiesFragment extends Fragment {

    private List<Discontinuity> discontinuities;

    private List<TextView[]> fields = new ArrayList<>();

    private String[] array1;
    private String[] array2;
    private String[] array3;
    private String[] array4;
    private String[] array5;
    private String[] array6;
    private String[] array7;
    private String[] array8;
    private String[] array9;
    private String[] array10;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_summary_discontinuities, container, false);

        array1 = getResources().getStringArray(R.array.type_dropdown_arrays);
        array2 = getResources().getStringArray(R.array.opening_dropdown_arrays);
        array3 = getResources().getStringArray(R.array.relevance_dropdown_arrays);
        array4 = getResources().getStringArray(R.array.roughness_dropdown_arrays);
        array5 = getResources().getStringArray(R.array.yes_no_dropdown_arrays);
        array6 = getResources().getStringArray(R.array.infilling_dropdown_arrays);
        array7 = getResources().getStringArray(R.array.spacing_dropdown_arrays);
        array8 = getResources().getStringArray(R.array.infilling_type_dropdown_arrays);
        array9 = getResources().getStringArray(R.array.persistence_dropdown_arrays);
        array10 = getResources().getStringArray(R.array.weathering_dropdown_arrays);

        TextView[] fields1 = new TextView[15];
        fields1[0] = (TextView) rootView.findViewById(R.id.array001);
        fields1[1] = (TextView) rootView.findViewById(R.id.array002);
        fields1[2] = (TextView) rootView.findViewById(R.id.array003);
        fields1[3] = (TextView) rootView.findViewById(R.id.array004);
        fields1[4] = (TextView) rootView.findViewById(R.id.array005);
        fields1[5] = (TextView) rootView.findViewById(R.id.array006);
        fields1[6] = (TextView) rootView.findViewById(R.id.array007);
        fields1[7] = (TextView) rootView.findViewById(R.id.array008);
        fields1[8] = (TextView) rootView.findViewById(R.id.array009);
        fields1[9] = (TextView) rootView.findViewById(R.id.array0010);
        fields1[10] = (TextView) rootView.findViewById(R.id.array0011);
        fields1[11] = (TextView) rootView.findViewById(R.id.array0012);
        fields.add(fields1);

        fields1 = new TextView[15];
        fields1[0] = (TextView) rootView.findViewById(R.id.array101);
        fields1[1] = (TextView) rootView.findViewById(R.id.array102);
        fields1[2] = (TextView) rootView.findViewById(R.id.array103);
        fields1[3] = (TextView) rootView.findViewById(R.id.array104);
        fields1[4] = (TextView) rootView.findViewById(R.id.array105);
        fields1[5] = (TextView) rootView.findViewById(R.id.array106);
        fields1[6] = (TextView) rootView.findViewById(R.id.array107);
        fields1[7] = (TextView) rootView.findViewById(R.id.array108);
        fields1[8] = (TextView) rootView.findViewById(R.id.array109);
        fields1[9] = (TextView) rootView.findViewById(R.id.array1010);
        fields1[10] = (TextView) rootView.findViewById(R.id.array1011);
        fields1[11] = (TextView) rootView.findViewById(R.id.array1012);
        fields.add(fields1);

        fields1 = new TextView[15];
        fields1[0] = (TextView) rootView.findViewById(R.id.array201);
        fields1[1] = (TextView) rootView.findViewById(R.id.array202);
        fields1[2] = (TextView) rootView.findViewById(R.id.array203);
        fields1[3] = (TextView) rootView.findViewById(R.id.array204);
        fields1[4] = (TextView) rootView.findViewById(R.id.array205);
        fields1[5] = (TextView) rootView.findViewById(R.id.array206);
        fields1[6] = (TextView) rootView.findViewById(R.id.array207);
        fields1[7] = (TextView) rootView.findViewById(R.id.array208);
        fields1[8] = (TextView) rootView.findViewById(R.id.array209);
        fields1[9] = (TextView) rootView.findViewById(R.id.array2010);
        fields1[10] = (TextView) rootView.findViewById(R.id.array2011);
        fields1[11] = (TextView) rootView.findViewById(R.id.array2012);
        fields.add(fields1);

        fields1 = new TextView[15];
        fields1[0] = (TextView) rootView.findViewById(R.id.array301);
        fields1[1] = (TextView) rootView.findViewById(R.id.array302);
        fields1[2] = (TextView) rootView.findViewById(R.id.array303);
        fields1[3] = (TextView) rootView.findViewById(R.id.array304);
        fields1[4] = (TextView) rootView.findViewById(R.id.array305);
        fields1[5] = (TextView) rootView.findViewById(R.id.array306);
        fields1[6] = (TextView) rootView.findViewById(R.id.array307);
        fields1[7] = (TextView) rootView.findViewById(R.id.array308);
        fields1[8] = (TextView) rootView.findViewById(R.id.array309);
        fields1[9] = (TextView) rootView.findViewById(R.id.array3010);
        fields1[10] = (TextView) rootView.findViewById(R.id.array3011);
        fields1[11] = (TextView) rootView.findViewById(R.id.array3012);
        fields.add(fields1);

        fields1 = new TextView[15];
        fields1[0] = (TextView) rootView.findViewById(R.id.array401);
        fields1[1] = (TextView) rootView.findViewById(R.id.array402);
        fields1[2] = (TextView) rootView.findViewById(R.id.array403);
        fields1[3] = (TextView) rootView.findViewById(R.id.array404);
        fields1[4] = (TextView) rootView.findViewById(R.id.array405);
        fields1[5] = (TextView) rootView.findViewById(R.id.array406);
        fields1[6] = (TextView) rootView.findViewById(R.id.array407);
        fields1[7] = (TextView) rootView.findViewById(R.id.array408);
        fields1[8] = (TextView) rootView.findViewById(R.id.array409);
        fields1[9] = (TextView) rootView.findViewById(R.id.array4010);
        fields1[10] = (TextView) rootView.findViewById(R.id.array4011);
        fields1[11] = (TextView) rootView.findViewById(R.id.array4012);
        fields.add(fields1);

        for(Discontinuity discontinuity : discontinuities) {
            setFields(fields.get(discontinuity.getPosition()), discontinuity);
        }

        return rootView;
    }

    private void setFields(TextView[] fields1, Discontinuity discontinuity) {

        fields1[0].setText(discontinuity.getType() == 0 ? "N/A" : array1[discontinuity.getType()]);
        fields1[1].setText(discontinuity.getRelevance() == 0 ? "N/A" : array3[discontinuity.getRelevance()]);
        fields1[2].setText(discontinuity.getOrientationDd() + " [°]");
        fields1[3].setText(discontinuity.getOrientationD() + " [°]");
        fields1[4].setText(discontinuity.getSpacing() == 0 ? "N/A" : array7[discontinuity.getSpacing()]);
        fields1[5].setText(discontinuity.getPersistence() == 0 ? "N/A" : array9[discontinuity.getPersistence()]);
        fields1[6].setText(discontinuity.getOpening() == 0 ? "N/A" : array2[discontinuity.getOpening()]);
        fields1[7].setText(discontinuity.getRoughness() == 0 ? "N/A" : array4[discontinuity.getRoughness()]);
        fields1[8].setText(discontinuity.getSlickensided() == 0 ? "N/A" : array5[discontinuity.getSlickensided()]);
        fields1[9].setText(discontinuity.getInfilling() == 0 ? "N/A" : array6[discontinuity.getInfilling()]);
        fields1[10].setText(discontinuity.getInfillingType() == 0 ? "N/A" : array8[discontinuity.getInfillingType()]);
        fields1[11].setText(discontinuity.getWeathering() == 0 ? "N/A" : array10[discontinuity.getWeathering()]);

    }

    public void setDiscontinuities(List<Discontinuity> discontinuities) {
        this.discontinuities = discontinuities;
    }
}
