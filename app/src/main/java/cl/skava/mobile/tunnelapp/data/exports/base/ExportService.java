package cl.skava.mobile.tunnelapp.data.exports.base;

import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SelectionDetailLists;

/**
 * Created by Jose Ignacio Vera on 24-10-2016.
 */
public interface ExportService {
    boolean exportCharacterizerData(Project p, Tunnel t, Face f, String directoryPath, SelectionDetailLists selectionDetailLists);
    boolean exportForecastingData(Project p, Tunnel t, Face f, String directoryPath);
    boolean exportAll();
}
