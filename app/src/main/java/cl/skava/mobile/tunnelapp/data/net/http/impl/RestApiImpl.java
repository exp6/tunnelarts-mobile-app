package cl.skava.mobile.tunnelapp.data.net.http.impl;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;

import cl.skava.mobile.tunnelapp.data.net.http.base.RestApi;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.account.LoginInteractor;
import cl.skava.mobile.tunnelapp.domain.model.account.User;

/**
 * Created by Jose Ignacio Vera on 27-07-2016.
 */
public class RestApiImpl implements RestApi {

    private LoginInteractor mCallback;
    private User mUser;

    public RestApiImpl(LoginInteractor callback) {
        mCallback = callback;
    }

    @Override
    public void validateUserFromApi(User user) throws MalformedURLException {
        mUser = user;
        JsonObject json = new JsonObject();
        json.addProperty("username", user.getUserName());
        json.addProperty("password", user.getPassword());
        String jsonString = json.toString();
        ApiConnection.createGET(RestApi.API_URL_LOGIN_SERVICE).requestSyncCallPOST(jsonString, this);
    }

    @Override
    public void onValidationResult(User user) {
        mCallback.onRestValidation(validateUser(user));
    }

    public User getmUser() {
        return mUser;
    }

    public void setmUser(User mUser) {
        this.mUser = mUser;
    }

    private User validateUser(User user) {
        String data = user.getData();

        try {
            if(data!=null) {
                JSONObject json = new JSONObject(data);
                user.setId(json.getString("IdUser"));
                user.setIdClient(json.getString("IdClient"));
                user.setFirstName(json.getString("FirstName"));
                user.setLastName(json.getString("LastName"));
                user.setContact(json.getString("Contact"));
                user.setEmail(json.getString("Email"));
                JSONArray roles = json.getJSONArray("Roles");

                boolean isGeologist = false;
                for(int i = 0 ; i < roles.length() ; i++){
                    JSONObject p = (JSONObject)roles.get(i);
                    String roleName = p.getString("Name");
                    if(roleName.trim().equalsIgnoreCase("Geologist")) {
                        isGeologist = true;
                        break;
                    }
                }

                return ((user.getIdClient().isEmpty() || !isGeologist) ? null : user);
            }
            else {
                return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
