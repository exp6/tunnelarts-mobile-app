package cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.MappingInputListItemsFragment;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public class MappingInputListAdapter extends RecyclerView.Adapter<MappingInputListAdapter.ViewHolder> {

    private static final String TAG = "MappingInputListAdapter";
    protected List<MappingInputModule> mMappingInputModuleDataset;
    private MappingInputListItemsFragment.OnModuleItemListener mCallback;

    private int selectedItem = 0;
    ViewHolder viewHolderPool[];
    protected RecyclerView mRecyclerView;

    private Mapping selectedMapping;

    private int mListOrder;

    public MappingInputListAdapter(List<MappingInputModule> dataSet,
                                   MappingInputListItemsFragment.OnModuleItemListener callback,
                                   RecyclerView recyclerView,
                                   Mapping mapping,
                                   int position,
                                   int listOrder) {
        mMappingInputModuleDataset = dataSet;
        mCallback = callback;
        viewHolderPool = new ViewHolder[mMappingInputModuleDataset.size()];
        mRecyclerView = recyclerView;
        setHasStableIds(true);
        selectedMapping = mapping;
        selectedItem = position;
        mListOrder = listOrder;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedItem = getLayoutPosition();
                    mCallback.onModuleItemSelected(selectedItem, mListOrder);

                    notifyDataSetChanged();
                    mRecyclerView.smoothScrollToPosition(selectedItem);
                }
            });

            textView = (TextView) v.findViewById(R.id.pk_input_label);
        }

        public TextView getTextView() {
            return textView;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_geoclassifier_mapping_inputs_row_item, parent, false);
        holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MappingInputModule mappingInputModule = mMappingInputModuleDataset.get(position);
        holder.getTextView().setText(mappingInputModule.getName());

        ImageView lock = ((ImageView) holder.itemView.findViewById(R.id.input_status));

        if(mappingInputModule.getCompleted()) lock.setImageResource(R.drawable.ic_check_box);

        if (viewHolderPool[position] == null)
            viewHolderPool[position] = holder;

        holder.itemView.setSelected(selectedItem == position);
        holder.itemView.setBackgroundColor(holder.itemView.isSelected() ? Color.LTGRAY : Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return mMappingInputModuleDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
