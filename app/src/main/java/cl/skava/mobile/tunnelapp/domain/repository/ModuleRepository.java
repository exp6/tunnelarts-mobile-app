package cl.skava.mobile.tunnelapp.domain.repository;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.Module;

/**
 * Created by Jose Ignacio Vera on 24-06-2016.
 */
public interface ModuleRepository {

    List<Module> getUserModules();
}
