package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by Jose Ignacio Vera on 08-08-2016.
 */
public class FailureZone extends DataInput {

    private String id;
    private Integer senseOfMovement;
    private Float orientationDd;
    private Float orientationD;
    private Float thickness;
    private Float matrixBlock;
    private Integer matrixColourValue;
    private Integer matrixColourChroma;
    private Integer matrixColourHue;
    private Integer matrixGainSize;
    private Integer blockSize;
    private Integer blockShape;
    private Integer blockGeneticGroup;
    private Integer blockSubGroup;
    private String idMapping;
    private Boolean completed;
    private Float rakeOfStriae;
    private Boolean noneRake;

    public FailureZone(String id, Integer senseOfMovement, Float orientationDd, Float orientationD, Float thickness, Float matrixBlock, Integer matrixColourValue, Integer matrixColourChroma, Integer matrixColourHue, Integer matrixGainSize, Integer blockSize, Integer blockShape, Integer blockGeneticGroup, Integer blockSubGroup, String idMapping, Boolean completed, Float rakeOfStriae, Boolean noneRake) {
        this.id = id;
        this.senseOfMovement = senseOfMovement;
        this.orientationDd = orientationDd;
        this.orientationD = orientationD;
        this.thickness = thickness;
        this.matrixBlock = matrixBlock;
        this.matrixColourValue = matrixColourValue;
        this.matrixColourChroma = matrixColourChroma;
        this.matrixColourHue = matrixColourHue;
        this.matrixGainSize = matrixGainSize;
        this.blockSize = blockSize;
        this.blockShape = blockShape;
        this.blockGeneticGroup = blockGeneticGroup;
        this.blockSubGroup = blockSubGroup;
        this.idMapping = idMapping;
        this.completed = completed;
        this.rakeOfStriae = rakeOfStriae;
        this.noneRake = noneRake;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSenseOfMovement() {
        return senseOfMovement;
    }

    public void setSenseOfMovement(Integer senseOfMovement) {
        this.senseOfMovement = senseOfMovement;
    }

    public Float getOrientationDd() {
        return orientationDd;
    }

    public void setOrientationDd(Float orientationDd) {
        this.orientationDd = orientationDd;
    }

    public Float getOrientationD() {
        return orientationD;
    }

    public void setOrientationD(Float orientationD) {
        this.orientationD = orientationD;
    }

    public Float getThickness() {
        return thickness;
    }

    public void setThickness(Float thickness) {
        this.thickness = thickness;
    }

    public Float getMatrixBlock() {
        return matrixBlock;
    }

    public void setMatrixBlock(Float matrixBlock) {
        this.matrixBlock = matrixBlock;
    }

    public Integer getMatrixColourValue() {
        return matrixColourValue;
    }

    public void setMatrixColourValue(Integer matrixColourValue) {
        this.matrixColourValue = matrixColourValue;
    }

    public Integer getMatrixColourChroma() {
        return matrixColourChroma;
    }

    public void setMatrixColourChroma(Integer matrixColourChroma) {
        this.matrixColourChroma = matrixColourChroma;
    }

    public Integer getMatrixColourHue() {
        return matrixColourHue;
    }

    public void setMatrixColourHue(Integer matrixColourHue) {
        this.matrixColourHue = matrixColourHue;
    }

    public Integer getMatrixGainSize() {
        return matrixGainSize;
    }

    public void setMatrixGainSize(Integer matrixGainSize) {
        this.matrixGainSize = matrixGainSize;
    }

    public Integer getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(Integer blockSize) {
        this.blockSize = blockSize;
    }

    public Integer getBlockShape() {
        return blockShape;
    }

    public void setBlockShape(Integer blockShape) {
        this.blockShape = blockShape;
    }

    public Integer getBlockGeneticGroup() {
        return blockGeneticGroup;
    }

    public void setBlockGeneticGroup(Integer blockGeneticGroup) {
        this.blockGeneticGroup = blockGeneticGroup;
    }

    public Integer getBlockSubGroup() {
        return blockSubGroup;
    }

    public void setBlockSubGroup(Integer blockSubGroup) {
        this.blockSubGroup = blockSubGroup;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Float getRakeOfStriae() {
        return rakeOfStriae;
    }

    public void setRakeOfStriae(Float rakeOfStriae) {
        this.rakeOfStriae = rakeOfStriae;
    }

    public Boolean getNoneRake() {
        return noneRake;
    }

    public void setNoneRake(Boolean noneRake) {
        this.noneRake = noneRake;
    }

    @Override
    public String toString() {
        return "FailureZone{" +
                "id='" + id + '\'' +
                ", senseOfMovement=" + senseOfMovement +
                ", orientationDd=" + orientationDd +
                ", orientationD=" + orientationD +
                ", thickness=" + thickness +
                ", matrixBlock=" + matrixBlock +
                ", matrixColourValue=" + matrixColourValue +
                ", matrixColourChroma=" + matrixColourChroma +
                ", matrixColourHue=" + matrixColourHue +
                ", matrixGainSize=" + matrixGainSize +
                ", blockSize=" + blockSize +
                ", blockShape=" + blockShape +
                ", blockGeneticGroup=" + blockGeneticGroup +
                ", blockSubGroup=" + blockSubGroup +
                ", idMapping='" + idMapping + '\'' +
                ", completed=" + completed +
                ", rakeOfStriae=" + rakeOfStriae +
                ", noneRake=" + noneRake +
                '}';
    }
}
