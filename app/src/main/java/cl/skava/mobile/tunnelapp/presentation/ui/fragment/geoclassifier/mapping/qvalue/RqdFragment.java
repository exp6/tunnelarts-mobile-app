package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.qvalue;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;

import cl.skava.mobile.tunnelapp.R;

/**
 * Created by Jose Ignacio Vera on 05-07-2016.
 */
public class RqdFragment extends Fragment {

    private static final String TAG = "RqdFragment";

    private OnRqdValueListener mCallback;

    public interface OnRqdValueListener {
        void OnRqdSetValue(Double value);
    }

    private Integer rqdValue = 0;

    private Boolean mappingFinished;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_qvalue_rqd, container, false);
        rootView.setTag(TAG);

        NumberPicker np = (NumberPicker) rootView.findViewById(R.id.numberPicker1);
        np.setMinValue(0);
        np.setMaxValue(100);
        np.setValue(rqdValue);
        np.setWrapSelectorWheel(true);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mCallback.OnRqdSetValue(new Double(newVal));
            }
        });

        if(mappingFinished) {
            np.setEnabled(false);
        }

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnRqdValueListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnRqdValueListener");
        }
    }

    public void setRqdValue(Integer rqdValue) {
        this.rqdValue = rqdValue;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }
}
