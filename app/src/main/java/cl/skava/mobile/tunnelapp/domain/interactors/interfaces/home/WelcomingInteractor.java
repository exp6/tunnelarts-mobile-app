package cl.skava.mobile.tunnelapp.domain.interactors.interfaces.home;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.domain.interactors.base.Interactor;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;

/**
 * Created by Jose Ignacio Vera on 15-06-2016.
 */
public interface WelcomingInteractor extends Interactor {

    interface Callback {
        void onProjectListRetrieved(List<Project> projects);
        void onTunnelListRetrieved(List<Tunnel> tunnels);
        void onFaceListRetrieved(List<Face> faces);
        void onRetrievalFailed(String error);
        void onClientRetrieved(Client client);
        void onFormationUnitRetrieved(HashMap<Project, List<FormationUnit>> formationUnits);
        void onRockQualityRetrieved(HashMap<Project, List<RockQuality>> rockQualities);
    }
}
