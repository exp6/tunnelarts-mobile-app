package cl.skava.mobile.tunnelapp.domain.model;

/**
 * Created by Jose Ignacio Vera on 14-09-2016.
 */
public class UserProject {

    private String id;
    private String idUser;
    private String idProject;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    @Override
    public String toString() {
        return "UserProject{" +
//                "id='" + id + '\'' +
                ", idUser='" + idUser + '\'' +
                ", idProject='" + idProject + '\'' +
                '}';
    }
}
