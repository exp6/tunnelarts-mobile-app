package cl.skava.mobile.tunnelapp.domain.model.mapping;

import java.util.List;

/**
 * Created by Jose Ignacio Vera on 24-08-2016.
 */
public class EvaluationMethodSelectionGroup {

    private String id;
    private String index;
    private String description;
    private List<EvaluationMethodSelection> subSelections;
    private Boolean checked;
    private String descriptionEs;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<EvaluationMethodSelection> getSubSelections() {
        return subSelections;
    }

    public void setSubSelections(List<EvaluationMethodSelection> subSelections) {
        this.subSelections = subSelections;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getDescriptionEs() {
        return descriptionEs;
    }

    public void setDescriptionEs(String descriptionEs) {
        this.descriptionEs = descriptionEs;
    }

    @Override
    public boolean equals(Object c) {
        if(!(c instanceof EvaluationMethodSelectionGroup)) {
            return false;
        }

        EvaluationMethodSelectionGroup that = (EvaluationMethodSelectionGroup)c;
        return this.id.equals(that.getId()) && this.id.equals(that.getId());
    }

    @Override
    public String toString() {
        return "EvaluationMethodSelectionGroup{" +
                "id='" + id + '\'' +
                ", index='" + index + '\'' +
                ", description='" + description + '\'' +
                ", subSelections=" + subSelections +
                ", checked=" + checked +
                ", descriptionEs='" + descriptionEs + '\'' +
                '}';
    }
}
