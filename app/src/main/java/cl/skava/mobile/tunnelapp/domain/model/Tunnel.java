package cl.skava.mobile.tunnelapp.domain.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Jose Ignacio Vera on 15-06-2016.
 */
public class Tunnel implements Serializable {

    private String id;
    private String name;
    private String idProject;
    private Float span;
    private Float perimeter;
    private String idTunnelProfile;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public Float getSpan() {
        return span;
    }

    public void setSpan(Float span) {
        this.span = span;
    }

    public Float getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(Float perimeter) {
        this.perimeter = perimeter;
    }

    public String getIdTunnelProfile() {
        return idTunnelProfile;
    }

    public void setIdTunnelProfile(String idTunnelProfile) {
        this.idTunnelProfile = idTunnelProfile;
    }

    @Override
    public String toString() {
        return "Tunnel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", idProject='" + idProject + '\'' +
                ", span=" + span +
                ", perimeter=" + perimeter +
                ", idTunnelProfile='" + idTunnelProfile + '\'' +
                '}';
    }
}
