package cl.skava.mobile.tunnelapp.domain.model;

import java.io.Serializable;

/**
 * Created by Jose Ignacio Vera on 05-01-2017.
 */
public class RockQuality implements Serializable {

    private String id;
    private String idProject;
    private Integer code;
    private String name;
    private Float upperBound;
    private Float lowerBound;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getUpperBound() {
        return upperBound;
    }

    public void setUpperBound(Float upperBound) {
        this.upperBound = upperBound;
    }

    public Float getLowerBound() {
        return lowerBound;
    }

    public void setLowerBound(Float lowerBound) {
        this.lowerBound = lowerBound;
    }

    @Override
    public String toString() {
        return "RockQuality{" +
                "id='" + id + '\'' +
                ", idProject='" + idProject + '\'' +
                ", code=" + code +
                ", name='" + name + '\'' +
                ", upperBound=" + upperBound +
                ", lowerBound=" + lowerBound +
                '}';
    }
}
