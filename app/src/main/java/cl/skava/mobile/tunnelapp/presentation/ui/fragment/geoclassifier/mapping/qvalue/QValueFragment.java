package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.qvalue;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueDiscontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputSelectionDiscontinuity;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class QValueFragment extends Fragment
        implements RqdFragment.OnRqdValueListener,
        JnFragment.OnJnValueListener,
        JwFragment.OnJwValueListener,
        JrFragment.OnJrValueListener,
        JaFragment.OnJaValueListener,
        SrfFragment.OnSrfValueListener{

    private static final String TAG = "QValueFragment";

    private TabLayout tabLayout;
    private Toolbar toolbar;

    private Toolbar toolbarParent;

    private Double rqdValue;
    private Double jnValue;
    private Double jrValue;
    private Double jaValue;
    private Double jwValue;
    private Double srfValue;

    private Double degreeOfJointing;
    private Double jointFrictions;
    private Double activeStress;

    private Double qValue;

    private String jaId;
    private String jnId;
    private String jrId;
    private String jwId;
    private String srfId;
    private Boolean jnIntersection = false;

    private String rockQuality = "";
    private Integer rockQualityCode = null;

    private List<RockQuality> qualities = new ArrayList<>();

    private QValue mappingInput;

    private Boolean mappingFinished;

    private List<EvaluationMethodSelectionType> qValueSelectionMap;
    private QValueInputSelection qValueInputSelection;

    private Fragment currentFragment;

    private List<QValueDiscontinuity> qValueDiscontinuities;
    private int discontinuities;

    private TabLayout tabLayoutJn;

    private List<EvaluationMethodSelectionType> jrSelectionMap;
    private List<EvaluationMethodSelectionType> jaSelectionMap;

    private EvaluationMethodSelectionType emptyJrSelectionMap;
    private EvaluationMethodSelectionType emptyJaSelectionMap;

    private int jnType = 0;

    private List<Fragment> inputFragments;
    private int NUM_INPUTS;

    private List<cl.skava.mobile.tunnelapp.domain.model.RockQuality> rockQualities;

    private final String lang = Locale.getDefault().getLanguage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    private void initFragments() {
        inputFragments = new ArrayList<>();

        EvaluationMethodSelectionType emst;

        RqdFragment rqdFragment = new RqdFragment();
        rqdFragment.onAttachFragment(this);
        rqdFragment.setRqdValue(mappingInput.getRqd().intValue());
        rqdFragment.setMappingFinished(mappingFinished);
        inputFragments.add(rqdFragment);

        emst = getSelectionMap(0);
        JnFragment jnFragment = new JnFragment();
        jnFragment.onAttachFragment(this);
        jnFragment.setSelectionMap(emst);
        jnFragment.setIntersection(qValueInputSelection.getJnIntersection());
        jnFragment.setMappingFinished(mappingFinished);
        jnFragment.setJnType(jnType);
        inputFragments.add(jnFragment);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_qvalue, container, false);
        rootView.setTag(TAG);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText("RQD"));
        tabLayout.addTab(tabLayout.newTab().setText("JN"));
        tabLayout.addTab(tabLayout.newTab().setText("JR"));
        tabLayout.addTab(tabLayout.newTab().setText("JA"));
        tabLayout.addTab(tabLayout.newTab().setText("JW"));
        tabLayout.addTab(tabLayout.newTab().setText("SRF"));

        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        updateQValue();

        tabLayoutJn = (TabLayout) rootView.findViewById(R.id.tabs1);
        tabLayoutJn.setVisibility(View.GONE);

        RqdFragment f1 = new RqdFragment();
        f1.onAttachFragment(this);
        f1.setRqdValue(mappingInput.getRqd().intValue());
        f1.setMappingFinished(mappingFinished);
        currentFragment = f1;
        updateFragment(f1, R.id.qvalue_input_content);

        final Fragment parentFragment = this;

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                EvaluationMethodSelectionType emst = getSelectionMap(tab.getPosition() - 1);
                saveCurrentSelection(currentFragment);

                switch (tab.getPosition()) {
                    case 1:
                        tabLayoutJn.setVisibility(View.GONE);
                        JnFragment f2 = new JnFragment();
                        f2.onAttachFragment(parentFragment);
                        f2.setSelectionMap(emst);
                        f2.setIntersection(qValueInputSelection.getJnIntersection());
                        f2.setMappingFinished(mappingFinished);
                        f2.setJnType(jnType);
                        f2.setEnteredValue(mappingInput.getJn().doubleValue());
                        currentFragment = f2;
                        break;
                    case 2:
                        tabLayoutJn.setVisibility(View.VISIBLE);
                        JrFragment f3 = new JrFragment();
                        f3.onAttachFragment(parentFragment);
                        emst = jrSelectionMap.get(0);
                        f3.setSelectionMap(emst);
                        f3.setMappingFinished(mappingFinished);
                        f3.setDiscontinuity(0);

                        updateDiscontinuities();

                        if (discontinuities > 1) {
                            initTabs();
                            tabLayoutJn.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                @Override
                                public void onTabSelected(TabLayout.Tab tab) {
                                    saveCurrentSelection(currentFragment);

                                    JrFragment f3 = new JrFragment();
                                    f3.onAttachFragment(parentFragment);
                                    EvaluationMethodSelectionType emst = jrSelectionMap.get(tab.getPosition());
                                    f3.setSelectionMap(emst);
                                    f3.setMappingFinished(mappingFinished);
                                    f3.setDiscontinuity(tab.getPosition());
                                    currentFragment = f3;
                                    updateFragment(currentFragment, R.id.qvalue_input_content);
                                }

                                @Override
                                public void onTabUnselected(TabLayout.Tab tab) {
                                }

                                @Override
                                public void onTabReselected(TabLayout.Tab tab) {
                                }
                            });
                        } else {
                            tabLayoutJn.setVisibility(View.GONE);
                        }

                        currentFragment = f3;
                        break;
                    case 3:
                        tabLayoutJn.setVisibility(View.VISIBLE);
                        JaFragment f4 = new JaFragment();
                        f4.onAttachFragment(parentFragment);
                        emst = jaSelectionMap.get(0);
                        f4.setSelectionMap(emst);
                        f4.setMappingFinished(mappingFinished);
                        f4.setDiscontinuity(0);
                        f4.setEnteredValue(qValueDiscontinuities.get(0).getJa().doubleValue());

                        updateDiscontinuities();

                        if (discontinuities > 1) {
                            initTabs();
                            tabLayoutJn.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                @Override
                                public void onTabSelected(TabLayout.Tab tab) {
                                    saveCurrentSelection(currentFragment);

                                    JaFragment f4 = new JaFragment();
                                    f4.onAttachFragment(parentFragment);
                                    EvaluationMethodSelectionType emst = jaSelectionMap.get(tab.getPosition());
                                    f4.setSelectionMap(emst);
                                    f4.setMappingFinished(mappingFinished);
                                    f4.setDiscontinuity(tab.getPosition());
                                    f4.setEnteredValue(qValueDiscontinuities.get(tab.getPosition()).getJa().doubleValue());
                                    currentFragment = f4;
                                    updateFragment(currentFragment, R.id.qvalue_input_content);
                                }

                                @Override
                                public void onTabUnselected(TabLayout.Tab tab) {
                                }

                                @Override
                                public void onTabReselected(TabLayout.Tab tab) {
                                }
                            });
                        } else tabLayoutJn.setVisibility(View.GONE);

                        currentFragment = f4;
                        break;
                    case 4:
                        tabLayoutJn.setVisibility(View.GONE);
                        JwFragment f5 = new JwFragment();
                        f5.onAttachFragment(parentFragment);
                        f5.setSelectionMap(emst);
                        f5.setMappingFinished(mappingFinished);
                        f5.setEnteredValue(mappingInput.getJw().doubleValue());
                        currentFragment = f5;
                        break;
                    case 5:
                        tabLayoutJn.setVisibility(View.GONE);
                        SrfFragment f6 = new SrfFragment();
                        f6.onAttachFragment(parentFragment);
                        f6.setSelectionMap(emst);
                        f6.setMappingFinished(mappingFinished);
                        f6.setEnteredValue(mappingInput.getSrf().doubleValue());
                        currentFragment = f6;
                        break;
                    default:
                        tabLayoutJn.setVisibility(View.GONE);
                        RqdFragment f1 = new RqdFragment();
                        f1.onAttachFragment(parentFragment);
                        f1.setRqdValue(mappingInput.getRqd().intValue());
                        f1.setMappingFinished(mappingFinished);
                        currentFragment = f1;
                        break;
                }

                updateFragment(currentFragment, R.id.qvalue_input_content);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        toolbarParent.inflateMenu(R.menu.menu_geoclassifier_mapping_input_nav);
        toolbarParent.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                TabLayout.Tab tab = tabLayout.getTabAt(0);
                switch (item.getItemId()) {
                    case R.id.action_next_mapping_input:
                        int prevPosition = tabLayout.getSelectedTabPosition();
                        tab = (prevPosition == tabLayout.getTabCount() - 1) ? tabLayout.getTabAt(0) : tabLayout.getTabAt(++prevPosition);
                        break;

                    case R.id.action_previous_mapping_input:
                        int nextPosition = tabLayout.getSelectedTabPosition();
                        tab = (nextPosition > 0) ? tabLayout.getTabAt(--nextPosition) : tabLayout.getTabAt(tabLayout.getTabCount() - 1);
                        break;
                }
                tab.select();
                return false;
            }
        });

        return rootView;
    }

    private void initTabs() {
        tabLayoutJn.removeAllTabs();
        for(int i = 0; i < discontinuities; i++) {
            tabLayoutJn.addTab(tabLayoutJn.newTab().setText(getResources().getString(R.string.characterization_mapping_input_disc_label_discontinuity) +" " + (i+1)));
        }
    }

    private void updateDiscontinuities() {
        List<QValueDiscontinuity> aux = new ArrayList<>(discontinuities);

        List<EvaluationMethodSelectionType> aux2 = new ArrayList<>(discontinuities);
        List<EvaluationMethodSelectionType> aux3 = new ArrayList<>(discontinuities);

        for(int i = 0; i < discontinuities; i++) {
            if(i >= (qValueDiscontinuities.size())) {
                QValueDiscontinuity qValueDiscontinuity = new QValueDiscontinuity("", "", i, new Float(0), new Float(1), false, UUID.fromString("00000000-0000-0000-0000-000000000000").toString(), UUID.fromString("00000000-0000-0000-0000-000000000000").toString());
                aux.add(qValueDiscontinuity);

                EvaluationMethodSelectionType emptyjr = getEmptyMap(i, 0);
                EvaluationMethodSelectionType emptyja = getEmptyMap(i, 1);
                aux2.add(emptyjr);
                aux3.add(emptyja);
            }
            else {
                aux.add(qValueDiscontinuities.get(i));
                aux2.add(jrSelectionMap.get(i));
                aux3.add(jaSelectionMap.get(i));
            }
        }
        qValueDiscontinuities = aux;
        jrSelectionMap = aux2;
        jaSelectionMap = aux3;
    }

    private EvaluationMethodSelectionType getEmptyMap(int id, int code) {
        EvaluationMethodSelectionType emptyjr = new EvaluationMethodSelectionType();
        List<EvaluationMethodSelectionGroup> selectionGroups = new ArrayList<>();
        EvaluationMethodSelectionGroup newEmsg;
        List<EvaluationMethodSelection> selections;

        List<EvaluationMethodSelectionGroup> selectionGroupList = (code == 0 ? emptyJrSelectionMap.getSelectionGroups() : emptyJaSelectionMap.getSelectionGroups());
        for(int i = 0; i < selectionGroupList.size(); i++) {
            EvaluationMethodSelectionGroup emsg = selectionGroupList.get(i);
            newEmsg = new EvaluationMethodSelectionGroup();
            newEmsg.setId(selectionGroupList.get(i).getId());
            newEmsg.setIndex(selectionGroupList.get(i).getIndex());
            newEmsg.setDescription(selectionGroupList.get(i).getDescription());
            newEmsg.setDescriptionEs(selectionGroupList.get(i).getDescriptionEs());
            newEmsg.setChecked(selectionGroupList.get(i).getChecked());
            EvaluationMethodSelection ems;

            selections = new ArrayList<>();
            for(int j = 0; j < emsg.getSubSelections().size(); j++) {
                ems = new EvaluationMethodSelection();
                ems.setId(emsg.getSubSelections().get(j).getId());
                ems.setIndex(emsg.getSubSelections().get(j).getIndex());
                ems.setStart(emsg.getSubSelections().get(j).getStart());
                ems.setEnd(emsg.getSubSelections().get(j).getEnd());
                ems.setDescription(emsg.getSubSelections().get(j).getDescription());
                ems.setDescriptionEs(emsg.getSubSelections().get(j).getDescriptionEs());
                ems.setChecked(emsg.getSubSelections().get(j).getChecked());
                selections.add(ems);
            }
            newEmsg.setSubSelections(selections);
            selectionGroups.add(newEmsg);
        }

        emptyjr.setId("" + id);
        emptyjr.setMethodTypeCode(0);
        emptyjr.setDescription("");
        emptyjr.setCode(0);
        emptyjr.setSelectionGroups(selectionGroups);
        return emptyjr;
    }

    private void saveCurrentSelection(Fragment f) {
        EvaluationMethodSelectionType map;
        int position = 0;

        if(f instanceof JaFragment) {
            Log.i(TAG, "JaFragment");
            JaFragment fx = (JaFragment) f;
            map = fx.getSelectionMap();
            jaSelectionMap.set(fx.getDiscontinuity(), map);
        }

        if(f instanceof JnFragment) {
            Log.i(TAG, "JnFragment");
            JnFragment fx = (JnFragment) f;
            map = fx.getSelectionMap();
            position = getSelectionMapPosition(map);
            qValueSelectionMap.set(position, map);
        }

        if(f instanceof JrFragment) {
            Log.i(TAG, "JrFragment");
            JrFragment fx = (JrFragment) f;
            map = fx.getSelectionMap();
            jrSelectionMap.set(fx.getDiscontinuity(), map);
        }

        if(f instanceof JwFragment) {
            Log.i(TAG, "JwFragment");
            JwFragment fx = (JwFragment) f;
            map = fx.getSelectionMap();
            position = getSelectionMapPosition(map);
            qValueSelectionMap.set(position, map);
        }

        if(f instanceof SrfFragment) {
            Log.i(TAG, "SrfFragment");
            SrfFragment fx = (SrfFragment) f;
            map = fx.getSelectionMap();
            position = getSelectionMapPosition(map);
            qValueSelectionMap.set(position, map);
        }
    }

    private EvaluationMethodSelectionType getSelectionMap(Integer code) {
        for(EvaluationMethodSelectionType e : qValueSelectionMap) {
            if(e.getCode().intValue() == code.intValue()) {
                return e;
            }
        }
        return null;
    }

    private int getSelectionMapPosition(EvaluationMethodSelectionType map) {
        for(int i = 0; i < qValueSelectionMap.size(); i++) {
            EvaluationMethodSelectionType e = qValueSelectionMap.get(i);
            if(e.getCode().intValue() == map.getCode().intValue()) {
                return i;
            }
        }
        return 0;
    }

    public void setToolbarParent(Toolbar toolbarParent) {
        this.toolbarParent = toolbarParent;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void updateFragment(Fragment fragment, int container) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(container, fragment);
        transaction.commitAllowingStateLoss();
    }

    private void updateQValue() {
        double localjn;

        switch (jnType) {
            case 1:
                localjn = 3*jnValue;
                break;
            case 2:
                localjn = 2*jnValue;
                break;
            default:
                localjn = jnValue;
                break;
        }

        double rqdFinalValue = rqdValue < 10 ? 10 : rqdValue;

        degreeOfJointing = rqdFinalValue/localjn;
        activeStress = jwValue/srfValue;

        List<Double> jointFrictionsList = new ArrayList<>();
        for(QValueDiscontinuity q : qValueDiscontinuities) {
            jointFrictionsList.add(q.getJr().doubleValue()/q.getJa().doubleValue());
        }

        Double jointFrictionsMax = calculateQMax(jointFrictionsList);
        Double jointFrictionsMin = calculateQMin(jointFrictionsList);
        Double jointFrictionsAvg = calculateQAvg(jointFrictionsList);

        Double qMax = (degreeOfJointing * jointFrictionsMax * activeStress);
        Double qMin = (degreeOfJointing * jointFrictionsMin * activeStress);
        Double qAvg = (degreeOfJointing * jointFrictionsAvg * activeStress);

        RockQuality rockQualityObj = qualities.get(0);
        cl.skava.mobile.tunnelapp.domain.model.RockQuality rockQuality;
        String rockQualityName = "";

        if(rockQualities != null) {
            if(!rockQualities.isEmpty()) {
                rockQuality = rockQualities.get(0);

                for(cl.skava.mobile.tunnelapp.domain.model.RockQuality rq : rockQualities) {
                    if(qualityMatchedFinal(rq, qAvg)) {
                        rockQuality = rq;
                        break;
                    }
                }
                mappingInput.setRockQualityCode(rockQuality.getCode());
                rockQualityName = rockQuality.getName().trim();
            }
        }
        else {
            for(RockQuality rq : qualities) {
                if(qualityMatched(rq, qAvg)) {
                    rockQualityObj = rq;
                    break;
                }
            }
            mappingInput.setRockQualityCode(rockQualityObj.getCode());
            rockQualityName = rockQualityObj.getName().trim();
        }

        mappingInput.setqMax(qMax.floatValue());
        mappingInput.setqMin(qMin.floatValue());
        mappingInput.setqAvg(qAvg.floatValue());

        toolbar.setTitle("Q Max = " + round(qMax, 4) + " - Q Min = " + round(qMin, 4) + " - Q Avg = " + round(qAvg, 4));
        toolbar.setSubtitle("RQD/Jn = " + round(degreeOfJointing, 4) + " - Max(Jr/Ja) = " + round(jointFrictionsMax, 4) + " - Min(Jr/Ja) = " + round(jointFrictionsMin, 4) + " - Avg(Jr/Ja) = " + round(jointFrictionsAvg, 4) + " - Jw/SRF = " + round(activeStress, 4) + ", "+(lang.equals("es") ? "Calidad Roca" : "Rock Quality")+": " + rockQualityName);
    }

    private Double calculateQMax(List<Double> items) {
        Double result = 0.0;
        for(Double item : items) {
            if(item > result) result = item;
            else continue;
        }
        return result;
    }

    private Double calculateQMin(List<Double> items) {
        Double result = calculateQMax(items);
        for(Double item : items) {
            if(item < result) result = item;
            else continue;
        }
        return result;
    }

    private Double calculateQAvg(List<Double> items) {
        Double result = 0.0;
        Double total = new Double(items.size());

        for(Double item  : items) {
            result += item;
        }
        return result/total;
    }

    @Override
    public void OnRqdSetValue(Double value) {
        rqdValue = value;
        mappingInput.setRqd(new Float(rqdValue));
        updateQValue();
    }

    @Override
    public void OnJnSetValue(Double value, String id, Boolean intersection, int disc) {
        jnValue = value;
        jnId = id;
        jnIntersection = intersection;
        qValueInputSelection.setJn(jnId);
        qValueInputSelection.setJnIntersection(jnIntersection);
        mappingInput.setJn(new Float(jnValue));
        mappingInput.setDiscontinuities(disc);
        discontinuities = disc;
        updateQValue();
    }

    @Override
    public void OnJnSetType(Integer type) {
        jnType = type;
//        jnIntersection = intersection;
//        qValueInputSelection.setJnIntersection(jnIntersection);
        mappingInput.setJnType(jnType);
        updateQValue();
    }

    @Override
    public void OnJwSetValue(Double value, String id) {
        jwValue = value;
        jwId = id;
        qValueInputSelection.setJw(jwId);
        mappingInput.setJw(new Float(jwValue));
        updateQValue();
    }

    @Override
    public void OnJrSetValue(Double value, String id, int disc) {
        jrValue = value;
        jrId = id;
        qValueInputSelection.setJr(jrId);
        qValueDiscontinuities.get(disc).setJr(new Float(jrValue));
        qValueDiscontinuities.get(disc).setJrSelection(jrId);
        mappingInput.setJr(new Float(jrValue));
        updateQValue();
    }

    @Override
    public void OnJaSetValue(Double value, String id, int disc) {
        jaValue = value;
        jaId = id;
        qValueInputSelection.setJa(jaId);
        qValueDiscontinuities.get(disc).setJa(new Float(jaValue));
        qValueDiscontinuities.get(disc).setJaSelection(jaId);
        mappingInput.setJa(new Float(jaValue));
        updateQValue();
    }

    @Override
    public void OnSrfSetValue(Double value, String id) {
        srfValue = value;
        srfId = id;
        qValueInputSelection.setSrf(srfId);
        mappingInput.setSrf(new Float(srfValue));
        updateQValue();
    }

    private boolean qualityMatched(RockQuality rq, Double qvalue) {
        return (rq.getLowerBound() <= qvalue && qvalue < rq.getUpperBound());
    }

    private boolean qualityMatchedFinal(cl.skava.mobile.tunnelapp.domain.model.RockQuality rq, Double qvalue) {
        return (rq.getLowerBound() <= qvalue && qvalue < rq.getUpperBound());
    }

    public void setMappingInput(QValue mappingInput) {
        this.mappingInput = mappingInput;
    }

    public QValue getMappingInput() {
        return mappingInput;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public QValueInputSelection getqValueInputSelection() {
        return qValueInputSelection;
    }

    public void setqValueInputSelection(QValueInputSelection qValueInputSelection) {
        this.qValueInputSelection = qValueInputSelection;
    }

    public void setqValueSelectionMap(List<EvaluationMethodSelectionType> qValueSelectionMap) {
        this.qValueSelectionMap = qValueSelectionMap;
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public List<QValueDiscontinuity> getqValueDiscontinuities() {
        return qValueDiscontinuities;
    }

    public void setqValueDiscontinuities(List<QValueDiscontinuity> qValueDiscontinuities) {
        this.qValueDiscontinuities = qValueDiscontinuities;
    }

    public void setJrSelectionMap(List<EvaluationMethodSelectionType> jrSelectionMap) {
        this.jrSelectionMap = jrSelectionMap;
    }

    public List<EvaluationMethodSelectionType> getJrSelectionMap() {
        return jrSelectionMap;
    }

    public void setJaSelectionMap(List<EvaluationMethodSelectionType> jaSelectionMap) {
        this.jaSelectionMap = jaSelectionMap;
    }

    public List<EvaluationMethodSelectionType> getJaSelectionMap() {
        return jaSelectionMap;
    }

    public EvaluationMethodSelectionType getEmptyJrSelectionMap() {
        return emptyJrSelectionMap;
    }

    public void setEmptyJrSelectionMap(EvaluationMethodSelectionType emptyJrSelectionMap) {
        this.emptyJrSelectionMap = emptyJrSelectionMap;
    }

    public EvaluationMethodSelectionType getEmptyJaSelectionMap() {
        return emptyJaSelectionMap;
    }

    public void setEmptyJaSelectionMap(EvaluationMethodSelectionType emptyJaSelectionMap) {
        this.emptyJaSelectionMap = emptyJaSelectionMap;
    }

    public void setRockQualities(List<cl.skava.mobile.tunnelapp.domain.model.RockQuality> rockQualities) {
        this.rockQualities = rockQualities;
    }

    private void initData() {
        rqdValue = (double) mappingInput.getRqd();
        jnValue = (double) mappingInput.getJn();
        jrValue = (double) mappingInput.getJr();
        jaValue = (double) mappingInput.getJa();
        jwValue = (double) mappingInput.getJw();
        srfValue = (double) mappingInput.getSrf();

//        discontinuities = mappingInput.getDiscontinuities();
        discontinuities = qValueDiscontinuities.isEmpty() ? 1 : qValueDiscontinuities.size();

        jnType = mappingInput.getJnType();

        RockQuality rq = new RockQuality();
        rq.setName("Exceptionally poor");
        rq.setUpperBound(new Double(0.01));
        rq.setLowerBound(new Double(0.001));
        rq.setCode(0);
        qualities.add(rq);

        rq = new RockQuality();
        rq.setName("Extremmely poor");
        rq.setUpperBound(new Double(0.1));
        rq.setLowerBound(new Double(0.01));
        rq.setCode(1);
        qualities.add(rq);

        rq = new RockQuality();
        rq.setName("Very poor");
        rq.setUpperBound(new Double(1));
        rq.setLowerBound(new Double(0.1));
        rq.setCode(2);
        qualities.add(rq);

        rq = new RockQuality();
        rq.setName("Poor");
        rq.setUpperBound(new Double(4));
        rq.setLowerBound(new Double(1));
        rq.setCode(3);
        qualities.add(rq);

        rq = new RockQuality();
        rq.setName("Fair");
        rq.setUpperBound(new Double(10));
        rq.setLowerBound(new Double(4));
        rq.setCode(4);
        qualities.add(rq);

        rq = new RockQuality();
        rq.setName("Good");
        rq.setUpperBound(new Double(40));
        rq.setLowerBound(new Double(10));
        rq.setCode(5);
        qualities.add(rq);

        rq = new RockQuality();
        rq.setName("Very good");
        rq.setUpperBound(new Double(100));
        rq.setLowerBound(new Double(40));
        rq.setCode(6);
        qualities.add(rq);

        rq = new RockQuality();
        rq.setName("Extremmely good");
        rq.setUpperBound(new Double(400));
        rq.setLowerBound(new Double(100));
        rq.setCode(7);
        qualities.add(rq);

        rq = new RockQuality();
        rq.setName("Exceptionally good");
        rq.setUpperBound(new Double(401));
        rq.setLowerBound(new Double(400));
        rq.setCode(8);
        qualities.add(rq);
    }

    class RockQuality {
        String name;
        Double lowerBound;
        Double upperBound;
        Integer code;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Double getLowerBound() {
            return lowerBound;
        }

        public void setLowerBound(Double lowerBound) {
            this.lowerBound = lowerBound;
        }

        public Double getUpperBound() {
            return upperBound;
        }

        public void setUpperBound(Double upperBound) {
            this.upperBound = upperBound;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }
    }
}
