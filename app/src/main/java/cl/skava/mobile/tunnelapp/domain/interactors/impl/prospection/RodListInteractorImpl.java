package cl.skava.mobile.tunnelapp.domain.interactors.impl.prospection;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.base.AbstractInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.prospection.RodListInteractor;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.domain.repository.RodRepository;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class RodListInteractorImpl extends AbstractInteractor
        implements RodListInteractor {

    private RodListInteractor.Callback mCallback;
    private RodRepository mSticksRepository;

    public RodListInteractorImpl(Executor threadExecutor,
                                 MainThread mainThread,
                                 RodListInteractor.Callback callback,
                                 RodRepository positionRepository) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mSticksRepository = positionRepository;
    }

    private void postSticks(final List<Rod> rods) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRodListRetrieved(rods);
            }
        });
    }

    @Override
    public void run() {
        final List<Rod> rods = getList();
        postSticks(rods);
    }

    private List<Rod> getList() {
        final List<Rod> rods = mSticksRepository.getList();

        if (rods == null) {
            notifyError();
        }

        return rods;
    }

    private void notifyError() {
//        mMainThread.post(new Runnable() {
//            @Override
//            public void run() {
//                mCallback.onRetrievalFailed("No Modules on the system.");
//            }
//        });
    }

    @Override
    public void create(Rod rod) {
        if(mSticksRepository.insert(rod)) {
            final List<Rod> rods = getList();

            if(rods == null) {
                return;
            }

            postSticks(rods);
        }
    }

    @Override
    public void updateItems(List<Rod> rods) {
        for(Rod r : rods) {
            mSticksRepository.update(r);
        }
        postSticks(rods);
    }

    @Override
    public void update(Rod rod) {
        if(mSticksRepository.update(rod)) {
            final List<Rod> rods = getList();

            if(rods == null) {
                return;
            }

            postSticks(rods);
        }
    }

    @Override
    public void delete(Rod rod) {
        if(mSticksRepository.delete(rod)) {
            final List<Rod> rods = getList();

            if(rods == null) {
                return;
            }

            postSticks(rods);
        }
    }
}
