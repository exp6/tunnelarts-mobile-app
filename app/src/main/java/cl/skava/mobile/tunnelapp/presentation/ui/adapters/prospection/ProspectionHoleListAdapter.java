package cl.skava.mobile.tunnelapp.presentation.ui.adapters.prospection;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.prospection.calculations.ProspectionHoleListFragment;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class ProspectionHoleListAdapter extends RecyclerView.Adapter<ProspectionHoleListAdapter.ViewHolder> {

    private static final String TAG = "ProspectionHoleListAdapter";
    protected List<ProspectionHole> mProspectionDataset;

    private int selectedItem = 0;
    ViewHolder viewHolderPool[];
    protected RecyclerView mRecyclerView;
    private ProspectionHoleListFragment.OnProspectionItemSelectedListener mCallback;

    public ProspectionHoleListAdapter(List<ProspectionHole> dataSet,
                                      ProspectionHoleListFragment.OnProspectionItemSelectedListener callback,
                                      RecyclerView recyclerView) {
        mProspectionDataset = dataSet;
        mCallback = callback;
        viewHolderPool = new ViewHolder[mProspectionDataset.size()];
        mRecyclerView = recyclerView;
        setHasStableIds(true);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameTextView;
        private final TextView angleTextView;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedItem = getLayoutPosition();
                    mCallback.onProspectionSelected(selectedItem);

                    for (ViewHolder vh : viewHolderPool) {
                        vh.itemView.setSelected(false);
                        vh.itemView.setBackgroundColor(Color.TRANSPARENT);
                    }
                    notifyDataSetChanged();
                    mRecyclerView.smoothScrollToPosition(selectedItem);
                }
            });

            nameTextView = (TextView) v.findViewById(R.id.prospection_name);
            angleTextView = (TextView) v.findViewById(R.id.prospection_angle);
        }

        public TextView getNameTextView() {
            return nameTextView;
        }

        public TextView getAngleTextView() {
            return angleTextView;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_prospection_calculations_prospection_row_item, parent, false);
        holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProspectionHole pProspection = mProspectionDataset.get(position);

        holder.getNameTextView().setText(pProspection.getName().trim());
        holder.getAngleTextView().setText(pProspection.getAngle().toString());
//        ImageView lock = ((ImageView) holder.itemView.findViewById(R.id.locked_icon));

//        if(mapping.getFinished()) lock.setImageResource(R.drawable.ic_lock);

        if (viewHolderPool[position] == null)
            viewHolderPool[position] = holder;

        holder.itemView.setSelected(selectedItem == position);
        holder.itemView.setBackgroundColor(holder.itemView.isSelected() ? Color.LTGRAY : Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return mProspectionDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
