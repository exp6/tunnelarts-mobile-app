package cl.skava.mobile.tunnelapp.presentation.ui.adapters.prospection;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.prospection.calculations.RodListFragment;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class RodListAdapter extends RecyclerView.Adapter<RodListAdapter.ViewHolder> {

    private static final String TAG = "RodListAdapter";

    protected List<Rod> mRodDataset;

    private int selectedItem = 0;
    ViewHolder viewHolderPool[];
    protected RecyclerView mRecyclerView;
    private RodListFragment.OnRodItemSelectedListener mCallback;

    public RodListAdapter(List<Rod> dataSet,
                          RodListFragment.OnRodItemSelectedListener callback,
                          RecyclerView recyclerView) {
        mRodDataset = dataSet;
        mCallback = callback;
        viewHolderPool = new ViewHolder[mRodDataset.size()];
        mRecyclerView = recyclerView;
        setHasStableIds(true);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView stickName;
        private final TextView speedValue;
        private final TextView avgPressureValue;
        private final TextView cumulativeDepthValue;
//        private final TextView previousQValue;
//        private final TextView waterColourValue;
        private final TextView qValue;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedItem = getLayoutPosition();
                    mCallback.onRodSelected(selectedItem);
                    notifyDataSetChanged();
                    mRecyclerView.smoothScrollToPosition(selectedItem);
                }
            });

            stickName = (TextView) v.findViewById(R.id.stick_name_text);
            speedValue = (TextView) v.findViewById(R.id.speed_value);
            avgPressureValue = (TextView) v.findViewById(R.id.avg_pressure_value);
            cumulativeDepthValue = (TextView) v.findViewById(R.id.cumulative_depth_value);
//            previousQValue = (TextView) v.findViewById(R.id.q_previous_value);
//            waterColourValue = (TextView) v.findViewById(R.id.color_water_value);
            qValue = (TextView) v.findViewById(R.id.q_value);
        }

        public TextView getStickName() {
            return stickName;
        }

        public TextView getSpeedValue() {
            return speedValue;
        }

        public TextView getAvgPressureValue() {
            return avgPressureValue;
        }

        public TextView getCumulativeDepthValue() {
            return cumulativeDepthValue;
        }

//        public TextView getPreviousQValue() {
//            return previousQValue;
//        }

//        public TextView getWaterColourValue() {
//            return waterColourValue;
//        }

        public TextView getqValue() {
            return qValue;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_prospection_calculations_rod_row_item, parent, false);
        holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Rod rod = mRodDataset.get(position);

        holder.getStickName().setText(rod.getName().trim());
        holder.getSpeedValue().setText(rod.getSpeed().toString() + " [m/min]");
        holder.getAvgPressureValue().setText(rod.getAveragePressure().toString() + " [Bar]");
        holder.getCumulativeDepthValue().setText(rod.getDrilledLength().toString() + " [m]");
//        holder.getPreviousQValue().setText(rod.getqPrevious().toString());
//        holder.getWaterColourValue().setText(rod.getWaterColor().toString());
        holder.getqValue().setText(rod.getqValue().toString());

        Button editRod = (Button) holder.itemView.findViewById(R.id.button_edit);
        Button deleteRod = (Button) holder.itemView.findViewById(R.id.button_delete);

        editRod.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCallback.editRod(rod);
            }
        });

        deleteRod.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCallback.deleteSelectedRod(rod);
            }
        });

        if (viewHolderPool[position] == null)
            viewHolderPool[position] = holder;

        deleteRod.setVisibility(View.GONE);

        if(position == mRodDataset.size()-1) {
            deleteRod.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mRodDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
