package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by Jose Ignacio Vera on 24-08-2016.
 */
public class EvaluationMethodSelection {

    private String id;
    private String index;
    private Double start;
    private Double end;
    private String description;
    private Boolean checked;
    private String descriptionEs;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public Double getStart() {
        return start;
    }

    public void setStart(Double start) {
        this.start = start;
    }

    public Double getEnd() {
        return end;
    }

    public void setEnd(Double end) {
        this.end = end;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getDescriptionEs() {
        return descriptionEs;
    }

    public void setDescriptionEs(String descriptionEs) {
        this.descriptionEs = descriptionEs;
    }

    @Override
    public boolean equals(Object c) {
        if(!(c instanceof EvaluationMethodSelection)) {
            return false;
        }

        EvaluationMethodSelection that = (EvaluationMethodSelection)c;
        return this.id.equals(that.getId()) && this.id.equals(that.getId());
    }

    @Override
    public String toString() {
        return "EvaluationMethodSelection{" +
                "id='" + id + '\'' +
                ", index='" + index + '\'' +
                ", start=" + start +
                ", end=" + end +
                ", description='" + description + '\'' +
                ", checked=" + checked +
                ", descriptionEs='" + descriptionEs + '\'' +
                '}';
    }
}
