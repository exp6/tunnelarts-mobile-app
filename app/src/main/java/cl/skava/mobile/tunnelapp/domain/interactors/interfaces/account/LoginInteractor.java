package cl.skava.mobile.tunnelapp.domain.interactors.interfaces.account;

import cl.skava.mobile.tunnelapp.domain.interactors.base.Interactor;
import cl.skava.mobile.tunnelapp.domain.model.account.User;

/**
 * Created by Jose Ignacio Vera on 24-06-2016.
 */
public interface LoginInteractor extends Interactor {

    interface Callback {
        void onUserValidationStatus(boolean status, User user);
    }

    void validateUser(User user);
    void onRestValidation(User user);
}
