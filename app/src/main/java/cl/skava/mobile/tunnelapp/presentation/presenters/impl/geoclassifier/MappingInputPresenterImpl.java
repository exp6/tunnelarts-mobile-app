package cl.skava.mobile.tunnelapp.presentation.presenters.impl.geoclassifier;

import android.content.Context;

import java.util.List;

import cl.skava.mobile.tunnelapp.data.repository.MappingInputRepositoryImpl;
import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.impl.geoclassifier.MappingInputsInteractorImpl;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.geoclassifier.MappingInputsInteractor;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInstance;
import cl.skava.mobile.tunnelapp.domain.repository.MappingInputRepository;
import cl.skava.mobile.tunnelapp.presentation.presenters.base.AbstractPresenter;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.geoclassifier.MappingInputPresenter;

/**
 * Created by Jose Ignacio Vera on 26-07-2016.
 */
public class MappingInputPresenterImpl extends AbstractPresenter
        implements MappingInputPresenter,
        MappingInputsInteractor.Callback {

    private MappingInputPresenter.View mView;
    private MappingInputRepository mMappingInputRepository;

    MappingInputsInteractor mappingInputInteractor;

    private Context mActivity;

    public MappingInputPresenterImpl(Executor executor,
                                     MainThread mainThread,
                                     View view,
                                     Context activity) {
        super(executor, mainThread);
        mView = view;
        mActivity = activity;
    }

    @Override
    public void resume() {
        mView.showProgress();
    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void getMappingInputs(Mapping mapping, List<MappingInputModule> inputs) {
        mView.showProgress();
        mMappingInputRepository = new MappingInputRepositoryImpl(mActivity, mapping, inputs);

        MappingInputsInteractor mappingInputsInteractor = new MappingInputsInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mMappingInputRepository
        );

        mappingInputsInteractor.execute();
    }

    @Override
    public void saveMappingInputs(MappingInstance mappingInstance, List<MappingInputModule> inputs) {
        mMappingInputRepository = new MappingInputRepositoryImpl(mActivity, mappingInstance.getMapping(), inputs);

        MappingInputsInteractor mappingInputsInteractor = new MappingInputsInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mMappingInputRepository
        );

        mappingInputsInteractor.saveMappingInstance(mappingInstance);
    }

//    @Override
//    public void onMappingInputListRetrieved(List<MappingInputModule> mappingInput) {
//
//    }

    @Override
    public void onMappingInstanceRetrieved(MappingInstance mappingInstance) {
        mView.hideProgress();
        mView.displayMappingInstance(mappingInstance);
    }

    @Override
    public void onSaveStatus(boolean success) {
        mView.hideProgress();
        mView.onSaveStatus(success);
    }
}
