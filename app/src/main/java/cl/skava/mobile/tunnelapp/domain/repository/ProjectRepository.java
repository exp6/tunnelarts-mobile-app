package cl.skava.mobile.tunnelapp.domain.repository;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Project;

/**
 * Created by Jose Ignacio Vera on 15-06-2016.
 */
public interface ProjectRepository {

    Client getClient();
    List<Project> getAllProjects();
}
