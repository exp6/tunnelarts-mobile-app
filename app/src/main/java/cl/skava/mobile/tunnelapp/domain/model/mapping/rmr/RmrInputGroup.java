package cl.skava.mobile.tunnelapp.domain.model.mapping.rmr;

import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInput;
import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInputType;

/**
 * Created by Jose Ignacio Vera on 24-08-2016.
 */
public class RmrInputGroup extends DataInputType {

    private String id;
    private String idRmrInputGroupType;
    private String codeIndex;
    private String description;
    private String descriptionEs;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdRmrInputGroupType() {
        return idRmrInputGroupType;
    }

    public void setIdRmrInputGroupType(String idRmrInputGroupType) {
        this.idRmrInputGroupType = idRmrInputGroupType;
    }

    public String getCodeIndex() {
        return codeIndex;
    }

    public void setCodeIndex(String codeIndex) {
        this.codeIndex = codeIndex;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionEs() {
        return descriptionEs;
    }

    public void setDescriptionEs(String descriptionEs) {
        this.descriptionEs = descriptionEs;
    }

    @Override
    public String toString() {
        return "RmrInputGroup{" +
                "id='" + id + '\'' +
                ", idRmrInputGroupType='" + idRmrInputGroupType + '\'' +
                ", codeIndex='" + codeIndex + '\'' +
                ", description='" + description + '\'' +
                ", descriptionEs='" + descriptionEs + '\'' +
                '}';
    }
}
