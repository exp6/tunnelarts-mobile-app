package cl.skava.mobile.tunnelapp.presentation.ui.activity.account;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//import butterknife.BindView;
//import butterknife.ButterKnife;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.executor.impl.ThreadExecutor;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.presentation.presenters.impl.MainPresenterImpl;
import cl.skava.mobile.tunnelapp.presentation.presenters.impl.account.LoginPresenterImpl;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.MainPresenter;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.account.LoginPresenter;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.account.LoginFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.account.LoginUsersCachedFragment;
import cl.skava.mobile.tunnelapp.threading.MainThreadImpl;

/**
 * Created by Jose Ignacio Vera on 18-05-2016.
 */
public class LoginActivity extends AppCompatActivity
        implements LoginPresenter.View,
        LoginFragment.OnUserDataListener,
        LoginUsersCachedFragment.OnUserDataListener{

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

//    @BindView(R.id.input_email)
    EditText _emailText;
//    @BindView(R.id.input_password)
    EditText _passwordText;
//    @BindView(R.id.btn_login)
    Button _loginButton;

    private ProgressDialog progressDialog;
    private LoginPresenter mPresenter;

    private List<User> cachedUsers;

    private boolean userLogged;

    private boolean writePermissionGranted;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        checkWritePermissions();
//        ButterKnife.bind(this);

        mPresenter = new LoginPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                this.getExternalFilesDir(null).getPath()+ File.separator + "users");

        mPresenter.resume();
    }

    private void checkWritePermissions() {
        this.writePermissionGranted = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the HomeActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess(User user) {
//        _loginButton.setEnabled(true);

        Intent intent = getIntent();
//        intent.putExtra("module_data_set", (Serializable) mModuleDataset);
        intent.putExtra("user", user);
        intent.putExtra("userCached", userLogged);
        setResult(RESULT_OK, intent);
//
        finish();
    }

    @Override
    public void onLoginAccess(User user) {
        if(isNetworkAvailable()) {

            progressDialog = new ProgressDialog(LoginActivity.this,
                    R.style.AppTheme_Dark_Dialog);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getResources().getString(R.string.system_label_authenticating));
            progressDialog.show();

            mPresenter.validateUser(user);
        }
        else {
            final String alertTitle = getResources().getString(R.string.system_label_no_internet);
            final Context c = this;
            final String msg1 = getResources().getString(R.string.system_label_no_internet_msg);

            final LoginFragment fragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.content_login);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                public void run() {
                    new android.support.v7.app.AlertDialog.Builder(c)
                            .setTitle(alertTitle)
                            .setMessage(msg1)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    fragment.enableLoginButton();
                                }
                            })
                            .show();
                }
            });
        }
    }

    @Override
    public void onLoginAccessCache(User user) {
        checkPermissionsGranted();
        if (!writePermissionGranted) {
            return;
        }
        progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getResources().getString(R.string.system_label_authenticating));
        progressDialog.show();

        mPresenter.validateUser(user);
    }

    public void checkPermissionsGranted() {
        checkWritePermissions();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !writePermissionGranted) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    /**
     * Maneja el resultado de una petición de permisos. Específicamente maneja cuando se piden
     * los permisos para WRITE_EXTERNAL_STORAGE en {@link LoginFragment#checkPermissionsGranted()} ()}, código 1.
     *
     * @param requestCode el código de la petición de permisos
     * @param permissions los permisos
     * @param grantResults los resultados de cada permiso, si fue concedido o no
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                checkWritePermissions();
                break;
        }
    }

    @Override
    public void goToNewLogin() {
        LoginFragment fragment = new LoginFragment();
        userLogged = true;
        fragment.setExistCache(userLogged);
        updateFragment(fragment, R.id.content_login);
    }

    @Override
    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), getResources().getString(R.string.account_label_login_failed), Toast.LENGTH_LONG).show();

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_login);

        if(fragment instanceof LoginFragment) {
            LoginFragment f = (LoginFragment) fragment;
            f.enableLoginButton();
        }
//        _loginButton.setEnabled(true);
    }

    @Override
    public void goToCacheLogin() {
        LoginUsersCachedFragment fragment = new LoginUsersCachedFragment();
        userLogged = true;
        fragment.setmUserDataSet(cachedUsers);
        updateFragment(fragment, R.id.content_login);
    }

    @Override
    public void onUserValidationStatus(boolean status, User user) {
        progressDialog.dismiss();

        if(status) {
            onLoginSuccess(user);
        }
        else {
            final LoginFragment fragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.content_login);

            final String alertTitle = getResources().getString(R.string.account_label_invalid_user);
            final Context c = this;
            final String msg1 = getResources().getString(R.string.account_label_username_password_incorrect);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                public void run() {
                    new android.support.v7.app.AlertDialog.Builder(c)
                            .setTitle(alertTitle)
                            .setMessage(msg1)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    fragment.enableLoginButton();
//                                    _loginButton.setEnabled(true);
                                }
                            })
                            .show();
                }
            });
        }
    }

    @Override
    public void onCachedUsers(List<User> users) {
        cachedUsers = users;

        if(users.isEmpty()) {
            LoginFragment fragment = new LoginFragment();
            userLogged = false;
            fragment.setExistCache(userLogged);
            updateFragment(fragment, R.id.content_login);
        }
        else {
            LoginUsersCachedFragment fragment = new LoginUsersCachedFragment();
            userLogged = true;
            fragment.setmUserDataSet(cachedUsers);
            updateFragment(fragment, R.id.content_login);
        }
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(String message) {

    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void updateFragment(Fragment fragment, int container) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment);
        transaction.commitAllowingStateLoss();
    }

    public boolean isWritePermissionGranted() {
        return writePermissionGranted;
    }
}
