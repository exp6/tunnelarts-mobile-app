package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse;

import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInput;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInput;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputGroupType;

/**
 * Created by Carlos Vergara on 20-03-2017.
 */

public class SyncResponseHandlerFactoryImpl implements SyncResponseHandlerFactory {
    @Override
    public SyncResponseHandler createHandler(String handlerName) {
        switch (handlerName) {
            case "Client":
                return new ClientSyncResponseHandler();
            case "UserProject":
                return new UserProjectSyncResponseHandler();
            case "Project":
                return new ProjectSyncResponseHandler();
            case "FormationUnit":
                return new FormationUnitSyncResponseHandler();
            case "RockQuality":
                return new RockQualitySyncResponseHandler();
            case "ProjectMappingInput":
                return new ProjectMappingInputSyncResponseHandler();
            case "MappingInput":
                return new MappingInputSyncResponseHandler();
            case "Tunnel":
                return new TunnelSyncResponseHandler();
            case "Face":
                return new FaceSyncResponseHandler();
            case "Mapping":
                return new MappingSyncResponseHandler();
            case "ChangeLog":
                return new ChangeLogSyncResponseHandler();
            case "QValueInputGroupType":
                return new SingleTableSyncResponseHandler(QValueInputGroup.class);
            case "QValueInputGroup":
                return new SingleTableSyncResponseHandler(QValueInput.class);
            case "QValueInput":
                return new QValueInputSyncResponseHandler();
            case "QValueDiscontinuity":
                return new SingleTableSyncResponseHandler(RmrInputGroupType.class);
            case "RmrInputGroupType":
                return new SingleTableSyncResponseHandler(RmrInputGroup.class);
            case "RmrInputGroup":
                return new SingleTableSyncResponseHandler(RmrInput.class);
            case "RmrInput":
                return new RmrInputSyncResponseHandler();
            case "ProspectionHoleGroup":
                return new ProspectionHoleGroupSyncResponseHandler();
            case "ProspectionHole":
                return new ProspectionHoleSyncResponseHandler();
            case "Rod":
                return new RodSyncResponseHandler();
            case "ProspectionHoleGroupCalculation":
                return new ProspectionHoleGroupCalculationSyncResponseHandler();
            case "ProspectionRefPicture":
                return new ProspectionRefPictureSyncResponseHandler();
            default:
                return new DefaultSyncResponseHandler();
        }
    }
}
