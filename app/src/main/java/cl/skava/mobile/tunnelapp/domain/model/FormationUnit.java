package cl.skava.mobile.tunnelapp.domain.model;

import java.io.Serializable;

/**
 * Created by Jose Ignacio Vera on 05-01-2017.
 */
public class FormationUnit implements Serializable {

    private String id;
    private String idProject;
    private Integer code;
    private String label;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "FormationUnit{" +
                "id='" + id + '\'' +
                ", idProject='" + idProject + '\'' +
                ", code=" + code +
                ", label='" + label + '\'' +
                '}';
    }
}
