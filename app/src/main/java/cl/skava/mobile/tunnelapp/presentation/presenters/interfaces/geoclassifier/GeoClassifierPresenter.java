package cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.geoclassifier;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SelectionDetailLists;
import cl.skava.mobile.tunnelapp.presentation.presenters.base.BasePresenter;
import cl.skava.mobile.tunnelapp.presentation.ui.base.BaseView;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public interface GeoClassifierPresenter extends BasePresenter {

    interface View extends BaseView {
        //View methods goes here
        void displayMappingList(List<Mapping> mappings);
        void displayMappingInputList(List<MappingInputModule> mappingInputModules);
        void syncDataResult(boolean success, String msg);
        void onSyncProgressPercentaje(Integer percentaje);
        void notifySyncSegment(String msg);
        void onExportFinish(boolean success);
    }

    //Presenter methods goes here
    void getInputsByMapping(Mapping mapping);
    void createMapping(Mapping mapping);
    void saveMappings();
    void syncData(User user);
    void deleteMapping(Mapping mapping);
    void updateMapping(Mapping mapping);
    void exportData(Project p, Tunnel t, Face f, String directoryPath, SelectionDetailLists selectionDetailLists);
}
