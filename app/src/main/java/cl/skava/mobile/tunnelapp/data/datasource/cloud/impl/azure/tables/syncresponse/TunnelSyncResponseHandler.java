package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse;

import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;

/**
 * Created by Carlos Vergara on 20-03-2017.
 */

public class TunnelSyncResponseHandler extends FetchFromTableSyncResponseHandler<Tunnel> {
    TunnelSyncResponseHandler() {

    }

    @Override
    protected void onDataFetchedSuccessfully(CloudMobileService cloudMobileService, List<Tunnel> tunnels) {
        CloudStoreService cloudStoreService = (CloudStoreService) cloudMobileService;
        cloudStoreService.syncFaces(tunnels);
    }

    @Override
    protected void initializeClassToFetchFrom() {
        setClassToFetchFrom(Tunnel.class);
    }
}
