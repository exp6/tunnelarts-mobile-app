package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncprocess;

import android.content.Context;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;

import java.net.MalformedURLException;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreServiceHelper;
import cl.skava.mobile.tunnelapp.data.net.cloud.impl.azure.tables.SyncClient;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.repository.BaseRepository;

/**
 * Created by Jose Ignacio Vera on 23-09-2016.
 */
public class SyncBaseData implements TableSyncProcess {

    private BaseRepository mCallback;
    private User mUser;
    private SyncClient mProvider;
    private MobileServiceClient mClient;

    public SyncBaseData(Context activity, BaseRepository callback, User user, MobileServiceClient client) {
        mCallback = callback;
        mUser = user;
        mClient = client;
//        mProvider = new SyncClient(this, mClient);
    }

    @Override
    public void doSync() {

    }

    @Override
    public void handleSyncResponse(boolean success, String entity) {

    }
}
