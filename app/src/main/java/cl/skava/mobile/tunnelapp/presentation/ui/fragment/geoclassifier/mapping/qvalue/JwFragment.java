package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.qvalue;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;

/**
 * Created by Jose Ignacio Vera on 05-07-2016.
 */
public class JwFragment extends Fragment {

    private static final String TAG = "JwFragment";

    private List<Jw> jws = new ArrayList<>();

    private OnJwValueListener mCallback;

    public interface OnJwValueListener {
        void OnJwSetValue(Double value, String id);
    }

    private boolean isValid;
    private EvaluationMethodSelectionType selectionMap;

    private Boolean mappingFinished;

    private final String lang = Locale.getDefault().getLanguage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initDataset();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_qvalue_jw, container, false);
        rootView.setTag(TAG);

        RadioGroup group = (RadioGroup) rootView.findViewById(R.id.radio_group1);
        RadioButton button;

        int i = 0;
        for(Jw jw :  jws) {
            final Jw jw1 = jw;

            button = new RadioButton(getActivity());
            button.setId(i);
            button.setText(jw.getIndex().trim() + " (" + jw.getStart() + (jw.getEnd() != null ? " - " + jw.getEnd() : "") + ")     " + jw.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (jw1.getEnd() != null) {
                        execTextDialog(getResources().getString(R.string.characterization_mapping_input_qmet_label_range1)
                                        + " " + jw1.getIndex(), getResources().getString(R.string.characterization_mapping_input_qmet_label_range2) +" "
                                + jw1.getStart() + " " + getResources().getString(R.string.characterization_mapping_input_qmet_label_range3) + " "
                                + jw1.getEnd(), jw1.getStart(), jw1.getEnd(), jw1.getId()
                        , jw1, (jw1.isChecked() ? enteredValue : (enteredValue >= jw1.getStart() && enteredValue <= jw1.getEnd() ? enteredValue : jw1.getStart())));
                    } else {
                        mCallback.OnJwSetValue(jw1.getStart(), jw1.getId());
                        updateMap(jw1);
                    }
                }
            });
            button.setChecked(jw.isChecked());

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }

        return rootView;
    }

    private void updateMap(Jw jw) {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        EvaluationMethodSelection input;
        groups.get(0).setChecked(true);
        inputs = groups.get(0).getSubSelections();

        for(int j = 0; j < inputs.size(); j++) {
            input = inputs.get(j);
            input.setChecked(false);
            if(jw.getId().equalsIgnoreCase(input.getId())) {
                input.setChecked(true);
            }
        }

        groups.get(0).setSubSelections(inputs);
        selectionMap.setSelectionGroups(groups);
    }

    private Double enteredValue = null;

    public void execTextDialog(String title, String msg, Double start, Double end, String id, Jw jw, Double valueSelected) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this.getContext());
        alert.setTitle(title);
        alert.setMessage(msg);
        // Create TextView
        final EditText input = new EditText(this.getContext());
        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        final Double startValue = start;
        final Double endValue = end;
        final String id1 = id;

        input.addTextChangedListener(new TextValidator(input) {
            @Override
            public void validate(TextView textView, String text) {
                final String message = getResources().getString(R.string.characterization_mapping_input_qmet_label_range_validation1)
                        + " " + startValue
                        + " " + getResources().getString(R.string.characterization_mapping_input_qmet_label_range_validation2)
                        + " " + endValue;

                try {
                    Double inputValue = new Double(text);
                    if (inputValue.doubleValue() >= startValue.doubleValue() && inputValue.doubleValue() <= endValue.doubleValue()) {
                        isValid = true;
                        enteredValue = inputValue;
                    } else {
                        input.setError(message);
                        isValid = false;
                    }
                } catch (NumberFormatException e) {
                    input.setError(message);
                }
            }
        });

        final Jw jw1 = jw;

        Double valueFinal = round(valueSelected, 1);

        input.setText(valueFinal.toString());
        alert.setView(input);
        alert.setCancelable(false);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                final AlertDialog alertDialog = (AlertDialog)dialog;

                if(input.getText().toString().isEmpty()) {
                    enteredValue = startValue;
                }

                if(isValid) {
                    mCallback.OnJwSetValue(enteredValue, id1);
                    updateMap(jw1);
                    alertDialog.dismiss();
                }
                else {
                    input.setError(getResources().getString(R.string.characterization_mapping_input_qmet_label_range_validation3));
                }

            }
        });

        alert.show();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnJwValueListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnJwValueListener");
        }
    }

    public void setSelectionMap(EvaluationMethodSelectionType selectionMap) {
        this.selectionMap = selectionMap;
    }

    public EvaluationMethodSelectionType getSelectionMap() {
        return selectionMap;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public void setEnteredValue(Double enteredValue) {
        this.enteredValue = enteredValue;
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private void initDataset() {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        EvaluationMethodSelectionGroup e = groups.get(0);
        List<EvaluationMethodSelection> inputs = e.getSubSelections();

        Jw jw;

        for(EvaluationMethodSelection em : inputs) {
            jw = new Jw();
            jw.setId(em.getId());
            jw.setIndex(em.getIndex());
            jw.setStart(em.getStart());
            jw.setEnd(em.getEnd().doubleValue() == 0.0 ? null : em.getEnd());
            jw.setDescription((lang.equals("es") ? em.getDescriptionEs() : em.getDescription()));
            jw.setChecked(em.getChecked());
            jws.add(jw);
        }
    }

    class Jw {
        private String id;
        private String index;
        private Double value;
        private Double start;
        private Double end;
        private String description;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public Double getStart() {
            return start;
        }

        public void setStart(Double start) {
            this.start = start;
        }

        public Double getEnd() {
            return end;
        }

        public void setEnd(Double end) {
            this.end = end;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }

    public abstract class TextValidator implements TextWatcher {
        private final TextView textView;

        public TextValidator(TextView textView) {
            this.textView = textView;
        }

        public abstract void validate(TextView textView, String text);

        @Override
        final public void afterTextChanged(Editable s) {
            String text = textView.getText().toString();
            validate(textView, text);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { /* Don't care */ }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { /* Don't care */ }
    }
}
