package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse;

import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.Project;

/**
 * Created by Carlos Vergara on 20-03-2017.
 */

public class RockQualitySyncResponseHandler extends FetchFromTableSyncResponseHandler<Project> {
    RockQualitySyncResponseHandler() {

    }

    @Override
    protected void onDataFetchedSuccessfully(CloudMobileService cloudMobileService, List<Project> projects) {
        CloudStoreService cloudStoreService = (CloudStoreService) cloudMobileService;
        cloudStoreService.syncProjectMappingInputs(projects);
    }

    @Override
    protected void initializeClassToFetchFrom() {
        setClassToFetchFrom(Project.class);
    }
}
