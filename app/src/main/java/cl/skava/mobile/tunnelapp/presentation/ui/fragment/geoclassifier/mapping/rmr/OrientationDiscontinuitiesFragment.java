package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rmr;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;

/**
 * Created by Jose Ignacio Vera on 08-07-2016.
 */
public class OrientationDiscontinuitiesFragment extends Fragment {

    private static final String TAG = "OrientationDiscontinuitiesFragment";

    private List<OrientationDGroup> orientationDGroups = new ArrayList<>();

    private OnOrientationDValueListener mCallback;

    public interface OnOrientationDValueListener {
        void OnOrientationDSetValue(Double value, String id);
    }

    private EvaluationMethodSelectionType selectionMap;

    private Boolean mappingFinished;

    private final String lang = Locale.getDefault().getLanguage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initDataset();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_rmr_orientationdiscontinuities, container, false);
        rootView.setTag(TAG);

        RadioGroup group = (RadioGroup) rootView.findViewById(R.id.radio_group1);
        RadioButton button;

        final RadioGroup group2 = (RadioGroup) rootView.findViewById(R.id.radio_group2);

        int i = 0;
        for(OrientationDGroup orientationDGroup :  orientationDGroups) {
            final OrientationDGroup orientationDGroup1 = orientationDGroup;
            button = new RadioButton(getActivity());
            button.setId(i);
//            button.setText(orientationDGroup.getIndex().trim() + ". " + orientationDGroup.getDescription());
            button.setText(orientationDGroup.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    displayOds(group2, orientationDGroup1);
                }
            });
            button.setChecked(orientationDGroup.isChecked());
            if(orientationDGroup.isChecked()) {
                displayOds(group2, orientationDGroup1);
            }

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }

        return rootView;
    }

    private void displayOds(RadioGroup group, OrientationDGroup orientationDGroup) {
        List<OrientationD> orientationDs = orientationDGroup.getOds();
        final OrientationDGroup orientationDGroup1 = orientationDGroup;
        group.removeAllViews();
        RadioButton button;

        int i = 0;
        for(OrientationD orientationD :  orientationDs) {
            final OrientationD orientationD1 = orientationD;

            button = new RadioButton(getActivity());
            button.setId(i);
//            button.setText(orientationD.getIndex().trim() + " (" + orientationD.getStart() + (orientationD.getEnd() != null ? " - " + orientationD.getEnd() : "") + ")     " + orientationD.getDescription());
            button.setText("(" + orientationD.getStart() + (orientationD.getEnd() != null ? " - " + orientationD.getEnd() : "") + ")     " + orientationD.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mCallback.OnOrientationDSetValue(orientationD1.getStart(), orientationD1.getId());
                    updateMap(orientationDGroup1, orientationD1);
                }
            });
            button.setChecked(orientationD1.isChecked());

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }
    }

    private void updateMap(OrientationDGroup orientationDGroup, OrientationD orientationD) {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        EvaluationMethodSelection input;
        for(int i = 0; i < groups.size(); i++) {
            groups.get(i).setChecked(false);
            if(orientationDGroup.getId().equalsIgnoreCase(groups.get(i).getId())) {
                groups.get(i).setChecked(true);
            }

            inputs = groups.get(i).getSubSelections();
            for(int j = 0; j < inputs.size(); j++) {
                input = inputs.get(j);
                input.setChecked(false);
                if(orientationD.getId().equalsIgnoreCase(input.getId())) {
                    input.setChecked(true);
                }
            }

            groups.get(i).setSubSelections(inputs);
        }
        selectionMap.setSelectionGroups(groups);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnOrientationDValueListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnOrientationDValueListener");
        }
    }

    public void setSelectionMap(EvaluationMethodSelectionType selectionMap) {
        this.selectionMap = selectionMap;
    }

    public EvaluationMethodSelectionType getSelectionMap() {
        return selectionMap;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    private void initDataset() {

        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        OrientationDGroup orientationDGroup;
        List<OrientationD> orientationDs;
        OrientationD orientationD;

        for(EvaluationMethodSelectionGroup e : groups) {
            orientationDGroup = new OrientationDGroup();
            orientationDGroup.setId(e.getId());
            orientationDGroup.setIndex(e.getIndex());
            orientationDGroup.setDescription((lang.equals("es") ? e.getDescriptionEs() : e.getDescription()));
            orientationDGroup.setChecked(e.getChecked());
            inputs = e.getSubSelections();

            orientationDs = new ArrayList<>();

            for (EvaluationMethodSelection em : inputs) {
                orientationD = new OrientationD();
                orientationD.setId(em.getId());
                orientationD.setIndex(em.getIndex());
                orientationD.setStart(em.getStart());
                orientationD.setEnd(em.getEnd().doubleValue() == 0.0 ? null : em.getEnd());
                orientationD.setDescription((lang.equals("es") ? em.getDescriptionEs() : em.getDescription()));
                orientationD.setChecked(em.getChecked());
                orientationDs.add(orientationD);
            }

            orientationDGroup.setOds(orientationDs);
            orientationDGroups.add(orientationDGroup);
        }
    }

    class OrientationD {
        private String id;
        private String index;
        private Double value;
        private Double start;
        private Double end;
        private String description;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public Double getStart() {
            return start;
        }

        public void setStart(Double start) {
            this.start = start;
        }

        public Double getEnd() {
            return end;
        }

        public void setEnd(Double end) {
            this.end = end;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }

    class OrientationDGroup {
        private String id;
        private String index;
        private String description;
        private List<OrientationD> ods;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<OrientationD> getOds() {
            return ods;
        }

        public void setOds(List<OrientationD> ods) {
            this.ods = ods;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }
}
