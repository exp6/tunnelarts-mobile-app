package cl.skava.mobile.tunnelapp.domain.model.modelentity;

/**
 * Created by Carlos Vergara on 23-03-2017.
 */

public interface ModelEntity {
    String getId();
}
