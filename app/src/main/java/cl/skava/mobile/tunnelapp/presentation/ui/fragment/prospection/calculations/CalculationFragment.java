package cl.skava.mobile.tunnelapp.presentation.ui.fragment.prospection.calculations;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

//import org.jpmml.evaluator.Evaluator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.executor.impl.ThreadExecutor;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.presentation.presenters.impl.prospection.ProspectionCalculationsPresenterImpl;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.prospection.ProspectionCalculationsPresenter;
import cl.skava.mobile.tunnelapp.threading.MainThreadImpl;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class CalculationFragment extends Fragment
        implements ProspectionCalculationsPresenter.View,
        ProspectionHoleListFragment.OnProspectionItemSelectedListener,
        RodListFragment.OnRodItemSelectedListener {

    private static final String TAG = "CalculationFragment";

    //User Selection Context
    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Module selectedModule;

    private User mUser;
    private ProspectionHoleGroup selectedProspectionHoleGroup;
    private ProspectionHole selectedProspectionHole;
    private Rod selectedRod;

    private final String FORECASTING_DIRECTORY = "forecasts";
    private final String MODEL_DIRECTORY = "prospection";

    private OnExecuteModelListener mCallback;

    public interface OnExecuteModelListener {
        void onExecStart();
        void onExecStop();
        void hideDelete();
        void hideEdit();
        void showDelete();
        void showEdit();
    }

    private ProspectionCalculationsPresenter mPresenter;

    private List<ProspectionHole> mProspectionDataset;
    private List<Rod> mRodDataset;

    private ProgressDialog uiProgressDialog;

    private View dialogContent;
    private AlertDialog alertDialog;

    //private List<Evaluator> evaluators;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCallback = (OnExecuteModelListener) getActivity();

        if (savedInstanceState == null) {
            initDatasources();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_prospection_calculations, container, false);
        rootView.setTag(TAG);

        dialogContent = inflater.inflate(R.layout.fragment_prospection_calculations_prospection_new, null);

        FloatingActionButton myFab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                newProspection();
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.resume();
    }

    private void newProspection() {

        if(alertDialog == null)
            alertDialog = new AlertDialog.Builder(getActivity()).create();

        final int index = mProspectionDataset.size() + 1;

        alertDialog.setTitle(getResources().getString(R.string.forecasting_label_probe_hole_new));
        alertDialog.setMessage(getResources().getString(R.string.forecasting_label_probe_hole_new_msg) + " " + index);
        // Alert dialog button
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.system_label_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

//                        EditText inputName = (EditText) dialogContent.findViewById(R.id.input_name);
                        EditText inputAngle = (EditText) dialogContent.findViewById(R.id.input_angle);

                        ProspectionHole prospection = new ProspectionHole();
                        prospection.setName("PH " + index);
                        prospection.setAngle(Float.parseFloat(inputAngle.getText().toString()));
                        prospection.setIdProspectionHoleGroup(selectedProspectionHoleGroup.getId().toUpperCase());
                        prospection.setCreatedAt(new Date());

//                        inputName.setText("");
                        inputAngle.setText("");

                        mPresenter.createNewProspection(prospection);
                        dialog.dismiss();
                    }
                });

        alertDialog.setView(dialogContent);
        alertDialog.show();
    }

    public void editSelectedItem() {
//        EditText inputName = (EditText) dialogContent.findViewById(R.id.input_name);
        EditText inputAngle = (EditText) dialogContent.findViewById(R.id.input_angle);

//        inputName.setText(selectedProspectionHole.getName());
        inputAngle.setText(selectedProspectionHole.getAngle().toString());

        if(alertDialog == null)
            alertDialog = new AlertDialog.Builder(getActivity()).create();

        alertDialog.setTitle(getResources().getString(R.string.forecasting_label_probe_hole_edit));
        alertDialog.setMessage(getResources().getString(R.string.forecasting_label_probe_hole_edit_msg) + " " + selectedProspectionHole.getName().trim());
        // Alert dialog button
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.system_label_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

//                        EditText inputName = (EditText) dialogContent.findViewById(R.id.input_name);
                        EditText inputAngle = (EditText) dialogContent.findViewById(R.id.input_angle);

//                        selectedProspectionHole.setName(inputName.getText().toString());
                        selectedProspectionHole.setAngle(Float.parseFloat(inputAngle.getText().toString()));

                        dialog.dismiss();

//                        inputName.setText("");
                        inputAngle.setText("");

                        mPresenter.updateProspection(selectedProspectionHole);
                    }
                });

        alertDialog.setView(dialogContent);
        alertDialog.show();
    }

    private void initDatasources() {
        mPresenter = new ProspectionCalculationsPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                getActivity(),
                selectedProspectionHoleGroup.getId());

        mProspectionDataset = new ArrayList<>();
        mRodDataset = new ArrayList<>();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setSelectionContext(Project project,
                                    Tunnel tunnel,
                                    Face face,
                                    Module module,
                                    User user,
                                    ProspectionHoleGroup prospectionHoleGroup) {
        selectedProject = project;
        selectedTunnel = tunnel;
        selectedFace = face;
        selectedModule = module;
        mUser = user;
        selectedProspectionHoleGroup = prospectionHoleGroup;
    }

    public void executeModel() {
        InputStream in = null;
        OutputStream out = null;
        File outFile = null;

        List<InputStream> modelsIn = new ArrayList<>();
        List<OutputStream> modelsOut = new ArrayList<>();
        List<File> outputFiles = new ArrayList<>();

//        try {
//            in = getActivity().getResources().openRawResource(R.raw.model);
//            outFile = new File(getActivity().getExternalFilesDir(null).getPath() + File.separator + MODEL_DIRECTORY + File.separator + "model.r");

//            if (!outFile.exists()) {
//                try{
//                    outFile.getParentFile().mkdir();
//                }
//                catch(SecurityException se){
//                    //handle it
//                }
//            }

//            out = new FileOutputStream(outFile);
//
//            InputStream is;
//            OutputStream out2;
//            File outModelFile;
//
//            for(int i = 1; i <= 1; i++) {
////                is = getActivity().getAssets().open("prospection/model/Modelo_Skava_" + i + ".rda");
//                is = getActivity().getAssets().open("forecasting/model/PMML_Modelo_Skava_" + i + ".pmml.ser");
////                outModelFile = new File(getActivity().getExternalFilesDir(null) + File.separator + MODEL_DIRECTORY + File.separator + "Modelo_Skava_" + i + ".rda");
//                outModelFile = new File(getActivity().getExternalFilesDir(null) + File.separator + MODEL_DIRECTORY + File.separator + "PMML_Modelo_Skava_" + i + ".pmml.ser");
//                out2 = new FileOutputStream(outModelFile);
//
//                modelsIn.add(is);
//                modelsOut.add(out2);
//                outputFiles.add(outModelFile);
//            }

//            mPresenter.executeModel(outFile, in, out, modelsIn, modelsOut, outputFiles, mRodDataset, selectedProspectionHoleGroup.getqValueSelected());
//        } catch(IOException e) {
//            e.printStackTrace();
//        }

        outFile = new File(getActivity().getExternalFilesDir(null).getPath() + File.separator + MODEL_DIRECTORY + File.separator + FORECASTING_DIRECTORY + File.separator + selectedTunnel.getId().toLowerCase());
        if(outFile.listFiles().length == 0) {
            final Context c = getActivity();

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                public void run() {
                    new android.support.v7.app.AlertDialog.Builder(c)
                            .setTitle("No existen modelos predictivos para el tunel seleccionado.")
                            .setMessage("Esto es debido a que no se han entrenado modelos predictivos para este tunel. Contáctese con soporte técnico para este caso.")
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setPositiveButton("SALIR", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            })
                            .show();
                }
            });
        }
        else {
            File[] models = outFile.listFiles();
            mPresenter.executeModel(models, mRodDataset, selectedProspectionHoleGroup.getqValueSelected());
        }
    }

    @Override
    public void showProgress() {
        uiProgressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_Dark_Dialog);
        uiProgressDialog.setMessage(getResources().getString(R.string.system_label_preparing_user_interface));
        uiProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        uiProgressDialog.dismiss();
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void showExecutionProgress() {
        mCallback.onExecStart();
    }

    @Override
    public void hideExecutionProgress() {
        mCallback.onExecStop();
    }

    @Override
    public void displayPositionList(List<ProspectionHole> prospections) {
        mProspectionDataset = prospections;

        if(mProspectionDataset.isEmpty()) {
            mCallback.hideDelete();
            mCallback.hideEdit();
        }
        else {
            mCallback.showDelete();
            mCallback.showEdit();
        }

        if(mProspectionDataset != null){
            if(!mProspectionDataset.isEmpty()){
                selectedProspectionHole = mProspectionDataset.get(0);
            }
            else {
                RodListFragment fragment = new RodListFragment();
                fragment.setMRodDataset(new ArrayList<Rod>());
                fragment.onAttachFragment(this);
                fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, mUser, selectedProspectionHole, null, selectedProspectionHoleGroup);
                updateFragment(fragment, R.id.stick_list);
            }
        }

        ProspectionHoleListFragment fragment = (ProspectionHoleListFragment) getChildFragmentManager().findFragmentById(R.id.positions_list);

        if (fragment == null) {
            fragment = new ProspectionHoleListFragment();
            fragment.setMProspectionDataset(mProspectionDataset);
            fragment.onAttachFragment(this);
            fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, mUser, selectedProspectionHoleGroup);
            updateFragment(fragment, R.id.positions_list);
        }
        else {
            fragment.setMProspectionDataset(mProspectionDataset);
            fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, mUser, selectedProspectionHoleGroup);
            fragment.updateItemList();
        }
    }

    @Override
    public void displayStickList(List<Rod> rods) {
        mRodDataset = rods;

        if(mRodDataset != null){
            if(!mRodDataset.isEmpty()){
                selectedRod = mRodDataset.get(0);
            }
        }

        RodListFragment fragment = (RodListFragment) getChildFragmentManager().findFragmentById(R.id.stick_list);

        if (fragment == null) {
            fragment = new RodListFragment();
            fragment.setMRodDataset(mRodDataset);
            fragment.onAttachFragment(this);
            fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, mUser, selectedProspectionHole, selectedProspectionHole, selectedProspectionHoleGroup);
            updateFragment(fragment, R.id.stick_list);
        }
        else {
            fragment.setMRodDataset(mRodDataset);
            fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, mUser, selectedProspectionHole, selectedProspectionHole, selectedProspectionHoleGroup);
            fragment.updateItemList();
        }
    }

    private void updateFragment(Fragment fragment, int container) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(container, fragment);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onProspectionSelected(int position) {
        selectedProspectionHole = mProspectionDataset.get(position);
        mPresenter.getRodsByProspection(selectedProspectionHole);
    }

    @Override
    public void onRodSelected(int position) {
        selectedRod = mRodDataset.get(position);
    }

    @Override
    public void createRod(Rod rod) {
        mPresenter.createNewRod(rod);
    }

    @Override
    public void execModel() {
        final Context c = getActivity();

        Handler h = new Handler(Looper.getMainLooper());
        h.post(new Runnable() {
            public void run() {
                new android.support.v7.app.AlertDialog.Builder(c)
                        .setTitle(getResources().getString(R.string.forecasting_label_execute_title))
                        .setMessage(getResources().getString(R.string.forecasting_label_execute_msg))
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setPositiveButton(getResources().getString(R.string.system_label_yes), new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                executeModel();
                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public void deleteSelectedRod(Rod rod) {
        final Context c = getActivity();
        final Rod r = rod;

        Handler h = new Handler(Looper.getMainLooper());
        h.post(new Runnable() {
            public void run() {
                new android.support.v7.app.AlertDialog.Builder(c)
                        .setTitle(getResources().getString(R.string.system_label_delete_item) + " " + getResources().getString(R.string.forecasting_label_rod))
                        .setMessage(getResources().getString(R.string.system_label_delete_item_msg) + getResources().getString(R.string.system_label_proceed))
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(getResources().getString(R.string.system_label_yes), new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                mPresenter.deleteRod(r);
                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public void updateSelectedRod(Rod rod) {
        mPresenter.updateRod(rod);
    }

    @Override
    public void editRod(Rod rod) {
        RodListFragment fragment = (RodListFragment) getChildFragmentManager().findFragmentById(R.id.stick_list);
        fragment.editRod(rod);
    }

    public void deleteSelectedProspection() {
        mPresenter.deleteProspection(selectedProspectionHole);
    }
}
