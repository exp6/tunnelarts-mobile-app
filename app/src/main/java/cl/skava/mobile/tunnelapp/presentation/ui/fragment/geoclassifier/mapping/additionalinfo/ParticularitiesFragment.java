package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.additionalinfo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.INotificationSideChannel;
import android.support.v4.content.FileProvider;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.data.cache.FileManager;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Particularities;
import cl.skava.mobile.tunnelapp.presentation.ui.activity.geoclassifier.mapping.MappingInputActivity;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.MappingInputFragment;
import id.zelory.compressor.Compressor;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Jose Ignacio Vera on 04-07-2016.
 */
public class ParticularitiesFragment extends Fragment {

    public static final String TAG = "ParticularitiesFragment";

    private Particularities mappingInput;

    private Boolean mappingFinished;
    private static EditText inputOverbreakLocationStart;
    private static EditText inputOverbreakLocationEnd;

    private String overbreakPicturePath;

    private Button buttonShowPic;
    private ImageButton buttonDeletePic;

    public static final int TAKE_OVERBREAK_PICTURE = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_additionalinfo_2, container, false);
        rootView.setTag(TAG);

        CheckBox none1 = (CheckBox) rootView.findViewById(R.id.chk_none1);
        CheckBox none2 = (CheckBox) rootView.findViewById(R.id.chk_none2);

        none1.setChecked(mappingInput.getInstabilityNa());
        none2.setChecked(mappingInput.getOverbreakNa());

        final EditText inputInstabilityCausedBy = ((EditText) rootView.findViewById(R.id.input_instability_caused_by));
        final EditText inputInstabilityLocation = ((EditText) rootView.findViewById(R.id.input_instability_location));
//        final EditText inputOverbreakCausedBy = ((EditText) rootView.findViewById(R.id.input_overbreak_caused_by));
        final EditText inputOverbreakVolume = ((EditText) rootView.findViewById(R.id.input_volume_location));
        final Spinner spinnerOverbreakCausedByValue = (Spinner) rootView.findViewById(R.id.spinner_overbreak_caused_by);

        inputOverbreakLocationStart = ((EditText) rootView.findViewById(R.id.input_overbreak_location_start));
        inputOverbreakLocationEnd = ((EditText) rootView.findViewById(R.id.input_overbreak_location_end));

        final ImageButton b1 = (ImageButton) rootView.findViewById(R.id.overbreak_start_time_btn);
        b1.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTimePickerDialog(0);
                    }
                });
        final ImageButton b2 = (ImageButton) rootView.findViewById(R.id.overbreak_end_time_btn);
        b2.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTimePickerDialog(1);
                    }
                });

        inputInstabilityCausedBy.setText(mappingInput.getInstabilityCausedBy().toString());
        inputInstabilityLocation.setText(mappingInput.getInstabilityLocation().toString());
//        inputOverbreakCausedBy.setText(mappingInput.getOverbreakCausedBy().toString());
        inputOverbreakVolume.setText(mappingInput.getOverbreakVolume().toString());
        inputOverbreakLocationStart.setText(mappingInput.getOverbreakLocationStart() == null ? "" : mappingInput.getOverbreakLocationStart().toString());
        inputOverbreakLocationEnd.setText(mappingInput.getOverbreakLocationEnd() == null ? "" : mappingInput.getOverbreakLocationEnd().toString());
        spinnerOverbreakCausedByValue.setSelection(mappingInput.getOverbreakCausedByValue());

        final ImageButton ib = (ImageButton) rootView.findViewById(R.id.overbreak_picture);
        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });

        final Button buttonShow = (Button) rootView.findViewById(R.id.overbreak_view_picture);
        buttonShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPicture();
            }
        });

        final ImageButton buttonDelete = (ImageButton) rootView.findViewById(R.id.overbreak_delete_picture);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePicture();
            }
        });

        overbreakPicturePath = getOverbreakPicturePath(getActivity().getExternalFilesDir(null).getPath());

        updateVisibilityOfOverbreakButtons(buttonShow, buttonDelete);

        if(none1.isChecked()) {
            inputInstabilityCausedBy.setEnabled(false);
            inputInstabilityLocation.setEnabled(false);
        }

        if(none2.isChecked()) {
//            inputOverbreakCausedBy.setEnabled(false);
            inputOverbreakLocationStart.setEnabled(false);
            inputOverbreakLocationEnd.setEnabled(false);
            inputOverbreakVolume.setEnabled(false);
            spinnerOverbreakCausedByValue.setEnabled(false);
            ib.setVisibility(View.INVISIBLE);
            buttonShow.setVisibility(View.INVISIBLE);
            buttonDelete.setVisibility(View.INVISIBLE);
            doDeletePicture(false);
        }

        none1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    inputInstabilityCausedBy.setText("");
                    inputInstabilityLocation.setText("");
                }

                inputInstabilityCausedBy.setEnabled(!isChecked);
                inputInstabilityLocation.setEnabled(!isChecked);
            }
        });

        none2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//                    inputOverbreakCausedBy.setText("");
                    inputOverbreakVolume.setText("");
                    inputOverbreakLocationStart.setText("");
                    inputOverbreakLocationEnd.setText("");
                    spinnerOverbreakCausedByValue.setSelection(0);
                }

//                inputOverbreakCausedBy.setEnabled(!isChecked);
                inputOverbreakLocationStart.setEnabled(!isChecked);
                inputOverbreakLocationEnd.setEnabled(!isChecked);
                inputOverbreakVolume.setEnabled(!isChecked);
                spinnerOverbreakCausedByValue.setEnabled(!isChecked);

                b1.setEnabled(!isChecked);
                b2.setEnabled(!isChecked);

                if (!isChecked) {
                    updateVisibilityOfOverbreakButtons(buttonShow, buttonDelete);
                    ib.setVisibility(View.VISIBLE);
                } else {
                    buttonShow.setVisibility(View.INVISIBLE);
                    buttonDelete.setVisibility(View.INVISIBLE);
                    ib.setVisibility(View.INVISIBLE);
                    doDeletePicture(false);
                }
            }
        });

        if(mappingFinished) {
            none1.setEnabled(false);
            none2.setEnabled(false);
            inputInstabilityCausedBy.setEnabled(false);
            inputInstabilityLocation.setEnabled(false);
//            inputOverbreakCausedBy.setEnabled(false);
            inputOverbreakLocationStart.setEnabled(false);
            inputOverbreakLocationEnd.setEnabled(false);
            inputOverbreakVolume.setEnabled(false);
            spinnerOverbreakCausedByValue.setEnabled(false);

            b1.setEnabled(false);
            b2.setEnabled(false);

            ib.setVisibility(View.INVISIBLE);
            buttonDelete.setVisibility(View.INVISIBLE);

            if (new File(overbreakPicturePath).exists()) {
                buttonShow.setVisibility(View.VISIBLE);
            } else {
                buttonShow.setVisibility(View.INVISIBLE);
            }
        }

        buttonShowPic = (Button) rootView.findViewById(R.id.overbreak_view_picture);
        buttonDeletePic = (ImageButton) rootView.findViewById(R.id.overbreak_delete_picture);

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMappingInput(Particularities mappingInput) {
        this.mappingInput = mappingInput;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public void showTimePickerDialog(int inputTimeField) {
        DialogFragment newFragment = new TimePickerFragment();
        Bundle args = new Bundle();
        args.putInt("inputTimeField", inputTimeField);
        newFragment.setArguments(args);
        newFragment.show(getChildFragmentManager(), "timePicker");
    }

    public void takePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null &&
                (overbreakPicturePath != null || !overbreakPicturePath.isEmpty())) {
            Pattern p = Pattern.compile("^(.+)" + File.separator + "overbreak\\.jpg$");
            Matcher m = p.matcher(overbreakPicturePath);
            m.find();
            File storage = new File(m.group(1));
            if (!storage.exists()) {
                storage.mkdir();
            }
            File photoFile = new File(storage, "overbreak.jpg");
            try {
                if (!photoFile.exists()) {
                    photoFile.createNewFile();
                }
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this.getContext(), "No se pudo crear el archivo temporal.", Toast.LENGTH_SHORT).show();
                return;
            }
            if (photoFile != null) {
                Uri photoUri = Uri.fromFile(photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, TAKE_OVERBREAK_PICTURE);
            }
        }
    }

    public void showPicture() {
        Intent showPictureIntent = new Intent();
        showPictureIntent.setAction(Intent.ACTION_VIEW);
        showPictureIntent.setDataAndType(Uri.fromFile(new File(overbreakPicturePath)), "image/*");
        startActivity(showPictureIntent);
    }

    public void doDeletePicture(boolean withMessages) {
        File f = new File(overbreakPicturePath);
        if (!f.exists()) {
            if (withMessages) {
                Toast.makeText(getActivity(), "El archivo no existe.", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        if (f.delete()) {
            Button buttonShow = (Button) getActivity().findViewById(R.id.overbreak_view_picture);
            ImageButton buttonDelete = (ImageButton) getActivity().findViewById(R.id.overbreak_delete_picture);
            updateVisibilityOfOverbreakButtons(buttonShow, buttonDelete);
            if (withMessages) {
                Toast.makeText(getActivity(), "Archivo borrado exitosamente.", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (withMessages) {
                Toast.makeText(getActivity(), "El archivo no se pudo borrar.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void deletePicture() {
        AlertDialog.OnClickListener shouldDeleteClickListener = new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == AlertDialog.BUTTON_POSITIVE) {
                    doDeletePicture(true);
                }
            }
        };
        AlertDialog.Builder shouldDeleteBuilder = new AlertDialog.Builder(this.getContext());
        shouldDeleteBuilder.setMessage("¿Desea borrar la foto tomada?").setPositiveButton("Sí", shouldDeleteClickListener)
                .setNegativeButton("No", shouldDeleteClickListener).show();
    }

    public void setOverbreakPicturePath(String overbreakPicturePath) {
        this.overbreakPicturePath = overbreakPicturePath;
    }

    public void updateVisibilityOfOverbreakButtons(Button buttonShow, ImageButton buttonDelete) {
        if(new File(overbreakPicturePath).exists()) {
            buttonShow.setVisibility(View.VISIBLE);
            buttonDelete.setVisibility(View.VISIBLE);
        } else {
            buttonShow.setVisibility(View.INVISIBLE);
            buttonDelete.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == TAKE_OVERBREAK_PICTURE && resultCode == RESULT_OK) {
            File pic = new File(getOverbreakPicturePath(getActivity().getExternalFilesDir(null).getPath()));
            if (pic.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(pic.getPath(), options);

                File compressedFile;
                compressedFile = Compressor.getDefault(getContext()).compressToFile(pic);
                copyFile(compressedFile, pic);

                if (options.outWidth == -1 || options.outHeight == -1) {
                    pic.delete();
                }
            }

            MappingInputFragment mif = (MappingInputFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.content_frame_gc);
            AdditionalInfoMainFragment aiff = (AdditionalInfoMainFragment) mif.getChildFragmentManager().findFragmentById(R.id.mapping_input_content);
            ParticularitiesFragment f = (ParticularitiesFragment) aiff.getCurrentTabFragment();


            if (buttonShowPic != null && buttonDeletePic != null) {
                f.updateVisibilityOfOverbreakButtons(buttonShowPic, buttonDeletePic);
            }
        }
    }

    private void copyFile(File origin, File dest) {
        try {
            FileManager fm = new FileManager();
            InputStream is = new FileInputStream(origin);
            OutputStream out = new FileOutputStream(dest);
            fm.copyFile(is, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getOverbreakPicturePath(String basePath) {
        return getFilesPath(basePath) + File.separator + "overbreak.jpg";
    }

    public String getFilesPath(String basePath) {
        if (mappingInput.getIdMapping() != null) {
            return basePath + File.separator + "media" + File.separator + mappingInput.getIdMapping().toLowerCase();
        }
        return ""; //No hay id
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        private int inputTimeField;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            Bundle args = getArguments();
            inputTimeField = args.getInt("inputTimeField");
            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            EditText mTime;

            switch(inputTimeField) {
                case 1:
                    mTime = inputOverbreakLocationEnd;
                    break;
                default:
                    mTime = inputOverbreakLocationStart;
                    break;
            }

            mTime.setText(hourOfDay + ":" + (minute < 10 ? "0" + minute : minute));
        }
    }
}
