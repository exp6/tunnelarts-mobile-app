package cl.skava.mobile.tunnelapp.presentation.views.geoclassifier.mapping.expandedtunnel;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import cl.skava.mobile.tunnelapp.R;

/**
 * Created by Carlos Vergara on 20-02-2017.
 */

public class FlexibleGrid extends View {
    private Paint paint;
    private float sizeOfExcavation;
    private float remainder;
    private double squareWidth;

    public FlexibleGrid(Context context) {
        super(context);
        init();
    }

    public FlexibleGrid(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FlexibleGrid(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(21)
    public FlexibleGrid(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init() {
        paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(1.5f);
        this.setBackgroundColor(Color.WHITE);
        squareWidth = getContext().getResources().getDisplayMetrics().widthPixels/12;
    }

    public void setSizeOfExcavation(Double sizeOfExcavation) {
        this.sizeOfExcavation = sizeOfExcavation.floatValue();
        remainder = Double.valueOf(sizeOfExcavation - Math.floor(sizeOfExcavation)).floatValue();
        int totalSize = (int)Math.ceil(squareWidth*Math.floor(sizeOfExcavation) + squareWidth*remainder);
        this.setMinimumHeight(totalSize);
        setLayoutParams(new RelativeLayout.LayoutParams(getContext().getResources().getDisplayMetrics().widthPixels, totalSize));
    }

    @Override
    public void onDraw(Canvas canvas) {
        if (sizeOfExcavation == 0) {
            return;
        }
        double height = sizeOfExcavation*squareWidth;
        float lengthOfSquares = Double.valueOf(squareWidth).floatValue();
        float y = 0;
        double limit = height;
        if (remainder > 0) {
            for (float x=0, square = 0; square < 12; x+=lengthOfSquares, square++) {
                canvas.drawRect(x, y, x + lengthOfSquares, y + lengthOfSquares*remainder, paint);
            }
            y += lengthOfSquares*remainder;
        }
        for (; y < limit; y+=lengthOfSquares) {
            for (float x=0, square = 0; square < 12; x+=lengthOfSquares, square++) {
                canvas.drawRect(x, y, x + lengthOfSquares, y + lengthOfSquares, paint);
            }
        }
    }
}
