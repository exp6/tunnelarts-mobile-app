package cl.skava.mobile.tunnelapp.data.repository;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.repository.TunnelRepository;

/**
 * Created by Jose Ignacio Vera on 16-06-2016.
 */
public class TunnelRepositoryImpl implements TunnelRepository {

    private final HashMap<Project, List<Tunnel>> tunnelsByProject = new HashMap<>();
    private CloudStoreService cloudStoreService;

    public TunnelRepositoryImpl(Context activity) {
        cloudStoreService = new CloudStoreService(activity);
        initDataset();
    }

    @Override
    public List<Tunnel> getTunnelsByProject(Project project) {
        List<Tunnel> tunnels = new ArrayList<>();
        Iterator<Project> iter = tunnelsByProject.keySet().iterator();
        Project p;

        while(iter.hasNext()) {
            p = iter.next();

            if(p.getId().equalsIgnoreCase(project.getId())) {
                tunnels = tunnelsByProject.get(p);
                break;
            }
        }
        return tunnels;
    }

    private void initDataset() {
        List<Project> ps = cloudStoreService.getProjects();

        for(Project p : ps) {
            List<Tunnel> tunnels = cloudStoreService.getTunnelsByProject(p.getId());
            tunnelsByProject.put(p, tunnels);
        }
    }
}
