package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.rockmassdescription;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMass;

/**
 * Created by Jose Ignacio Vera on 20-10-2016.
 */
public class RockMassOverviewFragment extends Fragment {

    private RockMass mappingInput;

    private List<String[]> rockTypes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_summary_rock_mass_overview, container, false);

        TextView briefd = (TextView) rootView.findViewById(R.id.briefd);
        briefd.setText(mappingInput.getBriefDesc());

        String[] array1 = getResources().getStringArray(R.array.genetic_group_dropdown_arrays);
        String[] array2 = getResources().getStringArray(R.array.jointing_dropdown_arrays);
        String[] array3 = getResources().getStringArray(R.array.null_dropdown_arrays);
        String[] array4 = getResources().getStringArray(R.array.strength_dropdown_arrays);
        String[] array5 = getResources().getStringArray(R.array.structure_dropdown_arrays);
        String[] array6 = getResources().getStringArray(R.array.water_dropdown_arrays);
        String[] array7 = getResources().getStringArray(R.array.weathering_dropdown_arrays);
        String[] array8 = getResources().getStringArray(R.array.colour_value_dropdown_arrays);
        String[] array9 = getResources().getStringArray(R.array.water_quality_dropdown_arrays);
        String[] array10 = getResources().getStringArray(R.array.colour_chroma_dropdown_arrays);
        String[] array11 = getResources().getStringArray(R.array.colour_hue_dropdown_arrays);

        rockTypes = new ArrayList<>(6);
        rockTypes.add(0, getResources().getStringArray(R.array.null_dropdown_arrays));
        rockTypes.add(1, getResources().getStringArray(R.array.igneous_plutonic_dropdown_arrays));
        rockTypes.add(2, getResources().getStringArray(R.array.igneous_volcanic_dropdown_arrays));
        rockTypes.add(3, getResources().getStringArray(R.array.pyroclastic_dropdown_arrays));
        rockTypes.add(4, getResources().getStringArray(R.array.sedimentary_clastic_dropdown_arrays));
        rockTypes.add(5, getResources().getStringArray(R.array.sedimentary_chemical_dropdown_arrays));
        rockTypes.add(6, getResources().getStringArray(R.array.metamorphic_dropdown_arrays));

        TextView array111 = (TextView) rootView.findViewById(R.id.array1);
        array111.setText(mappingInput.getGeneticGroup() == 0 ? "N/A" : array1[mappingInput.getGeneticGroup()]);

        TextView array12 = (TextView) rootView.findViewById(R.id.array2);
        array12.setText(mappingInput.getRockName() == 0 ? "N/A" : rockTypes.get(mappingInput.getGeneticGroup()-1)[mappingInput.getRockName()]);

        TextView array13 = (TextView) rootView.findViewById(R.id.array3);
        array13.setText(mappingInput.getStructure() == 0 ? "N/A" : array5[mappingInput.getStructure()]);

        TextView array14 = (TextView) rootView.findViewById(R.id.array4);
        array14.setText(mappingInput.getWeathering() == 0 ? "N/A" : array7[mappingInput.getWeathering()]);

        TextView array15 = (TextView) rootView.findViewById(R.id.array5);
        array15.setText(mappingInput.getColorValue() == 0 ? "N/A" : array8[mappingInput.getColorValue()]);

        TextView array16 = (TextView) rootView.findViewById(R.id.array6);
        array16.setText(mappingInput.getColorChroma() == 0 ? "N/A" : array10[mappingInput.getColorChroma()]);

        TextView array17 = (TextView) rootView.findViewById(R.id.array7);
        array17.setText(mappingInput.getColorHue() == 0 ? "N/A" : array11[mappingInput.getColorHue()]);

        TextView array18 = (TextView) rootView.findViewById(R.id.array8);
        array18.setText(mappingInput.getJointing() == 0 ? "N/A" : array2[mappingInput.getJointing()]);

        TextView array19 = (TextView) rootView.findViewById(R.id.array9);
        array19.setText(mappingInput.getStrength() == 0 ? "N/A" : array4[mappingInput.getStrength()]);

        TextView array20 = (TextView) rootView.findViewById(R.id.array10);
        array20.setText(mappingInput.getWater() == 0 ? "N/A" : array6[mappingInput.getWater()]);

        TextView array21 = (TextView) rootView.findViewById(R.id.array11);
        array21.setText(mappingInput.getWaterT().toString() + " [°C]");

        TextView array22 = (TextView) rootView.findViewById(R.id.array12);
        array22.setText(mappingInput.getWaterQuality() == 0 ? "N/A" : array9[mappingInput.getWaterQuality()]);

        TextView array23 = (TextView) rootView.findViewById(R.id.array13);
        array23.setText(mappingInput.getInflow().toString() + " [l/min]");

        return rootView;
    }

    public void setMappingInput(RockMass mappingInput) {
        this.mappingInput = mappingInput;
    }
}
