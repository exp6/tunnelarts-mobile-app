package cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.account;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.presentation.presenters.base.BasePresenter;
import cl.skava.mobile.tunnelapp.presentation.ui.base.BaseView;

/**
 * Created by Jose Ignacio Vera on 28-07-2016.
 */
public interface LoginPresenter extends BasePresenter {

    interface View extends BaseView {
        void onUserValidationStatus(boolean status, User user);
        void onCachedUsers(List<User> users);
    }

    void validateUser(User user);
}
