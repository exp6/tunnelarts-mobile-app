package cl.skava.mobile.tunnelapp.domain.executor.base;

/**
 * Created by Jose Ignacio Vera on 15-06-2016.
 *
 * This interface will define a class that will enable interactors to executeModel certain operations on the main (UI) thread. For example,
 * if an interactor needs to show an object to the UI this can be used to make sure the show method is called on the UI
 * thread.
 */
public interface MainThread {

    /**
     * Make runnable operation executeModel in the main thread.
     *
     * @param runnable The runnable to executeModel.
     */
    void post(final Runnable runnable);
}
