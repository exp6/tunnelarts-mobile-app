package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Discontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInstance;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.additionaldes.AdditionalDesFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.additionalinfo.AdditionalInfoMainFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.discontinuity.DiscontinuitiesFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.expandedtunnel.ExpandedTunnelFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.gsi.GsiFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.pictures.PicturesFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.qvalue.QValueFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rmr.RmrFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rockmasslith.LithologiesFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rockmasslith.RockMassFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.SummaryFragment;

/**
 * Created by Jose Ignacio Vera on 30-06-2016.
 */
public class MappingInputFragment extends Fragment {

    private static final String TAG = "MappingInputFragment";

    private TabLayout tabLayout;

    //User Selection Context
    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Module selectedModule;
    private Mapping selectedMapping;
    private MappingInputModule selectedMappingInputModule;
    private List<FormationUnit> formationUnits;
    private List<RockQuality> rockQualities;

    //Mapping Context
    private MappingInstance mMappingInstance;

    private String navFragmentInputName = "";

    private OnNavigationButtonClickListener mCallback;

    public interface OnNavigationButtonClickListener {
//        void onButtonNext();
//        void onButtonPrevious();
    }

    protected List<Mapping> mMappingDataset;

    private User user;
    private Client mClient;

    private List<MappingInputModule> mMappingInputModuleDataset;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallback = (OnNavigationButtonClickListener) getActivity();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_module_active, container, false);
        rootView.setTag(TAG);

        initComponents(rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initComponents(View rootView) {
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.removeAllTabs();

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar.setTitle(selectedProject.getName() + " - " + selectedTunnel.getName() + " - " + selectedFace.getName());
        toolbar.setSubtitle(getResources().getString(R.string.characterization_mappings_label_chainage)+": " + selectedMapping.getChainageStart() + " / " + selectedMapping.getChainageEnd()
                + " - "+getResources().getString(R.string.characterization_mappings_label_client)+": " + mClient.getName().trim()
                + " - "+getResources().getString(R.string.home_label_geologist)+": " + user.getFirstName() + " " + user.getLastName());

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        navFragmentInputName = selectedMappingInputModule.getLabel();

        switch(navFragmentInputName) {
            case "RMO":
                Log.i(TAG, "Rock Mass FRAGMENT ACTIVE");
                tabLayout.setVisibility(View.GONE);
                RockMassFragment f0 = new RockMassFragment();
                f0.setMappingInput(mMappingInstance.getMappingRockMass());
                f0.setMappingFinished(selectedMapping.getFinished());
                updateFragment(f0, R.id.mapping_input_content);
                break;
            case "LTH":
                Log.i(TAG, "Lithologies FRAGMENT ACTIVE");
                tabLayout.setVisibility(View.GONE);
                LithologiesFragment f1 = new LithologiesFragment();
                f1.setMappingInputs(mMappingInstance.getMappingLithologies());
                f1.setMappingFinished(selectedMapping.getFinished());
                f1.setFormationUnits(formationUnits);
                updateFragment(f1, R.id.mapping_input_content);
                break;
            case "DISC":
                Log.i(TAG, "Discontinuities FRAGMENT ACTIVE");
                //Discontinuity
                tabLayout.setVisibility(View.GONE);
                Bundle bundle = new Bundle();

                List<Discontinuity> discontinuitiesList = getmMappingInstance().getMappingDiscontinuities();
                Discontinuity[] discontinuityArray = new Discontinuity[discontinuitiesList.size()];
                bundle.putParcelableArray("discontinuities", discontinuitiesList.toArray(discontinuityArray));

                List<Discontinuity> deletedDiscontinuitiesList = getmMappingInstance().getDeletedDiscontinuities();
                Discontinuity[] deletedDiscontinuitiesArray = new Discontinuity[deletedDiscontinuitiesList.size()];
                bundle.putParcelableArray("deletedDiscontinuities", deletedDiscontinuitiesList.toArray(deletedDiscontinuitiesArray));

                bundle.putInt("mappingFinished", selectedMapping.getFinished()? 1 : 0);

                bundle.putString("mappingId", selectedMapping.getId());

                DiscontinuitiesFragment f2 = (DiscontinuitiesFragment)Fragment.instantiate(getContext(), DiscontinuitiesFragment.class.getName(), bundle);
                /*DiscontinuitiesFragment f2 = new DiscontinuitiesFragment();
                f2.setSelectedMapping(selectedMapping);
                f2.setMappingInputs(mMappingInstance.getMappingDiscontinuities());
                f2.setMappingFinished(selectedMapping.getFinished());*/
                updateFragment(f2, R.id.mapping_input_content);
                break;
            case "AINFO":
                Log.i(TAG, "Additional Information FRAGMENT ACTIVE");
                //Additional Information
                tabLayout.setVisibility(View.GONE);
                AdditionalInfoMainFragment f3 = new AdditionalInfoMainFragment();
                f3.setSpecialFeatures(mMappingInstance.getMappingSpecialFeatures());
                f3.setRockMassHazard(mMappingInstance.getMappingRockMassHazard());
                f3.setFailureZone(mMappingInstance.getMappingFailureZone());
                f3.setParticularities(mMappingInstance.getMappingParticularities());
                f3.setMappingFinished(selectedMapping.getFinished());
//                f3.setOverbreakPicturePath(selectedMapping.getOverbreakPicturePath(getActivity().getExternalFilesDir(null).getPath()));
                updateFragment(f3, R.id.mapping_input_content);
                break;
            case "PIC":
                Log.i(TAG, "Picture FRAGMENT ACTIVE");
                //Picture
                tabLayout.setVisibility(View.GONE);
                PicturesFragment pf = new PicturesFragment();
                pf.setmFiles(mMappingInstance.getMappingPictures());
                pf.setmFilesOriginal(mMappingInstance.getMappingOriginalPictures());
                pf.setMappingPictures(mMappingInstance.getMappingPicturesData());
                pf.setMappingFinished(selectedMapping.getFinished());
                updateFragment(pf, R.id.mapping_input_content);
                break;
            case "EXPT":
                Log.i(TAG, "Expanded Tunnel FRAGMENT ACTIVE");
                //Expanded Tunnel
                tabLayout.setVisibility(View.GONE);
                ExpandedTunnelFragment ef = new ExpandedTunnelFragment();
                ef.setTunnel(this.selectedTunnel);
                ef.setMappingFinished(selectedMapping.getFinished());
                ef.setmFiles(mMappingInstance.getMappingExpandedTunnelPictures());
                ef.setMappingPicture(mMappingInstance.getMappingExpandedTunnel());
                ef.setmMappingDataset(mMappingDataset);
                ef.setSelectedMapping(selectedMapping);
                updateFragment(ef, R.id.mapping_input_content);
                break;
            case "QMET":
                Log.i(TAG, "Q Value FRAGMENT ACTIVE");
                //Q Value
                tabLayout.setVisibility(View.GONE);
                QValueFragment qf = new QValueFragment();
                qf.setMappingInput(mMappingInstance.getMappingQValue());
                qf.setqValueInputSelection(mMappingInstance.getqValueInputSelection());
                qf.setqValueSelectionMap(mMappingInstance.getqValueEvaluationMethodSelectionTypes());
                qf.setToolbarParent(toolbar);
                qf.setMappingFinished(selectedMapping.getFinished());
                qf.setqValueDiscontinuities(mMappingInstance.getqValueDiscontinuities());
                qf.setJrSelectionMap(mMappingInstance.getJrSelectionMap());
                qf.setJaSelectionMap(mMappingInstance.getJaSelectionMap());
                qf.setEmptyJrSelectionMap(mMappingInstance.getEmptyJrMap());
                qf.setEmptyJaSelectionMap(mMappingInstance.getEmptyJaMap());
                qf.setRockQualities(rockQualities);
                updateFragment(qf, R.id.mapping_input_content);
                break;
            case "RMR":
                Log.i(TAG, "RMR Value FRAGMENT ACTIVE");
                //RMR Value
                tabLayout.setVisibility(View.GONE);
                RmrFragment rf = new RmrFragment();
                rf.setMappingInput(mMappingInstance.getMappingRmr());
                rf.setRmrInputSelection(mMappingInstance.getRmrInputSelection());
                rf.setRmrSelectionMap(mMappingInstance.getRmrEvaluationMethodSelectionTypes());
                rf.setToolbarParent(toolbar);
                rf.setMappingFinished(selectedMapping.getFinished());
                updateFragment(rf, R.id.mapping_input_content);
                break;
            case "GSI":
                Log.i(TAG, "GSI Value FRAGMENT ACTIVE");
                //GSI Value
                tabLayout.setVisibility(View.GONE);
                GsiFragment gf = new GsiFragment();
                gf.setMappingFinished(selectedMapping.getFinished());
                gf.setmFiles(mMappingInstance.getMappingGsiPictures());
                gf.setMappingPicture(mMappingInstance.getMappingGsi());
                gf.setMappingInput(mMappingInstance.getMappingGsiValue());
                updateFragment(gf, R.id.mapping_input_content);
                break;
//            case "Support Recommendation":
//                Log.i(TAG, "Support Recommendation FRAGMENT ACTIVE");
//                //Support Recommendation
//                tabLayout.setVisibility(View.GONE);
//                SupportRecFragment sf = new SupportRecFragment();
//                sf.setMappingFinished(selectedMapping.getFinished());
//                updateFragment(sf, R.id.mapping_input_content);
//                break;
            case "GCOMM":
                Log.i(TAG, "Additional Description FRAGMENT ACTIVE");
                //Additional Description
                tabLayout.setVisibility(View.GONE);
//                tabLayout.addTab(tabLayout.newTab().setText("Mapping Conditions"));
//                tabLayout.addTab(tabLayout.newTab().setText("Other Information"));
                AdditionalDesFragment f4 = new AdditionalDesFragment();
                f4.setMappingInput(mMappingInstance.getMappingAdditionalDescription());
                f4.setMappingFinished(selectedMapping.getFinished());
                updateFragment(f4, R.id.mapping_input_content);
                break;
            case "SUMM":
                Log.i(TAG, "Summary FRAGMENT ACTIVE");
                //Summary
                tabLayout.setVisibility(View.GONE);
                SummaryFragment f5 = new SummaryFragment();
                f5.setFinalInput(mMappingInstance);
                f5.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedMapping, user, mClient);
                f5.setmMappingInputModuleDataset(mMappingInputModuleDataset);
                updateFragment(f5, R.id.mapping_input_content);
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void updateFragment(Fragment fragment, int container) {
        currentFragment = fragment;
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(container, fragment);
        transaction.commitAllowingStateLoss();
//        transaction.commit();
    }

    private Fragment currentFragment;

    public Fragment getCurrentFragment() {
        return currentFragment;
    }

    public void setSelectionContext(Project project,
                                    Tunnel tunnel,
                                    Face face,
                                    Module module,
                                    Mapping mapping,
                                    MappingInputModule mappingInputModule,
                                    MappingInstance mappingInstance) {
        selectedProject = project;
        selectedTunnel = tunnel;
        selectedFace = face;
        selectedModule = module;
        selectedMapping = mapping;
        selectedMappingInputModule = mappingInputModule;
        mMappingInstance = mappingInstance;
    }

    public MappingInstance getmMappingInstance() {
        return mMappingInstance;
    }

    public void setmMappingInstance(MappingInstance mMappingInstance) {
        this.mMappingInstance = mMappingInstance;
    }

    public String getNavFragmentInputName() {
        return navFragmentInputName;
    }

    public void setmMappingDataset(List<Mapping> mMappingDataset) {
        this.mMappingDataset = mMappingDataset;
    }

    public void setmClient(Client mClient) {
        this.mClient = mClient;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setFormationUnits(List<FormationUnit> formationUnits) {
        this.formationUnits = formationUnits;
    }

    public void setRockQualities(List<RockQuality> rockQualities) {
        this.rockQualities = rockQualities;
    }

    public void setmMappingInputModuleDataset(List<MappingInputModule> mMappingInputModuleDataset) {
        this.mMappingInputModuleDataset = mMappingInputModuleDataset;
    }
}
