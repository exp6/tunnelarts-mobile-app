package cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue;

import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInput;
import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInputType;

/**
 * Created by Jose Ignacio Vera on 24-08-2016.
 */
public class QValueInputGroup extends DataInputType {

    private String id;
    private String idQValueInputGroupType;
    private String codeIndex;
    private String description;
    private String descriptionEs;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdQValueInputGroupType() {
        return idQValueInputGroupType;
    }

    public void setIdQValueInputGroupType(String idQValueInputGroupType) {
        this.idQValueInputGroupType = idQValueInputGroupType;
    }

    public String getCodeIndex() {
        return codeIndex;
    }

    public void setCodeIndex(String codeIndex) {
        this.codeIndex = codeIndex;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionEs() {
        return descriptionEs;
    }

    public void setDescriptionEs(String descriptionEs) {
        this.descriptionEs = descriptionEs;
    }

    @Override
    public String toString() {
        return "QValueInputGroup{" +
                "id='" + id + '\'' +
                ", idQValueInputGroupType='" + idQValueInputGroupType + '\'' +
                ", codeIndex='" + codeIndex + '\'' +
                ", description='" + description + '\'' +
                ", descriptionEs='" + descriptionEs + '\'' +
                '}';
    }
}
