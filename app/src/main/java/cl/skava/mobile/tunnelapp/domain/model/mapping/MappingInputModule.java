package cl.skava.mobile.tunnelapp.domain.model.mapping;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public class MappingInputModule implements Serializable {

    private String name;
    private Boolean enabled;
    private Boolean completed;
    private Integer code;
    private String label;

    private List<MappingInputModule> subInputs;

    private MappingInputModule parent;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<MappingInputModule> getSubInputs() {
        return subInputs;
    }

    public void setSubInputs(List<MappingInputModule> subInputs) {
        this.subInputs = subInputs;
    }

    public MappingInputModule getParent() {
        return parent;
    }

    public void setParent(MappingInputModule parent) {
        this.parent = parent;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "MappingInputModule{" +
                "name='" + name + '\'' +
                ", enabled=" + enabled +
                ", completed=" + completed +
                ", code=" + code +
                ", label='" + label + '\'' +
                ", subInputs=" + subInputs +
                ", parent=" + parent +
                '}';
    }
}
