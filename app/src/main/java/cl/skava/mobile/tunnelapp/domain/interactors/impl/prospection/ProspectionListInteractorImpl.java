package cl.skava.mobile.tunnelapp.domain.interactors.impl.prospection;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.base.AbstractInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.prospection.ProspectionListInteractor;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.repository.ProspectionRepository;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class ProspectionListInteractorImpl extends AbstractInteractor
        implements ProspectionListInteractor {

    private ProspectionListInteractor.Callback mCallback;
    private ProspectionRepository mPositionRepository;

    public ProspectionListInteractorImpl(Executor threadExecutor,
                                         MainThread mainThread,
                                         ProspectionListInteractor.Callback callback,
                                         ProspectionRepository positionRepository
    ) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mPositionRepository = positionRepository;
    }

    private void postPositions(final List<ProspectionHole> prospections) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProspectionListRetrieved(prospections);
            }
        });
    }

    @Override
    public void run() {
        final List<ProspectionHole> prospections = getList();
        postPositions(prospections);
    }

    private List<ProspectionHole> getList() {
        final List<ProspectionHole> prospections = mPositionRepository.getList();

        if (prospections == null) {
            notifyError();
        }

        return prospections;
    }

    private void notifyError() {
//        mMainThread.post(new Runnable() {
//            @Override
//            public void run() {
//                mCallback.onRetrievalFailed("No Modules on the system.");
//            }
//        });
    }

    @Override
    public void create(ProspectionHole prospection) {
        if(mPositionRepository.insert(prospection)) {
            final List<ProspectionHole> prospections = getList();

            if(prospections == null) {
                return;
            }

            postPositions(prospections);
        }
    }

    @Override
    public void update(ProspectionHole prospection) {
        if(mPositionRepository.update(prospection)) {
            final List<ProspectionHole> prospections = getList();

            if(prospections == null) {
                return;
            }

            postPositions(prospections);
        }
    }

    @Override
    public void delete(ProspectionHole prospection) {
        if(mPositionRepository.delete(prospection)) {
            final List<ProspectionHole> prospections = getList();

            if(prospections == null) {
                return;
            }

            postPositions(prospections);
        }
    }
}
