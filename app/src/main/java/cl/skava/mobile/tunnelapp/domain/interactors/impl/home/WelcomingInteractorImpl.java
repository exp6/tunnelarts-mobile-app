package cl.skava.mobile.tunnelapp.domain.interactors.impl.home;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.base.AbstractInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.home.WelcomingInteractor;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.repository.FaceRepository;
import cl.skava.mobile.tunnelapp.domain.repository.FormationUnitRepository;
import cl.skava.mobile.tunnelapp.domain.repository.ProjectRepository;
import cl.skava.mobile.tunnelapp.domain.repository.RockQualityRepository;
import cl.skava.mobile.tunnelapp.domain.repository.TunnelRepository;

/**
 * Created by Jose Ignacio Vera on 15-06-2016.
 */
public class WelcomingInteractorImpl extends AbstractInteractor implements WelcomingInteractor {

    private WelcomingInteractor.Callback mCallback;
    private ProjectRepository mProjectsRepository;
    private TunnelRepository mTunnelRepository;
    private FaceRepository mFaceRepository;
    private FormationUnitRepository mFormationUnitRepository;
    private RockQualityRepository mRockQualityRepository;

    public WelcomingInteractorImpl(
            Executor threadExecutor,
            MainThread mainThread,
            WelcomingInteractor.Callback callback,
            ProjectRepository projectsRepository,
            TunnelRepository tunnelsRepository,
            FaceRepository facesRepository,
            FormationUnitRepository formationUnitRepository,
            RockQualityRepository rockQualityRepository
    ) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mProjectsRepository = projectsRepository;
        mTunnelRepository = tunnelsRepository;
        mFaceRepository = facesRepository;
        mFormationUnitRepository = formationUnitRepository;
        mRockQualityRepository = rockQualityRepository;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRetrievalFailed("No Projects on the system.");
            }
        });
    }

    private void postProjects(final List<Project> projects) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProjectListRetrieved(projects);
            }
        });
    }

    private void postTunnels(final List<Tunnel> tunnels) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onTunnelListRetrieved(tunnels);
            }
        });
    }

    private void postFaces(final List<Face> faces) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFaceListRetrieved(faces);
            }
        });
    }

    private void postClient(final Client client) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onClientRetrieved(client);
            }
        });
    }

    private void postFormationUnits(final HashMap<Project, List<FormationUnit>> formationUnitsByProject) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFormationUnitRetrieved(formationUnitsByProject);
            }
        });
    }

    private void postRockQuality(final HashMap<Project, List<RockQuality>> rockQualityByProject) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRockQualityRetrieved(rockQualityByProject);
            }
        });
    }

    @Override
    public void run() {
        final Client client = mProjectsRepository.getClient();
        final List<Project> projects = mProjectsRepository.getAllProjects();

        if (projects == null || projects.isEmpty()) {
            notifyError();
            return;
        }

        final List<Tunnel> tunnels = mTunnelRepository.getTunnelsByProject(projects.get(0));
        final List<Face> faces = mFaceRepository.getFacesByTunnel(tunnels.get(0));

        final HashMap<Project, List<FormationUnit>> formationUnitsByProject = mFormationUnitRepository.getFormationUnitsByProject();
        final HashMap<Project, List<RockQuality>> rockQualityByProject = mRockQualityRepository.getRockQualityByProject();

        postProjects(projects);
        postTunnels(tunnels);
        postFaces(faces);
        postClient(client);
        postFormationUnits(formationUnitsByProject);
        postRockQuality(rockQualityByProject);
    }
}
