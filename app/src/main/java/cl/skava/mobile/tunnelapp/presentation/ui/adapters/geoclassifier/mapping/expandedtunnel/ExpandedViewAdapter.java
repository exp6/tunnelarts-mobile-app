package cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.mapping.expandedtunnel;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.presentation.views.geoclassifier.mapping.expandedtunnel.FlexibleGrid;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.expandedtunnel.ExpandedTunnelFragment;

/**
 * Created by Jose Ignacio Vera on 07-07-2016.
 */
public class ExpandedViewAdapter extends RecyclerView.Adapter<ExpandedViewAdapter.ViewHolder> {

    private ImageView[] mImgs;
    private String[] initialChStr;
    private String[] finalChStr;
    private Double[] initialCh;
    private Double[] finalCh;
    private ExpandedTunnelFragment mCallback;

    private ViewHolder viewHolderPool[];

    private File[] mFiles;

    private Boolean mMappingFinished;

    public ExpandedViewAdapter(ExpandedTunnelFragment callback, ImageView[] imgs, Double[] initialCh, Double[] finalCh, File[] files, Boolean mappingFinished) {
        mCallback = callback;

        mImgs = imgs;

        this.initialCh = initialCh;
        this.finalCh = finalCh;

        initialChStr = new String[initialCh.length];
        finalChStr = new String[initialCh.length];
        ChainageNumberFormat format = new ChainageNumberFormat();

        viewHolderPool = new ViewHolder[initialChStr.length];

        for (int i = 0; i < initialChStr.length; i++) {
            initialChStr[i] = format.format(initialCh[i]);
            finalChStr[i] = format.format(finalCh[i]);
        }

        mFiles = files;
        mMappingFinished = mappingFinished;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            textView = (TextView) v.findViewById(R.id.mapping_expanded_item_text);
        }

        public TextView getTextView() {
            return textView;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_geoclassifier_mapping_expandedtunnel_item, parent, false);
        holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ImageButton editModeBtn = (ImageButton) holder.itemView.findViewById(R.id.mapping_expanded_item_btn_edit_pic);
        ImageButton deleteBtn = (ImageButton) holder.itemView.findViewById(R.id.mapping_expanded_item_btn_delete_pic);

        holder.getTextView().setText(initialChStr[position] + " - " + finalChStr[position]);

        ImageView v3 = (ImageView) holder.itemView.findViewById(R.id.mapping_expanded);

        FlexibleGrid flexibleGrid = (FlexibleGrid) holder.itemView.findViewById(R.id.flexible_grid_background);
        flexibleGrid.setSizeOfExcavation(Math.abs(initialCh[position] - finalCh[position]));
        //flexibleGrid.setMinimumHeight(521);

        editModeBtn.setVisibility(View.GONE);
        deleteBtn.setVisibility(View.GONE);

        if(mMappingFinished) {
            editModeBtn.setEnabled(false);
            deleteBtn.setEnabled(false);
        }

        if(mFiles[position] != null) {
            mImgs[position] = getImageFromFile(mFiles[position], v3);
        }

        if(mImgs[position] != null) {
            v3.setImageDrawable(mImgs[position].getDrawable());
            //flexibleGrid.setMinimumHeight(v3.getDrawable().getIntrinsicHeight());
            deleteBtn.setVisibility(View.VISIBLE);
        }

        //flexibleGrid.invalidate();

        if(position < 1) {
            editModeBtn.setVisibility(View.VISIBLE);

//            if(mFiles[position] != null) {
//                mImgs[position] = getImageFromFile(mFiles[position], v3);
//            }

            final int pos = position;

            editModeBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ActivityCompat.checkSelfPermission(mCallback.getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                        mCallback.requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    }
                    if (mCallback.isWritePermissionGranted()) {
                        try {
                            mCallback.editPicture(pos);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            deleteBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mCallback.deletePicture(pos);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mImgs.length;
    }

    class ChainageNumberFormat extends DecimalFormat {

        public ChainageNumberFormat() {
            DecimalFormatSymbols custom = new DecimalFormatSymbols();
            custom.setGroupingSeparator('+');
            custom.setDecimalSeparator(',');
            setMinimumFractionDigits(2);
            setMaximumFractionDigits(2);

            setDecimalFormatSymbols(custom);
        }
    }

    @Nullable
    private ImageView getImageFromFile(File imgFile, ImageView myImage) {
//        File imgFile1 = new  File(mCallback.getContext().getExternalFilesDir(null), "/media/c7d66dc6-a7ba-423e-baaf-38f1653c4a25/roof.png");
        if(imgFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            myImage.setImageBitmap(myBitmap);
            return myImage;
        }

        return null;
    }
}
