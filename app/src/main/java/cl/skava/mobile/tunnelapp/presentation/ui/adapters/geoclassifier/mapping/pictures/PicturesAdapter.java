package cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.mapping.pictures;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.Locale;
import java.util.NoSuchElementException;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.pictures.PicturesFragment;

/**
 * Created by Jose Ignacio Vera on 05-07-2016.
 */
public class PicturesAdapter extends RecyclerView.Adapter<PicturesAdapter.ViewHolder> {

    private int picItems;
    private PicturesFragment mCallback;
    private ImageView[] mImgs;
    private File[] mFiles;

    private Boolean mMappingFinished;

    private final String lang = Locale.getDefault().getLanguage();

    public PicturesAdapter(PicturesFragment callback, ImageView[] imgs, File[] files, Boolean mappingFinished) {
        mCallback = callback;
        mImgs = imgs;
        picItems = imgs.length;
        mFiles = files;
        mMappingFinished = mappingFinished;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            textView = (TextView) v.findViewById(R.id.mapping_picture_item_text);
        }

        public TextView getTextView() {
            return textView;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_geoclassifier_mapping_pictures_item, parent, false);
        holder = new ViewHolder(v);

        View v2 = v.findViewById(R.id.mapping_picture_item);
        View v3 = v.findViewById(R.id.mapping_picture);

        int height = parent.getMeasuredWidth() / 4;
        v.setMinimumHeight(height);
        v2.setMinimumHeight(height);
        v3.setMinimumHeight(height);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ImageButton takePicture = (ImageButton) holder.itemView.findViewById(R.id.mapping_picture_item_btn_take_pic);
        ImageButton editMode = (ImageButton) holder.itemView.findViewById(R.id.mapping_picture_item_btn_edit_pic);
//        ImageButton zoomMode = (ImageButton) holder.itemView.findViewById(R.id.mapping_picture_item_btn_view_pic);
        ImageButton undoPic = (ImageButton) holder.itemView.findViewById(R.id.mapping_picture_item_btn_undo_pic);

        final int pos = position;
        final ViewHolder holder1 = holder;

        takePicture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCallback.takePicture(pos);
            }
        });

        editMode.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCallback.editPicture(pos);
            }
        });

//        zoomMode.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                mCallback.zoomPicture(pos, v);
//            }
//        });

        undoPic.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCallback.undoPicture(pos);
            }
        });

        editMode.setVisibility(View.GONE);
        undoPic.setVisibility(View.GONE);

        if(mMappingFinished) {
            takePicture.setEnabled(false);
            editMode.setEnabled(false);
            undoPic.setEnabled(false);
        }

        if(mFiles[position] != null) {
            ImageView v3 = (ImageView) holder.itemView.findViewById(R.id.mapping_picture);
            v3 = getImageFromFile(mFiles[position], v3);
            mImgs[position] = scaleImage(v3);
        }

        if(mImgs[position] != null) {
            ImageView v3 = (ImageView) holder.itemView.findViewById(R.id.mapping_picture);
            v3.setImageDrawable(mImgs[position].getDrawable());
            editMode.setVisibility(View.VISIBLE);
            undoPic.setVisibility(View.VISIBLE);
        }

        switch (position) {
            case 1:
                holder.getTextView().setText(lang.equals("es") ? "Pared Izquierda" : "Left Wall");
                break;
            case 2:
                holder.getTextView().setText(lang.equals("es") ? "Pared Derecha" : "Right Wall");
                break;
            case 3:
                holder.getTextView().setText(lang.equals("es") ? "Cubierta" : "Roof");
                break;
            default:
                holder.getTextView().setText(lang.equals("es") ? "Frente" : "Face");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return picItems;
    }

    private ImageView getImageFromFile(File imgFile, ImageView myImage) {
        if(imgFile.exists()){
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

            int reqWidth = dpToPx(250);
            int reqHeight = dpToPx(250);

            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            options.inJustDecodeBounds = false;
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

            myImage.setImageBitmap(myBitmap);
        }

        return myImage;
    }

    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private ImageView scaleImage(ImageView view) throws NoSuchElementException {
        // Get bitmap from the the ImageView.
        Bitmap bitmap = null;

        try {
            Drawable drawing = view.getDrawable();
            bitmap = ((BitmapDrawable) drawing).getBitmap();
        } catch (NullPointerException e) {
            throw new NoSuchElementException("No drawable on given view");
        }

        // Get current dimensions AND the desired bounding box
        int width = 0;

        try {
            width = bitmap.getWidth();
        } catch (NullPointerException e) {
            throw new NoSuchElementException("Can't find bitmap on given view/drawable");
        }

        int height = bitmap.getHeight();
        int bounding = dpToPx(250);

        // Determine how much to scale: the dimension requiring less scaling is
        // closer to the its side. This way the image always stays inside your
        // bounding box AND either x/y axis touches it.
        float xScale = ((float) bounding) / width;
        float yScale = ((float) bounding) / height;
        float scale = (xScale <= yScale) ? xScale : yScale;

        // Create a matrix for the scaling and add the scaling data
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);

        // Create a new bitmap and convert it to a format understood by the ImageView
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        width = scaledBitmap.getWidth(); // re-use
        height = scaledBitmap.getHeight(); // re-use
        BitmapDrawable result = new BitmapDrawable(scaledBitmap);

        // Apply the scaled bitmap
        view.setImageDrawable(result);

        return view;
    }

    private int dpToPx(int dp) {
        float density = mCallback.getActivity().getApplicationContext().getResources().getDisplayMetrics().density;
        return Math.round((float)dp * density);
    }
}
