package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse;

import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputGroupType;

/**
 * Created by Carlos Vergara on 20-03-2017.
 */

public class MappingSyncResponseHandler extends FetchFromTableSyncResponseHandler<Mapping> {
    MappingSyncResponseHandler() {

    }

    @Override
    protected void onDataFetchedSuccessfully(CloudMobileService cloudMobileService, List<Mapping> mappings) {
        CloudStoreService cloudStoreService = (CloudStoreService) cloudMobileService;
        if(cloudStoreService.isSyncAll()) cloudStoreService.syncMappingsInputs(mappings);
        else {
            List<Mapping> selectedMappings = cloudStoreService.getSelectedMappings();
            cloudStoreService.syncMappingsInputs(selectedMappings);
        }
    }

    @Override
    protected void onDataFetchedFailed(CloudMobileService cloudMobileService) {
        CloudStoreService cloudStoreService = (CloudStoreService) cloudMobileService;
        if (cloudStoreService.isSyncAll()) {
            cloudStoreService.syncSingleTable(QValueInputGroupType.class);
        } else {
            cloudStoreService.onSyncResult(1);
        }
    }

    @Override
    protected void initializeClassToFetchFrom() {
        setClassToFetchFrom(Mapping.class);
    }
}
