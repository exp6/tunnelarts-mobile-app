package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Discontinuity;
import cl.skava.mobile.tunnelapp.presentation.views.geoclassifier.mapping.summary.StereonetView;

/**
 * Created by JoseVera on 4/11/2017.
 */

public class StereonetFragment extends Fragment {

    private List<Double> dipList;
    private List<Double> dipDirList;

    private List<Discontinuity> discontinuities;

    public StereonetFragment() {}

    public static StereonetFragment newInstance() {
        StereonetFragment fragment = new StereonetFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_summary_stereonet_report, container, false);

        dipList = new ArrayList<>();
        dipDirList = new ArrayList<>();

        TableLayout dipdirTable = (TableLayout) rootView.findViewById(R.id.stereonet_table);
        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (3*scale + 0.5f);

        if(discontinuities.isEmpty()) {
            dipdirTable.setVisibility(View.GONE);
        }
        else {
            for(int i = 0; i < discontinuities.size(); i++) {
                TableRow tableRow = new TableRow(getContext());
                TableRow.LayoutParams layoutParamsTable = new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f);
                layoutParamsTable.setMargins(dpAsPixels,0,0,0);

                TextView tvDip = new TextView(getActivity());
                tvDip.setLayoutParams(layoutParamsTable);
                tvDip.setText((i+1)+"");
                tvDip.setTextColor(Color.BLACK);
                tvDip.setPadding(dpAsPixels,0,0,0);
                tableRow.addView(tvDip);

                TextView tvDipDir = new TextView(getActivity());
                tvDipDir.setLayoutParams(layoutParamsTable);
                tvDipDir.setText(discontinuities.get(i).getOrientationD().intValue() + " / " + discontinuities.get(i).getOrientationDd().intValue());
                tvDipDir.setTextColor(Color.BLACK);
                tvDipDir.setPadding(dpAsPixels,0,0,0);
                tableRow.addView(tvDipDir);

                dipdirTable.addView(tableRow);

                dipList.add(new Double(discontinuities.get(i).getOrientationD()));
                dipDirList.add(new Double(discontinuities.get(i).getOrientationDd()));
            }
        }

//        StereonetView stereonetView = new StereonetView(getContext(), dipList, dipDirList);
        StereonetView stereonetView = new StereonetView(getContext(), discontinuities);
        LinearLayout linearLayout = (LinearLayout) rootView.findViewById(R.id.stereonet_layout);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.gravity = Gravity.END;

        linearLayout.addView(stereonetView, layoutParams);

        return rootView;
    }

    public void setDiscontinuities(List<Discontinuity> discontinuities) {
        this.discontinuities = discontinuities;
    }
}
