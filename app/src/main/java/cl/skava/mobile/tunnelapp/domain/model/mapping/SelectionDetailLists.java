package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by Jose Ignacio Vera on 25-10-2016.
 */
public class SelectionDetailLists {

    private String[] structure;
    private String[] jointing;
    private String[] waterDropdown;
    private String[] waterQuality;
    private String[] geneticGroup;
    private String[] strength;
    private String[] colourValue;
    private String[] colourChroma;
    private String[] colourHue;
    private String[] texture;
    private String[] grainSize;
    private String[] type;
    private String[] relevance;
    private String[] spacing;
    private String[] persistence;
    private String[] opening;
    private String[] roughness;
    private String[] infilling;
    private String[] infillingType;
    private String[] weathering;
    private String[] typeOfFault;
    private String[] blockSize;
    private String[] blockShape;
    private String[] blockShape2;
    private String[] faceDistance;
    private String[] responsible;
    private String[] reason;
    private String[] light;
    private String[] air;
    private String[] yesNo;
    private String[] igneousPlutonic;
    private String[] igneousVolcanic;
    private String[] pyroclastic;
    private String[] sedimentaryClastic;
    private String[] sedimentaryChemical;
    private String[] senseOfMovement;
    private String[] typeOfBolt;
    private String[] boltsDiameter;
    private String[] shotcrete;
    private String[] location;
    private String[] metamorphic;

    public String[] getStructure() {
        return structure;
    }

    public void setStructure(String[] structure) {
        this.structure = structure;
    }

    public String[] getJointing() {
        return jointing;
    }

    public void setJointing(String[] jointing) {
        this.jointing = jointing;
    }

    public String[] getWaterDropdown() {
        return waterDropdown;
    }

    public void setWaterDropdown(String[] waterDropdown) {
        this.waterDropdown = waterDropdown;
    }

    public String[] getWaterQuality() {
        return waterQuality;
    }

    public void setWaterQuality(String[] waterQuality) {
        this.waterQuality = waterQuality;
    }

    public String[] getGeneticGroup() {
        return geneticGroup;
    }

    public void setGeneticGroup(String[] geneticGroup) {
        this.geneticGroup = geneticGroup;
    }

    public String[] getStrength() {
        return strength;
    }

    public void setStrength(String[] strength) {
        this.strength = strength;
    }

    public String[] getColourValue() {
        return colourValue;
    }

    public void setColourValue(String[] colourValue) {
        this.colourValue = colourValue;
    }

    public String[] getColourChroma() {
        return colourChroma;
    }

    public void setColourChroma(String[] colourChroma) {
        this.colourChroma = colourChroma;
    }

    public String[] getColourHue() {
        return colourHue;
    }

    public void setColourHue(String[] colourHue) {
        this.colourHue = colourHue;
    }

    public String[] getTexture() {
        return texture;
    }

    public void setTexture(String[] texture) {
        this.texture = texture;
    }

    public String[] getGrainSize() {
        return grainSize;
    }

    public void setGrainSize(String[] grainSize) {
        this.grainSize = grainSize;
    }

    public String[] getType() {
        return type;
    }

    public void setType(String[] type) {
        this.type = type;
    }

    public String[] getRelevance() {
        return relevance;
    }

    public void setRelevance(String[] relevance) {
        this.relevance = relevance;
    }

    public String[] getSpacing() {
        return spacing;
    }

    public void setSpacing(String[] spacing) {
        this.spacing = spacing;
    }

    public String[] getPersistence() {
        return persistence;
    }

    public void setPersistence(String[] persistence) {
        this.persistence = persistence;
    }

    public String[] getOpening() {
        return opening;
    }

    public void setOpening(String[] opening) {
        this.opening = opening;
    }

    public String[] getRoughness() {
        return roughness;
    }

    public void setRoughness(String[] roughness) {
        this.roughness = roughness;
    }

    public String[] getInfilling() {
        return infilling;
    }

    public void setInfilling(String[] infilling) {
        this.infilling = infilling;
    }

    public String[] getInfillingType() {
        return infillingType;
    }

    public void setInfillingType(String[] infillingType) {
        this.infillingType = infillingType;
    }

    public String[] getWeathering() {
        return weathering;
    }

    public void setWeathering(String[] weathering) {
        this.weathering = weathering;
    }

    public String[] getTypeOfFault() {
        return typeOfFault;
    }

    public void setTypeOfFault(String[] typeOfFault) {
        this.typeOfFault = typeOfFault;
    }

    public String[] getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(String[] blockSize) {
        this.blockSize = blockSize;
    }

    public String[] getBlockShape() {
        return blockShape;
    }

    public void setBlockShape(String[] blockShape) {
        this.blockShape = blockShape;
    }

    public String[] getBlockShape2() {
        return blockShape2;
    }

    public void setBlockShape2(String[] blockShape2) {
        this.blockShape2 = blockShape2;
    }

    public String[] getFaceDistance() {
        return faceDistance;
    }

    public void setFaceDistance(String[] faceDistance) {
        this.faceDistance = faceDistance;
    }

    public String[] getResponsible() {
        return responsible;
    }

    public void setResponsible(String[] responsible) {
        this.responsible = responsible;
    }

    public String[] getReason() {
        return reason;
    }

    public void setReason(String[] reason) {
        this.reason = reason;
    }

    public String[] getLight() {
        return light;
    }

    public void setLight(String[] light) {
        this.light = light;
    }

    public String[] getAir() {
        return air;
    }

    public void setAir(String[] air) {
        this.air = air;
    }

    public String[] getYesNo() {
        return yesNo;
    }

    public void setYesNo(String[] yesNo) {
        this.yesNo = yesNo;
    }

    public String[] getIgneousPlutonic() {
        return igneousPlutonic;
    }

    public void setIgneousPlutonic(String[] igneousPlutonic) {
        this.igneousPlutonic = igneousPlutonic;
    }

    public String[] getIgneousVolcanic() {
        return igneousVolcanic;
    }

    public void setIgneousVolcanic(String[] igneousVolcanic) {
        this.igneousVolcanic = igneousVolcanic;
    }

    public String[] getPyroclastic() {
        return pyroclastic;
    }

    public void setPyroclastic(String[] pyroclastic) {
        this.pyroclastic = pyroclastic;
    }

    public String[] getSedimentaryClastic() {
        return sedimentaryClastic;
    }

    public void setSedimentaryClastic(String[] sedimentaryClastic) {
        this.sedimentaryClastic = sedimentaryClastic;
    }

    public String[] getSedimentaryChemical() {
        return sedimentaryChemical;
    }

    public void setSedimentaryChemical(String[] sedimentaryChemical) {
        this.sedimentaryChemical = sedimentaryChemical;
    }

    public String[] getSenseOfMovement() {
        return senseOfMovement;
    }

    public void setSenseOfMovement(String[] senseOfMovement) {
        this.senseOfMovement = senseOfMovement;
    }

    public String[] getTypeOfBolt() {
        return typeOfBolt;
    }

    public void setTypeOfBolt(String[] typeOfBolt) {
        this.typeOfBolt = typeOfBolt;
    }

    public String[] getBoltsDiameter() {
        return boltsDiameter;
    }

    public void setBoltsDiameter(String[] boltsDiameter) {
        this.boltsDiameter = boltsDiameter;
    }

    public String[] getShotcrete() {
        return shotcrete;
    }

    public void setShotcrete(String[] shotcrete) {
        this.shotcrete = shotcrete;
    }

    public String[] getLocation() {
        return location;
    }

    public void setLocation(String[] location) {
        this.location = location;
    }

    public String[] getMetamorphic() {
        return metamorphic;
    }

    public void setMetamorphic(String[] metamorphic) {
        this.metamorphic = metamorphic;
    }
}
