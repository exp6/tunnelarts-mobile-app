package cl.skava.mobile.tunnelapp.domain.model;

import java.io.Serializable;

/**
 * Created by Jose Ignacio Vera on 14-09-2016.
 */
public class Client implements Serializable {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
