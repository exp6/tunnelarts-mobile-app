package cl.skava.mobile.tunnelapp.domain.interactors.impl;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.base.AbstractInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.MainInteractor;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.repository.ModuleRepository;

/**
 * Created by Jose Ignacio Vera on 24-06-2016.
 */
public class MainInteractorImpl extends AbstractInteractor
        implements MainInteractor {

    private MainInteractor.Callback mCallback;
    private ModuleRepository mModuleRepository;

    public MainInteractorImpl(
            Executor threadExecutor,
            MainThread mainThread,
            MainInteractor.Callback callback,
            ModuleRepository modulesRepository
    ) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mModuleRepository = modulesRepository;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRetrievalFailed("No Modules on the system.");
            }
        });
    }

    private void postModules(final List<Module> modules) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onModuleListRetrieved(modules);
            }
        });
    }

    @Override
    public void run() {
        final List<Module> modules = mModuleRepository.getUserModules();

        if (modules == null) {
            notifyError();
            return;
        }

        postModules(modules);
    }


}
