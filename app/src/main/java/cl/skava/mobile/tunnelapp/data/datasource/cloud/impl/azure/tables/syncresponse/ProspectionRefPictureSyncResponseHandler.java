package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;

/**
 * Created by Carlos Vergara on 20-03-2017.
 */

public class ProspectionRefPictureSyncResponseHandler implements SyncResponseHandler {
    ProspectionRefPictureSyncResponseHandler() {

    }

    @Override
    public void handleResponseFromService(CloudMobileService cloudMobileService) {
        CloudStoreService cloudStoreService = (CloudStoreService) cloudMobileService;
        cloudStoreService.incrementProgress();
        if (cloudStoreService.isProspectionPicturesOnly()) {
            cloudStoreService.setProspectionPicturesOnly(false);
            cloudStoreService.fetchForecastData();
        } else {
            cloudStoreService.onSyncResult(2);
        }
    }
}
