package cl.skava.mobile.tunnelapp.presentation.ui.activity.geoclassifier.mapping;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.executor.impl.ThreadExecutor;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.AdditionalDescription;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Discontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.ExpandedTunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.FailureZone;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Gsi;
import cl.skava.mobile.tunnelapp.domain.model.mapping.GsiValue;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Lithology;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInstance;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Particularities;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Picture;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.Rmr;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMass;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMassHazard;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SpecialFeatures;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputSelection;
import cl.skava.mobile.tunnelapp.presentation.presenters.impl.geoclassifier.MappingInputPresenterImpl;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.geoclassifier.MappingInputPresenter;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.MappingInputFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.additionaldes.AdditionalDesFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.additionalinfo.AdditionalInfoFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.additionalinfo.AdditionalInfoMainFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.additionalinfo.FailureZoneFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.additionalinfo.ParticularitiesFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.discontinuity.DiscontinuitiesFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.discontinuity.DiscontinuityFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.expandedtunnel.ExpandedTunnelFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.gsi.GsiFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.pictures.PicturesFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.qvalue.QValueFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rmr.RmrFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rockmasslith.LithologiesFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rockmasslith.LithologyFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rockmasslith.RockMassFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.SummaryFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.recommendations.RockboltsFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.recommendations.ShotcreteFragment;
import cl.skava.mobile.tunnelapp.presentation.views.geoclassifier.mapping.summary.StereonetView;
import cl.skava.mobile.tunnelapp.threading.MainThreadImpl;

/**
 * Created by Jose Ignacio Vera on 30-06-2016.
 */
public class MappingInputActivity extends AppCompatActivity
        implements MappingInputPresenter.View,
        MappingInputFragment.OnNavigationButtonClickListener {

    private static final String TAG = "MappingInputActivity";

    private CharSequence mTitle;
    private Toolbar toolbar;

    private DrawerLayout mDrawer;

    //User Selection Context
    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Module selectedModule;
    private Mapping selectedMapping;
    private MappingInputModule selectedMappingInputModule;
    protected List<MappingInputModule> mMappingInputModuleDataset;
    protected List<Mapping> mMappingDataset;

    private MappingInstance mMappingInstance;
    private String originalMappingInstance;

    private ProgressDialog uiProgressDialog;

    private MappingInputPresenter mPresenter;

    private Integer initialPosition;

    private User user;
    private Client mClient;

    private List<Integer> metaPositions;

    private int NUM_INPUTS;

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;

    private List<MappingInputModule> mappingInputsExpandedListModule;

    private Boolean firstTimeLoadSlide = true;

    private int listPosition;

//    private SparseArray<WeakReference<MappingInputFragment>> mFragments;
    private List<MappingInputFragment> mFragments;

    private MappingInputFragment currentFragment;

    private List<FormationUnit> formationUnits;
    private List<RockQuality> rockQualities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = (User) getParameter("user");
        mClient = (Client) getIntent().getSerializableExtra("client");

        if (savedInstanceState == null) {
            initComponents();
            initMainFragment();
        }
    }

    private void initComponents() {
        setContentView(R.layout.activity_geoclassifier_mapping_inputs);

        NavigationView navigationView = (NavigationView)findViewById(R.id.navigation);
        View header = navigationView.getHeaderView(0);
        TextView user_email = (TextView) header.findViewById(R.id.user_email);
        user_email.setText(user.getEmail());
        TextView user_complete_name = (TextView) header.findViewById(R.id.user_complete_name);
        user_complete_name.setText(user.getFirstName() + " " + user.getLastName());
        TextView user_role = (TextView) header.findViewById(R.id.user_role);
        user_role.setText(getResources().getString(R.string.home_label_geologist));

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                mDrawer.closeDrawers();

                switch (menuItem.getItemId()) {
                    case R.id.home:
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result", "home");
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                        return true;
                    case R.id.logout:
                        Intent returnIntent1 = new Intent();
                        returnIntent1.putExtra("result", "login");
                        setResult(Activity.RESULT_OK, returnIntent1);
                        finish();
                        return true;
                    default:
//                        Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                        return true;

                }
            }
        });
    }

    private void initMainFragment() {
        uiProgressDialog = new ProgressDialog(MappingInputActivity.this, R.style.AppTheme_Dark_Dialog);
        uiProgressDialog.setCancelable(false);
        uiProgressDialog.setMessage(getResources().getString(R.string.system_label_preparing_user_interface));
        uiProgressDialog.show();

        selectedProject = (Project) getParameter("project");
        selectedTunnel = (Tunnel) getParameter("tunnel");
        selectedFace = (Face) getParameter("face");
        selectedModule = (Module) getParameter("module");
        selectedMapping = (Mapping) getParameter("mapping");
        mMappingInputModuleDataset = (List<MappingInputModule>) getParameter("mapping_input_list");
        mMappingDataset = (List<Mapping>) getParameter("mapping_list");
        formationUnits = (List<FormationUnit>) getParameter("formationUnits");
        rockQualities = (List<RockQuality>) getParameter("rockQualities");

        initialPosition = (Integer) getParameter("mapping_input_position");

        metaPositions = (List<Integer>) getParameter("meta_positions");

//        setTotalMappingInputs();

        mPresenter = new MappingInputPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                this);
        mPresenter.getMappingInputs(selectedMapping, mMappingInputModuleDataset);

//        updateMappingInputFragment(initialPosition);
    }

    public Object getParameter(String key) {
        return getIntent().getSerializableExtra(key);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    private void saveFinalData() {
        MappingInputFragment f1 = (MappingInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame_gc);
        //saveMappingInputData(f1, mMappingInstance);
        mMappingInstance = getModifiedMappingInputData(f1, mMappingInstance);

        uiProgressDialog = new ProgressDialog(MappingInputActivity.this, R.style.AppTheme_Dark_Dialog);
        uiProgressDialog.setMessage(getResources().getString(R.string.characterization_mappings_label_saving_inputs));
        uiProgressDialog.show();

        mPresenter.saveMappingInputs(mMappingInstance, mMappingInputModuleDataset);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        MappingInputModule input;
        Intent returnIntent;

        switch (item.getItemId()) {
            case android.R.id.home:
                if(selectedMapping.getFinished()) {
                    returnIntent = new Intent();
                    returnIntent.putExtra("result", "mappingInputs");
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
                else {
                    final Context c = this;

                    Handler h = new Handler(Looper.getMainLooper());
                    h.post(new Runnable() {
                        public void run() {
                            quitActivity();
                        }
                    });
                }
                return true;

            case R.id.action_user:
                if (mDrawer.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawer.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawer.openDrawer(Gravity.RIGHT);
                }
                return true;

            case R.id.action_next_mapping_input:
                input = findNextInput();

                if(input != null) {
                    updateMappingInputFragment(input);
                }
                else {
                    if(selectedMapping.getFinished()) {
                        returnIntent = new Intent();
                        returnIntent.putExtra("result", "mappingInputs");
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                    else {
                        final Context c = this;

                        Handler h = new Handler(Looper.getMainLooper());
                        h.post(new Runnable() {
                            public void run() {
                                quitActivity();
                            }
                        });
                    }
                }
                return true;

            case R.id.action_previous_mapping_input:
                input = findPreviousInput();

                if(input != null) {
                    updateMappingInputFragment(input);
                }
                else {
                    if(selectedMapping.getFinished()) {
                        returnIntent = new Intent();
                        returnIntent.putExtra("result", "mappingInputs");
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                    else {
                        final Context c = this;

                        Handler h = new Handler(Looper.getMainLooper());
                        h.post(new Runnable() {
                            public void run() {
                                quitActivity();
                            }
                        });
                    }
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void quitActivity() {
        MappingInputFragment f1 = (MappingInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame_gc);
        if (!getModifiedMappingInputData(f1, mMappingInstance).toString().equals(originalMappingInstance)) {
            showConfirmSaveChanges();
        } else {
            returnToPrevious();
        }
    }

    private void showConfirmSaveChanges() {
        new android.support.v7.app.AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.characterization_mappings_label_quit_mapping_section))
                .setMessage(getResources().getString(R.string.characterization_mappings_label_save_all_changes))
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(getResources().getString(R.string.system_label_yes), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        saveFinalData();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        returnToPrevious();
                    }
                })
                .show();
    }

    private void returnToPrevious() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", "mappingInputs");
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private MappingInputModule findNextInput() {
        MappingInputModule parent = selectedMappingInputModule.getParent();
        MappingInputModule input = null;

        if(parent != null) {
            List<MappingInputModule> subInputs = parent.getSubInputs();
            Integer code = selectedMappingInputModule.getCode();

            if((code + 1) < subInputs.size()) {
                input = subInputs.get(code+1);
            }
            else {
                selectedMappingInputModule = parent;
                input = findNextParent();
            }
        }
        else {
            input = findNextParent();
        }

        return input;
    }

    private MappingInputModule findNextParent() {
        MappingInputModule input = null;

        Integer code = selectedMappingInputModule.getCode();
        if((code + 1) < mMappingInputModuleDataset.size()) {
            input = mMappingInputModuleDataset.get(code + 1);

            if(input.getSubInputs() != null) {
                input = input.getSubInputs().get(0);
            }
        }

        return input;
    }

    private MappingInputModule findPreviousInput() {
        MappingInputModule parent = selectedMappingInputModule.getParent();
        MappingInputModule input = null;

        if(parent != null) {
            List<MappingInputModule> subInputs = parent.getSubInputs();
            Integer code = selectedMappingInputModule.getCode();

            if((code - 1) >= 0) {
                input = subInputs.get(code - 1);
            }
            else {
                selectedMappingInputModule = parent;
                input = findPreviousParent();
            }
        }
        else {
            input = findPreviousParent();
        }

        return input;
    }

    private MappingInputModule findPreviousParent() {
        MappingInputModule input = null;

        Integer code = selectedMappingInputModule.getCode();
        if((code - 1) >= 0) {
            input = mMappingInputModuleDataset.get(code - 1);

            if(input.getSubInputs() != null) {
                input = input.getSubInputs().get(input.getSubInputs().size()-1);
            }
        }

        return input;
    }

    private void updateMappingInputFragment(MappingInputModule mappingInputModule) {
        MappingInputFragment f1 = (MappingInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame_gc);

        if(f1 != null) {
            mMappingInstance = getModifiedMappingInputData(f1, mMappingInstance);
        }

        selectedMapping = mMappingInstance.getMapping();

        selectedMappingInputModule = mappingInputModule;
        String inputTitle = selectedMappingInputModule.getParent() != null ? selectedMappingInputModule.getParent().getName() + " - " + selectedMappingInputModule.getName() : selectedMappingInputModule.getName();
        setTitle(getApplicationName(this) + " - " + selectedModule.getName() + " - " + inputTitle);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        MappingInputFragment fragment = new MappingInputFragment();
        fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, selectedMapping, selectedMappingInputModule, mMappingInstance);
        fragment.setmMappingDataset(mMappingDataset);
        fragment.setmClient(mClient);
        fragment.setUser(user);
        fragment.setFormationUnits(formationUnits);
        fragment.setRockQualities(rockQualities);
        fragment.setmMappingInputModuleDataset(mappingInputsExpandedListModule);

        transaction.replace(R.id.content_frame_gc, fragment);
        transaction.commit();
    }

//    private void updateMappingInputFragment(MappingInputModule mappingInput) {
//        selectedMappingInputModule = mappingInput;
//        String inputTitle = selectedMappingInputModule.getParent() != null ? selectedMappingInputModule.getParent().getName() + " - " + selectedMappingInputModule.getName() : selectedMappingInputModule.getName();
//        setTitle(getApplicationName(this) + " - " + selectedModule.getName() + " - " + inputTitle);
//
//        final String applicationName = getApplicationName(this);
//
//        listPosition = 0;
//
//        for(int i = 0; i < mappingInputsExpandedListModule.size(); i++) {
//            MappingInputModule mappingInput1 = mappingInputsExpandedListModule.get(i);
//            if(mappingInput1.getName().trim().equalsIgnoreCase(mappingInput.getName().trim()) &&
//                    mappingInput1.getCode().intValue() == mappingInput.getCode().intValue()) {
//                listPosition = i;
//                break;
//            }
//        }
//
//        mPager = (ViewPager) findViewById(R.id.content_frame_gc);
//        mPager.addOnPageChangeListener(
//                new ViewPager.SimpleOnPageChangeListener() {
//                    @Override
//                    public void onPageSelected(int position) {
//                        if (!firstTimeLoadSlide) {
//                            MappingInputFragment f1 = ((ScreenSlidePagerAdapter) mPager.getAdapter()).getInputFragments(listPosition);
//                            saveMappingInputData(f1, mMappingInstance);
//
//                            MappingInputModule mappingInput1 = mappingInputsExpandedListModule.get(position);
//                            selectedMappingInputModule = mappingInput1;
//                            String inputTitle = mappingInput1.getParent() != null ? mappingInput1.getParent().getName() + " - " + mappingInput1.getName() : mappingInput1.getName();
//                            setTitle(applicationName + " - " + selectedModule.getName() + " - " + inputTitle);
//
//                        } else {
//                            firstTimeLoadSlide = false;
//                        }
//
//                        listPosition = position;
//                    }
//
//                });
//
//        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), mFragments);
//        mPager.setAdapter(mPagerAdapter);
//        mPager.setCurrentItem(listPosition);
//    }

    private MappingInstance getModifiedMappingInputData(MappingInputFragment fragment, MappingInstance mappingInstance) {

        Fragment f = fragment.getChildFragmentManager().findFragmentById(R.id.mapping_input_content);
//        Fragment f = fragment.getCurrentFragment();

        switch(fragment.getNavFragmentInputName()) {
            case "RMO":
                RockMassFragment rmf = (RockMassFragment) f;
                Spinner spinnerStructure = (Spinner) rmf.getView().findViewById(R.id.spinner_structure);
                Spinner spinnerJointing = (Spinner) rmf.getView().findViewById(R.id.spinner_jointing);
                Spinner spinnerWater = (Spinner) rmf.getView().findViewById(R.id.spinner_water);
                String inputWaterT = ((EditText) rmf.getView().findViewById(R.id.input_water_t)).getText().toString();
                Spinner spinnerWaterQuality = (Spinner) rmf.getView().findViewById(R.id.spinner_water_quality);
                String inputInflow = ((EditText) rmf.getView().findViewById(R.id.input_inflow)).getText().toString();
                Spinner spinnerGeneticGroup = (Spinner) rmf.getView().findViewById(R.id.spinner_genetic_group);
                Spinner rockName = (Spinner) rmf.getView().findViewById(R.id.spinner_rock_name);
                Spinner weathering = (Spinner) rmf.getView().findViewById(R.id.spinner_weathering);
                Spinner colorValue = (Spinner) rmf.getView().findViewById(R.id.spinner_colour_value);
                Spinner colorChroma = (Spinner) rmf.getView().findViewById(R.id.spinner_colour_chroma);
                Spinner colorHue = (Spinner) rmf.getView().findViewById(R.id.spinner_colour_hue);
                Spinner strength = (Spinner) rmf.getView().findViewById(R.id.spinner_strength);

                RockMass rm = mappingInstance.getMappingRockMass();
                rm.setStructure(spinnerStructure.getSelectedItemPosition());
                rm.setJointing(spinnerJointing.getSelectedItemPosition());
                rm.setWater(spinnerWater.getSelectedItemPosition());
                rm.setWaterT(Float.parseFloat(inputWaterT));
                rm.setWaterQuality(spinnerWaterQuality.getSelectedItemPosition());
                rm.setInflow(Float.parseFloat(inputInflow));
                rm.setGeneticGroup(spinnerGeneticGroup.getSelectedItemPosition() > 0 ? spinnerGeneticGroup.getSelectedItemPosition()+1 : 0);
                rm.setRockName(rockName.getSelectedItemPosition());
                rm.setWeathering(weathering.getSelectedItemPosition());
                rm.setColorValue(colorValue.getSelectedItemPosition());
                rm.setColorChroma(colorChroma.getSelectedItemPosition());
                rm.setColorHue(colorHue.getSelectedItemPosition());
                rm.setStrength(strength.getSelectedItemPosition());
                rm.setCompleted(!inputWaterT.trim().equals("")
                        && !inputInflow.trim().equals("")
                        && spinnerStructure.getSelectedItemPosition() != 0
                        && spinnerJointing.getSelectedItemPosition() != 0
                        && spinnerWater.getSelectedItemPosition() != 0
                        && spinnerWaterQuality.getSelectedItemPosition() != 0
                        && spinnerGeneticGroup.getSelectedItemPosition() != 0
                        && rockName.getSelectedItemPosition() != 0
                        && weathering.getSelectedItemPosition() != 0
                        && colorValue.getSelectedItemPosition() != 0
                        && colorChroma.getSelectedItemPosition() != 0
                        && colorHue.getSelectedItemPosition() != 0
                        && strength.getSelectedItemPosition() != 0);
                rm.setBriefDesc(rmf.getBriefDesc().getText().toString());
                mappingInstance.setMappingRockMass(rm);
                break;
            case "LTH":
                LithologiesFragment lf = (LithologiesFragment) f;
                LithologyFragment lf1 = lf.getCurrentTabFragment();

                EditText inputPresence = ((EditText) lf1.getView().findViewById(R.id.input_presence));
                Spinner spinnerStrength = (Spinner) lf1.getView().findViewById(R.id.spinner_strength);
                Spinner spinnerColourValue = (Spinner) lf1.getView().findViewById(R.id.spinner_colour_value);
                Spinner spinnerColourChroma = (Spinner) lf1.getView().findViewById(R.id.spinner_colour_chroma);
                Spinner spinnerColourHue = (Spinner) lf1.getView().findViewById(R.id.spinner_colour_hue);
                Spinner spinnerTexture = (Spinner) lf1.getView().findViewById(R.id.spinner_texture);
                Spinner spinnerAlterationWeathering = (Spinner) lf1.getView().findViewById(R.id.spinner_alteration_w);
                Spinner spinnerGrainSize = (Spinner) lf1.getView().findViewById(R.id.spinner_grain_size);
                Spinner spinnerGeneticGroup1 = (Spinner) lf1.getView().findViewById(R.id.spinner_genetic_group);
                Spinner spinnerSubGroup = (Spinner) lf1.getView().findViewById(R.id.spinner_rock_name);
                EditText inputJvMin = ((EditText) lf1.getView().findViewById(R.id.input_jv_min));
                EditText inputJvMax = ((EditText) lf1.getView().findViewById(R.id.input_jv_max));
                Spinner spinnerBlockShape = (Spinner) lf1.getView().findViewById(R.id.spinner_block_shape);
                EditText inputBlockShapeL = ((EditText) lf1.getView().findViewById(R.id.input_block_shape_l));
                EditText inputBlockShapeW = ((EditText) lf1.getView().findViewById(R.id.input_block_shape_w));
                EditText inputBlockShapeH = ((EditText) lf1.getView().findViewById(R.id.input_block_shape_h));

                Lithology lith = lf1.getMappingInput();
                lith.setPresence(Float.parseFloat(inputPresence.getText().toString()));
                lith.setStrength(spinnerStrength.getSelectedItemPosition());
                lith.setColourValue(spinnerColourValue.getSelectedItemPosition());
                lith.setColourChroma(spinnerColourChroma.getSelectedItemPosition());
                lith.setColorHue(spinnerColourHue.getSelectedItemPosition());
                lith.setTexture(spinnerTexture.getSelectedItemPosition());
                lith.setAlterationWeathering(spinnerAlterationWeathering.getSelectedItemPosition());
                lith.setGrainSize(spinnerGrainSize.getSelectedItemPosition());
                lith.setGeneticGroup(spinnerGeneticGroup1.getSelectedItemPosition() > 0 ? spinnerGeneticGroup1.getSelectedItemPosition()+1 : 0);
                lith.setSubGroup(spinnerSubGroup.getSelectedItemPosition());
                lith.setJvMin(Integer.parseInt(inputJvMin.getText().toString()));
                lith.setJvMax(Integer.parseInt(inputJvMax.getText().toString()));
                lith.setBlockShape(spinnerBlockShape.getSelectedItemPosition());
                lith.setBlockSizeOne(Float.parseFloat(inputBlockShapeL.getText().toString()));
                lith.setBlockSizeTwo(Float.parseFloat(inputBlockShapeW.getText().toString()));
                lith.setBlockSizeThree(Float.parseFloat(inputBlockShapeH.getText().toString()));
                lith.setCompleted(!inputPresence.getText().toString().trim().equals("")
                        && spinnerStrength.getSelectedItemPosition() != 0
                        && spinnerColourValue.getSelectedItemPosition() != 0
                        && spinnerColourChroma.getSelectedItemPosition() != 0
                        && spinnerColourHue.getSelectedItemPosition() != 0
                        && spinnerTexture.getSelectedItemPosition() != 0
                        && spinnerAlterationWeathering.getSelectedItemPosition() != 0
                        && spinnerGrainSize.getSelectedItemPosition() != 0
                        && spinnerGeneticGroup1.getSelectedItemPosition() != 0
                        && spinnerSubGroup.getSelectedItemPosition() != 0
                        && spinnerBlockShape.getSelectedItemPosition() != 0
                        && !inputJvMin.getText().toString().trim().equals("")
                        && !inputJvMax.getText().toString().trim().equals("")
                        && !inputBlockShapeL.getText().toString().trim().equals("")
                        && !inputBlockShapeW.getText().toString().trim().equals("")
                        && !inputBlockShapeH.getText().toString().trim().equals(""));
                lith.setBriefDesc(lf1.getBriefDesc().getText().toString());

                List<Lithology> ls = lf.getMappingInputs();
                ls.set(lith.getPosition(), lith);
                mappingInstance.setMappingLithologies(ls);
                break;
            case "DISC":
                DiscontinuitiesFragment ddf = (DiscontinuitiesFragment) f;
                ddf.saveCurrentTab();
                List<Discontinuity> discontinuities = ddf.getDiscontinuities();
                mappingInstance.setMappingDiscontinuities(discontinuities);
                List<Discontinuity> deletedDiscontinuities = ddf.getDeletedDiscontinuities();
                mappingInstance.setDeletedDiscontinuities(deletedDiscontinuities);
                ddf.saveStereonet();
                break;
            case "AINFO":
                AdditionalInfoMainFragment aiff = (AdditionalInfoMainFragment) f;
                Fragment ff = aiff.getCurrentTabFragment();

                if(ff instanceof AdditionalInfoFragment) {
                    AdditionalInfoFragment aif = (AdditionalInfoFragment) ff;

                    CheckBox chkNone1 = (CheckBox) aif.getView().findViewById(R.id.chk_none1);
                    CheckBox chkZeolites = (CheckBox) aif.getView().findViewById(R.id.chk_zeolites);
                    CheckBox chkClay = (CheckBox) aif.getView().findViewById(R.id.chk_clay);
                    CheckBox chkChlorite = (CheckBox) aif.getView().findViewById(R.id.chk_chlorite);
                    CheckBox chkSulfides = (CheckBox) aif.getView().findViewById(R.id.chk_sulfides);
                    CheckBox chkSulfates = ((CheckBox) aif.getView().findViewById(R.id.chk_sulfates));

                    CheckBox chkNone2 = (CheckBox) aif.getView().findViewById(R.id.chk_none2);
                    CheckBox chkRockBurst = (CheckBox) aif.getView().findViewById(R.id.chk_rock_burst);
                    CheckBox chkSwelling = ((CheckBox) aif.getView().findViewById(R.id.chk_swelling));
                    CheckBox chkSqueezing = (CheckBox) aif.getView().findViewById(R.id.chk_squeezing);
                    CheckBox chkSlaking = ((CheckBox) aif.getView().findViewById(R.id.chk_slaking));

                    SpecialFeatures sf = mappingInstance.getMappingSpecialFeatures();
                    sf.setNone(chkNone1.isChecked());
                    sf.setZeolites(chkZeolites.isChecked());
                    sf.setClay(chkClay.isChecked());
                    sf.setChlorite(chkChlorite.isChecked());
                    sf.setSulfides(chkSulfides.isChecked());
                    sf.setSulfates(chkSulfates.isChecked());
                    sf.setCompleted(sf.getNone()
                                    || sf.getZeolites()
                                    || sf.getClay()
                                    || sf.getChlorite()
                                    || sf.getRedTuff()
                                    || sf.getSulfides()
                                    || sf.getSulfates());

                    RockMassHazard rmh = mappingInstance.getMappingRockMassHazard();
                    rmh.setNone(chkNone2.isChecked());
                    rmh.setRockBurst(chkRockBurst.isChecked());
                    rmh.setSwelling(chkSwelling.isChecked());
                    rmh.setSqueezing(chkSqueezing.isChecked());
                    rmh.setSlaking(chkSlaking.isChecked());
                    rmh.setCompleted(rmh.getNone()
                            || rmh.getRockBurst()
                            || rmh.getSwelling()
                            || rmh.getSqueezing()
                            || rmh.getSlaking());

                    mappingInstance.setMappingSpecialFeatures(sf);
                    mappingInstance.setMappingRockMassHazard(rmh);
                }

                if(ff instanceof FailureZoneFragment) {
                    FailureZoneFragment fzf = (FailureZoneFragment) ff;
                    Spinner spinnerTypeFault = (Spinner) fzf.getView().findViewById(R.id.spinner_sense_movement);
                    EditText inputOrientationDD = ((EditText) fzf.getView().findViewById(R.id.input_orientation_dd));
                    EditText inputOrientationD = ((EditText) fzf.getView().findViewById(R.id.input_orientation_d));
                    EditText inputThickness = ((EditText) fzf.getView().findViewById(R.id.input_thickness));
                    EditText inputMatrixBlock = ((EditText) fzf.getView().findViewById(R.id.input_matrix_Block));
                    Spinner spinnerMatrixColourValue = (Spinner) fzf.getView().findViewById(R.id.spinner_matrix_colour_value);
                    Spinner spinnerMatrixColourChroma = (Spinner) fzf.getView().findViewById(R.id.spinner_matrix_colour_chroma);
                    Spinner spinnerMatrixColorHue = (Spinner) fzf.getView().findViewById(R.id.spinner_matrix_colour_hue);
                    Spinner spinnerMatrixGrainSize = (Spinner) fzf.getView().findViewById(R.id.spinner_matrix_grain_size);
                    Spinner spinnerBlockSize = (Spinner) fzf.getView().findViewById(R.id.spinner_block_size);
                    Spinner spinnerBlockShape1 = (Spinner) fzf.getView().findViewById(R.id.spinner_block_shape);
                    Spinner spinnerBlockGeneticGroup = (Spinner) fzf.getView().findViewById(R.id.spinner_block_genetic_group);
                    Spinner spinnerBlockSubGroup = (Spinner) fzf.getView().findViewById(R.id.spinner_block_sub_group);
                    EditText inputRakeOfStriae = ((EditText) fzf.getView().findViewById(R.id.input_rake_striae));
                    CheckBox none = (CheckBox) fzf.getView().findViewById(R.id.chk_none);
//
                    FailureZone fz = mappingInstance.getMappingFailureZone();
                    fz.setSenseOfMovement(spinnerTypeFault.getSelectedItemPosition());
                    fz.setOrientationDd(Float.parseFloat(inputOrientationDD.getText().toString()));
                    fz.setOrientationD(Float.parseFloat(inputOrientationD.getText().toString()));
                    fz.setThickness(Float.parseFloat(inputThickness.getText().toString()));
                    fz.setMatrixBlock(Float.parseFloat(inputMatrixBlock.getText().toString()));
                    fz.setMatrixColourValue(spinnerMatrixColourValue.getSelectedItemPosition());
                    fz.setMatrixColourChroma(spinnerMatrixColourChroma.getSelectedItemPosition());
                    fz.setMatrixColourHue(spinnerMatrixColorHue.getSelectedItemPosition());
                    fz.setMatrixGainSize(spinnerMatrixGrainSize.getSelectedItemPosition());
                    fz.setBlockSize(spinnerBlockSize.getSelectedItemPosition());
                    fz.setBlockShape(spinnerBlockShape1.getSelectedItemPosition());
                    fz.setBlockGeneticGroup(spinnerBlockGeneticGroup.getSelectedItemPosition());
                    fz.setBlockSubGroup(spinnerBlockSubGroup.getSelectedItemPosition());
                    fz.setRakeOfStriae(inputRakeOfStriae.getText().toString().isEmpty() ? 0 : Float.parseFloat(inputRakeOfStriae.getText().toString()));
                    fz.setNoneRake(none.isChecked());
                    fz.setCompleted(!inputOrientationDD.getText().toString().trim().equals("")
                            && !inputOrientationD.getText().toString().trim().equals("")
                            && !inputThickness.getText().toString().trim().equals("")
                            && !inputMatrixBlock.getText().toString().trim().equals("")
                            && spinnerTypeFault.getSelectedItemPosition() != 0
                            && spinnerMatrixColourValue.getSelectedItemPosition() != 0
                            && spinnerMatrixColourChroma.getSelectedItemPosition() != 0
                            && spinnerMatrixColorHue.getSelectedItemPosition() != 0
                            && spinnerMatrixGrainSize.getSelectedItemPosition() != 0
                            && spinnerBlockSize.getSelectedItemPosition() != 0
                            && spinnerBlockShape1.getSelectedItemPosition() != 0
                            && spinnerBlockGeneticGroup.getSelectedItemPosition() != 0
                            && spinnerBlockSubGroup.getSelectedItemPosition() != 0
                            && !inputRakeOfStriae.getText().toString().trim().equals(""));
                    mappingInstance.setMappingFailureZone(fz);
                }

                if(ff instanceof ParticularitiesFragment) {
                    ParticularitiesFragment pf = (ParticularitiesFragment) ff;

                    CheckBox none1 = (CheckBox) pf.getView().findViewById(R.id.chk_none1);
                    CheckBox none2 = (CheckBox) pf.getView().findViewById(R.id.chk_none2);

                    EditText inputInstabilityCausedBy = ((EditText) pf.getView().findViewById(R.id.input_instability_caused_by));
                    EditText inputInstabilityLocation = ((EditText) pf.getView().findViewById(R.id.input_instability_location));
//                    EditText inputOverbreakCausedBy = ((EditText) pf.getView().findViewById(R.id.input_overbreak_caused_by));
                    EditText inputOverbreakVolume = ((EditText) pf.getView().findViewById(R.id.input_volume_location));
                    EditText inputOverbreakLocationStart = ((EditText) pf.getView().findViewById(R.id.input_overbreak_location_start));
                    EditText inputOverbreakLocationEnd = ((EditText) pf.getView().findViewById(R.id.input_overbreak_location_end));
                    Spinner spinnerOverbreakCausedByValue = (Spinner) pf.getView().findViewById(R.id.spinner_overbreak_caused_by);

                    Particularities p = mappingInstance.getMappingParticularities();
                    p.setInstabilityCausedBy(inputInstabilityCausedBy.getText().toString());
                    p.setInstabilityLocation(inputInstabilityLocation.getText().toString());
//                    p.setOverbreakCausedBy(inputOverbreakCausedBy.getText().toString());
                    p.setOverbreakLocationStart(inputOverbreakLocationStart.getText().toString());
                    p.setOverbreakLocationEnd(inputOverbreakLocationEnd.getText().toString());
                    p.setOverbreakVolume(inputOverbreakVolume.getText().toString().isEmpty() ? 0 : Integer.parseInt(inputOverbreakVolume.getText().toString()));
                    p.setInstabilityNa(none1.isChecked());
                    p.setOverbreakNa(none2.isChecked());
                    p.setOverbreakCausedByValue(spinnerOverbreakCausedByValue.getSelectedItemPosition());
                    p.setCompleted((!inputInstabilityCausedBy.getText().toString().trim().equals("")
                            && !inputInstabilityLocation.getText().toString().trim().equals("")
//                            && !inputOverbreakCausedBy.getText().toString().trim().equals("")
                            && !inputOverbreakLocationStart.getText().toString().trim().equals("")
                            && !inputOverbreakLocationEnd.getText().toString().trim().equals("")
                            && spinnerOverbreakCausedByValue.getSelectedItemPosition() != 0
                            && !inputOverbreakVolume.getText().toString().trim().equals("")) || (none1.isChecked() && none2.isChecked()));
                    mappingInstance.setMappingParticularities(p);
                }
                break;
            case "PIC":
                //IMAGE
                PicturesFragment pf = (PicturesFragment) f;
                File[] mappingPictures = pf.getmFiles();
                mappingInstance.setMappingPictures(mappingPictures);
                List<Picture> mappingPicturesData = pf.getMappingPictures();
                mappingInstance.setMappingPicturesData(mappingPicturesData);
                File[] mappingOriginalPictures = pf.getmFilesOriginal();
                mappingInstance.setMappingOriginalPictures(mappingOriginalPictures);
                break;
            case "EXPT":
                //IMAGE
                ExpandedTunnelFragment ef = (ExpandedTunnelFragment) f;
                File[] mappingPictures1 = ef.getmFiles();
                ExpandedTunnel expandedTunnel = ef.getMappingPicture();
                mappingInstance.setMappingExpandedTunnelPictures(mappingPictures1);
                mappingInstance.setMappingExpandedTunnel(expandedTunnel);
                break;
            case "QMET":
                QValueFragment qvf = (QValueFragment) f;
                QValue qv = qvf.getMappingInput();
                QValueInputSelection qvis = qvf.getqValueInputSelection();
                qv.setCompleted(qvis.getJa() != null
                        && qvis.getJn() != null
                        && qvis.getJr() != null
                        && qvis.getJw() != null
                        && qvis.getSrf() != null
                        && qvis.getJnIntersection() != null);

                mappingInstance.setMappingQValue(qv);
                mappingInstance.setqValueInputSelection(qvis);
                mappingInstance.setqValueDiscontinuities(qvf.getqValueDiscontinuities());
                mappingInstance.setJrSelectionMap(qvf.getJrSelectionMap());
                mappingInstance.setJaSelectionMap(qvf.getJaSelectionMap());
                break;
            case "RMR":
                RmrFragment rmrf = (RmrFragment) f;
                Rmr rmr = rmrf.getMappingInput();
                RmrInputSelection rmris = rmrf.getRmrInputSelection();
                rmr.setCompleted(rmris.getRqd() != null
                        && rmris.getSpacing() != null
                        && rmris.getPersistence() != null
                        && rmris.getOpening() != null
                        && rmris.getRoughness() != null
                        && rmris.getInfilling() != null
                        && rmris.getWeathering() != null
                        && rmris.getGroundwater() != null
                        && rmris.getOrientationDD() != null
                        && rmris.getStrength() != null);
                mappingInstance.setMappingRmr(rmr);
                mappingInstance.setRmrInputSelection(rmris);
                break;
            case "GSI":
                //IMAGE
                GsiFragment gf = (GsiFragment) f;
                File[] mappingPictures2 = gf.getmFiles();
                Gsi gsi = gf.getMappingPicture();
                GsiValue gsiValue = gf.getMappingInput();
                mappingInstance.setMappingGsiPictures(mappingPictures2);
                mappingInstance.setMappingGsi(gsi);
                mappingInstance.setMappingGsiValue(gsiValue);
                break;
//            case "Support Recommendation":
//
//                break;
            case "GCOMM":
                AdditionalDesFragment adf = (AdditionalDesFragment) f;
                Spinner spinnerDistance = (Spinner) adf.getView().findViewById(R.id.spinner_distance_to_face);
                Spinner spinnerResponsible = (Spinner) adf.getView().findViewById(R.id.spinner_responsible);
                Spinner spinnerReason = (Spinner) adf.getView().findViewById(R.id.spinner_reason);
                Spinner spinnerLightQuality = ((Spinner) adf.getView().findViewById(R.id.spinner_light_quality));
                Spinner spinnerAirQuality = (Spinner) adf.getView().findViewById(R.id.spinner_air_quality);
                String inputTime = ((EditText) adf.getView().findViewById(R.id.input_time)).getText().toString();
                Spinner spinnerDamage = (Spinner) adf.getView().findViewById(R.id.spinner_damage);
                String inputDescription = ((EditText) adf.getView().findViewById(R.id.input_description)).getText().toString();

                AdditionalDescription ad = mappingInstance.getMappingAdditionalDescription();
                ad.setDistanceToFace(spinnerDistance.getSelectedItemPosition());
                ad.setResponsibleForRestrictedArea(spinnerResponsible.getSelectedItemPosition());
                ad.setReasonForRestrictedArea(spinnerReason.getSelectedItemPosition());
                ad.setLightQuality(spinnerLightQuality.getSelectedItemPosition());
                ad.setAirQuality(spinnerAirQuality.getSelectedItemPosition());
                ad.setAvailableTime(inputTime);
                ad.setNewDamageBehind(spinnerDamage.getSelectedItemPosition());
                ad.setDescription(inputDescription);
                ad.setCompleted(!inputTime.trim().equals("")
                        && !inputDescription.trim().equals("")
                        && spinnerDistance.getSelectedItemPosition() != 0
                        && spinnerResponsible.getSelectedItemPosition() != 0
                        && spinnerReason.getSelectedItemPosition() != 0
                        && spinnerLightQuality.getSelectedItemPosition() != 0
                        && spinnerAirQuality.getSelectedItemPosition() != 0
                        && spinnerDamage.getSelectedItemPosition() != 0);
                mappingInstance.setMappingAdditionalDescription(ad);
                break;
            case "SUMM":
                SummaryFragment summaryFragment = (SummaryFragment) f;
                RockboltsFragment rockboltsFragment = summaryFragment.getRockboltsFragment();

                if(rockboltsFragment != null) {
                    rockboltsFragment.saveData();
                    mappingInstance.setRockbolts(rockboltsFragment.getRockbolts());
                    mappingInstance.setDeletedRockbolts(rockboltsFragment.getRockboltsDeleted());
                }

                ShotcreteFragment shotcreteFragment = summaryFragment.getShotcreteFragment();

                if(shotcreteFragment != null) {
                    shotcreteFragment.saveData();
                    mappingInstance.setShotcretes(shotcreteFragment.getShotcreteList());
                    mappingInstance.setDeletedShotcretes(shotcreteFragment.getShotcreteListDeleted());
                }
                break;
        }

        //mMappingInstance = mappingInstance;

        return mappingInstance;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_geoclassifier_mapping_nav, menu);

//        MenuItem item = toolbar.getMenu().findItem(R.id.action_next_mapping_input);
//        if(item != null) item.setVisible(false);
//
//        MenuItem item2 = toolbar.getMenu().findItem(R.id.action_previous_mapping_input);
//        if(item2 != null) item2.setVisible(false);
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    public static String getApplicationName(Context context) {
        int stringId = context.getApplicationInfo().labelRes;
        return context.getString(stringId);
    }

    @Override
    public void displayMappingInstance(MappingInstance mappingInstance) {
        mMappingInstance = mappingInstance;
        originalMappingInstance = mappingInstance.toString();
        setTotalMappingInputs();
        MappingInputModule input = searchFinalInput();
        updateMappingInputFragment(input);
        uiProgressDialog.dismiss();
    }

    private MappingInputModule searchFinalInput() {
        int initialPos = metaPositions.get(0);
        MappingInputModule input = mMappingInputModuleDataset.get(initialPos);

        for(int i = 1; i < metaPositions.size(); i++) {
            int pos = metaPositions.get(i);

            if(input.getSubInputs() != null) {
                input = input.getSubInputs().get(pos);
            }
        }

        return input;
    }

    private void setTotalMappingInputs() {
        mappingInputsExpandedListModule = new ArrayList<>();
        mFragments = new ArrayList<>();

        List<MappingInputModule> subInputs;

        for(MappingInputModule input : mMappingInputModuleDataset) {
            subInputs = input.getSubInputs();

            if(subInputs != null) {
                for(MappingInputModule input1 : subInputs) {
                    mappingInputsExpandedListModule.add(input1);
                    mFragments.add(getMappingInputFragment(input1));
                }
            }
            else {
                mappingInputsExpandedListModule.add(input);
                mFragments.add(getMappingInputFragment(input));
            }
        }
        NUM_INPUTS = mFragments.size();
    }

    private MappingInputFragment getMappingInputFragment(MappingInputModule mappingInputModule) {
        MappingInputFragment fragment = new MappingInputFragment();

        fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, selectedMapping, mappingInputModule, mMappingInstance);
        fragment.setmMappingDataset(mMappingDataset);
        fragment.setmClient(mClient);
        fragment.setUser(user);

        return fragment;
    }

    @Override
    public void onSaveStatus(boolean success) {
        uiProgressDialog.dismiss();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", "mappingInputs");
        setResult(Activity.RESULT_OK, returnIntent);

//        String title = success ? "Data saved successfully" : "Failure on data save";
////        String msg = success ? "" : "";
//        Drawable myIcon = success ? getResources().getDrawable(android.R.drawable.ic_dialog_info ) : getResources().getDrawable(android.R.drawable.ic_dialog_alert );
//
//        new AlertDialog.Builder(this)
//                .setTitle(title)
////                .setMessage(msg)
//                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
                        finish();
//                    }
//                })
//                .setIcon(myIcon)
//                .show();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(String message) {

    }

//    @Override
//    public void onButtonNext() {
//        int prevPosition = selectedMappingInputModule.getCode();
//        if(prevPosition == mMappingInputModuleDataset.size()-1) finish(); else updateMappingInputFragment(++prevPosition);
//    }

//    @Override
//    public void onButtonPrevious() {
//        int nextPosition = selectedMappingInputModule.getCode();
//        if(nextPosition > 0) updateMappingInputFragment(--nextPosition); else finish();
//    }

    @Override
    public void onBackPressed(){
        Intent returnIntent;

        if(selectedMapping.getFinished()) {
            returnIntent = new Intent();
            returnIntent.putExtra("result", "mappingInputs");
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
        else {
            final Context c = this;

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                public void run() {
                    /*new android.support.v7.app.AlertDialog.Builder(c)
                            .setTitle(getResources().getString(R.string.characterization_mappings_label_quit_mapping_section))
                            .setMessage(getResources().getString(R.string.characterization_mappings_label_save_all_changes))
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setPositiveButton(getResources().getString(R.string.system_label_yes), new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    saveFinalData();
                                }
                            })
                            .setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Intent returnIntent = new Intent();
                                    returnIntent.putExtra("result", "mappingInputs");
                                    setResult(Activity.RESULT_OK, returnIntent);
                                    finish();
                                }
                            })
                            .show();*/
                    quitActivity();
                }
            });
        }
    }

    public Fragment getCurrentFragment() {
        MappingInputFragment mappingInputFragment = getContentFragment();
        Fragment currentFragment = getMappingInputContentFragment(mappingInputFragment);
        return currentFragment;
    }

    private MappingInputFragment getContentFragment() {
        int contentFragmentId = R.id.content_frame_gc;
        return (MappingInputFragment) getSupportFragmentManager().findFragmentById(contentFragmentId);
    }

    private Fragment getMappingInputContentFragment(MappingInputFragment containerFragment) {
        FragmentManager childFragmentManager = containerFragment.getChildFragmentManager();
        int mappingContentId = R.id.mapping_input_content;
        return childFragmentManager.findFragmentById(mappingContentId);
    }

    private class ScreenSlidePagerAdapter extends FragmentPagerAdapter {

        private List<MappingInputFragment> mInputFragments;

        public ScreenSlidePagerAdapter(FragmentManager fm, List<MappingInputFragment> inputFragments) {
            super(fm);
            mInputFragments = inputFragments;
        }

        @Override
        public Fragment getItem(int position) {
            MappingInputFragment fragment = mInputFragments.get(position);
            fragment.setmMappingInstance(mMappingInstance);
            return fragment;
        }

        @Override
        public int getCount() {
            return NUM_INPUTS;
        }

        public MappingInputFragment getInputFragments(int position) {
            return mInputFragments.get(position);
        }
    }
}
