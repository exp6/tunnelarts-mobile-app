package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.gsi;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.data.cache.FileManager;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Gsi;
import cl.skava.mobile.tunnelapp.domain.model.mapping.GsiValue;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class GsiFragment extends Fragment {

    private static final String TAG = "GsiFragment";

    private Toolbar toolbar;

    private Uri pictureURI;
    private ImageView mImg;

    private ImageView mImgFinal;

    public int EDIT_PICTURE_REQUEST_CODE = 1;

    private ImageButton deleteBtn;

    private Boolean mappingFinished;

    private File[] mFiles;

    private Gsi mappingPicture;

    private String pathAviaryDir = "/sdcard/DCIM/100AVIARY";

    private List<ImageView> sf_ss;
    private List<ImageView> sfs;

    private List<ImageView> st_ss;
    private List<ImageView> sts;

    private final String[] sf_labels = new String[5];
    private final String[] st_labels = new String[6];

    private GsiValue mappingInput;

    private GsiNumberValue[][] gsiNumberValues;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_gsi, container, false);
        rootView.setTag(TAG);

        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);

//        toolbar.setVisibility(View.GONE);

        initLabels();

        sf_ss = new ArrayList<>();
        ImageView sf1_s = (ImageView) rootView.findViewById(R.id.sf1_s);
        sf_ss.add(sf1_s);
        ImageView sf2_s = (ImageView) rootView.findViewById(R.id.sf2_s);
        sf_ss.add(sf2_s);
        ImageView sf3_s = (ImageView) rootView.findViewById(R.id.sf3_s);
        sf_ss.add(sf3_s);
        ImageView sf4_s = (ImageView) rootView.findViewById(R.id.sf4_s);
        sf_ss.add(sf4_s);
        ImageView sf5_s = (ImageView) rootView.findViewById(R.id.sf5_s);
        sf_ss.add(sf5_s);

        sfs = new ArrayList<>();
        ImageView sf1 = (ImageView) rootView.findViewById(R.id.sf1);
        sfs.add(sf1);
        ImageView sf2 = (ImageView) rootView.findViewById(R.id.sf2);
        sfs.add(sf2);
        ImageView sf3 = (ImageView) rootView.findViewById(R.id.sf3);
        sfs.add(sf3);
        ImageView sf4 = (ImageView) rootView.findViewById(R.id.sf4);
        sfs.add(sf4);
        ImageView sf5 = (ImageView) rootView.findViewById(R.id.sf5);
        sfs.add(sf5);

        for(ImageView img : sf_ss) {
            img.setVisibility(View.GONE);
        }

        for(int i = 0; i < sfs.size(); i++) {
            final int i1 = i;

            sfs.get(i).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v)
                {
                    for(ImageView img : sfs) {
                        img.setVisibility(View.VISIBLE);
                    }

                    for(ImageView img : sf_ss) {
                        img.setVisibility(View.GONE);
                    }

                    selectItem(sfs.get(i1), sf_ss.get(i1));
                    mappingInput.setSurface(i1);
                    updateTitle();
                }
            });
        }

        st_ss = new ArrayList<>();
        ImageView st1_s = (ImageView) rootView.findViewById(R.id.st1_s);
        st_ss.add(st1_s);
        ImageView st2_s = (ImageView) rootView.findViewById(R.id.st2_s);
        st_ss.add(st2_s);
        ImageView st3_s = (ImageView) rootView.findViewById(R.id.st3_s);
        st_ss.add(st3_s);
        ImageView st4_s = (ImageView) rootView.findViewById(R.id.st4_s);
        st_ss.add(st4_s);
        ImageView st5_s = (ImageView) rootView.findViewById(R.id.st5_s);
        st_ss.add(st5_s);
        ImageView st6_s = (ImageView) rootView.findViewById(R.id.st6_s);
        st_ss.add(st6_s);

        sts = new ArrayList<>();
        ImageView st1 = (ImageView) rootView.findViewById(R.id.st1);
        sts.add(st1);
        ImageView st2 = (ImageView) rootView.findViewById(R.id.st2);
        sts.add(st2);
        ImageView st3 = (ImageView) rootView.findViewById(R.id.st3);
        sts.add(st3);
        ImageView st4 = (ImageView) rootView.findViewById(R.id.st4);
        sts.add(st4);
        ImageView st5 = (ImageView) rootView.findViewById(R.id.st5);
        sts.add(st5);
        ImageView st6 = (ImageView) rootView.findViewById(R.id.st6);
        sts.add(st6);

        for(ImageView img : st_ss) {
            img.setVisibility(View.GONE);
        }

        for(int i = 0; i < sts.size(); i++) {
            final int i1 = i;

            sts.get(i).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v)
                {
                    for(ImageView img : sts) {
                        img.setVisibility(View.VISIBLE);
                    }

                    for(ImageView img : st_ss) {
                        img.setVisibility(View.GONE);
                    }

                    selectItem(sts.get(i1), st_ss.get(i1));
                    mappingInput.setStructure(i1);
                    updateTitle();
                }
            });
        }

        selectItem(sfs.get(mappingInput.getSurface()), sf_ss.get(mappingInput.getSurface()));
        selectItem(sts.get(mappingInput.getStructure()), st_ss.get(mappingInput.getStructure()));



        updateTitle();

        return rootView;
    }

    private void updateTitle() {
        setGsiValue(mappingInput.getStructure(), mappingInput.getSurface());
        toolbar.setTitle(getResources().getString(R.string.characterization_mapping_input_gsi_label_structure)+" = " + st_labels[mappingInput.getStructure()] + " - "+getResources().getString(R.string.characterization_mapping_input_gsi_label_surface_quality)+" = " + sf_labels[mappingInput.getSurface()] + " / GSI: Max = " + mappingInput.getMax() + " - Avg = " + mappingInput.getAvg() + " - Min = " + mappingInput.getMin());
    }

    private void initLabels() {
        sf_labels[0] = getResources().getString(R.string.characterization_mapping_input_gsi_label_surface_quality_1);
        sf_labels[1] = getResources().getString(R.string.characterization_mapping_input_gsi_label_surface_quality_2);
        sf_labels[2] = getResources().getString(R.string.characterization_mapping_input_gsi_label_surface_quality_3);
        sf_labels[3] = getResources().getString(R.string.characterization_mapping_input_gsi_label_surface_quality_4);
        sf_labels[4] = getResources().getString(R.string.characterization_mapping_input_gsi_label_surface_quality_5);

        st_labels[0] = getResources().getString(R.string.characterization_mapping_input_gsi_label_structure_1);
        st_labels[1] = getResources().getString(R.string.characterization_mapping_input_gsi_label_structure_2);
        st_labels[2] = getResources().getString(R.string.characterization_mapping_input_gsi_label_structure_3);
        st_labels[3] = getResources().getString(R.string.characterization_mapping_input_gsi_label_structure_4);
        st_labels[4] = getResources().getString(R.string.characterization_mapping_input_gsi_label_structure_5);
        st_labels[5] = getResources().getString(R.string.characterization_mapping_input_gsi_label_structure_6);

        gsiNumberValues = new GsiNumberValue[st_labels.length][sf_labels.length];

        GsiNumberValue gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(100);
        gsiNumberValue.setAvg(new Float(88.5));
        gsiNumberValue.setMin(77);
        gsiNumberValues[0][0] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(90);
        gsiNumberValue.setAvg(new Float(78.5));
        gsiNumberValue.setMin(67);
        gsiNumberValues[0][1] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(80);
        gsiNumberValue.setAvg(new Float(67.5));
        gsiNumberValue.setMin(55);
        gsiNumberValues[0][2] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(0);
        gsiNumberValue.setAvg(0);
        gsiNumberValue.setMin(0);
        gsiNumberValues[0][3] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(0);
        gsiNumberValue.setAvg(0);
        gsiNumberValue.setMin(0);
        gsiNumberValues[0][4] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(87);
        gsiNumberValue.setAvg(76);
        gsiNumberValue.setMin(65);
        gsiNumberValues[1][0] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(77);
        gsiNumberValue.setAvg(new Float(65.5));
        gsiNumberValue.setMin(54);
        gsiNumberValues[1][1] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(67);
        gsiNumberValue.setAvg(new Float(55.5));
        gsiNumberValue.setMin(44);
        gsiNumberValues[1][2] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(55);
        gsiNumberValue.setAvg(new Float(44.5));
        gsiNumberValue.setMin(34);
        gsiNumberValues[1][3] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(43);
        gsiNumberValue.setAvg(new Float(33.5));
        gsiNumberValue.setMin(24);
        gsiNumberValues[1][4] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(75);
        gsiNumberValue.setAvg(new Float(64.5));
        gsiNumberValue.setMin(54);
        gsiNumberValues[2][0] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(65);
        gsiNumberValue.setAvg(55);
        gsiNumberValue.setMin(45);
        gsiNumberValues[2][1] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(54);
        gsiNumberValue.setAvg(new Float(45.5));
        gsiNumberValue.setMin(37);
        gsiNumberValues[2][2] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(44);
        gsiNumberValue.setAvg(new Float(35.5));
        gsiNumberValue.setMin(27);
        gsiNumberValues[2][3] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(34);
        gsiNumberValue.setAvg(new Float(25.5));
        gsiNumberValue.setMin(17);
        gsiNumberValues[2][4] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(63);
        gsiNumberValue.setAvg(new Float(54.5));
        gsiNumberValue.setMin(46);
        gsiNumberValues[3][0] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(54);
        gsiNumberValue.setAvg(46);
        gsiNumberValue.setMin(38);
        gsiNumberValues[3][1] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(45);
        gsiNumberValue.setAvg(new Float(36.5));
        gsiNumberValue.setMin(28);
        gsiNumberValues[3][2] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(37);
        gsiNumberValue.setAvg(28);
        gsiNumberValue.setMin(19);
        gsiNumberValues[3][3] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(27);
        gsiNumberValue.setAvg(new Float(19.5));
        gsiNumberValue.setMin(12);
        gsiNumberValues[3][4] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(53);
        gsiNumberValue.setAvg(new Float(45.5));
        gsiNumberValue.setMin(46);
        gsiNumberValues[4][0] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(46);
        gsiNumberValue.setAvg(new Float(38.5));
        gsiNumberValue.setMin(31);
        gsiNumberValues[4][1] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(38);
        gsiNumberValue.setAvg(new Float(30.5));
        gsiNumberValue.setMin(23);
        gsiNumberValues[4][2] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(28);
        gsiNumberValue.setAvg(new Float(20.5));
        gsiNumberValue.setMin(13);
        gsiNumberValues[4][3] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(19);
        gsiNumberValue.setAvg(13);
        gsiNumberValue.setMin(7);
        gsiNumberValues[4][4] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(0);
        gsiNumberValue.setAvg(0);
        gsiNumberValue.setMin(0);
        gsiNumberValues[5][0] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(0);
        gsiNumberValue.setAvg(0);
        gsiNumberValue.setMin(0);
        gsiNumberValues[5][1] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(31);
        gsiNumberValue.setAvg(27);
        gsiNumberValue.setMin(17);
        gsiNumberValues[5][2] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(23);
        gsiNumberValue.setAvg(new Float(15.5));
        gsiNumberValue.setMin(8);
        gsiNumberValues[5][3] = gsiNumberValue;

        gsiNumberValue = new GsiNumberValue();
        gsiNumberValue.setMax(13);
        gsiNumberValue.setAvg(new Float(6.5));
        gsiNumberValue.setMin(0);
        gsiNumberValues[5][4] = gsiNumberValue;
    }

    private void setGsiValue(int st, int sf) {
        GsiNumberValue gsiNumberValue = gsiNumberValues[st][sf];
        mappingInput.setMax(gsiNumberValue.getMax());
        mappingInput.setAvg(gsiNumberValue.getAvg());
        mappingInput.setMin(gsiNumberValue.getMin());
    }

    private void selectItem(ImageView defaultImg, ImageView pressedImg) {
        pressedImg.setVisibility(View.VISIBLE);
        defaultImg.setVisibility(View.GONE);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void updateFragment(Fragment fragment, int container) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(container, fragment);
        transaction.commitAllowingStateLoss();
    }

    public void editPicture() throws Exception {
        dispatchEditPictureIntent();
    }

    public void deletePicture() {
        new AlertDialog.Builder(getActivity())
                .setTitle("Delete edition")
                .setMessage("Are you sure to delete this edition?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        mImg = null;
                        mImgFinal.setImageResource(R.drawable.gsi_input);
                        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.gsi_input);
                        File extendedViewFile = getOutputFile(mappingPicture);
                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(extendedViewFile);
                            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                            pictureURI = Uri.fromFile(extendedViewFile);
                            deleteBtn.setVisibility(View.GONE);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void dispatchEditPictureIntent() throws Exception {

        if (mFiles[0] == null){
            Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.gsi_input);
            File extendedViewFile = getOutputFile(mappingPicture);
//            FileOutputStream fos = null;
//            try {
//                //create a file from the bitmap
//                ByteArrayOutputStream bos = new ByteArrayOutputStream();
//                baseAsBitmap.compress(Bitmap.CompressFormat.PNG, 90 /*ignored for PNG*/, bos);
//                byte[] bitmapdata = bos.toByteArray();
//                //write the bytes in file
//                fos = new FileOutputStream(extendedViewFile);
//                fos.write(bitmapdata);
//                fos.flush();
//                fos.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//
//            }
            FileOutputStream fos = new FileOutputStream(extendedViewFile);
//            Bitmap imageBitmap = ((BitmapDrawable)mImageView.getDrawable()).getBitmap();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            pictureURI = Uri.fromFile(extendedViewFile);
        }
        else {
            pictureURI = Uri.fromFile(mFiles[0]);
        }

        Intent newIntent = new Intent("aviary.intent.action.EDIT");
        newIntent.setDataAndType(pictureURI, "image/*"); // required
        newIntent.putExtra("app-id", getActivity().getPackageName());
        String[] tools = new String[]{"BRIGHTNESS", "CROP", "DRAW", "TEXT"};
        newIntent.putExtra("tools-list", tools);
        newIntent.putExtra("extra-api-key-secret", "cd9f4966112e789c");
        newIntent.putExtra("white-label", "");

        File mediaDirectory1 = new File(pathAviaryDir);
        FileManager fm = new FileManager();
        fm.clearDirectory(mediaDirectory1);

        startActivityForResult(newIntent, EDIT_PICTURE_REQUEST_CODE);
    }

    public File getOutputFile(Gsi gsi) {
        File mediaDirectory = new File(getActivity().getExternalFilesDir(null).getPath() + File.separator + "media");
        if (!mediaDirectory.exists()) {
            try{
                mediaDirectory.mkdir();
            }
            catch(SecurityException se){
                se.printStackTrace();
            }
        }

        File mFile = new File(mediaDirectory.getPath() + File.separator + gsi.getIdMapping().toLowerCase() + File.separator + gsi.getSideName().trim().toLowerCase() + ".jpg");

        if (!mFile.getParentFile().exists()) {
            try{
                mFile.getParentFile().mkdir();
            }
            catch(SecurityException se){
                se.printStackTrace();
            }
        }
        return mFile;
    }

//    public File getOutputFile(String namePrefix, String code) throws Exception {
//        File skavaPicturesFolder = new File(getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "TunnelArts");
//
//        if (! skavaPicturesFolder.exists()){
//            if (! skavaPicturesFolder.mkdirs()){
//                throw new Exception("Failed to create directory");
//            }
//        }
//
//        File assessmentPicturesFolder = new File(skavaPicturesFolder, code);
//        if (! assessmentPicturesFolder.exists()){
//            if (! assessmentPicturesFolder.mkdirs()){
//                throw new Exception("Failed to create directory");
//            }
//        }
//
//        String newFileName = assessmentPicturesFolder.getPath() + File.separator + namePrefix + ".jpg";
//        File mediaFile = new File(newFileName);
//        return mediaFile;
//
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageView mImageView = new ImageView(getActivity());

        if (resultCode == getActivity().RESULT_OK && requestCode == EDIT_PICTURE_REQUEST_CODE) {
            Uri editedImageUri = data.getData();
            mImageView.setImageURI(editedImageUri);
            mImg = mImageView;
            mImgFinal.setImageDrawable(mImg.getDrawable());
            pictureURI = editedImageUri;
            deleteBtn.setVisibility(View.VISIBLE);

            try {
                File mFile = getOutputFile(mappingPicture);

                FileOutputStream fos = new FileOutputStream(mFile);
                Bitmap imageBitmap = ((BitmapDrawable)mImageView.getDrawable()).getBitmap();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);

                /**
                 * PATCH TEMPORAL
                 */
                File mediaDirectory = new File(pathAviaryDir);
                File[] image = mediaDirectory.listFiles();
                InputStream is = new FileInputStream(image[0]);
                OutputStream out = new FileOutputStream(mFile);

                FileManager fm = new FileManager();
                fm.copyFile(is, out);

                fm.clearDirectory(mediaDirectory);
                /**
                 * FIN PATCH TEMPORAL
                 */

                mFiles[0] = mFile;
                mappingPicture.setLocalUri(mFile.getParent());
                mImgFinal.setImageURI(Uri.fromFile(mFiles[0]));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private ImageView getImageFromFile(File imgFile, ImageView myImage) {
        if(imgFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            myImage.setImageBitmap(myBitmap);
        }

        return myImage;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public File[] getmFiles() {
        return mFiles;
    }

    public void setmFiles(File[] mFiles) {
        this.mFiles = mFiles;
    }

    public Gsi getMappingPicture() {
        return mappingPicture;
    }

    public void setMappingPicture(Gsi mappingPicture) {
        this.mappingPicture = mappingPicture;
    }

    public GsiValue getMappingInput() {
        return mappingInput;
    }

    public void setMappingInput(GsiValue mappingInput) {
        this.mappingInput = mappingInput;
    }

    class GsiNumberValue {

        private float max;
        private float min;
        private float avg;

        public float getMax() {
            return max;
        }

        public void setMax(float max) {
            this.max = max;
        }

        public float getMin() {
            return min;
        }

        public void setMin(float min) {
            this.min = min;
        }

        public float getAvg() {
            return avg;
        }

        public void setAvg(float avg) {
            this.avg = avg;
        }
    }
}
