package cl.skava.mobile.tunnelapp.presentation.ui.adapters.home;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.home.ProjectListFragment;

/**
 * Created by Jose Ignacio Vera on 03-06-2016.
 */
public class ProjectListAdapter extends RecyclerView.Adapter<ProjectListAdapter.ViewHolder> {

    private static final String TAG = "ProjectListAdapter";
    protected List<Project> mProjectDataset;
    private ProjectListFragment.OnProjectItemSelectedListener mCallback;

    private int selectedItem = 0;
    ViewHolder viewHolderPool[];
    protected RecyclerView mRecyclerView;

    private final String lang = Locale.getDefault().getLanguage();

    public ProjectListAdapter(List<Project> dataSet,
                              ProjectListFragment.OnProjectItemSelectedListener callback,
                              RecyclerView recyclerView) {
        mProjectDataset = dataSet;
        mCallback = callback;
        viewHolderPool = new ViewHolder[mProjectDataset.size()];
        mRecyclerView = recyclerView;
        setHasStableIds(true);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameTextView;
        private final TextView codeTextView;
        private final TextView dateTextView;
        private final TextView progressTextView;
        private final ProgressBar mProgress;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedItem = getLayoutPosition();
                    mCallback.onProjectSelected(selectedItem);

                    for(ViewHolder vh : viewHolderPool) {
                        vh.itemView.setSelected(false);
                        vh.itemView.setBackgroundColor(Color.TRANSPARENT);
                    }
                    notifyDataSetChanged();
                    mRecyclerView.smoothScrollToPosition(selectedItem);
                }
            });
            nameTextView = (TextView) v.findViewById(R.id.project_name);
            codeTextView = (TextView) v.findViewById(R.id.project_code);
            dateTextView = (TextView) v.findViewById(R.id.project_date);
            progressTextView = (TextView) v.findViewById(R.id.progress_percentage);
            mProgress = (ProgressBar) v.findViewById(R.id.progress_bar);
        }

        public TextView getNameTextView() {
            return nameTextView;
        }
        public TextView getCodeTextView() {
            return codeTextView;
        }
        public TextView getDateTextView() {
            return dateTextView;
        }
        public TextView getProgressTextView() {
            return progressTextView;
        }
        public ProgressBar getMProgress() {
            return mProgress;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        ViewHolder holder;
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_home_projects_row_item, viewGroup, false);
        holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        //Note: Aca se define el contenido de un item. Layout Manager lo invoca en funcion del tamaño del dataset a traves de un indice pasado como parametro (position)

        Project p = mProjectDataset.get(position);

        viewHolder.getNameTextView().setText(p.getName());
        viewHolder.getCodeTextView().setText((lang.equals("es") ? "Código " : "Code ") + p.getCode());

        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        viewHolder.getDateTextView().setText((lang.equals("es") ? "Fecha" : "Date ") + formatter.format(p.getStartDate()));

        viewHolder.getProgressTextView().setText(p.getProgress().toString() + "%");
        viewHolder.getMProgress().setProgress(p.getProgress().intValue());

        if (viewHolderPool[position] == null)
            viewHolderPool[position] = viewHolder;

        viewHolder.itemView.setSelected(selectedItem == position);
        viewHolder.itemView.setBackgroundColor(viewHolder.itemView.isSelected() ? Color.LTGRAY : Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return mProjectDataset.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        Log.d(TAG, "ATTACHED!!.");
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
