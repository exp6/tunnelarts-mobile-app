package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rmr;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.mapping.pictures.PicturesAdapter;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.mapping.rmr.ConditionDiscontinuitiesAdapter;

/**
 * Created by Jose Ignacio Vera on 08-07-2016.
 */
public class ConditionDiscontinuitiesFragment extends Fragment {

    private static final String TAG = "ConditionD";

    private List<ArrayList> mInputItems = new ArrayList<>();

    private RecyclerView recyclerView;

    private OnConditionDiscontinuitiesValueListener mCallback;

    public interface OnConditionDiscontinuitiesValueListener {
        void OnConditionDiscontinuitiesSetValue(Double value, String id);
    }

    private EvaluationMethodSelectionType selectionMap;

    private Boolean mappingFinished;

    private List<SelectionItem> rqds = new ArrayList<>();

    private final String lang = Locale.getDefault().getLanguage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initDataset();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_rmr_conditiondiscontinuities, container, false);
        rootView.setTag(TAG);

        RadioGroup group = (RadioGroup) rootView.findViewById(R.id.radio_group1);
        RadioButton button;

        int i = 0;
        for(SelectionItem rqd : rqds) {
            final SelectionItem rqd1 = rqd;

            button = new RadioButton(getActivity());
            button.setId(i);
            button.setText("(" + rqd.getStart() + (rqd.getEnd() != null ? " - " + rqd.getEnd() : "") + ")     " + rqd.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mCallback.OnConditionDiscontinuitiesSetValue(rqd1.getStart(), rqd1.getId());
                    updateMap(rqd1);
                }
            });
            button.setChecked(rqd.isChecked());

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }

        return rootView;
    }

    private void updateMap(SelectionItem rqd) {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        EvaluationMethodSelection input;
        groups.get(0).setChecked(true);
        inputs = groups.get(0).getSubSelections();

        for(int j = 0; j < inputs.size(); j++) {
            input = inputs.get(j);
            input.setChecked(false);
            if(rqd.getId().equalsIgnoreCase(input.getId())) {
                input.setChecked(true);
            }
        }

        groups.get(0).setSubSelections(inputs);
        selectionMap.setSelectionGroups(groups);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnConditionDiscontinuitiesValueListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnConditionDiscontinuitiesValueListener");
        }
    }

    public EvaluationMethodSelectionType getSelectionMap() {
        return selectionMap;
    }

    public void setSelectionMap(EvaluationMethodSelectionType selectionMap) {
        this.selectionMap = selectionMap;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    private void initDataset() {

        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        EvaluationMethodSelectionGroup e = groups.get(0);
        List<EvaluationMethodSelection> inputs = e.getSubSelections();

            SelectionItem selectionItem;

            for(EvaluationMethodSelection em : inputs) {
                selectionItem = new SelectionItem();
                selectionItem.setId(em.getId());
                selectionItem.setIndex(em.getIndex());
                selectionItem.setStart(em.getStart());
                selectionItem.setEnd(em.getEnd().doubleValue() == 0.0 ? null : em.getEnd());
                selectionItem.setDescription((lang.equals("es") ? em.getDescriptionEs() : em.getDescription()));
                selectionItem.setChecked(em.getChecked());
                rqds.add(selectionItem);
            }
            mInputItems.add((ArrayList) rqds);
    }

    public class SelectionItem {
        private String id;
        private String index;
        private Double value;
        private Double start;
        private Double end;
        private String description;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public Double getStart() {
            return start;
        }

        public void setStart(Double start) {
            this.start = start;
        }

        public Double getEnd() {
            return end;
        }

        public void setEnd(Double end) {
            this.end = end;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }
}
