package cl.skava.mobile.tunnelapp.domain.interactors.impl.account;

import java.net.MalformedURLException;

import cl.skava.mobile.tunnelapp.data.net.http.base.RestApi;
import cl.skava.mobile.tunnelapp.data.net.http.impl.RestApiImpl;
import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.base.AbstractInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.account.LoginInteractor;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.repository.UserRepository;

/**
 * Created by Jose Ignacio Vera on 28-07-2016.
 */
public class LoginInteractorImpl extends AbstractInteractor implements LoginInteractor {

    private RestApi mRestApi;
    private LoginInteractor.Callback mCallback;
    private UserRepository mUserRepository;

    public LoginInteractorImpl(Executor threadExecutor,
                               MainThread mainThread,
                               LoginInteractor.Callback callback,
                               UserRepository userRepository
                               ) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mRestApi = new RestApiImpl(this);
        mUserRepository = userRepository;
    }

    @Override
    public void run() {

    }

    private void retrieveValidation(final User user) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserValidationStatus(user != null, user);
            }
        });
    }

    @Override
    public void validateUser(User user) {
        final User validatedUser = user;

        if(!mUserRepository.isCached(user)) {
            try {
                mRestApi.validateUserFromApi(user);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        else {
            onRestValidation(mUserRepository.get(user.getId()));
        }
    }

    @Override
    public void onRestValidation(User user) {

        if(user != null) {
            mUserRepository.writeCache(user);
        }

        retrieveValidation(user);
    }
}
