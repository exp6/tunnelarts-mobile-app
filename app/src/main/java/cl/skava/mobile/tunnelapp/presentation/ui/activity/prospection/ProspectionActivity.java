package cl.skava.mobile.tunnelapp.presentation.ui.activity.prospection;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.presentation.ui.activity.prospection.calculations.ProspectionCalculationActivity;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.prospection.ProspectionFragment;

/**
 * Created by Jose Ignacio Vera on 28-06-2016.
 */
public class ProspectionActivity extends AppCompatActivity
        implements ProspectionFragment.OnProspectionItemListener {

    private static final String TAG = "ProspectionActivity";

    private CharSequence mTitle;
    private Toolbar toolbar;

    private DrawerLayout mDrawer;
    private ProgressDialog uiProgressDialog;

    //User Selection Context
    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Module selectedModule;

    private User user;

    private Client mClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            user = (User) getIntent().getSerializableExtra("user");
            mClient = (Client) getIntent().getSerializableExtra("client");
            initComponents();
            initMainFragment();
        }
    }

    private void initComponents() {
        setContentView(R.layout.activity_prospection);

        NavigationView navigationView = (NavigationView)findViewById(R.id.navigation);
        View header = navigationView.getHeaderView(0);
        TextView user_email = (TextView) header.findViewById(R.id.user_email);
        user_email.setText(user.getEmail());
        TextView user_complete_name = (TextView) header.findViewById(R.id.user_complete_name);
        user_complete_name.setText(user.getFirstName() + " " + user.getLastName());
        TextView user_role = (TextView) header.findViewById(R.id.user_role);
        user_role.setText(getResources().getString(R.string.home_label_geologist));

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        setTitle(getResources().getString(R.string.app_name) + " - " + mClient.getName().trim() + " - " + getResources().getString(R.string.forecasting_module_name));

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                mDrawer.closeDrawers();

                switch (menuItem.getItemId()) {
                    case R.id.home:
                        finish();
                        return true;
                    case R.id.logout:
                        Intent returnIntent1 = new Intent();
                        returnIntent1.putExtra("result","login");
                        setResult(Activity.RESULT_OK,returnIntent1);
                        finish();
                        return true;
                    default:
                        return true;
                }
            }
        });
    }

    private void initMainFragment() {
//        uiProgressDialog = new ProgressDialog(ProspectionActivity.this, R.style.AppTheme_Dark_Dialog);
//        uiProgressDialog.setMessage("Preparing User Interface...");
//        uiProgressDialog.show();

        selectedProject = (Project) getIntent().getSerializableExtra("project");
        selectedTunnel = (Tunnel) getIntent().getSerializableExtra("tunnel");
        selectedFace = (Face) getIntent().getSerializableExtra("face");
        selectedModule = (Module) getIntent().getSerializableExtra("module");

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        ProspectionFragment fragment = new ProspectionFragment();
        fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, user);
        transaction.replace(R.id.content_frame_gc, fragment);
        transaction.commit();
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final ProspectionFragment fragment = (ProspectionFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame_gc);

        final Context c = this;
        Handler h;

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_user:
                if (mDrawer.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawer.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawer.openDrawer(Gravity.RIGHT);
                }
                return true;
            case R.id.export_data:
                h = new Handler(Looper.getMainLooper());
                h.post(new Runnable() {
                    public void run() {
                        new android.support.v7.app.AlertDialog.Builder(c)
                                .setTitle(getResources().getString(R.string.system_label_archive_data1) + " " + getResources().getString(R.string.forecasting_module_name) + " " + getResources().getString(R.string.system_label_archive_data2))
                                .setMessage(getResources().getString(R.string.system_label_archive_data_msg1) + " " + getResources().getString(R.string.forecasting_module_name) + getResources().getString(R.string.system_label_proceed))
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setPositiveButton(getResources().getString(R.string.system_label_yes), new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        fragment.exportData();
                                    }
                                })
                                .setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                })
                                .show();
                    }
                });
                return true;
            case R.id.sync_data:
                h = new Handler(Looper.getMainLooper());
                h.post(new Runnable() {
                    public void run() {
                        new android.support.v7.app.AlertDialog.Builder(c)
                                .setTitle(getResources().getString(R.string.system_label_sync_data1) + " " + getResources().getString(R.string.forecasting_module_name) + " " + getResources().getString(R.string.system_label_sync_data2))
                                .setMessage(getResources().getString(R.string.system_label_sync_data_msg1) + " " + getResources().getString(R.string.forecasting_module_name) + getResources().getString(R.string.system_label_proceed))
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setPositiveButton(getResources().getString(R.string.system_label_yes), new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        fragment.syncData();
                                    }
                                })
                                .setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                })
                                .show();
                    }
                });
                return true;
            case R.id.edit_item:
                fragment.editSelectedItem();
                return true;
            case R.id.delete_item:
                h = new Handler(Looper.getMainLooper());
                h.post(new Runnable() {
                    public void run() {
                        new android.support.v7.app.AlertDialog.Builder(c)
                                .setTitle(getResources().getString(R.string.system_label_delete_item) + " " + getResources().getString(R.string.forecasting_label_prospection_hole_group_name))
                                .setMessage(getResources().getString(R.string.system_label_delete_item_msg) + getResources().getString(R.string.system_label_proceed))
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(getResources().getString(R.string.system_label_yes), new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        fragment.deleteSelectedProspectionHoleGroup();
                                    }
                                })
                                .setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                })
                                .show();
                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_prospection_right, menu);
        MenuItem item = toolbar.getMenu().findItem(R.id.action_processing);
        item.setVisible(false);
        MenuItem itemExec = toolbar.getMenu().findItem(R.id.action_exec_model);
        itemExec.setVisible(false);
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onProspectionHoleSelected(ProspectionHoleGroup prospectionHoleGroup) {
        initCalculationActivity(prospectionHoleGroup);
    }

    @Override
    public void onSyncFinish() {
        reloadMainFragment();
    }

    @Override
    public void hideDelete() {
        MenuItem item = toolbar.getMenu().findItem(R.id.delete_item);
        if(item != null) item.setVisible(false);

        MenuItem item2 = toolbar.getMenu().findItem(R.id.export_data);
        if(item2 != null) item2.setVisible(false);
    }

    @Override
    public void hideEdit() {
        MenuItem item = toolbar.getMenu().findItem(R.id.edit_item);
        if(item != null)
            item.setVisible(false);
    }

    @Override
    public void showDelete() {
        MenuItem item = toolbar.getMenu().findItem(R.id.delete_item);
        if(item != null) item.setVisible(true);

        MenuItem item2 = toolbar.getMenu().findItem(R.id.export_data);
        if(item2 != null) item2.setVisible(true);
    }

    @Override
    public void showEdit() {
        MenuItem item = toolbar.getMenu().findItem(R.id.edit_item);
        if(item != null)
            item.setVisible(true);
    }

    private void reloadMainFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        ProspectionFragment fragment = new ProspectionFragment();
        fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, user);
        transaction.replace(R.id.content_frame_gc, fragment);
        transaction.commitAllowingStateLoss();
    }

    private void initCalculationActivity(ProspectionHoleGroup prospectionHoleGroup) {
        Intent intent;
        intent = new Intent(this, ProspectionCalculationActivity.class);
        intent.putExtra("project", selectedProject);
        intent.putExtra("tunnel", selectedTunnel);
        intent.putExtra("face", selectedFace);
        intent.putExtra("module", selectedModule);
        intent.putExtra("user", user);
        intent.putExtra("prospectionHoleGroup", prospectionHoleGroup);
        intent.putExtra("client", mClient);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {
            String result = data.getStringExtra("result");

            if(result.equals("login")) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result","login");
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }

            if(result.equals("home")) {
                finish();
            }
        }
    }
}
