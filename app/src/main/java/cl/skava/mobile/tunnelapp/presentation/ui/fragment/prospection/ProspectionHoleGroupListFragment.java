package cl.skava.mobile.tunnelapp.presentation.ui.fragment.prospection;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.prospection.ProspectionHoleGroupListAdapter;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class ProspectionHoleGroupListFragment extends Fragment {

    private static final String TAG = "ProspectionHoleGroupListFragment";

    protected RecyclerView mRecyclerView;
    protected ProspectionHoleGroupListAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;

    protected List<ProspectionHoleGroup> mProspectionHoleGroupDataset;

    //User Selection Context
    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Module selectedModule;

    private OnProspectionHoleGroupListlListener mCallback;

    public interface OnProspectionHoleGroupListlListener {
        void onProspectionHoleGroupSelected(int position, boolean forImg);
    }

    private Integer prospectionHoleGroupPosition;

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnProspectionHoleGroupListlListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnProspectionHoleGroupListlListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_prospection_hole_group_list, container, false);
        rootView.setTag(TAG);

        ((TextView) rootView.findViewById(R.id.project_label)).setText(selectedProject.getName());
        ((TextView) rootView.findViewById(R.id.tunnel_label)).setText(selectedTunnel.getName());
        ((TextView) rootView.findViewById(R.id.face_label)).setText(selectedFace.getName());

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        setRecyclerViewLayoutManager();
        mAdapter = new ProspectionHoleGroupListAdapter(mProspectionHoleGroupDataset, mRecyclerView, mCallback, prospectionHoleGroupPosition);
        mRecyclerView.setAdapter(mAdapter);

        return rootView;
    }

    public void setRecyclerViewLayoutManager() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setSelectionContext(Project project,
                                    Tunnel tunnel,
                                    Face face,
                                    Module module) {
        selectedProject = project;
        selectedTunnel = tunnel;
        selectedFace = face;
        selectedModule = module;
    }

    public void updateItemList() {
        mAdapter = new ProspectionHoleGroupListAdapter(mProspectionHoleGroupDataset, mRecyclerView, mCallback, prospectionHoleGroupPosition);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void setmProspectionHoleGroupDataset(List<ProspectionHoleGroup> mProspectionHoleGroupDataset) {
        this.mProspectionHoleGroupDataset = mProspectionHoleGroupDataset;
    }

    public void setProspectionHoleGroupPosition(Integer prospectionHoleGroupPosition) {
        this.prospectionHoleGroupPosition = prospectionHoleGroupPosition;
    }
}
