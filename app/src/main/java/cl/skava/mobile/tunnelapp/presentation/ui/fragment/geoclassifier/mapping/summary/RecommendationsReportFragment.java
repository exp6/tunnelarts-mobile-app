package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.recommendations.RockboltsFragment;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.recommendations.ShotcreteFragment;

public class RecommendationsReportFragment extends Fragment {

//    private TabLayout tabLayout;

    public RecommendationsReportFragment() {}

    public static RecommendationsReportFragment newInstance() {
        RecommendationsReportFragment fragment = new RecommendationsReportFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_summary_recommendations, container, false);

//        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
//        tabLayout.addTab(tabLayout.newTab().setText("ROCKBOLTS"));
//        tabLayout.addTab(tabLayout.newTab().setText("SHOTCRETE"));

        RockboltsFragment rf = new RockboltsFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.recc_input_content, rf);
        transaction.commitAllowingStateLoss();

//        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
//                switch (tab.getPosition()) {
//                    case 1:
//                        ShotcreteFragment sf = new ShotcreteFragment();
//                        transaction.replace(R.id.recc_input_content, sf);
//                        break;
//                    default:
//                        RockboltsFragment rf = new RockboltsFragment();
//                        transaction.replace(R.id.recc_input_content, rf);
//                        break;
//                }
//                transaction.commitAllowingStateLoss();
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//            }
//        });

        return rootView;
    }
}
