package cl.skava.mobile.tunnelapp.data.datasource;

/**
 * Created by Jose Ignacio Vera on 15-07-2016.
 */
public class DbConstants {
    public static final String DATABASE_NAME = "TunnelArts.db";
    public static final int DATABASE_VERSION = 1;


}
