package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.recommendations;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RecommendationRockbolt;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.mapping.summary.RockboltListAdapter;

/**
 * Created by Jose Ignacio Vera on 11-10-2016.
 */
public class RockboltsFragment extends Fragment {

    private RecyclerView recyclerView;

    private List<RecommendationRockbolt> rockbolts;
    private RockboltListAdapter mAdapter;

    private int index;

    private FloatingActionButton myFab;

    private List<RecommendationRockbolt> rockboltsDeleted;

    private Mapping selectedMapping;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_summary_recommendations_rockbolts, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setReverseLayout(true);
        initDataset();

        mAdapter = new RockboltListAdapter(rockbolts, recyclerView, this);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);

        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addItem();
            }
        });

        return rootView;
    }

    private void initDataset() {
        index = rockbolts.size();
    }

    public void updateItemList() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        layoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new RockboltListAdapter(rockbolts, recyclerView, this);
        recyclerView.setAdapter(mAdapter);
    }

    public void deleteItem(int position) {
        saveData();
        rockbolts = mAdapter.getmRockbolts();

        RecommendationRockbolt deleted = rockbolts.get(position);
        if(!deleted.getId().trim().isEmpty()) rockboltsDeleted.add(deleted);

        rockbolts.remove(position);
        index = rockbolts.size();
        if(index < 6) {
            myFab.setVisibility(View.VISIBLE);
        }
        updateItemList();
    }

    public void addItem() {
        saveData();
        rockbolts = mAdapter.getmRockbolts();
        RecommendationRockbolt rockbolt = new RecommendationRockbolt("", index, 0, 0, new Float(0), new Float(0), 0, new Float(0), new Float(0), "", "", new Float(0), new Float(0), selectedMapping.getId(), false);
        rockbolts.add(rockbolt);
        index = rockbolts.size();
        if(index == 6) {
            myFab.setVisibility(View.GONE);
        }
        updateItemList();
    }

    public void setMyFab(FloatingActionButton myFab) {
        this.myFab = myFab;
    }

    public void saveData() {
        RockboltListAdapter.ViewHolder[] viewHolders = mAdapter.getViewHolderPool();
        for(int i = 0; i < viewHolders.length; i++) {
            RockboltListAdapter.ViewHolder viewHolder = viewHolders[i];
            RecommendationRockbolt rockbolt = rockbolts.get(i);

            int input1 = viewHolder.getType().getSelectedItemPosition();
            String input2 = viewHolder.getQuantity().getText().toString().trim();
            String input3 = viewHolder.getPk().getText().toString().trim();
            String input4 = viewHolder.getLength().getText().toString().trim();
            int input5 = viewHolder.getDiameter().getSelectedItemPosition();
            String input6 = viewHolder.getSpacing().getText().toString().trim();
            String input7 = viewHolder.getAngle().getText().toString().trim();
            String input8 = viewHolder.getTimeFrom().getText().toString().trim();
            String input9 = viewHolder.getTimeTo().getText().toString().trim();
            String input10 = viewHolder.getPositionHorizontal().getText().toString().trim();
            String input11 = viewHolder.getPositionVertical().getText().toString().trim();

            rockbolt.setType(input1);
            rockbolt.setQuantity(input2.isEmpty() ? new Integer(0) : new Integer(input2));
            rockbolt.setPk(input3.isEmpty() ? new Float(0) : new Float(input3));
            rockbolt.setLength(input4.isEmpty() ? new Float(0) : new Float(input4));
            rockbolt.setDiameter(input5);
            rockbolt.setSpacing(input6.isEmpty() ? new Float(0) : new Float(input6));
            rockbolt.setAngle(input7.isEmpty() ? new Float(0) : new Float(input7));
            rockbolt.setTimeFrom(input8);
            rockbolt.setTimeTo(input9);
            rockbolt.setHorizontal(input10.isEmpty() ? new Float(0) : new Float(input10));
            rockbolt.setVertical(input11.isEmpty() ? new Float(0) : new Float(input11));

            rockbolts.set(i, rockbolt);
        }
    }

    public List<RecommendationRockbolt> getRockbolts() {
        return rockbolts;
    }

    public void setRockbolts(List<RecommendationRockbolt> rockbolts) {
        this.rockbolts = rockbolts;
    }

    public List<RecommendationRockbolt> getRockboltsDeleted() {
        return rockboltsDeleted;
    }

    public void setRockboltsDeleted(List<RecommendationRockbolt> rockboltsDeleted) {
        this.rockboltsDeleted = rockboltsDeleted;
    }

    public void setSelectedMapping(Mapping selectedMapping) {
        this.selectedMapping = selectedMapping;
    }
}
