package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.repository.ChangeLog;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.MappingInput;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.ProjectMappingInput;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.UserProject;
import cl.skava.mobile.tunnelapp.domain.model.mapping.AdditionalDescription;
import cl.skava.mobile.tunnelapp.domain.model.mapping.AdditionalInformation;
import cl.skava.mobile.tunnelapp.domain.model.mapping.BaseData;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Discontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.ExpandedTunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.FailureZone;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Gsi;
import cl.skava.mobile.tunnelapp.domain.model.mapping.GsiValue;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Lithology;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Particularities;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Picture;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RecommendationRockbolt;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RecommendationShotcrete;
import cl.skava.mobile.tunnelapp.domain.model.mapping.StereonetPicture;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueDiscontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInput;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputGroupType;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputSelectionDiscontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.Rmr;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMass;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMassHazard;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SpecialFeatures;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInput;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputGroupType;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputSelection;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroupCalculation;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;

/**
 * Created by Jose Ignacio Vera on 25-07-2016.
 */
public class CloudStoreServiceHelper {

    private static final Double TOTAL_PERCENTAGE = 100.0;

    @NonNull
    protected static List<String> getTableList() {
        List<String> list = new ArrayList<String>();
        List<Class> entities = new ArrayList<Class>();

        //Add the model entity class for table sync. Class name must be the same as the cloud SQL table.

        //Base Data Entities
        entities.addAll(getBaseDataEntities());

        //Geomechanical Characterizer Entities
        entities.addAll(getGeoCharacterizerEntities());

        //Forecast Module Entities
        entities.addAll(getProspectionEntities());

        list = setupSyncList(entities);
        return list;
    }

    @NonNull
    protected static List<String> getChangeLogReference() {
        List<String> list = new ArrayList<String>();

        //ChangeLog
        list.add(formatClassName(ChangeLog.class));
        return list;
    }

    private static String formatClassName(Class c) {
        return c.getName();
    }

    private static List<String> setupSyncList(List<Class> classes) {
        List<String> list = new ArrayList<String>();
        for(Class cl : classes) {
            list.add(formatClassName(cl));
        }
        return list;
    }

    protected static Integer getSyncPercentage(Integer total, Integer progress) {
        Long result = Math.round(((double)progress * TOTAL_PERCENTAGE) / (double)total);
        return result.intValue();
    }

    public static List<Class> getBaseDataEntities() {
        List<Class> entities = new ArrayList<>();

        //Here you add Base Data entities (System Main Module)
        entities.add(Client.class);
        entities.add(UserProject.class);
        entities.add(Project.class);
        entities.add(Tunnel.class);
        entities.add(Face.class);
        entities.add(FormationUnit.class);
        entities.add(RockQuality.class);
        entities.add(ProjectMappingInput.class);
        entities.add(MappingInput.class);

        return entities;
    }

    public static List<Class> getGeoCharacterizerEntities() {
        List<Class> entities = new ArrayList<>();

        //Here you add Mapping entities (Geomechanical Characterizer Module)
        entities.add(Mapping.class);
        entities.addAll(getMappingInputEntities());
        entities.addAll(getQValueSelectionEntities());
        entities.addAll(getRmrSelectionEntities());

        return entities;
    }

    public static List<Class> getMappingInputEntities() {
        List<Class> mappingInputs = new ArrayList<>();

        //Here you add mapping input entities (Geomechanical Characterizer Module)
        mappingInputs.add(BaseData.class);
        mappingInputs.add(RockMass.class);
        mappingInputs.add(Lithology.class);
        mappingInputs.add(Discontinuity.class);
        mappingInputs.add(AdditionalInformation.class);
        mappingInputs.add(FailureZone.class);
        mappingInputs.add(Particularities.class);
        mappingInputs.add(SpecialFeatures.class);
        mappingInputs.add(RockMassHazard.class);
        mappingInputs.add(AdditionalDescription.class);
        mappingInputs.add(QValue.class);
        mappingInputs.add(Rmr.class);
        mappingInputs.add(QValueInputSelection.class);
        mappingInputs.add(RmrInputSelection.class);
        mappingInputs.add(Picture.class);
        mappingInputs.add(ExpandedTunnel.class);
        mappingInputs.add(Gsi.class);
        mappingInputs.add(RecommendationRockbolt.class);
        mappingInputs.add(RecommendationShotcrete.class);
        mappingInputs.add(GsiValue.class);
        mappingInputs.add(StereonetPicture.class);

        return mappingInputs;
    }

    public static List<Class> getQValueSelectionEntities() {
        List<Class> mappingInputs = new ArrayList<>();

        //Here you add QValue Selection entities (Geomechanical Characterizer Module)
        mappingInputs.add(QValueInputGroupType.class);
        mappingInputs.add(QValueInputGroup.class);
        mappingInputs.add(QValueInput.class);
        mappingInputs.add(QValueDiscontinuity.class);

        return mappingInputs;
    }

    public static List<Class> getRmrSelectionEntities() {
        List<Class> mappingInputs = new ArrayList<>();

        //Here you add Rmr Selection entities (Geomechanical Characterizer Module)
        mappingInputs.add(RmrInputGroupType.class);
        mappingInputs.add(RmrInputGroup.class);
        mappingInputs.add(RmrInput.class);

        return mappingInputs;
    }

    public static List<Class> getProspectionEntities() {
        List<Class> prospectionEntities = new ArrayList<>();

        //Here you add Forecasting entities (Forecast Module)
        prospectionEntities.add(ProspectionHoleGroup.class);
        prospectionEntities.add(ProspectionHole.class);
        prospectionEntities.add(Rod.class);
        prospectionEntities.add(ProspectionHoleGroupCalculation.class);
        prospectionEntities.add(ProspectionRefPicture.class);

        return prospectionEntities;
    }
}
