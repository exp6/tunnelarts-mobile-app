package cl.skava.mobile.tunnelapp.domain.interactors.interfaces.prospection;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.interactors.base.Interactor;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;

/**
 * Created by Jose Ignacio Vera on 02-08-2016.
 */
public interface ExecuteModelInteractor extends Interactor {

    interface Callback {
        void onExecResult(List<Rod> finalRods);
    }
}
