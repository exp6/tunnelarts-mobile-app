package cl.skava.mobile.tunnelapp.presentation.presenters.impl;

import android.content.Context;

import java.util.List;

import cl.skava.mobile.tunnelapp.data.repository.BaseRepositoryImpl;
import cl.skava.mobile.tunnelapp.data.repository.ModuleRepositoryImpl;
import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.impl.MainInteractorImpl;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.MainInteractor;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.repository.BaseRepository;
import cl.skava.mobile.tunnelapp.domain.repository.ModuleRepository;
import cl.skava.mobile.tunnelapp.presentation.presenters.base.AbstractPresenter;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.MainPresenter;

/**
 * Created by Jose Ignacio Vera on 16-06-2016.
 */
public class MainPresenterImpl extends AbstractPresenter
        implements MainPresenter,
        MainInteractor.Callback {

    private MainPresenter.View mView;
    private ModuleRepository mModuleRepository;
    private BaseRepository mBaseRepository;

    public MainPresenterImpl(Executor executor,
                             MainThread mainThread,
                             View view) {
        super(executor, mainThread);
        mView = view;
    }


    @Override
    public void resume() {
        mView.showProgress();
        mModuleRepository = new ModuleRepositoryImpl();

        MainInteractor interactor = new MainInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mModuleRepository
        );
        interactor.execute();
    }

    @Override
    public void pause() {}

    @Override
    public void stop() {}

    @Override
    public void destroy() {}

    @Override
    public void onError(String message) {}

    @Override
    public void onModuleListRetrieved(List<Module> modules) {
        mView.hideProgress();
        mView.setupModuleDataset(modules);
    }

    @Override
    public void onRetrievalFailed(String error) {
        mView.hideProgress();
        onError(error);
    }

    @Override
    public void syncUserData(Context activity, boolean isNetworkAvailable, User user) {
        int code = 0;
        mBaseRepository = new BaseRepositoryImpl();

        if (isNetworkAvailable) {
            mBaseRepository.initRepository(activity, this, user);
        } else {
            onSyncResult(code);
        }
    }

    @Override
    public void onSyncResult(int code) {
        mView.hideProgress();
        mView.syncUserDataResult(code);
    }

    @Override
    public void setSyncProgressPercentaje(Integer percentaje) {
        mView.hideProgress();
        mView.onSyncProgressPercentaje(percentaje);
    }

    @Override
    public void onNetworkError(String msg) {
        mView.hideProgress();
        mView.onNetworkErrorUI(msg);
    }

    @Override
    public void notifySyncSegment(String msg) {
        mView.hideProgress();
        mView.notifySyncSegment(msg);
    }

    @Override
    public void syncBasicData(Context activity, boolean isNetworkAvailable, User user) {
        int code = 0;
        mBaseRepository = new BaseRepositoryImpl();

        if (isNetworkAvailable) {
            mBaseRepository.syncBasicData(activity, this, user);
        } else {
            onSyncResult(code);
        }
    }
}
