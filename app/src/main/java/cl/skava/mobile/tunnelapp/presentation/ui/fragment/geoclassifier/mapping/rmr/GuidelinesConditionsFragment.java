package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rmr;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.mapping.rmr.ConditionDiscontinuitiesAdapter;

/**
 * Created by Jose Ignacio Vera on 20-10-2016.
 */
public class GuidelinesConditionsFragment extends Fragment {

    private static final String TAG = "GuidelinesConditionsFragment";

    private List<SelectionItem> persistence = new ArrayList<>();
    private List<SelectionItem> opening = new ArrayList<>();
    private List<SelectionItem> roughness = new ArrayList<>();
    private List<SelectionItem> infilling = new ArrayList<>();
    private List<SelectionItem> weathering = new ArrayList<>();

    private List<ArrayList> mInputItems = new ArrayList<>();

    private RecyclerView recyclerView;

    private OnConditionDiscontinuitiesValueListener mCallback;

    public interface OnConditionDiscontinuitiesValueListener {
        void OnConditionDiscontinuitiesSetValue(int position, Double value, String id);
    }

    private List<EvaluationMethodSelectionType> rmrSelectionMaps;

    private Boolean mappingFinished;

    private final String lang = Locale.getDefault().getLanguage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initDataset();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_rmr_guidelinesconditions, container, false);
        rootView.setTag(TAG);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new ConditionDiscontinuitiesAdapter(mInputItems, this, rmrSelectionMaps, mappingFinished));

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnConditionDiscontinuitiesValueListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnConditionDiscontinuitiesValueListener");
        }
    }

    public void onItemClick(int position, Double value, String id, List<EvaluationMethodSelectionType> rmrSelectionMaps) {
        setRmrSelectionMaps(rmrSelectionMaps);
        mCallback.OnConditionDiscontinuitiesSetValue(position, value, id);
    }

    public List<EvaluationMethodSelectionType> getRmrSelectionMaps() {
        return rmrSelectionMaps;
    }

    public void setRmrSelectionMaps(List<EvaluationMethodSelectionType> rmrSelectionMaps) {
        this.rmrSelectionMaps = rmrSelectionMaps;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    private void initDataset() {
        List<SelectionItem> selectionItems;
        for(EvaluationMethodSelectionType map : rmrSelectionMaps) {
            selectionItems = new ArrayList<>();
            List<EvaluationMethodSelectionGroup> groups = map.getSelectionGroups();
            EvaluationMethodSelectionGroup e = groups.get(0);
            List<EvaluationMethodSelection> inputs = e.getSubSelections();
            SelectionItem selectionItem;

            for(EvaluationMethodSelection em : inputs) {
                selectionItem = new SelectionItem();
                selectionItem.setId(em.getId());
                selectionItem.setIndex(em.getIndex());
                selectionItem.setStart(em.getStart());
                selectionItem.setEnd(em.getEnd().doubleValue() == 0.0 ? null : em.getEnd());
                selectionItem.setDescription((lang.equals("es") ? em.getDescriptionEs() : em.getDescription()));
                selectionItem.setChecked(em.getChecked());
                selectionItems.add(selectionItem);
            }
            mInputItems.add((ArrayList) selectionItems);
        }
    }

    public class SelectionItem {
        private String id;
        private String index;
        private Double value;
        private Double start;
        private Double end;
        private String description;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public Double getStart() {
            return start;
        }

        public void setStart(Double start) {
            this.start = start;
        }

        public Double getEnd() {
            return end;
        }

        public void setEnd(Double end) {
            this.end = end;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }
}
