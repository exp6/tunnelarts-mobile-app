package cl.skava.mobile.tunnelapp.domain.interactors.impl.account;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.base.AbstractInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.account.UserCacheInteractor;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.repository.UserRepository;

/**
 * Created by Jose Ignacio Vera on 25-08-2016.
 */
public class UserCacheInteractorImpl extends AbstractInteractor implements UserCacheInteractor {

    UserCacheInteractor.Callback mCallback;
    private UserRepository mUserRepository;

    public UserCacheInteractorImpl(Executor threadExecutor,
                                   MainThread mainThread,
                                   UserCacheInteractor.Callback callback,
                                   UserRepository userRepository) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mUserRepository = userRepository;
    }

    private void postUsers(final List<User> users) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCachedUsers(users);
            }
        });
    }

    @Override
    public void run() {
        final List<User> users = getList();
        postUsers(users);
    }

    private List<User> getList() {
        final List<User> users = mUserRepository.getList();

        if (users == null) {
//            notifyError();
        }

        return users;
    }
}
