package cl.skava.mobile.tunnelapp.domain.model.mapping.rmr;

import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInput;

/**
 * Created by Jose Ignacio Vera on 24-08-2016.
 */
public class RmrInputSelection extends DataInput {

    private String id;
    private String idMapping;
    private String rqd;
    private String spacing;
    private String persistence;
    private String opening;
    private String roughness;
    private String infilling;
    private String weathering;
    private String groundwater;
    private String orientationDD;
    private String strength;
    private String conditionDD;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public String getRqd() {
        return rqd;
    }

    public void setRqd(String rqd) {
        this.rqd = rqd;
    }

    public String getSpacing() {
        return spacing;
    }

    public void setSpacing(String spacing) {
        this.spacing = spacing;
    }

    public String getPersistence() {
        return persistence;
    }

    public void setPersistence(String persistence) {
        this.persistence = persistence;
    }

    public String getOpening() {
        return opening;
    }

    public void setOpening(String opening) {
        this.opening = opening;
    }

    public String getRoughness() {
        return roughness;
    }

    public void setRoughness(String roughness) {
        this.roughness = roughness;
    }

    public String getInfilling() {
        return infilling;
    }

    public void setInfilling(String infilling) {
        this.infilling = infilling;
    }

    public String getWeathering() {
        return weathering;
    }

    public void setWeathering(String weathering) {
        this.weathering = weathering;
    }

    public String getGroundwater() {
        return groundwater;
    }

    public void setGroundwater(String groundwater) {
        this.groundwater = groundwater;
    }

    public String getOrientationDD() {
        return orientationDD;
    }

    public void setOrientationDD(String orientationDD) {
        this.orientationDD = orientationDD;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getConditionDD() {
        return conditionDD;
    }

    public void setConditionDD(String conditionDD) {
        this.conditionDD = conditionDD;
    }

    @Override
    public String toString() {
        return "RmrInputSelection{" +
                "id='" + id + '\'' +
                ", idMapping='" + idMapping + '\'' +
                ", rqd='" + rqd + '\'' +
                ", spacing='" + spacing + '\'' +
                ", persistence='" + persistence + '\'' +
                ", opening='" + opening + '\'' +
                ", roughness='" + roughness + '\'' +
                ", infilling='" + infilling + '\'' +
                ", weathering='" + weathering + '\'' +
                ", groundwater='" + groundwater + '\'' +
                ", orientationDD='" + orientationDD + '\'' +
                ", strength='" + strength + '\'' +
                ", conditionDD='" + conditionDD + '\'' +
                '}';
    }
}
