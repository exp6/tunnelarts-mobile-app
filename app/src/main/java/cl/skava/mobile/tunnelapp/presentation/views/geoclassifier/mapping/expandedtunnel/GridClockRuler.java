package cl.skava.mobile.tunnelapp.presentation.views.geoclassifier.mapping.expandedtunnel;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Carlos Vergara on 06-03-2017.
 */

public class GridClockRuler extends RelativeLayout {

    private ArrayList<TextView> textViews;

    public GridClockRuler(Context context) {
        super(context);
        initViews();
    }

    public GridClockRuler(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public GridClockRuler(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    @TargetApi(21)
    public GridClockRuler(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initViews();
    }

    public void initViews() {
        setBackgroundColor(Color.WHITE);
        textViews = new ArrayList<>();
        for (int i = 1, cnt = 6; i < 14; i++, cnt++) {
            TextView tx = new TextView(this.getContext());
            tx.setText(Integer.valueOf(cnt).toString());
            RelativeLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            tx.setLayoutParams(params);
            textViews.add(tx);
            this.addView(tx);
            if (cnt == 12) {
                cnt = 0;
            }
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int divisions = this.getWidth()/12;
        int span = l;
        for (int i = 0; i < this.getChildCount(); i++) {
            TextView tx = (TextView) this.getChildAt(i);
            tx.layout(span, t, r, b);
            span += divisions - 1;
        }
    }
}
