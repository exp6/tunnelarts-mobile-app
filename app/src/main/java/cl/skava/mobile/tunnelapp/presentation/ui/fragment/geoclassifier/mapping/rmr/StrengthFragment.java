package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.rmr;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;

/**
 * Created by Jose Ignacio Vera on 05-07-2016.
 */
public class StrengthFragment extends Fragment {
    private static final String TAG = "StrengthFragment";

    private List<StrengthGroup> strengthGroups = new ArrayList<>();

    private OnStrengthValueListener mCallback;

    public interface OnStrengthValueListener {
        void OnStrengthSetValue(Double value, String id);
    }

    private EvaluationMethodSelectionType selectionMap;

    private Boolean mappingFinished;

    private final String lang = Locale.getDefault().getLanguage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initDataset();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_rmr_strength, container, false);
        rootView.setTag(TAG);

        RadioGroup group = (RadioGroup) rootView.findViewById(R.id.radio_group1);
        RadioButton button;

        final RadioGroup group2 = (RadioGroup) rootView.findViewById(R.id.radio_group2);

        int i = 0;
        for(StrengthGroup strengthGroup :  strengthGroups) {
            final StrengthGroup strengthGroup1 = strengthGroup;
            button = new RadioButton(getActivity());
            button.setId(i);
//            button.setText(strengthGroup.getIndex().trim() + ". " + strengthGroup.getDescription());
            button.setText(strengthGroup.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    displayStrengths(group2, strengthGroup1);
                }
            });
            button.setChecked(strengthGroup.isChecked());
            if(strengthGroup.isChecked()) {
                displayStrengths(group2, strengthGroup1);
            }

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }

        return rootView;
    }

    private void displayStrengths(RadioGroup group, StrengthGroup strengthGroup) {
        List<Strength> strengths = strengthGroup.getStrengths();
        final StrengthGroup strengthGroup1 = strengthGroup;
        group.removeAllViews();
        RadioButton button;

        int i = 0;
        for(Strength strength :  strengths) {
            final Strength strength1 = strength;

            button = new RadioButton(getActivity());
            button.setId(i);
//            button.setText(strength.getIndex().trim() + " (" + strength.getStart() + (strength.getEnd() != null ? " - " + strength.getEnd() : "") + ")     " + strength.getDescription());
            button.setText("(" + strength.getStart() + (strength.getEnd() != null ? " - " + strength.getEnd() : "") + ")     " + strength.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mCallback.OnStrengthSetValue(strength1.getStart(), strength1.getId());
                    updateMap(strengthGroup1, strength1);
                }
            });
            button.setChecked(strength1.isChecked());

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }
    }

    private void updateMap(StrengthGroup strengthGroup, Strength strength) {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        EvaluationMethodSelection input;
        for(int i = 0; i < groups.size(); i++) {
            groups.get(i).setChecked(false);
            if(strengthGroup.getId().equalsIgnoreCase(groups.get(i).getId())) {
                groups.get(i).setChecked(true);
            }

            inputs = groups.get(i).getSubSelections();
            for(int j = 0; j < inputs.size(); j++) {
                input = inputs.get(j);
                input.setChecked(false);
                if(strength.getId().equalsIgnoreCase(input.getId())) {
                    input.setChecked(true);
                }
            }

            groups.get(i).setSubSelections(inputs);
        }
        selectionMap.setSelectionGroups(groups);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnStrengthValueListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnStrengthValueListener");
        }
    }

    public void setSelectionMap(EvaluationMethodSelectionType selectionMap) {
        this.selectionMap = selectionMap;
    }

    public EvaluationMethodSelectionType getSelectionMap() {
        return selectionMap;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    private void initDataset() {

        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        StrengthGroup strengthGroup;
        List<Strength> strengths;
        Strength strength;

        for(EvaluationMethodSelectionGroup e : groups) {
            strengthGroup = new StrengthGroup();
            strengthGroup.setId(e.getId());
            strengthGroup.setIndex(e.getIndex());
            strengthGroup.setDescription((lang.equals("es") ? e.getDescriptionEs() : e.getDescription()));
            strengthGroup.setChecked(e.getChecked());
            inputs = e.getSubSelections();

            strengths = new ArrayList<>();

            for (EvaluationMethodSelection em : inputs) {
                strength = new Strength();
                strength.setId(em.getId());
                strength.setIndex(em.getIndex());
                strength.setStart(em.getStart());
                strength.setEnd(em.getEnd().doubleValue() == 0.0 ? null : em.getEnd());
                strength.setDescription((lang.equals("es") ? em.getDescriptionEs() : em.getDescription()));
                strength.setChecked(em.getChecked());
                strengths.add(strength);
            }

            strengthGroup.setStrengths(strengths);
            strengthGroups.add(strengthGroup);
        }
    }

    class Strength {
        private String id;
        private String index;
        private Double value;
        private Double start;
        private Double end;
        private String description;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public Double getStart() {
            return start;
        }

        public void setStart(Double start) {
            this.start = start;
        }

        public Double getEnd() {
            return end;
        }

        public void setEnd(Double end) {
            this.end = end;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }

    class StrengthGroup {
        private String id;
        private String index;
        private String description;
        private List<Strength> strengths;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<Strength> getStrengths() {
            return strengths;
        }

        public void setStrengths(List<Strength> strengths) {
            this.strengths = strengths;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }
}
