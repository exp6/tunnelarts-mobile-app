package cl.skava.mobile.tunnelapp.domain.model.mapping;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueDiscontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputSelectionDiscontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.Rmr;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputSelection;

/**
 * Created by Jose Ignacio Vera on 26-07-2016.
 */
public class MappingInstance implements Serializable {

    private Mapping mapping;
    private BaseData mappingBaseData;
    private RockMass mappingRockMass;
    private List<Lithology> mappingLithologies;
    private List<Discontinuity> mappingDiscontinuities;
    private List<Discontinuity> deletedDiscontinuities = new ArrayList<>();
    private AdditionalDescription mappingAdditionalDescription;
    private AdditionalInformation mappingAdditionalInformation;
    private FailureZone mappingFailureZone;
    private Particularities mappingParticularities;
    private SpecialFeatures mappingSpecialFeatures;
    private RockMassHazard mappingRockMassHazard;
    private ExpandedTunnel mappingExpandedTunnel;
    private Gsi mappingGsi;
    private QValue mappingQValue;
    private Rmr mappingRmr;
    private SupportRecommendation mappingSupportRecommendation;
    private QValueInputSelection qValueInputSelection;
    private RmrInputSelection rmrInputSelection;
    private List<EvaluationMethodSelectionType> qValueEvaluationMethodSelectionTypes;
    private List<EvaluationMethodSelectionType> rmrEvaluationMethodSelectionTypes;
    private File[] mappingPictures;
    private List<Picture> mappingPicturesData;
    private File[] mappingOriginalPictures;
    private File[] mappingExpandedTunnelPictures;
    private File[] mappingGsiPictures;
    private List<RecommendationRockbolt> rockbolts;
    private List<RecommendationRockbolt> deletedRockbolts;
    private List<RecommendationShotcrete> shotcretes;
    private List<RecommendationShotcrete> deletedShotcretes;
    private List<QValueDiscontinuity> qValueDiscontinuities;
    private List<EvaluationMethodSelectionType> jrSelectionMap;
    private List<EvaluationMethodSelectionType> jaSelectionMap;
    private EvaluationMethodSelectionType emptyJrMap;
    private EvaluationMethodSelectionType emptyJaMap;
    private GsiValue mappingGsiValue;
    private StereonetPicture mappingStereonet;

    public Mapping getMapping() {
        return mapping;
    }

    public void setMapping(Mapping mapping) {
        this.mapping = mapping;
    }

    public BaseData getMappingBaseData() {
        return mappingBaseData;
    }

    public void setMappingBaseData(BaseData mappingBaseData) {
        this.mappingBaseData = mappingBaseData;
    }

    public RockMass getMappingRockMass() {
        return mappingRockMass;
    }

    public void setMappingRockMass(RockMass mappingRockMass) {
        this.mappingRockMass = mappingRockMass;
    }

    public List<Lithology> getMappingLithologies() {
        return mappingLithologies;
    }

    public void setMappingLithologies(List<Lithology> mappingLithologies) {
        this.mappingLithologies = mappingLithologies;
    }

    public List<Discontinuity> getMappingDiscontinuities() {
        return mappingDiscontinuities;
    }

    public void setMappingDiscontinuities(List<Discontinuity> mappingDiscontinuities) {
        this.mappingDiscontinuities = mappingDiscontinuities;
    }

    public AdditionalDescription getMappingAdditionalDescription() {
        return mappingAdditionalDescription;
    }

    public void setMappingAdditionalDescription(AdditionalDescription mappingAdditionalDescription) {
        this.mappingAdditionalDescription = mappingAdditionalDescription;
    }

    public AdditionalInformation getMappingAdditionalInformation() {
        return mappingAdditionalInformation;
    }

    public void setMappingAdditionalInformation(AdditionalInformation mappingAdditionalInformation) {
        this.mappingAdditionalInformation = mappingAdditionalInformation;
    }

    public FailureZone getMappingFailureZone() {
        return mappingFailureZone;
    }

    public void setMappingFailureZone(FailureZone mappingFailureZone) {
        this.mappingFailureZone = mappingFailureZone;
    }

    public Particularities getMappingParticularities() {
        return mappingParticularities;
    }

    public void setMappingParticularities(Particularities mappingParticularities) {
        this.mappingParticularities = mappingParticularities;
    }

    public SpecialFeatures getMappingSpecialFeatures() {
        return mappingSpecialFeatures;
    }

    public void setMappingSpecialFeatures(SpecialFeatures mappingSpecialFeatures) {
        this.mappingSpecialFeatures = mappingSpecialFeatures;
    }

    public RockMassHazard getMappingRockMassHazard() {
        return mappingRockMassHazard;
    }

    public void setMappingRockMassHazard(RockMassHazard mappingRockMassHazard) {
        this.mappingRockMassHazard = mappingRockMassHazard;
    }

    public ExpandedTunnel getMappingExpandedTunnel() {
        return mappingExpandedTunnel;
    }

    public void setMappingExpandedTunnel(ExpandedTunnel mappingExpandedTunnel) {
        this.mappingExpandedTunnel = mappingExpandedTunnel;
    }

    public Gsi getMappingGsi() {
        return mappingGsi;
    }

    public void setMappingGsi(Gsi mappingGsi) {
        this.mappingGsi = mappingGsi;
    }

    public QValue getMappingQValue() {
        return mappingQValue;
    }

    public void setMappingQValue(QValue mappingQValue) {
        this.mappingQValue = mappingQValue;
    }

    public Rmr getMappingRmr() {
        return mappingRmr;
    }

    public void setMappingRmr(Rmr mappingRmr) {
        this.mappingRmr = mappingRmr;
    }

    public SupportRecommendation getMappingSupportRecommendation() {
        return mappingSupportRecommendation;
    }

    public void setMappingSupportRecommendation(SupportRecommendation mappingSupportRecommendation) {
        this.mappingSupportRecommendation = mappingSupportRecommendation;
    }

    public QValueInputSelection getqValueInputSelection() {
        return qValueInputSelection;
    }

    public void setqValueInputSelection(QValueInputSelection qValueInputSelection) {
        this.qValueInputSelection = qValueInputSelection;
    }

    public RmrInputSelection getRmrInputSelection() {
        return rmrInputSelection;
    }

    public void setRmrInputSelection(RmrInputSelection rmrInputSelection) {
        this.rmrInputSelection = rmrInputSelection;
    }

    public List<EvaluationMethodSelectionType> getqValueEvaluationMethodSelectionTypes() {
        return qValueEvaluationMethodSelectionTypes;
    }

    public void setqValueEvaluationMethodSelectionTypes(List<EvaluationMethodSelectionType> qValueEvaluationMethodSelectionTypes) {
        this.qValueEvaluationMethodSelectionTypes = qValueEvaluationMethodSelectionTypes;
    }

    public List<EvaluationMethodSelectionType> getRmrEvaluationMethodSelectionTypes() {
        return rmrEvaluationMethodSelectionTypes;
    }

    public void setRmrEvaluationMethodSelectionTypes(List<EvaluationMethodSelectionType> rmrEvaluationMethodSelectionTypes) {
        this.rmrEvaluationMethodSelectionTypes = rmrEvaluationMethodSelectionTypes;
    }

    public File[] getMappingPictures() {
        return mappingPictures;
    }

    public void setMappingPictures(File[] mappingPictures) {
        this.mappingPictures = mappingPictures;
    }

    public List<Picture> getMappingPicturesData() {
        return mappingPicturesData;
    }

    public void setMappingPicturesData(List<Picture> mappingPicturesData) {
        this.mappingPicturesData = mappingPicturesData;
    }

    public File[] getMappingOriginalPictures() {
        return mappingOriginalPictures;
    }

    public void setMappingOriginalPictures(File[] mappingOriginalPictures) {
        this.mappingOriginalPictures = mappingOriginalPictures;
    }

    public File[] getMappingExpandedTunnelPictures() {
        return mappingExpandedTunnelPictures;
    }

    public void setMappingExpandedTunnelPictures(File[] mappingExpandedTunnelPictures) {
        this.mappingExpandedTunnelPictures = mappingExpandedTunnelPictures;
    }

    public File[] getMappingGsiPictures() {
        return mappingGsiPictures;
    }

    public void setMappingGsiPictures(File[] mappingGsiPictures) {
        this.mappingGsiPictures = mappingGsiPictures;
    }

    public List<RecommendationRockbolt> getRockbolts() {
        return rockbolts;
    }

    public void setRockbolts(List<RecommendationRockbolt> rockbolts) {
        this.rockbolts = rockbolts;
    }

    public List<RecommendationRockbolt> getDeletedRockbolts() {
        return deletedRockbolts;
    }

    public void setDeletedRockbolts(List<RecommendationRockbolt> deletedRockbolts) {
        this.deletedRockbolts = deletedRockbolts;
    }

    public List<RecommendationShotcrete> getShotcretes() {
        return shotcretes;
    }

    public void setShotcretes(List<RecommendationShotcrete> shotcretes) {
        this.shotcretes = shotcretes;
    }

    public List<RecommendationShotcrete> getDeletedShotcretes() {
        return deletedShotcretes;
    }

    public void setDeletedShotcretes(List<RecommendationShotcrete> deletedShotcretes) {
        this.deletedShotcretes = deletedShotcretes;
    }

    public List<QValueDiscontinuity> getqValueDiscontinuities() {
        return qValueDiscontinuities;
    }

    public void setqValueDiscontinuities(List<QValueDiscontinuity> qValueDiscontinuities) {
        this.qValueDiscontinuities = qValueDiscontinuities;
    }

    public List<EvaluationMethodSelectionType> getJrSelectionMap() {
        return jrSelectionMap;
    }

    public void setJrSelectionMap(List<EvaluationMethodSelectionType> jrSelectionMap) {
        this.jrSelectionMap = jrSelectionMap;
    }

    public List<EvaluationMethodSelectionType> getJaSelectionMap() {
        return jaSelectionMap;
    }

    public void setJaSelectionMap(List<EvaluationMethodSelectionType> jaSelectionMap) {
        this.jaSelectionMap = jaSelectionMap;
    }

    public EvaluationMethodSelectionType getEmptyJrMap() {
        return emptyJrMap;
    }

    public void setEmptyJrMap(EvaluationMethodSelectionType emptyJrMap) {
        this.emptyJrMap = emptyJrMap;
    }

    public EvaluationMethodSelectionType getEmptyJaMap() {
        return emptyJaMap;
    }

    public void setEmptyJaMap(EvaluationMethodSelectionType emptyJaMap) {
        this.emptyJaMap = emptyJaMap;
    }

    public GsiValue getMappingGsiValue() {
        return mappingGsiValue;
    }

    public void setMappingGsiValue(GsiValue mappingGsiValue) {
        this.mappingGsiValue = mappingGsiValue;
    }

    public List<Discontinuity> getDeletedDiscontinuities() {
        return deletedDiscontinuities;
    }

    public void setDeletedDiscontinuities(List<Discontinuity> deletedDiscontinuities) {
        this.deletedDiscontinuities = deletedDiscontinuities;
    }

    public StereonetPicture getMappingStereonet() {
        return mappingStereonet;
    }

    public void setMappingStereonet(StereonetPicture mappingStereonet) {
        this.mappingStereonet = mappingStereonet;
    }

    @Override
    public String toString() {
        return "MappingInstance{" +
                "mapping=" + mapping +
                ", mappingBaseData=" + mappingBaseData +
                ", mappingRockMass=" + mappingRockMass +
                ", mappingLithologies=" + mappingLithologies +
                ", mappingDiscontinuities=" + mappingDiscontinuities +
                ", deletedDiscontinuities=" + deletedDiscontinuities +
                ", mappingAdditionalDescription=" + mappingAdditionalDescription +
                ", mappingAdditionalInformation=" + mappingAdditionalInformation +
                ", mappingFailureZone=" + mappingFailureZone +
                ", mappingParticularities=" + mappingParticularities +
                ", mappingSpecialFeatures=" + mappingSpecialFeatures +
                ", mappingRockMassHazard=" + mappingRockMassHazard +
                ", mappingExpandedTunnel=" + mappingExpandedTunnel +
                ", mappingGsi=" + mappingGsi +
                ", mappingQValue=" + mappingQValue +
                ", mappingRmr=" + mappingRmr +
                ", mappingSupportRecommendation=" + mappingSupportRecommendation +
                ", qValueInputSelection=" + qValueInputSelection +
                ", rmrInputSelection=" + rmrInputSelection +
                ", qValueEvaluationMethodSelectionTypes=" + qValueEvaluationMethodSelectionTypes +
                ", rmrEvaluationMethodSelectionTypes=" + rmrEvaluationMethodSelectionTypes +
                ", mappingPictures=" + Arrays.toString(mappingPictures) +
                ", mappingPicturesData=" + mappingPicturesData +
                ", mappingOriginalPictures=" + Arrays.toString(mappingOriginalPictures) +
                ", mappingExpandedTunnelPictures=" + Arrays.toString(mappingExpandedTunnelPictures) +
                ", mappingGsiPictures=" + Arrays.toString(mappingGsiPictures) +
                ", rockbolts=" + rockbolts +
                ", deletedRockbolts=" + deletedRockbolts +
                ", shotcretes=" + shotcretes +
                ", deletedShotcretes=" + deletedShotcretes +
                ", qValueDiscontinuities=" + qValueDiscontinuities +
                ", jrSelectionMap=" + jrSelectionMap +
                ", jaSelectionMap=" + jaSelectionMap +
                ", emptyJrMap=" + emptyJrMap +
                ", emptyJaMap=" + emptyJaMap +
                ", mappingGsiValue=" + mappingGsiValue +
                ", mappingStereonet=" + mappingStereonet +
                '}';
    }
}
