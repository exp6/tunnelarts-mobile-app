package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.basedata;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;

import java.util.Calendar;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.BaseData;

/**
 * Created by Jose Ignacio Vera on 30-06-2016.
 */
public class BaseDataFragment extends Fragment {

    private static final String TAG = "BaseDataFragment";

    private static EditText date;
    private static EditText time;

    private BaseData mappingInput;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_basedata, container, false);
        rootView.setTag(TAG);

        date = (EditText) rootView.findViewById(R.id.input_date);
        time = (EditText) rootView.findViewById(R.id.input_time);

        ImageButton b = (ImageButton) rootView.findViewById(R.id.mapping_basedata_date_btn);
        b.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDatePickerDialog(v);
                    }
                });

        ImageButton b1 = (ImageButton) rootView.findViewById(R.id.mapping_basedata_time_btn);
        b1.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTimePickerDialog(v);
                    }
                });

        EditText inputDirection = ((EditText) rootView.findViewById(R.id.input_direction));
        EditText inputSlope = ((EditText) rootView.findViewById(R.id.input_slope));
        EditText inputRoundLength = ((EditText) rootView.findViewById(R.id.input_round_length));
        EditText inputDate = ((EditText) rootView.findViewById(R.id.input_date));
        EditText inputTime = ((EditText) rootView.findViewById(R.id.input_time));

        inputDirection.setText(mappingInput.getDirection());
        inputSlope.setText(mappingInput.getSlope());
        inputRoundLength.setText(mappingInput.getRoundLength());
        inputDate.setText(mappingInput.getDate());
        inputTime.setText(mappingInput.getTime());

        return rootView;
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getChildFragmentManager(), "datePicker");
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getChildFragmentManager(), "timePicker");
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMappingInput(BaseData mappingInput) {
        this.mappingInput = mappingInput;
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            date.setText(day + "/" + month + "/" + year);
        }
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            time.setText(hourOfDay + ":" + minute);
        }
    }
}
