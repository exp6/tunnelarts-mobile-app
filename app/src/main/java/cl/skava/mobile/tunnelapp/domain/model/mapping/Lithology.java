package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by Jose Ignacio Vera on 01-08-2016.
 */
public class Lithology extends DataInput {

    private String id;
    private Integer position;
    private Float presence;
    private Integer strength;
    private Integer colourValue;
    private Integer colourChroma;
    private Integer colorHue;
    private Integer texture;
    private Integer alterationWeathering;
    private Integer grainSize;
    private Integer geneticGroup;
    private Integer subGroup;
    private Integer jvMin;
    private Integer jvMax;
    private Integer blockShape;
    private Float blockSizeOne;
    private Float blockSizeTwo;
    private Float blockSizeThree;
    private String idMapping;
    private Boolean completed;
    private String briefDesc;
    private Integer formationUnit;

    public Lithology(String id, Integer position, Float presence, Integer strength, Integer colourValue, Integer colourChroma, Integer colorHue, Integer texture, Integer alterationWeathering, Integer grainSize, Integer geneticGroup, Integer subGroup, Integer jvMin, Integer jvMax, Integer blockShape, Float blockSizeOne, Float blockSizeTwo, Float blockSizeThree, String idMapping, Boolean completed, String briefDesc, Integer formationUnit) {
        this.id = id;
        this.position = position;
        this.presence = presence;
        this.strength = strength;
        this.colourValue = colourValue;
        this.colourChroma = colourChroma;
        this.colorHue = colorHue;
        this.texture = texture;
        this.alterationWeathering = alterationWeathering;
        this.grainSize = grainSize;
        this.geneticGroup = geneticGroup;
        this.subGroup = subGroup;
        this.jvMin = jvMin;
        this.jvMax = jvMax;
        this.blockShape = blockShape;
        this.blockSizeOne = blockSizeOne;
        this.blockSizeTwo = blockSizeTwo;
        this.blockSizeThree = blockSizeThree;
        this.idMapping = idMapping;
        this.completed = completed;
        this.briefDesc = briefDesc;
        this.formationUnit = formationUnit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Float getPresence() {
        return presence;
    }

    public void setPresence(Float presence) {
        this.presence = presence;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getColourValue() {
        return colourValue;
    }

    public void setColourValue(Integer colourValue) {
        this.colourValue = colourValue;
    }

    public Integer getColourChroma() {
        return colourChroma;
    }

    public void setColourChroma(Integer colourChroma) {
        this.colourChroma = colourChroma;
    }

    public Integer getColorHue() {
        return colorHue;
    }

    public void setColorHue(Integer colorHue) {
        this.colorHue = colorHue;
    }

    public Integer getTexture() {
        return texture;
    }

    public void setTexture(Integer texture) {
        this.texture = texture;
    }

    public Integer getAlterationWeathering() {
        return alterationWeathering;
    }

    public void setAlterationWeathering(Integer alterationWeathering) {
        this.alterationWeathering = alterationWeathering;
    }

    public Integer getGrainSize() {
        return grainSize;
    }

    public void setGrainSize(Integer grainSize) {
        this.grainSize = grainSize;
    }

    public Integer getGeneticGroup() {
        return geneticGroup;
    }

    public void setGeneticGroup(Integer geneticGroup) {
        this.geneticGroup = geneticGroup;
    }

    public Integer getSubGroup() {
        return subGroup;
    }

    public void setSubGroup(Integer subGroup) {
        this.subGroup = subGroup;
    }

    public Integer getJvMin() {
        return jvMin;
    }

    public void setJvMin(Integer jvMin) {
        this.jvMin = jvMin;
    }

    public Integer getJvMax() {
        return jvMax;
    }

    public void setJvMax(Integer jvMax) {
        this.jvMax = jvMax;
    }

    public Integer getBlockShape() {
        return blockShape;
    }

    public void setBlockShape(Integer blockShape) {
        this.blockShape = blockShape;
    }

    public Float getBlockSizeOne() {
        return blockSizeOne;
    }

    public void setBlockSizeOne(Float blockSizeOne) {
        this.blockSizeOne = blockSizeOne;
    }

    public Float getBlockSizeTwo() {
        return blockSizeTwo;
    }

    public void setBlockSizeTwo(Float blockSizeTwo) {
        this.blockSizeTwo = blockSizeTwo;
    }

    public Float getBlockSizeThree() {
        return blockSizeThree;
    }

    public void setBlockSizeThree(Float blockSizeThree) {
        this.blockSizeThree = blockSizeThree;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public String getBriefDesc() {
        return briefDesc;
    }

    public void setBriefDesc(String briefDesc) {
        this.briefDesc = briefDesc;
    }

    public Integer getFormationUnit() {
        return formationUnit;
    }

    public void setFormationUnit(Integer formationUnit) {
        this.formationUnit = formationUnit;
    }

    @Override
    public String toString() {
        return "Lithology{" +
                "id='" + id + '\'' +
                ", position=" + position +
                ", presence=" + presence +
                ", strength=" + strength +
                ", colourValue=" + colourValue +
                ", colourChroma=" + colourChroma +
                ", colorHue=" + colorHue +
                ", texture=" + texture +
                ", alterationWeathering=" + alterationWeathering +
                ", grainSize=" + grainSize +
                ", geneticGroup=" + geneticGroup +
                ", subGroup=" + subGroup +
                ", jvMin=" + jvMin +
                ", jvMax=" + jvMax +
                ", blockShape=" + blockShape +
                ", blockSizeOne=" + blockSizeOne +
                ", blockSizeTwo=" + blockSizeTwo +
                ", blockSizeThree=" + blockSizeThree +
                ", idMapping='" + idMapping + '\'' +
                ", completed=" + completed +
                ", briefDesc='" + briefDesc + '\'' +
                ", formationUnit=" + formationUnit +
                '}';
    }
}
