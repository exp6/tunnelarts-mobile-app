package cl.skava.mobile.tunnelapp.presentation.views.geoclassifier.mapping.expandedtunnel;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;

import java.lang.annotation.Target;

/**
 * Created by Carlos Vergara on 23-02-2017.
 */

public class LockableScrollView extends ScrollView {
    private boolean isLocked = false;

    public LockableScrollView(Context context) {
        super(context);
    }

    public LockableScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LockableScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public LockableScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setLocked(boolean isLocked) {
        this.isLocked = isLocked;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        //return (isLocked == true)? false : super.onInterceptTouchEvent(ev);
        /*if (!this.isLocked) {
            return super.onInterceptTouchEvent(ev);
        }*/
        //return this.dispatchTouchEvent(ev);
        //return false;
        if (this.isLocked) {
            return false;
        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        //return (isLocked == true)? false : super.onInterceptTouchEvent(ev);
        /*if (!this.isLocked) {
            return super.onTouchEvent(ev);
        }*/
        //return this.dispatchTouchEvent(ev);
        //return false;
        boolean ret;
        if (this.isLocked) {
            ret = false;
        } else {
            ret = super.onTouchEvent(ev);
        }
        return ret;
    }


}
