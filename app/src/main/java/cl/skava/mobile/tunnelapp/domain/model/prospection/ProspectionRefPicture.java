package cl.skava.mobile.tunnelapp.domain.model.prospection;

/**
 * Created by Jose Ignacio Vera on 11-11-2016.
 */
public class ProspectionRefPicture {

    private String id;
    private String idProspectionHoleGroup;
    private Integer code;
    private String sideName;
    private String remoteUri;
    private String localUri;
    private String remoteUriOriginal;
    private String localUriOriginal;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdProspectionHoleGroup() {
        return idProspectionHoleGroup;
    }

    public void setIdProspectionHoleGroup(String idProspectionHoleGroup) {
        this.idProspectionHoleGroup = idProspectionHoleGroup;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getSideName() {
        return sideName;
    }

    public void setSideName(String sideName) {
        this.sideName = sideName;
    }

    public String getRemoteUri() {
        return remoteUri;
    }

    public void setRemoteUri(String remoteUri) {
        this.remoteUri = remoteUri;
    }

    public String getLocalUri() {
        return localUri;
    }

    public void setLocalUri(String localUri) {
        this.localUri = localUri;
    }

    public String getRemoteUriOriginal() {
        return remoteUriOriginal;
    }

    public void setRemoteUriOriginal(String remoteUriOriginal) {
        this.remoteUriOriginal = remoteUriOriginal;
    }

    public String getLocalUriOriginal() {
        return localUriOriginal;
    }

    public void setLocalUriOriginal(String localUriOriginal) {
        this.localUriOriginal = localUriOriginal;
    }

    @Override
    public String toString() {
        return "ProspectionRefPicture{" +
                "id='" + id + '\'' +
                ", idProspectionHoleGroup='" + idProspectionHoleGroup + '\'' +
                ", code=" + code +
                ", sideName='" + sideName + '\'' +
                ", remoteUri='" + remoteUri + '\'' +
                ", localUri='" + localUri + '\'' +
                ", remoteUriOriginal='" + remoteUriOriginal + '\'' +
                ", localUriOriginal='" + localUriOriginal + '\'' +
                '}';
    }
}
