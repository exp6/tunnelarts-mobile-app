package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by Jose Ignacio Vera on 12-10-2016.
 */
public class RecommendationShotcrete extends DataInput {

    private String id;
    private Integer position;
    private Float thickness;
    private Integer fiberReinforced;
    private Float pk;
    private Integer location;
    private String timeFrom;
    private String timeTo;
    private String idMapping;
    private Boolean completed;

    public RecommendationShotcrete(String id, Integer position, Float thickness, Integer fiberReinforced, Float pk, Integer location, String timeFrom, String timeTo, String idMapping, Boolean completed) {
        this.id = id;
        this.position = position;
        this.thickness = thickness;
        this.fiberReinforced = fiberReinforced;
        this.pk = pk;
        this.location = location;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.idMapping = idMapping;
        this.completed = completed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Float getThickness() {
        return thickness;
    }

    public void setThickness(Float thickness) {
        this.thickness = thickness;
    }

    public Integer getFiberReinforced() {
        return fiberReinforced;
    }

    public void setFiberReinforced(Integer fiberReinforced) {
        this.fiberReinforced = fiberReinforced;
    }

    public Float getPk() {
        return pk;
    }

    public void setPk(Float pk) {
        this.pk = pk;
    }

    public Integer getLocation() {
        return location;
    }

    public void setLocation(Integer location) {
        this.location = location;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "RecommendationShotcrete{" +
                "id='" + id + '\'' +
                ", position=" + position +
                ", thickness=" + thickness +
                ", fiberReinforced=" + fiberReinforced +
                ", pk=" + pk +
                ", location=" + location +
                ", timeFrom='" + timeFrom + '\'' +
                ", timeTo='" + timeTo + '\'' +
                ", idMapping='" + idMapping + '\'' +
                ", completed=" + completed +
                '}';
    }
}
