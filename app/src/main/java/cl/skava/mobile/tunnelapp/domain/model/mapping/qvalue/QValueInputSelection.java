package cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue;

import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInput;

/**
 * Created by Jose Ignacio Vera on 24-08-2016.
 */
public class QValueInputSelection extends DataInput {

    private String id;
    private String idMapping;
    private String ja;
    private String jn;
    private String jr;
    private String jw;
    private String srf;
    private Boolean jnIntersection;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public String getJa() {
        return ja;
    }

    public void setJa(String ja) {
        this.ja = ja;
    }

    public String getJn() {
        return jn;
    }

    public void setJn(String jn) {
        this.jn = jn;
    }

    public String getJr() {
        return jr;
    }

    public void setJr(String jr) {
        this.jr = jr;
    }

    public String getJw() {
        return jw;
    }

    public void setJw(String jw) {
        this.jw = jw;
    }

    public String getSrf() {
        return srf;
    }

    public void setSrf(String srf) {
        this.srf = srf;
    }

    public Boolean getJnIntersection() {
        return jnIntersection;
    }

    public void setJnIntersection(Boolean jnIntersection) {
        this.jnIntersection = jnIntersection;
    }

    @Override
    public String toString() {
        return "QValueInputSelection{" +
                "id='" + id + '\'' +
                ", idMapping='" + idMapping + '\'' +
                ", ja='" + ja + '\'' +
                ", jn='" + jn + '\'' +
                ", jr='" + jr + '\'' +
                ", jw='" + jw + '\'' +
                ", srf='" + srf + '\'' +
                ", jnIntersection=" + jnIntersection +
                '}';
    }
}
