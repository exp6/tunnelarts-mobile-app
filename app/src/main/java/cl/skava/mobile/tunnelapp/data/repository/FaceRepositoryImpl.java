package cl.skava.mobile.tunnelapp.data.repository;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.repository.FaceRepository;

/**
 * Created by Jose Ignacio Vera on 21-06-2016.
 */
public class FaceRepositoryImpl implements FaceRepository {

    private final HashMap<Tunnel, List<Face>> facesByTunnel = new HashMap<>();
    private CloudStoreService cloudStoreService;

    public FaceRepositoryImpl(Context activity) {
        cloudStoreService = new CloudStoreService(activity);
        initDataset();
    }

    @Override
    public List<Face> getFacesByTunnel(Tunnel tunnel) {
        List<Face> faces = new ArrayList<>();
        Iterator<Tunnel> iter = facesByTunnel.keySet().iterator();
        Tunnel t;

        while(iter.hasNext()) {
            t = iter.next();

            if(t.getId().equalsIgnoreCase(tunnel.getId())) {
                faces = facesByTunnel.get(t);
                break;
            }
        }
        return faces;
    }

    private void initDataset() {
        List<Project> ps = cloudStoreService.getProjects();

        for(Project p : ps) {
            List<Tunnel> tunnels = cloudStoreService.getTunnelsByProject(p.getId());

            for(Tunnel t : tunnels) {
                List<Face> faces = cloudStoreService.getFacesByTunnel(t.getId());
                facesByTunnel.put(t, faces);
            }
        }
    }
}
