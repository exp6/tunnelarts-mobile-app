package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by Jose Ignacio Vera on 04-01-2017.
 */
public class GsiValue extends DataInput {

    private String id;
    private String idMapping;
    private Integer structure;
    private Integer surface;
    private Float max;
    private Float min;
    private Float avg;
    private Boolean completed;

    public GsiValue(String id, String idMapping, Integer structure, Integer surface, Float max, Float min, Float avg, Boolean completed) {
        this.id = id;
        this.idMapping = idMapping;
        this.structure = structure;
        this.surface = surface;
        this.max = max;
        this.min = min;
        this.avg = avg;
        this.completed = completed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Integer getStructure() {
        return structure;
    }

    public void setStructure(Integer structure) {
        this.structure = structure;
    }

    public Integer getSurface() {
        return surface;
    }

    public void setSurface(Integer surface) {
        this.surface = surface;
    }

    public Float getMax() {
        return max;
    }

    public void setMax(Float max) {
        this.max = max;
    }

    public Float getMin() {
        return min;
    }

    public void setMin(Float min) {
        this.min = min;
    }

    public Float getAvg() {
        return avg;
    }

    public void setAvg(Float avg) {
        this.avg = avg;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "GsiValue{" +
                "id='" + id + '\'' +
                ", idMapping='" + idMapping + '\'' +
                ", structure=" + structure +
                ", surface=" + surface +
                ", max=" + max +
                ", min=" + min +
                ", avg=" + avg +
                ", completed=" + completed +
                '}';
    }
}
