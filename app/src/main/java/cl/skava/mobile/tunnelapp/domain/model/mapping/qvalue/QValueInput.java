package cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue;

import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInput;
import cl.skava.mobile.tunnelapp.domain.model.mapping.DataInputType;

/**
 * Created by Jose Ignacio Vera on 24-08-2016.
 */
public class QValueInput extends DataInputType {

    private String id;
    private String codeIndex;
    private String description;
    private Float startValue;
    private Float endValue;
    private String idQValueInputGroup;
    private String descriptionEs;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodeIndex() {
        return codeIndex;
    }

    public void setCodeIndex(String codeIndex) {
        this.codeIndex = codeIndex;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getStartValue() {
        return startValue;
    }

    public void setStartValue(Float startValue) {
        this.startValue = startValue;
    }

    public Float getEndValue() {
        return endValue;
    }

    public void setEndValue(Float endValue) {
        this.endValue = endValue;
    }

    public String getIdQValueInputGroup() {
        return idQValueInputGroup;
    }

    public void setIdQValueInputGroup(String idQValueInputGroup) {
        this.idQValueInputGroup = idQValueInputGroup;
    }

    public String getDescriptionEs() {
        return descriptionEs;
    }

    public void setDescriptionEs(String descriptionEs) {
        this.descriptionEs = descriptionEs;
    }

    @Override
    public String toString() {
        return "QValueInput{" +
                "id='" + id + '\'' +
                ", codeIndex='" + codeIndex + '\'' +
                ", description='" + description + '\'' +
                ", startValue=" + startValue +
                ", endValue=" + endValue +
                ", idQValueInputGroup='" + idQValueInputGroup + '\'' +
                ", descriptionEs='" + descriptionEs + '\'' +
                '}';
    }
}
