package cl.skava.mobile.tunnelapp.domain.interactors.impl.geoclassifier;

import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.base.AbstractInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.geoclassifier.MappingInputsInteractor;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInstance;
import cl.skava.mobile.tunnelapp.domain.repository.MappingInputRepository;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public class MappingInputsInteractorImpl extends AbstractInteractor implements MappingInputsInteractor {

    private MappingInputsInteractor.Callback mCallback;
    private MappingInputRepository mMappingInputRepository;

    public MappingInputsInteractorImpl(Executor threadExecutor,
                                       MainThread mainThread,
                                       MappingInputsInteractor.Callback callback,
                                       MappingInputRepository mappingInputRepository) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mMappingInputRepository = mappingInputRepository;
    }

    private void postMappingInstance(final MappingInstance mappingInstance) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onMappingInstanceRetrieved(mappingInstance);
            }
        });
    }

    private void postSaveStatus(final boolean success) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSaveStatus(success);
            }
        });
    }

    @Override
    public void run() {
        final MappingInstance mappingInstance = mMappingInputRepository.getMappingInstance();

        if(mappingInstance == null) {
            return;
        }

        postMappingInstance(mappingInstance);
    }

    @Override
    public void saveMappingInstance(MappingInstance mappingInstance) {
        final boolean success = mMappingInputRepository.saveMappingInstance(mappingInstance);
        postSaveStatus(success);
    }
}
