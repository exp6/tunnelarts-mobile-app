package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse;

import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;

/**
 * Created by Carlos Vergara on 20-03-2017.
 */

public class ProspectionHoleGroupSyncResponseHandler extends FetchFromTableSyncResponseHandler<ProspectionHoleGroup> {
    ProspectionHoleGroupSyncResponseHandler() {

    }

    @Override
    protected void onDataFetchedSuccessfully(CloudMobileService cloudMobileService, List fetchedData) {
        CloudStoreService cloudStoreService = (CloudStoreService) cloudMobileService;
        cloudStoreService.syncProspectionHoles(fetchedData);
    }

    @Override
    protected void initializeClassToFetchFrom() {
        setClassToFetchFrom(ProspectionHoleGroup.class);
    }

    @Override
    protected void onDataFetchedFailed(CloudMobileService cloudMobileService) {
        cloudMobileService.onSyncResult(2);
    }
}
