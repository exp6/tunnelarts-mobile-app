package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;

/**
 * Created by Carlos Vergara on 20-03-2017.
 */

public class SingleTableSyncResponseHandler implements SyncResponseHandler {
    private Class classToSync;

    SingleTableSyncResponseHandler(Class classToSync) {
        this.classToSync = classToSync;
    }

    @Override
    public void handleResponseFromService(CloudMobileService cloudMobileService) {
        CloudStoreService cloudStoreService = (CloudStoreService) cloudMobileService;
        cloudStoreService.incrementProgress();
        cloudStoreService.syncSingleTable(classToSync);
    }
}
