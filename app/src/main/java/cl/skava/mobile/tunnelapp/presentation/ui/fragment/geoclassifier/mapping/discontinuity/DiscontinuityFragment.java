package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.discontinuity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Discontinuity;
import cl.skava.mobile.tunnelapp.presentation.ui.activity.geoclassifier.mapping.MappingInputActivity;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class DiscontinuityFragment extends Fragment {

    private static final String TAG = "DiscontinuityFragment";

    private Discontinuity mappingInput;
    private Boolean mappingFinished;
    private DiscontinuitiesFragment parent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parent = getParentFragmentFromContainer(container);

        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_discontinuity, container, false);
        rootView.setTag(TAG);

        Spinner spinnerType = (Spinner) rootView.findViewById(R.id.spinner_type);
        Spinner spinnerRelevance = (Spinner) rootView.findViewById(R.id.spinner_relevance);
        EditText inputOrientationDD = ((EditText) rootView.findViewById(R.id.input_orientation_dd));
        EditText inputOrientationD = ((EditText) rootView.findViewById(R.id.input_orientation_d));
        Spinner spinnerSpacing = (Spinner) rootView.findViewById(R.id.spinner_spacing);
        Spinner spinnerPersistence = (Spinner) rootView.findViewById(R.id.spinner_persistence);
        Spinner spinnerOpening = (Spinner) rootView.findViewById(R.id.spinner_opening);
        Spinner spinnerRoughness = (Spinner) rootView.findViewById(R.id.spinner_roughness);
        Spinner spinnerInfilling = (Spinner) rootView.findViewById(R.id.spinner_infilling);
        Spinner spinnerInfillingType = (Spinner) rootView.findViewById(R.id.spinner_infilling_type);
        Spinner spinnerWeathering = (Spinner) rootView.findViewById(R.id.spinner_weathering);
        Spinner spinnerSlickensided = (Spinner) rootView.findViewById(R.id.spinner_slickensided);

        spinnerType.setSelection(mappingInput.getType());
        spinnerRelevance.setSelection(mappingInput.getRelevance());
        inputOrientationDD.setText(mappingInput.getOrientationDd().toString());
        inputOrientationD.setText(mappingInput.getOrientationD().toString());
        spinnerSpacing.setSelection(mappingInput.getSpacing());
        spinnerPersistence.setSelection(mappingInput.getPersistence());
        spinnerOpening.setSelection(mappingInput.getOpening());
        spinnerRoughness.setSelection(mappingInput.getRoughness());
        spinnerInfilling.setSelection(mappingInput.getInfilling());
        spinnerInfillingType.setSelection(mappingInput.getInfillingType());
        spinnerWeathering.setSelection(mappingInput.getWeathering());
        spinnerSlickensided.setSelection(mappingInput.getSlickensided());

        if(mappingFinished) {
            spinnerType.setEnabled(false);
            spinnerRelevance.setEnabled(false);
            inputOrientationDD.setEnabled(false);
            inputOrientationD.setEnabled(false);
            spinnerSpacing.setEnabled(false);
            spinnerPersistence.setEnabled(false);
            spinnerOpening.setEnabled(false);
            spinnerRoughness.setEnabled(false);
            spinnerInfilling.setEnabled(false);
            spinnerInfillingType.setEnabled(false);
            spinnerWeathering.setEnabled(false);
            spinnerSlickensided.setEnabled(false);
        }

        attachDeleteEvent(rootView);

        return rootView;
    }

    private DiscontinuitiesFragment getParentFragmentFromContainer(ViewGroup container) {
        MappingInputActivity parentActivity = (MappingInputActivity) container.getContext();
        return (DiscontinuitiesFragment) parentActivity.getCurrentFragment();
    }

    private void attachDeleteEvent(View view) {
        int buttonId = R.id.discontinuities_delete;
        Button deleteButton = (Button) view.findViewById(buttonId);
        Button.OnClickListener listener = getDeleteListener();
        deleteButton.setOnClickListener(listener);
    }

    private Button.OnClickListener getDeleteListener() {
        return new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mappingFinished) {
                    return;
                }
                deleteDiscontinuity(mappingInput);
            }
        };
    }

    private void deleteDiscontinuity(Discontinuity discontinuity) {
        parent.deleteDiscontinuity(discontinuity.getPosition());
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public Discontinuity getMappingInput() {
        return mappingInput;
    }

    public void setMappingInput(Discontinuity mappingInput) {
        this.mappingInput = mappingInput;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }
}
