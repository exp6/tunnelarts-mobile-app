package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;

/**
 * Created by Carlos Vergara on 20-03-2017.
 */

public class FaceSyncResponseHandler implements SyncResponseHandler {
    FaceSyncResponseHandler() {

    }

    @Override
    public void handleResponseFromService(CloudMobileService cloudMobileService) {
        CloudStoreService cloudStoreService = (CloudStoreService) cloudMobileService;
        if (cloudStoreService.isSyncAll()) {
            cloudStoreService.syncGeoCharacterizerData();
        } else if (cloudStoreService.isSyncFaceProgress()) {
            cloudStoreService.setSyncFaceProgress(false);
            cloudStoreService.syncGeoCharacterizerData();
        } else {
            cloudStoreService.incrementProgress();
            cloudStoreService.onSyncResult(1);
        }
    }
}
