package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class BaseData extends DataInput {

    private String id;
    private String advisor;
    private String direction;
    private String slope;
    private String roundLength;
    private String date;
    private String time;
    private String idMapping;
    private Boolean completed;

    public BaseData(String id, String advisor, String direction, String slope, String roundLength, String date, String time, String idMapping, Boolean completed) {
        this.id = id;
        this.advisor = advisor;
        this.direction = direction;
        this.slope = slope;
        this.roundLength = roundLength;
        this.date = date;
        this.time = time;
        this.idMapping = idMapping;
        this.completed = completed;
    }

    public String getAdvisor() {
        return advisor;
    }

    public void setAdvisor(String advisor) {
        this.advisor = advisor;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getSlope() {
        return slope;
    }

    public void setSlope(String slope) {
        this.slope = slope;
    }

    public String getRoundLength() {
        return roundLength;
    }

    public void setRoundLength(String roundLength) {
        this.roundLength = roundLength;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "BaseData{" +
                "id='" + id + '\'' +
                ", advisor='" + advisor + '\'' +
                ", direction='" + direction + '\'' +
                ", slope='" + slope + '\'' +
                ", roundLength='" + roundLength + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", idMapping='" + idMapping + '\'' +
                ", completed=" + completed +
                '}';
    }
}
