package cl.skava.mobile.tunnelapp.presentation.ui.fragment.prospection;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.data.cache.FileManager;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;

/**
 * Created by Jose Ignacio Vera on 10-11-2016.
 */
public class ProspectionImagesFragment extends Fragment {

    private static final String TAG = "ProspectionImagesFragment";
    private Uri pictureURI;

    private ImageButton editMode;
    private ImageButton undoPic;

    private File[] mFiles;
    private File[] mFilesOriginal;

    private String pathAviaryDir = "/sdcard/DCIM/100AVIARY";

    private File photoFile;
    private Uri photoURI;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int EDIT_PICTURE_REQUEST_CODE = 2;

    private ImageView refImageView;
    private ImageView refImageViewOriginal;

    private ProspectionRefPicture prospectionRefPicture;

    private OnProspectionPicturelListener mCallback;

    public interface OnProspectionPicturelListener {
        void onProspectionPicturelListener(int prospectionHoleGroupPosition);
        void onTakeRefPicture();
        void onEditRefPicture();
        void onUndoRefPicture();
    }

    private Integer prospectionHoleGroupPosition;

    private List<ProspectionRefPicture> mProspectionRefPictures;

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnProspectionPicturelListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnProspectionPicturelListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_prospection_images, container, false);
        rootView.setTag(TAG);

        refImageView = (ImageView) rootView.findViewById(R.id.image);

        ImageButton takePicture = (ImageButton) rootView.findViewById(R.id.image_btn_take_pic);
        editMode = (ImageButton) rootView.findViewById(R.id.image_btn_edit_pic);
        undoPic = (ImageButton) rootView.findViewById(R.id.image_btn_undo_pic);

        takePicture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCallback.onTakeRefPicture();
            }
        });

        editMode.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCallback.onEditRefPicture();
            }
        });

        undoPic.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Handler h = new Handler(Looper.getMainLooper());
                h.post(new Runnable() {
                    public void run() {
                        new android.support.v7.app.AlertDialog.Builder(getActivity())
                                .setTitle(getResources().getString(R.string.characterization_mapping_input_pic_label_switch_original))
                                .setMessage(getResources().getString(R.string.characterization_mapping_input_pic_label_switch_original_msg) + getResources().getString(R.string.system_label_proceed))
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(getResources().getString(R.string.system_label_yes), new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        mCallback.onUndoRefPicture();
                                    }
                                })
                                .setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                })
                                .show();
                    }
                });
            }
        });

        if(mProspectionRefPictures != null) {
            if(!mProspectionRefPictures.isEmpty()) {
                if(mProspectionRefPictures.get(0).getLocalUri() != null) {
                    mFiles = new File[mProspectionRefPictures.size()];
                    mFiles[0] = new File(mProspectionRefPictures.get(0).getLocalUri().trim() + File.separator + mProspectionRefPictures.get(0).getSideName().trim()+".jpg");
                }
                else {
                    editMode.setVisibility(View.GONE);
                    undoPic.setVisibility(View.GONE);
                }
            }
            else {
                editMode.setVisibility(View.GONE);
                undoPic.setVisibility(View.GONE);
            }
        }
        else {
            editMode.setVisibility(View.GONE);
            undoPic.setVisibility(View.GONE);
            takePicture.setVisibility(View.GONE);
        }

        if(mFiles != null) {
            if(mFiles.length > 0) {
                refImageView = getImageFromFile(mFiles[0], refImageView);
            }
        }
        else {
            editMode.setVisibility(View.GONE);
            undoPic.setVisibility(View.GONE);
        }

        return rootView;
    }

    private ImageView getImageFromFile(File imgFile, ImageView myImage) {
        if(imgFile != null) {
            if(imgFile.exists()){
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

                int reqWidth = dpToPx(250);
                int reqHeight = dpToPx(250);

                options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
                options.inJustDecodeBounds = false;
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

                myImage.setImageBitmap(myBitmap);
            }
        }
        return myImage;
    }

    private int dpToPx(int dp) {
        float density = getActivity().getApplicationContext().getResources().getDisplayMetrics().density;
        return Math.round((float)dp * density);
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setProspectionHoleGroupPosition(Integer prospectionHoleGroupPosition) {
        this.prospectionHoleGroupPosition = prospectionHoleGroupPosition;
    }

    public void setmProspectionRefPictures(List<ProspectionRefPicture> mProspectionRefPictures) {
        this.mProspectionRefPictures = mProspectionRefPictures;
    }
}
