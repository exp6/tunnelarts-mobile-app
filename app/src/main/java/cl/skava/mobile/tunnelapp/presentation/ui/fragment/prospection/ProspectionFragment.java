package cl.skava.mobile.tunnelapp.presentation.ui.fragment.prospection;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.data.cache.FileManager;
import cl.skava.mobile.tunnelapp.domain.executor.impl.ThreadExecutor;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroupCalculation;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.presentation.presenters.impl.prospection.ProspectionPresenterImpl;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.prospection.ProspectionPresenter;
import cl.skava.mobile.tunnelapp.threading.MainThreadImpl;
import id.zelory.compressor.Compressor;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class ProspectionFragment extends Fragment
        implements ProspectionPresenter.View,
        ProspectionHoleGroupListFragment.OnProspectionHoleGroupListlListener,
        ProspectionModulesFragment.OnProspectionModulesListener {

    private static final String TAG = "ProspectionFragment";

    //User Selection Context
    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Module selectedModule;

    private View dialogContent;
    private AlertDialog alertDialog;
    private ProgressDialog uiProgressDialog;

    private User mUser;
    private ProspectionHoleGroup selectedProspectionHoleGroup;
    private OnProspectionItemListener mCallback;

    private Integer prospectionHoleGroupPosition = 0;
    private Integer viewpagerPosition = 0;

    private Boolean mForImg = false;

    public interface OnProspectionItemListener {
        void onProspectionHoleSelected(ProspectionHoleGroup prospectionHoleGroup);
        void onSyncFinish();
        void hideDelete();
        void hideEdit();
        void showDelete();
        void showEdit();
    }

    private ProspectionPresenter mPresenter;
    protected List<ProspectionHoleGroup> mProspectionHoleGroupDataset;
    private HashMap<Mapping, QValue> mQValuesByMappings;

    private Spinner spinnerMappingQvalue;
    private Spinner spinnerMappingQvalue2;

    private boolean noMappings = false;

    private HashMap<ProspectionHole, List<Rod>> mRodsByProspectionHole;

    private File photoFile;
    private Uri photoURI;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int EDIT_PICTURE_REQUEST_CODE = 2;

    private ProspectionRefPicture prospectionRefPicture;
    private ImageView refImageView;
    private ImageView refImageViewOriginal;

    private File[] mFiles;
    private File[] mFilesOriginal;

    List<ProspectionRefPicture> mProspectionRefPictures;

    private HashMap<ProspectionHoleGroup, List<ProspectionHoleGroupCalculation>> mCalculationsByGroup;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallback = (OnProspectionItemListener) getActivity();

        if (savedInstanceState == null) {
            initDatasources();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_prospection, container, false);
        rootView.setTag(TAG);

        dialogContent = inflater.inflate(R.layout.fragment_prospection_hole_group_new, null);

        FloatingActionButton myFab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                newProspectionHoleGroup();
            }
        });

        return rootView;
    }

    private Float calculateQ(QValue qValue) {
        Float rqdValue = qValue.getRqd();
        Float jnValue = qValue.getJn();
        Float jrValue = qValue.getJr();
        Float jaValue = qValue.getJa();
        Float jwValue = qValue.getJw();
        Float srfValue = qValue.getSrf();

        Float degreeOfJointing;
        Float jointFrictions;
        Float activeStress;

        Float qValueFinal;

        if(rqdValue < 10) {
            rqdValue = new Float(10);
        }

        degreeOfJointing = rqdValue/jnValue;
        jointFrictions = jrValue/jaValue;
        activeStress = jwValue/srfValue;

        qValueFinal = (degreeOfJointing * jointFrictions * activeStress);

        return qValueFinal;
    }

    @Override
    public void onResume() {
        super.onResume();
//        if(!mForImg) viewpagerPosition = 0;
//        else viewpagerPosition = 1;
//        if(prospectionHoleGroupPosition != 0) viewpagerPosition = 1;
        mPresenter.resume();
    }

    private void newProspectionHoleGroup() {
        if(alertDialog == null)
            alertDialog = new AlertDialog.Builder(getActivity())
                    .create();

        final int index = mProspectionHoleGroupDataset.size() + 1;

        alertDialog.setTitle(getResources().getString(R.string.forecasting_label_prospection_hole_new_title));
        alertDialog.setMessage(getResources().getString(R.string.forecasting_label_prospection_hole_new_msg) + " " + index);
        // Alert dialog button
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.system_label_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        if(!noMappings) {
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

//                            EditText input = (EditText) dialogContent.findViewById(R.id.input_prospection_name);

                            List<Mapping> mappings = new ArrayList<Mapping>(mQValuesByMappings.keySet());
                            List<QValue> qValues = new ArrayList<QValue>(mQValuesByMappings.values());
                            int chainageItem = spinnerMappingQvalue.getSelectedItemPosition();
                            int qValueItem = spinnerMappingQvalue2.getSelectedItemPosition();

                            Float qValue;

                            switch (qValueItem) {
                                case 1:
                                    qValue = qValues.get(chainageItem).getqAvg();
                                    break;
                                case 2:
                                    qValue = qValues.get(chainageItem).getqMax();
                                    break;
                                default:
                                    qValue = qValues.get(chainageItem).getqMin();
                                    break;
                            }

                            ProspectionHoleGroup prospectionHoleGroup = new ProspectionHoleGroup();
                            prospectionHoleGroup.setName("PHG " + index);
                            prospectionHoleGroup.setqValueSelected(qValue);
                            prospectionHoleGroup.setIdMapping(mappings.get(chainageItem).getId());
                            prospectionHoleGroup.setIdFace(selectedFace.getId());
                            prospectionHoleGroup.setIdUser(mUser.getId().toUpperCase());
                            prospectionHoleGroup.setCreatedAt(new Date());
                            dialog.dismiss();

                            mPresenter.createNewProspectionHoleGroup(prospectionHoleGroup);
//                            input.setText("");
                        }
                    });
        }

        alertDialog.setView(dialogContent);
        alertDialog.show();
    }

    public void editSelectedItem() {
//        EditText input = (EditText) dialogContent.findViewById(R.id.input_prospection_name);
//        input.setText(selectedProspectionHoleGroup.getName().trim());
        List<Mapping> mappings = new ArrayList<Mapping>(mQValuesByMappings.keySet());

        int position = 0;

        for(int i = 0; i < mappings.size(); i++) {
            Mapping m = mappings.get(i);
            if(m.getId().equalsIgnoreCase(selectedProspectionHoleGroup.getIdMapping())) {
                position = i;
                break;
            }
        }

        spinnerMappingQvalue.setSelection(position);

        if(alertDialog == null)
            alertDialog = new AlertDialog.Builder(getActivity()).create();

        alertDialog.setTitle(getResources().getString(R.string.forecasting_label_prospection_hole_edit_title));
        alertDialog.setMessage(getResources().getString(R.string.forecasting_label_prospection_hole_edit_msg) + " " + selectedProspectionHoleGroup.getName().trim());
        // Alert dialog button
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.system_label_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        if(!noMappings) {
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
//                            EditText input = (EditText) dialogContent.findViewById(R.id.input_prospection_name);

                            List<Mapping> mappings = new ArrayList<Mapping>(mQValuesByMappings.keySet());
                            List<QValue> qValues = new ArrayList<QValue>(mQValuesByMappings.values());
                            int chainageItem = spinnerMappingQvalue.getSelectedItemPosition();

                            int qValueItem = spinnerMappingQvalue2.getSelectedItemPosition();

                            Float qValue;

                            switch (qValueItem) {
                                case 1:
                                    qValue = qValues.get(chainageItem).getqAvg();
                                    break;
                                case 2:
                                    qValue = qValues.get(chainageItem).getqMax();
                                    break;
                                default:
                                    qValue = qValues.get(chainageItem).getqMin();
                                    break;
                            }

                            selectedProspectionHoleGroup.setqValueSelected(qValue);
                            selectedProspectionHoleGroup.setIdMapping(mappings.get(chainageItem).getId());
                            dialog.dismiss();

                            mPresenter.updateProspectionHoleGroup(selectedProspectionHoleGroup);
                        }
                    });
        }

        alertDialog.setView(dialogContent);
        alertDialog.show();
    }

    private void initDatasources() {
        mPresenter = new ProspectionPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                getActivity(),
                selectedFace.getId());

        mProspectionHoleGroupDataset = new ArrayList<>();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setSelectionContext(Project project,
                                    Tunnel tunnel,
                                    Face face,
                                    Module module,
                                    User user) {
        selectedProject = project;
        selectedTunnel = tunnel;
        selectedFace = face;
        selectedModule = module;
        mUser = user;
    }

    @Override
    public void showProgress() {
        uiProgressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_Dark_Dialog);
        uiProgressDialog.setCancelable(false);
        uiProgressDialog.setMessage(getResources().getString(R.string.system_label_preparing_user_interface));
        uiProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        uiProgressDialog.dismiss();
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void displayProspectionHoleGroupList(List<ProspectionHoleGroup> prospectionHoleGroups) {
        mProspectionHoleGroupDataset = prospectionHoleGroups;

        if(mProspectionHoleGroupDataset.isEmpty()) {
            mCallback.hideDelete();
            mCallback.hideEdit();
        }
        else {
            mCallback.showDelete();
            mCallback.showEdit();
        }

        if(mProspectionHoleGroupDataset != null){
            if(!mProspectionHoleGroupDataset.isEmpty()){
//                if(!mForImg) selectedProspectionHoleGroup = mProspectionHoleGroupDataset.get(0);
                selectedProspectionHoleGroup = mProspectionHoleGroupDataset.get(prospectionHoleGroupPosition);
                mPresenter.getRodsByProspectionHoleMap(selectedProspectionHoleGroup.getId());
            }
            else {
                ProspectionModulesFragment fragment = new ProspectionModulesFragment();
                fragment.setRodsByProspection(null);
                fragment.onAttachFragment(this);
                fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, mUser, null);
                fragment.setViewpagerPosition(viewpagerPosition);
                fragment.setProspectionHoleGroupPosition(prospectionHoleGroupPosition);
                updateFragment(fragment, R.id.prospection_hole_avg_list);
            }
        }

        ProspectionHoleGroupListFragment fragment = (ProspectionHoleGroupListFragment) getChildFragmentManager().findFragmentById(R.id.prospection_hole_group_list);

        if (fragment == null) {
            fragment = new ProspectionHoleGroupListFragment();
            fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule);
            fragment.setmProspectionHoleGroupDataset(mProspectionHoleGroupDataset);
            fragment.onAttachFragment(this);
            fragment.setProspectionHoleGroupPosition(prospectionHoleGroupPosition);
            updateFragment(fragment, R.id.prospection_hole_group_list);
        } else {
            fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule);
            fragment.setmProspectionHoleGroupDataset(mProspectionHoleGroupDataset);
            fragment.setProspectionHoleGroupPosition(prospectionHoleGroupPosition);
            fragment.updateItemList();

            ProspectionModulesFragment fragment2 = (ProspectionModulesFragment) getChildFragmentManager().findFragmentById(R.id.prospection_hole_avg_list);
            fragment2.setViewpagerPosition(viewpagerPosition);
            fragment2.setProspectionHoleGroupPosition(prospectionHoleGroupPosition);
            updateFragment(fragment2, R.id.prospection_hole_avg_list);
        }
    }

    @Override
    public void setQValuesMappings(HashMap<Mapping, QValue> qValuesByMappings) {
        mQValuesByMappings = qValuesByMappings;
        spinnerMappingQvalue = (Spinner) dialogContent.findViewById(R.id.spinner_mapping_qvalue);
        spinnerMappingQvalue2 = (Spinner) dialogContent.findViewById(R.id.spinner_mapping_qvalue1);

        List<String> labels = new ArrayList<>();

        if(mQValuesByMappings.isEmpty()) {
            noMappings = true;
            labels.add(getResources().getString(R.string.forecasting_label_prospection_hole_no_mappings));
        }
        else {
            noMappings = false;
            ChainageNumberFormat chainageNumberFormat = new ChainageNumberFormat();

            Iterator<Mapping> iter = mQValuesByMappings.keySet().iterator();
            Mapping m;
            QValue qvalue;

            while(iter.hasNext()) {
                m = iter.next();
                qvalue = mQValuesByMappings.get(m);

                if(qvalue ==  null) continue;

//                labels.add(chainageNumberFormat.format(m.getChainageStart()) + " - " + chainageNumberFormat.format(m.getChainageEnd()) + "  (Q: " + calculateQ(qvalue) + ")");
                labels.add(chainageNumberFormat.format(m.getChainageStart()) + " - " + chainageNumberFormat.format(m.getChainageEnd()));
            }

            spinnerMappingQvalue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    List<QValue> qValues = new ArrayList<QValue>(mQValuesByMappings.values());
                    int chainageItem = position;

                    QValue qValue = qValues.get(chainageItem);
                    String[] labelArray = new String[3];
                    labelArray[0] = "Q Min.: " + round(qValue.getqMin(), 4);
                    labelArray[1] = "Q Avg.: " + round(qValue.getqAvg(), 4);
                    labelArray[2] = "Q Max.: " + round(qValue.getqMax(), 4);

                    ArrayAdapter<String> a = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, labelArray);
                    spinnerMappingQvalue2.setAdapter(a);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                }
            });
        }

        String[] labelArray = labels.toArray(new String[labels.size()]);

        ArrayAdapter<String> a = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, labelArray);
        spinnerMappingQvalue.setAdapter(a);
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    public void syncDataResult(boolean success, String msg) {

        if(success) {
            mCallback.onSyncFinish();
        }
        else {
            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                public void run() {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getResources().getString(R.string.system_label_failure_data_sync))
                            .setPositiveButton(getResources().getString(R.string.system_label_button_retry), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    syncData();
                                }
                            })
                            .setNegativeButton(getResources().getString(R.string.system_label_button_skip), new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            });
        }
    }

    @Override
    public void onSyncProgressPercentaje(Integer percentaje) {
        uiProgressDialog.setProgress(percentaje);
    }

    @Override
    public void notifySyncSegment(String msg) {
        uiProgressDialog.setMessage(msg);
    }

    @Override
    public void onRodsByProspectionHoleRetrieved(HashMap<ProspectionHole, List<Rod>> rodsByProspectionHole) {
        mRodsByProspectionHole = rodsByProspectionHole;

        if(mForImg) {
            viewpagerPosition = 1;
            mForImg = false;
        }
        else {
            viewpagerPosition = 0;
        }

        ProspectionModulesFragment fragment = new ProspectionModulesFragment();
        fragment.setRodsByProspection(mRodsByProspectionHole);
        fragment.onAttachFragment(this);
        fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, mUser, selectedProspectionHoleGroup);
        fragment.setViewpagerPosition(viewpagerPosition);
        fragment.setProspectionHoleGroupPosition(prospectionHoleGroupPosition);
        updateFragment(fragment, R.id.prospection_hole_avg_list);

    }

    @Override
    public void onExportFinish(boolean success) {
        uiProgressDialog.dismiss();

        Handler h = new Handler(Looper.getMainLooper());
        h.post(new Runnable() {
            public void run() {
                new AlertDialog.Builder(getActivity())
                        .setTitle(getResources().getString(R.string.system_label_data_export_finished))
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .show();
            }
        });
    }

    @Override
    public void displayProspectionRefPictureList(List<ProspectionRefPicture> prospectionRefPictures) {
        mProspectionRefPictures = prospectionRefPictures;
        ProspectionModulesFragment fragment = (ProspectionModulesFragment) getChildFragmentManager().findFragmentById(R.id.prospection_hole_avg_list);

        if(fragment != null && selectedProspectionHoleGroup != null) {
            fragment.setmProspectionRefPictures(findRefPicturesByPhgId(selectedProspectionHoleGroup.getId()));
            updateFragment(fragment, R.id.prospection_hole_avg_list);
        }
    }

    @Override
    public void onCalculationsByProspectionHoleGroup(HashMap<ProspectionHoleGroup, List<ProspectionHoleGroupCalculation>> calculationsByGroup) {
        mCalculationsByGroup = calculationsByGroup;
    }

    private List<ProspectionRefPicture> findRefPicturesByPhgId(String prospectionHoleGroupId) {
        List<ProspectionRefPicture> prospectionRefPictures = new ArrayList<>();

        for(ProspectionRefPicture prospectionRefPicture : mProspectionRefPictures) {
            if(prospectionRefPicture.getIdProspectionHoleGroup().trim().equalsIgnoreCase(prospectionHoleGroupId.trim())) {
                prospectionRefPictures.add(prospectionRefPicture);
            }
        }

        return prospectionRefPictures;
    }

    public void syncData() {
        if(isNetworkAvailable()) {
            uiProgressDialog=new ProgressDialog(getActivity());
            uiProgressDialog.setMessage(getResources().getString(R.string.forecasting_label_prospection_sync_data));
            uiProgressDialog.setCancelable(false);
            uiProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            uiProgressDialog.setIndeterminate(false);
            uiProgressDialog.setProgress(0);
            uiProgressDialog.show();

            mPresenter.syncData(mUser);
        }
        else {
            final String alertTitle = getResources().getString(R.string.system_label_no_internet);
            final Context c = getActivity();
            final String msg1 = getResources().getString(R.string.system_label_no_internet_msg);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                public void run() {
                    new android.support.v7.app.AlertDialog.Builder(c)
                            .setTitle(alertTitle)
                            .setMessage(msg1)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            })
                            .show();
                }
            });
        }
    }

    public void exportData() {
        uiProgressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_Dark_Dialog);
        uiProgressDialog.setCancelable(false);
        uiProgressDialog.setMessage(getResources().getString(R.string.system_label_exporting_data1) + " " + getResources().getString(R.string.forecasting_module_name) + " " + getResources().getString(R.string.system_label_exporting_data2));
        uiProgressDialog.show();

        mPresenter.exportData(selectedProject, selectedTunnel, selectedFace, Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS).toString());
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void updateFragment(Fragment fragment, int container) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(container, fragment);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onProspectionHoleGroupSelected(int position, boolean forImg) {
        selectedProspectionHoleGroup = mProspectionHoleGroupDataset.get(position);
        prospectionHoleGroupPosition = position;
//        if(!forImg) viewpagerPosition = 0;
//        mForImg = forImg;
        mPresenter.getRodsByProspectionHoleMap(selectedProspectionHoleGroup.getId());
    }

    public void deleteSelectedProspectionHoleGroup() {
        mPresenter.deleteProspectionHoleGroup(selectedProspectionHoleGroup);
    }

    @Override
    public void goToForecasting() {
        mCallback.onProspectionHoleSelected(selectedProspectionHoleGroup);
    }

    @Override
    public void onProspectionPicturelListener(int prospectionHoleGroupPosition) {
        this.prospectionHoleGroupPosition = prospectionHoleGroupPosition;
        viewpagerPosition = 1;
        mForImg = true;
    }

    @Override
    public void onTakeRefPicture() {
        takePicture();
        mForImg = true;
    }

    @Override
    public void setRefPictures() {
        if(selectedProspectionHoleGroup != null) {
            ProspectionModulesFragment fragment = (ProspectionModulesFragment) getChildFragmentManager().findFragmentById(R.id.prospection_hole_avg_list);
            fragment.setmProspectionRefPictures(findRefPicturesByPhgId(selectedProspectionHoleGroup.getId()));
            updateFragment(fragment, R.id.prospection_hole_avg_list);
        }
    }

    private String pathAviaryDir = "/sdcard/DCIM/100AVIARY";

    @Override
    public void onEditRefPicture() {
        List<ProspectionRefPicture> prfList = findRefPicturesByPhgId(selectedProspectionHoleGroup.getId());
        prospectionRefPicture = prfList.get(0);
        File mFile = new File(prospectionRefPicture.getLocalUri().trim() + File.separator + prospectionRefPicture.getSideName().trim().toLowerCase() + ".jpg");

        Intent newIntent = new Intent("aviary.intent.action.EDIT");
        newIntent.setDataAndType(Uri.fromFile(mFile), "image/*"); // required
        newIntent.putExtra("app-id", getActivity().getPackageName());
        String[] tools = new String[]{"BRIGHTNESS", "CROP", "DRAW", "TEXT"};
        newIntent.putExtra("tools-list", tools);
        newIntent.putExtra("extra-api-key-secret", "cd9f4966112e789c");
        newIntent.putExtra("white-label", "");

        File mediaDirectory1 = new File(pathAviaryDir);
        FileManager fm = new FileManager();
        fm.clearDirectory(mediaDirectory1);

        mForImg = true;

        startActivityForResult(newIntent, EDIT_PICTURE_REQUEST_CODE);
    }

    @Override
    public void onUndoRefPicture() {
        List<ProspectionRefPicture> prfList = findRefPicturesByPhgId(selectedProspectionHoleGroup.getId());
        prospectionRefPicture = prfList.get(0);
        File mFile = new File(prospectionRefPicture.getLocalUri().trim() + File.separator + prospectionRefPicture.getSideName().trim().toLowerCase() + ".jpg");
        File mFileOriginal = new File(prospectionRefPicture.getLocalUri().trim() + File.separator + prospectionRefPicture.getSideName().trim().toLowerCase() + "-original.jpg");
        copyFile(mFileOriginal, mFile);

        ProspectionModulesFragment fragment = new ProspectionModulesFragment();
        fragment.setRodsByProspection(mRodsByProspectionHole);
        fragment.onAttachFragment(this);
        fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, mUser, selectedProspectionHoleGroup);
        fragment.setViewpagerPosition(1);
        fragment.setProspectionHoleGroupPosition(prospectionHoleGroupPosition);
        updateFragment(fragment, R.id.prospection_hole_avg_list);
    }

    @Override
    public void onCalculationsReady(List<Double> avgDistancesPerPosition, List<Double> avgQValuesPerPosition, List<Double> minQValuesPerPosition, List<Double> maxQValuesPerPosition, List<String> estimatedRockPerPosition) {
        List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations = new ArrayList<>();

        ProspectionHoleGroupCalculation prospectionHoleGroupCalculation;
        for(int i = 0; i < avgQValuesPerPosition.size(); i++) {
            prospectionHoleGroupCalculation = new ProspectionHoleGroupCalculation();
            prospectionHoleGroupCalculation.setIdProspectionHoleGroup(selectedProspectionHoleGroup.getId());
            prospectionHoleGroupCalculation.setRodPosition(i);
            prospectionHoleGroupCalculation.setProjectedDistance(avgDistancesPerPosition.get(i).floatValue());
            prospectionHoleGroupCalculation.setEstimatedRock(estimatedRockPerPosition.get(i));
            prospectionHoleGroupCalculation.setqMax(maxQValuesPerPosition.get(i).floatValue());
            prospectionHoleGroupCalculation.setqMin(minQValuesPerPosition.get(i).floatValue());
            prospectionHoleGroupCalculation.setqAvg(avgQValuesPerPosition.get(i).floatValue());

            if(!isNewCalculation(prospectionHoleGroupCalculation)) {
                prospectionHoleGroupCalculation.setId(getCalculationExistingId(prospectionHoleGroupCalculation));
            }

            prospectionHoleGroupCalculations.add(prospectionHoleGroupCalculation);
        }

        mPresenter.updateProspectionHoleGroupCalculations(prospectionHoleGroupCalculations, selectedProspectionHoleGroup.getId());
    }

    private boolean isNewCalculation(ProspectionHoleGroupCalculation prospectionHoleGroupCalculation) {
        List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations = getExistingCalculations();
        return (prospectionHoleGroupCalculation.getRodPosition()+1) > prospectionHoleGroupCalculations.size();
    }

    private String getCalculationExistingId(ProspectionHoleGroupCalculation prospectionHoleGroupCalculation) {
        List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations = getExistingCalculations();
        return prospectionHoleGroupCalculations.get(prospectionHoleGroupCalculation.getRodPosition().intValue()).getId();
    }

    private List<ProspectionHoleGroupCalculation> getExistingCalculations() {
        Iterator<ProspectionHoleGroup> iter = mCalculationsByGroup.keySet().iterator();
        ProspectionHoleGroup prospectionHoleGroup;
        List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations = new ArrayList<>();

        while(iter.hasNext()) {
            prospectionHoleGroup = iter.next();
            if(selectedProspectionHoleGroup.getId().trim().equalsIgnoreCase(prospectionHoleGroup.getId().trim())) {
                prospectionHoleGroupCalculations = mCalculationsByGroup.get(prospectionHoleGroup);
                break;
            }
        }

        return (prospectionHoleGroupCalculations == null ? new ArrayList<ProspectionHoleGroupCalculation>() : prospectionHoleGroupCalculations);
    }

    public void takePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            String fileName = "prospection_ref_image";

            try {
                photoFile = createImageFile(fileName.trim());
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.tunnelarts.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile(String fileName) throws IOException {
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                fileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageView mImageView = new ImageView(getActivity());

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == getActivity().RESULT_OK) {
            mImageView = getImageFromFile(photoFile, mImageView);
            refImageView = mImageView;
            refImageViewOriginal = refImageView;

            try {
                File mediaDirectory = new File(getActivity().getExternalFilesDir(null).getPath() + File.separator + "prospection" + File.separator + "media");

                if (!mediaDirectory.getParentFile().exists()) {
                    try{
                        mediaDirectory.getParentFile().mkdir();
                    }
                    catch(SecurityException se){
                        se.printStackTrace();
                    }
                }

                if (!mediaDirectory.exists()) {
                    try{
                        mediaDirectory.mkdir();
                    }
                    catch(SecurityException se){
                        se.printStackTrace();
                    }
                }

                List<ProspectionRefPicture> prfList = findRefPicturesByPhgId(selectedProspectionHoleGroup.getId());
                prospectionRefPicture = prfList.get(0);
                File mFile = new File(mediaDirectory.getPath() + File.separator + selectedProspectionHoleGroup.getId().toLowerCase() + File.separator + prospectionRefPicture.getSideName().trim().toLowerCase() + ".jpg");
                File mFileOriginal = new File(mediaDirectory.getPath() + File.separator + selectedProspectionHoleGroup.getId().toLowerCase() + File.separator + prospectionRefPicture.getSideName().trim().toLowerCase() + "-original.jpg");

                if (!mFile.getParentFile().exists()) {
                    try{
                        mFile.getParentFile().mkdir();
                    }
                    catch(SecurityException se){
                        se.printStackTrace();
                    }
                }

                File compressedFile;
                compressedFile = Compressor.getDefault(getContext()).compressToFile(photoFile);

                copyFile(compressedFile, mFile);
                copyFile(compressedFile, mFileOriginal);

//                copyFile(photoFile, mFile);
//                copyFile(photoFile, mFileOriginal);

                int pos = mProspectionRefPictures.indexOf(prospectionRefPicture);
                mProspectionRefPictures.get(pos).setLocalUri(mFile.getParent());
                mProspectionRefPictures.get(pos).setLocalUriOriginal(mFile.getParent());

                mPresenter.savePicture(mProspectionRefPictures.get(pos));

                ProspectionModulesFragment fragment = new ProspectionModulesFragment();
                fragment.setRodsByProspection(mRodsByProspectionHole);
                fragment.onAttachFragment(this);
                fragment.setSelectionContext(selectedProject, selectedTunnel, selectedFace, selectedModule, mUser, selectedProspectionHoleGroup);
                fragment.setViewpagerPosition(1);
                fragment.setProspectionHoleGroupPosition(prospectionHoleGroupPosition);
                updateFragment(fragment, R.id.prospection_hole_avg_list);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        if (resultCode == getActivity().RESULT_OK && requestCode == EDIT_PICTURE_REQUEST_CODE) {
            Uri editedImageUri = data.getData();
            mImageView.setImageURI(editedImageUri);
            Bitmap imageBitmap = ((BitmapDrawable)mImageView.getDrawable()).getBitmap();

            List<ProspectionRefPicture> prfList = findRefPicturesByPhgId(selectedProspectionHoleGroup.getId());
            prospectionRefPicture = prfList.get(0);
            File mFile = new File(prospectionRefPicture.getLocalUri().trim() + File.separator + prospectionRefPicture.getSideName().trim().toLowerCase() + ".jpg");

            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(mFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);

            /**
             * PATCH TEMPORAL
             */
            File mediaDirectory1 = new File(pathAviaryDir);

            FileManager fm = new FileManager();

            File[] image = mediaDirectory1.listFiles();
//            InputStream is = null;
//            try {
//                is = new FileInputStream(image[0]);
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            }
//            OutputStream out = null;
//            try {
//                out = new FileOutputStream(mFile);
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            }

            File compressedFile;
            compressedFile = Compressor.getDefault(getContext()).compressToFile(image[0]);
            copyFile(compressedFile, mFile);

//            try {
//                fm.copyFile(is, out);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            fm.clearDirectory(mediaDirectory1);
            /**
             * FIN PATCH TEMPORAL
             */


        }
    }

    private ImageView getImageFromFile(File imgFile, ImageView myImage) {
        if(imgFile != null) {
            if(imgFile.exists()){
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

                int reqWidth = dpToPx(250);
                int reqHeight = dpToPx(250);

                options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
                options.inJustDecodeBounds = false;
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

                myImage.setImageBitmap(myBitmap);
            }
        }
        return myImage;
    }

    private int dpToPx(int dp) {
        float density = getActivity().getApplicationContext().getResources().getDisplayMetrics().density;
        return Math.round((float)dp * density);
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    private void copyFile(File origin, File dest) {
        try {
            FileManager fm = new FileManager();
            InputStream is = new FileInputStream(origin);
            OutputStream out = new FileOutputStream(dest);
            fm.copyFile(is, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class ChainageNumberFormat extends DecimalFormat {

        public ChainageNumberFormat() {
            DecimalFormatSymbols custom = new DecimalFormatSymbols();
            custom.setGroupingSeparator('+');
            custom.setDecimalSeparator(',');
            setMinimumFractionDigits(2);
            setMaximumFractionDigits(2);

            setDecimalFormatSymbols(custom);
        }
    }
}
