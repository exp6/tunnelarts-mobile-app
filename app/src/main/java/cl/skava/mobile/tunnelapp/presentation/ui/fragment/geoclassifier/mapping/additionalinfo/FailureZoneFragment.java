package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.additionalinfo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.FailureZone;

/**
 * Created by Jose Ignacio Vera on 04-07-2016.
 */
public class FailureZoneFragment extends Fragment {

    private static final String TAG = "FailureZoneFragment";

    private FailureZone mappingInput;

    private Boolean mappingFinished;

    private List<String[]> rockTypes;
    private boolean instantiateFirst = false;

    private Spinner spinnerBlockSubGroup;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initRockTypeData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_additionalinfo_1, container, false);
        rootView.setTag(TAG);

        Spinner spinnerTypeFault = (Spinner) rootView.findViewById(R.id.spinner_sense_movement);
        EditText inputOrientationDD = ((EditText) rootView.findViewById(R.id.input_orientation_dd));
        EditText inputOrientationD = ((EditText) rootView.findViewById(R.id.input_orientation_d));
        EditText inputThickness = ((EditText) rootView.findViewById(R.id.input_thickness));
        EditText inputMatrixBlock = ((EditText) rootView.findViewById(R.id.input_matrix_Block));
        Spinner spinnerMatrixColourValue = (Spinner) rootView.findViewById(R.id.spinner_matrix_colour_value);
        Spinner spinnerMatrixColourChroma = (Spinner) rootView.findViewById(R.id.spinner_matrix_colour_chroma);
        Spinner spinnerMatrixColorHue = (Spinner) rootView.findViewById(R.id.spinner_matrix_colour_hue);
        Spinner spinnerMatrixGrainSize = (Spinner) rootView.findViewById(R.id.spinner_matrix_grain_size);
        Spinner spinnerBlockSize = (Spinner) rootView.findViewById(R.id.spinner_block_size);
        Spinner spinnerBlockShape = (Spinner) rootView.findViewById(R.id.spinner_block_shape);
        Spinner spinnerBlockGeneticGroup = (Spinner) rootView.findViewById(R.id.spinner_block_genetic_group);
        spinnerBlockSubGroup = (Spinner) rootView.findViewById(R.id.spinner_block_sub_group);
        final EditText inputRakeOfStriae = ((EditText) rootView.findViewById(R.id.input_rake_striae));

        spinnerTypeFault.setSelection(mappingInput.getSenseOfMovement());
        inputOrientationDD.setText(mappingInput.getOrientationDd().toString());
        inputOrientationD.setText(mappingInput.getOrientationD().toString());
        inputThickness.setText(mappingInput.getThickness().toString());
        inputMatrixBlock.setText(mappingInput.getMatrixBlock().toString());
        spinnerMatrixColourValue.setSelection(mappingInput.getMatrixColourValue());
        spinnerMatrixColourChroma.setSelection(mappingInput.getMatrixColourChroma());
        spinnerMatrixColorHue.setSelection(mappingInput.getMatrixColourHue());
        spinnerMatrixGrainSize.setSelection(mappingInput.getMatrixGainSize());
        spinnerBlockSize.setSelection(mappingInput.getBlockSize());
        spinnerBlockShape.setSelection(mappingInput.getBlockShape());
        spinnerBlockGeneticGroup.setSelection(mappingInput.getBlockGeneticGroup());
        setRockType(mappingInput.getBlockGeneticGroup());
        spinnerBlockSubGroup.setSelection(mappingInput.getBlockSubGroup());
        instantiateFirst = true;
        inputRakeOfStriae.setText(mappingInput.getRakeOfStriae().toString());

        CheckBox none = (CheckBox) rootView.findViewById(R.id.chk_none);
        none.setChecked(mappingInput.getNoneRake());

        none.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                inputRakeOfStriae.setText("");
                inputRakeOfStriae.setEnabled(!isChecked);
            }
        });

        inputRakeOfStriae.setEnabled(!none.isChecked());
        if(none.isChecked()) inputRakeOfStriae.setText("");

        if(mappingFinished) {
            spinnerTypeFault.setEnabled(false);
            inputOrientationDD.setEnabled(false);
            inputOrientationD.setEnabled(false);
            inputThickness.setEnabled(false);
            inputMatrixBlock.setEnabled(false);
            spinnerMatrixColourValue.setEnabled(false);
            spinnerMatrixColourChroma.setEnabled(false);
            spinnerMatrixColorHue.setEnabled(false);
            spinnerMatrixGrainSize.setEnabled(false);
            spinnerBlockSize.setEnabled(false);
            spinnerBlockShape.setEnabled(false);
            spinnerBlockGeneticGroup.setEnabled(false);
            spinnerBlockSubGroup.setEnabled(false);
            inputRakeOfStriae.setEnabled(false);
        }

        spinnerBlockGeneticGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (instantiateFirst) instantiateFirst = false;
                else setRockType(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        return rootView;
    }

    private void setRockType(int position) {
        ArrayAdapter<String> a = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, rockTypes.get(position));
        spinnerBlockSubGroup.setAdapter(a);
    }

    private void initRockTypeData() {
        rockTypes = new ArrayList<>(8);
        rockTypes.add(0, getResources().getStringArray(R.array.null_dropdown_arrays));
        rockTypes.add(1, getResources().getStringArray(R.array.na_dropdown_arrays));
        rockTypes.add(2, getResources().getStringArray(R.array.igneous_plutonic_dropdown_arrays));
        rockTypes.add(3, getResources().getStringArray(R.array.igneous_volcanic_dropdown_arrays));
        rockTypes.add(4, getResources().getStringArray(R.array.pyroclastic_dropdown_arrays));
        rockTypes.add(5, getResources().getStringArray(R.array.sedimentary_clastic_dropdown_arrays));
        rockTypes.add(6, getResources().getStringArray(R.array.sedimentary_chemical_dropdown_arrays));
        rockTypes.add(7, getResources().getStringArray(R.array.metamorphic_dropdown_arrays));
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMappingInput(FailureZone mappingInput) {
        this.mappingInput = mappingInput;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }
}
