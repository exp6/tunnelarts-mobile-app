package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.blobs;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudStorageService;
import cl.skava.mobile.tunnelapp.data.net.cloud.impl.azure.blobs.StorageClient;
import cl.skava.mobile.tunnelapp.domain.model.mapping.ExpandedTunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Gsi;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Particularities;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Picture;
import cl.skava.mobile.tunnelapp.domain.model.mapping.StereonetPicture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.repository.BaseRepository;

/**
 * Created by Jose Ignacio Vera on 01-09-2016.
 */
public class BlobStorageService implements CloudStorageService {

    private StorageClient provider;
    private final String STORAGE_CONNECTION_STRING =
            "DefaultEndpointsProtocol=http;"
                    + "AccountName=tunnelarts;"
                    + "AccountKey=IBPSBXbTCCLdvECvlEz/shJl/ABj91NX6onWKFnuOovApjCdawER8PgeTHtxbHFn58NSk0ZWaoeBIV5tofmGOg==";
    private File mExternalLocalDirectory;
    private final String MEDIA_DIRECTORY = "media";
    private BaseRepository mCallback;
    private int total;
    private int progress = 0;
    private Iterator<Mapping> iter;
    private Mapping selectedMapping;
    private File[] localFiles;
    private HashMap<Mapping, File[]> mFileListPerMapping;
    private HashMap<Mapping, List<Picture>> mPicturesPerMapping;
    private HashMap<Mapping, List<Picture>> picturesOriginalByMapping = new HashMap<>();
    private HashMap<Mapping, Particularities> mParticularitiesByMapping = new HashMap<>();
    private HashMap<Mapping, StereonetPicture> mStereonetsByMapping = new HashMap<>();

    private Boolean mSyncAll;
    private final String FILE_EXTENSION = ".jpg";
    private int mediaItemCode;
    private HashMap<Mapping, ExpandedTunnel> mExpandedByMapping;
    private HashMap<Mapping, Gsi> mGsiByMapping;
    private Integer moduleCode = 0;

    private ProspectionHoleGroup selectedProspectionHoleGroup;
    private Iterator<ProspectionHoleGroup> prospectionIter;

    private HashMap<ProspectionHoleGroup, File[]> mFilesPerProspectionHoleGroup;
    private HashMap<ProspectionHoleGroup, List<ProspectionRefPicture>> picturesByProspectionHoleGroup;
    private HashMap<ProspectionHoleGroup, List<ProspectionRefPicture>> picturesOriginalByProspectionHoleGroup;

    private final String PROSPECTION_DIRECTORY = "prospection";

    private final String lang = Locale.getDefault().getLanguage();

    private List<String> tunnelIds;
    private int listIndex;
    private int modelIndex;
    private final String FORECASTS_DIRECTORY = "forecasts";
    private final String FORECAST_CONTAINER_ID_PREFIX = "tunnel-prediction-";

    public BlobStorageService(File externalLocalDirectory, BaseRepository callback) {
        provider = new StorageClient(STORAGE_CONNECTION_STRING, this);
        mExternalLocalDirectory = externalLocalDirectory;
        mCallback = callback;
    }

    public BlobStorageService(File externalLocalDirectory) {
        provider = new StorageClient(STORAGE_CONNECTION_STRING, this);
        mExternalLocalDirectory = externalLocalDirectory;
    }

    @Override
    public void saveData(String referenceId, File sourceFile, String fileName) {
        provider.uploadData(referenceId, sourceFile, fileName);
    }

    @Override
    public void setUploadDataStatus(Boolean status, String msg) {
        if(status) {
            int mapSize;

            switch(moduleCode) {
                case 1:
                    //Pictures Prospection Hole Group
                    mapSize = picturesByProspectionHoleGroup.get(selectedProspectionHoleGroup).size();

                    if(progress >= mapSize) {
                        picturesByProspectionHoleGroup.get(selectedProspectionHoleGroup).get((progress-mapSize)).setRemoteUriOriginal(msg);
                    }
                    else {
                        picturesByProspectionHoleGroup.get(selectedProspectionHoleGroup).get(progress).setRemoteUri(msg);
                    }
                    break;
                default:
                    switch (mediaItemCode) {
                        case 1:
                            //Expanded Tunnel
                            mExpandedByMapping.get(selectedMapping).setRemoteUri(msg);
                            break;
                        case 2:
                            //Stereonet
                            mStereonetsByMapping.get(selectedMapping).setRemoteUri(msg);
                            break;
                        case 3:
                            mParticularitiesByMapping.get(selectedMapping).setOverbreakPictureRemoteUri(msg);
                            break;
                        default:
                            //Pictures
                            mapSize = mPicturesPerMapping.get(selectedMapping).size();

                            if(progress >= mapSize) {
                                mPicturesPerMapping.get(selectedMapping).get((progress-mapSize)).setRemoteUriOriginal(msg);
                            }
                            else {
                                mPicturesPerMapping.get(selectedMapping).get(progress).setRemoteUri(msg);
                            }
                            break;
                    }
                    break;
            }
            handleSyncStatus(1);
        }
    }

    @Override
    public void setDownloadDataStatus(Boolean status, String msg) {
        if(status) handleSyncStatus(2);
    }

    @Override
    public void setDataExistsStatus(Boolean status, String msg) {
        if (status) pullFile(msg);
        else handleSyncStatus(0);
    }

    private void handleSyncStatus(int syncCode) {
        ++progress;
        if(progress == total) {
            switch (syncCode) {
                case 1:
                    //Upload operation
                    progress = 0;
                    downloadFile();
                    break;
                default:
                    //Data check & Download operation
                    switch(moduleCode) {
                        case 1:
                            syncProspectionFileList();
                            break;
                        case 2:
                            downloadForecastModel();
                            break;
                        default:
                            syncNextMappingInputFileList();
                            break;
                    }
                    break;
            }
        }
        else {
            mCallback.setSyncProgress(getSyncPercentage(total, progress));
            switch (syncCode) {
                case 2:
                    //Download operation
                    downloadFile();
                    break;
                default:
                    //Data check & Upload operation
                    switch(moduleCode) {
                        case 1:
                            syncLocalFile(getProspectionItemName(), localFiles[progress]);
                            break;
                        default:
                            syncLocalFile(getItemName(), localFiles[progress]);
                            break;
                    }
                    break;
            }
        }
    }

    private void downloadFile() {
        String fileName;

        switch(moduleCode) {
            case 1:
                fileName = getProspectionItemName() + FILE_EXTENSION;
                break;
            case 2:
                modelIndex++;
                if(modelIndex > 50) {
                    modelIndex = 1;
                    progress = 0;
                    listIndex++;
                }
                fileName = "PMML_Modelo_Skava_" + modelIndex + ".pmml.ser";
                break;
            default:
                //            mCallback.notifySyncSegment("Synchronizing Characterization media records [Downloading]...");
                // TODO ver acá
                //fileName = getItemName() + FILE_EXTENSION;
                String itemName = getItemName();
                if (itemName.equals("expanded") || itemName.equals("expanded-export")) {
                    fileName = itemName + ".png";
                } else {
                    fileName = getItemName() + ".jpg";
                }
                break;
        }
        pullFile(fileName);
    }

    public File[] getLocalProspectionRefPictureFiles(String mappingId, List<ProspectionRefPicture> mappingPicturesData) {
        List<File> pictureFileList = new ArrayList<>();
        File sourceDirectory = new File(mExternalLocalDirectory + File.separator + PROSPECTION_DIRECTORY + File.separator + MEDIA_DIRECTORY + File.separator + mappingId.toLowerCase());

        if(!mappingPicturesData.isEmpty()) {
            for(int i = 0; i < mappingPicturesData.size(); i++) {
                ProspectionRefPicture p = mappingPicturesData.get(i);
                String fileName = p.getSideName().trim().toLowerCase() + FILE_EXTENSION;
                pictureFileList.add(new File(sourceDirectory, fileName));
            }

            for(int i = 0; i < mappingPicturesData.size(); i++) {
                ProspectionRefPicture p = mappingPicturesData.get(i);
                String fileName = p.getSideName().trim().toLowerCase() + "-original" + FILE_EXTENSION;
                pictureFileList.add(new File(sourceDirectory, fileName));
            }
        }
        File[] pictureFiles = pictureFileList.toArray(new File[pictureFileList.size()]);

        return pictureFiles;
    }

    public File[] getLocalPictureFiles(String mappingId, List<Picture> mappingPicturesData) {
//        File[] pictureFiles = new File[4];
        File[] pictureFilesOriginal = new File[4];
        List<File> pictureFileList = new ArrayList<>();
        File sourceDirectory = new File(mExternalLocalDirectory + File.separator + MEDIA_DIRECTORY + File.separator + mappingId.toLowerCase());

        if(!mappingPicturesData.isEmpty()) {
            for(int i = 0; i < mappingPicturesData.size(); i++) {
                Picture p = mappingPicturesData.get(i);
                String fileName = p.getSideName().trim().toLowerCase() + FILE_EXTENSION;
//                pictureFiles[i] = new File(sourceDirectory, fileName);
                pictureFileList.add(new File(sourceDirectory, fileName));
            }

            for(int i = 0; i < mappingPicturesData.size(); i++) {
                Picture p = mappingPicturesData.get(i);
                String fileName = p.getSideName().trim().toLowerCase() + "-original" + FILE_EXTENSION;
//                pictureFilesOriginal[i] = new File(sourceDirectory, fileName);
                pictureFileList.add(new File(sourceDirectory, fileName));
            }
        }
        File[] pictureFiles = pictureFileList.toArray(new File[pictureFileList.size()]);

        return pictureFiles;
    }

    public File getLocalExpandedFile(String mappingId, ExpandedTunnel mappingExpandedData, boolean export) {
        File sourceDirectory = new File(mExternalLocalDirectory + File.separator + MEDIA_DIRECTORY + File.separator + mappingId.toLowerCase());
        //String fileName = mappingExpandedData.getSideName().trim().toLowerCase() + FILE_EXTENSION;
        String fileName = mappingExpandedData.getSideName().trim().toLowerCase();
        if (export) {
            fileName += "-export";
        }
        fileName += ".png";
        return (new File(sourceDirectory, fileName));
    }

    public File getLocalExpandedFile(String mappingId, ExpandedTunnel mappingExpandedData) {
        return getLocalExpandedFile(mappingId, mappingExpandedData, false);
    }

    public File getLocalOverbreakFile(String mappingId) {
        File sourceDirectory = new File(mExternalLocalDirectory + File.separator + MEDIA_DIRECTORY + File.separator + mappingId.toLowerCase());
        return new File(sourceDirectory, "overbreak.jpg");
    }

    public File getLocalGsiFile(String mappingId, Gsi mappingGsiData) {
        File sourceDirectory = new File(mExternalLocalDirectory + File.separator + MEDIA_DIRECTORY + File.separator + mappingId.toLowerCase());
        String fileName = mappingGsiData.getSideName().trim().toLowerCase() + FILE_EXTENSION;
        return (new File(sourceDirectory, fileName));
    }

    public File getLocalStereonetFile(String mappingId) {
        File sourceDirectory = new File(mExternalLocalDirectory + File.separator + MEDIA_DIRECTORY + File.separator + mappingId.toLowerCase());
        return new File(sourceDirectory, "stereonet.jpg");
    }

    public void executeSyncProcess() {
        switch(moduleCode) {
            case 1:
                mCallback.notifySyncSegment(lang.equals("es") ? "Sincronizando Pronóstico registros gráficos..." : "Synchronizing Forecasting media records...");
                prospectionIter = mFilesPerProspectionHoleGroup.keySet().iterator();
                syncProspectionFileList();
                break;
            case 2:
                mCallback.notifySyncSegment(lang.equals("es") ? "Sincronizando Pronóstico modelos predictivos..." : "Synchronizing Forecasting prediction models...");
                listIndex = 0;
                progress = 0;
                modelIndex = 0;
                total = 50;
                downloadForecastModel();
                break;
            default:
                mCallback.notifySyncSegment(lang.equals("es") ? "Sincronizando Caracterización registros gráficos..." : "Synchronizing Characterization media records...");
                iter = mFileListPerMapping.keySet().iterator();
                syncNextMappingInputFileList();
                break;
        }
    }

    private Integer getSyncPercentage(Integer total, Integer progress) {
        Long result = Math.round(((double)progress * 100.0) / (double)total);
        return result.intValue();
    }

    private void downloadForecastModel() {
        int code = 9;

        if(listIndex < tunnelIds.size()-1) {
            downloadFile();
        }
        else {
            mCallback.onSyncStorageResult(code, mSyncAll);
        }
    }

    private void syncProspectionFileList() {
        int code = 8;

        if(prospectionIter.hasNext()) {
            progress = 0;
            selectedProspectionHoleGroup = prospectionIter.next();
            String itemName = getProspectionItemName();
            localFiles = mFilesPerProspectionHoleGroup.get(selectedProspectionHoleGroup);
            total = localFiles.length;
            syncLocalFile(itemName, localFiles[progress]);
        }
        else {
            mCallback.onSyncStorageResult(code, mSyncAll);
        }
    }

    private void syncNextMappingInputFileList() {
        int code;

        switch (mediaItemCode) {
            case 1:
                //Expanded Tunnel
                mCallback.notifySyncSegment(lang.equals("es") ? "Sincronizando Caracterización registros gráficos [Túnel Expandido]..." : "Synchronizing Characterization media records [Expanded Tunnel]...");
                code = 3;
                break;
            case 2:
                //Stereonet
                mCallback.notifySyncSegment(lang.equals("es") ? "Sincronizando Caracterización registros gráficos [Red Estereográfica]..." : "Synchronizing Characterization media records [Stereonet]...");
                code = 6;
                break;
            case 3:
                mCallback.notifySyncSegment(lang.equals("es") ? "Sincronizando Caracterización registros gráficos [Sobre Excavación]..." : "Synchronizing Characterization media records [Overbreak]...");
                code = 11;
                break;
            default:
                //Pictures
                mCallback.notifySyncSegment(lang.equals("es") ? "Sincronizando Caracterización registros gráficos [Fotos]..." : "Synchronizing Characterization media records [Pictures]...");
                code = 1;
                break;
        }

        if(iter.hasNext()) {
            progress = 0;
            selectedMapping = iter.next();
            String itemName = (mediaItemCode == 3)? "overbreak" : getItemName();
            localFiles = mFileListPerMapping.get(selectedMapping);
            total = localFiles.length;
            syncLocalFile(itemName, localFiles[progress]);
        }
        else {
            mCallback.onSyncStorageResult(code, mSyncAll);
        }
    }

    private String getProspectionItemName() {
        //Prospection Pictures
        ProspectionRefPicture p;
        String result = "";

        int mapSize = picturesByProspectionHoleGroup.get(selectedProspectionHoleGroup).size();
        if(progress >= mapSize) {
            p = picturesOriginalByProspectionHoleGroup.get(selectedProspectionHoleGroup).get((progress-mapSize));
            result = p.getSideName().trim().toLowerCase()+"-original";
        }
        else {
            p = picturesByProspectionHoleGroup.get(selectedProspectionHoleGroup).get(progress);
            result = p.getSideName().trim().toLowerCase();
        }

        return result;
    }

    private String getItemName() {
        switch (mediaItemCode) {
            case 1:
                //Expanded Tunnel
                ExpandedTunnel expandedTunnel = mExpandedByMapping.get(selectedMapping);
                String expandedTunnelName = expandedTunnel.getSideName().trim().toLowerCase();
                if (progress == 1) {
                    return expandedTunnelName + "-export";
                }
                return expandedTunnelName;
//            case 2:
//                //GSI
//                Gsi gsi = mGsiByMapping.get(selectedMapping);
//                return gsi.getSideName().trim().toLowerCase();
            case 2:
                //Stereonet
                StereonetPicture stereonetPicture = mStereonetsByMapping.get(selectedMapping);
                return stereonetPicture.getSideName().trim().toLowerCase();
            default:
                //Pictures
                Picture p;
                String result = "";

                int mapSize = mPicturesPerMapping.get(selectedMapping).size();
                if(progress >= mapSize) {
                    p = picturesOriginalByMapping.get(selectedMapping).get((progress-mapSize));
                    result = p.getSideName().trim().toLowerCase()+"-original";
                }
                else {
                    p = mPicturesPerMapping.get(selectedMapping).get(progress);
                    result = p.getSideName().trim().toLowerCase();
                }

                return result;
        }
    }

    private void check4FileUploaded(String filename, String extension) {
        switch(moduleCode) {
            case 1:
                provider.blobExists(PROSPECTION_DIRECTORY + "-" + selectedProspectionHoleGroup.getId().toLowerCase(), filename + extension);
                break;
            default:
                // TODO ver acá
                //provider.blobExists(selectedMapping.getId().toLowerCase(), filename + FILE_EXTENSION);
                provider.blobExists(selectedMapping.getId().toLowerCase(), filename + extension);
                break;
        }
    }

    private void check4FileUploaded(String filename) {
        check4FileUploaded(filename, ".jpg");
    }

    private void syncLocalFile(String filename, File localFile) {
        if(localFile == null || !localFile.exists()) {
            if (filename.equals("expanded") || filename.equals("expanded-export")) {
                check4FileUploaded(filename, ".png");
                //check4FileUploaded(filename+"-export", ".png");
            } else {
                check4FileUploaded(filename);
            }
        }
        else pushFile(localFile);
    }

    private void pushFile(File localFile) {
        switch(moduleCode) {
            case 1:
                saveData(PROSPECTION_DIRECTORY + "-" + selectedProspectionHoleGroup.getId().toLowerCase(), localFile, localFile.getName());
                break;
            default:
                //        mCallback.notifySyncSegment("Synchronizing Characterization media records [Uploading]...");
                saveData(selectedMapping.getId().toLowerCase(), localFile, localFile.getName());
                break;
        }
    }

    private void pullFile(String localFileName) {
        File destinationFileDir;

        switch(moduleCode) {
            case 1:
                destinationFileDir = new File(mExternalLocalDirectory + File.separator + PROSPECTION_DIRECTORY + File.separator + MEDIA_DIRECTORY + File.separator + selectedProspectionHoleGroup.getId().toLowerCase());
                if (!destinationFileDir.getParentFile().getParentFile().exists()) createFileDir(destinationFileDir.getParentFile().getParentFile());
                if (!destinationFileDir.getParentFile().exists()) createFileDir(destinationFileDir.getParentFile());
                if (!destinationFileDir.exists()) createFileDir(destinationFileDir);
                provider.downloadData(PROSPECTION_DIRECTORY + "-" + selectedProspectionHoleGroup.getId().toLowerCase(), destinationFileDir, localFileName);
                break;
            case 2:
                destinationFileDir = new File(mExternalLocalDirectory + File.separator + PROSPECTION_DIRECTORY + File.separator + FORECASTS_DIRECTORY + File.separator + tunnelIds.get(listIndex).toLowerCase());
                if (!destinationFileDir.getParentFile().getParentFile().exists()) createFileDir(destinationFileDir.getParentFile().getParentFile());
                if (!destinationFileDir.getParentFile().exists()) createFileDir(destinationFileDir.getParentFile());
                if (!destinationFileDir.exists()) createFileDir(destinationFileDir);
                provider.downloadData(FORECAST_CONTAINER_ID_PREFIX + tunnelIds.get(listIndex).toLowerCase(), destinationFileDir, localFileName);
                break;
            default:
                destinationFileDir = new File(mExternalLocalDirectory + File.separator + MEDIA_DIRECTORY + File.separator + selectedMapping.getId().toLowerCase());
                if (!destinationFileDir.getParentFile().exists()) createFileDir(destinationFileDir.getParentFile());
                if (!destinationFileDir.exists()) createFileDir(destinationFileDir);
                provider.downloadData(selectedMapping.getId().toLowerCase(), destinationFileDir, localFileName);
                break;
        }
    }

    private void createFileDir(File directory) {
        try{
            directory.mkdir();
        }
        catch(SecurityException se){
            //handle it
        }
    }

    public void setModuleCode(Integer moduleCode) {
        this.moduleCode = moduleCode;
    }

    public void setmFileListPerMapping(HashMap<Mapping, File[]> mFileListPerMapping) {
        this.mFileListPerMapping = mFileListPerMapping;
    }

    public void setmPicturesPerMapping(HashMap<Mapping, List<Picture>> mPicturesPerMapping) {
        this.mPicturesPerMapping = mPicturesPerMapping;
    }

    public HashMap<Mapping, List<Picture>> getmPicturesPerMapping() {
        return mPicturesPerMapping;
    }

    public void setmSyncAll(Boolean mSyncAll) {
        this.mSyncAll = mSyncAll;
    }

    public void setMediaItemCode(int mediaItemCode) {
        this.mediaItemCode = mediaItemCode;
    }

    public HashMap<Mapping, ExpandedTunnel> getmExpandedByMapping() {
        return mExpandedByMapping;
    }

    public void setmExpandedByMapping(HashMap<Mapping, ExpandedTunnel> mExpandedByMapping) {
        this.mExpandedByMapping = mExpandedByMapping;
    }

    public HashMap<Mapping, Gsi> getmGsiByMapping() {
        return mGsiByMapping;
    }

    public void setmGsiByMapping(HashMap<Mapping, Gsi> mGsiByMapping) {
        this.mGsiByMapping = mGsiByMapping;
    }

    public void setPicturesOriginalByMapping(HashMap<Mapping, List<Picture>> picturesOriginalByMapping) {
        this.picturesOriginalByMapping = picturesOriginalByMapping;
    }

    public void setmFilesPerProspectionHoleGroup(HashMap<ProspectionHoleGroup, File[]> mFilesPerProspectionHoleGroup) {
        this.mFilesPerProspectionHoleGroup = mFilesPerProspectionHoleGroup;
    }

    public void setPicturesByProspectionHoleGroup(HashMap<ProspectionHoleGroup, List<ProspectionRefPicture>> picturesByProspectionHoleGroup) {
        this.picturesByProspectionHoleGroup = picturesByProspectionHoleGroup;
    }

    public void setPicturesOriginalByProspectionHoleGroup(HashMap<ProspectionHoleGroup, List<ProspectionRefPicture>> picturesOriginalByProspectionHoleGroup) {
        this.picturesOriginalByProspectionHoleGroup = picturesOriginalByProspectionHoleGroup;
    }

    public HashMap<ProspectionHoleGroup, List<ProspectionRefPicture>> getPicturesByProspectionHoleGroup() {
        return picturesByProspectionHoleGroup;
    }

    public void setTunnelIds(List<String> tunnelIds) {
        this.tunnelIds = tunnelIds;
    }

    public void setmParticularitiesByMapping(HashMap<Mapping, Particularities> mParticularitiesByMapping) {
        this.mParticularitiesByMapping = mParticularitiesByMapping;
    }

    public void setmStereonetsByMapping(HashMap<Mapping, StereonetPicture> mStereonetsByMapping) {
        this.mStereonetsByMapping = mStereonetsByMapping;
    }

    public HashMap<Mapping, StereonetPicture> getmStereonetsByMapping() {
        return mStereonetsByMapping;
    }
}
