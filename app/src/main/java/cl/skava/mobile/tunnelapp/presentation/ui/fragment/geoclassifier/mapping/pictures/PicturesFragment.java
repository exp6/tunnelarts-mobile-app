package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.pictures;

import android.Manifest;
import android.animation.Animator;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


//import com.adobe.creativesdk.aviary.AdobeImageIntent;
//import com.adobe.creativesdk.foundation.AdobeCSDKFoundation;
//import com.adobe.creativesdk.foundation.auth.IAdobeAuthClientCredentials;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.NoSuchElementException;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.data.cache.FileManager;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Picture;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.mapping.pictures.PicturesAdapter;
import id.zelory.compressor.Compressor;

/**
 * Created by Jose Ignacio Vera on 01-07-2016.
 */
public class PicturesFragment extends Fragment
{

    private static final String TAG = "PicturesFragment";
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private int selectedPicPos;
    private ImageView[] imgs;
    private Uri[] pictureURIs;
    private File[] mFiles;
    private RecyclerView recyclerView;

    public int EDIT_PICTURE_REQUEST_CODE = 2;

    private Animator mCurrentAnimator;
    private int mShortAnimationDuration;

    private View rootView;

    private Boolean mappingFinished;

    private List<Picture> mappingPictures;

    private ImageView[] imgsOriginal;
    private Uri[] pictureURIsOriginal;
    private File[] mFilesOriginal;

    private Uri photoURI;

    private File photoFile;

    private String pathAviaryDir = "/sdcard/DCIM/100AVIARY";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_pictures, container, false);
        rootView.setTag(TAG);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);

        imgs = new ImageView[mFiles.length];
        pictureURIs = new Uri[mFiles.length];

        imgsOriginal = new ImageView[mFiles.length];
        pictureURIsOriginal = new Uri[mFiles.length];

        initImgsOriginal();

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new PicturesAdapter(this, imgs, mFiles, mappingFinished));

        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        return rootView;
    }

    private void initImgsOriginal() {
        for(int i = 0; i < mFilesOriginal.length; i++) {
            imgsOriginal[i] = getImageFromFile(mFilesOriginal[i], new ImageView(getActivity()));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void takePicture(int location) {
        Log.i(TAG, "TAKE " + location + " PICTURE!!");
        selectedPicPos = location;
        dispatchTakePictureIntent();
    }

    public void editPicture(int location) {
        Log.i(TAG, "EDIT " + location + " PICTURE!!");
        selectedPicPos = location;
        dispatchEditPictureIntent();
    }

    public void undoPicture(int location) {
        Log.i(TAG, "UNDO " + location + " PICTURE!!");
        selectedPicPos = location;
        Handler h = new Handler(Looper.getMainLooper());
        h.post(new Runnable() {
            public void run() {
                new android.support.v7.app.AlertDialog.Builder(getActivity())
                        .setTitle(getResources().getString(R.string.characterization_mapping_input_pic_label_switch_original))
                        .setMessage(getResources().getString(R.string.characterization_mapping_input_pic_label_switch_original_msg) + getResources().getString(R.string.system_label_proceed))
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(getResources().getString(R.string.system_label_yes), new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                undoEdit();
                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                            }
                        })
                        .show();
            }
        });
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            String fileName = mappingPictures.get(selectedPicPos).getSideName();

            try {
                photoFile = createImageFile(fileName.trim());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.tunnelarts.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private void dispatchEditPictureIntent() {
        Intent newIntent = new Intent("aviary.intent.action.EDIT");
        newIntent.setDataAndType(Uri.fromFile(mFiles[selectedPicPos]), "image/*"); // required
        newIntent.putExtra("app-id", getActivity().getPackageName());
        String[] tools = new String[]{"BRIGHTNESS", "CROP", "DRAW", "TEXT"};
        newIntent.putExtra("tools-list", tools);
        newIntent.putExtra("extra-api-key-secret", "cd9f4966112e789c");
        newIntent.putExtra("white-label", "");

        File mediaDirectory1 = new File(pathAviaryDir);
        FileManager fm = new FileManager();
        fm.clearDirectory(mediaDirectory1);

        startActivityForResult(newIntent, EDIT_PICTURE_REQUEST_CODE);

    }

    private void undoEdit() {
        switchToOriginal();
        updateDataset();
    }

    private void switchToOriginal() {
        copyFile(mFilesOriginal[selectedPicPos], mFiles[selectedPicPos]);
        imgs[selectedPicPos] = imgsOriginal[selectedPicPos];
    }

    private void copyFile(File origin, File dest) {
        try {
            FileManager fm = new FileManager();
            InputStream is = new FileInputStream(origin);
            OutputStream out = new FileOutputStream(dest);
            fm.copyFile(is, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File createImageFile(String fileName) throws IOException {
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                fileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageView mImageView = new ImageView(getActivity());

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == getActivity().RESULT_OK) {
            mImageView = getImageFromFile(photoFile, mImageView);
            imgs[selectedPicPos] = mImageView;
            imgsOriginal[selectedPicPos] = imgs[selectedPicPos];

            try {
                Picture p = mappingPictures.get(selectedPicPos);
                File mediaDirectory = new File(getActivity().getExternalFilesDir(null).getPath() + File.separator + "media");
                if (!mediaDirectory.exists()) {
                    try{
                        mediaDirectory.mkdir();
                    }
                    catch(SecurityException se){
                        se.printStackTrace();
                    }
                }

                File mFile = new File(mediaDirectory.getPath() + File.separator + p.getIdMapping().toLowerCase() + File.separator + p.getSideName().trim().toLowerCase() + ".jpg");
                File mFileOriginal = new File(mediaDirectory.getPath() + File.separator + p.getIdMapping().toLowerCase() + File.separator + p.getSideName().trim().toLowerCase() + "-original.jpg");

                if (!mFile.getParentFile().exists()) {
                    try{
                        mFile.getParentFile().mkdir();
                    }
                    catch(SecurityException se){
                        se.printStackTrace();
                    }
                }

                File compressedFile;
                compressedFile = Compressor.getDefault(getContext()).compressToFile(photoFile);

                copyFile(compressedFile, mFile);
                copyFile(compressedFile, mFileOriginal);

//                copyFile(photoFile, mFile);
//                copyFile(photoFile, mFileOriginal);

                mFiles[selectedPicPos] = mFile;
                mFilesOriginal[selectedPicPos] = mFileOriginal;
                mappingPictures.get(selectedPicPos).setLocalUri(mFile.getParent());
                mappingPictures.get(selectedPicPos).setLocalUriOriginal(mFileOriginal.getParent());
            } catch (Exception e) {
                e.printStackTrace();
            }

            updateDataset();
        }

        if (resultCode == getActivity().RESULT_OK && requestCode == EDIT_PICTURE_REQUEST_CODE) {
            Uri editedImageUri = data.getData();
            mImageView.setImageURI(editedImageUri);
            Bitmap imageBitmap = ((BitmapDrawable)mImageView.getDrawable()).getBitmap();
            ImageView mImageViewPrev = scaleImage(mImageView);
            mImageView = mImageViewPrev;
            imgs[selectedPicPos] = mImageView;
            pictureURIs[selectedPicPos] = editedImageUri;

            try {
                Picture p = mappingPictures.get(selectedPicPos);
                File mediaDirectory = new File(getActivity().getExternalFilesDir(null).getPath() + File.separator + "media");
                if (!mediaDirectory.exists()) {
                    try{
                        mediaDirectory.mkdir();
                    }
                    catch(SecurityException se){
                        se.printStackTrace();
                    }
                }

                File mFile = new File(mediaDirectory.getPath() + File.separator + p.getIdMapping().toLowerCase() + File.separator + p.getSideName().trim().toLowerCase() + ".jpg");

                if (!mFile.getParentFile().exists()) {
                    try{
                        mFile.getParentFile().mkdir();
                    }
                    catch(SecurityException se){
                        se.printStackTrace();
                    }
                }

                FileOutputStream fos = new FileOutputStream(mFile);
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);

                /**
                 * PATCH TEMPORAL
                 */

                File mediaDirectory1 = new File(pathAviaryDir);

                FileManager fm = new FileManager();

                File[] image = mediaDirectory1.listFiles();
//                InputStream is = new FileInputStream(image[0]);

                File compressedFile;
                compressedFile = Compressor.getDefault(getContext()).compressToFile(image[0]);
                copyFile(compressedFile, mFile);

//                OutputStream out = new FileOutputStream(mFile);
//
//                fm.copyFile(is, out);
                fm.clearDirectory(mediaDirectory1);
                /**
                 * FIN PATCH TEMPORAL
                 */

                mFiles[selectedPicPos] = mFile;
                mappingPictures.get(selectedPicPos).setLocalUri(mFile.getParent());
//                mFiles[selectedPicPos] = mFile;
            } catch (Exception e) {
                e.printStackTrace();
            }
            updateDataset();
        }
    }

    private void updateDataset() {
        recyclerView.setAdapter(new PicturesAdapter(this, imgs, mFiles, mappingFinished));
    }

    private ImageView scaleImage(ImageView view) throws NoSuchElementException {
        // Get bitmap from the the ImageView.
        Bitmap bitmap = null;

        try {
            Drawable drawing = view.getDrawable();
            bitmap = ((BitmapDrawable) drawing).getBitmap();
        } catch (NullPointerException e) {
            throw new NoSuchElementException("No drawable on given view");
        }

        // Get current dimensions AND the desired bounding box
        int width = 0;

        try {
            width = bitmap.getWidth();
        } catch (NullPointerException e) {
            throw new NoSuchElementException("Can't find bitmap on given view/drawable");
        }

        int height = bitmap.getHeight();
        int bounding = dpToPx(250);

        // Determine how much to scale: the dimension requiring less scaling is
        // closer to the its side. This way the image always stays inside your
        // bounding box AND either x/y axis touches it.
        float xScale = ((float) bounding) / width;
        float yScale = ((float) bounding) / height;
        float scale = (xScale <= yScale) ? xScale : yScale;

        // Create a matrix for the scaling and add the scaling data
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);

        // Create a new bitmap and convert it to a format understood by the ImageView
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        width = scaledBitmap.getWidth(); // re-use
        height = scaledBitmap.getHeight(); // re-use
        BitmapDrawable result = new BitmapDrawable(scaledBitmap);

        // Apply the scaled bitmap
        view.setImageDrawable(result);

        return view;
    }

    private int dpToPx(int dp) {
        float density = getActivity().getApplicationContext().getResources().getDisplayMetrics().density;
        return Math.round((float)dp * density);
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    public File[] getmFiles() {
        return mFiles;
    }

    public void setmFiles(File[] mFiles) {
        this.mFiles = mFiles;
    }

    public List<Picture> getMappingPictures() {
        return mappingPictures;
    }

    public void setMappingPictures(List<Picture> mappingPictures) {
        this.mappingPictures = mappingPictures;
    }

    public File[] getmFilesOriginal() {
        return mFilesOriginal;
    }

    public void setmFilesOriginal(File[] mFilesOriginal) {
        this.mFilesOriginal = mFilesOriginal;
    }

    private ImageView getImageFromFile(File imgFile, ImageView myImage) {
        if(imgFile != null) {
            if(imgFile.exists()){
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

                int reqWidth = dpToPx(250);
                int reqHeight = dpToPx(250);

                options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
                options.inJustDecodeBounds = false;
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

                myImage.setImageBitmap(myBitmap);
            }
        }
        return myImage;
    }

    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
