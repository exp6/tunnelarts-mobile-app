package cl.skava.mobile.tunnelapp.threading;

import android.os.Handler;
import android.os.Looper;

import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;

/**
 * Created by Jose Ignacio Vera on 15-06-2016.
 *
 * This class makes sure that the runnable we provide will be executeModel on the main UI thread.
 */
public class MainThreadImpl implements MainThread {

    private static MainThread sMainThread;

    private Handler mHandler;

    private MainThreadImpl() {
        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void post(Runnable runnable) {
        mHandler.post(runnable);
    }

    public static MainThread getInstance() {
        if (sMainThread == null) {
            sMainThread = new MainThreadImpl();
        }
        return sMainThread;
    }
}
