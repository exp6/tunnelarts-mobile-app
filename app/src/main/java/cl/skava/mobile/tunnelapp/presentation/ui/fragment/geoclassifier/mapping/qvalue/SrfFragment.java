package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.qvalue;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.EvaluationMethodSelectionType;

/**
 * Created by Jose Ignacio Vera on 05-07-2016.
 */
public class SrfFragment extends Fragment {

    private static final String TAG = "SrfFragment";

    private List<SrfGroup> srfGroups = new ArrayList<>();

    private OnSrfValueListener mCallback;

    public interface OnSrfValueListener {
        void OnSrfSetValue(Double value, String id);
    }

    private boolean isValid;

    private EvaluationMethodSelectionType selectionMap;

    private Boolean mappingFinished;

    private final String lang = Locale.getDefault().getLanguage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initDataset();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_qvalue_srf, container, false);
        rootView.setTag(TAG);

        RadioGroup group = (RadioGroup) rootView.findViewById(R.id.radio_group1);
        RadioButton button;

        final RadioGroup group2 = (RadioGroup) rootView.findViewById(R.id.radio_group2);

        final TableLayout tbl1 = (TableLayout) rootView.findViewById(R.id.table1);
        final TableLayout tbl2 = (TableLayout) rootView.findViewById(R.id.table2);
        final TableLayout tbl22 = (TableLayout) rootView.findViewById(R.id.table22);
        final TableLayout tbl3 = (TableLayout) rootView.findViewById(R.id.table3);
        final TableLayout tbl33 = (TableLayout) rootView.findViewById(R.id.table33);

        tbl1.setVisibility(View.GONE);
        tbl2.setVisibility(View.GONE);
        tbl22.setVisibility(View.GONE);
        tbl3.setVisibility(View.GONE);
        tbl33.setVisibility(View.GONE);

        int i = 0;
        for(SrfGroup srfGroup : srfGroups) {
            final SrfGroup srfGroup1 = srfGroup;
            button = new RadioButton(getActivity());
            button.setId(i);
            button.setText(srfGroup.getIndex().trim() + ". " + srfGroup.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    displaySrfs(group2, srfGroup1);

                    if(srfGroup1.getIndex().trim().equalsIgnoreCase("a")) {
                        tbl1.setVisibility(View.VISIBLE);
                        tbl2.setVisibility(View.GONE);
                        tbl22.setVisibility(View.GONE);
                        tbl3.setVisibility(View.GONE);
                        tbl33.setVisibility(View.GONE);
                    }

                    if(srfGroup1.getIndex().trim().equalsIgnoreCase("b")) {
                        tbl1.setVisibility(View.GONE);
                        tbl2.setVisibility(View.VISIBLE);
                        tbl22.setVisibility(View.VISIBLE);
                        tbl3.setVisibility(View.GONE);
                        tbl33.setVisibility(View.GONE);
                    }

                    if(srfGroup1.getIndex().trim().equalsIgnoreCase("c")) {
                        tbl1.setVisibility(View.GONE);
                        tbl2.setVisibility(View.GONE);
                        tbl22.setVisibility(View.GONE);
                        tbl3.setVisibility(View.VISIBLE);
                        tbl33.setVisibility(View.VISIBLE);
                    }

                    if(srfGroup1.getIndex().trim().equalsIgnoreCase("d")) {
                        tbl1.setVisibility(View.GONE);
                        tbl2.setVisibility(View.GONE);
                        tbl22.setVisibility(View.GONE);
                        tbl3.setVisibility(View.GONE);
                        tbl33.setVisibility(View.GONE);
                    }
                }
            });
            button.setChecked(srfGroup.isChecked());
            if(srfGroup.isChecked()) {
                displaySrfs(group2, srfGroup1);

                if(srfGroup1.getIndex().trim().equalsIgnoreCase("a")) {
                    tbl1.setVisibility(View.VISIBLE);
                    tbl2.setVisibility(View.GONE);
                    tbl22.setVisibility(View.GONE);
                    tbl3.setVisibility(View.GONE);
                    tbl33.setVisibility(View.GONE);
                }

                if(srfGroup1.getIndex().trim().equalsIgnoreCase("b")) {
                    tbl1.setVisibility(View.GONE);
                    tbl2.setVisibility(View.VISIBLE);
                    tbl22.setVisibility(View.VISIBLE);
                    tbl3.setVisibility(View.GONE);
                    tbl33.setVisibility(View.GONE);
                }

                if(srfGroup1.getIndex().trim().equalsIgnoreCase("c")) {
                    tbl1.setVisibility(View.GONE);
                    tbl2.setVisibility(View.GONE);
                    tbl22.setVisibility(View.GONE);
                    tbl3.setVisibility(View.VISIBLE);
                    tbl33.setVisibility(View.VISIBLE);
                }

                if(srfGroup1.getIndex().trim().equalsIgnoreCase("d")) {
                    tbl1.setVisibility(View.GONE);
                    tbl2.setVisibility(View.GONE);
                    tbl22.setVisibility(View.GONE);
                    tbl3.setVisibility(View.GONE);
                    tbl33.setVisibility(View.GONE);
                }
            }

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }

        return rootView;
    }

    private void displaySrfs(RadioGroup group, SrfGroup srfGroup) {
        List<Srf> srfs = srfGroup.getSrfs();
        final SrfGroup srfGroup1 = srfGroup;
        group.removeAllViews();
        RadioButton button;

        int i = 0;
        for(Srf srf :  srfs) {
            final Srf srf1 = srf;

            button = new RadioButton(getActivity());
            button.setId(i);
            button.setText(srf.getIndex().trim() + " (" + srf.getStart() + (srf.getEnd() != null ? " - " + srf.getEnd() : "") + ")     " + srf.getDescription());
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (srf1.getEnd() != null) {
                        execTextDialog(getResources().getString(R.string.characterization_mapping_input_qmet_label_range1)
                                        + " " + srf1.getIndex(), getResources().getString(R.string.characterization_mapping_input_qmet_label_range2) +" "
                                + srf1.getStart() + " " + getResources().getString(R.string.characterization_mapping_input_qmet_label_range3) + " "
                                + srf1.getEnd(), srf1.getStart(), srf1.getEnd(), srf1.getId(), srf1
                        , (srf1.isChecked() ? enteredValue : (enteredValue >= srf1.getStart() && enteredValue <= srf1.getEnd() ? enteredValue : srf1.getStart())), srfGroup1);
                    } else {
                        mCallback.OnSrfSetValue(srf1.getStart(), srf1.getId());
                        updateMap(srfGroup1, srf1);
                    }
                }
            });
            button.setChecked(srf1.isChecked());

            if(mappingFinished) {
                button.setEnabled(false);
            }

            group.addView(button);
            i++;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void updateMap(SrfGroup srfGroup, Srf srf) {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        EvaluationMethodSelection input;
        for(int i = 0; i < groups.size(); i++) {
            groups.get(i).setChecked(false);
            if(srfGroup.getId().equalsIgnoreCase(groups.get(i).getId())) {
                groups.get(i).setChecked(true);
            }

            inputs = groups.get(i).getSubSelections();
            for(int j = 0; j < inputs.size(); j++) {
                input = inputs.get(j);
                input.setChecked(false);
                if(srf.getId().equalsIgnoreCase(input.getId())) {
                    input.setChecked(true);
                }
            }

            groups.get(i).setSubSelections(inputs);
        }
        selectionMap.setSelectionGroups(groups);
    }

    private Double enteredValue = null;

    public void execTextDialog(String title, String msg, Double start, Double end, String id, Srf srf, Double valueSelected, SrfGroup srfGroup) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this.getContext());
        alert.setTitle(title);
        alert.setMessage(msg);
        // Create TextView
        final EditText input = new EditText(this.getContext());
        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        final Double startValue = start;
        final Double endValue = end;
        final String id1 = id;
        input.addTextChangedListener(new TextValidator(input) {
            @Override
            public void validate(TextView textView, String text) {
                final String message = getResources().getString(R.string.characterization_mapping_input_qmet_label_range_validation1)
                        + " " + startValue
                        + " " + getResources().getString(R.string.characterization_mapping_input_qmet_label_range_validation2)
                        + " " + endValue;

                try {
                    Double inputValue = new Double(text);
                    if (inputValue.doubleValue() >= startValue.doubleValue() && inputValue.doubleValue() <= endValue.doubleValue()) {
                        isValid = true;
                        enteredValue = inputValue;
                    } else {
                        input.setError(message);
                        isValid = false;
                    }
                } catch (NumberFormatException e) {
                    input.setError(message);
                }
            }
        });

        final Srf srf1 = srf;
        Double valueFinal = round(valueSelected, 1);
        final SrfGroup srfGroup1 = srfGroup;

        input.setText(valueFinal.toString());
        alert.setView(input);
        alert.setCancelable(false);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                final AlertDialog alertDialog = (AlertDialog)dialog;

                if(input.getText().toString().isEmpty()) {
                    enteredValue = startValue;
                }

                if(isValid) {
                    mCallback.OnSrfSetValue(enteredValue, id1);
                    updateMap(srfGroup1, srf1);
                    alertDialog.dismiss();
                }
                else {
                    input.setError(getResources().getString(R.string.characterization_mapping_input_qmet_label_range_validation3));
                }

            }
        });

        alert.show();
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void setEnteredValue(Double enteredValue) {
        this.enteredValue = enteredValue;
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnSrfValueListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnSrfValueListener");
        }
    }

    public void setSelectionMap(EvaluationMethodSelectionType selectionMap) {
        this.selectionMap = selectionMap;
    }

    public EvaluationMethodSelectionType getSelectionMap() {
        return selectionMap;
    }

    public void setMappingFinished(Boolean mappingFinished) {
        this.mappingFinished = mappingFinished;
    }

    private void initDataset() {
        List<EvaluationMethodSelectionGroup> groups = selectionMap.getSelectionGroups();
        List<EvaluationMethodSelection> inputs;

        SrfGroup srfGroup;
        List<Srf> srfs;
        Srf srf;

        for(EvaluationMethodSelectionGroup e : groups) {
            srfGroup = new SrfGroup();
            srfGroup.setId(e.getId());
            srfGroup.setIndex(e.getIndex());
            srfGroup.setDescription((lang.equals("es") ? e.getDescriptionEs() : e.getDescription()));
            srfGroup.setChecked(e.getChecked());
            inputs = e.getSubSelections();

            srfs = new ArrayList<>();

            for(EvaluationMethodSelection em : inputs) {
                srf = new Srf();
                srf.setId(em.getId());
                srf.setIndex(em.getIndex());
                srf.setStart(em.getStart());
                srf.setEnd(em.getEnd().doubleValue() == 0.0 ? null : em.getEnd());
                srf.setDescription((lang.equals("es") ? em.getDescriptionEs() : em.getDescription()));
                srf.setChecked(em.getChecked());
                srfs.add(srf);
            }

            srfGroup.setSrfs(srfs);
            srfGroups.add(srfGroup);
        }
    }

    class Srf {
        private String id;
        private String index;
        private Double value;
        private Double start;
        private Double end;
        private String description;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public Double getStart() {
            return start;
        }

        public void setStart(Double start) {
            this.start = start;
        }

        public Double getEnd() {
            return end;
        }

        public void setEnd(Double end) {
            this.end = end;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }

    class SrfGroup {
        private String id;
        private String index;
        private String description;
        private List<Srf> srfs;
        private boolean checked;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<Srf> getSrfs() {
            return srfs;
        }

        public void setSrfs(List<Srf> srfs) {
            this.srfs = srfs;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }

    public abstract class TextValidator implements TextWatcher {
        private final TextView textView;

        public TextValidator(TextView textView) {
            this.textView = textView;
        }

        public abstract void validate(TextView textView, String text);

        @Override
        final public void afterTextChanged(Editable s) {
            String text = textView.getText().toString();
            validate(textView, text);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { /* Don't care */ }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { /* Don't care */ }
    }
}
