package cl.skava.mobile.tunnelapp.presentation.ui.activity.home;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.executor.impl.ThreadExecutor;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.presentation.presenters.impl.MainPresenterImpl;
import cl.skava.mobile.tunnelapp.presentation.presenters.interfaces.MainPresenter;
import cl.skava.mobile.tunnelapp.presentation.ui.activity.account.LoginActivity;
import cl.skava.mobile.tunnelapp.presentation.ui.activity.geoclassifier.GeoClassifierActivity;
import cl.skava.mobile.tunnelapp.presentation.ui.activity.prospection.ProspectionActivity;
import cl.skava.mobile.tunnelapp.presentation.ui.fragment.home.HomeFragment;
import cl.skava.mobile.tunnelapp.threading.MainThreadImpl;


/**
 * Created by Jose Ignacio Vera on 17-05-2016.
 */
public class HomeActivity extends AppCompatActivity
        implements MainPresenter.View,
        HomeFragment.OnHomeItemListener {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mMenuItemsStr;
    private Toolbar toolbar;
    private List<Module> mModuleDataset;
    private MainPresenter mPresenter;
    private static final int SYNC_CALL_REQUEST = 1;

    private ProgressDialog homeUIProgressDialog;

    private boolean userLogged = false;
    private User user;

    private boolean userCached;

    private View dialogContent;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new MainPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this);

        if (savedInstanceState == null) {
            Intent intent = new Intent(this, LoginActivity.class);
//        startActivity(intent);
            startActivityForResult(intent, SYNC_CALL_REQUEST);
//            initSyncUserData();
//            initComponents(savedInstanceState);
        }
    }

    private void initSyncUserData() {
        homeUIProgressDialog=new ProgressDialog(this);
        homeUIProgressDialog.setCancelable(false);
        homeUIProgressDialog.setMessage(getResources().getString(R.string.system_label_sync_user_data));
        homeUIProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        homeUIProgressDialog.setIndeterminate(false);
        homeUIProgressDialog.setProgress(0);
        homeUIProgressDialog.show();

        boolean isNetworkAvailable = isNetworkAvailable();
        mPresenter.syncUserData(this, isNetworkAvailable, user);
    }

    private void syncBasicData() {
        homeUIProgressDialog=new ProgressDialog(this);
        homeUIProgressDialog.setCancelable(false);
        homeUIProgressDialog.setMessage(getResources().getString(R.string.system_label_sync_user_data));
        homeUIProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        homeUIProgressDialog.setIndeterminate(false);
        homeUIProgressDialog.setProgress(0);
        homeUIProgressDialog.show();

        boolean isNetworkAvailable = isNetworkAvailable();
        mPresenter.syncBasicData(this, isNetworkAvailable, user);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SYNC_CALL_REQUEST) {
            if (resultCode == RESULT_OK) {
//                Intent i = getIntent();
//                mModuleDataset = (ArrayList<Module>) data.getParcelableExtra("module_data_set");
//                mModuleDataset = (ArrayList<Module>) data.getExtras().getSerializable("module_data_set");
//                mPresenter.resume();
                user = (User) data.getSerializableExtra("user");
                userCached = (boolean) data.getSerializableExtra("userCached");
                userLogged = true;
                initComponents(null);

//                if(userCached) {
//                    final Context c = this;
//
//                    Handler h = new Handler(Looper.getMainLooper());
//                    h.post(new Runnable() {
//                        public void run() {
//                            new android.support.v7.app.AlertDialog.Builder(c)
//                                    .setTitle(getResources().getString(R.string.account_label_user_already_logged))
//                                    .setMessage(getResources().getString(R.string.account_label_user_new_data))
//                                    .setCancelable(false)
//                                    .setIcon(android.R.drawable.ic_dialog_info)
//                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//
//                                        public void onClick(DialogInterface dialog, int whichButton) {
//                                            initSyncUserData();
//                                        }})
//                                    .setNegativeButton(getResources().getString(R.string.system_label_button_skip), new DialogInterface.OnClickListener() {
//
//                                        public void onClick(DialogInterface dialog, int whichButton) {
//                                            mPresenter.resume();
//                                        }})
//                                    .show();
//                        }
//                    });
//                }
//                else {
//                    initSyncUserData();
//                }

                if(!userCached) initSyncUserData();
                else mPresenter.resume();
            }
        }

        if (requestCode == 2 && resultCode == RESULT_OK) {
            String result = data.getStringExtra("result");

            if(result.equals("login")) {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivityForResult(intent, SYNC_CALL_REQUEST);
            }
        }
    }

    private void initComponents(Bundle savedInstanceState) {
        setContentView(R.layout.activity_home);
        mTitle = mDrawerTitle = getTitle();
        mMenuItemsStr = getResources().getStringArray(R.array.main_menu_items);

        NavigationView navigationView = (NavigationView)findViewById(R.id.navigation);
        View header = navigationView.getHeaderView(0);
        TextView user_email = (TextView) header.findViewById(R.id.user_email);
        user_email.setText(user.getEmail());
        TextView user_complete_name = (TextView) header.findViewById(R.id.user_complete_name);
        user_complete_name.setText(user.getFirstName() + " " + user.getLastName());
        TextView user_role = (TextView) header.findViewById(R.id.user_role);
        user_role.setText(getResources().getString(R.string.home_label_geologist));

        toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        setSupportActionBar(toolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,  mDrawerLayout, toolbar,
                R.string.drawer_open, R.string.drawer_close
        );
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        if (savedInstanceState == null) {
            setTitle(getResources().getString(R.string.app_name) + " - " + getResources().getString(R.string.home_label_choose));
            mDrawerToggle.syncState();
        }

        final Intent intent = new Intent(this, LoginActivity.class);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                mDrawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {
                    case R.id.home:
//                        Intent returnIntent = new Intent();
//                        returnIntent.putExtra("result", "home");
//                        setResult(Activity.RESULT_OK, returnIntent);
//                        finish();
                        return true;
                    case R.id.logout:
                        startActivityForResult(intent, SYNC_CALL_REQUEST);
                        return true;
                    default:
                        return true;

                }
            }
        });

        dialogContent = getLayoutInflater().inflate(R.layout.fragment_home_sync_select, null);
        RadioGroup groupType = (RadioGroup) dialogContent.findViewById(R.id.radio_group_type);
        groupType.check(R.id.radio_all);
    }

    @Override
    public void onResume() {
        super.onResume();

//        if (!isUserLogged()) {
//            Intent intent = new Intent(this, LoginActivity.class);
//            startActivityForResult(intent, SYNC_CALL_REQUEST);
//        }
    }

    private boolean isUserLogged() {
        return userLogged;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home_right, menu);

//        MenuItem item = toolbar.getMenu().findItem(R.id.sync_data);
//        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch(item.getItemId()) {
            case R.id.sync_data:
//                initSyncUserData();
                syncData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private int syncOption = 0;

    private void syncData() {

        RadioGroup groupType = (RadioGroup) dialogContent.findViewById(R.id.radio_group_type);

        groupType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                boolean isChecked = checkedRadioButton.isChecked();
                if (isChecked)
                {
                    switch (checkedRadioButton.getId()) {
                        case R.id.radio_basic:
                            syncOption = 1;
                            break;
                        default:
                            syncOption = 0;
                            break;
                    }
                }
            }
        });

        if(alertDialog == null)
            alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setTitle(getResources().getString(R.string.home_label_sync_title1));
        alertDialog.setMessage(getResources().getString(R.string.home_label_sync_title));
        // Alert dialog button
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.system_label_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "GO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {



                        switch (syncOption) {
                            case 1:
                                syncBasicData();
                                break;
                            default:
                                initSyncUserData();
                                break;
                        }
                        dialog.dismiss();
                    }
                });

        alertDialog.setView(dialogContent);
        alertDialog.show();
    }

    @Override
    public void userModulesLoaded() {
        homeUIProgressDialog.dismiss();
    }

    @Override
    public void initModuleActivity(Project project, Tunnel tunnel, Face face, Module module, Client client, List<FormationUnit> formationUnits, List<RockQuality> rockQualities) {
        Intent intent;

        switch(module.getCode()) {
            case 1:
                Log.i("HomeActivity", "CLASIFICACION GEOMECANICA ACTIVITY");

                if(module.getEnabled()) {
                    intent = new Intent(this, GeoClassifierActivity.class);
                    intent.putExtra("project", project);
                    intent.putExtra("tunnel", tunnel);
                    intent.putExtra("face", face);
                    intent.putExtra("module", module);
                    intent.putExtra("user", user);
                    intent.putExtra("client", client);
                    intent.putExtra("formationUnits", (Serializable) formationUnits);
                    intent.putExtra("rockQualities", (Serializable) rockQualities);
//                startActivity(intent);
                    startActivityForResult(intent, 2);
                }
                break;
            case 2:
                Log.i("HomeActivity", "PROSPECCION ACTIVITY");

                if(module.getEnabled()) {
                    intent = new Intent(this, ProspectionActivity.class);
                    intent.putExtra("project", project);
                    intent.putExtra("tunnel", tunnel);
                    intent.putExtra("face", face);
                    intent.putExtra("module", module);
                    intent.putExtra("user", user);
                    intent.putExtra("client", client);
                    intent.putExtra("rockQualities", (Serializable) rockQualities);
                    startActivityForResult(intent, 2);
                }
                break;
            case 3:
                Log.i("HomeActivity", "REPORTES ACTIVITY");
//                intent = new Intent(this, LoginActivity.class);
                break;
            default:
                Log.i("HomeActivity", "LOGIN ACTIVITY");
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
        }

//        startActivity(intent);
    }

    @Override
    public void changeTitle(Client client) {
        setTitle(getResources().getString(R.string.app_name) + " - " + client.getName().trim() + " - " + getResources().getString(R.string.home_label_choose));
    }

    @Override
    public void setupModuleDataset(List<Module> modules) {
        mModuleDataset = modules;
        initMainFragment();
    }

    @Override
    public void syncUserDataResult(int code) {
        homeUIProgressDialog.dismiss();

        switch(code){
            case 1:
                mPresenter.resume();
                break;
            default:
                final String alertTitle = getResources().getString(R.string.system_label_no_internet);
                final Context c = this;
                final String msg1 = getResources().getString(R.string.system_label_no_internet_msg);

                Handler h = new Handler(Looper.getMainLooper());
                h.post(new Runnable() {
                    public void run() {
                        new android.support.v7.app.AlertDialog.Builder(c)
                                .setTitle(alertTitle)
                                .setMessage(msg1)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(getResources().getString(R.string.system_label_button_retry), new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        switch (syncOption) {
                                            case 1:
                                                mPresenter.syncBasicData(c, isNetworkAvailable(), user);
                                                break;
                                            default:
                                                mPresenter.syncUserData(c, isNetworkAvailable(), user);
                                                break;
                                        }
                                    }
                                })
                                .setNegativeButton(getResources().getString(R.string.system_label_button_skip), new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
//                                        if(userCached) {
                                            mPresenter.resume();
//                                        }
//                                        else {
//                                            mPresenter.syncUserData(c, isNetworkAvailable(), user);
//                                        }
                                    }
                                })
                                .show();
                    }
                });
                break;
        }
    }

    @Override
    public void onSyncProgressPercentaje(Integer percentaje) {
        homeUIProgressDialog.setProgress(percentaje);
    }

    @Override
    public void onNetworkErrorUI(String msg) {
        homeUIProgressDialog.dismiss();

        final Context c = this;
        final String msg1 = msg;

        Handler h = new Handler(Looper.getMainLooper());
        h.post(new Runnable() {
            public void run() {
                        new android.support.v7.app.AlertDialog.Builder(c)
                .setTitle(getResources().getString(R.string.system_label_network_error))
                .setMessage(getResources().getString(R.string.system_label_network_error_msg1) + msg1 + getResources().getString(R.string.system_label_network_error_msg2))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(getResources().getString(R.string.system_label_button_retry), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        switch (syncOption) {
                            case 1:
                               syncBasicData();
                                break;
                            default:
                                initSyncUserData();
                                break;
                        }
                    }})
                .setNegativeButton(getResources().getString(R.string.system_label_button_skip), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                    }})
                .show();
            }
        });
    }

    @Override
    public void notifySyncSegment(String msg) {
        homeUIProgressDialog.setMessage(msg);
    }

    @Override
    public void showProgress() {
        homeUIProgressDialog = new ProgressDialog(HomeActivity.this, R.style.AppTheme_Dark_Dialog);
        homeUIProgressDialog.setIndeterminate(true);
        homeUIProgressDialog.setMessage(getResources().getString(R.string.system_label_preparing_user_interface));
        homeUIProgressDialog.show();
    }

    @Override
    public void hideProgress() {
//        homeUIProgressDialog.dismiss();
    }

    @Override
    public void showError(String message) {}

    private void initMainFragment() {
//        homeUIProgressDialog = new ProgressDialog(HomeActivity.this, R.style.AppTheme_Dark_Dialog);
//        homeUIProgressDialog.setIndeterminate(true);
//        homeUIProgressDialog.setMessage("Preparing User Interface...");
//        homeUIProgressDialog.show();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        HomeFragment fragment = new HomeFragment();
        fragment.setMModulesDataset(mModuleDataset);
        fragment.setUser(user);
        transaction.replace(R.id.content_frame_home, fragment);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
