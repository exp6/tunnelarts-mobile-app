package cl.skava.mobile.tunnelapp.domain.repository;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.prospection.ProspectionHoleGroupListInteractor;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroupCalculation;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;

/**
 * Created by Jose Ignacio Vera on 16-08-2016.
 */
public interface ProspectionHoleGroupRepository {

    boolean insert(ProspectionHoleGroup prospectionHoleGroup);

    boolean update(ProspectionHoleGroup prospectionHoleGroup);

    boolean delete(ProspectionHoleGroup prospectionHoleGroup);

    List<ProspectionHoleGroup> getList();

    HashMap<Mapping, QValue> getQValuesByMappings();

    HashMap<ProspectionHole, List<Rod>> getRodsByProspectionHole(String prospectionHoleGroupId);

    void setInteractor(ProspectionHoleGroupListInteractor interactor);

    void syncData(User user);

    CloudStoreService getCloudStoreService();

    List<ProspectionRefPicture> getRefPictures();

    boolean insertRefPicture(ProspectionRefPicture prospectionRefPicture);

    HashMap<ProspectionHoleGroup, List<ProspectionHoleGroupCalculation>> getCalculationsByGroup();

    boolean insertProspectionHoleGroupCalculations(List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations, String prospectionHoleGroupId);
}
