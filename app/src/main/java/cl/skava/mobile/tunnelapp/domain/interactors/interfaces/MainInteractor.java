package cl.skava.mobile.tunnelapp.domain.interactors.interfaces;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.interactors.base.Interactor;
import cl.skava.mobile.tunnelapp.domain.model.Module;

/**
 * Created by Jose Ignacio Vera on 24-06-2016.
 */
public interface MainInteractor extends Interactor {

    interface Callback {
        void onModuleListRetrieved(List<Module> modules);
        void onRetrievalFailed(String error);

    }
}
