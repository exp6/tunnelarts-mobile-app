package cl.skava.mobile.tunnelapp.domain.interactors.interfaces.prospection;

import java.util.HashMap;
import java.util.List;

import cl.skava.mobile.tunnelapp.domain.interactors.base.Interactor;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SelectionDetailLists;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroupCalculation;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public interface ProspectionHoleGroupListInteractor extends Interactor {

    interface Callback {
        void onProspectionHoleGroupListRetrieved(List<ProspectionHoleGroup> prospectionHoleGroups);
        void onQValuesByMappingsRetrieved(HashMap<Mapping, QValue> qValuesByMappings);
        void onSyncStatus(boolean status, String msg);
        void onSyncProgressPercentaje(Integer percentaje);
        void notifySyncSegment(String msg);
        void onRodsByProspectionHoleRetrieved(HashMap<ProspectionHole, List<Rod>> rodsByProspectionHole);
        void onExportStatus(boolean status);
        void onProspectionRefPictureListRetrieved(List<ProspectionRefPicture> prospectionRefPicture);
        void onProspectionHoleGroupCalculationListRetrieved(HashMap<ProspectionHoleGroup, List<ProspectionHoleGroupCalculation>> calculationsByGroup);
    }

    void create(ProspectionHoleGroup prospectionHoleGroup);
    void update(ProspectionHoleGroup prospectionHoleGroup);
    void delete(ProspectionHoleGroup prospectionHoleGroup);
    void syncData(User user);
    void onSyncError(String msg);
    void onSyncSuccess();
    void setSyncProgressPercentaje(Integer percentage);
    void notifySyncSegment(String msg);
    void getRodsByProspectionHoleMap(String prospectionHoleGroupId);
    void exportData(Project p, Tunnel t, Face f, String directoryPath);
    void savePicture(ProspectionRefPicture prospectionRefPicture);
    void updateProspectionHoleGroupCalculations(List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations, String prospectionHoleGroupId);
}
