package cl.skava.mobile.tunnelapp.domain.interactors.impl.geoclassifier;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.exports.base.ExportService;
import cl.skava.mobile.tunnelapp.domain.executor.base.Executor;
import cl.skava.mobile.tunnelapp.domain.executor.base.MainThread;
import cl.skava.mobile.tunnelapp.domain.interactors.base.AbstractInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.geoclassifier.MappingListInteractor;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SelectionDetailLists;
import cl.skava.mobile.tunnelapp.domain.repository.MappingInputRepository;
import cl.skava.mobile.tunnelapp.domain.repository.MappingRepository;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public class MappingListInteractorImpl extends AbstractInteractor
        implements MappingListInteractor {

    private MappingListInteractor.Callback mCallback;
    private MappingRepository mMappingRepository;
    MappingInputRepository mMappingInputRepository;

    private String mFaceId;

    private ExportService mExportService;

    public MappingListInteractorImpl(Executor threadExecutor,
                                     MainThread mainThread,
                                     MappingListInteractor.Callback callback,
                                     MappingRepository mappingRepository,
                                     MappingInputRepository mappingInputRepository,
                                     String faceId,
                                     ExportService exportService) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mMappingRepository = mappingRepository;
        mMappingInputRepository = mappingInputRepository;
        mFaceId = faceId;
        mExportService = exportService;
    }

    private void postMappings(final List<Mapping> mappings) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onMappingListRetrieved(mappings);
            }
        });
    }

    private void postMappingInputs(final List<MappingInputModule> mappingInputModules) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onMappingInputListRetrieved(mappingInputModules);
            }
        });
    }

    @Override
    public void run() {
        final List<Mapping> mappings = getList(mFaceId);

        if(mappings == null) {
            return;
        }

        postMappings(mappings);

        final List<MappingInputModule> mappingInputModules = mappings.isEmpty() ? new ArrayList<MappingInputModule>() : mMappingInputRepository.getList(mappings.get(mappings.size()-1).getId());
        postMappingInputs(mappingInputModules);
    }

    @Override
    public void create(Mapping mapping) {
        if(mMappingRepository.insert(mapping)) {
            final List<Mapping> mappings = getList(mapping.getIdFace());

            if(mappings == null) {
                return;
            }

            postMappings(mappings);

            final List<MappingInputModule> mappingInputModules = mappings.isEmpty() ? new ArrayList<MappingInputModule>() : mMappingInputRepository.getList(mappings.get(mappings.size()-1).getId());
            postMappingInputs(mappingInputModules);
        }
    }

    @Override
    public void update(Mapping mapping) {
        if(mMappingRepository.update(mapping)) {
            final List<Mapping> mappings = getList(mapping.getIdFace());

            if(mappings == null) {
                return;
            }

            postMappings(mappings);

            final List<MappingInputModule> mappingInputModules = mappings.isEmpty() ? new ArrayList<MappingInputModule>() : mMappingInputRepository.getList(mappings.get(mappings.size()-1).getId());
            postMappingInputs(mappingInputModules);
        }
    }

    @Override
    public void delete(Mapping mapping) {

        if(mMappingInputRepository.deleteInputs(mapping)) {
            if(mMappingRepository.delete(mapping)) {
                final List<Mapping> mappings = getList(mapping.getIdFace());

                if(mappings == null) {
                    return;
                }

                postMappings(mappings);

                final List<MappingInputModule> mappingInputModules = mappings.isEmpty() ? new ArrayList<MappingInputModule>() : mMappingInputRepository.getList(mappings.get(mappings.size()-1).getId());
                postMappingInputs(mappingInputModules);
            }
        }
    }

    @Override
    public List<Mapping> getList(String faceId) {
        final List<Mapping> mappings = mMappingRepository.getList(faceId);

        if(mappings == null) {
            return null;
        }

        return mappings;
    }

    @Override
    public void saveData() {
        mMappingRepository.setInteractor(this);
        mMappingRepository.saveData();
    }

    @Override
    public void getInputsByMapping(Mapping mapping) {
        final List<MappingInputModule> mappingInputModules = mMappingInputRepository.getList(mapping.getId());

        if(mappingInputModules == null) {
            return;
        }

        postMappingInputs(mappingInputModules);
    }

    @Override
    public void onSyncError(String msg) {
        final String msg1 = msg;
        final boolean status = false;
        postSyncStatus(status, msg1);
    }

    @Override
    public void onSyncSuccess() {
        final String msg = "";
        final boolean status = true;
        postSyncStatus(status, msg);
    }

    @Override
    public void syncData(User user) {
        mMappingRepository.setInteractor(this);
        mMappingRepository.syncDataModule(user);
    }

    @Override
    public void setSyncProgressPercentaje(Integer percentage) {
        mCallback.onSyncProgressPercentaje(percentage);
    }

    @Override
    public void notifySyncSegment(String msg) {
        mCallback.notifySyncSegment(msg);
    }

    @Override
    public void exportData(Project p, Tunnel t, Face f, String directoryPath, SelectionDetailLists selectionDetailLists) {
        final boolean status = mExportService.exportCharacterizerData(p, t, f, directoryPath, selectionDetailLists);
        postExportStatus(status);
    }

    private void postExportStatus(final boolean status) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onExportStatus(status);
            }
        });
    }

    private void postSyncStatus(final boolean status, final String msg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSyncStatus(status, msg);
            }
        });
    }
}
