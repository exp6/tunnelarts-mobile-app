package cl.skava.mobile.tunnelapp.domain.model.mapping;

/**
 * Created by Jose Ignacio Vera on 08-08-2016.
 */
public class AdditionalInformation extends DataInput {

    private String id;
    private String idMapping;
    private Boolean completed;

    public AdditionalInformation(String id, String idMapping, Boolean completed) {
        this.id = id;
        this.idMapping = idMapping;
        this.completed = completed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdMapping() {
        return idMapping;
    }

    public void setIdMapping(String idMapping) {
        this.idMapping = idMapping;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "AdditionalInformation{" +
                "id='" + id + '\'' +
                ", idMapping='" + idMapping + '\'' +
                ", completed=" + completed +
                '}';
    }
}
