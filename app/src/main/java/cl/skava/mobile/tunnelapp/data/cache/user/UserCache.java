package cl.skava.mobile.tunnelapp.data.cache.user;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.account.User;

/**
 * Created by Jose Ignacio Vera on 27-07-2016.
 */
public interface UserCache {
    User get(final String userId);
    List<User> getAllUsers();
    void put(User user);
    boolean isCached(final String userId);
    boolean isExpired();
    void evictAll();
}
