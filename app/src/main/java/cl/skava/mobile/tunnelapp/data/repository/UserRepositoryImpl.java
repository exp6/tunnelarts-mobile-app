package cl.skava.mobile.tunnelapp.data.repository;

import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.cache.user.UserCache;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.repository.UserRepository;

/**
 * Created by Jose Ignacio Vera on 25-08-2016.
 */
public class UserRepositoryImpl implements UserRepository {

    private UserCache mUserCache;


    public UserRepositoryImpl(UserCache userCache) {
        mUserCache = userCache;
    }

    @Override
    public List<User> getList() {
        return mUserCache.getAllUsers();
    }

    @Override
    public void writeCache(User user) {
        mUserCache.put(user);
    }

    @Override
    public boolean isCached(User user) {
        return mUserCache.isCached(user.getId());
    }

    @Override
    public User get(String userId) {
        return mUserCache.get(userId);
    }
}
