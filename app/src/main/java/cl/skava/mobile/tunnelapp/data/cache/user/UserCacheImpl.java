package cl.skava.mobile.tunnelapp.data.cache.user;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cl.skava.mobile.tunnelapp.data.cache.FileManager;
import cl.skava.mobile.tunnelapp.data.cache.JsonSerializer;
import cl.skava.mobile.tunnelapp.domain.model.account.User;

/**
 * Created by Jose Ignacio Vera on 27-07-2016.
 */
public class UserCacheImpl implements UserCache {
    private static final String DEFAULT_FILE_NAME = "user_";
    private static final long EXPIRATION_TIME = 60 * 10 * 1000;

//    private final File cacheDir;
    private final FileManager fileManager;
    private final JsonSerializer serializer;
//    private final ThreadExecutor threadExecutor;
    private String mDirectoryPath;

    public UserCacheImpl(FileManager fileManager, JsonSerializer userCacheSerializer, String directoryPath){
//        this.cacheDir = this.context.getCacheDir();
        this.serializer = userCacheSerializer;
        this.fileManager = fileManager;
//        this.threadExecutor = executor;
        mDirectoryPath = directoryPath;
    }

    @Override
    public User get(String userId) {
        File userEntityFile = UserCacheImpl.this.buildFile(userId);
        String fileContent = UserCacheImpl.this.fileManager.readFileContent(userEntityFile);
        User user = UserCacheImpl.this.serializer.deserialize(fileContent);
        return user;
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        File directory = new File(mDirectoryPath);
        File[] files = directory.listFiles();

        if(files != null) {
            String userJson;
            User user;

            for(File f : files) {
                userJson = this.fileManager.readFileContent(f);
                user = this.serializer.deserialize(userJson);
                users.add(user);
            }
        }

        return users;
    }

    @Override
    public void put(User user) {
        if (user != null) {
            File userEntitiyFile = this.buildFile(user.getId());
            if (!isCached(user.getId())) {
                String jsonString = this.serializer.serialize(user);
                new Thread(new CacheWriter(this.fileManager, userEntitiyFile, jsonString, mDirectoryPath)).start();
//                this.executeAsynchronously(new CacheWriter(this.fileManager, userEntitiyFile,
//                        jsonString));
//                setLastCacheUpdateTimeMillis();
            }
        }
    }

    @Override
    public boolean isCached(String userId) {
        File userFile = this.buildFile(userId);
        return this.fileManager.exists(userFile);
    }

    @Override
    public boolean isExpired() {
        long currentTime = System.currentTimeMillis();
        long lastUpdateTime = this.getLastCacheUpdateTimeMillis();

        boolean expired = ((currentTime - lastUpdateTime) > EXPIRATION_TIME);

        if (expired) {
            this.evictAll();
        }

        return expired;
    }

    @Override
    public void evictAll() {
//        this.executeAsynchronously(new CacheEvictor(this.fileManager, this.cacheDir));
    }

    private File buildFile(String userId) {
        StringBuilder fileNameBuilder = new StringBuilder();
        fileNameBuilder.append(mDirectoryPath);
        fileNameBuilder.append(File.separator);
        fileNameBuilder.append(DEFAULT_FILE_NAME);
        fileNameBuilder.append(userId);

        return new File(fileNameBuilder.toString());
    }

    private void setLastCacheUpdateTimeMillis() {
        long currentMillis = System.currentTimeMillis();
//        this.fileManager.writeToPreferences(this.context, SETTINGS_FILE_NAME,
//                SETTINGS_KEY_LAST_CACHE_UPDATE, currentMillis);
    }

    private long getLastCacheUpdateTimeMillis() {
//        return this.fileManager.getFromPreferences(this.context, SETTINGS_FILE_NAME,
//                SETTINGS_KEY_LAST_CACHE_UPDATE);
        return 0;
    }

//    private void executeAsynchronously(Runnable runnable) {
//        this.threadExecutor.execute(runnable);
//    }

    private static class CacheWriter implements Runnable {
        private final FileManager fileManager;
        private final File fileToWrite;
        private final String fileContent;
        private final String directory;

        CacheWriter(FileManager fileManager, File fileToWrite, String fileContent, String directory) {
            this.fileManager = fileManager;
            this.fileToWrite = fileToWrite;
            this.fileContent = fileContent;
            this.directory = directory;
        }

        @Override
        public void run() {
            this.fileManager.createDirectory(directory);
            this.fileManager.writeToFile(fileToWrite, fileContent);
        }
    }

    private static class CacheEvictor implements Runnable {
        private final FileManager fileManager;
        private final File cacheDir;

        CacheEvictor(FileManager fileManager, File cacheDir) {
            this.fileManager = fileManager;
            this.cacheDir = cacheDir;
        }

        @Override
        public void run() {
            this.fileManager.clearDirectory(this.cacheDir);
        }
    }
}
