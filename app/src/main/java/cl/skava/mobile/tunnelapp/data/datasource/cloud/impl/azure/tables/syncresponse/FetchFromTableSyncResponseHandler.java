package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse;

import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.model.UserProject;

/**
 * Created by Carlos Vergara on 20-03-2017.
 */

abstract class FetchFromTableSyncResponseHandler <T> implements SyncResponseHandler {

    private Class<T> classToFetchFrom;

    @Override
    public void handleResponseFromService(CloudMobileService cloudMobileService) {
        initializeClassToFetchFrom();
        CloudStoreService cloudStoreService = (CloudStoreService) cloudMobileService;
        cloudStoreService.incrementProgress();
        List<T> fetchedData = cloudStoreService.getSyncTableFromClass(classToFetchFrom);
        if (!fetchedData.isEmpty()) {
            onDataFetchedSuccessfully(cloudStoreService, fetchedData);
        } else {
            onDataFetchedFailed(cloudStoreService);
        }
    }

    protected abstract void onDataFetchedSuccessfully(CloudMobileService cloudMobileService, List<T> fetchedData);

    protected abstract void initializeClassToFetchFrom();

    protected void setClassToFetchFrom(Class<T> classToFetchFrom) {
        this.classToFetchFrom = classToFetchFrom;
    }

    protected void onDataFetchedFailed(CloudMobileService cloudMobileService) {
        cloudMobileService.onSyncResult(0);
    }
}
