package cl.skava.mobile.tunnelapp.domain.repository;

import java.util.List;

import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;

/**
 * Created by Jose Ignacio Vera on 21-06-2016.
 */
public interface FaceRepository {

    List<Face> getFacesByTunnel(Tunnel tunnel);
}
