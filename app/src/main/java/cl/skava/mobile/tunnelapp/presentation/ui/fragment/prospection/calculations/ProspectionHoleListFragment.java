package cl.skava.mobile.tunnelapp.presentation.ui.fragment.prospection.calculations;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.Module;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.prospection.ProspectionHoleListAdapter;

/**
 * Created by Jose Ignacio Vera on 10-08-2016.
 */
public class ProspectionHoleListFragment extends Fragment {

    private static final String TAG = "ProspectionHoleListFragment";

    protected RecyclerView mRecyclerView;
    protected ProspectionHoleListAdapter mAdapter;

    protected RecyclerView.LayoutManager mLayoutManager;

    protected List<ProspectionHole> mProspectionDataset;

    private OnProspectionItemSelectedListener mCallback;

    public interface OnProspectionItemSelectedListener {
        void onProspectionSelected(int position);
    }

    //User Selection Context
    private Project selectedProject;
    private Tunnel selectedTunnel;
    private Face selectedFace;
    private Module selectedModule;

    private User mUser;
    private ProspectionHoleGroup selectedProspectionHoleGroup;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_prospection_calculations_prospection_list, container, false);
        rootView.setTag(TAG);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        setRecyclerViewLayoutManager();
        mAdapter = new ProspectionHoleListAdapter(mProspectionDataset, mCallback, mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);

        TextView label = (TextView) rootView.findViewById(R.id.prospection_hole_label);
        label.setText(selectedProspectionHoleGroup.getName().trim());

        return rootView;
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            mCallback = (OnProspectionItemSelectedListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnProspectionItemSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setRecyclerViewLayoutManager() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMProspectionDataset(List<ProspectionHole> mProspectionDataset) {
        this.mProspectionDataset = mProspectionDataset;
    }

    public RecyclerView.LayoutManager getMLayoutManager() {
        return mLayoutManager;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void updateItemList() {
        mAdapter = new ProspectionHoleListAdapter(mProspectionDataset, mCallback, mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void setSelectionContext(Project project,
                                    Tunnel tunnel,
                                    Face face,
                                    Module module,
                                    User user,
                                    ProspectionHoleGroup prospectionHoleGroup) {
        selectedProject = project;
        selectedTunnel = tunnel;
        selectedFace = face;
        selectedModule = module;
        mUser = user;
        selectedProspectionHoleGroup = prospectionHoleGroup;
    }
}
