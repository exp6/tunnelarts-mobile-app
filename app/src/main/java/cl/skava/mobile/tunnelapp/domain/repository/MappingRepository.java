package cl.skava.mobile.tunnelapp.domain.repository;

import java.util.List;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.CloudStoreService;
import cl.skava.mobile.tunnelapp.domain.interactors.base.AbstractInteractor;
import cl.skava.mobile.tunnelapp.domain.interactors.interfaces.geoclassifier.MappingListInteractor;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.MappingInputModule;

/**
 * Created by Jose Ignacio Vera on 29-06-2016.
 */
public interface MappingRepository {

    boolean insert(Mapping mapping);

    boolean update(Mapping mapping);

    Mapping get(Object id);

    boolean delete(Mapping mapping);

    List<Mapping> getList(String faceId);

    List<Mapping> getAll();

    void saveData();

    void setInteractor(MappingListInteractor interactor);

    void syncDataModule(User user);

    CloudStoreService getCloudStoreService();
}
