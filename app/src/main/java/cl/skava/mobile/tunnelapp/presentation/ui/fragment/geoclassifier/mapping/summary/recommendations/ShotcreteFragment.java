package cl.skava.mobile.tunnelapp.presentation.ui.fragment.geoclassifier.mapping.summary.recommendations;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cl.skava.mobile.tunnelapp.R;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RecommendationShotcrete;
import cl.skava.mobile.tunnelapp.presentation.ui.adapters.geoclassifier.mapping.summary.ShotcreteListAdapter;

/**
 * Created by Jose Ignacio Vera on 11-10-2016.
 */
public class ShotcreteFragment extends Fragment {

    private RecyclerView recyclerView;

    private List<RecommendationShotcrete> shotcreteList;
    private ShotcreteListAdapter mAdapter;

    private int index;

    private FloatingActionButton myFab;

    private List<RecommendationShotcrete> shotcreteListDeleted;

    private Mapping selectedMapping;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_geoclassifier_mapping_summary_recommendations_shotcrete, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setReverseLayout(true);
        initDataset();

        mAdapter = new ShotcreteListAdapter(shotcreteList, recyclerView, this);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);

        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addItem();
            }
        });

        return rootView;
    }

    private void initDataset() {
        index = shotcreteList.size();
    }

    public void updateItemList() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        layoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new ShotcreteListAdapter(shotcreteList, recyclerView, this);
        recyclerView.setAdapter(mAdapter);
    }

    public void deleteItem(int position) {
        saveData();
        shotcreteList = mAdapter.getmShotcreteList();

        RecommendationShotcrete deleted = shotcreteList.get(position);
        if(!deleted.getId().trim().isEmpty()) shotcreteListDeleted.add(deleted);

        shotcreteList.remove(position);
        index = shotcreteList.size();
        if(index < 3) {
            myFab.setVisibility(View.VISIBLE);
        }
        updateItemList();
    }

    public void addItem() {
        saveData();
        shotcreteList = mAdapter.getmShotcreteList();
        RecommendationShotcrete shotcrete = new RecommendationShotcrete("", index, new Float(0), 0, new Float(0), 0, "", "", selectedMapping.getId(), false);
        shotcreteList.add(shotcrete);
        index = shotcreteList.size();
        if(index == 3) {
            myFab.setVisibility(View.GONE);
        }
        updateItemList();
    }

    public void setMyFab(FloatingActionButton myFab) {
        this.myFab = myFab;
    }

    public void saveData() {
        ShotcreteListAdapter.ViewHolder[] viewHolders = mAdapter.getViewHolderPool();
        for(int i = 0; i < viewHolders.length; i++) {
            ShotcreteListAdapter.ViewHolder viewHolder = viewHolders[i];
            RecommendationShotcrete shotcrete = shotcreteList.get(i);

            String input1 = viewHolder.getThickness().getText().toString().trim();
            int input2 = viewHolder.getFiberReinforced().getSelectedItemPosition();
            String input3 = viewHolder.getPk().getText().toString().trim();
            int input4 = viewHolder.getLocation().getSelectedItemPosition();
            String input5 = viewHolder.getTimeFrom().getText().toString().trim();
            String input6 = viewHolder.getTimeTo().getText().toString().trim();

            shotcrete.setThickness(input1.isEmpty() ? new Float(0) : new Float(input1));
            shotcrete.setFiberReinforced(input2);
            shotcrete.setPk(input3.isEmpty() ? new Float(0) : new Float(input3));
            shotcrete.setLocation(input4);
            shotcrete.setTimeFrom(input5);
            shotcrete.setTimeTo(input6);

            shotcreteList.set(i, shotcrete);
        }
    }

    public List<RecommendationShotcrete> getShotcreteList() {
        return shotcreteList;
    }

    public void setShotcreteList(List<RecommendationShotcrete> shotcreteList) {
        this.shotcreteList = shotcreteList;
    }

    public List<RecommendationShotcrete> getShotcreteListDeleted() {
        return shotcreteListDeleted;
    }

    public void setShotcreteListDeleted(List<RecommendationShotcrete> shotcreteListDeleted) {
        this.shotcreteListDeleted = shotcreteListDeleted;
    }

    public void setSelectedMapping(Mapping selectedMapping) {
        this.selectedMapping = selectedMapping;
    }
}
