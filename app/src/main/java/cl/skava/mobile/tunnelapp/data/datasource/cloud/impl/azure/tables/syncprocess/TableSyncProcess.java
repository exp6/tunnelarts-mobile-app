package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncprocess;

/**
 * Created by Jose Ignacio Vera on 23-09-2016.
 */
public interface TableSyncProcess {
    void doSync();
    void handleSyncResponse(boolean success, String entity);
}
