package cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables;

import android.content.Context;

import com.google.common.collect.Lists;
import com.google.common.util.concurrent.ListenableFuture;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.table.query.Query;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOperations;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;
import com.microsoft.windowsazure.mobileservices.table.sync.MobileServiceSyncTable;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.CloudMobileService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.base.DomainSyncService;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse.DefaultSyncResponseHandler;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse.SyncResponseHandler;
import cl.skava.mobile.tunnelapp.data.datasource.cloud.impl.azure.tables.syncresponse.SyncResponseHandlerFactoryImpl;
import cl.skava.mobile.tunnelapp.data.net.cloud.impl.azure.tables.SyncClient;
import cl.skava.mobile.tunnelapp.data.repository.ChangeLog;
import cl.skava.mobile.tunnelapp.domain.model.Client;
import cl.skava.mobile.tunnelapp.domain.model.Face;
import cl.skava.mobile.tunnelapp.domain.model.FormationUnit;
import cl.skava.mobile.tunnelapp.domain.model.MappingInput;
import cl.skava.mobile.tunnelapp.domain.model.Project;
import cl.skava.mobile.tunnelapp.domain.model.ProjectMappingInput;
import cl.skava.mobile.tunnelapp.domain.model.RockQuality;
import cl.skava.mobile.tunnelapp.domain.model.Tunnel;
import cl.skava.mobile.tunnelapp.domain.model.UserProject;
import cl.skava.mobile.tunnelapp.domain.model.account.User;
import cl.skava.mobile.tunnelapp.domain.model.mapping.AdditionalDescription;
import cl.skava.mobile.tunnelapp.domain.model.mapping.AdditionalInformation;
import cl.skava.mobile.tunnelapp.domain.model.mapping.BaseData;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Discontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.ExpandedTunnel;
import cl.skava.mobile.tunnelapp.domain.model.mapping.FailureZone;
import cl.skava.mobile.tunnelapp.domain.model.mapping.GsiValue;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Lithology;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Mapping;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Particularities;
import cl.skava.mobile.tunnelapp.domain.model.mapping.Picture;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RecommendationRockbolt;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RecommendationShotcrete;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMass;
import cl.skava.mobile.tunnelapp.domain.model.mapping.RockMassHazard;
import cl.skava.mobile.tunnelapp.domain.model.mapping.SpecialFeatures;
import cl.skava.mobile.tunnelapp.domain.model.mapping.StereonetPicture;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValue;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueDiscontinuity;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInput;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputGroupType;
import cl.skava.mobile.tunnelapp.domain.model.mapping.qvalue.QValueInputSelection;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.Rmr;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInput;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputGroup;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputGroupType;
import cl.skava.mobile.tunnelapp.domain.model.mapping.rmr.RmrInputSelection;
import cl.skava.mobile.tunnelapp.domain.model.modelentity.ModelEntity;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHole;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroup;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionHoleGroupCalculation;
import cl.skava.mobile.tunnelapp.domain.model.prospection.ProspectionRefPicture;
import cl.skava.mobile.tunnelapp.domain.model.prospection.Rod;
import cl.skava.mobile.tunnelapp.domain.repository.BaseRepository;

/**
 * Created by Jose Ignacio Vera on 19-07-2016.
 */
public class CloudStoreService extends LocalStoreServiceImpl implements CloudMobileService, DomainSyncService {

    private BaseRepository mCallback;
    private final String APP_SERVICE_URI = "https://tunnelarts.azurewebsites.net/";
    private MobileServiceClient mClient;
    private SyncClient provider;
    private int progressIndex = 0;
    private int totalProgress;
    private int mappingProgressIndex;
    private int totalMappingProgress;
    private User mUser;
    private Boolean syncAll;
    private Boolean picturesOnly = false;
    private Boolean expandedOnly = false;
    private Boolean overbreakOnly = false;
    private Boolean gsiOnly = false;
    private Boolean prospectionPicturesOnly = false;
    private Boolean stereonetOnly = false;

    private List<ChangeLog> changeLogs = new ArrayList<>();

    private Boolean syncFaceProgress = false;

    private final String lang = Locale.getDefault().getLanguage();

    public void setSyncFaceProgress(Boolean syncFaceProgress) {
        this.syncFaceProgress = syncFaceProgress;
    }

    private List<Mapping> selectedMappings;

    public List<Mapping> getSelectedMappings() {
        return selectedMappings;
    }

    public void setSelectedMappings(List<Mapping> selectedMappings) {
        this.selectedMappings = selectedMappings;
    }

    public CloudStoreService(Context activity, BaseRepository callback, User user){
        mCallback = callback;
        mUser = user;
        initService(activity);
    }

    public CloudStoreService(Context activity){
        initService(activity);
    }

    private void initService(Context activity) {
        syncAll = false;

        try {
            mClient = new MobileServiceClient(APP_SERVICE_URI, activity);
            setMobileServiceClient(mClient);
            provider = new SyncClient(this, mClient);
            List<String> tablesEntities = CloudStoreServiceHelper.getTableList();
            tablesEntities.addAll(CloudStoreServiceHelper.getChangeLogReference());

            provider.initLocalStore(tablesEntities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void initBasicService(Context activity) {
        syncAll = false;

        try {
            mClient = new MobileServiceClient(APP_SERVICE_URI, activity);
            setMobileServiceClient(mClient);
            provider = new SyncClient(this, mClient);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initDataRepository() {
        List<String> changeLogReference = CloudStoreServiceHelper.getChangeLogReference();
        getSyncChangeLog();
    }

    @Override
    public void syncAllData() {
        syncAll = true;
        syncBaseData();
    }

    @Override
    public void setSyncProgressPercentage(Integer percentage) {
        mCallback.setSyncProgress(percentage);
    }

    @Override
    public void onSyncResult(int code) {
        mCallback.onSyncResult(code);
    }

    @Override
    public void syncEntity(Class c, Query mSyncQuery) {
        syncEntity(c, mSyncQuery, true);
    }

    public void syncEntity(Class c, Query mSyncQuery, boolean doHandleSync) {
        provider.pullData(c, mSyncQuery, doHandleSync);
    }

    @Override
    public void pushAllData() {
        provider.pushAllEntities();
    }

    @Override
    public void setPushSyncStatus(boolean success, String msg) {
        if(success)
            mCallback.onSyncResult(1);
        else
            networkError(msg);
    }

    public void syncBaseData() {
        List<Class> entityTables = CloudStoreServiceHelper.getBaseDataEntities();
        initSyncProgress(lang.equals("es") ? "Sincronizando Datos Base..." : "Synchronizing Base Data...", entityTables.size());
        syncClient();
    }

    public void syncGeoCharacterizerData() {
        syncModuleData(0);
    }

    public void syncForecastingData() {
        syncModuleData(1);
    }

    private void syncModuleData(int code) {
        List<Face> faces = getFaces();
        if(!faces.isEmpty()) {
            List<Class> entityTables;
            String progressLabel;

            switch(code) {
                case 1:
                    entityTables = CloudStoreServiceHelper.getProspectionEntities();
                    progressLabel = lang.equals("es") ? "Sincronizando Datos Pronóstico..." : "Synchronizing Forecast Data...";
                    syncProspectionHoleGroups(faces);
                    break;
                default:
                    entityTables = CloudStoreServiceHelper.getGeoCharacterizerEntities();
                    progressLabel = lang.equals("es") ? "Sincronizando Datos Caracterización..." : "Synchronizing Characterization Data...";
                    syncMappings(faces);
                    break;
            }

            initSyncProgress(progressLabel, entityTables.size());
        }
        else {
            onSyncResult(0);
        }
    }

    public void initSyncProgress(String progressLabel ,int totalElements) {
        progressIndex = 0;
        totalProgress = totalElements;
        mCallback.notifySyncSegment(progressLabel);
        setSyncProgressPercentage(CloudStoreServiceHelper.getSyncPercentage(totalProgress, progressIndex));
    }

    public void syncClient() {
        //Pull Client
        List<String> values = new ArrayList<>();
        values.add(mUser.getIdClient().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(Client.class, "id", values);
        syncEntity(Client.class, mSyncQuery);
    }

    public void syncProjectsByUser() {
        //Pull Projects by User
        List<String> values = new ArrayList<>();
        values.add(mUser.getId().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(UserProject.class, "IdUser", values);
        syncEntity(UserProject.class, mSyncQuery);
    }

    public void syncProjects(List<UserProject> userProjects) {
        //Pull Projects
        List<String> values = new ArrayList<>();
        for(UserProject up : userProjects) values.add(up.getIdProject().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(Project.class, "id", values);
        syncEntity(Project.class, mSyncQuery);
    }

    private void getSyncChangeLog() {
        //Pull change logs
        Query mSyncQuery = QueryOperations.tableName(ChangeLog.class.getSimpleName())
                .field("IdClient").eq(mUser.getIdClient().toLowerCase());
        syncEntity(ChangeLog.class, mSyncQuery);
    }

    @Override
    public void syncFormationUnit(List<Project> projects) {
        //Pull Formation Unit
        List<String> values = new ArrayList<>();
        for(Project p : projects) values.add(p.getId().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(FormationUnit.class, "IdProject", values);
        syncEntity(FormationUnit.class, mSyncQuery);
    }

    @Override
    public void syncRockQuality(List<Project> projects) {
        //Pull Rock Quality
        List<String> values = new ArrayList<>();
        for(Project p : projects) values.add(p.getId().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(RockQuality.class, "IdProject", values);
        syncEntity(RockQuality.class, mSyncQuery);
    }

    @Override
    public void syncProjectMappingInputs(List<Project> projects) {
        //Pull Project Mapping Inputs
        List<String> values = new ArrayList<>();
        for(Project p : projects) values.add(p.getId().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(ProjectMappingInput.class, "IdProject", values);
        syncEntity(ProjectMappingInput.class, mSyncQuery);
    }

    @Override
    public void syncMappingInputs() {
        //Pull Mapping Inputs
        Query mSyncQuery = QueryOperations.tableName(MappingInput.class.getSimpleName());
        syncEntity(MappingInput.class, mSyncQuery);
    }

    @Override
    public void syncMappingsById(List<Mapping> mappings) {
        //Pull Mappings by id
        List<String> values = new ArrayList<>();
        for(Mapping m : mappings) values.add(m.getId().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(Mapping.class, "id", values);
        syncEntity(Mapping.class, mSyncQuery);
    }

    public void syncTunnels(List<Project> projects) {
        //Pull Tunnels
        List<String> values = new ArrayList<>();
        for(Project p : projects) values.add(p.getId().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(Tunnel.class, "IdProject", values);
        syncEntity(Tunnel.class, mSyncQuery);
    }

    public void syncFaces(List<Tunnel> tunnels) {
        //Pull Faces
        List<String> values = new ArrayList<>();
        for(Tunnel t : tunnels) values.add(t.getId().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(Face.class, "IdTunnel", values);
        syncEntity(Face.class, mSyncQuery);
    }

    public void syncQValuesDiscontinuities(List<QValue> qValues) {
        //Pull QValuesDiscontinuities
        List<String> ids = getListOfIds(qValues);
        batchSyncIdsByEntityAndField(ids, QValueDiscontinuity.class, "IdQValue");
    }

    private List<String> getListOfIds(List<? extends ModelEntity> entities) {
        List<String> ids = new ArrayList<>();
        for (ModelEntity entity : entities) {
            ids.add(entity.getId().toLowerCase());
        }
        return ids;
    }

    private void batchSyncIdsByEntityAndField(List<String> ids, Class c, String fieldToQuery) {
        int QUERY_BATCH_SIZE = 10;
        List<List<String>> partitionedIds = Lists.partition(ids, QUERY_BATCH_SIZE);
        int amountOfPartitions = partitionedIds.size();
        int currentPartition = 0;
        for (List<String> idBatch : partitionedIds) {
            currentPartition++;
            Query mSyncQuery = getSyncQueryByField(c , fieldToQuery, idBatch);
            boolean doResponseHandler = amountOfPartitions == currentPartition;
            syncEntity(c, mSyncQuery, doResponseHandler);
        }
    }

    public void syncMappings(List<Face> faces) {
        //Pull Mappings
        List<String> values = new ArrayList<>();
        for(Face f : faces) values.add(f.getId().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(Mapping.class, "IdFace", values);
        syncEntity(Mapping.class, mSyncQuery);
    }

//    public void syncMappingsInputs(List<Mapping> mappings) {
//        //Pull MappingInputs
//        String mappingId;
//
//        List<Class> mappingInputs = CloudStoreServiceHelper.getMappingInputEntities();
//
//        mappingProgressIndex = 0;
//        totalMappingProgress = mappings.size() * mappingInputs.size();
//        totalProgress = totalProgress + (mappings.size()-1) * mappingInputs.size();
//
//        for(int i = 0; i < mappings.size(); i++) {
//            mappingId = mappings.get(i).getId().toLowerCase();
//
//            for(Class c : mappingInputs) {
//                syncMappingInput(c, mappingId);
//            }
//        }
//    }

    public void syncMappingsInputs(List<Mapping> mappings) {
        //Pull MappingInputs
        String mappingId;

        HashMap<String, List<Class>> mappingInputsPerProject = new HashMap<>();
        List<Project> projects = getProjects();
        List<ProjectMappingInput> projectMappingInputs;
        List<MappingInput> mappingInputsModules = getMappingInputs();
        List<MappingInput> mappingInputsModulesEnabled;
        List<Class> totalEntities;
        for(Project p : projects) {
            projectMappingInputs = getMappingInputsByProject(p.getId());
            mappingInputsModulesEnabled = findMappingInputsEnabled(projectMappingInputs, mappingInputsModules);
            totalEntities = new ArrayList<>();
            for(MappingInput mi : mappingInputsModulesEnabled) {
                totalEntities = setMappingInputEntity(mi.getLabel().trim(), totalEntities);
            }
            mappingInputsPerProject.put(p.getId(), totalEntities);
        }

        Tunnel tunnel;
        int inputTotalSize = 0;
        for(Mapping m : mappings) {
            tunnel = getTunnelByFace(m.getIdFace());
            inputTotalSize += mappingInputsPerProject.get(tunnel.getIdProject()).size();
        }

        mappingProgressIndex = 0;
        totalMappingProgress = inputTotalSize;
        totalProgress += totalMappingProgress;

        for(int i = 0; i < mappings.size(); i++) {
            mappingId = mappings.get(i).getId().toLowerCase();
            tunnel = getTunnelByFace(mappings.get(i).getIdFace());
            totalEntities = mappingInputsPerProject.get(tunnel.getIdProject());

            for(Class c : totalEntities) {
                syncMappingInput(c, mappingId);
            }
        }
    }

    public List<MappingInput> findMappingInputsEnabled(List<ProjectMappingInput> projectMappingInputs, List<MappingInput> mappingInputsModules) {
        List<MappingInput> mappingInputs = new ArrayList<>();

        for(ProjectMappingInput pmi : projectMappingInputs) {
            for(MappingInput mi : mappingInputsModules) {
                if(pmi.getIdMappingInput().trim().equalsIgnoreCase(mi.getId().trim())) {
                    mappingInputs.add(mi);
                    break;
                }
            }
        }
        return mappingInputs;
    }

    public List<Class> setMappingInputEntity(String label, List<Class> totalEntities) {
        switch(label) {
            case "RMO":
                totalEntities.add(BaseData.class);
                totalEntities.add(RockMass.class);
                break;
            case "LTH":
                totalEntities.add(Lithology.class);
                break;
            case "DISC":
                totalEntities.add(Discontinuity.class);
                totalEntities.add(StereonetPicture.class);
                break;
            case "AINFO":
                totalEntities.add(AdditionalInformation.class);
                totalEntities.add(FailureZone.class);
                totalEntities.add(Particularities.class);
                totalEntities.add(SpecialFeatures.class);
                totalEntities.add(RockMassHazard.class);
                break;
            case "QMET":
                totalEntities.add(QValue.class);
                totalEntities.add(QValueInputSelection.class);
                break;
            case "RMR":
                totalEntities.add(Rmr.class);
                totalEntities.add(RmrInputSelection.class);
                break;
            case "GSI":
                totalEntities.add(GsiValue.class);
                break;
            case "PIC":
                totalEntities.add(Picture.class);
                break;
            case "EXPT":
                totalEntities.add(ExpandedTunnel.class);
                break;
            case "GCOMM":
                totalEntities.add(AdditionalDescription.class);
                break;
            case "SUMM":
                totalEntities.add(RecommendationRockbolt.class);
                totalEntities.add(RecommendationShotcrete.class);
                break;
        }
        return totalEntities;
    }

    public void syncMappingInput(Class c, String mappingId) {
        Query mSyncQuery = QueryOperations.tableName(c.getSimpleName());
        mSyncQuery.field("IdMapping").eq(mappingId);
        syncEntity(c, mSyncQuery);
    }

    public void syncProspectionHoleGroups(List<Face> faces) {
        //Pull ProspectionHoleGroups.
        List<String> values = new ArrayList<>();
        for(Face f : faces) values.add(f.getId().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(ProspectionHoleGroup.class, "IdFace", values);
        syncEntity(ProspectionHoleGroup.class, mSyncQuery);
    }

    public void syncProspectionHoles(List<ProspectionHoleGroup> prospectionHoleGroups) {
        //Pull ProspectionHoles
        List<String> values = new ArrayList<>();
        for(ProspectionHoleGroup phg : prospectionHoleGroups) values.add(phg.getId().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(ProspectionHole.class, "IdProspectionHoleGroup", values);
        syncEntity(ProspectionHole.class, mSyncQuery);
    }

    public void syncProspectionRefPictures(List<ProspectionHoleGroup> prospectionHoleGroups) {
        //Pull ProspectionRefPictures
        List<String> values = new ArrayList<>();
        for(ProspectionHoleGroup phg : prospectionHoleGroups) values.add(phg.getId().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(ProspectionRefPicture.class, "IdProspectionHoleGroup", values);
        syncEntity(ProspectionRefPicture.class, mSyncQuery);
    }

    public void syncProspectionHoleGroupCalculations(List<ProspectionHoleGroup> prospectionHoleGroups) {
        //Pull ProspectionHoleGroupCalculations
        List<String> values = new ArrayList<>();
        for(ProspectionHoleGroup phg : prospectionHoleGroups) values.add(phg.getId().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(ProspectionHoleGroupCalculation.class, "IdProspectionHoleGroup", values);
        syncEntity(ProspectionHoleGroupCalculation.class, mSyncQuery);
    }

    public void syncRods(List<ProspectionHole> prospections) {
        //Pull Rods
        List<String> values = new ArrayList<>();
        for(ProspectionHole ph : prospections) values.add(ph.getId().toLowerCase());
        Query mSyncQuery = getSyncQueryByField(Rod.class, "IdProspectionHole", values);
        syncEntity(Rod.class, mSyncQuery);
    }

    private Query getSyncQueryByField(Class entityClass, String fieldName, List<String> values) {
        Query mSyncQuery = QueryOperations.tableName(entityClass.getSimpleName());

        for(int i = 0; i < values.size(); i++) {
            if(i < values.size()-1) mSyncQuery.field(fieldName).eq(values.get(i)).or();
            else mSyncQuery.field(fieldName).eq(values.get(i));
        }
        return mSyncQuery;
    }

    public void syncSingleTable(Class c) {
        Query mSyncQuery = QueryOperations.tableName(c.getSimpleName());
        syncEntity(c, mSyncQuery);
    }

    public void syncPictures(Class c) {
        //Pull Pictures
        picturesOnly = true;
        syncSingleTable(c);
    }

    public void syncExpanded(Class c) {
        //Pull Expanded
        picturesOnly = false;
        expandedOnly = true;
        syncSingleTable(c);
    }

    public void syncParticularities(Class c) {
        overbreakOnly = true;
        syncSingleTable(c);
    }

    public void syncStereonet(Class c) {
        //Pull Expanded
        overbreakOnly = false;
        stereonetOnly = true;
        syncSingleTable(c);
    }

    public void syncGsi(Class c) {
        //Pull Gsi
        expandedOnly = false;
        gsiOnly = true;
        syncSingleTable(c);
    }

    public void syncProspectionPictures(Class c) {
        //Pull Prospection Pictures
        prospectionPicturesOnly = true;
        syncSingleTable(c);
    }

    public void incrementProgress() {
        ++progressIndex;
        setSyncProgressPercentage(CloudStoreServiceHelper.getSyncPercentage(totalProgress, progressIndex));
    }

    public Boolean isPicturesOnly() {
        return picturesOnly;
    }

    public Boolean isExpandedOnly() {
        return expandedOnly;
    }

    public Boolean isOverbreakOnly() {
        return overbreakOnly;
    }

    public Boolean isStereonetOnly() {
        return stereonetOnly;
    }

    public void setExpandedOnly(Boolean expandedOnly) {
        this.expandedOnly = expandedOnly;
    }

    public void setOverbreakOnly(Boolean overbreakOnly) {
        this.overbreakOnly = overbreakOnly;
    }

    public void setStereonetOnly(Boolean stereonetOnly) {
        this.stereonetOnly = stereonetOnly;
    }

    @Override
    public void handleSyncResponse(boolean success, String entity) {
        if (success) {
            SyncResponseHandlerFactoryImpl syncResponseHandlerFactory = new SyncResponseHandlerFactoryImpl();
            SyncResponseHandler syncResponseHandler = syncResponseHandlerFactory.createHandler(entity);
            syncResponseHandler.handleResponseFromService(this);
        } else {
            onSyncResult(0);
        }
    }

    public void handleChangeLog() {
        try {
            changeLogs = mClient.getSyncTable(ChangeLog.class)
                    .read(QueryOperations.tableName(ChangeLog.class.getSimpleName()).orderBy("createdAt", QueryOrder.Descending))
                    .get();

            Date currentCacheDate = new Date();

            if(changeLogs != null) {
                if(changeLogs.isEmpty()) {
                    //Sync all
                    ChangeLog newCL = new ChangeLog();
                    newCL.setCreatedAt(currentCacheDate);
                    newCL.setIdClient(mUser.getIdClient().toLowerCase());
                    mClient.getSyncTable(ChangeLog.class).insert(newCL).get();

//                    provider.pushAllEntities();
                    syncAllData();
                }
                else {
                    ChangeLog changeLog = changeLogs.get(0);

                    //Need to compare with cache data
                    if(!changeLog.getCreatedAt().equals(currentCacheDate)) {
                        //Sync all
                        ChangeLog newCL = new ChangeLog();
                        newCL.setCreatedAt(currentCacheDate);
                        newCL.setIdClient(mUser.getIdClient().toLowerCase());
                        mClient.getSyncTable(ChangeLog.class).insert(newCL).get();

//                        provider.pushAllEntities();
                        syncAllData();
                    }
                    else {
                        onSyncResult(1);
                    }
                }
            }
//            else {
//                //Something wrong with cloud database
//            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void networkError(String msg) {
        mCallback.onNetworkError(msg);
    }

    public void setmCallback(BaseRepository mCallback) {
        this.mCallback = mCallback;
    }

    public void sendStorageResultCodeToCallback(int code) {
        mCallback.onSyncStorageResult(code, syncAll);
    }

    public void increaseMappingProgressIndex() {
        this.mappingProgressIndex++;
    }

    public void finishMappingOrProceed() {
        if(mappingProgressIndex == totalMappingProgress) {
            if(isSyncAll()) syncSingleTable(QValueInputGroupType.class);
            else doSyncStorageData();
        }
        else {
            incrementProgress();
        }
    }

    public <T> List<T> getSyncTableFromClass(Class<T> c) {
        MobileServiceSyncTable<T> syncTable = mClient.getSyncTable(c);
        Query selectFromTable = QueryOperations.tableName(c.getSimpleName());
        ListenableFuture<MobileServiceList<T>> asyncReadFromTable = syncTable.read(selectFromTable);
        List<T> resultDataset;
        try {
            resultDataset = asyncReadFromTable.get();
        } catch (Exception e) {
            resultDataset = new ArrayList<>();
            e.printStackTrace();
        }
        return resultDataset;
    }

    public Boolean isSyncAll() {
        return syncAll;
    }

    public Boolean isSyncFaceProgress() {
        return syncFaceProgress;
    }

    public void doSyncStorageData() {
        mCallback.syncStorageData(syncAll, 0);
    }

    public Boolean isProspectionPicturesOnly() {
        return prospectionPicturesOnly;
    }

    public void setProspectionPicturesOnly(Boolean prospectionPicturesOnly) {
        this.prospectionPicturesOnly = prospectionPicturesOnly;
    }

    public void fetchForecastData() {
        mCallback.onSyncStorageResult(7, syncAll);
    }
}
